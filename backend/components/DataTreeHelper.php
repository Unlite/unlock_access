<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 19.04.2019
 * Time: 04:06:38
 */

namespace backend\components;


class DataTreeHelper
{
	public static function renderTree($data)
	{
		return self::renderSubTree(json_decode($data));
	}

	public static function renderSubTree($data,$str = '')
	{
		$str .= '<table class="table" style="margin-bottom: 0">';
		foreach ($data as $key => $value){
			$str .= '<tr>';
			if((is_array($value) || is_object($value)) && !empty($value)) {
				$str .= '<td>'.$key.'</td>';
				$str .= '<td>';
				$str .= self::renderSubTree($value);
				$str .= '</td>';

			} else{
				$str .= '<td>';
				$str .= $key;
				$str .= '</td>';
				$str .= '<td>';
				$str .= is_string($value) || is_int($value) || is_float($value) ? $value : '';
				$str .= '</td>';
			}
			$str .= '</tr>';
		}
		$str .= '</table>';
		return $str;
	}
}