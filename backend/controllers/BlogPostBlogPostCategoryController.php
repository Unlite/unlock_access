<?php

namespace backend\controllers;

use Yii;
use common\models\BlogPostBlogPostCategory;
use backend\models\search\BlogPostBlogPostCategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BlogPostBlogPostCategoryController implements the CRUD actions for BlogPostBlogPostCategory model.
 */
class BlogPostBlogPostCategoryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BlogPostBlogPostCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BlogPostBlogPostCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BlogPostBlogPostCategory model.
     * @param integer $blog_post_id
     * @param integer $blog_post_category_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($blog_post_id, $blog_post_category_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($blog_post_id, $blog_post_category_id),
        ]);
    }

    /**
     * Creates a new BlogPostBlogPostCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
	 * @param null|string $back
     * @return mixed
     */
    public function actionCreate($back = null)
    {
        $model = new BlogPostBlogPostCategory();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'blog_post_id' => $model->blog_post_id, 'blog_post_category_id' => $model->blog_post_category_id]);
        }

        return $this->render('create', [
            'model' => $model,
			'back' => $back,
        ]);
    }

    /**
     * Updates an existing BlogPostBlogPostCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $blog_post_id
     * @param integer $blog_post_category_id
	 * @param null|string $back
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($blog_post_id, $blog_post_category_id,$back = null)
    {
        $model = $this->findModel($blog_post_id, $blog_post_category_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?:['view', 'blog_post_id' => $model->blog_post_id, 'blog_post_category_id' => $model->blog_post_category_id]);
        }

        return $this->render('update', [
            'model' => $model,
			'back' => $back,
        ]);
    }

    /**
     * Deletes an existing BlogPostBlogPostCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $blog_post_id
     * @param integer $blog_post_category_id
	 * @param null|string $back
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($blog_post_id, $blog_post_category_id, $back = null )
    {
        $this->findModel($blog_post_id, $blog_post_category_id)->delete();

        return $this->redirect($back ?: ['index']);
    }

    /**
     * Finds the BlogPostBlogPostCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $blog_post_id
     * @param integer $blog_post_category_id
     * @return BlogPostBlogPostCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($blog_post_id, $blog_post_category_id)
    {
        if (($model = BlogPostBlogPostCategory::findOne(['blog_post_id' => $blog_post_id, 'blog_post_category_id' => $blog_post_category_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
