<?php

namespace backend\controllers;

use common\models\forms\BonusesForm;
use Yii;
use common\models\Bonuses;
use backend\models\search\BonusesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BonusesController implements the CRUD actions for Bonuses model.
 */
class BonusesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Bonuses models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BonusesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Bonuses model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Bonuses model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($back = null)
    {
        $model = new BonusesForm();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect($back ?: ['view', 'id' => $model->id]);
		} else {
			return $this->render('create', [
				'model' => $model,
				'back' => $back
			]);
		}
    }

    /**
     * Updates an existing Bonuses model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id,$back = null)
    {
        $model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect($back ?: ['view', 'id' => $model->id]);
		} else {
			return $this->render('update', [
				'model' => $model,
				'back' => $back
			]);
		}
    }

    /**
     * Deletes an existing Bonuses model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $back = null)
    {
		$this->findModel($id)->delete();

		return $this->redirect($back ?: ['index']);
    }

    /**
     * Finds the Bonuses model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Bonuses the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BonusesForm::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
