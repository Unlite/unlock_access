<?php

namespace backend\controllers;

use backend\models\forms\CustomListSiteMultiForm;
use Yii;
use common\models\CustomListSite;
use backend\models\search\CustomListSiteSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CustomListSiteController implements the CRUD actions for CustomListSite model.
 */
class CustomListSiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CustomListSite models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CustomListSiteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CustomListSite model.
     * @param integer $custom_list_id
     * @param integer $site_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($custom_list_id, $site_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($custom_list_id, $site_id),
        ]);
    }

    /**
     * Creates a new CustomListSite model.
     * If creation is successful, the browser will be redirected to the 'view' page.
	 * @param null|string $back
     * @return mixed
     */
    public function actionCreate($back = null)
    {
        $model = new CustomListSite();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect( $back ?: ['view', 'custom_list_id' => $model->custom_list_id, 'site_id' => $model->site_id]);
        }

        return $this->render('create', [
            'model' => $model,
			'back' => $back,
        ]);
    }


	/**
	 * Creates a new CustomListSite models with multi CustomLists.
	 * @param null|string $back
	 * @return mixed
	 */
	public function actionCreateMultiLists($back = null)
	{
		$model = new CustomListSiteMultiForm();

		if ($model->load(Yii::$app->request->post()) && $model->add()) {
			return $this->redirect($back ?: ['index']);
		} elseif (!$back) {
			return $this->render('create', [
				'model' => $model,
				'back' => $back,
				'multi' => true,
			]);
		} else {
			return $this->redirect($back);
		}
	}

    /**
     * Updates an existing CustomListSite model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $custom_list_id
     * @param integer $site_id
	 * @param null|string $back
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($custom_list_id, $site_id,$back = null)
    {
        $model = $this->findModel($custom_list_id, $site_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'custom_list_id' => $model->custom_list_id, 'site_id' => $model->site_id]);
        }

        return $this->render('update', [
            'model' => $model,
			'back' => $back,
        ]);
    }

    /**
     * Deletes an existing CustomListSite model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $custom_list_id
     * @param integer $site_id
	 * @param null|string $back
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($custom_list_id, $site_id,$back = null)
    {
        $this->findModel($custom_list_id, $site_id)->delete();

        return $this->redirect($back ?: ['index']);
    }

    /**
     * Finds the CustomListSite model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $custom_list_id
     * @param integer $site_id
     * @return CustomListSite the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($custom_list_id, $site_id)
    {
        if (($model = CustomListSite::findOne(['custom_list_id' => $custom_list_id, 'site_id' => $site_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
