<?php

namespace backend\controllers;

use backend\models\forms\DomainUnblockProxyMultiForm;
use Yii;
use common\models\DomainUnblockProxy;
use backend\models\search\DomainUnblockProxySearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DomainUnblockProxyController implements the CRUD actions for DomainUnblockProxy model.
 */
class DomainUnblockProxyController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DomainUnblockProxy models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DomainUnblockProxySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DomainUnblockProxy model.
     * @param integer $domain_unblock_id
     * @param integer $proxy_id
     * @return mixed
     */
    public function actionView($domain_unblock_id, $proxy_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($domain_unblock_id, $proxy_id),
        ]);
    }

    /**
     * Creates a new DomainUnblockProxy model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param null $back
     * @return mixed
     */
    public function actionCreate($back = null)
    {
        $model = new DomainUnblockProxy();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'domain_unblock_id' => $model->domain_unblock_id, 'proxy_id' => $model->proxy_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'back' => $back,
                'multi' => false,
            ]);
        }
    }

    /**
     * Creates a new DomainUnblockProxy models with multi Proxies.
     * @param null $back
     * @return mixed
     */
    public function actionCreateMultiProxies($back = null)
    {
        $siteGeo = new DomainUnblockProxyMultiForm();

        if ($siteGeo->load(Yii::$app->request->post()) && $siteGeo->add()) {
            return $this->redirect($back ?: ['index']);
        } elseif (!$back) {
            return $this->render('create', [
                'model' => $siteGeo,
                'back' => $back,
                'multi' => true,
            ]);
        } else {
            return $this->redirect($back);
        }
    }

    /**
     * Updates an existing DomainUnblockProxy model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $domain_unblock_id
     * @param integer $proxy_id
     * @param null $back
     * @return mixed
     */
    public function actionUpdate($domain_unblock_id, $proxy_id, $back = null)
    {
        $model = $this->findModel($domain_unblock_id, $proxy_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'domain_unblock_id' => $model->domain_unblock_id, 'proxy_id' => $model->proxy_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'back' => $back,
            ]);
        }
    }

    /**
     * Deletes an existing DomainUnblockProxy model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $domain_unblock_id
     * @param integer $proxy_id
     * @param null $back
     * @return mixed
     */
    public function actionDelete($domain_unblock_id, $proxy_id, $back = null)
    {
        $this->findModel($domain_unblock_id, $proxy_id)->delete();

        return $this->redirect($back ?: ['index']);
    }

    /**
     * Finds the DomainUnblockProxy model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $domain_unblock_id
     * @param integer $proxy_id
     * @return DomainUnblockProxy the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($domain_unblock_id, $proxy_id)
    {
        if (($model = DomainUnblockProxy::findOne(['domain_unblock_id' => $domain_unblock_id, 'proxy_id' => $proxy_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
