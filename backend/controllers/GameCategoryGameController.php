<?php

namespace backend\controllers;

use backend\models\forms\GameCategoryGameMultiForm;
use Yii;
use common\models\GameCategoryGame;
use backend\models\search\GameCategoryGameSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GameCategoryGameController implements the CRUD actions for GameCategoryGame model.
 */
class GameCategoryGameController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all GameCategoryGame models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GameCategoryGameSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single GameCategoryGame model.
     * @param integer $game_category_id
     * @param integer $game_id
     * @return mixed
     */
    public function actionView($game_category_id, $game_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($game_category_id, $game_id),
        ]);
    }

    /**
     * Creates a new GameCategoryGame model.
     * If creation is successful, the browser will be redirected to the 'view' page.

     * @return mixed
     */
    public function actionCreate($back = null)
    {
        $model = new GameCategoryGame();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'game_category_id' => $model->game_category_id, 'game_id' => $model->game_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
				'back' => $back,
            ]);
        }
    }


	/**
	 * Creates a new GameCategoryGame models with multi GameCategories.
	 * @param null|string $back
	 * @return mixed
	 */
	public function actionCreateMultiGameCategories($back = null)
	{
		$model = new GameCategoryGameMultiForm();

		if ($model->load(Yii::$app->request->post()) && $model->add()) {
			return $this->redirect($back ?: ['index']);
		} elseif (!$back) {
			return $this->render('create', [
				'model' => $model,
				'back' => $back,
				'multi' => true,
			]);
		} else {
			return $this->redirect($back);
		}
	}

    /**
     * Updates an existing GameCategoryGame model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $game_category_id
     * @param integer $game_id
	 * @param null|string $back
     * @return mixed
     */
    public function actionUpdate($game_category_id, $game_id, $back = null)
    {
        $model = $this->findModel($game_category_id, $game_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'game_category_id' => $model->game_category_id, 'game_id' => $model->game_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
				'back' => $back,
            ]);
        }
    }

    /**
     * Deletes an existing GameCategoryGame model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $game_category_id
     * @param integer $game_id
	 * @param null|string $back
     * @return mixed
     */
    public function actionDelete($game_category_id, $game_id,$back = null)
    {
        $this->findModel($game_category_id, $game_id)->delete();

        return $this->redirect($back ?: ['index']);
    }

    /**
     * Finds the GameCategoryGame model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $game_category_id
     * @param integer $game_id
     * @return GameCategoryGame the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($game_category_id, $game_id)
    {
        if (($model = GameCategoryGame::findOne(['game_category_id' => $game_category_id, 'game_id' => $game_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
