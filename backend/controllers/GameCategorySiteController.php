<?php

namespace backend\controllers;

use backend\models\forms\GameCategoryGameMultiForm;
use backend\models\forms\GameCategorySiteMultiForm;
use Yii;
use common\models\GameCategorySite;
use backend\models\search\GameCategorySiteSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GameCategorySiteController implements the CRUD actions for GameCategorySite model.
 */
class GameCategorySiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all GameCategorySite models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GameCategorySiteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single GameCategorySite model.
     * @param integer $game_category_id
     * @param integer $site_id
     * @return mixed
     */
    public function actionView($game_category_id, $site_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($game_category_id, $site_id),
        ]);
    }

    /**
     * Creates a new GameCategorySite model.
     * If creation is successful, the browser will be redirected to the 'view' page.
	 * @param null|string $back
     * @return mixed
     */
    public function actionCreate($back = null)
    {
        $model = new GameCategorySite();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'game_category_id' => $model->game_category_id, 'site_id' => $model->site_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
				'back' => $back,
            ]);
        }
    }

	/**
	 * Creates a new GameCategorySite models with multi GameCategory.
	 * @param null|string $back
	 * @return mixed
	 */
	public function actionCreateMultiGameCategories($back = null)
	{
		$model = new GameCategorySiteMultiForm();
		if ($model->load(Yii::$app->request->post()) && $model->add()) {
			return $this->redirect($back ?: ['index']);
		} elseif (!$back) {
			return $this->render('create', [
				'model' => $model,
				'back' => $back,
				'multi' => true,
			]);
		} else {
			return $this->redirect($back);
		}
	}

    /**
     * Updates an existing GameCategorySite model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $game_category_id
     * @param integer $site_id
	 * @param null|string $back
     * @return mixed
     */
    public function actionUpdate($game_category_id, $site_id,$back = null)
    {
        $model = $this->findModel($game_category_id, $site_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'game_category_id' => $model->game_category_id, 'site_id' => $model->site_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
				'back' => $back,
            ]);
        }
    }

    /**
     * Deletes an existing GameCategorySite model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $game_category_id
     * @param integer $site_id
	 * @param null|string $back
     * @return mixed
     */
    public function actionDelete($game_category_id, $site_id,$back = null)
    {
        $this->findModel($game_category_id, $site_id)->delete();

        return $this->redirect($back ?: ['index']);
    }

    /**
     * Finds the GameCategorySite model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $game_category_id
     * @param integer $site_id
     * @return GameCategorySite the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($game_category_id, $site_id)
    {
        if (($model = GameCategorySite::findOne(['game_category_id' => $game_category_id, 'site_id' => $site_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
