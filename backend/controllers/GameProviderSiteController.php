<?php

namespace backend\controllers;

use backend\models\forms\GameProviderSiteMultiForm;
use Yii;
use common\models\GameProviderSite;
use backend\models\search\GameProviderSiteSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GameProviderSiteController implements the CRUD actions for GameProviderSite model.
 */
class GameProviderSiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all GameProviderSite models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GameProviderSiteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single GameProviderSite model.
     * @param integer $game_provider_id
     * @param integer $site_id
     * @return mixed
     */
    public function actionView($game_provider_id, $site_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($game_provider_id, $site_id),
        ]);
    }

    /**
     * Creates a new GameProviderSite model.
     * If creation is successful, the browser will be redirected to the 'view' page.
	 * @param null|string $back
     * @return mixed
     */
    public function actionCreate($back = null)
    {
        $model = new GameProviderSite();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'game_provider_id' => $model->game_provider_id, 'site_id' => $model->site_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
				'back' => $back,
            ]);
        }
    }

	/**
	 * Creates a new GameProviderSite models with multi GameProviders.
	 * @param null|string $back
	 * @return mixed
	 */
	public function actionCreateMultiGameProviders($back = null)
	{
		$model = new GameProviderSiteMultiForm();

		if ($model->load(Yii::$app->request->post()) && $model->add()) {
			return $this->redirect($back ?: ['index']);
		} elseif (!$back) {
			return $this->render('create', [
				'model' => $model,
				'back' => $back,
				'multi' => true,
			]);
		} else {
			return $this->redirect($back);
		}
	}

    /**
     * Updates an existing GameProviderSite model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $game_provider_id
     * @param integer $site_id
	 * @param null|string $back
     * @return mixed
     */
    public function actionUpdate($game_provider_id, $site_id,$back = null)
    {
        $model = $this->findModel($game_provider_id, $site_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'game_provider_id' => $model->game_provider_id, 'site_id' => $model->site_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
				'back' => $back,
            ]);
        }
    }

    /**
     * Deletes an existing GameProviderSite model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $game_provider_id
     * @param integer $site_id
	 * @param null|string $back
     * @return mixed
     */
    public function actionDelete($game_provider_id, $site_id,$back = null)
    {
        $this->findModel($game_provider_id, $site_id)->delete();

        return $this->redirect($back ?: ['index']);
    }

    /**
     * Finds the GameProviderSite model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $game_provider_id
     * @param integer $site_id
     * @return GameProviderSite the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($game_provider_id, $site_id)
    {
        if (($model = GameProviderSite::findOne(['game_provider_id' => $game_provider_id, 'site_id' => $site_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
