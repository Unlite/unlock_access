<?php

namespace backend\controllers;

use Yii;
use common\models\LinkWildcard;
use backend\models\search\LinkWildcardSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LinkWildcardController implements the CRUD actions for LinkWildcard model.
 */
class LinkWildcardController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LinkWildcard models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LinkWildcardSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LinkWildcard model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LinkWildcard model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param null $back
     * @return mixed
     */
    public function actionCreate($back = null)
    {
        $model = new LinkWildcard();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'back' => $back
            ]);
        }
    }

    /**
     * Updates an existing LinkWildcard model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param null $back
     * @return mixed
     */
    public function actionUpdate($id, $back = null)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'back' => $back
            ]);
        }
    }

    /**
     * Deletes an existing LinkWildcard model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param null $back
     * @return mixed
     */
    public function actionDelete($id, $back = null)
    {
        $this->findModel($id)->delete();

        return $this->redirect($back ?: ['index']);
    }

    /**
     * Finds the LinkWildcard model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LinkWildcard the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LinkWildcard::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
