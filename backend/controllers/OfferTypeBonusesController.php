<?php

namespace backend\controllers;

use Yii;
use common\models\OfferTypeBonuses;
use backend\models\search\OfferTypeBonusesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OfferTypeBonusesController implements the CRUD actions for OfferTypeBonuses model.
 */
class OfferTypeBonusesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OfferTypeBonuses models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OfferTypeBonusesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OfferTypeBonuses model.
     * @param integer $offer_type_id
     * @param integer $bonuses_id
     * @return mixed
     */
    public function actionView($offer_type_id, $bonuses_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($offer_type_id, $bonuses_id),
        ]);
    }

    /**
     * Creates a new OfferTypeBonuses model.
     * If creation is successful, the browser will be redirected to the 'view' page.
	 * @param null|string $back
     * @return mixed
     */
    public function actionCreate($back = null)
    {
        $model = new OfferTypeBonuses();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'offer_type_id' => $model->offer_type_id, 'bonuses_id' => $model->bonuses_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
				'back' => $back,
            ]);
        }
    }

    /**
     * Updates an existing OfferTypeBonuses model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $offer_type_id
     * @param integer $bonuses_id
	 * @param null|string $back
     * @return mixed
     */
    public function actionUpdate($offer_type_id, $bonuses_id,$back = null)
    {
        $model = $this->findModel($offer_type_id, $bonuses_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'offer_type_id' => $model->offer_type_id, 'bonuses_id' => $model->bonuses_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
				'back' => $back,
            ]);
        }
    }

    /**
     * Deletes an existing OfferTypeBonuses model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $offer_type_id
     * @param integer $bonuses_id
	 * @param null|string $back
     * @return mixed
     */
    public function actionDelete($offer_type_id, $bonuses_id,$back = null)
    {
        $this->findModel($offer_type_id, $bonuses_id)->delete();

        return $this->redirect($back ?: ['index']);
    }

    /**
     * Finds the OfferTypeBonuses model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $offer_type_id
     * @param integer $bonuses_id
     * @return OfferTypeBonuses the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($offer_type_id, $bonuses_id)
    {
        if (($model = OfferTypeBonuses::findOne(['offer_type_id' => $offer_type_id, 'bonuses_id' => $bonuses_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
