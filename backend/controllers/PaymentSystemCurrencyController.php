<?php

namespace backend\controllers;

use backend\models\forms\PaymentSystemCurrencyMultiForm;
use Yii;
use common\models\PaymentSystemCurrency;
use backend\models\search\PaymentSystemCurrencySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PaymentSystemCurrencyController implements the CRUD actions for PaymentSystemCurrency model.
 */
class PaymentSystemCurrencyController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PaymentSystemCurrency models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PaymentSystemCurrencySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PaymentSystemCurrency model.
     * @param integer $payment_system_id
     * @param string $currency_code
     * @return mixed
     */
    public function actionView($payment_system_id, $currency_code)
    {
        return $this->render('view', [
            'model' => $this->findModel($payment_system_id, $currency_code),
        ]);
    }

    /**
     * Creates a new PaymentSystemCurrency model.
     * If creation is successful, the browser will be redirected to the 'view' page.
	 * @param null|string $back
     * @return mixed
     */
    public function actionCreate($back = null)
    {
        $model = new PaymentSystemCurrency();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'payment_system_id' => $model->payment_system_id, 'currency_code' => $model->currency_code]);
        } else {
            return $this->render('create', [
                'model' => $model,
				'back' => $back,
            ]);
        }
    }

	/**
	 * Creates a new PaymentSystemCurrency models with multi Locales.
	 * @param null|string $back
	 * @return mixed
	 */
	public function actionCreateMultiCurrencies($back = null)
	{
		$model = new PaymentSystemCurrencyMultiForm();

		if ($model->load(Yii::$app->request->post()) && $model->add()) {
			return $this->redirect($back ?: ['index']);
		} elseif (!$back) {
			return $this->render('create', [
				'model' => $model,
				'back' => $back,
				'multi' => true,
			]);
		} else {
			return $this->redirect($back);
		}
	}
    /**
     * Updates an existing PaymentSystemCurrency model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $payment_system_id
     * @param string $currency_code
	 * @param null|string $back
     * @return mixed
     */
    public function actionUpdate($payment_system_id, $currency_code,$back = null)
    {
        $model = $this->findModel($payment_system_id, $currency_code);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect( $back ?: ['view', 'payment_system_id' => $model->payment_system_id, 'currency_code' => $model->currency_code]);
        } else {
            return $this->render('update', [
                'model' => $model,
				'back' => $back,
            ]);
        }
    }

    /**
     * Deletes an existing PaymentSystemCurrency model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $payment_system_id
     * @param string $currency_code
	 * @param null|string $back
     * @return mixed
     */
    public function actionDelete($payment_system_id, $currency_code, $back = null)
    {
        $this->findModel($payment_system_id, $currency_code)->delete();

        return $this->redirect($back ?: ['index']);
    }

    /**
     * Finds the PaymentSystemCurrency model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $payment_system_id
     * @param string $currency_code
     * @return PaymentSystemCurrency the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($payment_system_id, $currency_code)
    {
        if (($model = PaymentSystemCurrency::findOne(['payment_system_id' => $payment_system_id, 'currency_code' => $currency_code])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
