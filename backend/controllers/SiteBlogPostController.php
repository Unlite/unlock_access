<?php

namespace backend\controllers;

use Yii;
use common\models\SiteBlogPost;
use backend\models\search\SiteBlogPostSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SiteBlogPostController implements the CRUD actions for SiteBlogPost model.
 */
class SiteBlogPostController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SiteBlogPost models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SiteBlogPostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SiteBlogPost model.
     * @param integer $site_id
     * @param integer $blog_post_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($site_id, $blog_post_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($site_id, $blog_post_id),
        ]);
    }

    /**
     * Creates a new SiteBlogPost model.
     * If creation is successful, the browser will be redirected to the 'view' page.
	 * @param null|string $back
     * @return mixed
     */
    public function actionCreate($back = null)
    {
        $model = new SiteBlogPost();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'site_id' => $model->site_id, 'blog_post_id' => $model->blog_post_id]);
        }

        return $this->render('create', [
            'model' => $model,
			'back' => $back,
        ]);
    }

    /**
     * Updates an existing SiteBlogPost model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $site_id
     * @param integer $blog_post_id
	 * @param null|string $back
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($site_id, $blog_post_id, $back = null)
    {
        $model = $this->findModel($site_id, $blog_post_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?:['view', 'site_id' => $model->site_id, 'blog_post_id' => $model->blog_post_id]);
        }

        return $this->render('update', [
            'model' => $model,
			'back' => $back,
        ]);
    }

    /**
     * Deletes an existing SiteBlogPost model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $site_id
     * @param integer $blog_post_id
	 * @param null|string $back
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($site_id, $blog_post_id, $back = null)
    {
        $this->findModel($site_id, $blog_post_id)->delete();

        return $this->redirect($back ?: ['index']);
    }

    /**
     * Finds the SiteBlogPost model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $site_id
     * @param integer $blog_post_id
     * @return SiteBlogPost the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($site_id, $blog_post_id)
    {
        if (($model = SiteBlogPost::findOne(['site_id' => $site_id, 'blog_post_id' => $blog_post_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
