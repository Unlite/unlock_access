<?php

namespace backend\controllers;

use backend\models\search\BonusesSearch;
use backend\models\search\CashbackSearch;
use backend\models\search\DomainSearch;
use backend\models\search\ReviewSearch;
use backend\models\search\SiteImageSearch;
use backend\models\search\SiteTranslationSearch;
use backend\models\search\TranslateSearch;
use common\components\cashback\models\Cashback;
use common\models\forms\SiteInfoForm;
use common\models\SiteInfo;
use common\models\Translate;
use Yii;
use common\models\Site;
use backend\models\search\SiteSearch;
use yii\base\Model;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SiteController implements the CRUD actions for Site model.
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Site models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SiteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Site model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Displays a single info Site model.
     * @param integer $id
     * @return mixed
     */
    public function actionTypeInfo($id)
    {
        return $this->render('type-info', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Site model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Site();
        $modelInfo = new SiteInfo();

        if ($model->load(Yii::$app->request->post()) && $modelInfo->load(Yii::$app->request->post()) && Model::validateMultiple([$model, $modelInfo])) {
            $model->save(false); // skip validation as model is already validated

            $modelInfo->id = $model->id; // no need for validation rule on user_id as you set it yourself
            $modelInfo->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'info' => $modelInfo,
            ]);
        }
    }

    /**
     * Updates an existing Site model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelInfo = SiteInfo::findOne($id);
        if(!$modelInfo){
            $modelInfo = new SiteInfo();
            $modelInfo->link('site', $model);
        }

        if ($model->load(Yii::$app->request->post()) && $modelInfo->load(Yii::$app->request->post()) && Model::validateMultiple([$model, $modelInfo])) {
            $model->save(false);
            $modelInfo->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'info' => $modelInfo,
            ]);
        }
    }

    /**
     * Deletes an existing Site model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Lists all Domains models of Site.
     * @param $id
     * @return string
     */
    public function actionDomains($id)
    {
        $model = $this->findModel($id);
        $searchModel = new DomainSearch();
        $dataProvider = $searchModel->search(ArrayHelper::merge(Yii::$app->request->queryParams, ['DomainSearch' => ['site_id' => $model->id]]));

        return $this->render('domains/index', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Translations models of Site.
     * @param $id
     * @return string
     */
    public function actionTranslations($id)
    {
        $model = $this->findModel($id);
		$translate = new Translate();

		if(isset(Yii::$app->request->post('Translate')['id']) && !empty(Yii::$app->request->post('Translate')['id'])){
			$translate = Translate::findOne(Yii::$app->request->post('Translate')['id']);
		}
		$translate->load(Yii::$app->request->post());

		if ($translate->validate()) {
			$translate->save();
		}
        return $this->render('translate/index', [
            'model' => $model,
        ]);
    }

	/**
	 * Displays a single Site model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionReviews($id)
	{
		$model = $this->findModel($id);
		$modelInfo = SiteInfoForm::findOne($id);
		if(!$modelInfo){
			$modelInfo = new SiteInfoForm();
			$modelInfo->link('site', $model);
		}

		if ($modelInfo->load(Yii::$app->request->post()) && $modelInfo->validate()) {
			$modelInfo->save();
			return $this->redirect(['reviews', 'id' => $model->id]);
		} else {
			return $this->render('reviews', [
				'model' => $model,
				'info' => $modelInfo,
			]);
		}
	}

//    /**
//     * Lists all Reviews models of Site.
//     * @param $id
//     * @return string
//     */
//    public function actionReviews($id)
//    {
//        $model = $this->findModel($id);
//        $searchModel = new ReviewSearch();
//        $dataProvider = $searchModel->search(ArrayHelper::merge(Yii::$app->request->queryParams, ['ReviewSearch' => ['site_id' => $model->id]]));
//
//        return $this->render('reviews/index', [
//            'model' => $model,
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
//        ]);
//    }

    /**
     * Lists all Bonuses models of Site.
     * @param $id
     * @return string
     */
    public function actionBonuses($id)
    {
        $model = $this->findModel($id);
        $searchModel = new BonusesSearch();
        $dataProvider = $searchModel->search(ArrayHelper::merge(Yii::$app->request->queryParams, ['BonusesSearch' => ['site_id' => $model->id]]));

        return $this->render('bonuses/index', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all SiteImages models of Site.
     * @param $id
     * @return string
     */
    public function actionImages($id)
    {
        $model = $this->findModel($id);
        $searchModel = new SiteImageSearch();
        $dataProvider = $searchModel->search(ArrayHelper::merge(Yii::$app->request->queryParams, ['SiteImageSearch' => ['site_id' => $model->id]]));

        return $this->render('images/index', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the Site model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Site the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Site::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
