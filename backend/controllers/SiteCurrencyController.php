<?php

namespace backend\controllers;

use Yii;
use common\models\SiteCurrency;
use backend\models\search\SiteCurrencySearch;
use backend\models\forms\SiteCurrencyMultiForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SiteCurrencyController implements the CRUD actions for SiteCurrency model.
 */
class SiteCurrencyController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SiteCurrency models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SiteCurrencySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SiteCurrency model.
     * @param integer $site_id
     * @param string $currency_code
     * @return mixed
     */
    public function actionView($site_id, $currency_code)
    {
        return $this->render('view', [
            'model' => $this->findModel($site_id, $currency_code),
        ]);
    }

    /**
     * Creates a new SiteCurrency model.
     * If creation is successful, the browser will be redirected to the 'view' page.
	 * @param null|string $back
     * @return mixed
     */
    public function actionCreate($back = null)
    {
        $model = new SiteCurrency();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'site_id' => $model->site_id, 'currency_code' => $model->currency_code]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

	/**
	 * Creates a new SiteCurrency models with multi Currencies.
	 * @param null|string $back
	 * @return mixed
	 */
	public function actionCreateMultiCurrencies($back = null)
	{
		$model = new SiteCurrencyMultiForm();

		if ($model->load(Yii::$app->request->post()) && $model->add()) {
			return $this->redirect($back ?: ['index']);
		} elseif (!$back) {
			return $this->render('create', [
				'model' => $model,
				'back' => $back,
				'multi' => true,
			]);
		} else {
			return $this->redirect($back);
		}
	}

    /**
     * Updates an existing SiteCurrency model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $site_id
     * @param string $currency_code
	 * @param null|string $back
     * @return mixed
     */
    public function actionUpdate($site_id, $currency_code,$back = null)
    {
        $model = $this->findModel($site_id, $currency_code);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect( $back ?: ['view', 'site_id' => $model->site_id, 'currency_code' => $model->currency_code]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SiteCurrency model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $site_id
     * @param string $currency_code
	 * @param null|string $back
     * @return mixed
     */
    public function actionDelete($site_id, $currency_code,$back = null)
    {
        $this->findModel($site_id, $currency_code)->delete();

		return $this->redirect($back ?: ['index']);
    }

    /**
     * Finds the SiteCurrency model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $site_id
     * @param string $currency_code
     * @return SiteCurrency the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($site_id, $currency_code)
    {
        if (($model = SiteCurrency::findOne(['site_id' => $site_id, 'currency_code' => $currency_code])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
