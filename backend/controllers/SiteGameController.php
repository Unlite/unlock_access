<?php

namespace backend\controllers;

use backend\models\forms\SiteGameMultiForm;
use Yii;
use common\models\SiteGame;
use backend\models\search\SiteGameSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SiteGameController implements the CRUD actions for SiteGame model.
 */
class SiteGameController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SiteGame models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SiteGameSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SiteGame model.
     * @param integer $site_id
     * @param integer $game_id
     * @return mixed
     */
    public function actionView($site_id, $game_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($site_id, $game_id),
        ]);
    }

    /**
     * Creates a new SiteGame model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param null|string $back
     * @return mixed
     */
    public function actionCreate($back = null)
    {
        $model = new SiteGame();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'site_id' => $model->site_id, 'game_id' => $model->game_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'back' => $back,
                'multi' => false,
            ]);
        }
    }

    /**
     * Creates a new SiteGame models with multi Games.
     * @param null|string $back
     * @return mixed
     */
    public function actionCreateMultiGames($back = null)
    {
        $model = new SiteGameMultiForm();

        if ($model->load(Yii::$app->request->post()) && $model->add()) {
            return $this->redirect($back ?: ['index']);
        } elseif (!$back) {
            return $this->render('create', [
                'model' => $model,
                'back' => $back,
                'multi' => true,
            ]);
        } else {
            return $this->redirect($back);
        }
    }

    /**
     * Updates an existing SiteGame model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $site_id
     * @param integer $game_id
     * @param null|string $back
     * @return mixed
     */
    public function actionUpdate($site_id, $game_id, $back = null)
    {
        $model = $this->findModel($site_id, $game_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'site_id' => $model->site_id, 'game_id' => $model->game_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'back' => $back,
            ]);
        }
    }

    /**
     * Deletes an existing SiteGame model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $site_id
     * @param integer $game_id
     * @param null|string $back
     * @return mixed
     */
    public function actionDelete($site_id, $game_id, $back = null)
    {
        $this->findModel($site_id, $game_id)->delete();

        return $this->redirect($back ?: ['index']);
    }

    /**
     * Finds the SiteGame model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $site_id
     * @param integer $game_id
     * @return SiteGame the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($site_id, $game_id)
    {
        if (($model = SiteGame::findOne(['site_id' => $site_id, 'game_id' => $game_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
