<?php

namespace backend\controllers;

use backend\models\forms\SiteGeoMultiForm;
use Yii;
use common\models\SiteGeo;
use backend\models\search\SiteGeoSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SiteGeoController implements the CRUD actions for SiteGeo model.
 */
class SiteGeoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SiteGeo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SiteGeoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SiteGeo model.
     * @param integer $site_id
     * @param string $geo_code
     * @return mixed
     */
    public function actionView($site_id, $geo_code)
    {
        return $this->render('view', [
            'model' => $this->findModel($site_id, $geo_code),
        ]);
    }

    /**
     * Creates a new SiteGeo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param null $back
     * @return mixed
     */
    public function actionCreate($back = null)
    {
        $model = new SiteGeo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'site_id' => $model->site_id, 'geo_code' => $model->geo_code]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'back' => $back,
                'multi' => false,
            ]);
        }
    }

    /**
     * Creates a new SiteGeo models with multi Geo.
     * @param null $back
     * @return mixed
     */
    public function actionCreateMultiGeo($back = null)
    {
        $siteGeo = new SiteGeoMultiForm();

        if ($siteGeo->load(Yii::$app->request->post()) && $siteGeo->add()) {
            return $this->redirect($back ?: ['index']);
        }elseif (!$back){
            return $this->render('create', [
                'model' => $siteGeo,
                'back' => $back,
                'multi' => true,
            ]);
        }else{
            return $this->redirect($back);
        }
    }

    /**
     * Updates an existing SiteGeo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $site_id
     * @param string $geo_code
     * @param null $back
     * @return mixed
     */
    public function actionUpdate($site_id, $geo_code, $back = null)
    {
        $model = $this->findModel($site_id, $geo_code);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'site_id' => $model->site_id, 'geo_code' => $model->geo_code]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'back' => $back,
            ]);
        }
    }

    /**
     * Deletes an existing SiteGeo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $site_id
     * @param string $geo_code
     * @param null $back
     * @return mixed
     */
    public function actionDelete($site_id, $geo_code, $back = null)
    {
        $this->findModel($site_id, $geo_code)->delete();

        return $this->redirect($back ?: ['index']);
    }

    /**
     * Finds the SiteGeo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $site_id
     * @param string $geo_code
     * @return SiteGeo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($site_id, $geo_code)
    {
        if (($model = SiteGeo::findOne(['site_id' => $site_id, 'geo_code' => $geo_code])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
