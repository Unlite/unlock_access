<?php

namespace backend\controllers;

use Yii;
use common\models\SiteImage;
use backend\models\search\SiteImageSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SiteImageController implements the CRUD actions for SiteImage model.
 */
class SiteImageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SiteImage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SiteImageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SiteImage model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SiteImage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param null|string $back
     * @return mixed
     */
    public function actionCreate($back = null)
    {
        $model = new SiteImage();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'back' => $back,
            ]);
        }
    }

    /**
     * Updates an existing SiteImage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param null|string $back
     * @return mixed
     */
    public function actionUpdate($id, $back = null)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'back' => $back,
            ]);
        }
    }

    /**
     * Deletes an existing SiteImage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param null|string $back
     * @return mixed
     */
    public function actionDelete($id, $back = null)
    {
        $this->findModel($id)->delete();

        return $this->redirect($back ?: ['index']);
    }

    /**
     * Finds the SiteImage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SiteImage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SiteImage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
