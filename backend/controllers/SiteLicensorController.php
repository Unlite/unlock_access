<?php

namespace backend\controllers;

use backend\models\forms\SiteLicensorMultiForm;
use Yii;
use common\models\SiteLicensor;
use backend\models\search\SiteLicensorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SiteLicensorController implements the CRUD actions for SiteLicensor model.
 */
class SiteLicensorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SiteLicensor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SiteLicensorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SiteLicensor model.
     * @param integer $site_id
     * @param integer $licensor_id
     * @return mixed
     */
    public function actionView($site_id, $licensor_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($site_id, $licensor_id),
        ]);
    }

    /**
     * Creates a new SiteLicensor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
	 * @param null|string $back
     * @return mixed
     */
    public function actionCreate($back = null)
    {
        $model = new SiteLicensor();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'site_id' => $model->site_id, 'licensor_id' => $model->licensor_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
				'back' => $back,
				'multi' => false,
            ]);
        }
    }

	/**
	 * Creates a new SiteLicensor models with multi Licensors.
	 * @param null|string $back
	 * @return mixed
	 */
	public function actionCreateMultiLicensors($back = null)
	{
		$model = new SiteLicensorMultiForm();

		if ($model->load(Yii::$app->request->post()) && $model->add()) {
			return $this->redirect($back ?: ['index']);
		} elseif (!$back) {
			return $this->render('create', [
				'model' => $model,
				'back' => $back,
				'multi' => true,
			]);
		} else {
			return $this->redirect($back);
		}
	}

    /**
     * Updates an existing SiteLicensor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $site_id
	 * @param null|string $back
     * @param integer $licensor_id
     * @return mixed
     */
    public function actionUpdate($site_id, $licensor_id, $back = null)
    {
        $model = $this->findModel($site_id, $licensor_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'site_id' => $model->site_id, 'licensor_id' => $model->licensor_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SiteLicensor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $site_id
     * @param integer $licensor_id
	 * @param null|string $back
     * @return mixed
     */
    public function actionDelete($site_id, $licensor_id, $back = null)
    {
        $this->findModel($site_id, $licensor_id)->delete();

        return $this->redirect($back ?: ['index']);
    }

    /**
     * Finds the SiteLicensor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $site_id
     * @param integer $licensor_id
     * @return SiteLicensor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($site_id, $licensor_id)
    {
        if (($model = SiteLicensor::findOne(['site_id' => $site_id, 'licensor_id' => $licensor_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
