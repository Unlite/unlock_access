<?php

namespace backend\controllers;

use backend\models\forms\SiteLocaleMultiForm;
use Yii;
use common\models\SiteLocale;
use backend\models\search\SiteLocaleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SiteLocaleController implements the CRUD actions for SiteLocale model.
 */
class SiteLocaleController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SiteLocale models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SiteLocaleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SiteLocale model.
     * @param integer $site_id
     * @param string $locale_code
     * @return mixed
     */
    public function actionView($site_id, $locale_code)
    {
        return $this->render('view', [
            'model' => $this->findModel($site_id, $locale_code),
        ]);
    }

    /**
     * Creates a new SiteLocale model.
     * If creation is successful, the browser will be redirected to the 'view' page.
	 * @param null|string $back
     * @return mixed
     */
    public function actionCreate($back = null)
    {
        $model = new SiteLocale();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'site_id' => $model->site_id, 'locale_code' => $model->locale_code]);
        } else {
            return $this->render('create', [
                'model' => $model,
				'back' => $back,
            ]);
        }
    }

	/**
	 * Creates a new SiteLocale models with multi Locales.
	 * @param null|string $back
	 * @return mixed
	 */
	public function actionCreateMultiLocales($back = null)
	{
		$model = new SiteLocaleMultiForm();

		if ($model->load(Yii::$app->request->post()) && $model->add()) {
			return $this->redirect($back ?: ['index']);
		} elseif (!$back) {
			return $this->render('create', [
				'model' => $model,
				'back' => $back,
				'multi' => true,
			]);
		} else {
			return $this->redirect($back);
		}
	}

    /**
     * Updates an existing SiteLocale model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $site_id
     * @param string $locale_code
	 * @param null|string $back
     * @return mixed
     */
    public function actionUpdate($site_id, $locale_code,$back = null)
    {
        $model = $this->findModel($site_id, $locale_code);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect( $back ?: ['view', 'site_id' => $model->site_id, 'locale_code' => $model->locale_code]);
        } else {
            return $this->render('update', [
                'model' => $model,
				'back' => $back,
            ]);
        }
    }

    /**
     * Deletes an existing SiteLocale model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $site_id
     * @param string $locale_code
	 * @param null|string $back
     * @return mixed
     */
    public function actionDelete($site_id, $locale_code,$back = null)
    {
        $this->findModel($site_id, $locale_code)->delete();

        return $this->redirect($back ?: ['index']);
    }

    /**
     * Finds the SiteLocale model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $site_id
     * @param string $locale_code
     * @return SiteLocale the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($site_id, $locale_code)
    {
        if (($model = SiteLocale::findOne(['site_id' => $site_id, 'locale_code' => $locale_code])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
