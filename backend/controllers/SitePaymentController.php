<?php

namespace backend\controllers;

use backend\models\forms\SitePaymentMultiForm;
use Yii;
use common\models\SitePayment;
use backend\models\search\SitePaymentSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SitePaymentController implements the CRUD actions for SitePayment model.
 */
class SitePaymentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SitePayment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SitePaymentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SitePayment model.
     * @param integer $site_id
     * @param integer $payment_id
     * @return mixed
     */
    public function actionView($site_id, $payment_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($site_id, $payment_id),
        ]);
    }

    /**
     * Creates a new SitePayment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param null|string $back
     * @return mixed
     */
    public function actionCreate($back = null)
    {
        $model = new SitePayment();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'site_id' => $model->site_id, 'payment_id' => $model->payment_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'back' => $back,
                'multi' => false,
            ]);
        }
    }

    /**
     * Creates a new SitePayment models with multi Payments.
     * @param null|string $back
     * @return mixed
     */
    public function actionCreateMultiPayments($back = null)
    {
        $model = new SitePaymentMultiForm();

        if ($model->load(Yii::$app->request->post()) && $model->add()) {
            return $this->redirect($back ?: ['index']);
        } elseif (!$back) {
            return $this->render('create', [
                'model' => $model,
                'back' => $back,
                'multi' => true,
            ]);
        } else {
            return $this->redirect($back);
        }
    }

    /**
     * Updates an existing SitePayment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $site_id
     * @param integer $payment_id
     * @param null|string $back
     * @return mixed
     */
    public function actionUpdate($site_id, $payment_id, $back = null)
    {
        $model = $this->findModel($site_id, $payment_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'site_id' => $model->site_id, 'payment_id' => $model->payment_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
				'back' => $back
            ]);
        }
    }

    /**
     * Deletes an existing SitePayment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $site_id
     * @param integer $payment_id
     * @param null|string $back
     * @return mixed
     */
    public function actionDelete($site_id, $payment_id, $back = null)
    {
        $this->findModel($site_id, $payment_id)->delete();

        return $this->redirect($back ?: ['index']);
    }

    /**
     * Finds the SitePayment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $site_id
     * @param integer $payment_id
     * @return SitePayment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($site_id, $payment_id)
    {
        if (($model = SitePayment::findOne(['site_id' => $site_id, 'payment_id' => $payment_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
