<?php

namespace backend\controllers;

use backend\models\forms\SiteRelationsMultiForm;
use Yii;
use common\models\SiteRelations;
use backend\models\search\SiteRelationsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SiteRelationsController implements the CRUD actions for SiteRelations model.
 */
class SiteRelationsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SiteRelations models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SiteRelationsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SiteRelations model.
     * @param integer $site_id_l
     * @param integer $site_id_h
     * @return mixed
     */
    public function actionView($site_id_l, $site_id_h)
    {
        return $this->render('view', [
            'model' => $this->findModel($site_id_l, $site_id_h),
        ]);
    }

    /**
     * Creates a new SiteRelations model.
     * If creation is successful, the browser will be redirected to the 'view' page.
	 * @param null|string $back
     * @return mixed
     */
    public function actionCreate($back = null)
    {
        $model = new SiteRelations();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect( $back ?: ['view', 'site_id_l' => $model->site_id_l, 'site_id_h' => $model->site_id_h]);
        } else {
            return $this->render('create', [
				'model' => $model,
				'back' => $back,
				'multi' => false,
            ]);
        }
    }


	/**
	 * Creates a new SiteRelations models with multi Sites.
	 * @param null|string $back
	 * @return mixed
	 */
	public function actionCreateMultiRelations($back = null)
	{
		$model = new SiteRelationsMultiForm();

		if ($model->load(Yii::$app->request->post()) && $model->add()) {
			return $this->redirect($back ?: ['index']);
		} elseif (!$back) {
			return $this->render('create', [
				'model' => $model,
				'back' => $back,
				'multi' => true,
			]);
		} else {
			return $this->redirect($back);
		}
	}

    /**
     * Updates an existing SiteRelations model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $site_id_l
     * @param integer $site_id_h
	 * @param null|string $back
     * @return mixed
     */
    public function actionUpdate($site_id_l, $site_id_h, $back = null)
    {
        $model = $this->findModel($site_id_l, $site_id_h);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'site_id_l' => $model->site_id_l, 'site_id_h' => $model->site_id_h]);
        } else {
            return $this->render('update', [
				'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SiteRelations model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $site_id_l
     * @param integer $site_id_h
     * @return mixed
     */
    public function actionDelete($site_id_l, $site_id_h,$back = null)
    {
        $this->findModel($site_id_l, $site_id_h)->delete();

        return $this->redirect( $back ?: ['index']);
    }

    /**
     * Finds the SiteRelations model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $site_id_l
     * @param integer $site_id_h
     * @return SiteRelations the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($site_id_l, $site_id_h)
    {
        if (($model = SiteRelations::findOne(['site_id_l' => $site_id_l, 'site_id_h' => $site_id_h])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
