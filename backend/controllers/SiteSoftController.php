<?php

namespace backend\controllers;

use backend\models\forms\SiteSoftMultiForm;
use Yii;
use common\models\SiteSoft;
use backend\models\search\SiteSoftSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SiteSoftController implements the CRUD actions for SiteSoft model.
 */
class SiteSoftController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SiteSoft models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SiteSoftSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SiteSoft model.
     * @param integer $site_id
     * @param integer $soft_id
     * @return mixed
     */
    public function actionView($site_id, $soft_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($site_id, $soft_id),
        ]);
    }

    /**
     * Creates a new SiteSoft model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param null|string $back
     * @return mixed
     */
    public function actionCreate($back = null)
    {
        $model = new SiteSoft();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'site_id' => $model->site_id, 'soft_id' => $model->soft_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'back' => $back,
                'multi' => false,
            ]);
        }
    }

    /**
     * Creates a new SiteSoft models with multi Soft.
     * @param null|string $back
     * @return mixed
     */
    public function actionCreateMultiSoft($back = null)
    {
        $model = new SiteSoftMultiForm();

        if ($model->load(Yii::$app->request->post()) && $model->add()) {
            return $this->redirect($back ?: ['index']);
        } elseif (!$back) {
            return $this->render('create', [
                'model' => $model,
                'back' => $back,
                'multi' => true,
            ]);
        } else {
            return $this->redirect($back);
        }
    }

    /**
     * Updates an existing SiteSoft model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $site_id
     * @param integer $soft_id
     * @param null|string $back
     * @return mixed
     */
    public function actionUpdate($site_id, $soft_id, $back = null)
    {
        $model = $this->findModel($site_id, $soft_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'site_id' => $model->site_id, 'soft_id' => $model->soft_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'back' => $back,
            ]);
        }
    }

    /**
     * Deletes an existing SiteSoft model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $site_id
     * @param integer $soft_id
     * @param null|string $back
     * @return mixed
     */
    public function actionDelete($site_id, $soft_id, $back = null)
    {
        $this->findModel($site_id, $soft_id)->delete();

        return $this->redirect($back ?: ['index']);
    }

    /**
     * Finds the SiteSoft model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $site_id
     * @param integer $soft_id
     * @return SiteSoft the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($site_id, $soft_id)
    {
        if (($model = SiteSoft::findOne(['site_id' => $site_id, 'soft_id' => $soft_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
