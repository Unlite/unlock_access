<?php

namespace backend\controllers;

use Yii;
use common\models\SiteTranslation;
use backend\models\search\SiteTranslationSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SiteTranslationController implements the CRUD actions for SiteTranslation model.
 */
class SiteTranslationController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SiteTranslation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SiteTranslationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SiteTranslation model.
     * @param integer $site_id
     * @param string $geo_code
     * @return mixed
     */
    public function actionView($site_id, $geo_code)
    {
        return $this->render('view', [
            'model' => $this->findModel($site_id, $geo_code),
        ]);
    }

    /**
     * Creates a new SiteTranslation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param null $back
     * @return mixed
     */
    public function actionCreate($back = null)
    {
        $model = new SiteTranslation();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'site_id' => $model->site_id, 'geo_code' => $model->geo_code]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'back' => $back
            ]);
        }
    }

    /**
     * Updates an existing SiteTranslation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $site_id
     * @param string $geo_code
     * @param null $back
     * @return mixed
     */
    public function actionUpdate($site_id, $geo_code, $back = null)
    {
        $model = $this->findModel($site_id, $geo_code);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'site_id' => $model->site_id, 'geo_code' => $model->geo_code]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'back' => $back
            ]);
        }
    }

    /**
     * Deletes an existing SiteTranslation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $site_id
     * @param string $geo_code
     * @param null $back
     * @return mixed
     */
    public function actionDelete($site_id, $geo_code, $back = null)
    {
        $this->findModel($site_id, $geo_code)->delete();

        return $this->redirect($back ?: ['index']);
    }

    /**
     * Finds the SiteTranslation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $site_id
     * @param string $geo_code
     * @return SiteTranslation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($site_id, $geo_code)
    {
        if (($model = SiteTranslation::findOne(['site_id' => $site_id, 'geo_code' => $geo_code])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
