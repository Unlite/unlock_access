<?php

namespace backend\controllers;

use backend\models\forms\TagBlogPostMultiForm;
use Yii;
use common\models\TagBlogPost;
use backend\models\search\TagBlogPostSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TagBlogPostController implements the CRUD actions for TagBlogPost model.
 */
class TagBlogPostController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TagBlogPost models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TagBlogPostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TagBlogPost model.
     * @param integer $tag_id
     * @param integer $blog_post_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($tag_id, $blog_post_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($tag_id, $blog_post_id),
        ]);
    }

    /**
     * Creates a new TagBlogPost model.
     * If creation is successful, the browser will be redirected to the 'view' page.
	 * @param null|string $back
     * @return mixed
     */
    public function actionCreate($back = null)
    {
        $model = new TagBlogPost();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'tag_id' => $model->tag_id, 'blog_post_id' => $model->blog_post_id]);
        }

        return $this->render('create', [
            'model' => $model,
			'back' => $back,
        ]);
    }

	/**
	 * Creates a new TagBlogPost models with multi Tag.
	 * @param null|string $back
	 * @return mixed
	 */
	public function actionCreateMultiBlogPostTags($back = null)
	{
		$model = new TagBlogPostMultiForm();

		if ($model->load(Yii::$app->request->post()) && $model->add()) {
			return $this->redirect($back ?: ['index']);
		} elseif (!$back) {
			return $this->render('create', [
				'model' => $model,
				'back' => $back,
				'multi' => true,
			]);
		} else {
			return $this->redirect($back);
		}
	}

	/**
     * Updates an existing TagBlogPost model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $tag_id
     * @param integer $blog_post_id
	 * @param null|string $back
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($tag_id, $blog_post_id,$back = null)
    {
        $model = $this->findModel($tag_id, $blog_post_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($back ?: ['view', 'tag_id' => $model->tag_id, 'blog_post_id' => $model->blog_post_id]);
        }

        return $this->render('update', [
            'model' => $model,
			'back' => $back,
        ]);
    }

    /**
     * Deletes an existing TagBlogPost model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $tag_id
     * @param integer $blog_post_id
	 * @param null|string $back
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($tag_id, $blog_post_id,$back = null)
    {
        $this->findModel($tag_id, $blog_post_id)->delete();

        return $this->redirect($back ?: ['index']);
    }

    /**
     * Finds the TagBlogPost model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $tag_id
     * @param integer $blog_post_id
     * @return TagBlogPost the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($tag_id, $blog_post_id)
    {
        if (($model = TagBlogPost::findOne(['tag_id' => $tag_id, 'blog_post_id' => $blog_post_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
