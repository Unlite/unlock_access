<?php

namespace backend\helpers;


class AccessHelper
{
    public static function routeAccess($route)
    {
        if (\Yii::$app->user->can($route)) return true;
        if (\Yii::$app->user->can('/*')) return true;
        $r = explode('/', $route);


        while ($r) {
            if (\Yii::$app->user->can('/' . ltrim(join('/', $r) . '/*', '/'))) return true;
            array_pop($r);
        }

        return false;
    }

    public static function routeAccessByUser($route, $user_id)
    {
        if (\Yii::$app->authManager->checkAccess($user_id, $route)) return true;
        if (\Yii::$app->authManager->checkAccess($user_id, '/*')) return true;
        $r = explode('/', $route);


        while ($r) {
            if (\Yii::$app->authManager->checkAccess($user_id, '/' . ltrim(join('/', $r) . '/*', '/'))) return true;
            array_pop($r);
        }

        return false;
    }
}