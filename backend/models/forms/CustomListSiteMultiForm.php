<?php

namespace backend\models\forms;

use common\models\Site;
use common\models\CustomList;
use common\models\CustomListSite;
use yii\base\Model;

class CustomListSiteMultiForm extends Model
{
    public $site_id;
    public $custom_list_ids;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id', 'custom_list_ids'], 'required'],
            [['site_id'], 'integer'],
            [['site_id'], 'exist', 'skipOnError' => false, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'id']],
            ['custom_list_ids', 'each', 'rule' => ['exist', 'targetClass' => CustomList::className(), 'targetAttribute' => 'id']],
        ];
    }

    public function add()
    {
        if (!$this->validate()) {
            \Yii::$app->session->addFlash('error', "Invalid data");
            return null;
        }
        $success_count = 0;

        foreach ($this->custom_list_ids as $custom_list_id) {
            $customListSite = new CustomListSite();
            $customListSite->site_id = $this->site_id;
            $customListSite->custom_list_id = $custom_list_id;
            if (!$customListSite->save()) {
                \Yii::$app->session->addFlash('danger', "Custom list {$custom_list_id} already added");
            } else {
                $success_count++;
            }
        }
        if ($success_count) \Yii::$app->session->addFlash('success', "{$success_count} custom lists added");
        return true;
    }
}