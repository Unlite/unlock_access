<?php

namespace backend\models\forms;

use common\models\Domain;
use common\models\DomainProxy;
use common\models\Proxy;
use yii\base\Model;

class DomainProxyMultiForm extends Model
{
    public $domain_id;
    public $proxy_ids;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['domain_id', 'proxy_ids'], 'required'],
            [['domain_id'], 'integer'],
            ['proxy_ids', 'each', 'rule' => ['integer']],

            [['domain_id'], 'exist', 'skipOnError' => false, 'targetClass' => Domain::className(), 'targetAttribute' => ['domain_id' => 'id']],
            ['proxy_ids', 'each', 'rule' => ['exist', 'targetClass' => Proxy::className(), 'targetAttribute' => 'id']],
        ];
    }

    public function add()
    {
        if (!$this->validate()) {
            \Yii::$app->session->addFlash('error', "Invalid data");
            return null;
        }
        $success_count = 0;

        foreach ($this->proxy_ids as $proxy_id) {
            $siteGeo = new DomainProxy();
            $siteGeo->domain_id = $this->domain_id;
            $siteGeo->proxy_id = $proxy_id;
            if (!$siteGeo->save()) {
                \Yii::$app->session->addFlash('danger', "Proxy {$proxy_id} already added");
            } else {
                $success_count++;
            }
        }
        if ($success_count) \Yii::$app->session->addFlash('success', "{$success_count} proxies added");
        return true;
    }
}