<?php

namespace backend\models\forms;

use common\models\DomainUnblock;
use common\models\DomainUnblockProxy;
use common\models\Proxy;
use common\models\SiteGeo;
use yii\base\Model;

class DomainUnblockProxyMultiForm extends Model
{
    public $domain_unblock_id;
    public $proxy_ids;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['domain_unblock_id', 'proxy_ids'], 'required'],
            [['domain_unblock_id'], 'integer'],
            ['proxy_ids', 'each', 'rule' => ['integer']],

            [['domain_unblock_id'], 'exist', 'skipOnError' => false, 'targetClass' => DomainUnblock::className(), 'targetAttribute' => ['domain_unblock_id' => 'id']],
            ['proxy_ids', 'each', 'rule' => ['exist', 'targetClass' => Proxy::className(), 'targetAttribute' => 'id']],
        ];
    }

    public function add()
    {
        if (!$this->validate()) {
            \Yii::$app->session->addFlash('error', "Invalid data");
            return null;
        }
        $success_count = 0;

        foreach ($this->proxy_ids as $proxy_id) {
            $siteGeo = new DomainUnblockProxy();
            $siteGeo->domain_unblock_id = $this->domain_unblock_id;
            $siteGeo->proxy_id = $proxy_id;
            if (!$siteGeo->save()) {
                \Yii::$app->session->addFlash('danger', "Proxy {$proxy_id} already added");
            } else {
                $success_count++;
            }
        }
        if ($success_count) \Yii::$app->session->addFlash('success', "{$success_count} proxies added");
        return true;
    }
}