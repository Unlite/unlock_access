<?php

namespace backend\models\forms;

use common\models\Game;
use common\models\GameCategory;
use common\models\GameCategoryGame;
use yii\base\Model;

class GameCategoryGameMultiForm extends Model
{
    public $game_id;
    public $game_category_ids;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['game_id', 'game_category_ids'], 'required'],
            [['game_id'], 'integer'],
            [['game_id'], 'exist', 'skipOnError' => false, 'targetClass' => Game::className(), 'targetAttribute' => ['game_id' => 'id']],
            ['game_category_ids', 'each', 'rule' => ['exist', 'targetClass' => GameCategory::className(), 'targetAttribute' => 'id']],
        ];
    }

    public function add()
    {
        if (!$this->validate()) {
            \Yii::$app->session->addFlash('error', "Invalid data");
            return null;
        }
        $success_count = 0;

        foreach ($this->game_category_ids as $game_category_id) {
            $gameCategoryGame = new GameCategoryGame();
            $gameCategoryGame->game_id = $this->game_id;
            $gameCategoryGame->game_category_id = $game_category_id;
            if (!$gameCategoryGame->save()) {
                \Yii::$app->session->addFlash('danger', "Category {$game_category_id} already added");
            } else {
                $success_count++;
            }
        }
        if ($success_count) \Yii::$app->session->addFlash('success', "{$success_count} Categories added");
        return true;
    }
}