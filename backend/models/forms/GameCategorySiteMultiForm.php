<?php

namespace backend\models\forms;

use common\models\GameCategory;
use common\models\GameCategorySite;
use common\models\Site;
use yii\base\Model;

class GameCategorySiteMultiForm extends Model
{
    public $site_id;
    public $game_category_ids;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id', 'game_category_ids'], 'required'],
            [['site_id'], 'integer'],
            [['site_id'], 'exist', 'skipOnError' => false, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'id']],
            ['game_category_ids', 'each', 'rule' => ['exist', 'targetClass' => GameCategory::className(), 'targetAttribute' => 'id']],
        ];
    }

    public function add()
    {
        if (!$this->validate()) {
            \Yii::$app->session->addFlash('error', "Invalid data");
            return null;
        }
        $success_count = 0;

        foreach ($this->game_category_ids as $game_category_id) {
            $gameCategoryGame = new GameCategorySite();
            $gameCategoryGame->site_id = $this->site_id;
            $gameCategoryGame->game_category_id = $game_category_id;
            $gameCategoryGame->is_auto_created = 0;
            if (!$gameCategoryGame->save()) {
                \Yii::$app->session->addFlash('danger', "Category {$game_category_id} already added");
            } else {
                $success_count++;
            }
        }
        if ($success_count) \Yii::$app->session->addFlash('success', "{$success_count} Categories added");
        return true;
    }
}