<?php

namespace backend\models\forms;

use common\models\GameProvider;
use common\models\GameProviderSite;
use common\models\Site;
use yii\base\Model;

class GameProviderSiteMultiForm extends Model
{
    public $site_id;
    public $game_provider_ids;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id', 'game_provider_ids'], 'required'],
            [['site_id'], 'integer'],
            [['site_id'], 'exist', 'skipOnError' => false, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'id']],
            ['game_provider_ids', 'each', 'rule' => ['exist', 'targetClass' => GameProvider::className(), 'targetAttribute' => 'id']],
        ];
    }

    public function add()
    {
        if (!$this->validate()) {
            \Yii::$app->session->addFlash('error', "Invalid data");
            return null;
        }
        $success_count = 0;

        foreach ($this->game_provider_ids as $game_provider_id) {
            $gameCategoryGame = new GameProviderSite();
            $gameCategoryGame->site_id = $this->site_id;
            $gameCategoryGame->game_provider_id = $game_provider_id;
            $gameCategoryGame->is_auto_created = 0;
            if (!$gameCategoryGame->save()) {
                \Yii::$app->session->addFlash('danger', "Provider {$game_provider_id} already added");
            } else {
                $success_count++;
            }
        }
        if ($success_count) \Yii::$app->session->addFlash('success', "{$success_count} Providers added");
        return true;
    }
}