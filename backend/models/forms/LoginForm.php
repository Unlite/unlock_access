<?php
namespace backend\models\forms;

use backend\helpers\AccessHelper;
use yii\helpers\ArrayHelper;

/**
 * Login form
 */
class LoginForm extends \common\models\forms\LoginForm
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [['username', 'validateRules']]);
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateRules($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !AccessHelper::routeAccessByUser('/main/index', $user->id)) {
                $this->addError($attribute, "You don't have permissions.");
            }
        }
    }
}
