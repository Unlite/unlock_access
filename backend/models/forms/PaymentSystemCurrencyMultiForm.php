<?php

namespace backend\models\forms;

use common\models\Currency;
use common\models\PaymentSystem;
use common\models\PaymentSystemCurrency;
use yii\base\Model;

class PaymentSystemCurrencyMultiForm extends Model
{
    public $payment_system_id;
    public $currency_codes;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_system_id', 'currency_codes'], 'required'],
            [['payment_system_id'], 'integer'],
            [['payment_system_id'], 'exist', 'skipOnError' => false, 'targetClass' => PaymentSystem::className(), 'targetAttribute' => ['payment_system_id' => 'id']],
            ['currency_codes', 'each', 'rule' => ['exist', 'targetClass' => Currency::className(), 'targetAttribute' => 'code']],
        ];
    }

    public function add()
    {
        if (!$this->validate()) {
            \Yii::$app->session->addFlash('error', "Invalid data");
            return null;
        }
        $success_count = 0;

        foreach ($this->currency_codes as $currency_code) {
            $paymentSystemCurrency = new PaymentSystemCurrency();
            $paymentSystemCurrency->payment_system_id = $this->payment_system_id;
            $paymentSystemCurrency->currency_code = $currency_code;
            if (!$paymentSystemCurrency->save()) {
                \Yii::$app->session->addFlash('danger', "Locale {$currency_code} already added");
            } else {
                $success_count++;
            }
        }
        if ($success_count) \Yii::$app->session->addFlash('success', "{$success_count} locales added");
        return true;
    }
}