<?php

namespace backend\models\forms;

use common\models\Domain;
use common\models\DomainProxy;
use common\models\Proxy;
use common\models\Settings;
use yii\base\Model;

class SettingsMultiForm extends Model
{
    public $settings;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['settings'], 'required'],
            [['settings'], 'objectArray'],
        ];
    }

	public function objectArray($attribute)
	{
		if(is_array($this->{$attribute})){
			foreach ($this->{$attribute} as $key => $value){
				if(!$value instanceof Settings){
					$this->addError('settings','Invalid data for settings Settings');
				}
			}
		} else {
			$this->addError('settings','Invalid data for settings Settings');
		}
    }

    public function save()
    {
        if (!$this->validate()) {
            \Yii::$app->session->addFlash('error', "Invalid data");
            return null;
        }
        /* @var $setting Settings */
        foreach ($this->settings as $setting) {
			if(!$setting->validate() || !$setting->save()){
				var_dump($setting->getErrors());
				$this->addError('settings' ,'Invalid data');
				return false;
			}
        }
        return true;
    }
}