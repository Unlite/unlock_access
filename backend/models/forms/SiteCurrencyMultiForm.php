<?php

namespace backend\models\forms;

use common\models\Currency;
use common\models\Site;
use common\models\SiteCurrency;
use yii\base\Model;

class SiteCurrencyMultiForm extends Model
{
    public $site_id;
    public $currency_codes;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id', 'currency_codes'], 'required'],
            [['site_id'], 'integer'],
            [['site_id'], 'exist', 'skipOnError' => false, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'id']],
            ['currency_codes', 'each', 'rule' => ['exist', 'targetClass' => Currency::className(), 'targetAttribute' => 'code']],
        ];
    }

    public function add()
    {
        if (!$this->validate()) {
            \Yii::$app->session->addFlash('error', "Invalid data");
            return null;
        }
        $success_count = 0;

        foreach ($this->currency_codes as $currency_code) {
            $siteCurrency = new SiteCurrency();
            $siteCurrency->site_id = $this->site_id;
            $siteCurrency->currency_code = $currency_code;
            if (!$siteCurrency->save()) {
                \Yii::$app->session->addFlash('danger', "Currency {$currency_code} already added");
            } else {
                $success_count++;
            }
        }
        if ($success_count) \Yii::$app->session->addFlash('success', "{$success_count} currencies added");
        return true;
    }
}