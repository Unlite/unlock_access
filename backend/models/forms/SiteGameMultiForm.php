<?php

namespace backend\models\forms;

use common\models\Site;
use common\models\SiteGame;
use common\models\Game;
use yii\base\Model;

class SiteGameMultiForm extends Model
{
    public $site_id;
    public $game_ids;
    public $is_promoted;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id', 'game_ids', 'is_promoted'], 'required'],
            [['site_id','is_promoted'], 'integer'],
            ['game_ids', 'each', 'rule' => ['integer']],

            [['site_id'], 'exist', 'skipOnError' => false, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'id']],
            ['game_ids', 'each', 'rule' => ['exist', 'targetClass' => Game::className(), 'targetAttribute' => 'id']],
        ];
    }

    public function add()
    {
        if (!$this->validate()) {
            \Yii::$app->session->addFlash('error', "Invalid data");
            return null;
        }
        $success_count = 0;

        foreach ($this->game_ids as $game_id) {
            $siteGame = new SiteGame();
            $siteGame->site_id = $this->site_id;
            $siteGame->game_id = $game_id;
            $siteGame->is_promoted = $this->is_promoted;
            if (!$siteGame->save()) {
                \Yii::$app->session->addFlash('danger', "Game {$game_id} already added");
            } else {
                $success_count++;
            }
        }
        if ($success_count) \Yii::$app->session->addFlash('success', "{$success_count} games added");
        return true;
    }
}