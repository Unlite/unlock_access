<?php

namespace backend\models\forms;

use common\models\Geo;
use common\models\Site;
use common\models\SiteGeo;
use yii\base\Model;

class SiteGeoMultiForm extends Model
{
    public $site_id;
    public $geo_codes;
    public $is_allowed;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id', 'geo_codes','is_allowed'], 'required'],
            [['site_id','is_allowed'], 'integer'],

            [['site_id'], 'exist', 'skipOnError' => false, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'id']],
            ['geo_codes', 'each', 'rule' => ['exist', 'targetClass' => Geo::className(), 'targetAttribute' => 'code']],
        ];
    }

    public function add()
    {
        if (!$this->validate()) {
            \Yii::$app->session->addFlash('error', "Invalid data");
            return null;
        }
        $success_count = 0;

        foreach ($this->geo_codes as $geo_code) {
            $siteGeo = new SiteGeo();
            $siteGeo->site_id = $this->site_id;
            $siteGeo->is_allowed = $this->is_allowed;
            $siteGeo->geo_code = $geo_code;
            if (!$siteGeo->save()) {
                \Yii::$app->session->addFlash('danger', "Geo {$geo_code} already added");
            }else{
                $success_count++;
            }
        }
        if ($success_count) \Yii::$app->session->addFlash('success', "{$success_count} geo added");
        return true;
    }
}