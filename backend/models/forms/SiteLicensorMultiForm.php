<?php

namespace backend\models\forms;

use common\models\Licensor;
use common\models\Site;
use common\models\SiteLicensor;
use common\models\SiteLocale;
use yii\base\Model;

class SiteLicensorMultiForm extends Model
{
    public $site_id;
    public $licensor_ids;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id', 'licensor_ids'], 'required'],
            [['site_id'], 'integer'],
            [['site_id'], 'exist', 'skipOnError' => false, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'id']],
            ['licensor_ids', 'each', 'rule' => ['exist', 'targetClass' => Licensor::className(), 'targetAttribute' => 'id']],
        ];
    }

    public function add()
    {
        if (!$this->validate()) {
            \Yii::$app->session->addFlash('error', "Invalid data");
            return null;
        }
        $success_count = 0;

        foreach ($this->licensor_ids as $licensor_id) {
            $siteLicensors = new SiteLicensor();
            $siteLicensors->site_id = $this->site_id;
            $siteLicensors->licensor_id = $licensor_id;
            if (!$siteLicensors->save()) {
                \Yii::$app->session->addFlash('danger', "Licensor {$licensor_id} already added");
            } else {
                $success_count++;
            }
        }
        if ($success_count) \Yii::$app->session->addFlash('success', "{$success_count} licensors added");
        return true;
    }
}