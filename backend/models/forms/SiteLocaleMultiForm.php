<?php

namespace backend\models\forms;

use common\models\Locale;
use common\models\Site;
use common\models\SiteLocale;
use yii\base\Model;

class SiteLocaleMultiForm extends Model
{
    public $site_id;
    public $locale_codes;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id', 'locale_codes'], 'required'],
            [['site_id'], 'integer'],
            [['site_id'], 'exist', 'skipOnError' => false, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'id']],
            ['locale_codes', 'each', 'rule' => ['exist', 'targetClass' => Locale::className(), 'targetAttribute' => 'code']],
        ];
    }

    public function add()
    {
        if (!$this->validate()) {
            \Yii::$app->session->addFlash('error', "Invalid data");
            return null;
        }
        $success_count = 0;

        foreach ($this->locale_codes as $locale_code) {
            $siteLocale = new SiteLocale();
            $siteLocale->site_id = $this->site_id;
            $siteLocale->locale_code = $locale_code;
            if (!$siteLocale->save()) {
                \Yii::$app->session->addFlash('danger', "Locale {$locale_code} already added");
            } else {
                $success_count++;
            }
        }
        if ($success_count) \Yii::$app->session->addFlash('success', "{$success_count} locales added");
        return true;
    }
}