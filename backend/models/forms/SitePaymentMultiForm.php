<?php

namespace backend\models\forms;

use common\models\PaymentSystem;
use common\models\Site;
use common\models\SitePayment;
use yii\base\Model;

/**
 * Class SitePaymentMultiForm
 * @package backend\models\forms
 * @property integer $site_id
 * @property integer[] $payment_ids
 * @property integer $is_deposit
 * @property integer $is_withdrawal
 */
class SitePaymentMultiForm extends Model
{
	public $site_id;
	public $payment_ids;
	public $is_deposit;
	public $is_withdrawal;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['site_id', 'payment_ids'], 'required'],
			[['site_id', 'is_deposit', 'is_withdrawal'], 'integer'],
			['payment_ids', 'each', 'rule' => ['integer']],
			[['site_id'], 'exist', 'skipOnError' => false, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'id']],
			['payment_ids', 'each', 'rule' => ['exist', 'targetClass' => PaymentSystem::className(), 'targetAttribute' => 'id']],
		];
	}

	public function add()
	{
		if (!$this->validate()) {
			\Yii::$app->session->addFlash('error', "Invalid data");
			return null;
		}
		$success_count = 0;
		$update_count = 0;

		$searchedSitePayments = SitePayment::find()->where(['in', 'payment_id', $this->payment_ids])->andWhere(['site_id'=>$this->site_id])->all();
		foreach ($this->payment_ids as $key => $payment_id) {
			foreach ($searchedSitePayments as $searchedSitePayment) {
				if ($searchedSitePayment->payment_id == $payment_id) {
					if ($this->is_deposit != 0)
						$searchedSitePayment->is_deposit = $this->is_deposit;
					if ($this->is_withdrawal != 0)
						$searchedSitePayment->is_withdrawal = $this->is_withdrawal;
					$searchedSitePayment->save();
					unset($this->payment_ids[$key]);
					$update_count++;
					break;
				}
			}
		}

		foreach ($this->payment_ids as $key => $payment_id) {
			$sitePayment = new SitePayment();
			$sitePayment->site_id = $this->site_id;
			$sitePayment->payment_id = $payment_id;
			$sitePayment->is_deposit = $this->is_deposit ?: 0;
			$sitePayment->is_withdrawal = $this->is_withdrawal ?: 0;
			if (!$sitePayment->save()) {
				\Yii::$app->session->addFlash('danger', "PaymentSystem {$payment_id} already added");
			} else {
				$success_count++;
			}
		}
		if ($success_count) \Yii::$app->session->addFlash('success', "{$success_count} payments added");
		if ($update_count) \Yii::$app->session->addFlash('success', "{$update_count} payments updated");
		return true;
	}
}