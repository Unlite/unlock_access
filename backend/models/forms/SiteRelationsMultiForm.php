<?php

namespace backend\models\forms;

use common\models\Locale;
use common\models\Site;
use common\models\SiteLocale;
use common\models\SiteRelations;
use yii\base\Model;

class SiteRelationsMultiForm extends Model
{
    public $site_id_l;
    public $site_ids;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id_l', 'site_ids'], 'required'],
            [['site_id_l'], 'integer'],
			['site_ids', 'each', 'rule' => ['integer']],
            [['site_id_l'], 'exist', 'skipOnError' => false, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id_l' => 'id']],
            ['site_ids', 'each', 'rule' => ['exist', 'targetClass' => Site::className(), 'targetAttribute' => 'id']],
        ];
    }

    public function add()
    {
        if (!$this->validate()) {
            \Yii::$app->session->addFlash('error', "Invalid data");
            return null;
        }
        $success_count = 0;

        foreach ($this->site_ids as $site_id_h) {
            $siteLocale = new SiteRelations();
            $siteLocale->site_id_l = $this->site_id_l;
            $siteLocale->site_id_h = $site_id_h;
            if (!$siteLocale->save()) {
                \Yii::$app->session->addFlash('danger', "Site {$site_id_h} already added");
            } else {
                $success_count++;
            }
        }
        if ($success_count) \Yii::$app->session->addFlash('success', "{$success_count} site added");
        return true;
    }
}