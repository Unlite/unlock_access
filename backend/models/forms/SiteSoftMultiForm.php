<?php

namespace backend\models\forms;

use common\models\Site;
use common\models\SiteSoft;
use common\models\Soft;
use yii\base\Model;

class SiteSoftMultiForm extends Model
{
    public $site_id;
    public $soft_ids;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id', 'soft_ids'], 'required'],
            [['site_id'], 'integer'],
            ['soft_ids', 'each', 'rule' => ['integer']],

            [['site_id'], 'exist', 'skipOnError' => false, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'id']],
            ['soft_ids', 'each', 'rule' => ['exist', 'targetClass' => Soft::className(), 'targetAttribute' => 'id']],
        ];
    }

    public function add()
    {
        if (!$this->validate()) {
            \Yii::$app->session->addFlash('error', "Invalid data");
            return null;
        }
        $success_count = 0;

        foreach ($this->soft_ids as $soft_id) {
            $siteSoft = new SiteSoft();
            $siteSoft->site_id = $this->site_id;
            $siteSoft->soft_id = $soft_id;
            if (!$siteSoft->save()) {
                \Yii::$app->session->addFlash('danger', "Soft {$soft_id} already added");
            } else {
                $success_count++;
            }
        }
        if ($success_count) \Yii::$app->session->addFlash('success', "{$success_count} soft added");
        return true;
    }
}