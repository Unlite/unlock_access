<?php

namespace backend\models\forms;

use common\models\BlogPost;
use common\models\TagBlogPost;
use yii\base\Model;
use common\models\Tag;

class TagBlogPostMultiForm extends Model
{
    public $blog_post_id;
    public $tag_ids;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['blog_post_id', 'tag_ids'], 'required'],
            [['blog_post_id'], 'integer'],
            [['blog_post_id'], 'exist', 'skipOnError' => false, 'targetClass' => BlogPost::className(), 'targetAttribute' => ['blog_post_id' => 'id']],
            ['tag_ids', 'each', 'rule' => ['exist', 'targetClass' => Tag::className(), 'targetAttribute' => 'id']],
        ];
    }

    public function add()
    {
        if (!$this->validate()) {
            \Yii::$app->session->addFlash('error', "Invalid data");
            return null;
        }
        $success_count = 0;

        foreach ($this->tag_ids as $game_category_id) {
            $tagBlogPost = new TagBlogPost();
            $tagBlogPost->blog_post_id = $this->blog_post_id;
            $tagBlogPost->tag_id = $game_category_id;
            if (!$tagBlogPost->save()) {
                \Yii::$app->session->addFlash('danger', "Tag {$game_category_id} already added");
            } else {
                $success_count++;
            }
        }
        if ($success_count) \Yii::$app->session->addFlash('success', "{$success_count} Tags added");
        return true;
    }
}