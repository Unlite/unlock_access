<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Bonuses;

/**
 * BonusesSearch represents the model behind the search form about `common\models\Bonuses`.
 */
class BonusesSearch extends Bonuses
{
	/** @var  string */
	public $start_at_date;

	/** @var  string */
	public $end_at_date;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'site_id', 'bonus_type_id', 'start_at', 'end_at', 'is_promoted'], 'integer'],
            [['start_at_date','end_at_date', 'status'], 'string'],
            [['text', 'url', 'promo_code', 'is_promoted', 'short_text', 'start_at_date', 'end_at_date', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Bonuses::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'site_id' => $this->site_id,
            'bonus_type_id' => $this->bonus_type_id,
            'start_at' => $this->start_at,
            'end_at' => $this->end_at,
            'is_promoted' => $this->is_promoted,
        ]);

        $query->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'promo_code', $this->promo_code])
			->andFilterWhere(['like', 'short_text', $this->short_text])
			->andFilterWhere(['like', 'status', $this->status]);

        if(!empty($this->start_at_date) && empty($this->end_at_date))
			$query->andFilterWhere(['start_at' => strtotime($this->start_at_date)]);
		if(!empty($this->end_at_date) && empty($this->start_at_date))
			$query->andFilterWhere(['end_at' => strtotime($this->end_at_date)]);
		if(!empty($this->end_at_date) && !empty($this->start_at_date)){
			$query->andFilterWhere([
				'or',
				['and',
					['<=', 'start_at', strtotime($this->start_at_date)],
					['>=', 'start_at', strtotime($this->end_at_date)],
				],
				['and',
					['>=', 'end_at', strtotime($this->start_at_date)],
					['<=', 'end_at', strtotime($this->end_at_date)]
				],

			]);
		}

		return $dataProvider;
    }
}
