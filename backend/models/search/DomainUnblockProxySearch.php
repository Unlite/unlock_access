<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DomainUnblockProxy;

/**
 * DomainUnblockProxySearch represents the model behind the search form about `common\models\DomainUnblockProxy`.
 */
class DomainUnblockProxySearch extends DomainUnblockProxy
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['domain_unblock_id', 'proxy_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DomainUnblockProxy::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'domain_unblock_id' => $this->domain_unblock_id,
            'proxy_id' => $this->proxy_id,
        ]);

        return $dataProvider;
    }
}
