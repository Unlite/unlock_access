<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PaymentSystemCurrency;

/**
 * PaymentSystemCurrencySearch represents the model behind the search form about `common\models\PaymentSystemCurrency`.
 */
class PaymentSystemCurrencySearch extends PaymentSystemCurrency
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_system_id'], 'integer'],
            [['currency_code'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaymentSystemCurrency::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'payment_system_id' => $this->payment_system_id,
        ]);

        $query->andFilterWhere(['like', 'currency_code', $this->currency_code]);

        return $dataProvider;
    }
}
