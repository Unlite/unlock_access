<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\SiteBlogPost;

/**
 * SiteBlogPostSearch represents the model behind the search form of `common\models\SiteBlogPost`.
 */
class SiteBlogPostSearch extends SiteBlogPost
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id', 'blog_post_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SiteBlogPost::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'site_id' => $this->site_id,
            'blog_post_id' => $this->blog_post_id,
        ]);

        return $dataProvider;
    }
}
