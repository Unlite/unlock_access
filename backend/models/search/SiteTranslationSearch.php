<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\SiteTranslation;

/**
 * SiteTranslationSearch represents the model behind the search form about `common\models\SiteTranslation`.
 */
class SiteTranslationSearch extends SiteTranslation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id'], 'integer'],
            [['geo_code', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SiteTranslation::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'site_id' => $this->site_id,
        ]);

        $query->andFilterWhere(['like', 'geo_code', $this->geo_code])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
