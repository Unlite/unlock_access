<?php

namespace backend\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Translate;

/**
 * TranslateSearch represents the model behind the search form of `\common\models\Translate`.
 */
class TranslateSearch extends Translate
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'table_id', 'serialized'], 'integer'],
            [['locale_code', 'table', 'key', 'value'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Translate::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'table_id' => $this->table_id,
            'serialized' => $this->serialized,
        ]);

        $query->andFilterWhere(['like', 'locale_code', $this->locale_code])
            ->andFilterWhere(['like', 'table', $this->table])
            ->andFilterWhere(['like', 'key', $this->key])
            ->andFilterWhere(['like', 'value', $this->value]);

        return $dataProvider;
    }
}
