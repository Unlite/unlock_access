<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\BlogPostBlogPostCategory */
/* @var $blog_post common\models\BlogPost|null */
/* @var $form yii\widgets\ActiveForm */
/* @var $back null|string */

$action = ['/blog-post-blog-post-category/create'];
if (isset($back)) {
	$action['back'] = $back;
}
$blog_post_categories = \common\models\BlogPostCategory::find()->where(['not in', 'id', !empty($blog_post) ? ArrayHelper::getColumn($blog_post->blogPostCategories, 'id') : []])->all()
?>

<div class="blog-post-blog-post-category-form">

    <?php $form = ActiveForm::begin(['action' => $action]); ?>

    <?= $form->field($model, 'blog_post_id')->dropDownList(ArrayHelper::map(\common\models\BlogPost::find()->all(),'id','title')) ?>

    <?= $form->field($model, 'blog_post_category_id')->dropDownList(ArrayHelper::map($blog_post_categories, 'id', 'name')) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
