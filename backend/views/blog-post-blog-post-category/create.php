<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\BlogPostBlogPostCategory */

$this->title = Yii::t('app', 'Create Blog Post Blog Post Category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Blog Post Blog Post Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-post-blog-post-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
