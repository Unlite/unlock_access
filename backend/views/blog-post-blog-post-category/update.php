<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BlogPostBlogPostCategory */

$this->title = Yii::t('app', 'Update Blog Post Blog Post Category: ' . $model->blog_post_id, [
    'nameAttribute' => '' . $model->blog_post_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Blog Post Blog Post Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->blog_post_id, 'url' => ['view', 'blog_post_id' => $model->blog_post_id, 'blog_post_category_id' => $model->blog_post_category_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="blog-post-blog-post-category-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
