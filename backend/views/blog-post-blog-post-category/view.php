<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\BlogPostBlogPostCategory */

$this->title = $model->blog_post_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Blog Post Blog Post Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-post-blog-post-category-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'blog_post_id' => $model->blog_post_id, 'blog_post_category_id' => $model->blog_post_category_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'blog_post_id' => $model->blog_post_id, 'blog_post_category_id' => $model->blog_post_category_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'blog_post_id',
            'blog_post_category_id',
        ],
    ]) ?>

</div>
