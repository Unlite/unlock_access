<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\BlogPost */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="blog-post-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput() ?>

    <?= $form->field($model, 'slug')->textInput()->hint(\common\widgets\inflector\Slug::widget(['from' => '#blogpostform-title', 'to' => '#blogpostform-slug', 'confirm' => !$model->isNewRecord])) ?>

    <?= $form->field($model, 'short_description')->textInput(['value' => empty($model->short_description) ?  '-' : $model->short_description]) ?>

    <?= $form->field($model, 'page_title')->textInput() ?>

    <?= $form->field($model, 'page_description')->textInput() ?>

	<?= $form->field($model, 'author_name')->textInput() ?>

    <?= $form->field($model, 'udt')->widget(\kartik\date\DatePicker::class, [
		'options' => ['placeholder' => 'Enter update date'],
		'pluginOptions' => [
			'autoclose'=>true,
			'format' => 'dd.mm.yyyy',
			'todayHighlight' => true
		]
	]);?>

	<?= $form->field($model, 'is_published')->checkbox() ?>

	<?= $form->field($model, 'thumbnail_src')->textInput() ?>

	<?= $form->field($model, 'imageFile')->fileInput() ?>

    <?= $form->field($model, 'text')->widget(\yii2mod\markdown\MarkdownEditor::class, [
		'editorOptions' => [
			'showIcons' => ["code", "table"],
		],
	]); ?>



    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
