<?php

use yii\bootstrap\Html;

/* @var $model common\models\BlogPostBlogPostCategory */

?>

<span>
    <?= $model->blogPostCategory->name ?>
    <?= Html::a(Html::icon('trash'), ['/blog-post-blog-post-category/delete', 'blog_post_id' => $model->blog_post_id, 'blog_post_category_id' => $model->blog_post_category_id, 'back' => \yii\helpers\Url::to()], [
        'class' => 'btn btn-xs btn-danger',
        'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
        ],
    ]) ?>
</span>
