<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\BlogPostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Blog Posts');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-post-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Blog Post'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'short_description',
			[
				'attribute'=>'text',
				'value' => function ($model) {
					return StringHelper::truncate($model->text, 250);
				},
                'contentOptions' => [
                    'style' => [
                        'max-width' => '600px',
                        'white-space' => 'normal',
                    ],
                ],
			],
			[
				'attribute' => 'created_at',
				'value' => function($data){return \Yii::$app->formatter->asDatetime($data->created_at,'php:d.m.Y');}
			],
            //'updated_at',
            [
                'attribute' => 'is_published',
                'value' => function(\common\models\BlogPost $model) {return $model->is_published ? 'Yes' : 'No';},
                'filter' => [0 => 'No',1 => 'Yes'],
            ],
            //'page_title',
            //'page_description',
            //'thumbnail_src',
            //'author_name',
            //'updated_date_time:datetime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
