<?php

use yii\bootstrap\Html;

/* @var $model common\models\SiteBlogPost */

?>

<span>
    <?= $model->site->name ?>
    <?= Html::a(Html::icon('trash'), ['/site-blog-post/delete', 'blog_post_id' => $model->blog_post_id, 'site_id' => $model->site_id, 'back' => \yii\helpers\Url::to()], [
        'class' => 'btn btn-xs btn-danger',
        'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
        ],
    ]) ?>
</span>
