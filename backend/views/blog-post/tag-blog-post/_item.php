<?php

use yii\bootstrap\Html;

/* @var $model common\models\TagBlogPost */

?>

<span>
    <?= $model->tag->name ?>
    <?= Html::a(Html::icon('trash'), ['/tag-blog-post/delete', 'blog_post_id' => $model->blog_post_id, 'tag_id' => $model->tag_id, 'back' => \yii\helpers\Url::to()], [
        'class' => 'btn btn-xs btn-danger',
        'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
        ],
    ]) ?>
</span>
