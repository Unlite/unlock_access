<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model common\models\BlogPost */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Blog Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-post-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="panel">
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
		<?php Modal::begin([
			'header' => '<h2>Add Site</h2>',
			'toggleButton' => ['label' => 'Add Site', 'class' => 'btn btn-success'],
			'options' => [
				'tabindex' => false // important for Select2 to work properly
			],
		]);

		echo $this->render('/site-blog-post/_form', [
			'blog_post' => $model,
			'model' => new \common\models\SiteBlogPost(['blog_post_id' => $model->id]),
			'back' => \yii\helpers\Url::to()
		]);

		Modal::end();
		?>

		<?php Modal::begin([
			'header' => '<h2>Add Tag</h2>',
			'toggleButton' => ['label' => 'Add Tag', 'class' => 'btn btn-success'],
		]);

		echo $this->render('/tag-blog-post/_form-multi', [
			'blog_post' => $model,
			'model' => new \backend\models\forms\TagBlogPostMultiForm(['blog_post_id' => $model->id]),
			'back' => \yii\helpers\Url::to()
		]);

		Modal::end();
		?>

		<?php Modal::begin([
			'header' => '<h2>Add Site</h2>',
			'toggleButton' => ['label' => 'Add Category', 'class' => 'btn btn-success'],
			'options' => [
				'tabindex' => false // important for Select2 to work properly
			],
		]);

		echo $this->render('/blog-post-blog-post-category/_form', [
			'blog_post' => $model,
			'model' => new \common\models\BlogPostBlogPostCategory(['blog_post_id' => $model->id]),
			'back' => \yii\helpers\Url::to()
		]);

		Modal::end();
		?>
    </div>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'slug',
            'short_description',
			'page_title',
			'page_description',
			[
				'attribute' => 'created_at',
				'value' => \Yii::$app->formatter->asDatetime($model->created_at,'php:d.m.Y')
			],
			[
				'attribute' => 'updated_at',
				'value' => \Yii::$app->formatter->asDatetime($model->updated_at,'php:d.m.Y')
			],
			'is_published:boolean',
            'thumbnail_src:image',
            'author_name',
			[
				'attribute' => 'updated_date_time',
				'value' => \Yii::$app->formatter->asDatetime($model->updated_date_time,'php:d.m.Y')
			],
			[
				'label' => 'Tags (' . $model->getTagBlogPosts()->count() . ')',
				'value' => function (\common\models\BlogPost $model, $widget) {
					$tagDataProvider = new \yii\data\ActiveDataProvider([
						'query' => $model->getTagBlogPosts(),
						'pagination' => ['pageSize' => 50],

					]);
					return ListView::widget([
						'dataProvider' => $tagDataProvider,
						'itemView' => 'tag-blog-post/_item',
						'emptyText' => false,
						'summary' => '',
					]);
				},
				'format' => 'raw',
			],
			[
				'label' => 'Sites (' . $model->getSiteBlogPosts()->count() . ')',
				'value' => function (\common\models\BlogPost $model) {
					$siteDataProvider = new \yii\data\ActiveDataProvider([
						'query' => $model->getSiteBlogPosts(),
						'pagination' => [
							'pageSize' => 50,
						],
					]);
					return ListView::widget([
						'dataProvider' => $siteDataProvider,
						'itemView' => 'site-blog-post/_item',
						'emptyText' => false,
						'summary' => '',
					]);
				},
				'format' => 'raw',
			],
			[
				'label' => 'Categories (' . $model->getBlogPostBlogPostCategories()->count() . ')',
				'value' => function (\common\models\BlogPost $model) {
					$categoryDataProvider = new \yii\data\ActiveDataProvider([
						'query' => $model->getBlogPostBlogPostCategories(),
						'pagination' => [
							'pageSize' => 50,
						],
					]);
					return ListView::widget([
						'dataProvider' => $categoryDataProvider,
						'itemView' => 'blog-post-blog-post-category/_item',
						'emptyText' => false,
						'summary' => '',
					]);
				},
				'format' => 'raw',
			],
        ],
    ]) ?>

    <h3>Text</h3>
    <div class="well">
		<?= \yii\helpers\Markdown::process($model->text, 'gfm') ?>
    </div>
</div>
