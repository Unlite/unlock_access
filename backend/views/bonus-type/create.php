<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\BonusType */

$this->title = Yii::t('app', 'Create Bonus Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bonus Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bonus-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
