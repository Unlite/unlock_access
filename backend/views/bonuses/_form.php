<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model common\models\Bonuses */
/* @var $form yii\widgets\ActiveForm */
$action = ['/bonuses/' . ($model->isNewRecord ? 'create' : 'update')];
if (!$model->isNewRecord) {
	$action['id'] = $model->id;
}
if (isset($back)) {
	$action['back'] = $back;
}
?>

<div class="bonuses-form">
    <?php $form = ActiveForm::begin(['action' => $action]); ?>

	<?= $form->field($model, 'site_id')->widget(\kartik\select2\Select2::className(),[
		'data' => ArrayHelper::map(\common\models\Site::find()->all(),'id','name'),
		'options' => [
			'placeholder' => 'Select Site...',
		],
	]) ?>


    <?= $form->field($model, 'bonus_type_id')->dropDownList(ArrayHelper::map(\common\models\BonusType::find()->all(),'id','name')) ?>


    <?= $form->field($model, 'text')->textInput() ?>
	<?= $form->field($model, 'short_text')->textInput() ?>
    <?= $form->field($model, 'url')->textInput()->hint(\common\modules\away\widgets\MacrosListWidget::widget()) ?>

    <?= $form->field($model, 'promo_code')->textInput() ?>
    <?= $form->field($model, 'is_promoted')->checkbox() ?>
	<?=$form->field($model, 'st')->widget(DatePicker::classname(), [
		'options' => ['placeholder' => 'Enter expired date'],
//		'value' => date('dd.mm.yyyy',$model->start_at),
		'pluginOptions' => [
			'autoclose'=>true,
            'format' => 'dd.mm.yyyy',
            'todayHighlight' => true
		]
	]);?>
	<?=$form->field($model, 'et')->widget(DatePicker::classname(), [
		'options' => ['placeholder' => 'Enter expired date'],
		'pluginOptions' => [
			'autoclose'=>true,
			'format' => 'dd.mm.yyyy',
			'todayHighlight' => true
		]
	]);?>

	<?= $form->field($model, 'status')->dropDownList(array_combine($model::statuses(), $model::statuses())) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
