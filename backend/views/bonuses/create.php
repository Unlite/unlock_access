<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Bonuses */

$this->title = Yii::t('app', 'Create Bonuses');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bonuses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bonuses-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
