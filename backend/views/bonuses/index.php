<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\BonusesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Bonuses');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bonuses-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Bonuses'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			[
				'label' => 'Site',
				'attribute' => 'site_id',
				'value' => function (\common\models\Bonuses $model, $key, $index, $column) {
					return \yii\bootstrap\Html::a($model->site->name, ['/site/view', 'id' => $model->site->id]);
				},
				'format' => 'Html',
				'filter' => Html::activeDropDownList($searchModel, 'site_id', \yii\helpers\ArrayHelper::map(\common\models\Site::find()->all(), 'id', 'name'), ['class' => 'form-control', 'prompt' => ''])
			],

			[
				'label' => 'Bonus Type',
				'attribute' => 'bonus_type_id',
				'value' => function (\common\models\Bonuses $model, $key, $index, $column) {
					return \yii\bootstrap\Html::a($model->bonusType->name, ['/bonus-type/view', 'id' => $model->bonusType->id]);
				},
				'format' => 'Html',
				'filter' =>  \yii\helpers\ArrayHelper::map(\common\models\BonusType::find()->all(), 'id', 'name')
			],
            [
                'attribute'=>'text',
                'value' => function($data)  {
                    return \yii\helpers\StringHelper::truncate($data->text,100);
                }
            ],

            'url:url',
            // 'promo_code',
            [
                'label' => 'Start date',
                'value' => function(\common\models\Bonuses $data) {return Yii::$app->formatter->asDate($data->start_at,'dd.MM.yyyy');},
                'filter' => DatePicker::widget([
					'options' => ['placeholder' => 'Enter Start date'],
					'model' => $searchModel,
					'attribute' => 'start_at_date',
					'pluginOptions' => [
						'autoclose'=>true,
						'format' => 'dd.mm.yyyy',
						'todayHighlight' => true
					]
				])
            ],
            [
                'label' => 'End date',
                'value' => function(\common\models\Bonuses $data) {return Yii::$app->formatter->asDate($data->end_at,'dd.MM.yyyy');},
                'filter' => DatePicker::widget([
					'options' => ['placeholder' => 'Enter End date'],
					'model' => $searchModel,
					'attribute' => 'end_at_date',
					'pluginOptions' => [
						'autoclose'=>true,
						'format' => 'dd.mm.yyyy',
						'todayHighlight' => true
					]
				])
            ],			[
				'attribute' => 'status',
				'filter' => array_combine($searchModel::statuses(),$searchModel::statuses())
			],
            // 'is_promoted',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
