<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;
use yii\widgets\ListView;
/* @var $this yii\web\View */
/* @var $model common\models\Bonuses */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bonuses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bonuses-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                    'attribute' => 'site_id',
                    'value' => function($data) {
						return Html::a($data->site->name,['/site/view','id'=>$data->site_id]);
                    },
                    'format' => 'raw'
            ],
            [
                    'attribute' => 'bonus_type_id',
                    'value' => function(\common\models\Bonuses $data) {
						return Html::a($data->bonusType->name,['/bonus_type/view','id'=>$data->bonus_type_id]);
                    },
                    'format' => 'raw'
            ],
            'text',
            'url:url',
            [
                'label' => 'Url',
                'value' => \yii\bootstrap\Html::a($model->url, \common\modules\away\widgets\ModelLinkWidget::widget(['model' => $model]), ['target' => '_blank']),
                'format' => 'Raw'
            ],
            'promo_code',
			[
				'attribute' => 'start_at',
				'value' => \Yii::$app->formatter->asDatetime($model->start_at,'php:d.m.Y')
			],
			[
				'attribute' => 'end_at',
				'value' => \Yii::$app->formatter->asDatetime($model->end_at,'php:d.m.Y')
			],
			[
				'attribute' => 'is_promoted',
				'value' => function($data) { return $data->is_promoted ? 'Yes' : 'No';}
			],
			[
				'label' => 'Offer Types',
				'value' => function (\common\models\Bonuses $model, $widget) {
					$linkDataProvider = new \yii\data\ActiveDataProvider([
						'query' => $model->getOfferTypeBonuses(),
						'pagination' => ['pageSize' => 50],

					]);
					return ListView::widget([
						'dataProvider' => $linkDataProvider,
						'itemView' => 'offer-type-bonuses/_item',
						'emptyText' => false,
						'summary' => '',
					]);
				},
				'format' => 'raw',
			],
			'short_text',
			'status'
        ],
    ]) ?>
	<?php Modal::begin([
		'header' => '<h2>Add Offer Type</h2>',
		'toggleButton' => ['label' => 'Add Offer Type', 'class' => 'btn btn-success'],
	]);

	echo $this->render('/offer-type-bonuses/_form', [
		'game' => $model,
		'is_allowed' => false,
		'model' => new \common\models\OfferTypeBonuses(['bonuses_id' => $model->id]),
		'back' => \yii\helpers\Url::to()
	]);

	Modal::end();
	?>
</div>
