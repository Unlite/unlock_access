<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Site;
use \common\components\cashback\models\Cashback;
/* @var $this yii\web\View */
/* @var $model common\components\cashback\models\Cashback */
/* @var $form yii\widgets\ActiveForm */
$sites = Site::find()
    ->andWhere(['not in','id',\yii\helpers\ArrayHelper::getColumn(Cashback::find()->andWhere(['!=' ,'id',null])->all(),'site_id')])
    ->all();
?>

<div class="cashback-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'site_id')->dropDownList(\yii\helpers\ArrayHelper::map($sites,'id','name')) ?>

    <?= $form->field($model, 'client_status')->dropDownList([ 'active' => 'Active', 'paused' => 'Paused', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'cron_status')->dropDownList([ 'active' => 'Active', 'paused' => 'Paused', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'percent')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_percent')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
