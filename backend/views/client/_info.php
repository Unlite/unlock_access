<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 02.05.2019
 * Time: 19:58:35
 *
 */
/* @var $this yii\web\View */
/* @var $model common\models\Client */
?>
<div class="col-md-6">
	<?= \yii\widgets\DetailView::widget([
		'model' => $model,
		'attributes' => [
			'id',
			'user_id',
			'token',
			'secret',
			'ip',
			'extension_id',
			'user_agent',
			'created_at',
			'last_online_at',
		],
	]) ?>
</div>
<div class="col-md-6">
	<table class="table table-striped table-bordered detail-view">
		<thead>
		<tr>
			<th>Currency</th>
			<th>Amount</th>
		</tr>
		</thead>
		<tbody>
		<?php
		$userCurrencyDataProvider = new \yii\data\ActiveDataProvider([
			'query' => \common\models\UserCurrency::find()->byUser(Yii::$app->user->id),
			'pagination' => [
				'pageSize' => 50,
			],
		]);
		echo  \yii\widgets\ListView::widget([
			'dataProvider' => $userCurrencyDataProvider,
			'itemView' => 'user-currency/_item',
			'emptyText' => false,
			'summary' => '',
		]);
		?>
		</tbody>
	</table>
</div>
