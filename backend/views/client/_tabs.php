<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 02.05.2019
 * Time: 19:45:51
 */

\yii\bootstrap\NavBar::begin([
	'options' => [
		'class' => 'nav-tabs nav-justified',
	],
]);
$menuItems = [
	['label' => 'Cashback', 'url' => ['/client/view', 'id' => $model->id]],
	['label' => 'Withdraw', 'url' => ['/client/withdraw', 'id' => $model->id]],
	['label' => 'Exchange', 'url' => ['/client/exchange', 'id' => $model->id]],
];
echo \yii\bootstrap\Nav::widget([
	'options' => ['class' => 'navbar-nav nav-tabs nav-justified'],
	'items' => $menuItems,
]);
\yii\bootstrap\NavBar::end();