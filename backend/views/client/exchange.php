<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Client */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $searchModel \common\models\WithdrawTransfer */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Clients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
$view = $this;

?>
<div class="client-view row">
    <?=$this->render('_info',['model'=>$model])?>
    <div class="col-xs-12">
        <h2>Transfers</h2>
        <?=$this->render('_tabs',['model'=>$model])?>
    </div>
    <div class="col-xs-12">
		<?= \yii\grid\GridView::widget([
			'dataProvider' => $dataProvider,
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],

				'id',
				'type',
				'status',
				[
                    'attribute' => 'amount',
                    'value' => function(\common\models\Exchange $data){
		                return Yii::$app->formatter->asCurrency($data->amount,$data->userCurrency->currency_code);
                    },
                    'header' => 'Exchange from',
                ],
				[
                    'attribute' => 'transferTo.amount',
                    'value' => function(\common\models\Exchange $data){
		                return Yii::$app->formatter->asCurrency($data->transferTo->amount,$data->transferTo->userCurrency->currency_code);
                    },
                    'header' => 'Exchange to',
                ],
				[
                    'attribute' => 'transferCommission.amount',
                    'value' => function(\common\models\Exchange $data){
		                return Yii::$app->formatter->asCurrency($data->transferCommission->amount,$data->transferCommission->userCurrency->currency_code);
                    },
                    'header' => 'Comission',
                ],
				'created_at:date',
				'updated_at:date',
				[
					'attribute' => 'data',
					'value' => function (\common\models\Exchange $data) use ($view){
						return $view->render('_data',['model'=>$data]);
					},
					'format' => 'html'
				],

				[
					'class' => \yii\grid\ActionColumn::className(),
					'buttons'=>[
						'reject'=> function ($url, $model){
		                    if($model->status != \common\models\Transfer::STATUS_REJECTED)
                                return Html::a('<span class="glyphicon glyphicon-remove"></span>',['/transfer/reject','id'=>$model['id']],['onclick'=>"return confirm('Are you shure you want to reject this transfer?')"]);
                        }
					],
					'template'=>'{view} {update} {delete} {reject}',
				],
			],
		]); ?>
    </div>

</div>
