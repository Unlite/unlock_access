<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Client */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $searchModel \common\models\WithdrawTransfer */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Clients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="client-view row">
    <?=$this->render('_info',['model'=>$model])?>
    <div class="col-xs-12">
        <h2>Transfers</h2>
        <?=$this->render('_tabs',['model'=>$model])?>
    </div>
    <div class="col-xs-12">

    </div>

</div>
