<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\CustomList;

/* @var $this yii\web\View */
/* @var $site common\models\Site|null */
/* @var $model common\models\CustomListSite */
/* @var $form yii\widgets\ActiveForm */
/* @var $back null|string */

$action = ['/custom-list-site/create-multi-lists'];
if (isset($back)) {
    $action['back'] = $back;
}
/* @var $customLists common\models\CustomList[]|null */
$customLists = CustomList::find()->where(['not in', 'id', !empty($site) ? ArrayHelper::getColumn($site->customLists, 'id') : []])->all()
?>

<div class="site-payment-form">

    <?php $form = ActiveForm::begin(['action' => $action]); ?>

    <?= $form->field($model, 'site_id')->dropDownList(ArrayHelper::map(\common\models\Site::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'custom_list_ids[]')->widget(\kartik\select2\Select2::className(),[
        'data' => ArrayHelper::map($customLists, 'id', 'name'),
        'options' => ['placeholder' => 'Select Custom lists...', 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Add', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
