<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CustomListSite */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="custom-list-site-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'custom_list_id')->textInput() ?>

    <?= $form->field($model, 'site_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
