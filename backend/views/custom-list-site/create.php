<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CustomListSite */
/* @var $back null|string */
/* @var $multi bool */

$this->title = Yii::t('app', 'Create Custom List Site');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Custom List Sites'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="custom-list-site-create">

    <h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render($multi ? '_form-multi' : '_form', [
		'model' => $model,
		'back' => $back,
	]) ?>

</div>
