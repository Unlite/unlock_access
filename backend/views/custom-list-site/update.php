<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CustomListSite */

$this->title = Yii::t('app', 'Update Custom List Site: {nameAttribute}', [
    'nameAttribute' => $model->custom_list_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Custom List Sites'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->custom_list_id, 'url' => ['view', 'custom_list_id' => $model->custom_list_id, 'site_id' => $model->site_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="custom-list-site-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
