<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CustomList */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="custom-list-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput() ?>

    <?= $form->field($model, 'type')->dropDownList(\common\models\CustomList::getTypes()) ?>

	<?= $form->field($model, 'status')->dropDownList(array_combine($model::statuses(), $model::statuses())) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
