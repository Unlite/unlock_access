<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CustomList */

$this->title = Yii::t('app', 'Create Custom List');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Custom Lists'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="custom-list-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
