<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CustomList */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Custom Lists'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="custom-list-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'type',
            'status'
        ],
    ]) ?>

    <?php echo \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                    'query' => $model->getCustomListSites(),
            ]),
        'columns' => [
                [
                    'label' => 'Site',
                    'value' => function($data){
                        /* @var $data \common\models\CustomListSite */
                        return Html::a($data->site->name,['site/view','id'=>$data->site_id]);
                    },
                    'format' => 'raw',
                ],
                [
                    'label' => 'Site url',
                    'value' => function($data){
                        /* @var $data \common\models\CustomListSite */
                        return Html::a($data->site->getDomains()->typeMain()->one()->direct_url,['site/view','id'=>$data->site_id]);
                    },
                    'format' => 'raw',
                ]
        ]
    ])?>

</div>
