<?php

use common\models\Proxy;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $domain common\models\Domain|null */
/* @var $model common\models\SiteGeo */
/* @var $form yii\widgets\ActiveForm */
/* @var $back null|string */

$action = ['/domain-proxy/create-multi-proxies'];
if (isset($back)) {
    $action['back'] = $back;
}
/* @var $proxies common\models\Proxy[]|null */
$proxies = Proxy::find()->where(['not in', 'id', !empty($domain) ? ArrayHelper::getColumn($domain->proxies, 'id') : []])->all();
$data = [];
foreach ($proxies as $proxy) {
    if (!isset($data[$proxy->geo->name])) $data[$proxy->geo->name] = [];
    $data[$proxy->geo->name][$proxy->id] = $proxy->fullAddress;
}
?>

<div class="site-geo-form">

    <?php $form = ActiveForm::begin(['action' => $action]); ?>

    <?= $form->field($model, 'domain_id')->dropDownList(ArrayHelper::map(\common\models\Domain::find()->all(), 'id', 'domain')) ?>

    <?= $form->field($model, 'proxy_ids[]')->dropDownList($data, ['multiple' => 'multiple', 'size' => 30]) ?>

    <div class="form-group">
        <?= Html::submitButton('Add', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
