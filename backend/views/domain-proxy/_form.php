<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DomainProxy */
/* @var $form yii\widgets\ActiveForm */
/* @var $back null|string */

$action = ['/domain-proxy/' . ($model->isNewRecord ? 'create' : 'update')];
if (!$model->isNewRecord) {
    $action['domain_id'] = $model->domain_id;
    $action['proxy_id'] = $model->proxy_id;
}
if (isset($back)) {
    $action['back'] = $back;
}
?>

<div class="domain-unblock-proxy-form">

    <?php $form = ActiveForm::begin(['action' => $action]); ?>

    <?= $form->field($model, 'domain_id')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Domain::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'proxy_id')
        ->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Proxy::find()->all(),
            'id',
            function(\common\models\Proxy $model, $default = null)
            {
                return $model->geo->name . ' | ' . $model->fullAddress;
            })) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
