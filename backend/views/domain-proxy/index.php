<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\DomainProxySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Domain Proxies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="domain-unblock-proxy-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Domain Proxy', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Create Domain Proxies', ['create-multi-proxies'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Domain',
                'attribute' => 'domain_id',
                'value' => function (\common\models\DomainProxy $model, $key, $index, $column) {
                    return \yii\bootstrap\Html::a($model->domain->domain, ['/domain/view', 'id' => $model->domain->id]);
                },
                'format' => 'Html',
                'filter' => Html::activeDropDownList($searchModel, 'domain_id', \yii\helpers\ArrayHelper::map(\common\models\Domain::find()->all(), 'id', 'domain'), ['class' => 'form-control', 'prompt' => ''])
            ],
            [
                'label' => 'Proxy',
                'attribute' => 'proxy_id',
                'value' => function (\common\models\DomainProxy $model, $key, $index, $column) {
                    return \yii\bootstrap\Html::a($model->proxy->fullAddress, ['/proxy/view', 'id' => $model->proxy->id]);
                },
                'format' => 'Html',
                'filter' => Html::activeDropDownList($searchModel, 'proxy_id', \yii\helpers\ArrayHelper::map(\common\models\Proxy::find()->all(), 'id', 'fullAddress'), ['class' => 'form-control', 'prompt' => ''])
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
