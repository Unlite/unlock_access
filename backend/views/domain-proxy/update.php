<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DomainProxy */
/* @var $back null|string */

$this->title = 'Update Domain Proxy: ' . $model->domain_id;
$this->params['breadcrumbs'][] = ['label' => 'Domain Proxies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->domain_id, 'url' => ['view', 'domain_id' => $model->domain_id, 'proxy_id' => $model->proxy_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="domain-unblock-proxy-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'back' => $back,
    ]) ?>

</div>
