<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DomainUnblockProxy */
/* @var $back null|string */
/* @var $multi bool */

$this->title = 'Create Domain Unblock Proxy';
$this->params['breadcrumbs'][] = ['label' => 'Domain Unblock Proxies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="domain-unblock-proxy-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render($multi ? '_form-multi' : '_form', [
        'model' => $model,
        'back' => $back,
    ]) ?>

</div>
