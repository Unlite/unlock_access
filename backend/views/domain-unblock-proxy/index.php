<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\DomainUnblockProxySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Domain Unblock Proxies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="domain-unblock-proxy-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Domain Proxy', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Create Domain Proxies', ['create-multi-proxies'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'domain_unblock_id',
            'proxy_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
