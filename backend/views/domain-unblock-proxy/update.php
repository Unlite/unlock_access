<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DomainUnblockProxy */
/* @var $back null|string */

$this->title = 'Update Domain Unblock Proxy: ' . $model->domain_unblock_id;
$this->params['breadcrumbs'][] = ['label' => 'Domain Unblock Proxies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->domain_unblock_id, 'url' => ['view', 'domain_unblock_id' => $model->domain_unblock_id, 'proxy_id' => $model->proxy_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="domain-unblock-proxy-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'back' => $back,
    ]) ?>

</div>
