<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\DomainUnblockProxy */

$this->title = $model->domain_unblock_id;
$this->params['breadcrumbs'][] = ['label' => 'Domain Unblock Proxies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="domain-unblock-proxy-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'domain_unblock_id' => $model->domain_unblock_id, 'proxy_id' => $model->proxy_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'domain_unblock_id' => $model->domain_unblock_id, 'proxy_id' => $model->proxy_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'domain_unblock_id',
            'proxy_id',
        ],
    ]) ?>

</div>
