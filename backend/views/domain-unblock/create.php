<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DomainUnblock */

$this->title = 'Create Domain Unblock';
$this->params['breadcrumbs'][] = ['label' => 'Domain Unblocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="domain-unblock-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
