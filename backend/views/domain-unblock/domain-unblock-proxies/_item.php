<?php

use yii\bootstrap\Html;
/* @var $model common\models\DomainUnblockProxy */

?>


<span>
    <?= Html::a($model->proxy->fullAddress, ['/proxy/view', 'id' => $model->proxy->id]) . " ({$model->proxy->geo->name})" ?>
    <?= Html::a(Html::icon('trash'), ['/domain-unblock-proxy/delete', 'domain_unblock_id' => $model->domain_unblock_id, 'proxy_id' => $model->proxy_id, 'back' => \yii\helpers\Url::to()], [
        'class' => 'btn btn-xs btn-danger',
        'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
        ],
    ]) ?>
</span>
