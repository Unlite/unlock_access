<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\DomainUnblockSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Domain Unblocks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="domain-unblock-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Domain Unblock', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Name',
                'attribute' => 'name',
                'value' => function (\common\models\DomainUnblock $model, $key, $index, $column) {
                    return \yii\bootstrap\Html::a($model->name, ['/domain-unblock/view', 'id' => $model->id]);
                },
                'format' => 'Html',
            ],
            'pattern',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
