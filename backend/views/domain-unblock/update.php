<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DomainUnblock */

$this->title = 'Update Domain Unblock: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Domain Unblocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="domain-unblock-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
