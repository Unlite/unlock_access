<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model common\models\DomainUnblock */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Domain Unblocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="domain-unblock-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'pattern',
            [
                'label' => 'Proxies',
                'value' => function (\common\models\DomainUnblock $model, $widget) {
                    $linkDataProvider = new \yii\data\ActiveDataProvider([
                        'query' => $model->getDomainUnblockProxies()
                    ]);
                    return ListView::widget([
                        'dataProvider' => $linkDataProvider,
                        'itemView' => 'domain-unblock-proxies/_item',
                        'emptyText' => false,
                        'summary' => '',
                    ]);
                },
                'format' => 'raw',
            ],
        ],
    ]) ?>
    <p>
        <?php Modal::begin([
            'header' => '<h2>Add Proxies</h2>',
            'toggleButton' => ['label' => 'Add Proxies', 'class' => 'btn btn-success'],
        ]);

        echo $this->render('/domain-unblock-proxy/_form-multi', [
            'model' => new \backend\models\forms\DomainUnblockProxyMultiForm(['domain_unblock_id' => $model->id]),
            'back' => \yii\helpers\Url::to(),
            'domain_unblock' => $model,
        ]);

        Modal::end();
        ?>
    </p>

</div>
