<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Domain */
/* @var $form yii\widgets\ActiveForm */
/* @var $back null|string */

$action = ['/domain/' . ($model->isNewRecord ? 'create' : 'update')];
if (!$model->isNewRecord) {
    $action['id'] = $model->id;
}
if (isset($back)) {
    $action['back'] = $back;
}
?>

<div class="domain-form">

    <?php $form = ActiveForm::begin(['action' => $action]); ?>

    <?= $form->field($model, 'site_id')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Site::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'type')->dropDownList(array_combine(\common\models\Domain::types(), \common\models\Domain::types())) ?>

    <?= $form->field($model, 'domain')->textInput() ?>

    <?= $form->field($model, 'direct_url')->textInput()->hint(\common\modules\away\widgets\MacrosListWidget::widget()) ?>

    <?= $form->field($model, 'proxy_pattern')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList(array_combine(\common\models\Domain::statuses(), \common\models\Domain::statuses())) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
