<?php

use yii\bootstrap\Html;
/* @var $model common\models\DomainProxy */

?>

<span>
    <?= Html::a($model->proxy->fullAddress, ['/proxy/view', 'id' => $model->proxy->id])
    . Html::tag('span', $model->proxy->type, ['class' => "label label-" . ($model->proxy->type == \common\models\Proxy::TYPE_PRIVATE ? 'success' : 'warning')])
    . " ({$model->proxy->geo->name})" ?>
    <?= Html::a(Html::icon('trash'), ['/domain-proxy/delete', 'domain_id' => $model->domain_id, 'proxy_id' => $model->proxy_id, 'back' => \yii\helpers\Url::to()], [
        'class' => 'btn btn-xs btn-danger',
        'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
        ],
    ]) ?>
</span>
