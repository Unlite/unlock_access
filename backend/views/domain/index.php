<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\DomainSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Domains';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="domain-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Domain', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Domain',
                'attribute' => 'domain',
                'value' => function (\common\models\Domain $model, $key, $index, $column) {
                    return \yii\bootstrap\Html::a($model->domain, ['/domain/view', 'id' => $model->id]);
                },
                'format' => 'Html',

            ],
            [
                'label' => 'Site',
                'attribute' => 'site_id',
                'value' => function (\common\models\Domain $model, $key, $index, $column) {
                    return \yii\bootstrap\Html::a($model->site->name, ['/site/view', 'id' => $model->site->id]);
                },
                'format' => 'Html',
                'filter' => Html::activeDropDownList($searchModel, 'site_id', \yii\helpers\ArrayHelper::map(\common\models\Site::find()->all(), 'id', 'name'), ['class' => 'form-control', 'prompt' => ''])
            ],
            [
                'attribute' => 'type',
                'filter' => Html::activeDropDownList($searchModel, 'type', array_combine(\common\models\Domain::types(), \common\models\Domain::types()), ['class' => 'form-control', 'prompt' => ''])

            ],
            [
                'attribute' => 'status',
                'filter' => Html::activeDropDownList($searchModel, 'status', array_combine(\common\models\Domain::statuses(), \common\models\Domain::statuses()), ['class' => 'form-control', 'prompt' => ''])

            ],
            'direct_url:url',
            'proxy_pattern',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
