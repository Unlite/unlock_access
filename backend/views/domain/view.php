<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model common\models\Domain */

$this->title = $model->domain;
$this->params['breadcrumbs'][] = ['label' => 'Domains', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->site->name, 'url' => ['/site/view', 'id' => $model->site->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="domain-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'label' => 'Site',
                'value' => \yii\bootstrap\Html::a($model->site->name, ['/site/view', 'id' => $model->site->id]),
                'format' => 'Html'
            ],
            'type',
            'status',
            'domain',
            [
                'label' => 'Direct Url',
                'value' => \yii\bootstrap\Html::a($model->direct_url, \common\modules\away\widgets\ModelLinkWidget::widget(['model' => $model]), ['target' => '_blank']),
                'format' => 'Raw'
            ],
            'proxy_pattern',
            [
                'label' => 'Proxies',
                'value' => function (\common\models\Domain $model) {
                    $linkDataProvider = new \yii\data\ActiveDataProvider([
                        'query' => $model->getDomainProxies()
                    ]);
                    return ListView::widget([
                        'dataProvider' => $linkDataProvider,
                        'itemView' => 'domain-proxies/_item',
                        'emptyText' => false,
                        'summary' => '',
                    ]);
                },
                'format' => 'raw',
            ],
        ],
    ]) ?>

    <p>
        <?php Modal::begin([
            'header' => '<h2>Add Proxies</h2>',
            'toggleButton' => ['label' => 'Add Proxies', 'class' => 'btn btn-success'],
        ]);

        echo $this->render('/domain-proxy/_form-multi', [
            'model' => new \backend\models\forms\DomainProxyMultiForm(['domain_id' => $model->id]),
            'back' => \yii\helpers\Url::to(),
            'domain' => $model,
        ]);

        Modal::end();
        ?>
    </p>
</div>
