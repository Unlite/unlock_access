<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\EmailSubscriptions */

$this->title = 'Create Email Subscriptions';
$this->params['breadcrumbs'][] = ['label' => 'Email Subscriptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-subscriptions-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
