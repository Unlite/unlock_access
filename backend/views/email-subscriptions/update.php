<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\EmailSubscriptions */

$this->title = 'Update Email Subscriptions: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Email Subscriptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="email-subscriptions-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
