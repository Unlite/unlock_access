<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\GameCategory;

/* @var $this yii\web\View */
/* @var $game common\models\Game|null */
/* @var $model common\models\GameCategoryGame */
/* @var $form yii\widgets\ActiveForm */
/* @var $back null|string */

$action = ['/game-category-game/create-multi-game-categories'];
if (isset($back)) {
    $action['back'] = $back;
}
/* @var $game_category common\models\GameCategory[]|null */
$game_category = GameCategory::find()->where(['not in', 'id', !empty($game) ? ArrayHelper::getColumn($game->gameCategories, 'id') : []])->all()
?>

<div class="site-payment-form">

    <?php $form = ActiveForm::begin(['action' => $action]); ?>

    <?= $form->field($model, 'game_id')->dropDownList(ArrayHelper::map(\common\models\Game::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'game_category_ids[]')->widget(\kartik\select2\Select2::className(),[
        'data' => ArrayHelper::map($game_category, 'id', 'name'),
        'options' => ['placeholder' => 'Select Categories...', 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Add', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
