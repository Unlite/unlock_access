<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\GameCategoryGame */
/* @var $back null|string */
/* @var $multi bool */

$this->title = Yii::t('app', 'Create Game Category Game');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Game Category Games'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-category-game-create">

    <h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render($multi ? '_form-multi' : '_form', [
		'model' => $model,
		'back' => $back,
	]) ?>

</div>
