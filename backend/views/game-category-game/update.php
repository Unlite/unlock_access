<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\GameCategoryGame */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Game Category Game',
]) . $model->game_category_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Game Category Games'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->game_category_id, 'url' => ['view', 'game_category_id' => $model->game_category_id, 'game_id' => $model->game_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="game-category-game-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
