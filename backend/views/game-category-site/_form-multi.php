<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\GameCategory;

/* @var $this yii\web\View */
/* @var $site common\models\Site|null */
/* @var $model common\models\GameCategoryGame */
/* @var $form yii\widgets\ActiveForm */
/* @var $back null|string */

$action = ['/game-category-site/create-multi-game-categories'];
if (isset($back)) {
    $action['back'] = $back;
}
/* @var $site_game_category common\models\GameCategory[]|null */
$site_game_category = GameCategory::find()->where(['not in', 'id', !empty($site) ? ArrayHelper::getColumn($site->gameCategories, 'id') : []])->all()
?>

<div class="site-payment-form">

    <?php $form = ActiveForm::begin(['action' => $action]); ?>

    <?= $form->field($model, 'site_id')->dropDownList(ArrayHelper::map(\common\models\Site::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'game_category_ids[]')->widget(\kartik\select2\Select2::className(),[
        'data' => ArrayHelper::map($site_game_category, 'id', 'name'),
        'options' => ['placeholder' => 'Select Categories...', 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Add', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
