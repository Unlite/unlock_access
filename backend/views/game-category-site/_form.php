<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Site;
use common\models\GameCategory;
/* @var $this yii\web\View */
/* @var $model common\models\GameCategorySite */
/* @var $site common\models\Site */
/* @var $form yii\widgets\ActiveForm */

$action = ['/game-category-site/' . ($model->isNewRecord ? 'create' : 'update')];
if (!$model->isNewRecord) {
	$action['site_id'] = $model->site_id;
}
if (isset($back)) {
	$action['back'] = $back;
}

?>

<div class="game-category-site-form">

    <?php $form = ActiveForm::begin(['action' => $action]); ?>

	<?= $form->field($model, 'site_id')->dropDownList(ArrayHelper::map(Site::find()->all(),'id','name')) ?>
	<?= $form->field($model, 'game_category_id')->dropDownList(ArrayHelper::map(GameCategory::find()->where(['not in', 'code', !empty($site) ? ArrayHelper::getColumn($site->gameCategorySites, 'id') : []])->all(),'id','name')) ?>

    <?= $form->field($model, 'is_auto_created')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
