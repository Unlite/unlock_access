<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\GameCategorySite */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Game Category Site',
]) . $model->game_category_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Game Category Sites'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->game_category_id, 'url' => ['view', 'game_category_id' => $model->game_category_id, 'site_id' => $model->site_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="game-category-site-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
