<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\GameCategory */

$this->title = Yii::t('app', 'Create Game Category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Game Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
