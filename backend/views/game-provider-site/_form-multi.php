<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\GameCategory;
use common\models\GameProvider;
/* @var $this yii\web\View */
/* @var $site common\models\Site|null */
/* @var $model common\models\GameProviderSite */
/* @var $form yii\widgets\ActiveForm */
/* @var $back null|string */

$action = ['/game-provider-site/create-multi-game-providers'];
if (isset($back)) {
    $action['back'] = $back;
}
/* @var $site_game_provider common\models\GameProvider[]|null */
$site_game_provider = GameProvider::find()->where(['not in', 'id', !empty($site) ? ArrayHelper::getColumn($site->gameProviders, 'id') : []])->all()
?>

<div class="site-payment-form">

    <?php $form = ActiveForm::begin(['action' => $action]); ?>

    <?= $form->field($model, 'site_id')->dropDownList(ArrayHelper::map(\common\models\Site::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'game_provider_ids[]')->widget(\kartik\select2\Select2::className(),[
        'data' => ArrayHelper::map($site_game_provider, 'id', 'name'),
        'options' => ['placeholder' => 'Select Providers...', 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Add', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
