<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\GameProviderSite */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Game Provider Site',
]) . $model->game_provider_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Game Provider Sites'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->game_provider_id, 'url' => ['view', 'game_provider_id' => $model->game_provider_id, 'site_id' => $model->site_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="game-provider-site-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
