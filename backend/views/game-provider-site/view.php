<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\GameProviderSite */

$this->title = $model->game_provider_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Game Provider Sites'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-provider-site-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'game_provider_id' => $model->game_provider_id, 'site_id' => $model->site_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'game_provider_id' => $model->game_provider_id, 'site_id' => $model->site_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'game_provider_id',
            'site_id',
            'is_auto_created',
        ],
    ]) ?>

</div>
