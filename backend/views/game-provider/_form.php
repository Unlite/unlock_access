<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\GameProvider */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="game-provider-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput() ?>

    <?= $form->field($model, 'img_src')->textInput() ?>

	<?= $form->field($model, 'status')->dropDownList(array_combine($model::statuses(), $model::statuses())) ?>

    <?= $form->field($model, 'description')->textarea() ?>
    <?= $form->field($model, 'short_description')->textarea() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
