<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\GameProviderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Game Providers');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-provider-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Game Provider'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
			[
				'attribute' => 'img_src',
				'value' => function (\common\models\GameProvider $model, $key, $index, $column) {
					return \yii\bootstrap\Html::img($model->img_src, ['class' => 'img-circle', 'width' => '75px', 'height' => '75px']);
				},
				'format' => 'Html',
			],
            [
                'attribute'=>'description',
				'value' => function ($model) {
					return StringHelper::truncate($model->description, 50);
				}
            ],
            [
                'attribute'=>'short_description',
				'value' => function ($model) {
					return StringHelper::truncate($model->short_description, 50);
				}
            ],
			[
				'attribute' => 'status',
				'filter' => array_combine($searchModel::statuses(),$searchModel::statuses())
			],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
