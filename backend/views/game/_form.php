<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Game */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="game-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput() ?>

    <?= $form->field($model, 'slug')->textInput()->hint(\common\widgets\inflector\Slug::widget(['from' => '#game-name', 'to' => '#game-slug', 'confirm' => !$model->isNewRecord])) ?>

    <?= $form->field($model, 'img_src')->textInput() ?>
    <?= $form->field($model, 'game_provider_id')->dropDownList(ArrayHelper::merge([''=>Yii::t('app','Select provider')],ArrayHelper::map(\common\models\GameProvider::find()->all(),'id','name'))) ?>
	<?= $form->field($model, 'embedded_url')->textInput() ?>
    <?= $form->field($model, 'description')->textarea() ?>
	<?= $form->field($model, 'page_title')->textInput() ?>
	<?= $form->field($model, 'page_description')->textInput() ?>
	<?= $form->field($model, 'embedded_width')->textInput() ?>
	<?= $form->field($model, 'embedded_height')->textInput() ?>

	<?= $form->field($model, 'status')->dropDownList(array_combine($model::statuses(), $model::statuses())) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
