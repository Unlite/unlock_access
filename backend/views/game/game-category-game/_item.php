<?php

use yii\bootstrap\Html;

/* @var $model common\models\GameCategoryGame */

?>

<span>
    <?= $model->gameCategory->name ?>
    <?= Html::a(Html::icon('trash'), ['/game-category-game/delete', 'game_id' => $model->game_id, 'game_category_id' => $model->game_category_id, 'back' => \yii\helpers\Url::to()], [
        'class' => 'btn btn-xs btn-danger',
        'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
        ],
    ]) ?>
</span>
