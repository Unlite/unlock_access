<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\GameSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Games';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Game', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'name',
                'value' => function (\common\models\Game $model, $key, $index, $column) {
                    return \yii\bootstrap\Html::a($model->name, ['/game/view', 'id' => $model->id]);
                },
                'format' => 'Html',

            ],
            [
                'attribute' => 'game_provider_id',
                'value' => function (\common\models\Game $model, $key, $index, $column) {
                    return \yii\bootstrap\Html::a($model->gameProvider->name, ['/game-provider/view', 'id' => $model->gameProvider->id]);
                },
                'format' => 'Html',

            ],
            [
                'attribute' => 'img_src',
                'value' => function (\common\models\Game $model, $key, $index, $column) {
                    return \yii\bootstrap\Html::img($model->img_src, ['class' => 'img-circle', 'width' => '75px', 'height' => '75px']);
                },
                'format' => 'Html',
            ],
            'description',
            'embedded_url',
			[
				'attribute' => 'status',
				'filter' => array_combine($searchModel::statuses(),$searchModel::statuses())
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
