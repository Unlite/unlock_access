<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model common\models\Game */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Games', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'slug',
            [
                'attribute' => 'img_src',
                'value' => function (\common\models\Game $model, $widget) {
                    return \yii\bootstrap\Html::img($model->img_src, ['class' => 'img-circle', 'width' => '75px', 'height' => '75px']);
                },
                'format' => 'Html',
            ],
			[
				'attribute' => 'game_provider_id',
				'value' => function (\common\models\Game $model) {
                    return $model->gameProvider ? \yii\bootstrap\Html::a($model->gameProvider->name, ['/game-provider/view', 'id' => $model->gameProvider->id]) : null;
				},
				'format' => 'Html',

			],
			'description',
			'embedded_url',
			[
				'label' => 'Categories',
				'value' => function (\common\models\Game $model, $widget) {
					$linkDataProvider = new \yii\data\ActiveDataProvider([
						'query' => $model->getGameCategoryGames(),
						'pagination' => ['pageSize' => 50],

					]);
					return ListView::widget([
						'dataProvider' => $linkDataProvider,
						'itemView' => 'game-category-game/_item',
						'emptyText' => false,
						'summary' => '',
					]);
				},
				'format' => 'raw',
			],
			'embedded_width',
			'embedded_height',
			'page_title',
			'page_description',
			'status'
        ],
    ]) ?>

	<?php Modal::begin([
		'header' => '<h2>Add Category</h2>',
		'toggleButton' => ['label' => 'Add Category', 'class' => 'btn btn-success'],
	]);

	echo $this->render('/game-category-game/_form-multi', [
		'game' => $model,
		'is_allowed' => false,
		'model' => new \backend\models\forms\GameCategoryGameMultiForm(['game_id' => $model->id]),
		'back' => \yii\helpers\Url::to()
	]);

	Modal::end();
	?>

</div>
