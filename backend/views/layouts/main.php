<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use backend\helpers\AccessHelper;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'My Company',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Home', 'url' => ['/main/index']],
        ['label' => 'Settings', 'items' => [
            ['label' => 'Tabs', 'url' => ['/settings/tabs'], 'visible' => AccessHelper::routeAccess('/settings/portal')],
            ['label' => 'List', 'url' => ['/settings/index'], 'visible' => AccessHelper::routeAccess('/settings/index')],
            ['label' => 'Translate', 'url' => ['/translate/index'], 'visible' => AccessHelper::routeAccess('/translate/index')],
        ], 'visible' => AccessHelper::routeAccess('/settings/index') || AccessHelper::routeAccess('/settings/portal')],
        ['label' => 'Cashback', 'items' => [
			['label' => 'Cashback', 'url' => ['/cashback/index'], 'visible' => AccessHelper::routeAccess('/cashback/index')],
			['label' => 'Clients', 'url' => ['/client/index'], 'visible' => AccessHelper::routeAccess('/client/index')],
			['label' => 'Transfers', 'url' => ['/transfer/index'], 'visible' => AccessHelper::routeAccess('/transfer/index')],

		]],
        ['label' => 'Sites', 'items' => [
            ['label' => 'Sites', 'url' => ['/site/index'], 'visible' => AccessHelper::routeAccess('/site/index')],
            ['label' => 'Domains', 'url' => ['/domain/index'], 'visible' => AccessHelper::routeAccess('/domain/index')],
            ['label' => 'Links', 'url' => ['/link/index'], 'visible' => AccessHelper::routeAccess('/link/index')],
        ], 'visible' => AccessHelper::routeAccess('/site/index') || AccessHelper::routeAccess('/domain/index') || AccessHelper::routeAccess('/link/index')],
        ['label' => 'Casino', 'items' => [
            ['label' => 'Games', 'url' => ['/game/index'], 'visible' => AccessHelper::routeAccess('/game/index')],
            ['label' => 'Game Providers', 'url' => ['/game-provider/index'], 'visible' => AccessHelper::routeAccess('/game-provider/index')],
            ['label' => 'Game Categories', 'url' => ['/game-category/index'], 'visible' => AccessHelper::routeAccess('/game-category/index')],
            ['label' => 'Bonuses', 'url' => ['/bonuses/index'], 'visible' => AccessHelper::routeAccess('/bonuses/index')],
            ['label' => 'Bonus types', 'url' => ['/bonus-type/index'], 'visible' => AccessHelper::routeAccess('/bonus-type/index')],
            ['label' => 'Offer types', 'url' => ['/offer-type/index'], 'visible' => AccessHelper::routeAccess('/offer-type/index')],
            ['label' => 'Soft', 'url' => ['/soft/index'], 'visible' => AccessHelper::routeAccess('/soft/index')],
            ['label' => 'Payments', 'url' => ['/payment/index'], 'visible' => AccessHelper::routeAccess('/payment/index')],
            ['label' => 'Custom Lists', 'url' => ['/custom-list/index'], 'visible' => AccessHelper::routeAccess('/custom-list/index')],
            ['label' => 'Blog', 'url' => ['/blog-post/index'], 'visible' => AccessHelper::routeAccess('/blog-post/index')],
            ['label' => 'Tags', 'url' => ['/tag/index'], 'visible' => AccessHelper::routeAccess('/tag/index')],
            ['label' => 'Blog Categories', 'url' => ['/blog-post-category/index'], 'visible' => AccessHelper::routeAccess('/blog-post-category/index')],
        ], 'visible' => AccessHelper::routeAccess('/game/index') || AccessHelper::routeAccess('/soft/index') || AccessHelper::routeAccess('/payment/index')],
        ['label' => 'Proxies', 'url' => ['/proxy/index'], 'visible' => AccessHelper::routeAccess('/proxy/index')],
		['label' => 'Localisation', 'items' => [
            ['label' => 'Geo', 'url' => ['/geo/index'], 'visible' => AccessHelper::routeAccess('/geo/index')],
            ['label' => 'Currency', 'url' => ['/currency/index'], 'visible' => AccessHelper::routeAccess('/currency/index')],
            ['label' => 'locale', 'url' => ['/locale/index'], 'visible' => AccessHelper::routeAccess('/locale/index')],
            ['label' => 'licensor', 'url' => ['/licensor/index'], 'visible' => AccessHelper::routeAccess('/licensor/index')],
        ], 'visible' => AccessHelper::routeAccess('/geo/index')
            || AccessHelper::routeAccess('/currency/index')
            || AccessHelper::routeAccess('/locale/index')
            || AccessHelper::routeAccess('/licensor/index')],
        ['label' => 'RBAC', 'items' => [
            ['label' => 'Assignments', 'url' => ['/rbac/assignment'], 'visible' => AccessHelper::routeAccess('/rbac/assignment')],
            ['label' => 'Roles', 'url' => ['/rbac/role'], 'visible' => AccessHelper::routeAccess('/rbac/role')],
            ['label' => 'Rules', 'url' => ['/rbac/rule'], 'visible' => AccessHelper::routeAccess('/rbac/rule')],
            ['label' => 'Routes', 'url' => ['/rbac/route'], 'visible' => AccessHelper::routeAccess('/rbac/route')],
            ['label' => 'Permissions', 'url' => ['/rbac/permission'], 'visible' => AccessHelper::routeAccess('/rbac/permission')],
        ], 'visible' => AccessHelper::routeAccess('/rbac/assignment')
            || AccessHelper::routeAccess('/rbac/role')
            || AccessHelper::routeAccess('/rbac/rule')
            || AccessHelper::routeAccess('/rbac/route')
            || AccessHelper::routeAccess('/rbac/permission')],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Login', 'url' => ['/main/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/main/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
