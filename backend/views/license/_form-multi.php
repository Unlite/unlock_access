<?php

use common\models\Geo;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $site common\models\Site|null */
/* @var $model common\models\License */
/* @var $form yii\widgets\ActiveForm */
/* @var $back null|string */

$action = ['/license/create-multi-geo'];
if (isset($back)) {
    $action['back'] = $back;
}
/* @var $geos common\models\Geo[]|null */
$geos = Geo::find()->where(['not in', 'code', !empty($site) ? ArrayHelper::getColumn($site->licenseGeos, 'code') : []])->all();
$data = ['SIC' => [], 'WORLD' => []];
foreach ($geos as $geo) {
    if (in_array($geo->code, ['ru', 'ua', 'by', 'kz', 'am', 'az', 'kg', 'md', 'tj', 'tm', 'uz'])) {
        $data['SIC'][$geo->code] = $geo->name;
    }else{
        $data['WORLD'][$geo->code] = $geo->name;
    }
}
?>

<div class="license-form">

    <?php $form = ActiveForm::begin(['action' => $action]); ?>

    <?= $form->field($model, 'site_id')->dropDownList(ArrayHelper::map(\common\models\Site::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'geo_codes[]')->widget(\kartik\select2\Select2::className(),[
        'data' => $data,
        'options' => ['placeholder' => 'Select Games...', 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Add', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
