<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\LicenseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Licenses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="license-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create License', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Create Site Geo Multi', ['create-multi-geo'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Site',
                'attribute' => 'site_id',
                'value' => function (\common\models\License $model, $key, $index, $column) {
                    return \yii\bootstrap\Html::a($model->site->name, ['/site/view', 'id' => $model->site->id]);
                },
                'format' => 'Html',
                'filter' => Html::activeDropDownList($searchModel, 'site_id', \yii\helpers\ArrayHelper::map(\common\models\Site::find()->all(), 'id', 'name'), ['class' => 'form-control', 'prompt' => ''])
            ],
            [
                'label' => 'Geo',
                'attribute' => 'geo_code',
                'value' => function (\common\models\License $model, $key, $index, $column) {
                    return \yii\bootstrap\Html::a($model->geo->name, ['/geo/view', 'id' => $model->geo->code]);
                },
                'format' => 'Html',
                'filter' => Html::activeDropDownList($searchModel, 'geo_code', \yii\helpers\ArrayHelper::map(\common\models\Geo::find()->all(), 'code', 'name'), ['class' => 'form-control', 'prompt' => ''])
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
