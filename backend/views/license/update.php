<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\License */
/* @var $back null|string */

$this->title = 'Update License: ' . $model->site_id;
$this->params['breadcrumbs'][] = ['label' => 'Licenses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->site_id, 'url' => ['view', 'site_id' => $model->site_id, 'geo_code' => $model->geo_code]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="license-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'back' => $back,
    ]) ?>

</div>
