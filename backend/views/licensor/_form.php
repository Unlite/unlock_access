<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\licensor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="licensor-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'geo_code')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Geo::find()->all(),'code','name')) ?>

    <?= $form->field($model, 'name')->textInput() ?>

	<?= $form->field($model, 'status')->dropDownList(array_combine($model::statuses(), $model::statuses())) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
