<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\licensor */

$this->title = Yii::t('app', 'Create Licensor');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Licensors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="licensor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
