<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\LinkWildcard */
/* @var $form yii\widgets\ActiveForm */
/* @var $back null|string */

$action = ['/link-wildcard/' . ($model->isNewRecord ? 'create' : 'update')];
if (!$model->isNewRecord) {
    $action['id'] = $model->id;
}
if (isset($back)) {
    $action['back'] = $back;
}
?>

<div class="link-wildcard-form">

    <?php $form = ActiveForm::begin(['action' => $action]); ?>

    <?= $form->field($model, 'link_id')->textInput() ?>

    <?= $form->field($model, 'pattern')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
