<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\LinkWildcard */
/* @var $back null|string */

$this->title = 'Create Link Wildcard';
$this->params['breadcrumbs'][] = ['label' => 'Link Wildcards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="link-wildcard-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'back' => $back,
    ]) ?>

</div>
