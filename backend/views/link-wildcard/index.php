<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\LinkWildcardSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Link Wildcards';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="link-wildcard-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Link Wildcard', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'link_id',
            'pattern',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
