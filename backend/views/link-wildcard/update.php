<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\LinkWildcard */
/* @var $back null|string */

$this->title = 'Update Link Wildcard: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Link Wildcards', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="link-wildcard-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'back' => $back,
    ]) ?>

</div>
