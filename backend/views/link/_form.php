<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Link */
/* @var $form yii\widgets\ActiveForm */
/* @var $back null|string */

$action = ['/link/' . ($model->isNewRecord ? 'create' : 'update')];
if (!$model->isNewRecord) {
    $action['id'] = $model->id;
}
if (isset($back)) {
    $action['back'] = $back;
}
?>

<div class="link-form">

    <?php $form = ActiveForm::begin(['action' => $action]); ?>

    <?= $form->field($model, 'domain_id')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Domain::find()->all(), 'id', 'domain')) ?>

    <?= $form->field($model, 'redirect_url')->textInput()->hint(\common\modules\away\widgets\MacrosListWidget::widget()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
