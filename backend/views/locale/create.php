<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\locale */

$this->title = Yii::t('app', 'Create Locale');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Locales'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="locale-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
