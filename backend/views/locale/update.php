<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\locale */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Locale',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Locales'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->code]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="locale-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
