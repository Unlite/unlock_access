<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\OfferTypeBonuses */
/* @var $form yii\widgets\ActiveForm */
$action = ['/offer-type-bonuses/' . ($model->isNewRecord ? 'create' : 'update')];
if (!$model->isNewRecord) {
	$action['bonuses_id'] = $model->bonuses_id;
}
if (isset($back)) {
	$action['back'] = $back;
}
?>

<div class="offer-type-bonuses-form">

    <?php $form = ActiveForm::begin(['action' => $action]); ?>

    <?= $form->field($model, 'offer_type_id')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\OfferType::find()->all(),'id','name')) ?>
    <?= $form->field($model, 'bonuses_id')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Bonuses::find()->all(),'id','text')) ?>

    <?= $form->field($model, 'offer_text')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
