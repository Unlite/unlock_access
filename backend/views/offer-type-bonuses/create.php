<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\OfferTypeBonuses */

$this->title = Yii::t('app', 'Create Offer Type Bonuses');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Offer Type Bonuses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="offer-type-bonuses-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
