<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OfferTypeBonuses */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Offer Type Bonuses',
]) . $model->offer_type_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Offer Type Bonuses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->offer_type_id, 'url' => ['view', 'offer_type_id' => $model->offer_type_id, 'bonuses_id' => $model->bonuses_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="offer-type-bonuses-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
