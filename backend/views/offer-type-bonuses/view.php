<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\OfferTypeBonuses */

$this->title = $model->offer_type_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Offer Type Bonuses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="offer-type-bonuses-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'offer_type_id' => $model->offer_type_id, 'bonuses_id' => $model->bonuses_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'offer_type_id' => $model->offer_type_id, 'bonuses_id' => $model->bonuses_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'offer_type_id',
            'bonuses_id',
            'offer_text',
        ],
    ]) ?>

</div>
