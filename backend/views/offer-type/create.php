<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\OfferType */

$this->title = Yii::t('app', 'Create Offer Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Offer Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="offer-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
