<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $paymentSystem common\models\PaymentSystem|null */
/* @var $model common\models\PaymentSystemCurrency */
/* @var $form yii\widgets\ActiveForm */
/* @var $back null|string */

$action = ['/payment-system-currency/create-multi-currencies'];
if (isset($back)) {
    $action['back'] = $back;
}
/* @var $currencies common\models\Currency[]|null */
$currencies = \common\models\Currency::find()->where(['not in', 'code', !empty($paymentSystem) ? ArrayHelper::getColumn($paymentSystem->currencies, 'code') : []])->all()
?>

<div class="site-payment-form">

    <?php $form = ActiveForm::begin(['action' => $action]); ?>

    <?= $form->field($model, 'payment_system_id')->dropDownList(ArrayHelper::map(\common\models\PaymentSystem::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'currency_codes[]')->widget(\kartik\select2\Select2::className(),[
        'data' => ArrayHelper::map($currencies, 'code', 'name'),
        'options' => ['placeholder' => 'Select currencies...', 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Add', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
