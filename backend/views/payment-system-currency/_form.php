<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PaymentSystemCurrency */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-system-currency-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'payment_system_id')->textInput() ?>

    <?= $form->field($model, 'currency_code')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
