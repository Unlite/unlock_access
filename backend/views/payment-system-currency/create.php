<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PaymentSystemCurrency */
/* @var $back null|string */
/* @var $multi bool */

$this->title = Yii::t('app', 'Create Payment System Currency');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Payment System Currencies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-system-currency-create">

    <h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render($multi ? '_form-multi' : '_form', [
		'model' => $model,
		'back' => $back,
	]) ?>

</div>
