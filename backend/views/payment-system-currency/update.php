<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PaymentSystemCurrency */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Payment System Currency',
]) . $model->payment_system_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Payment System Currencies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->payment_system_id, 'url' => ['view', 'payment_system_id' => $model->payment_system_id, 'currency_code' => $model->currency_code]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="payment-system-currency-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
