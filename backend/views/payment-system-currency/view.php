<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PaymentSystemCurrency */

$this->title = $model->payment_system_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Payment System Currencies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-system-currency-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'payment_system_id' => $model->payment_system_id, 'currency_code' => $model->currency_code], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'payment_system_id' => $model->payment_system_id, 'currency_code' => $model->currency_code], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'payment_system_id',
            'currency_code',
        ],
    ]) ?>

</div>
