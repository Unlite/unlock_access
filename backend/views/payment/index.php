<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PaymentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create PaymentSystem', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'name',
                'value' => function (\common\models\PaymentSystem $model, $key, $index, $column) {
                    return \yii\bootstrap\Html::a($model->name, ['/payment/view', 'id' => $model->id]);
                },
                'format' => 'Html',

            ],
            [
                'attribute' => 'img_src',
                'value' => function (\common\models\PaymentSystem $model, $key, $index, $column) {
                    return \yii\bootstrap\Html::img($model->img_src);
                },
                'format' => 'Html',
            ],
            'description',
            [
                'attribute' => 'status',
                'filter' => array_combine(\common\models\PaymentSystem::statuses(), \common\models\PaymentSystem::statuses())
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
