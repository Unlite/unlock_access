<?php

use yii\bootstrap\Html;

/* @var $model common\models\PaymentSystemCurrency */

?>

<span>
    <?= $model->currency->name ?>
    <?= Html::a(Html::icon('trash'), ['/payment-system-currency/delete', 'payment_system_id' => $model->payment_system_id, 'currency_code' => $model->currency_code, 'back' => \yii\helpers\Url::to()], [
        'class' => 'btn btn-xs btn-danger',
        'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
        ],
    ]) ?>
</span>
