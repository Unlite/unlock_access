<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model common\models\PaymentSystem */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'attribute' => 'img_src',
                'value' => function (\common\models\PaymentSystem $model, $widget) {
                    return \yii\bootstrap\Html::img($model->img_src);
                },
                'format' => 'Html',
            ],
			'description',
			'status',
			[
				'label' => 'Currencies',
				'value' => function (\common\models\PaymentSystem $model, $widget) {
					$linkDataProvider = new \yii\data\ActiveDataProvider([
						'query' => $model->getPaymentCurrency(),
						'pagination' => [
							'pageSize' => 50,
						],
					]);
					return ListView::widget([
						'dataProvider' => $linkDataProvider,
						'itemView' => 'payment-system-currency/_item',
						'emptyText' => false,
						'summary' => '',
					]);
				},
				'format' => 'raw',
			],
			[
				'label' => 'Connected to sites',
				'value' => function (\common\models\PaymentSystem $model, $widget) {
					$linkDataProvider = new \yii\data\ActiveDataProvider([
						'query' => $model->getSitePayments(),
						'pagination' => [
							'pageSize' => 50,
						],
					]);
					return ListView::widget([
						'dataProvider' => $linkDataProvider,
						'itemView' => 'site-payment/_item',
						'emptyText' => false,
						'summary' => '',
					]);
				},
				'format' => 'raw',
			],
        ],
    ]) ?>

	<?php Modal::begin([
		'header' => '<h2>Add Currency</h2>',
		'toggleButton' => ['label' => 'Add Currency', 'class' => 'btn btn-success'],
	]);

	echo $this->render('/payment-system-currency/_form-multi', [
		'paymentSystem' => $model,
		'model' => new \backend\models\forms\PaymentSystemCurrencyMultiForm(['payment_system_id' => $model->id]),
		'back' => \yii\helpers\Url::to()
	]);

	Modal::end();
	?>

</div>
