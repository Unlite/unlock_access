<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Proxy */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proxy-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'geo_code')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Geo::find()->all(), 'code', 'name')) ?>

    <?= $form->field($model, 'host')->textInput() ?>

    <?= $form->field($model, 'port')->textInput() ?>

    <?= $form->field($model, 'login')->textInput() ?>

    <?= $form->field($model, 'password')->textInput() ?>

    <?= $form->field($model, 'type')->dropDownList(array_combine(\common\models\Proxy::types(), \common\models\Proxy::types())) ?>

    <?= $form->field($model, 'status')->dropDownList(array_combine(\common\models\Proxy::statuses(), \common\models\Proxy::statuses())) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
