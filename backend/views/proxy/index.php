<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProxySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Proxies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proxy-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Proxy', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Proxy',
                'value' => function (\common\models\Proxy $model, $key, $index, $column) {
                    return \yii\bootstrap\Html::a($model->fullAddress, ['/proxy/view', 'id' => $model->id]);
                },
                'format' => 'Html',

            ],
            [
                'label' => 'Geo',
                'attribute' => 'geo_code',
                'value' => function (\common\models\Proxy $model, $key, $index, $column) {
                    return \yii\bootstrap\Html::a($model->geo->name, ['/geo/view', 'id' => $model->geo->code]);
                },
                'format' => 'Html',
                'filter' => Html::activeDropDownList($searchModel, 'geo_code', \yii\helpers\ArrayHelper::map(\common\models\Geo::find()->all(), 'code', 'name'), ['class' => 'form-control', 'prompt' => ''])
            ],

			[
				'attribute' => 'status',
				'filter' => Html::activeDropDownList($searchModel, 'status', array_combine(\common\models\Proxy::statuses(), \common\models\Proxy::statuses()), ['class' => 'form-control', 'prompt' => ''])
			],
            'host',
            'port',
            'login',
            'password',
            [
                'label' => 'Type',
                'attribute' => 'type',
                'format' => 'Html',
                'filter' => Html::activeDropDownList($searchModel, 'type', array_combine(\common\models\Proxy::types(), \common\models\Proxy::types()), ['class' => 'form-control', 'prompt' => ''])
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
