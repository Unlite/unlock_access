<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Review */
/* @var $form yii\widgets\ActiveForm */
/* @var $back null|string */

$action = ['/review/' . ($model->isNewRecord ? 'create' : 'update')];
if (!$model->isNewRecord) {
    $action['id'] = $model->id;
}
if (isset($back)) {
    $action['back'] = $back;
}
?>

<div class="review-form">

    <?php $form = ActiveForm::begin(['action' => $action]); ?>

    <?= $form->field($model, 'site_id')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Site::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'author')->textInput() ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'rating')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
