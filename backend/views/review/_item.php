<?php


/* @var $model common\models\Review */
use yii\bootstrap\Html;
use yii\widgets\DetailView;

?>
<div class="site-translation">
    <div class="panel panel-primary">
        <div class="panel-heading"><?= Html::encode($model->id . ' | ' . $model->author) ?></div>
        <div class="panel-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'site_id',
                    'author',
                    'text:ntext',
                    'rating',
                    'type',
                ],
            ]) ?>
        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-md-6">
                </div>
                <div class="col-md-6 text-right">
                    <?= Html::a(Html::icon('eye-open'), ['/review/view', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']); ?>
                    <?= Html::a(Html::icon('pencil'), ['/review/update', 'id' => $model->id, 'back' => \yii\helpers\Url::to()], ['class' => 'btn btn-sm btn-primary']); ?>
                    <?= Html::a(Html::icon('trash'), ['/review/delete', 'id' => $model->id, 'back' => \yii\helpers\Url::to()], [
                        'class' => 'btn btn-sm btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>

