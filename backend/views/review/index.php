<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ReviewSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reviews';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Review', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'label' => 'Site',
                'attribute' => 'site_id',
                'value' => function (\common\models\Review $model, $key, $index, $column) {
                    return \yii\bootstrap\Html::a($model->site->name, ['/site/view', 'id' => $model->site->id]);
                },
                'format' => 'Html',
            ],
            [
                'label' => 'User',
                'value' => function (\common\models\Review $model, $key, $index, $column) {
                    //return \yii\bootstrap\Html::a($model->user->username, ['/user/view', 'id' => $model->site->id]);
                    return Html::encode($model->user ? $model->user->username : '-');
                },
                'format' => 'Html',
            ],
            'author',
            'text:ntext',
            'rating',
            // 'type',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
