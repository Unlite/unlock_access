<?php
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use yii\bootstrap\Tabs;

/* @var $model \backend\models\forms\SettingsMultiForm
 * @var $form \yii\widgets\ActiveForm
 * @var $attribute string
 * @var $locales[] \common\models\Locale
 * @var $settings  \common\components\Portal;
 */
$this->registerCss('.settings-update .navbar-collapse {padding:0;}
.form-group .nav > li {
    flex: 1;
    text-align: center;
}
.form-group ul.nav.nav-tabs {
justify-content: space-around;
    display: flex;
}
.form-group .nav > li > a {
    padding:5px 0 5px 0;
}
');
$menuItems = [];
foreach ($locales as $locale) {
	$menuItems[] = [
        'label' => $locale->code,
        'content' => $form->field($model,'settings['.$attribute.'][value]['.$locale->code.']')->textInput(['value'=>$settings->get($attribute.'.data.'.$locale->code),'placeholder'=>$locale->name])];
}
NavBar::begin([
	'options' => [
		'class' => 'nav navbar-default',
		'style' => 'border-radius:5px 5px 0 0 ;margin-bottom: 0px;border:0'
	],
	'innerContainerOptions' => ['class' => '']
]);
echo Tabs::widget([
        'items' => $menuItems
        ]);
NavBar::end();