<?php

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

/* @var $this yii\web\View */
/* @var $model common\models\Site */

$this->registerCss('.settings-update .navbar-collapse {padding:0;}');
NavBar::begin([
    'options' => [
        'class' => 'nav navbar-default',
		'style' => 'border-radius:5px 5px 0 0 ;margin-bottom: 0px;border:0'
    ],
	'innerContainerOptions' => ['class' => '']
]);
$menuItems = [
    ['label' => 'Filter Tabs', 'url' => ['/settings/tabs']],
    ['label' => 'Page attributes', 'url' => ['/settings/page-attributes']],
];
echo Nav::widget([
    'options' => ['class' => 'nav navbar-nav navbar-left'],
    'items' => $menuItems,
]);
NavBar::end();