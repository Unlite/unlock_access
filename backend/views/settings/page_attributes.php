<?php

use yii\helpers\Html;
use common\models\CustomList;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\models\Locale;
/* @var $this yii\web\View */
/* @var $model \backend\models\forms\SettingsMultiForm */
/* @var $settings \common\components\Portal */

$this->title = Yii::t('app', 'Update Portal Settings');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Settings'), 'url' => ['index']];
$this->registerCss('.settings-update .help-block{margin:0}');
$locales = Locale::find()->all();
?>
<div class="settings-update">
	<?php $form = ActiveForm::begin([
		'enableClientValidation'=>false,
		'fieldConfig' => [
			'template' => "{input}\n{error}",
			'options' => [
				'tag' => false,
			],
		]
	]); ?>
    <div class="panel panel-default">
		<?=$this->render('_tabs')?>
        <div class="panel-body">
            <h3><?=Yii::t('app','Main pages attributes')?></h3>
            <div class="form-group">
                <?=Html::label(Yii::t('app','Home title')); ?>
                <?=$form->field($model,'settings[page_attribute_home_title][id]')->hiddenInput(['class'=>'form-control','value'=>$settings->get('page_attribute_home_title.id')])?>
                <?=$form->field($model,'settings[page_attribute_home_title][key]')->hiddenInput(['value'=>'page_attribute_home_title'])?>
                <?=$form->field($model,'settings[page_attribute_home_title][group]')->hiddenInput(['value'=>'portal'])?>
                <?=$this->render('_page_attribute',['attribute' => 'page_attribute_home_title','locales' => $locales,'form'=>$form,'model' => $model,'settings' => $settings])?>
                <?=$form->field($model,'settings[page_attribute_home_title][serialized]')->hiddenInput(['value'=>1])?>
            </div>
            <div class="form-group">
                <?=Html::label(Yii::t('app','Home description')); ?>
                <?=$form->field($model,'settings[page_attribute_home_description][id]')->hiddenInput(['class'=>'form-control','value'=>$settings->get('page_attribute_home_description.id')])?>
                <?=$form->field($model,'settings[page_attribute_home_description][key]')->hiddenInput(['value'=>'page_attribute_home_description'])?>
                <?=$form->field($model,'settings[page_attribute_home_description][group]')->hiddenInput(['value'=>'portal'])?>
                <?=$this->render('_page_attribute',['attribute' => 'page_attribute_home_description','locales' => $locales,'form'=>$form,'model' => $model,'settings' => $settings])?>
                <?=$form->field($model,'settings[page_attribute_home_description][serialized]')->hiddenInput(['value'=>1])?>
            </div>
            <div class="form-group">
                <?=Html::label(Yii::t('app','Casinos title')); ?>
                <?=$form->field($model,'settings[page_attribute_casinos_title][id]')->hiddenInput(['class'=>'form-control','value'=>$settings->get('page_attribute_casinos_title.id')])?>
                <?=$form->field($model,'settings[page_attribute_casinos_title][key]')->hiddenInput(['value'=>'page_attribute_casinos_title'])?>
                <?=$form->field($model,'settings[page_attribute_casinos_title][group]')->hiddenInput(['value'=>'portal'])?>
                <?=$this->render('_page_attribute',['attribute' => 'page_attribute_casinos_title','locales' => $locales,'form'=>$form,'model' => $model,'settings' => $settings])?>
                <?=$form->field($model,'settings[page_attribute_casinos_title][serialized]')->hiddenInput(['value'=>1])?>
            </div>
            <div class="form-group">
                <?=Html::label(Yii::t('app','Casinos description')); ?>
                <?=$form->field($model,'settings[page_attribute_casinos_description][id]')->hiddenInput(['class'=>'form-control','value'=>$settings->get('page_attribute_casinos_description.id')])?>
                <?=$form->field($model,'settings[page_attribute_casinos_description][key]')->hiddenInput(['value'=>'page_attribute_casinos_description'])?>
                <?=$form->field($model,'settings[page_attribute_casinos_description][group]')->hiddenInput(['value'=>'portal'])?>
                <?=$this->render('_page_attribute',['attribute' => 'page_attribute_casinos_description','locales' => $locales,'form'=>$form,'model' => $model,'settings' => $settings])?>
                <?=$form->field($model,'settings[page_attribute_casinos_description][serialized]')->hiddenInput(['value'=>1])?>
            </div>
            <div class="form-group">
                <?=Html::label(Yii::t('app','Games title')); ?>
                <?=$form->field($model,'settings[page_attribute_games_title][id]')->hiddenInput(['class'=>'form-control','value'=>$settings->get('page_attribute_games_title.id')])?>
                <?=$form->field($model,'settings[page_attribute_games_title][key]')->hiddenInput(['value'=>'page_attribute_games_title'])?>
                <?=$form->field($model,'settings[page_attribute_games_title][group]')->hiddenInput(['value'=>'portal'])?>
                <?=$this->render('_page_attribute',['attribute' => 'page_attribute_games_title','locales' => $locales,'form'=>$form,'model' => $model,'settings' => $settings])?>
                <?=$form->field($model,'settings[page_attribute_games_title][serialized]')->hiddenInput(['value'=>1])?>
            </div>
            <div class="form-group">
                <?=Html::label(Yii::t('app','Games description')); ?>
                <?=$form->field($model,'settings[page_attribute_games_description][id]')->hiddenInput(['class'=>'form-control','value'=>$settings->get('page_attribute_games_description.id')])?>
                <?=$form->field($model,'settings[page_attribute_games_description][key]')->hiddenInput(['value'=>'page_attribute_games_description'])?>
                <?=$form->field($model,'settings[page_attribute_games_description][group]')->hiddenInput(['value'=>'portal'])?>
                <?=$this->render('_page_attribute',['attribute' => 'page_attribute_games_description','locales' => $locales,'form'=>$form,'model' => $model,'settings' => $settings])?>
                <?=$form->field($model,'settings[page_attribute_games_description][serialized]')->hiddenInput(['value'=>1])?>
            </div>
            <div class="form-group">
                <?=Html::label(Yii::t('app','Bonuses title')); ?>
                <?=$form->field($model,'settings[page_attribute_bonuses_title][id]')->hiddenInput(['class'=>'form-control','value'=>$settings->get('page_attribute_bonuses_title.id')])?>
                <?=$form->field($model,'settings[page_attribute_bonuses_title][key]')->hiddenInput(['value'=>'page_attribute_bonuses_title'])?>
                <?=$form->field($model,'settings[page_attribute_bonuses_title][group]')->hiddenInput(['value'=>'portal'])?>
                <?=$this->render('_page_attribute',['attribute' => 'page_attribute_bonuses_title','locales' => $locales,'form'=>$form,'model' => $model,'settings' => $settings])?>
                <?=$form->field($model,'settings[page_attribute_bonuses_title][serialized]')->hiddenInput(['value'=>1])?>
            </div>
            <div class="form-group">
                <?=Html::label(Yii::t('app','Bonuses description')); ?>
                <?=$form->field($model,'settings[page_attribute_bonuses_description][id]')->hiddenInput(['class'=>'form-control','value'=>$settings->get('page_attribute_bonuses_description.id')])?>
                <?=$form->field($model,'settings[page_attribute_bonuses_description][key]')->hiddenInput(['value'=>'page_attribute_bonuses_description'])?>
                <?=$form->field($model,'settings[page_attribute_bonuses_description][group]')->hiddenInput(['value'=>'portal'])?>
                <?=$this->render('_page_attribute',['attribute' => 'page_attribute_bonuses_description','locales' => $locales,'form'=>$form,'model' => $model,'settings' => $settings])?>
                <?=$form->field($model,'settings[page_attribute_bonuses_description][serialized]')->hiddenInput(['value'=>1])?>
            </div>
            <div class="form-group">
                <?=Html::label(Yii::t('app','Tips&Guides title')); ?>
                <?=$form->field($model,'settings[page_attribute_tips_and_guides_title][id]')->hiddenInput(['class'=>'form-control','value'=>$settings->get('page_attribute_tips_and_guides_title.id')])?>
                <?=$form->field($model,'settings[page_attribute_tips_and_guides_title][key]')->hiddenInput(['value'=>'page_attribute_tips_and_guides_title'])?>
                <?=$form->field($model,'settings[page_attribute_tips_and_guides_title][group]')->hiddenInput(['value'=>'portal'])?>
                <?=$this->render('_page_attribute',['attribute' => 'page_attribute_tips_and_guides_title','locales' => $locales,'form'=>$form,'model' => $model,'settings' => $settings])?>
                <?=$form->field($model,'settings[page_attribute_tips_and_guides_title][serialized]')->hiddenInput(['value'=>1])?>
            </div>
            <div class="form-group">
                <?=Html::label(Yii::t('app','Tips&Guides description')); ?>
                <?=$form->field($model,'settings[page_attribute_tips_and_guides_description][id]')->hiddenInput(['class'=>'form-control','value'=>$settings->get('page_attribute_tips_and_guides_description.id')])?>
                <?=$form->field($model,'settings[page_attribute_tips_and_guides_description][key]')->hiddenInput(['value'=>'page_attribute_tips_and_guides_description'])?>
                <?=$form->field($model,'settings[page_attribute_tips_and_guides_description][group]')->hiddenInput(['value'=>'portal'])?>
                <?=$this->render('_page_attribute',['attribute' => 'page_attribute_tips_and_guides_description','locales' => $locales,'form'=>$form,'model' => $model,'settings' => $settings])?>
                <?=$form->field($model,'settings[page_attribute_tips_and_guides_description][serialized]')->hiddenInput(['value'=>1])?>
            </div>
            <div class="form-group">
                <?=Html::label(Yii::t('app','Cashback title')); ?>
                <?=$form->field($model,'settings[page_attribute_cashback_title][id]')->hiddenInput(['class'=>'form-control','value'=>$settings->get('page_attribute_cashback_title.id')])?>
                <?=$form->field($model,'settings[page_attribute_cashback_title][key]')->hiddenInput(['value'=>'page_attribute_cashback_title'])?>
                <?=$form->field($model,'settings[page_attribute_cashback_title][group]')->hiddenInput(['value'=>'portal'])?>
                <?=$this->render('_page_attribute',['attribute' => 'page_attribute_cashback_title','locales' => $locales,'form'=>$form,'model' => $model,'settings' => $settings])?>
                <?=$form->field($model,'settings[page_attribute_cashback_title][serialized]')->hiddenInput(['value'=>1])?>
            </div>
            <div class="form-group">
                <?=Html::label(Yii::t('app','Cashback description')); ?>
                <?=$form->field($model,'settings[page_attribute_cashback_description][id]')->hiddenInput(['class'=>'form-control','value'=>$settings->get('page_attribute_cashback_description.id')])?>
                <?=$form->field($model,'settings[page_attribute_cashback_description][key]')->hiddenInput(['value'=>'page_attribute_cashback_description'])?>
                <?=$form->field($model,'settings[page_attribute_cashback_description][group]')->hiddenInput(['value'=>'portal'])?>
                <?=$this->render('_page_attribute',['attribute' => 'page_attribute_cashback_description','locales' => $locales,'form'=>$form,'model' => $model,'settings' => $settings])?>
                <?=$form->field($model,'settings[page_attribute_cashback_description][serialized]')->hiddenInput(['value'=>1])?>
            </div>
            <div class="form-group">
                <?=Html::label(Yii::t('app','Search title')); ?>
                <?=$form->field($model,'settings[page_attribute_search_title][id]')->hiddenInput(['class'=>'form-control','value'=>$settings->get('page_attribute_search_title.id')])?>
                <?=$form->field($model,'settings[page_attribute_search_title][key]')->hiddenInput(['value'=>'page_attribute_search_title'])?>
                <?=$form->field($model,'settings[page_attribute_search_title][group]')->hiddenInput(['value'=>'portal'])?>
                <?=$this->render('_page_attribute',['attribute' => 'page_attribute_search_title','locales' => $locales,'form'=>$form,'model' => $model,'settings' => $settings])?>
                <?=$form->field($model,'settings[page_attribute_search_title][serialized]')->hiddenInput(['value'=>1])?>
            </div>
            <div class="form-group">
                <?=Html::label(Yii::t('app','Search description')); ?>
                <?=$form->field($model,'settings[page_attribute_search_description][id]')->hiddenInput(['class'=>'form-control','value'=>$settings->get('page_attribute_search_description.id')])?>
                <?=$form->field($model,'settings[page_attribute_search_description][key]')->hiddenInput(['value'=>'page_attribute_search_description'])?>
                <?=$form->field($model,'settings[page_attribute_search_description][group]')->hiddenInput(['value'=>'portal'])?>
                <?=$this->render('_page_attribute',['attribute' => 'page_attribute_search_description','locales' => $locales,'form'=>$form,'model' => $model,'settings' => $settings])?>
                <?=$form->field($model,'settings[page_attribute_search_description][serialized]')->hiddenInput(['value'=>1])?>
            </div>
        </div>


    </div>
    <div class="form-group">
		<?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-success']) ?>
    </div>
	<?php ActiveForm::end()?>

</div>