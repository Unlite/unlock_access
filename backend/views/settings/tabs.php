<?php

use common\models\CustomList;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\GameCategory;
use common\models\OfferType;

/* @var $this yii\web\View */
/* @var $model \backend\models\forms\SettingsMultiForm */
/* @var $settings \common\components\Portal */

$this->title = Yii::t('app', 'Update Portal Settings');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Settings'), 'url' => ['index']];
$this->registerCss('.settings-update .help-block{margin:0}')
?>
<div class="settings-update">
	<?php $form = ActiveForm::begin([
	        'enableClientValidation'=>false,
		'fieldConfig' => [
			'template' => "{input}\n{error}",
			'options' => [
				'tag' => false,
			],
		]
	]); ?>
    <div class="panel panel-default">
            <?=$this->render('_tabs')?>
        <div class="panel-body">

            <h3><?=Yii::t('app','Casinos fixed pages')?></h3>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
						<?=Html::label(Yii::t('app','Top 10')); ?>
                    </div>
                    <?=$form->field($model,'settings[top_10_custom_list][id]')->hiddenInput(['class'=>'form-control','value'=>$settings->get('top_10_custom_list.id')])?>
                    <?=$form->field($model,'settings[top_10_custom_list][key]')->hiddenInput(['value'=>'top_10_custom_list'])?>
                    <?=$form->field($model,'settings[top_10_custom_list][group]')->hiddenInput(['value'=>'portal'])?>
                    <?=$form->field($model,'settings[top_10_custom_list][value]')->dropDownList(ArrayHelper::map(CustomList::find()->all(),'id','name'),['value'=>$settings->get('top_10_custom_list.data')])?>
                    <?=$form->field($model,'settings[top_10_custom_list][serialized]')->hiddenInput(['value'=>0])?>
             </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
						<?=Html::label(Yii::t('app','Top Highrollers')); ?>
                    </div>
					<?=$form->field($model,'settings[top_highrollers_custom_list][id]')->hiddenInput(['class'=>'form-control','value'=>$settings->get('top_highrollers_custom_list.id')])?>
					<?=$form->field($model,'settings[top_highrollers_custom_list][key]')->hiddenInput(['value'=>'top_highrollers_custom_list'])?>
					<?=$form->field($model,'settings[top_highrollers_custom_list][group]')->hiddenInput(['value'=>'portal'])?>
					<?=$form->field($model,'settings[top_highrollers_custom_list][value]')->dropDownList(ArrayHelper::map(CustomList::find()->all(),'id','name'),['value'=>$settings->get('top_highrollers_custom_list.data')])?>
					<?=$form->field($model,'settings[top_highrollers_custom_list][serialized]')->hiddenInput(['value'=>0])?>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
						<?=Html::label(Yii::t('app','Mobile Casinos')); ?>
                    </div>
					<?=$form->field($model,'settings[mobile_casinos_custom_list][id]')->hiddenInput(['class'=>'form-control','value'=>$settings->get('mobile_casinos_custom_list.id')])?>
					<?=$form->field($model,'settings[mobile_casinos_custom_list][key]')->hiddenInput(['value'=>'mobile_casinos_custom_list'])?>
					<?=$form->field($model,'settings[mobile_casinos_custom_list][group]')->hiddenInput(['value'=>'portal'])?>
					<?=$form->field($model,'settings[mobile_casinos_custom_list][value]')->dropDownList(ArrayHelper::map(CustomList::find()->all(),'id','name'),['value'=>$settings->get('mobile_casinos_custom_list.data')])?>
					<?=$form->field($model,'settings[mobile_casinos_custom_list][serialized]')->hiddenInput(['value'=>0])?>
                </div>
            </div>
            <h3><?=Yii::t('app','Bonuses fixed pages')?></h3>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
						<?=Html::label(Yii::t('app','Exclusive')); ?>
                    </div>
					<?=$form->field($model,'settings[top_cashback_offer_type][id]')->hiddenInput(['class'=>'form-control','value'=>$settings->get('top_cashback_offer_type.id')])?>
					<?=$form->field($model,'settings[top_cashback_offer_type][key]')->hiddenInput(['value'=>'top_cashback_offer_type'])?>
					<?=$form->field($model,'settings[top_cashback_offer_type][group]')->hiddenInput(['value'=>'portal'])?>
					<?=$form->field($model,'settings[top_cashback_offer_type][value]')->dropDownList(ArrayHelper::map(OfferType::find()->all(),'id','name'),['value'=>$settings->get('top_cashback_offer_type.data')])?>
					<?=$form->field($model,'settings[top_cashback_offer_type][serialized]')->hiddenInput(['value'=>0])?>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
						<?=Html::label(Yii::t('app','Free spins')); ?>
                    </div>
                    <?=$form->field($model,'settings[free_money_offer_type][id]')->hiddenInput(['class'=>'form-control','value'=>$settings->get('free_money_offer_type.id')])?>
					<?=$form->field($model,'settings[free_money_offer_type][key]')->hiddenInput(['value'=>'free_money_offer_type'])?>
					<?=$form->field($model,'settings[free_money_offer_type][group]')->hiddenInput(['value'=>'portal'])?>
					<?=$form->field($model,'settings[free_money_offer_type][value]')->dropDownList(ArrayHelper::map(OfferType::find()->all(),'id','name'),['value'=>$settings->get('free_money_offer_type.data')])?>
					<?=$form->field($model,'settings[free_money_offer_type][serialized]')->hiddenInput(['value'=>0])?>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
						<?=Html::label(Yii::t('app','Deposit bonus')); ?>
                    </div>
                    <?=$form->field($model,'settings[free_spins_offer_type][id]')->hiddenInput(['class'=>'form-control','value'=>$settings->get('free_spins_offer_type.id')])?>
					<?=$form->field($model,'settings[free_spins_offer_type][key]')->hiddenInput(['value'=>'free_spins_offer_type'])?>
					<?=$form->field($model,'settings[free_spins_offer_type][group]')->hiddenInput(['value'=>'portal'])?>
					<?=$form->field($model,'settings[free_spins_offer_type][value]')->dropDownList(ArrayHelper::map(OfferType::find()->all(),'id','name'),['value'=>$settings->get('free_spins_offer_type.data')])?>
					<?=$form->field($model,'settings[free_spins_offer_type][serialized]')->hiddenInput(['value'=>0])?>
                </div>
            </div>
            <h3><?=Yii::t('app','Games fixed page categories')?></h3>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
						<?=Html::label(Yii::t('app','New category')); ?>
                    </div>
                    <?=$form->field($model,'settings[games_new_category][id]')->hiddenInput(['class'=>'form-control','value'=>$settings->get('games_new_category.id')])?>
					<?=$form->field($model,'settings[games_new_category][key]')->hiddenInput(['value'=>'games_new_category'])?>
					<?=$form->field($model,'settings[games_new_category][group]')->hiddenInput(['value'=>'portal'])?>
					<?=$form->field($model,'settings[games_new_category][value]')->dropDownList(ArrayHelper::map(GameCategory::find()->all(),'id','name'),['value'=>$settings->get('games_new_category.data')])?>
					<?=$form->field($model,'settings[games_new_category][serialized]')->hiddenInput(['value'=>0])?>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
						<?=Html::label(Yii::t('app','Playes\' choice category')); ?>
                    </div>
                    <?=$form->field($model,'settings[games_players_choice_category][id]')->hiddenInput(['class'=>'form-control','value'=>$settings->get('games_players_choice_category.id')])?>
					<?=$form->field($model,'settings[games_players_choice_category][key]')->hiddenInput(['value'=>'games_players_choice_category'])?>
					<?=$form->field($model,'settings[games_players_choice_category][group]')->hiddenInput(['value'=>'portal'])?>
					<?=$form->field($model,'settings[games_players_choice_category][value]')->dropDownList(ArrayHelper::map(GameCategory::find()->all(),'id','name'),['value'=>$settings->get('games_players_choice_category.data')])?>
					<?=$form->field($model,'settings[games_players_choice_category][serialized]')->hiddenInput(['value'=>0])?>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
						<?=Html::label(Yii::t('app','Mobile games category')); ?>
                    </div>
                    <?=$form->field($model,'settings[games_mobile_games_category][id]')->hiddenInput(['class'=>'form-control','value'=>$settings->get('games_mobile_games_category.id')])?>
					<?=$form->field($model,'settings[games_mobile_games_category][key]')->hiddenInput(['value'=>'games_mobile_games_category'])?>
					<?=$form->field($model,'settings[games_mobile_games_category][group]')->hiddenInput(['value'=>'portal'])?>
					<?=$form->field($model,'settings[games_mobile_games_category][value]')->dropDownList(ArrayHelper::map(GameCategory::find()->all(),'id','name'),['value'=>$settings->get('games_mobile_games_category.data')])?>
					<?=$form->field($model,'settings[games_mobile_games_category][serialized]')->hiddenInput(['value'=>0])?>
                </div>
            </div>

            <h3><?=Yii::t('app','Tips & Guides fixed page categories')?></h3>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
						<?=Html::label(Yii::t('app','Casino news')); ?>
                    </div>
                    <?=$form->field($model,'settings[top_cashback_game_category][id]')->hiddenInput(['class'=>'form-control','value'=>$settings->get('top_cashback_game_category.id')])?>
					<?=$form->field($model,'settings[top_cashback_game_category][key]')->hiddenInput(['value'=>'top_cashback_game_category'])?>
					<?=$form->field($model,'settings[top_cashback_game_category][group]')->hiddenInput(['value'=>'portal'])?>
					<?=$form->field($model,'settings[top_cashback_game_category][value]')->dropDownList(ArrayHelper::map(\common\models\BlogPostCategory::find()->all(),'id','name'),['value'=>$settings->get('top_cashback_game_category.data')])?>
					<?=$form->field($model,'settings[top_cashback_game_category][serialized]')->hiddenInput(['value'=>0])?>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
						<?=Html::label(Yii::t('app','Guides')); ?>
                    </div>
                    <?=$form->field($model,'settings[instant_details_game_category][id]')->hiddenInput(['class'=>'form-control','value'=>$settings->get('instant_details_game_category.id')])?>
					<?=$form->field($model,'settings[instant_details_game_category][key]')->hiddenInput(['value'=>'instant_details_game_category'])?>
					<?=$form->field($model,'settings[instant_details_game_category][group]')->hiddenInput(['value'=>'portal'])?>
					<?=$form->field($model,'settings[instant_details_game_category][value]')->dropDownList(ArrayHelper::map(\common\models\BlogPostCategory::find()->all(),'id','name'),['value'=>$settings->get('instant_details_game_category.data')])?>
					<?=$form->field($model,'settings[instant_details_game_category][serialized]')->hiddenInput(['value'=>0])?>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
						<?=Html::label(Yii::t('app','Promotions')); ?>
                    </div>
                    <?=$form->field($model,'settings[new_cashback_games_category][id]')->hiddenInput(['class'=>'form-control','value'=>$settings->get('new_cashback_games_category.id')])?>
					<?=$form->field($model,'settings[new_cashback_games_category][key]')->hiddenInput(['value'=>'new_cashback_games_category'])?>
					<?=$form->field($model,'settings[new_cashback_games_category][group]')->hiddenInput(['value'=>'portal'])?>
					<?=$form->field($model,'settings[new_cashback_games_category][value]')->dropDownList(ArrayHelper::map(\common\models\BlogPostCategory::find()->all(),'id','name'),['value'=>$settings->get('new_cashback_games_category.data')])?>
					<?=$form->field($model,'settings[new_cashback_games_category][serialized]')->hiddenInput(['value'=>0])?>
                </div>
            </div>
        </div>


</div>
    <div class="form-group">
		<?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-success']) ?>
    </div>
	<?php ActiveForm::end()?>

</div>
