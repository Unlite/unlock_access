<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\SiteBlogPost */
/* @var $blog_post common\models\BlogPost */
/* @var $form yii\widgets\ActiveForm */
$action = ['/site-blog-post/' . ($model->isNewRecord ? 'create' : 'update')];
if (!$model->isNewRecord) {
	$action['blog_post_id'] = $model->blog_post_id;
}
if (isset($back)) {
	$action['back'] = $back;
}

$sites = \common\models\Site::find()->where(['not in', 'id', !empty($blog_post) ? ArrayHelper::getColumn($blog_post->siteBlogPosts, 'site_id') : []])->all()

?>

<div class="site-blog-post-form">

    <?php $form = ActiveForm::begin(['action' => $action]); ?>

	<?= $form->field($model, 'site_id')->widget(\kartik\select2\Select2::className(),[
		'data' => \yii\helpers\ArrayHelper::map($sites,'id','name'),
		'options' => ['placeholder' => 'Select Site...'],
	]) ?>

	<?= $form->field($model, 'blog_post_id')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\BlogPost::find()->all(),'id','title')) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
