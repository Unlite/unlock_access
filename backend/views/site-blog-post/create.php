<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SiteBlogPost */
/* @var $back null|string */

$this->title = Yii::t('app', 'Create Site Blog Post');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Site Blog Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-blog-post-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'back' => $back,
    ]) ?>

</div>
