<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SiteBlogPost */

$this->title = Yii::t('app', 'Update Site Blog Post: {nameAttribute}', [
    'nameAttribute' => $model->site_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Site Blog Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->site_id, 'url' => ['view', 'site_id' => $model->site_id, 'blog_post_id' => $model->blog_post_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="site-blog-post-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
