<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Currency;

/* @var $this yii\web\View */
/* @var $site common\models\Site|null */
/* @var $model common\models\SiteCurrency */
/* @var $form yii\widgets\ActiveForm */
/* @var $back null|string */

$action = ['/site-currency/create-multi-currencies'];
if (isset($back)) {
    $action['back'] = $back;
}
/* @var $currencies common\models\Currency[]|null */

$currencies = Currency::find()->where(['not in', 'code', !empty($site) ? ArrayHelper::getColumn($site->currencies, 'code') : []])->orderBy(['name'=>SORT_ASC])->all()
?>

<div class="site-payment-form">

    <?php $form = ActiveForm::begin(['action' => $action]); ?>

    <?= $form->field($model, 'site_id')->dropDownList(ArrayHelper::map(\common\models\Site::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'currency_codes[]')->widget(\kartik\select2\Select2::className(),[
        'data' => ArrayHelper::map($currencies, 'code', 'name'),
        'options' => ['placeholder' => 'Select Currencies...', 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Add', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
