<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SiteCurrency */
/* @var $back null|string */
/* @var $multi bool */

$this->title = Yii::t('app', 'Create Site Currency');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Site Currencies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-currency-create">

    <h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render($multi ? '_form-multi' : '_form', [
		'model' => $model,
		'back' => $back,
	]) ?>

</div>
