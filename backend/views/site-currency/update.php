<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SiteCurrency */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Site Currency',
]) . $model->site_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Site Currencies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->site_id, 'url' => ['view', 'site_id' => $model->site_id, 'currency_code' => $model->currency_code]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="site-currency-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
