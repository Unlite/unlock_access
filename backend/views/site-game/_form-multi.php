<?php

use common\models\Game;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $site common\models\Site|null */
/* @var $model common\models\SiteGame */
/* @var $form yii\widgets\ActiveForm */
/* @var $back null|string */

$action = ['/site-game/create-multi-games'];
if (isset($back)) {
    $action['back'] = $back;
}
/* @var $games common\models\Game[]|null */

$games = Game::find()->where(['not in', 'id', !empty($site) ? ArrayHelper::getColumn($site->games, 'id') : []])->all();
?>

<div class="site-game-form">

    <?php $form = ActiveForm::begin(['action' => $action]); ?>

    <?= $form->field($model, 'site_id')->dropDownList(ArrayHelper::map(\common\models\Site::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'game_ids[]')->widget(\kartik\select2\Select2::className(),[
        'data' => ArrayHelper::map($games, 'id', 'name'),
        'options' => ['placeholder' => 'Select Games...', 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model,'is_promoted')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Add', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
