<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SiteGame */
/* @var $form yii\widgets\ActiveForm */
/* @var $back null|string */

$action = ['/site-game/' . ($model->isNewRecord ? 'create' : 'update')];
if (!$model->isNewRecord) {
    $action['site_id'] = $model->site_id;
    $action['game_id'] = $model->game_id;
}
if (isset($back)) {
    $action['back'] = $back;
}
?>

<div class="site-game-form">

    <?php $form = ActiveForm::begin(['action' => $action]); ?>

    <?= $form->field($model, 'site_id')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Site::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'game_id')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Game::find()->all(), 'id', 'name')) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
