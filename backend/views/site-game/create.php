<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SiteGame */
/* @var $back null|string */
/* @var $multi bool */

$this->title = 'Create Site Game';
$this->params['breadcrumbs'][] = ['label' => 'Site Games', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-game-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render($multi ? '_form-multi' : '_form', [
        'model' => $model,
        'back' => $back,
    ]) ?>

</div>
