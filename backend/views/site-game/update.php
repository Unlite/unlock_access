<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SiteGame */
/* @var $back null|string */

$this->title = 'Update Site Game: ' . $model->site_id;
$this->params['breadcrumbs'][] = ['label' => 'Site Games', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->site_id, 'url' => ['view', 'site_id' => $model->site_id, 'game_id' => $model->game_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="site-game-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'back' => $back,
    ]) ?>

</div>
