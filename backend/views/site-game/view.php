<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SiteGame */

$this->title = $model->site_id;
$this->params['breadcrumbs'][] = ['label' => 'Site Games', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-game-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'site_id' => $model->site_id, 'game_id' => $model->game_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'site_id' => $model->site_id, 'game_id' => $model->game_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'site_id',
            'game_id',
        ],
    ]) ?>

</div>
