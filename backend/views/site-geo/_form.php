<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SiteGeo */
/* @var $form yii\widgets\ActiveForm */
/* @var $back null|string */

$action = ['/site-geo/' . ($model->isNewRecord ? 'create' : 'update')];
if (!$model->isNewRecord) {
    $action['site_id'] = $model->site_id;
    $action['geo_code'] = $model->geo_code;
}
if (isset($back)) {
    $action['back'] = $back;
}
?>

<div class="site-geo-form">

    <?php $form = ActiveForm::begin(['action' => $action]); ?>

    <?= $form->field($model, 'site_id')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Site::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'geo_code')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Geo::find()->all(), 'code', 'name')) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
