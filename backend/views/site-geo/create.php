<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SiteGeo|backend\models\forms\SiteGeoMultiForm */
/* @var $back null|string */
/* @var $multi bool */

$this->title = 'Create Site Geo';
$this->params['breadcrumbs'][] = ['label' => 'Site Geos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-geo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render($multi ? '_form-multi' : '_form', [
        'model' => $model,
        'back' => $back,
    ]) ?>

</div>
