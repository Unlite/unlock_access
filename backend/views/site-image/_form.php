<?php

use common\models\SiteImage;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\SiteImage */
/* @var $form yii\widgets\ActiveForm */
/* @var $back null|string */

$action = ['/site-image/' . ($model->isNewRecord ? 'create' : 'update')];
if (!$model->isNewRecord) {
    $action['id'] = $model->id;
}
if (isset($back)) {
    $action['back'] = $back;
}
$types = ArrayHelper::map(SiteImage::find()->select(['type'])->distinct()->asArray()->all(), 'type','type');
$types = ArrayHelper::merge(SiteImage::getDefaultTypes(),$types);
?>

<div class="site-image-form">

    <?php $form = ActiveForm::begin(['action' => $action]); ?>

    <?= $form->field($model, 'site_id')->dropDownList(ArrayHelper::map(\common\models\Site::find()->all(), 'id', 'name')) ?>

	<?= $form->field($model, 'type')->widget(\kartik\select2\Select2::className(),[
		'data' => $types,
		'options' => ['placeholder' => 'Select type...'],
		'pluginOptions' => [
			'tags' => true,
			'tokenSeparators' => [',', ' '],
			'maximumInputLength' => 10
		],
	])->hint("Values: " . join(', ', $types))  ?>

    <?= $form->field($model, 'src')->textInput() ?>

    <?= $form->field($model, 'imageFile')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
