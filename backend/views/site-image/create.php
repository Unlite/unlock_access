<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SiteImage */
/* @var $back null|string */

$this->title = 'Create Site Image';
$this->params['breadcrumbs'][] = ['label' => 'Site Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-image-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'back' => $back,
    ]) ?>

</div>
