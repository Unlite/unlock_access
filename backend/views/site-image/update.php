<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SiteImage */
/* @var $back null|string */

$this->title = 'Update Site Image: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Site Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="site-image-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'back' => $back,
    ]) ?>

</div>
