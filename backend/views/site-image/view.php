<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SiteImage */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Site Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-image-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'label' => 'Site',
                'value' => \yii\bootstrap\Html::a($model->site->name, ['/site/view', 'id' => $model->site->id]),
                'format' => 'Html'
            ],
            'type',
            [
                'attribute' => 'src',
                'value' => function (\common\models\SiteImage $model, $widget) {
                    return \yii\bootstrap\Html::img($model->src, ['class' => 'img-circle', 'width' => '75px', 'height' => '75px']);
                },
                'format' => 'Html',
            ],
        ],
    ]) ?>

</div>
