<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\licensor;

/* @var $this yii\web\View */
/* @var $site common\models\Site|null */
/* @var $model common\models\SiteLicensor */
/* @var $form yii\widgets\ActiveForm */
/* @var $back null|string */

$action = ['/site-licensor/create-multi-licensors'];
if (isset($back)) {
    $action['back'] = $back;
}
/* @var $licensors common\models\Licensor[]|null */

$licensors = \common\models\Licensor::find()->where(['not in', 'id', !empty($site) ? ArrayHelper::getColumn($site->licenses, 'id') : []])->all()
?>

<div class="site-payment-form">

    <?php $form = ActiveForm::begin(['action' => $action]); ?>

    <?= $form->field($model, 'site_id')->dropDownList(ArrayHelper::map(\common\models\Site::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'licensor_ids[]')->widget(\kartik\select2\Select2::className(),[
        'data' => ArrayHelper::map($licensors, 'id', 'name'),
        'options' => ['placeholder' => 'Select licensors...', 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Add', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
