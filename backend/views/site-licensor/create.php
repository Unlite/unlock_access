<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SiteLicensor */

$this->title = Yii::t('app', 'Create Site Licensor');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Site Licensors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-licensor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
