<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SiteLicensor */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Site Licensor',
]) . $model->site_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Site Licensors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->site_id, 'url' => ['view', 'site_id' => $model->site_id, 'licensor_id' => $model->licensor_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="site-licensor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
