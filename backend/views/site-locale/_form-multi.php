<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Locale;

/* @var $this yii\web\View */
/* @var $site common\models\Site|null */
/* @var $model common\models\SiteLocale */
/* @var $form yii\widgets\ActiveForm */
/* @var $back null|string */

$action = ['/site-locale/create-multi-locales'];
if (isset($back)) {
    $action['back'] = $back;
}
/* @var $locales common\models\Locale[]|null */

$locales = Locale::find()->where(['not in', 'code', !empty($site) ? ArrayHelper::getColumn($site->locales, 'code') : []])->all()
?>

<div class="site-payment-form">

    <?php $form = ActiveForm::begin(['action' => $action]); ?>

    <?= $form->field($model, 'site_id')->dropDownList(ArrayHelper::map(\common\models\Site::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'locale_codes[]')->widget(\kartik\select2\Select2::className(),[
        'data' => ArrayHelper::map($locales, 'code', 'name'),
        'options' => ['placeholder' => 'Select Locales...', 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Add', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
