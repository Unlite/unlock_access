<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Locale;
use common\models\Site;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\locale */
/* @var $form yii\widgets\ActiveForm */
$action = ['/site-locale/' . ($model->isNewRecord ? 'create' : 'update')];
if (!$model->isNewRecord) {
	$action['site_id'] = $model->site_id;
}
if (isset($back)) {
	$action['back'] = $back;
}
?>

<div class="locale-form">

    <?php $form = ActiveForm::begin(['action' => $action]); ?>

    <?= $form->field($model, 'site_id')->dropDownList(ArrayHelper::map(Site::find()->all(),'id','name')) ?>
    <?= $form->field($model, 'locale_code')->dropDownList(ArrayHelper::map(Locale::find()->where(['not in', 'code', !empty($site) ? ArrayHelper::getColumn($site->locales, 'code') : []])->all(),'code','name')) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
