<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SiteLocale */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Site Locale',
]) . $model->site_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Site Locales'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->site_id, 'url' => ['view', 'site_id' => $model->site_id, 'locale_code' => $model->locale_code]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="site-locale-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
