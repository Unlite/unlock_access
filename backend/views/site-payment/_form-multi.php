<?php

use common\models\PaymentSystem;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $site common\models\Site|null */
/* @var $model \backend\models\forms\SitePaymentMultiForm */
/* @var $form yii\widgets\ActiveForm */
/* @var $back null|string */

$action = ['/site-payment/create-multi-payments'];
if (isset($back)) {
    $action['back'] = $back;
}
$relations = [];
if ($model->is_deposit) $relations = $site->getSitePayments()->deposit()->asArray()->all();
if ($model->is_withdrawal) $relations = $site->getSitePayments()->withdrawal()->asArray()->all();

/* @var $payments common\models\PaymentSystem[]|null */
$payments = PaymentSystem::find()->where(['not in', 'id', ArrayHelper::getColumn($relations, 'payment_id')])->all();
?>

<div class="site-payment-form">

    <?php $form = ActiveForm::begin(['action' => $action]); ?>

    <?= $form->field($model, 'site_id')->dropDownList(ArrayHelper::map(\common\models\Site::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'payment_ids[]')->widget(\kartik\select2\Select2::class, [
        'id' => 'mu',
        'data' => ArrayHelper::map($payments, 'id', 'name'),
        'options' => [
            'placeholder' => 'Select Payments...',
            'multiple' => true,
			'id' => $model->is_deposit ? 'sitepaymentmultiform-payment_ids_deposit' : 'sitepaymentmultiform-payment_ids_withdrawal'
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

	<?= $form->field($model, 'is_deposit')->hiddenInput()->label(false) ?>
	<?= $form->field($model, 'is_withdrawal')->hiddenInput()->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton('Add', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
