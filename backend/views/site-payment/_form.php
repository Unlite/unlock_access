<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\SitePayment */
/* @var $form yii\widgets\ActiveForm */
/* @var $back null|string */

$action = ['/site-payment/' . ($model->isNewRecord ? 'create' : 'update')];
if (!$model->isNewRecord) {
    $action['site_id'] = $model->site_id;
    $action['payment_id'] = $model->payment_id;
}
if (isset($back)) {
    $action['back'] = $back;
}
$payments = \common\models\PaymentSystem::find()->where(['not in', 'id', !empty($site) ? ArrayHelper::getColumn($site->sitePayments, 'payment_id') : []])->orderBy(['name'=>SORT_ASC])->all();
?>

<div class="site-payment-form">

    <?php $form = ActiveForm::begin(['action' => $action]); ?>

    <?= $form->field($model, 'site_id')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Site::find()->all(), 'id', 'name')) ?>
    <?= $form->field($model, 'payment_id')->dropDownList(\yii\helpers\ArrayHelper::map($payments,'id','name')) ?>

	<?= $form->field($model, 'is_deposit')->checkbox() ?>
	<?= $form->field($model, 'is_withdrawal')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
