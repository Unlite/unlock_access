<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SitePayment */
/* @var $back null|string */

$this->title = 'Update Site PaymentSystem: ' . $model->site_id;
$this->params['breadcrumbs'][] = ['label' => 'Site Payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->site_id, 'url' => ['view', 'site_id' => $model->site_id, 'payment_id' => $model->payment_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="site-payment-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'back' => $back,
    ]) ?>

</div>
