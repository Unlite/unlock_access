<?php

use common\models\Site;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $site common\models\Site|null */
/* @var $model common\models\SiteRelations */
/* @var $form yii\widgets\ActiveForm */
/* @var $back null|string */

$action = ['/site-relations/create-multi-relations'];
if (isset($back)) {
    $action['back'] = $back;
}
/* @var $sites common\models\Site[]|null */

$sites = Site::find()->where(['not in', 'id', !empty($site) ? ArrayHelper::merge([$site->id], ArrayHelper::getColumn($site->relations, 'id')) : []])->all();
?>

<div class="site-payment-form">

    <?php $form = ActiveForm::begin(['action' => $action]); ?>

    <?= $form->field($model, 'site_id_l')->dropDownList(ArrayHelper::map(\common\models\Site::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'site_ids[]')->widget(\kartik\select2\Select2::class, [
        'data' => ArrayHelper::map($sites, 'id', 'name'),
        'options' => ['placeholder' => 'Select Sites...', 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Add', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
