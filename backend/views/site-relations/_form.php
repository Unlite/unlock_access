<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SiteRelations */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="site-relations-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'site_id_l')->textInput() ?>

    <?= $form->field($model, 'site_id_h')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
