<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SiteRelations */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Site Relations',
]) . $model->site_id_l;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Site Relations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->site_id_l, 'url' => ['view', 'site_id_l' => $model->site_id_l, 'site_id_h' => $model->site_id_h]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="site-relations-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
