<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SiteRelations */

$this->title = $model->site_id_l;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Site Relations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-relations-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'site_id_l' => $model->site_id_l, 'site_id_h' => $model->site_id_h], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'site_id_l' => $model->site_id_l, 'site_id_h' => $model->site_id_h], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'site_id_l',
            'site_id_h',
        ],
    ]) ?>

</div>
