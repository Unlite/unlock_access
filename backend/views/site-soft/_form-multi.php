<?php

use common\models\Soft;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $site common\models\Site|null */
/* @var $model common\models\SiteSoft */
/* @var $form yii\widgets\ActiveForm */
/* @var $back null|string */

$action = ['/site-soft/create-multi-soft'];
if (isset($back)) {
    $action['back'] = $back;
}
/* @var $soft common\models\Soft[]|null */

$soft = Soft::find()->where(['not in', 'id', !empty($site) ? ArrayHelper::getColumn($site->softs, 'id') : []])->all();
?>

<div class="site-soft-form">

    <?php $form = ActiveForm::begin(['action' => $action]); ?>

    <?= $form->field($model, 'site_id')->dropDownList(ArrayHelper::map(\common\models\Site::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'soft_ids[]')->widget(\kartik\select2\Select2::className(),[
        'data' => ArrayHelper::map($soft, 'id', 'name'),
        'options' => ['placeholder' => 'Select Games...', 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Add', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
