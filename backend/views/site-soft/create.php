<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SiteSoft */
/* @var $back null|string */
/* @var $multi bool */

$this->title = 'Create Site Soft';
$this->params['breadcrumbs'][] = ['label' => 'Site Softs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-soft-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render($multi ? '_form-multi' : '_form', [
        'model' => $model,
        'back' => $back,
    ]) ?>

</div>
