<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SiteSoftSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Site Softs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-soft-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Site Soft', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Create Site Softs', ['create-multi-softs'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Site',
                'attribute' => 'site_id',
                'value' => function (\common\models\SiteSoft $model, $key, $index, $column) {
                    return \yii\bootstrap\Html::a($model->site->name, ['/site/view', 'id' => $model->site->id]);
                },
                'format' => 'Html',
                'filter' => Html::activeDropDownList($searchModel, 'site_id', \yii\helpers\ArrayHelper::map(\common\models\Site::find()->all(), 'id', 'name'), ['class' => 'form-control', 'prompt' => ''])
            ],
            [
                'label' => 'Soft',
                'attribute' => 'soft_id',
                'value' => function (\common\models\SiteSoft $model, $key, $index, $column) {
                    return \yii\bootstrap\Html::a($model->soft->name, ['/soft/view', 'id' => $model->game->id]);
                },
                'format' => 'Html',
                'filter' => Html::activeDropDownList($searchModel, 'soft_id', \yii\helpers\ArrayHelper::map(\common\models\Soft::find()->all(), 'id', 'name'), ['class' => 'form-control', 'prompt' => ''])
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
