<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SiteSoft */
/* @var $back null|string */

$this->title = 'Update Site Soft: ' . $model->site_id;
$this->params['breadcrumbs'][] = ['label' => 'Site Softs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->site_id, 'url' => ['view', 'site_id' => $model->site_id, 'soft_id' => $model->soft_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="site-soft-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'back' => $back,
    ]) ?>

</div>
