<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\SiteTranslationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="site-translation-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'site_id')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Site::find()->all(), 'id', 'name'), ['prompt' => '']) ?>

    <?= $form->field($model, 'geo_code')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Geo::find()->all(), 'code', 'name'), ['prompt' => '']) ?>

    <?= $form->field($model, 'promo_text') ?>

    <?= $form->field($model, 'description') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
