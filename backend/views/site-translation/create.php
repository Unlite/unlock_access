<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SiteTranslation */
/* @var $back null|string */

$this->title = 'Create Site Translation';
$this->params['breadcrumbs'][] = ['label' => 'Site Translations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-translation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'back' => $back,
    ]) ?>

</div>
