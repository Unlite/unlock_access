<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SiteTranslationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Site Translations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-translation-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Site Translation', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Site',
                'attribute' => 'site_id',
                'value' => function (\common\models\SiteTranslation $model, $key, $index, $column) {
                    return \yii\bootstrap\Html::a($model->site->name, ['/site/view', 'id' => $model->site->id]);
                },
                'format' => 'Html',

            ],
            [
                'label' => 'Geo',
                'attribute' => 'geo_code',
                'value' => function (\common\models\SiteTranslation $model, $key, $index, $column) {
                    return \yii\bootstrap\Html::a($model->geo->name, ['/geo/view', 'id' => $model->geo->code]);
                },
                'format' => 'Html',

            ],
            'description',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
