<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SiteTranslation */

$this->title = $model->site_id;
$this->params['breadcrumbs'][] = ['label' => 'Site Translations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-translation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'site_id' => $model->site_id, 'geo_code' => $model->geo_code], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'site_id' => $model->site_id, 'geo_code' => $model->geo_code], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'Site',
                'value' => \yii\bootstrap\Html::a($model->site->name, ['/site/view', 'id' => $model->site->id]),
                'format' => 'Html'
            ],
            [
                'label' => 'Geo',
                'value' => \yii\bootstrap\Html::a($model->geo->name, ['/geo/view', 'id' => $model->geo->code]),
                'format' => 'Html'
            ],
            'description',
            'promo_text',
        ],
    ]) ?>

</div>
