<?php

use common\models\Site;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Site */
/* @var $info common\models\SiteInfo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="site-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6 .col-sm-12 col-xs-12">
            <?= $form->field($model, 'name')->textInput() ?>

            <?= $form->field($model, 'slug')->textInput()->hint(\common\widgets\inflector\Slug::widget(['from' => '#site-name', 'to' => '#site-slug', 'confirm' => !$model->isNewRecord])) ?>

            <?= $form->field($model, 'predefined_rating')->textInput() ?>

            <?= $form->field($model, 'expire')->textInput() ?>

            <?= $form->field($model, 'type')->dropDownList(array_combine(Site::types(), Site::types())) ?>

            <?= $form->field($model, 'aliases')->textInput() ?>

            <?= $form->field($model, 'status')->dropDownList(array_combine(Site::statuses(), Site::statuses())) ?>
        </div>
        <div class="col-md-6 .col-sm-12 col-xs-12">
            <h4>Additional fields</h4>

            <?= $form->field($info, 'casino_max_pay')->textInput() ?>
            <?= $form->field($info, 'casino_max_win')->textInput() ?>
            <?= $form->field($info, 'min_deposit')->textInput() ?>
            <?= $form->field($info, 'min_withdraw')->textInput() ?>
            <?= $form->field($info, 'max_withdraw')->textInput() ?>
            <?= $form->field($info, 'payout')->textInput() ?>
            <?= $form->field($info, 'online_since')->textInput() ?>
            <?= $form->field($info, 'owner')->textInput() ?>
            <?= $form->field($info, 'email')->textInput() ?>
            <?= $form->field($info, 'support_types')->textInput() ?>
            <?= $form->field($info, 'is_mobile_friendly')->dropDownList([0=>"No",1=>"Yes"]) ?>
            <?= $form->field($info, 'best_support')->dropDownList([0=>"No",1=>"Yes"]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
<?php
$js = <<<JS
function renderInfo(type){
    $('.form-group').each(function(index){
        var classes = $(this).attr("class").split(' ');
        var hide = false;
        $.each(classes, function(index){
            if(this.indexOf('field-siteinfo-') === 0){
                if(this.indexOf('field-siteinfo-all') !== 0 && this.indexOf('field-siteinfo-'+type+'_') !== 0) hide = true;
            }
        });
        if(hide) $(this).hide();
        else $(this).show();
    });
}
var typeInput = $('#site-type');
typeInput.change();
typeInput.change(function(){
    renderInfo($(this).val());
});
JS;
$this->registerJs($js);
?>

</div>
