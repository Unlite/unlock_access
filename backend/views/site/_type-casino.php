<?php

use yii\bootstrap\Modal;
use yii\widgets\DetailView;
use yii\widgets\ListView;
use common\models\SitePayment;

/* @var $this yii\web\View */
/* @var $model common\models\Site */
$gameCategorySitesProvider = new \yii\data\ActiveDataProvider([
    'query' => $model->getGameCategorySites()->joinWith(['gameCategory']),
    'pagination' => ['pageSize' => 50],
]);
$customListSitesProvider = new \yii\data\ActiveDataProvider([
    'query' => $model->getCustomListSites()->joinWith(['customList']),
    'pagination' => ['pageSize' => 50],
]);
$gameProviderSitesProvider = new \yii\data\ActiveDataProvider([
    'query' => $model->getGameProviderSites()->joinWith(['gameProvider']),
    'pagination' => ['pageSize' => 50],
]);
?>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        [
            'label' => 'Games',
            'value' => function (\common\models\Site $model, $widget) {
                $linkDataProvider = new \yii\data\ActiveDataProvider([
                    'query' => $model->getSiteGames()
                ]);
                return ListView::widget([
                    'dataProvider' => $linkDataProvider,
                    'itemView' => 'site-games/_item',
                    'emptyText' => false,
                    'summary' => '',
                ]);
            },
            'format' => 'raw',
        ],
        [
            'label' => 'Soft',
            'value' => function (\common\models\Site $model, $widget) {
                $linkDataProvider = new \yii\data\ActiveDataProvider([
                    'query' => $model->getSiteSofts()
                ]);
                return ListView::widget([
                    'dataProvider' => $linkDataProvider,
                    'itemView' => 'site-soft/_item',
                    'emptyText' => false,
                    'summary' => '',
                ]);
            },
            'format' => 'raw',
        ],
        [
            'label' => 'Licensors',
            'value' => function (\common\models\Site $model, $widget) {
                $linkDataProvider = new \yii\data\ActiveDataProvider([
                    'query' => $model->getSiteLicensors()->joinWith(['licensor'])
                ]);
                return ListView::widget([
                    'dataProvider' => $linkDataProvider,
                    'itemView' => 'site-licensor/_item',
                    'emptyText' => false,
                    'summary' => '',
                ]);
            },
            'format' => 'raw',
        ],
        [
            'label' => 'Payments',
            'value' => function (\common\models\Site $model, $widget) {
                $linkDataProvider = new \yii\data\ActiveDataProvider([
                    'query' => $model->getSitePayments()->joinWith(['payment'])
                ]);
                return ListView::widget([
                    'dataProvider' => $linkDataProvider,
                    'itemView' => 'site-payments/_item',
                    'emptyText' => false,
                    'summary' => '',
                ]);
            },
            'format' => 'raw',
        ],
        [
            'label' => 'Locales',
            'value' => function (\common\models\Site $model, $widget) {
                $linkDataProvider = new \yii\data\ActiveDataProvider([
                    'query' => $model->getSiteLocales()->joinWith(['locale'])
                ]);
                return ListView::widget([
                    'dataProvider' => $linkDataProvider,
                    'itemView' => 'site-locale/_item',
                    'emptyText' => false,
                    'summary' => '',
                ]);
            },
            'format' => 'raw',
        ],
        [
            'label' => 'Currencies',
            'value' => function (\common\models\Site $model, $widget) {
                $linkDataProvider = new \yii\data\ActiveDataProvider([
                    'query' => $model->getSiteCurrencies()->joinWith(['currency'])
                ]);
                return ListView::widget([
                    'dataProvider' => $linkDataProvider,
                    'itemView' => 'site-currency/_item',
                    'emptyText' => false,
                    'summary' => '',
                ]);
            },
            'format' => 'raw',
        ],
        [
            'label' => 'Site Relations',
            'value' => function (\common\models\Site $model, $widget) {
                $arrayDataProvider = new \yii\data\ActiveDataProvider([
                    'query' => $model->getSiteRelations(),
                    'pagination' => ['pageSize' => 50],
                ]);
                return ListView::widget([
                    'dataProvider' => $arrayDataProvider,
                    'itemView' => 'site-relations/_item',
                    'emptyText' => false,
                    'summary' => '',
                    'viewParams' => ['site' => $model]
                ]);
            },
            'format' => 'raw',
        ],
        [
            'label' => 'Game Categories (' . $gameCategorySitesProvider->getCount() . ')',
            'value' => function (\common\models\Site $model, $widget) use ($gameCategorySitesProvider) {
                return ListView::widget([
                    'dataProvider' => $gameCategorySitesProvider,
                    'itemView' => 'game-category-site/_item',
                    'emptyText' => false,
                    'summary' => '',
                    'viewParams' => ['site' => $model]
                ]);
            },
            'format' => 'raw',
        ],
        [
            'label' => 'Game Providers (' . $gameProviderSitesProvider->getCount() . ')',
            'value' => function (\common\models\Site $model, $widget) use ($gameProviderSitesProvider) {
                return ListView::widget([
                    'dataProvider' => $gameProviderSitesProvider,
                    'itemView' => 'game-provider-site/_item',
                    'emptyText' => false,
                    'summary' => '',
                    'viewParams' => ['site' => $model]
                ]);
            },
            'format' => 'raw',
        ],
        [
            'label' => 'Custom Lists (' . $customListSitesProvider->getCount() . ')',
            'value' => function (\common\models\Site $model, $widget) use ($customListSitesProvider) {
                return ListView::widget([
                    'dataProvider' => $customListSitesProvider,
                    'itemView' => 'custom-list-site/_item',
                    'emptyText' => false,
                    'summary' => '',
                    'viewParams' => ['site' => $model]
                ]);
            },
            'format' => 'raw',
        ],
    ],
]) ?>
<span>
    <?php Modal::begin([
        'header' => '<h2>Add Games</h2>',
        'toggleButton' => ['label' => 'Add Games', 'class' => 'btn btn-success'],
    ]);

    echo $this->render('/site-game/_form-multi', [
        'site' => $model,
        'model' => new \backend\models\forms\SiteGameMultiForm(['site_id' => $model->id]),
        'back' => \yii\helpers\Url::to()
    ]);

    Modal::end();
    ?>
    <?php Modal::begin([
        'header' => '<h2>Add Soft</h2>',
        'toggleButton' => ['label' => 'Add Soft', 'class' => 'btn btn-success'],
    ]);

    echo $this->render('/site-soft/_form-multi', [
        'site' => $model,
        'model' => new \backend\models\forms\SiteSoftMultiForm(['site_id' => $model->id]),
        'back' => \yii\helpers\Url::to()
    ]);

    Modal::end();
    ?>
    <?php Modal::begin([
        'header' => '<h2>Add Licensor</h2>',
        'toggleButton' => ['label' => 'Add Licensor', 'class' => 'btn btn-success'],
    ]);

    echo $this->render('/site-licensor/_form-multi', [
        'site' => $model,
        'model' => new \backend\models\forms\SiteLicensorMultiForm(['site_id' => $model->id]),
        'back' => \yii\helpers\Url::to()
    ]);

    Modal::end();
    ?>
    <?php Modal::begin([
        'header' => '<h2>Add Deposit Payments</h2>',
        'toggleButton' => ['label' => 'Add Deposit Payments', 'class' => 'btn btn-success'],
    ]);

    echo $this->render('/site-payment/_form-multi.php', [
        'site' => $model,
        'model' => new \backend\models\forms\SitePaymentMultiForm(['site_id' => $model->id, 'is_deposit' => 1]),
        'back' => \yii\helpers\Url::to()
    ]);

    Modal::end();
    ?>
    <?php Modal::begin([
        'header' => '<h2>Add Withdraw Payments</h2>',
        'toggleButton' => ['label' => 'Add With. Payments', 'class' => 'btn btn-warning'],
    ]);

    echo $this->render('/site-payment/_form-multi.php', [
        'site' => $model,
        'model' => new \backend\models\forms\SitePaymentMultiForm(['site_id' => $model->id, 'is_withdrawal' => 1]),
        'back' => \yii\helpers\Url::to()
    ]);

    Modal::end();
    ?>
    <?php Modal::begin([
        'header' => '<h2>Add Locale</h2>',
        'toggleButton' => ['label' => 'Add Locale', 'class' => 'btn btn-success'],
    ]);

    echo $this->render('/site-locale/_form-multi.php', [
        'site' => $model,
        'model' => new \backend\models\forms\SiteLocaleMultiForm(['site_id' => $model->id]),
        'back' => \yii\helpers\Url::to()
    ]);

    Modal::end();
    ?>
    <?php Modal::begin([
        'header' => '<h2>Add Currency</h2>',
        'toggleButton' => ['label' => 'Add Currency', 'class' => 'btn btn-success'],
    ]);

    echo $this->render('/site-currency/_form-multi.php', [
        'site' => $model,
        'model' => new \backend\models\forms\SiteCurrencyMultiForm(['site_id' => $model->id]),
        'back' => \yii\helpers\Url::to()
    ]);

    Modal::end();
    ?>
    <?php Modal::begin([
        'header' => '<h2>Add Site Relation</h2>',
        'toggleButton' => ['label' => 'Add Site Relation', 'class' => 'btn btn-success'],
    ]);

    echo $this->render('/site-relations/_form-multi.php', [
        'site' => $model,
        'model' => new \backend\models\forms\SiteRelationsMultiForm(['site_id_l' => $model->id]),
        'back' => \yii\helpers\Url::to()
    ]);

    Modal::end();
    ?>
    <?php Modal::begin([
        'header' => '<h2>Add Game Category</h2>',
        'toggleButton' => ['label' => 'Add Game Category', 'class' => 'btn btn-success'],
    ]);

    echo $this->render('/game-category-site/_form-multi.php', [
        'site' => $model,
        'model' => new \backend\models\forms\GameCategorySiteMultiForm(['site_id' => $model->id]),
        'back' => \yii\helpers\Url::to()
    ]);

    Modal::end();
    ?>
    <?php Modal::begin([
        'header' => '<h2>Add Game Provider</h2>',
        'toggleButton' => ['label' => 'Add Game Provider', 'class' => 'btn btn-success'],
    ]);

    echo $this->render('/game-provider-site/_form-multi.php', [
        'site' => $model,
        'model' => new \backend\models\forms\GameProviderSiteMultiForm(['site_id' => $model->id]),
        'back' => \yii\helpers\Url::to()
    ]);

    Modal::end();
    ?>
    <?php Modal::begin([
        'header' => '<h2>Add Custom List</h2>',
        'toggleButton' => ['label' => 'Add Custom List', 'class' => 'btn btn-success'],
    ]);

    echo $this->render('/custom-list-site/_form-multi.php', [
        'site' => $model,
        'model' => new \backend\models\forms\CustomListSiteMultiForm(['site_id' => $model->id]),
        'back' => \yii\helpers\Url::to()
    ]);

    Modal::end();
    ?>
</span>