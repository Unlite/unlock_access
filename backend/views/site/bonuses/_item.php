<?php

use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\widgets\DetailView;
use yii\widgets\ListView;

/* @var $model common\models\Bonuses */
?>
<div class="domain">
    <div class="panel panel-primary">
        <div class="panel-heading"><?= Html::encode($model->text) ?></div>
        <div class="panel-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    [
                        'label' => 'Site',
                        'value' => \yii\bootstrap\Html::a($model->site->name, ['/site/view', 'id' => $model->site_id]),
                        'format' => 'Html'
                    ],
                    'text',
                    //'url:url',
                    [
                        'label' => 'Url',
                        'value' => \yii\bootstrap\Html::a($model->url, \common\modules\away\widgets\ModelLinkWidget::widget(['model' => $model]), ['target' => '_blank']),
                        'format' => 'Raw'
                    ],
                    'promo_code',
                    [
                        'attribute' => 'bonusType.name',
                        'label' => 'Bonus Type',
                    ],
                    [
                        'attribute' => 'start_at',
                        'value' => \Yii::$app->formatter->asDatetime($model->start_at,'php:d.m.Y')
                    ],
                    [
                        'attribute' => 'end_at',
                        'value' => \Yii::$app->formatter->asDatetime($model->end_at,'php:d.m.Y')
                    ],
                    [
                        'attribute' => 'is_promoted',
                        'value' => function($data) {return $data->is_promoted ? 'Yes' :'No';}
                    ],
					[
						'label' => 'Offer Types',
						'value' => function (\common\models\Bonuses $model, $widget) {
							$linkDataProvider = new \yii\data\ActiveDataProvider([
								'query' => $model->getOfferTypeBonuses(),
								'pagination' => ['pageSize' => 50],

							]);
							return ListView::widget([
								'dataProvider' => $linkDataProvider,
								'itemView' => 'offer-type-bonuses/_item',
								'emptyText' => false,
								'summary' => '',
							]);
						},
						'format' => 'raw',
					],
                ],
            ]) ?>

        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-md-6 text-left">
					<?php Modal::begin([
						'header' => '<h2>Add Offer Type</h2>',
						'toggleButton' => ['label' => 'Add Offer Type', 'class' => 'btn btn-success'],
					]);

					echo $this->render('/offer-type-bonuses/_form', [
						'game' => $model,
						'is_allowed' => false,
						'model' => new \common\models\OfferTypeBonuses(['bonuses_id' => $model->id]),
						'back' => \yii\helpers\Url::to()
					]);

					Modal::end();
					?>
                </div>
                <div class="col-md-6 text-right">
                    <?= Html::a(Html::icon('eye-open'), ['/bonuses/view', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']); ?>
                    <?= Html::a(Html::icon('pencil'), ['/bonuses/update', 'id' => $model->id, 'back' => \yii\helpers\Url::to()], ['class' => 'btn btn-sm btn-primary']); ?>
                    <?= Html::a(Html::icon('trash'), ['/bonuses/delete', 'id' => $model->id, 'back' => \yii\helpers\Url::to()], [
                        'class' => 'btn btn-sm btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>