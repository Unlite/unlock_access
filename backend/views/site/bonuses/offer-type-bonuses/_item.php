<?php

use yii\bootstrap\Html;

/* @var $model common\models\OfferTypeBonuses */

?>

<span>
    <?= $model->offerType->name .': '.$model->offer_text?>
    <?= Html::a(Html::icon('trash'), ['/offer-type-bonuses/delete', 'offer_type_id' => $model->offer_type_id, 'bonuses_id' => $model->bonuses_id, 'back' => \yii\helpers\Url::to()], [
        'class' => 'btn btn-xs btn-danger',
        'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
        ],
    ]) ?>
</span>
