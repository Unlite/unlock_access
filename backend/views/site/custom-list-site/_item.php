<?php

use yii\bootstrap\Html;

/* @var $model common\models\CustomListSite */

?>

<span>
    <?= $model->customList->name ?>
    <?= Html::a(Html::icon('trash'), ['/custom-list-site/delete', 'site_id' => $model->site_id, 'custom_list_id' => $model->custom_list_id, 'back' => \yii\helpers\Url::to()], [
        'class' => 'btn btn-xs btn-danger',
        'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
        ],
    ]) ?>
</span>
