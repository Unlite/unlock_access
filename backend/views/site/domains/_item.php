<?php

use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\widgets\DetailView;
use yii\widgets\ListView;

/* @var $model common\models\Domain */
?>
<div class="domain">
    <div class="panel panel-primary">
        <div class="panel-heading"><?= Html::encode($model->domain) ?></div>
        <div class="panel-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
//                    [
//                        'label' => 'Site',
//                        'value' => \yii\bootstrap\Html::a($model->site->name, ['/site/view', 'id' => $model->site->id]),
//                        'format' => 'Html'
//                    ],
                    'type',
                    'domain',
                    //'direct_url:url',
                    [
                        'label' => 'Direct Url',
                        'value' => \yii\bootstrap\Html::a($model->direct_url, \common\modules\away\widgets\ModelLinkWidget::widget(['model' => $model]), ['target' => '_blank']),
                        'format' => 'Raw'
                    ],
                    'proxy_pattern',
                    'status',
                    [
                        'label' => 'Proxies',
                        'value' => function (\common\models\Domain $model, $widget) {
                            $linkDataProvider = new \yii\data\ActiveDataProvider([
                                'query' => $model->getDomainProxies()
                            ]);
                            return ListView::widget([
                                'dataProvider' => $linkDataProvider,
                                'itemView' => '/domain/domain-proxies/_item',
                                'emptyText' => false,
                                'summary' => '',
                            ]);
                        },
                        'format' => 'raw',
                    ],
                ],
            ]) ?>
            <p>
                <?php Modal::begin([
                    'header' => '<h2>Add Proxies</h2>',
                    'toggleButton' => ['label' => 'Add Proxies', 'class' => 'btn btn-success'],
                ]);

                echo $this->render('/domain-proxy/_form-multi', [
                    'model' => new \backend\models\forms\DomainProxyMultiForm(['domain_id' => $model->id]),
                    'back' => \yii\helpers\Url::to(),
                    'domain' => $model,
                ]);

                Modal::end();
                ?>
            </p>

            <?php
            $linkDataProvider = new \yii\data\ActiveDataProvider([
                'query' => $model->getLinks()
            ]);
            echo ListView::widget([
                'dataProvider' => $linkDataProvider,
                'itemView' => '/site/links/_item',
                'emptyText' => false,
                'summary' => '',
            ]);
            ?>

        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-md-6">
                    <?php Modal::begin([
                        'header' => '<h2>Create Link</h2>',
                        'toggleButton' => ['label' => 'Create Link', 'class' => 'btn btn-success'],
                    ]);

                    echo $this->render('/link/_form', [
                        'model' => new \common\models\Link(['domain_id' => $model->id]),
                        'back' => \yii\helpers\Url::to()
                    ]);

                    Modal::end();
                    ?>
                </div>
                <div class="col-md-6 text-right">
                    <?= Html::a(Html::icon('eye-open'), ['/domain/view', 'id' => $model->id], ['class' => 'btn btn-primary']); ?>
                    <?= Html::a(Html::icon('pencil'), ['/domain/update', 'id' => $model->id, 'back' => \yii\helpers\Url::to()], ['class' => 'btn btn-primary']); ?>
                    <?= Html::a(Html::icon('trash'), ['/domain/delete', 'id' => $model->id, 'back' => \yii\helpers\Url::to()], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>