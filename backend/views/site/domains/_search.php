<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\DomainSearch */
/* @var $site common\models\Site */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="domain-search">

    <?php $form = ActiveForm::begin([
        'action' => ['/site/domains', 'id' => $site->id],
        'method' => 'get',
        'options' => ['class' => 'form-inline']
    ]); ?>

    <?= $form->field($model, 'type')->dropDownList(array_combine(\common\models\Domain::types(), \common\models\Domain::types()), ['prompt' => '']) ?>

    <?= $form->field($model, 'status')->dropDownList(array_combine(\common\models\Domain::statuses(), \common\models\Domain::statuses()), ['prompt' => '']) ?>

    <?= $form->field($model, 'domain') ?>

    <?= $form->field($model, 'direct_url') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
