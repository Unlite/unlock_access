<?php

use yii\bootstrap\Html;

/* @var $model common\models\GameCategorySite */

?>

<span>
    <?= Html::a($model->gameCategory->name,['game-category/view','id'=>$model->game_category_id]) ?>
    <?= Html::a(Html::icon('trash'), ['/game-category-site/delete', 'site_id' => $model->site_id, 'game_category_id' => $model->game_category_id, 'back' => \yii\helpers\Url::to()], [
        'class' => 'btn btn-xs btn-danger',
        'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
        ],
    ]) ?>
</span>
