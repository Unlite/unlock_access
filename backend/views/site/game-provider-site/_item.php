<?php

use yii\bootstrap\Html;

/* @var $model common\models\GameProviderSite */

?>

<span>
    <?= Html::a($model->gameProvider->name,['game-category/view','id'=>$model->game_provider_id]) ?>
    <?= Html::a(Html::icon('trash'), ['/game-provider-site/delete', 'site_id' => $model->site_id, 'game_provider_id' => $model->game_provider_id, 'back' => \yii\helpers\Url::to()], [
        'class' => 'btn btn-xs btn-danger',
        'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
        ],
    ]) ?>
</span>
