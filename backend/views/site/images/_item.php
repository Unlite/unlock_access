<?php

use yii\bootstrap\Html;
use yii\widgets\DetailView;

/* @var $model common\models\SiteImage */
?>
<div class="domain">
    <div class="panel panel-primary">
        <div class="panel-heading"></div>
        <div class="panel-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'type',
                    [
                        'attribute' => 'src',
                        'value' => function (\common\models\SiteImage $model, $widget) {
                            return \yii\bootstrap\Html::img($model->src, [/*'class' => 'img-circle',*/ 'style' => 'max-width: 1040px']);
                        },
                        'format' => 'Html',
                    ],
                ],
            ]) ?>
        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-md-6">
                </div>
                <div class="col-md-6 text-right">
                    <?= Html::a(Html::icon('eye-open'), ['/site-image/view', 'id' => $model->id], ['class' => 'btn btn-primary']); ?>
                    <?= Html::a(Html::icon('pencil'), ['/site-image/update', 'id' => $model->id, 'back' => \yii\helpers\Url::to()], ['class' => 'btn btn-primary']); ?>
                    <?= Html::a(Html::icon('trash'), ['/site-image/delete', 'id' => $model->id, 'back' => \yii\helpers\Url::to()], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>