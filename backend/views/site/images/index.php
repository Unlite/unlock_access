<?php

use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model common\models\Site */
/* @var $searchModel backend\models\search\SiteImageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $model->name . ' | Images';
$this->params['breadcrumbs'][] = ['label' => 'Sites', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Images';
?>
<div class="site-domains">
    <h1><?= Html::encode($model->name) ?></h1>

    <?= $this->render('../tabs', [
        'model' => $model,
    ]) ?>

    <p>
        <?php Modal::begin([
            'header' => '<h2>Create Image</h2>',
            'toggleButton' => ['label' => 'Create', 'class' => 'btn btn-success'],
			'options' => [
				'tabindex' => false
			],
        ]);

        echo $this->render('/site-image/_form', [
            'model' => new \common\models\SiteImage(['site_id' => $model->id]),
            'back' => \yii\helpers\Url::to()
        ]);

        Modal::end();
        ?>
    </p>

    <?php echo $this->render('_search', ['model' => $searchModel, 'site' => $model]); ?>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_item',
    ]); ?>

</div>
