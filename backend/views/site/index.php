<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SiteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sites';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Site', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'name',
                'value' => function (\common\models\Site $model, $key, $index, $column) {
                    return \yii\bootstrap\Html::a($model->name, ['/site/view', 'id' => $model->id]);
                },
                'format' => 'Html',

            ],
            'predefined_rating',
            'rating',
            // 'expire',
            [
                'attribute' => 'type',
                'filter' => Html::activeDropDownList($searchModel, 'type', array_combine(\common\models\Site::types(), \common\models\Site::types()), ['class' => 'form-control', 'prompt' => ''])
            ],
			[
				'attribute' => 'status',
				'filter' => Html::activeDropDownList($searchModel, 'status', array_combine(\common\models\Site::statuses(), \common\models\Site::statuses()), ['class' => 'form-control', 'prompt' => ''])
			],
            'aliases',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
