<?php

use yii\bootstrap\Html;
/* @var $model common\models\License */

?>


<span>
    <?= $model->geo->name ?>
    <?= Html::a(Html::icon('trash'), ['/license/delete', 'site_id' => $model->site_id, 'geo_code' => $model->geo_code, 'back' => \yii\helpers\Url::to()], [
        'class' => 'btn btn-xs btn-danger',
        'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
        ],
    ]) ?>
</span>
