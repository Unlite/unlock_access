<?php

use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\widgets\DetailView;

/* @var $model common\models\Link */
?>
<div class="link">
    <div class="panel panel-primary">
        <div class="panel-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
//                    [
//                        'label' => 'Domain',
//                        'value' => \yii\bootstrap\Html::a($model->domain->domain, ['/site/view', 'id' => $model->domain->id]),
//                        'format' => 'Html'
//                    ],
                    [
                        'label' => 'Redirect Url',
                        'value' => \yii\bootstrap\Html::a($model->redirect_url, \common\modules\away\widgets\ModelLinkWidget::widget(['model' => $model]), ['target' => '_blank']),
                        'format' => 'Raw'
                    ],
                    //'redirect_url:url',
                ],
            ]) ?>

            <?php
            $linkDataProvider = new \yii\data\ActiveDataProvider([
                'query' => $model->getLinkWildcards()
            ]);
            echo GridView::widget([
                'dataProvider' => $linkDataProvider,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],

                    //'id',
                    //'link_id',
                    'pattern',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => 'Actions',
                        'headerOptions' => ['style' => 'color:#337ab7'],
                        'template' => '{view} {update} {delete}',
                        'urlCreator' => function ($action, \common\models\LinkWildcard $model, $key, $index) {
                            return Yii::$app->getUrlManager()->createUrl(['link-wildcard/' . $action, 'id' => $model->id, 'back' => \yii\helpers\Url::to()]);
                        }
                    ],
                ],
                'emptyText' => false,
                'summary' => '',
            ]);
            ?>
        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-md-6">
                    <?php Modal::begin([
                        'header' => '<h2>Create Wildcard</h2>',
                        'toggleButton' => ['label' => 'Create Wildcard', 'class' => 'btn btn-success'],
                    ]);

                    echo $this->render('/link-wildcard/_form', [
                        'model' => new \common\models\LinkWildcard(['link_id' => $model->id]),
                        'back' => \yii\helpers\Url::to()
                    ]);

                    Modal::end();
                    ?>
                </div>
                <div class="col-md-6 text-right">
                    <?= Html::a(Html::icon('eye-open'), ['/link/view', 'id' => $model->id], ['class' => 'btn btn-primary']); ?>
                    <?= Html::a(Html::icon('pencil'), ['/link/update', 'id' => $model->id, 'back' => \yii\helpers\Url::to()], ['class' => 'btn btn-primary']); ?>
                    <?= Html::a(Html::icon('trash'), ['/link/delete', 'id' => $model->id, 'back' => \yii\helpers\Url::to()], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>