<?php

use common\models\Site;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Site */
/* @var $info common\models\forms\SiteInfoForm */
/* @var $form yii\widgets\ActiveForm */
$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Sites', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Review';
?>
<div class="site-view">
    <h1><?= Html::encode($this->title) ?></h1>
	<?= $this->render('tabs', [
		'model' => $model,
	]) ?>


    <div class="site-form">

		<?php $form = ActiveForm::begin(); ?>
        <div class="row">
            <div class="col-sm-12 col-xs-12">
				<?= $form->field($info, 'rua')->widget(DatePicker::classname(), [
					'options' => ['placeholder' => 'Enter update date'],
					'pluginOptions' => [
						'autoclose'=>true,
						'format' => 'dd.mm.yyyy',
						'todayHighlight' => true
					]
				]);?>
                <div class="col-md-6 col-sm-12">
                    <?= $form->field($info, 'description')->textarea() ?>
					<?= $form->field($info, 'page_title')->textInput() ?>
					<?= $form->field($info, 'page_description')->textInput() ?>
                </div>

                <div class="col-md-6 col-sm-12">
                    <?= $form->field($info, 'review')->widget(\yii2mod\markdown\MarkdownEditor::class, [
						'editorOptions' => [
							'showIcons' => ["code", "table"],
						],
					]) ?>
                </div>
            </div>
        </div>

        <div class="form-group">
			<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

		<?php ActiveForm::end(); ?>

    </div>

</div>
