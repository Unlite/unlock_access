<?php


/* @var $this yii\web\View */
/* @var $model common\models\Review */

use yii\bootstrap\Html;

$this->registerCss(".site-review .panel-body {min-height: 200px; max-height: 200px; overflow-y: scroll;}");

?>
<div class="site-review">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-9">
                    <?= Html::encode($model->author) . ' | ' . ($model->user_id ? 'BY USER' : 'GENERATED') ?>
                </div>
                <div class="col-md-3 text-right">
                    <?= \Yii::$app->formatter->asDecimal($model->rating, 1) ?>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <?= Html::encode($model->text) ?>
        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-md-7">
                    <?= \Yii::$app->formatter->asDatetime($model->created_at) ?>
                </div>
                <div class="col-md-5 text-right">
                    <?= Html::a(Html::icon('eye-open'), ['/review/view', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']); ?>
                    <?= Html::a(Html::icon('pencil'), ['/review/update', 'id' => $model->id, 'back' => \yii\helpers\Url::to()], ['class' => 'btn btn-sm btn-primary']); ?>
                    <?= Html::a(Html::icon('trash'), ['/review/delete', 'id' => $model->id, 'back' => \yii\helpers\Url::to()], [
                        'class' => 'btn btn-sm btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>

