<?php

use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model common\models\Site */
/* @var $searchModel backend\models\search\ReviewSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $model->name . ' | Reviews';
$this->params['breadcrumbs'][] = ['label' => 'Sites', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Reviews';
?>
<div class="site-reviews">
    <h1><?= Html::encode($model->name) ?></h1>

    <?= $this->render('../tabs', [
        'model' => $model,
    ]) ?>

    <p>
        <?php Modal::begin([
            'header' => '<h2>Create Domain</h2>',
            'toggleButton' => ['label' => 'Create', 'class' => 'btn btn-success'],
        ]);

        echo $this->render('/review/_form', [
            'model' => new \common\models\Review(['site_id' => $model->id]),
            'back' => \yii\helpers\Url::to()
        ]);

        Modal::end();
        ?>
    </p>

    <?php echo $this->render('_search', ['model' => $searchModel, 'site' => $model]); ?>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'col-md-4'],
        'itemView' => '_item',
    ]); ?>

</div>
