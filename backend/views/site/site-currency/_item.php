<?php

use yii\bootstrap\Html;

/* @var $model common\models\SiteCurrency */

?>

<span>
    <?= $model->currency->name ?>
    <?= Html::a(Html::icon('trash'), ['/site-currency/delete', 'site_id' => $model->site_id, 'currency_code' => $model->currency_code, 'back' => \yii\helpers\Url::to()], [
        'class' => 'btn btn-xs btn-danger',
        'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
        ],
    ]) ?>
</span>
