<?php

use yii\bootstrap\Html;

/* @var $model common\models\SiteGame */

?>

<span>
    <?= Html::a($model->game->name,['/game/view','id'=>$model->game->id]); ?>
    <?= $model->is_promoted ? Html::a(Html::icon('arrow-up'),'#',['class'=>'btn btn-xs btn-info','style'=>'cursor: default;']) : '';?>
    <?= Html::a(Html::icon('trash'), ['/site-game/delete', 'site_id' => $model->site_id, 'game_id' => $model->game_id, 'back' => \yii\helpers\Url::to()], [
        'class' => 'btn btn-xs btn-danger',
        'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
        ],
    ]) ?>
</span>
