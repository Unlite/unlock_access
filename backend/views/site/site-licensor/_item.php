<?php

use yii\bootstrap\Html;

/* @var $model common\models\SiteLocale */

?>

<span>
    <?= $model->licensor->name ?>
    <?= Html::a(Html::icon('trash'), ['/site-licensor/delete', 'site_id' => $model->site_id, 'licensor_id' => $model->licensor_id, 'back' => \yii\helpers\Url::to()], [
        'class' => 'btn btn-xs btn-danger',
        'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
        ],
    ]) ?>
</span>
