<?php

use yii\bootstrap\Html;

/* @var $model common\models\SiteLocale */

?>

<span>
    <?= $model->locale->name ?>
    <?= Html::a(Html::icon('trash'), ['/site-locale/delete', 'site_id' => $model->site_id, 'locale_code' => $model->locale_code, 'back' => \yii\helpers\Url::to()], [
        'class' => 'btn btn-xs btn-danger',
        'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
        ],
    ]) ?>
</span>
