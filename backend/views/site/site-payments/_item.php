<?php

use yii\bootstrap\Html;

/* @var $model common\models\SitePayment */

?>

<span>
    <?php if($model->is_deposit) echo Html::tag('span','D',['class'=>'btn btn-xs btn-success'])?>
    <?php if($model->is_withdrawal) echo Html::tag('span','W',['class'=>'btn btn-xs btn-warning'])?>
    <?= Html::a($model->payment->name,['payment/view','id'=>$model->payment_id]) ?>
    <?= Html::a(Html::icon('pencil'), ['site-payment/update/','site_id'=>$model->site_id,'payment_id'=>$model->payment_id, 'back' => \yii\helpers\Url::to()], [
        'class' => 'btn btn-xs btn-info',
        'data' => [
            'method' => 'post',
        ],
    ]) ?>
    <?= Html::a(Html::icon('trash'), ['/site-payment/delete', 'site_id' => $model->site_id, 'payment_id' => $model->payment_id, 'back' => \yii\helpers\Url::to()], [
        'class' => 'btn btn-xs btn-danger',
        'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
        ],
    ]) ?>
</span>
