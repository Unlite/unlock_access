<?php

use yii\bootstrap\Html;

/* @var $model common\models\SiteRelations */
/* @var $site common\models\Site */
$relationSite = $model->site_id_l !== $site->id ? $model->siteL : $model->siteH;
?>

<span>
    <?= Html::a($relationSite->name, ['/site/view', 'id' => $relationSite->id]) ?>
    <?= Html::a(Html::icon('trash'), ['/site-relations/delete', 'site_id_l' => $model->site_id_l, 'site_id_h' => $model->site_id_h, 'back' => \yii\helpers\Url::to()], [
        'class' => 'btn btn-xs btn-danger',
        'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
        ],
    ]) ?>
</span>
