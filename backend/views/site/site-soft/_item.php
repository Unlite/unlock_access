<?php

use yii\bootstrap\Html;

/* @var $model common\models\SiteSoft */

?>

<span>
    <?= $model->soft->name ?>
    <?= Html::a(Html::icon('trash'), ['/site-soft/delete', 'site_id' => $model->site_id, 'soft_id' => $model->soft_id, 'back' => \yii\helpers\Url::to()], [
        'class' => 'btn btn-xs btn-danger',
        'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
        ],
    ]) ?>
</span>
