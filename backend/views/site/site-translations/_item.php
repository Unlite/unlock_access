<?php

use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\widgets\DetailView;
use yii\widgets\ListView;

/* @var $model common\models\SiteTranslation */
?>
<div class="site-translation">
    <div class="panel panel-primary">
        <div class="panel-heading"><?= Html::encode($model->geo->name) ?></div>
        <div class="panel-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
//                    [
//                        'label' => 'Site',
//                        'value' => \yii\bootstrap\Html::a($model->site->name, ['/site/view', 'id' => $model->site->id]),
//                        'format' => 'Html'
//                    ],
                    'description',
                    'promo_text',
                ],
            ]) ?>
        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-md-6">
                </div>
                <div class="col-md-6 text-right">
                    <?= Html::a(Html::icon('eye-open'), ['/site-translation/view', 'site_id' => $model->site_id, 'geo_code' => $model->geo_code], ['class' => 'btn btn-primary']); ?>
                    <?= Html::a(Html::icon('pencil'), ['/site-translation/update', 'site_id' => $model->site_id, 'geo_code' => $model->geo_code, 'back' => \yii\helpers\Url::to()], ['class' => 'btn btn-primary']); ?>
                    <?= Html::a(Html::icon('trash'), ['/site-translation/delete', 'site_id' => $model->site_id, 'geo_code' => $model->geo_code, 'back' => \yii\helpers\Url::to()], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>