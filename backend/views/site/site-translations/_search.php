<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\DomainSearch */
/* @var $site common\models\Site */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="site-translations-search">

    <?php $form = ActiveForm::begin([
        'action' => ['/site/translations', 'id' => $site->id],
        'method' => 'get',
        'options' => ['class' => 'form-inline']
    ]); ?>

    <?= $form->field($model, 'geo_code')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Geo::find()->all(), 'code', 'name'), ['prompt' => '']) ?>

    <?= $form->field($model, 'description') ?>

    <?= $form->field($model, 'promo_text') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
