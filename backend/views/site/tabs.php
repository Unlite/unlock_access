<?php

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

/* @var $this yii\web\View */
/* @var $model common\models\Site */

NavBar::begin([
    'options' => [
        'class' => 'nav-tabs nav-justified',
    ],
]);
$menuItems = [
    ['label' => 'Info', 'url' => ['/site/view', 'id' => $model->id]],
    ['label' => 'Domains', 'url' => ['/site/domains', 'id' => $model->id]],
    ['label' => 'Images', 'url' => ['/site/images', 'id' => $model->id]],
    ['label' => 'Translations', 'url' => ['/site/translations', 'id' => $model->id]],
    ['label' => 'Reviews', 'url' => ['/site/reviews', 'id' => $model->id]],
    ['label' => 'Bonuses', 'url' => ['/site/bonuses', 'id' => $model->id]],
];
if ($model->type == \common\models\Site::TYPE_CASINO) $menuItems[] = ['label' => 'Info ' . $model->type, 'url' => ['/site/type-info', 'id' => $model->id]];
echo Nav::widget([
    'options' => ['class' => 'navbar-nav nav-tabs nav-justified'],
    'items' => $menuItems,
]);
NavBar::end();