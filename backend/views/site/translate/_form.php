<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 13.05.2019
 * Time: 19:36:34

 * @var yii\web\View $this
 * @var common\models\Site $model
 * @var string $locale_code
 */

?>
<div class="grid-container">
	<div class="grid-header">
		<div class="grid-column">Key</div>
		<div class="grid-column">Value</div>
		<div class="grid-column">
			<?= \yii\helpers\Html::a(Yii::t('app', 'Delete'), ['translate/delete-locale-by-table', 'locale_code' => $locale_code, 'table_id' => $model->id,'back' => \yii\helpers\Url::current(),], [
				'class' => 'btn btn-danger',
				'data' => [
					'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
					'method' => 'post',
				],
			]) ?>
        </div>
	</div>
	<div class="grid-body">
		<?php foreach ($model->translateFields() as $field){
			echo $this->render('/translate/_field',[
				'model' => $model->getTranslate($field)->byLocale($locale_code)->one() ?: new \common\models\Translate([
					'table_id'=>$model->id,
					'table' => \common\models\Site::getTableSchema()->fullName,
					'key'=>$field,
					'locale_code'=>$locale_code,
				])
			]);
		}
?>

	</div>
</div>
