<?php

use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\widgets\ListView;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model common\models\Site */

$this->title = $model->name . ' | Translations';
$this->params['breadcrumbs'][] = ['label' => 'Sites', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Translations';

$locales = ['en'=>'English'];
foreach ($model->translates as $translate){
	$locales[$translate->locale->code] = $translate->locale->name;
}
$items = [];
foreach ($locales as $code => $title){
    $items[] = [
            'label' => $title,
            'content' => $this->render('_form', ['model' => $model,'locale_code'=>$code])
    ];
}
$items[] = [
        'label' => $this->render('/translate/_dropdown',['parent' => $model,'locales' => $locales]),
        'linkOptions' => ['style'=>'padding:0'],
        'encode' => false,
        'disabled' => true,
];
?>
<div class="site-translations">
    <h1><?= Html::encode($model->name) ?></h1>

    <?= $this->render('../tabs', [
        'model' => $model,
    ]) ?>

		<?=\backend\components\TranslateTabs::widget([
			'items' => $items
		]);?>
</div>
<?=$this->registerCss('
.form-group {
    margin-bottom: 0; 
}
.help-block {
    display: contents;
    margin-top: 0;
    margin-bottom: 0;
}
.grid-container * {
    font-size: 14px;
}
.grid-header {
    display:grid;
    grid-template-columns: 3fr 7fr 80px;
}
.grid-body form{
    display:grid;
    grid-template-columns: 3fr 7fr 80px;
}
.grid-header {
    font-weight: bold;
}
.grid-column{
    border: 1px solid #ddd;
    padding: 8px;

}
');?>