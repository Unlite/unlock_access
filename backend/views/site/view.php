<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model common\models\Site */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Sites', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('tabs', [
        'model' => $model,
    ]) ?>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php
    $detailView = [
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'slug',
            'predefined_rating',
            'rating',
            'expire',
            'type',
            'status',
            'aliases',
            [
                'label' => 'Allowed Countries (' . $model->getSiteGeos()->allowed()->count() . ')',
                'value' => function (\common\models\Site $model) {
                    $linkDataProvider = new \yii\data\ActiveDataProvider([
                        'query' => $model->getSiteGeos()->allowed(),
                        'pagination' => [
                            'pageSize' => 50,
                        ],
                    ]);
                    return ListView::widget([
                        'dataProvider' => $linkDataProvider,
                        'itemView' => 'site-geos/_item',
                        'emptyText' => false,
                        'summary' => '',
                    ]);
                },
                'format' => 'raw',
            ],
            [
                'label' => 'Disallowed Countries (' . $model->getSiteGeos()->allowed(false)->count() . ')',
                'value' => function (\common\models\Site $model) {
                    $linkDataProvider = new \yii\data\ActiveDataProvider([
                        'query' => $model->getSiteGeos()->allowed(false),
                        'pagination' => [
                            'pageSize' => 50,
                        ],
                    ]);
                    return ListView::widget([
                        'dataProvider' => $linkDataProvider,
                        'itemView' => 'site-geos/_item',
                        'emptyText' => false,
                        'summary' => '',
                    ]);
                },
                'format' => 'raw',
            ],
        ],
    ];
    $detailViewInfo = [
        'model' => $model->info,
        'attributes' => [
        ],
    ];
    if ($model->type == \common\models\Site::TYPE_CASINO) {
        $detailViewInfo['attributes'][] = 'casino_max_pay';
        $detailViewInfo['attributes'][] = 'casino_max_win';
        $detailViewInfo['attributes'][] = 'min_deposit';
        $detailViewInfo['attributes'][] = 'min_withdraw';
        $detailViewInfo['attributes'][] = 'max_withdraw';
        $detailViewInfo['attributes'][] = 'payout';
        $detailViewInfo['attributes'][] = 'online_since';
        $detailViewInfo['attributes'][] = 'owner';
        $detailViewInfo['attributes'][] = 'email';
        $detailViewInfo['attributes'][] = 'support_types';
        $detailViewInfo['attributes'][] = 'page_title';
        $detailViewInfo['attributes'][] = 'page_description';
        $detailViewInfo['attributes'][] = [
            'attribute' => 'is_mobile_friendly',
            'value' => function (\common\models\SiteInfo $model) {
                return $model->is_mobile_friendly ? "Yes" : 'No';
            },
            'format' => 'raw'
        ];
        $detailViewInfo['attributes'][] = [
            'attribute' => 'best_support',
            'value' => function (\common\models\SiteInfo $model) {
                return $model->best_support ? "Yes" : 'No';
            },
            'format' => 'raw'
        ];
	}
    ?>
    <div class="row">
        <div class="col-md-6 .col-sm-12 col-xs-12">
            <?= DetailView::widget($detailView) ?>
        </div>
        <div class="col-md-6 .col-sm-12 col-xs-12">
            <?= DetailView::widget($detailViewInfo) ?>
        </div>
    </div>

    <span>
        <?php Modal::begin([
            'header' => '<h2>Add Allowed Geo</h2>',
            'toggleButton' => ['label' => 'Add Allowed Geo', 'class' => 'btn btn-success'],
        ]);

        echo $this->render('/site-geo/_form-multi', [
            'site' => $model,
            'model' => new \backend\models\forms\SiteGeoMultiForm(['site_id' => $model->id, 'is_allowed' => true]),
            'back' => \yii\helpers\Url::to()
        ]);

        Modal::end();
        ?>
        <?php Modal::begin([
            'header' => '<h2>Add Disallowed Geo</h2>',
            'toggleButton' => ['label' => 'Add Disallowed Geo', 'class' => 'btn btn-success'],
        ]);

        echo $this->render('/site-geo/_form-multi', [
            'site' => $model,
            'model' => new \backend\models\forms\SiteGeoMultiForm(['site_id' => $model->id, 'is_allowed' => false]),
            'back' => \yii\helpers\Url::to()
        ]);

        Modal::end();
        ?>
    </span>
</div>
