<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SoftSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Softs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="soft-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Soft', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'name',
                'value' => function (\common\models\Soft $model, $key, $index, $column) {
                    return \yii\bootstrap\Html::a($model->name, ['/soft/view', 'id' => $model->id]);
                },
                'format' => 'Html',

            ],
            [
                'attribute' => 'img_src',
                'value' => function (\common\models\Soft $model, $key, $index, $column) {
                    return \yii\bootstrap\Html::img($model->img_src, ['class' => 'img-circle', 'width' => '75px', 'height' => '75px']);
                },
                'format' => 'Html',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
