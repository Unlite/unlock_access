<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Soft */

$this->title = 'Update Soft: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Softs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="soft-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
