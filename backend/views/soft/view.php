<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Soft */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Softs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="soft-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'attribute' => 'img_src',
                'value' => function (\common\models\Soft $model, $widget) {
                    return \yii\bootstrap\Html::img($model->img_src, ['class' => 'img-circle', 'width' => '75px', 'height' => '75px']);
                },
                'format' => 'Html',
            ],
        ],
    ]) ?>

</div>
