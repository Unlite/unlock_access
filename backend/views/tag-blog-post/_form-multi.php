<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\TagBlogPost;

/* @var $this yii\web\View */
/* @var $blog_post common\models\BlogPost|null */
/* @var $model backend\models\forms\TagBlogPostMultiForm*/
/* @var $form yii\widgets\ActiveForm */
/* @var $back null|string */

$action = ['/tag-blog-post/create-multi-blog-post-tags'];
if (isset($back)) {
    $action['back'] = $back;
}
/* @var $tag common\models\Tag[]|null */
$tag = \common\models\Tag::find()->where(['not in', 'id', !empty($blog_post) ? ArrayHelper::getColumn($blog_post->tags, 'id') : []])->all()
?>

<div class="site-payment-form">

    <?php $form = ActiveForm::begin(['action' => $action]); ?>

    <?= $form->field($model, 'blog_post_id')->dropDownList(ArrayHelper::map(\common\models\BlogPost::find()->all(), 'id', 'title')) ?>

    <?= $form->field($model, 'tag_ids[]')->widget(\kartik\select2\Select2::className(),[
        'data' => ArrayHelper::map($tag, 'id', 'name'),
        'options' => ['placeholder' => 'Select Tags...', 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Add', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
