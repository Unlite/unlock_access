<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TagBlogPost */

$this->title = Yii::t('app', 'Create Tag Blog Post');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tag Blog Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tag-blog-post-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
