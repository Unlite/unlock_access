<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TagBlogPost */

$this->title = Yii::t('app', 'Update Tag Blog Post: {nameAttribute}', [
    'nameAttribute' => $model->tag_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tag Blog Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tag_id, 'url' => ['view', 'tag_id' => $model->tag_id, 'blog_post_id' => $model->blog_post_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="tag-blog-post-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
