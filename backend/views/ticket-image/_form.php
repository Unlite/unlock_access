<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TicketImage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ticket-image-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ticket_message_id')->textInput() ?>

    <?= $form->field($model, 'src')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
