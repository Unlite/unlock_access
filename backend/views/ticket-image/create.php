<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TicketImage */

$this->title = 'Create Ticket Image';
$this->params['breadcrumbs'][] = ['label' => 'Ticket Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-image-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
