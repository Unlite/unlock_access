<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TicketMessage */

$this->title = 'Create Ticket Message';
$this->params['breadcrumbs'][] = ['label' => 'Ticket Messages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-message-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
