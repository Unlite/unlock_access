<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 13.05.2019
 * Time: 20:18:02
 * @var \yii\base\Model $parent
 * @var string[] $fields
 * @var string[] $locales
 */
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
$fields = $parent->translateFields();
if(!empty($fields)){
$model = new \common\models\Translate([
	'table_id'=>$parent->id,
	'table' => $parent::getTableSchema()->fullName,
	'key'=>array_pop($fields),
]);
?>
		<?php $form = ActiveForm::begin(['options' => ['class'=>'container-fluid','style'=>'display: inline-table;']]); ?>
		<div class="input-group">
				<?= $form->field($model, 'id')->hiddenInput()->label(false) ?>
				<?= $form->field($model, 'table_id')->hiddenInput()->label(false) ?>
				<?= $form->field($model, 'table')->hiddenInput()->label(false) ?>
				<?= $form->field($model, 'locale_code')->dropDownList(ArrayHelper::map(\common\models\Locale::find()->andWhere(['not in','code',array_keys($locales)])->orderByName()->all(),'code','name'))->label(false) ?>
				<?= $form->field($model, 'key')->hiddenInput(['maxlength' => true])->label(false) ?>

				<span class="input-group-btn">
					<?= Html::submitButton('<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>', ['class' => 'btn btn-primary']) ?>
				</span>
		</div>
		<?php ActiveForm::end(); ?>

<?php } ?>
