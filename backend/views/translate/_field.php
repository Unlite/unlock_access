<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 13.05.2019
 * Time: 16:04:59
 * @var \common\models\Translate $model
 */
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<?php $form = ActiveForm::begin(); ?>
    <div class="grid-column">
		<?= $form->field($model, 'id')->hiddenInput()->label(false) ?>
		<?= $form->field($model, 'table_id')->hiddenInput()->label(false) ?>
		<?= $form->field($model, 'table')->hiddenInput()->label(false) ?>
		<?= $form->field($model, 'locale_code')->hiddenInput(['maxlength' => true])->label(false) ?>
		<?= $form->field($model, 'key')->textInput(['maxlength' => true,'readonly' => true])->label(false) ?>
    </div>
    <div class="grid-column">
		<?= $form->field($model, 'value')->textarea(['rows' => 2])->label(false) ?>
    </div>
    <div class="grid-column">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
<?php ActiveForm::end(); ?>

