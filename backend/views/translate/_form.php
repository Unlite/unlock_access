<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Translate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="translate-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'locale_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'table_id')->textInput() ?>

    <?= $form->field($model, 'table')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'key')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'value')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'serialized')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
