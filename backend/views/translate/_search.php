<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\TranslateSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="translate-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'locale_code') ?>

    <?= $form->field($model, 'table_id') ?>

    <?= $form->field($model, 'table') ?>

    <?= $form->field($model, 'key') ?>

    <?php // echo $form->field($model, 'value') ?>

    <?php // echo $form->field($model, 'serialized') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
