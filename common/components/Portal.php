<?php
namespace common\components;
use common\models\Settings;
use yii\helpers\ArrayHelper;

/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 31.07.2018
 * Time: 15:19:04
 */
class Portal
{
	/**
	 * @var \common\models\Settings[] Array of Settings models
	 */
	public $settings;

	public function __construct()
	{
		$this->settings = ArrayHelper::index(Settings::find()->all(),'key');
	}

	public function get($key)
	{
		return ArrayHelper::getValue($this->settings,$key);
	}

	public function deleteAll()
	{
		foreach ($this->settings as $setting){
			$setting->delete();
		}
	}
}