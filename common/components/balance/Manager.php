<?php

namespace common\components\balance;


use common\models\CurrencyRelations;
use common\models\Exchange;
use common\models\Transfer;
use common\models\UserCurrency;
use common\models\WithdrawTransfer;
use yii\base\Component;
use yii\base\Exception;
use yii\base\InvalidArgumentException;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class Manager extends Component implements ManagerInterface
{

    const EVENT_BEFORE_CREATE_TRANSFER = 'beforeCreateTransfer';
    const EVENT_AFTER_CREATE_TRANSFER = 'afterCreateTransfer';
    public $dbTransactions = [];
    /** @var int[] array of child IDs */
    private $revertChildIds = [];

    /**
     * @param $account
     * @param float $amount
     * @param array $data
     * @param bool $approved
     * @return int
     * @throws \Exception
     */
    public function increase($account, float $amount, array $data = [], $approved = true)
    {
        $this->beginDbTransaction();
        try {
            $account = $this->fetchAccount($account);

            $data['amount'] = $amount;
            $data['user_currency_id'] = $account->id;
            $data['status'] = $approved ? Transfer::STATUS_APPROVED : Transfer::STATUS_HOLD;

            $data = $this->beforeCreateTransaction($account, $data);

            $this->incrementAccountBalance($account->id, $amount);

            $transactionId = $this->createTransfer($data);

            $this->afterCreateTransaction($transactionId, $account, $data);

            $this->commitDbTransaction();
            return $transactionId;
        } catch (\Exception $e) {
            $this->rollBackDbTransaction();
            throw $e;
        }
    }

    /**
     * @param $account
     * @param float $amount
     * @param array $data
     * @param bool $approved
     * @return int
     * @throws \Exception
     */
    public function decrease($account, float $amount, array $data = [], $approved = true)
    {
        return $this->increase($account, -$amount, $data, $approved);
    }

    /**
     * @param $from_account
     * @param $to_account
     * @param float $amount
     * @param array $data
     * @return array
     * @throws \Exception
     */
    public function transfer($from_account, $to_account, float $amount, array $data = [])
    {
        $this->beginDbTransaction();
        try {
            $from = $this->fetchAccount($from_account);
            $to = $this->fetchAccount($to_account);

            $exchange_rate = 1;
            $exchange_commission = 0;

            if ($from->currency_code != $to->currency_code) {
                $currencyRelation = CurrencyRelations::findByCodes($from->currency_code, $to->currency_code)->asArray()->one();

                $exchange_rate = $currencyRelation['value'];
                $exchange_commission = $currencyRelation['exchange_commission'];
            }

            $commission_amount = $amount * $exchange_commission;
            $exchange_amount = ($amount - $commission_amount) * $exchange_rate;

            if ($exchange_rate != 1)
                $data['exchange_rate'] = $exchange_rate;
            if ($exchange_commission)
                $data['exchange_commission'] = $exchange_commission;

            $fromData = $data;
            $toData = $data;
            $commissionData = $data;

            $fromTransferId = $this->decrease($from, $amount - $commission_amount, $fromData);

            $toData['parent_id'] = $fromTransferId;
            $commissionData['parent_id'] = $fromTransferId;

            $result = [
                $fromTransferId,
                $this->decrease($from, $commission_amount, $commissionData),
                $this->increase($to, $exchange_amount, $toData)
            ];
            $this->commitDbTransaction();
            return $result;
        } catch (\Exception $e) {
            $this->rollBackDbTransaction();
            throw $e;
        }
    }

    /**
     * @param $from_account
     * @param string $to_currency
     * @param float $amount
     * @return array
     * @throws \Exception
     */
    public function exchange($from_account, string $to_currency, float $amount)
    {
        $from = $this->fetchAccount($from_account);
        return $this->transfer($from, ['user_id' => $from->user_id, 'currency_code' => $to_currency], $amount, ['type' => Exchange::TYPE]);
    }

    /**
     * @param $from_account
     * @param WithdrawFormInterface&Model $form
     * @return array
     * @throws \Exception
     */
    public function withdraw($from_account, WithdrawFormInterface $form)
    {
        if (!$form->validate()) throw new \DomainException('Form not validated');

		$this->beginDbTransaction();
		try {
			$from = $this->fetchAccount($from_account);

			$commissionAmount = $form->getAmount() * $form::getCommissionRate();
			$amount = $form->getAmount();
			$total = $amount - $commissionAmount;

			$data = ArrayHelper::merge(
				$form->getExtraData() ?? [],
				[
					'commission_rate' => $form::getCommissionRate(),
					'commission_amount' => $commissionAmount,
					'final_amount' => $total,
					'payment_system' => $form::getName(),
					'payment_system_id' => $form->getId(),
					'type' => WithdrawTransfer::TYPE
				]
			);
			$fromTransferId = $this->decrease($from, $total, $data, false);

			$commissionData = $data;
			$commissionData['parent_id'] = $fromTransferId;
			$commissionTransferId = $this->decrease($from, $commissionAmount, $commissionData,false);
			$this->commitDbTransaction();
			return [
				$fromTransferId,
				$commissionTransferId,
			];
		} catch (\Exception $e) {
			$this->rollBackDbTransaction();
			throw $e;
		}
    }

    /**
     * @param $transfer_id
     * @param string $comment
     * @param array $approve_data
     * @throws \Exception
     */
    public function approve($transfer_id, string $comment = '', array $approve_data = [])
    {
        $this->beginDbTransaction();
        try {
            $transfer = $this->findTransfer($transfer_id);
            if (!$transfer) {
                throw new InvalidArgumentException("Unable to find transaction '{$transfer_id}'");
            }
            if ($transfer['status'] !== Transfer::STATUS_HOLD) {
                throw new Exception('You can approve only HOLD status');
            }
            $data = [
                'approve_data' => [
                    'comment' => $comment,
                    'extra' => $approve_data
                ]
            ];
            $transfer['status'] = Transfer::STATUS_APPROVED;
            $this->updateTransfer($transfer_id, ArrayHelper::merge($transfer, $data));

        } catch (\Exception $e) {
            $this->rollBackDbTransaction();
            throw $e;
        }
    }

    /**
     * @param $transfer_id
     * @param string $comment
     * @param array $reject_data
     * @throws \Exception
     */
    public function reject($transfer_id, string $comment = '', array $reject_data = [])
    {
        $transfer = $this->findTransfer($transfer_id);
        if (!$transfer) {
            throw new InvalidArgumentException("Unable to find transaction '{$transfer_id}'");
        }
        if ($transfer['status'] === Transfer::STATUS_REJECTED) {
            throw new Exception("Can't reject rejected transfer");
        }
        if ($transfer['parent_id']) {
            if (in_array($transfer['id'], $this->revertChildIds)) {
                if (($key = array_search($transfer['id'], $this->revertChildIds)) !== false) {
                    unset($this->revertChildIds[$key]);
                }
            } else {
                throw new Exception('Can\'t revert child transfer. Revert allowed only for parent transfer with id: ' . $transfer['parent_id']);
            }
        }
        $data = [
            'reject_data' => [
                'comment' => $comment,
                'extra' => $reject_data
            ]
        ];

        $this->beginDbTransaction();
        try {
            $this->incrementAccountBalance($transfer['user_currency_id'], -$transfer['amount']);
            $transfer['status'] = Transfer::STATUS_REJECTED;

            $this->updateTransfer($transfer_id, ArrayHelper::merge($transfer, $data));


            $childIDs = Transfer::find()->byParent($transfer['id'])->select(['id'])->column();
            foreach ($childIDs as $childID) {
                $this->revertChildIds[] = $childID;
                $this->reject($childID, $comment, $reject_data);
            }
            $this->commitDbTransaction();
        } catch (\Exception $e) {
            $this->rollBackDbTransaction();
            $this->revertChildIds = [];
            throw $e;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function calculateBalance($account)
    {
        $account = $this->fetchAccount($account);
        return Transfer::find()
            ->byUserCurrency($account->id)
            ->statusRejected(false)
            ->sum('amount');
    }

    /**
     * @param mixed $idOrFilter account ID or filter condition.
     * @return UserCurrency
     */
    protected function fetchAccount($idOrFilter)
    {
        if ($idOrFilter instanceof UserCurrency) return $idOrFilter;

        if (is_array($idOrFilter)) {
            if (isset($idOrFilter['user_id'], $idOrFilter['currency_code'])) {
                $account = $this->findAccount($idOrFilter);
                if (!$account) {
                    $account = $this->createAccount($idOrFilter);
                }
            } else {
                throw new InvalidArgumentException('user_id and currency_code required');
            }
        } elseif (is_integer($idOrFilter)) {
            $account = $this->findAccount($idOrFilter);
            if (!$account) {
                throw new InvalidArgumentException('Unable to find account with id: ' . $idOrFilter);
            }
        } else {
            throw new InvalidArgumentException('Invalid type of argument: ' . gettype($idOrFilter));
        }
        return $account;
    }

    /**
     * {@inheritdoc}
     */
    protected function findTransfer($id)
    {
        $model = Transfer::findOne($id);

        return $model ? $this->unserializeAttributes($model->getAttributes()) : null;
    }

    protected function findAccount($idOrFilter)
    {
        return UserCurrency::find()->andWhere($idOrFilter)->one();
    }

    protected function findAccountId($idOrFilter)
    {
        $model = UserCurrency::find()->andWhere($idOrFilter)->one();

        return $model ? $model->getPrimaryKey(false) : null;
    }

    protected function createTransfer($attributes)
    {
        $model = new Transfer();
        $model->setAttributes($this->serializeAttributes($attributes, $model->attributes()), false);
        $model->save(false);
        return $model->id;
    }

    protected function updateTransfer($transfer_id, $attributes)
    {
        $model = Transfer::findOne($transfer_id);
        unset($attributes['id']);
        $model->setAttributes($this->serializeAttributes($attributes, $model->attributes()), false);
        $model->save(false);
        return $model->id;
    }

    protected function createAccount($attributes)
    {
        $model = new UserCurrency();
        $model->setAttributes($attributes, false);
        $model->save(false);
        return $model;
    }

    protected function incrementAccountBalance($accountId, $amount)
    {
        UserCurrency::updateAllCounters(['amount' => $amount], ['id' => $accountId]);
    }

    /**
     * Begins transaction.
     */
    public function beginDbTransaction()
    {
        $this->dbTransactions[] = $this->createDbTransaction();
    }

    /**
     * Commits current transaction.
     */
    public function commitDbTransaction()
    {
        $transaction = array_pop($this->dbTransactions);
        if ($transaction !== null) {
            $transaction->commit();
        }
    }

    /**
     * Rolls back current transaction.
     */
    public function rollBackDbTransaction()
    {
        $transaction = array_pop($this->dbTransactions);
        if ($transaction !== null) {
            $transaction->rollBack();
        }
    }

    /**
     * Creates transaction instance, actually beginning transaction.
     * If transactions are not supported, `null` will be returned.
     * @return object|\yii\db\Transaction|null transaction instance, `null` if transaction is not supported.
     */
    protected function createDbTransaction()
    {
        $db = Transfer::getDb();
        if ($db->hasMethod('getTransaction') && $db->getTransaction() !== null) {
            return null;
        }
        if ($db->hasMethod('beginTransaction')) {
            return $db->beginTransaction();
        }
        return null;
    }

    // Events :

    /**
     * This method is invoked before creating transaction.
     * @param UserCurrency $account
     * @param array $data transaction data.
     * @return array adjusted transaction data.
     */
    protected function beforeCreateTransaction($account, $data)
    {
        $event = new TransferEvent([
            'account' => $account,
            'transferData' => $data
        ]);
        $this->trigger(self::EVENT_BEFORE_CREATE_TRANSFER, $event);
        return $event->transferData;
    }

    /**
     * This method is invoked after transaction has been created.
     * @param mixed $transferId transaction ID.
     * @param UserCurrency $account account ID.
     * @param array $data transaction data.
     * @return void
     */
    protected function afterCreateTransaction($transferId, $account, $data): void
    {
        $event = new TransferEvent([
            'transferId' => $transferId,
            'account' => $account,
            'transferData' => $data
        ]);
        $this->trigger(self::EVENT_AFTER_CREATE_TRANSFER, $event);
    }

    // Serialize:

    /**
     * Processes attributes to be saved in persistent storage, serializing the ones not allowed for direct storage.
     * @param array $attributes raw attributes to be processed.
     * @param array $allowedAttributes list of attribute names, which are allowed to be saved in persistent stage.
     * @return array actual attributes.
     */
    protected function serializeAttributes($attributes, $allowedAttributes)
    {
        $safeAttributes = [];
        $dataAttributes = [];

        foreach ($attributes as $name => $value) {
            if (in_array($name, $allowedAttributes, true)) {
                $safeAttributes[$name] = $value;
            } else {
                $dataAttributes[$name] = $value;
            }
        }
        if (!empty($dataAttributes)) {
            $safeAttributes['data'] = Json::encode($dataAttributes);
        }
        return $safeAttributes;
    }

    /**
     * Processes the raw entity attributes from the persistent storage, converting serialized data into
     * actual attributes list.
     * @param array $attributes raw attribute values from persistent storage.
     * @return array actual attribute values
     */
    protected function unserializeAttributes($attributes)
    {
        if (empty($attributes['data'])) {
            unset($attributes['data']);
            return $attributes;
        }
        $dataAttributes = Json::decode($attributes['data']) ?: [];
        unset($attributes['data']);
        return array_merge($attributes, $dataAttributes);
    }
}