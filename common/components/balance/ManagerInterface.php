<?php

namespace common\components\balance;

interface ManagerInterface
{
    public function increase($account, float $amount, array $data = [], $approved = true);

    public function decrease($account, float $amount, array $data = [], $approved = true);

    public function transfer($from_account, $to_account, float $amount, array $data = []);

    public function exchange($from_account, string $to_currency, float $amount);

    public function approve($transfer_id, string $comment = '', array $approve_data = []);

    public function reject($transfer_id, string $comment = '', array $reject_data = []);

    public function calculateBalance($account);
}