<?php

namespace common\components\balance;


use common\models\UserCurrency;
use yii\base\Event;

class TransferEvent extends Event
{
    /**
     * @var UserCurrency
     */
    public $account;
    /**
     * @var array transfer data.
     */
    public $transferData;
    /**
     * @var mixed transfer ID.
     */
    public $transferId;
}