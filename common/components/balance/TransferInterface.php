<?php

namespace common\components\balance;


interface TransferInterface
{
    public function transfer($from_account, $to_account, float $amount, array $data = []);
}