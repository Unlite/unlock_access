<?php

namespace common\components\balance;


interface WithdrawFormInterface
{
    public static function getName(): string;
    public function getId(): int ;

    /**
     * Percent value of commission. MoreOrEqual 0 and Less 1
     * @return float >=0 and < 1
     */
    public static function getCommissionRate(): float;

    /**
     * List of currencies codes, which available for payment system
     * @return string[]
     */
    public static function getAvailableCurrencies(): array;

    public function getAmount(): float;

    /**
     * @return array|null
     */
    public function getExtraData(): ?array;

}