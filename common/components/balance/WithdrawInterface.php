<?php

namespace common\components\balance;


interface WithdrawInterface
{
    public function withdraw();
}