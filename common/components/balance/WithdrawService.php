<?php

namespace common\components\balance;


use yii\base\Model;

class WithdrawService
{
    /**
     * @var ManagerInterface
     */
    private $manager;

    public function __construct(ManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param WithdrawFormInterface&Model $form
     */
    public function withdraw(WithdrawFormInterface $form)
    {
        if (!$form->validate()) throw new \DomainException('Form not validated');


    }
}