<?php

namespace common\components\cashback;


use common\components\cashback\base\affiliates\Affiliya;

class Futuriti extends Affiliya
{
    public $site = 1;

    /**
     * @inheritdoc
     */
    static public function getId(): string
    {
        return parent::getId() . ':' . 'Futuriti';
    }

    /**
     * @inheritdoc
     */
    public function getBrandName(): string
    {
        return 'Futuriti';
    }
}