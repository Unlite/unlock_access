<?php


namespace common\components\cashback;


use common\components\cashback\base\affiliates\LivePartners;

class Netbet extends LivePartners
{

    public static function getId(): string
    {
        return parent::getId() . ':' . 'NetBet';
    }
    public $product_id = [120, 121, 122, 124, 125, 126, 127, 128, 151, 152, 153];
}