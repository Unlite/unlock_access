<?php


namespace common\components\cashback\base;

use common\components\cashback\models\Cashback;
use yii\base\Exception;
use yii\base\Component;

abstract class BaseCashback extends Component implements CashbackInterface
{
    /** @var \common\components\cashback\models\Cashback */
    public $cashbackModel;

    public function init()
    {
        if(!($this->cashbackModel instanceof Cashback)) throw new Exception("cashbackModel must be instance of Cashback model");
        parent::init();
    }

    public function parseDate($date)
    {
        return \Yii::$app->formatter->asTimestamp($date);
    }
}