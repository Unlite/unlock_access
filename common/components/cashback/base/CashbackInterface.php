<?php


namespace common\components\cashback\base;


interface CashbackInterface
{
    /**
     * @param $day
     * @return DataLoader
     */
    public function takeStatsByDay($day): DataLoader;

    /**
     * @param $month
     * @return DataLoader
     */
    public function takeStatsByMonth($month): DataLoader;

    /**
     * @param $start
     * @param $end
     * @return DataLoader
     */
    public function takeStatsByPeriod($start, $end): DataLoader;

    /**
     * @param $day
     * @return DataLoader
     */
    public function takeCustomersByDay($day): DataLoader;

    /**
     * @param $month
     * @return DataLoader
     */
    public function takeCustomersByMonth($month): DataLoader;

    /**
     * Return ID from Cashback table
     * @return string
     */
    public static function getId(): string;

}