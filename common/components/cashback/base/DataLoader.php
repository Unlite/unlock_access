<?php


namespace common\components\cashback\base;


use common\components\cashback\models\Cashback;
use common\components\cashback\models\CashbackCustomer;
use common\components\cashback\models\CashbackStat;
use common\models\Client;
use yii\base\Component;
use yii\base\InvalidConfigException;

class DataLoader extends Component
{
    /** @var \common\components\cashback\models\Cashback */
    public $model;
    public $period;
    public $date_start;
    public $date_end;
    public $calc = false;
    public $data;
    private $inserted = 0;
    private $failed = 0;
    private $updated = 0;

    public function init()
    {
        parent::init();
        if (!($this->model instanceof Cashback)) {
            throw new InvalidConfigException('The "model" property must be instance of ' . Cashback::className() . '.');
        }
    }

    public function saveStats()
    {
        foreach ($this->data as $row) {
            if (isset($row['net_revenue'], $row['customer_id'], $row['currency_code']) && !is_null($row['net_revenue']) && $row['customer_id'] && $row['currency_code']) {
                $q = CashbackStat::find()->where([
                    'customer_id' => $row['customer_id'],
                    'period' => $this->period,
                    'date_start' => $this->date_start,
                    'currency_code' => $row['currency_code']
                ]);
                if ($this->period == CashbackStat::PERIOD_CUSTOM)
                    $q->andWhere(['date_end' => $this->date_end]);
                $cashbackStat = $q->one();
                if (!$cashbackStat) $cashbackStat = new CashbackStat();

                $cashbackStat->attributes = $row;
                $cashbackStat->calc = $this->calc;
                $cashbackStat->cashback_id = $this->model->id;
                $cashbackStat->date_start = $this->date_start;
                $cashbackStat->period = $this->period;
                if ($this->period == CashbackStat::PERIOD_CUSTOM)
                    $cashbackStat->date_end = $this->date_end;

                if ($cashbackStat->isNewRecord) {
                    $cashbackStat->percent = $this->model->percent;
                    $cashbackStat->user_percent = $this->model->user_percent;
                }
                $cashbackStat->revenue = $cashbackStat->net_revenue * $cashbackStat->percent;
                $cashbackStat->user_revenue = $cashbackStat->revenue * $cashbackStat->user_percent;
                if ($cashbackStat->save()) {
                    if($cashbackStat->isNewRecord) $this->inserted++;
                    else $this->updated++;
                } else {
                    $this->failed++;
                    print_r($cashbackStat->getFirstErrors());
                }
            }
        }
    }

    public function saveCustomers()
    {
        foreach ($this->data as $row) {
            if (isset($row['client_token'], $row['customer_id']) && $row['client_token'] && $row['customer_id']) {
                $cashbackCustomer = CashbackCustomer::findOne(['customer_id' => $row['customer_id'], 'cashback_id' => $this->model->id]);
                if (!$cashbackCustomer) $cashbackCustomer = new CashbackCustomer();
                $cashbackCustomer->attributes = $row;
                if (!Client::find()->where(['token' => $cashbackCustomer->client_token])->count()) $cashbackCustomer->client_token = null;
                $cashbackCustomer->cashback_id = $this->model->id;
                if ($cashbackCustomer->save()) {
                    if($cashbackCustomer->isNewRecord) $this->inserted++;
                    else $this->updated++;
                } else {
                    $this->failed++;
                    print_r($cashbackCustomer->getFirstErrors());
                }
            }
        }
    }
}