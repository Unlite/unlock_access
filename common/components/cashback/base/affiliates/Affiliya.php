<?php


namespace common\components\cashback\base\affiliates;


use common\components\cashback\base\BaseCashback;
use common\components\cashback\base\DataLoader;
use common\components\cashback\models\CashbackStat;
use yii\httpclient\Client;
use yii\httpclient\Response;

/**
 * Class Affiliya
 * @property string $brandName
 */
abstract class Affiliya extends BaseCashback
{
    //http://affiliates.affiliya.com/statistics-ext.php?p=&d1=2017-11-01&d2=2017-11-30&pdd=customer&cg=&c=&m=&o=&s=&jd1=&jd2=&mode=csv&sbm=1&auth=basic&dnl=1
    const API_URL = 'https://secure.commissionlounge.com';

    const API_PATH_STATS = 'statistics-ext.php';
    const API_PATH_CUSTOMERS = 'reports-all-players.php';

    const AUTH_USER = 'croupierica';
    const AUTH_PASSWORD = 'Adcombo777';

    const CURRENCY_DEFAULT = 'eur';
    const CURRENCY_ASSOC = [
        '€' => 'eur',
        '$' => 'usd',
        'eur' => 'eur',
        'usd' => 'usd',
    ];

    public $site;
    public $date_start;
    public $date_end;

    /**
     * Return brand name for filtering customer registrations
     * @return string
     */
    abstract public function getBrandName(): string;

    static public function getId(): string
    {
        return 'Affiliya';
    }

    /**
     * @inheritdoc
     */
    function takeCustomersByDay($day = 'yesterday'): DataLoader
    {
        $this->setPeriod($day);
        return new DataLoader([
            'model' => $this->cashbackModel,
            'period' => CashbackStat::PERIOD_DAY,
            'date_start' => $this->date_start,
            'data' => $this->prepareCustomers($this->takeData(self::API_PATH_CUSTOMERS))
        ]);
    }

    /**
     * @inheritdoc
     */
    public function takeCustomersByMonth($month = 'first day of last month'): DataLoader
    {
        $this->setPeriod($month, 'last day of this month');
        return new DataLoader([
            'model' => $this->cashbackModel,
            'period' => CashbackStat::PERIOD_MONTH,
            'date_start' => $this->date_start,
            'data' => $this->prepareCustomers($this->takeData(self::API_PATH_CUSTOMERS))
        ]);
    }

    /**
     * @inheritdoc
     */
    public function takeStatsByDay($day = 'yesterday'): DataLoader
    {
        $this->setPeriod($day);
        return new DataLoader([
            'model' => $this->cashbackModel,
            'period' => CashbackStat::PERIOD_DAY,
            'date_start' => $this->date_start,
            'calc' => true,
            'data' => $this->prepareStats($this->takeData(self::API_PATH_STATS))
        ]);
    }

    /**
     * @inheritdoc
     */
    public function takeStatsByMonth($month = 'first day of last month'): DataLoader
    {
        $this->setPeriod($month, 'last day of this month');
        $dataLoader = new DataLoader([
            'model' => $this->cashbackModel,
            'period' => CashbackStat::PERIOD_MONTH,
            'date_start' => $this->date_start,
            'data' => $this->prepareStats($this->takeData(self::API_PATH_STATS))
        ]);
        return $dataLoader;
    }

    /**
     * @inheritdoc
     */
    public function takeStatsByPeriod($start = 'first day of this month', $end = 'today'): DataLoader
    {
        $this->date_start = $this->parseDate($start);
        $this->date_end = $this->parseDate($end);
        return new DataLoader([
            'model' => $this->cashbackModel,
            'period' => CashbackStat::PERIOD_CUSTOM,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
            'data' => $this->prepareStats($this->takeData(self::API_PATH_STATS))
        ]);
    }

    public function takeData($path)
    {
        $client = new Client([
            'baseUrl' => self::API_URL,
            'responseConfig' => [
                //'format' => Client::FORMAT_URLENCODED
            ],
            'transport' => 'yii\httpclient\CurlTransport',
        ]);
        $request = $client->createRequest()
            ->setUrl($path)
            ->setOptions([
                CURLOPT_USERPWD => self::AUTH_USER . ':' . self::AUTH_PASSWORD,
				CURLOPT_SSL_VERIFYPEER => true,
            ])
            ->setData([
                'd1' => $this->date_start,
                'd2' => $this->date_end,
                'pdd' => 'customer',
                'mode' => 'csv',
                'sbm' => 1,
                'dnl' => 1,
                'auth' => 'basic',
                'dnlids' => 1,
                'dnlfmt' => 1,
            ]);
//        if ($path === self::API_PATH_STATS) {
        if ($this->site) {
            $request->addData(['p' => $this->site]);
        }
        /** @var Response $response */
        $response = $request->send();
        if ($response->isOk) {
            return $response->content;
        }
        throw new \yii\httpclient\Exception('Invalid Request');

    }

    /**
     * Sets date_start and date_end
     * @param string $date
     * @param string $period
     */
    public function setPeriod($date = 'today', $period = '')
    {
        $this->date_start = $this->parseDate($date);
        $this->date_end = $this->parseDate($this->date_start . ' ' . $period);
    }

    /**
     * @inheritdoc
     */
    public function parseDate($date)
    {
        $ts = parent::parseDate($date);
        return \Yii::$app->formatter->asDate($ts, 'yyyy-MM-dd');
    }

    /**
     * @param $data
     * @return array
     */
    protected function prepareStats($data)
    {
        $lines = explode("\n", $data);
        array_shift($lines);
        $data = [];
        foreach ($lines as $csvString) {
            $values = explode(',', $csvString);
            if (count($values) <= 1) continue;
            //План,План ID,Период оплаты,Группа игроков,Игрок,Игрок ID
            //Impressions,Clicks,Deposits,First Deposits,
            //deposit count,first deposit count,Payouts,Poker Net Revenue,
            //Net Revenue,Affilate Income,Доход
            list($plan, $plan_id, $pay_period, $players_group, $player, $assoc['customer_id'],
                $impressions, $clicks, $nrc, $deposit_count,$assoc['first_deposits_amount'],$assoc['deposits_amount'],
//				$first_deposit_count,
//				$assoc['withdraw_amount'],
//				$poker_net_revenue,
                $assoc['net_revenue'], $affilate_income) = $values;
            if ($assoc['customer_id'] == 'Unknown' || empty($assoc['customer_id'])) continue;
            foreach ($assoc as $key => &$value) {
                if (stripos($key, '_amount') || $key === 'net_revenue') {
                    $_value = $this->parseCurrency($value);
//                	var_dump($_value['amount'],$value);
                    $value = $_value['amount'];
                    $assoc['currency'] = $_value['currency'];
                    $assoc['currency_code'] = $_value['currency_code'];
                }
            }
            array_push($data, $assoc);
        }
        return $data;
    }

    protected function parseCurrency($value)
    {
        $data = [
            'currency' => '€',
            'currency_code' => static::CURRENCY_DEFAULT,
            'amount' => 0,
        ];
        $value = str_replace([' ', ','], '', $value);
        $length = mb_strlen($value);
        $nStart = 0;
        $nEnd = $length - 1;
        while (!is_numeric(mb_substr($value, $nStart, 1)) && $nStart < $length) {
            ++$nStart;
        }
        while (!is_numeric(mb_substr($value, $nEnd, 1)) && $nEnd > 0) {
            --$nEnd;
        }
        $data['amount'] = floatval(mb_substr($value, $nStart, $nEnd - $nStart + 1));
        if($value[0] == '-'){
        	$data['amount'] = -$data['amount'];
		}
        $currency = mb_substr($value, 0, $nStart) . mb_substr($value, $nEnd + 1, $length - $nEnd - 1);

        if (array_key_exists($currency, static::CURRENCY_ASSOC)) {
            $data['currency'] = $currency;
            $data['currency_code'] = static::CURRENCY_ASSOC[$currency];
        }
        return $data;
    }

    protected function prepareCustomers($data)
    {
        $lines = explode("\n", $data);
        array_shift($lines);
        $data = [];
        foreach ($lines as $csvString) {
            $values = explode(',', $csvString);
            if (count($values) <= 1) continue;
            //Игрок,Игрок ID,Дата,Неподтвержденный,Страна,Страна ID,
            //URL,Payload,План,План ID,Бренд,Лендинг,
            //Лендинг ID,Группы кампаний,Группы кампаний ID,Кампания,Кампания ID,Промо,
            //Промо  ID
            list($player, $assoc['customer_id'], $assoc['register_date'], $unconfirmed, $country, $country_code,
                $url, $assoc['client_token'], $plan, $plan_id, $brand, $landing,
                $landing_id, $campaign_group, $campaign_group_id, $campaign, $campaign_id, $promo,
                $promo_id) = $values;
            //TODO: REMOVE THIS
//			if($assoc['customer_id'] == 'CE3351975190')
//				$assoc['client_token'] = 'wuq7kakehpcotpci0fkg7wh1bzmxuycm';

            if ($brand !== $this->brandName) continue;
            $assoc['register_date'] = $this->parseDate(\DateTime::createFromFormat('d/m/y', $assoc['register_date'])->format('Y-m-d'));
            array_push($data, $assoc);
        }
        return $data;
    }
}