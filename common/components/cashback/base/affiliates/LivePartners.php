<?php


namespace common\components\cashback\base\affiliates;

use common\components\cashback\base\BaseCashback;
use common\components\cashback\base\DataLoader;
use common\components\cashback\models\CashbackStat;
use yii\httpclient\Client;

abstract class LivePartners extends BaseCashback
{
    //https://admin.livepartners.com/stats/player-activity?start=0&limit=25&format=json&affiliate_website_id%5B%5D=347729&product_id%5B%5D=120&product_id%5B%5D=121&product_id%5B%5D=122&product_id%5B%5D=124&product_id%5B%5D=125&product_id%5B%5D=126&product_id%5B%5D=127&product_id%5B%5D=128&product_id%5B%5D=151&product_id%5B%5D=152&product_id%5B%5D=153&search_period=yesterday&apikey=vwbncniwhv58pqxgvsgwmkyf0eczu8rioat6p03r
    const API_URL = 'https://admin.livepartners.com/stats';
    const API_KEY = 'vwbncniwhv58pqxgvsgwmkyf0eczu8rioat6p03r';

    const API_PATH_STATS = 'player-activity';
    const API_PATH_CUSTOMERS = 'signed-players';

    const CURRENCY_DEFAULT = 'eur';

    public $search_period;
    private $date_start;
    private $date_end;

    public $product_id = [];
    public $affiliate_website_id = ['347729'];

    public $offset;
    public $limit;

    /**
     * @inheritdoc
     */
    public static function getId(): string
    {
        return 'LivePartners';
    }

    /**
     * @param string $day
     * @return DataLoader
     */
    public function takeStatsByDay($day = 'yesterday'): DataLoader
    {
        $this->setPeriod($day);
        return new DataLoader([
            'model' => $this->cashbackModel,
            'period' => CashbackStat::PERIOD_DAY,
            'date_start' => $this->date_start,
            'calc' => true,
            'data' => $this->prepareStats($this->takeData(self::API_PATH_STATS))
        ]);
    }

    /**
     * @param string $month
     * @return DataLoader
     */
    public function takeStatsByMonth($month = 'first day of last month'): DataLoader
    {
        $this->setPeriod($month, 'last day of this month');
        return new DataLoader([
            'model' => $this->cashbackModel,
            'period' => CashbackStat::PERIOD_MONTH,
            'date_start' => $this->date_start,
            'data' => $this->prepareStats($this->takeData(self::API_PATH_STATS))
        ]);
    }

    /**
     * @param string $start
     * @param string $end
     * @return DataLoader
     */
    public function takeStatsByPeriod($start = 'first day of this month', $end = 'today'): DataLoader
    {
        $this->search_period = 'specific_period';
        $this->date_start = $this->parseDate($start);
        $this->date_end = $this->parseDate($end);
        return new DataLoader([
            'model' => $this->cashbackModel,
            'period' => CashbackStat::PERIOD_CUSTOM,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
            'data' => $this->prepareStats($this->takeData(self::API_PATH_STATS))
        ]);
    }

    public function takeCustomersByDay($day = 'yesterday'): DataLoader
    {
        $this->setPeriod($day);
        return new DataLoader([
            'model' => $this->cashbackModel,
            'period' => CashbackStat::PERIOD_DAY,
            'date_start' => $this->date_start,
            'data' => $this->prepareCustomers($this->takeData(self::API_PATH_CUSTOMERS))
        ]);
    }

    /**
     * @inheritdoc
     */
    public function takeCustomersByMonth($month = 'first day of last month'): DataLoader
    {
        $this->setPeriod($month, 'last day of this month');
        return new DataLoader([
            'model' => $this->cashbackModel,
            'period' => CashbackStat::PERIOD_MONTH,
            'date_start' => $this->date_start,
            'data' => $this->prepareCustomers($this->takeData(self::API_PATH_CUSTOMERS))
        ]);
    }

    /**
     * @param $path
     * @return mixed
     * @throws \yii\httpclient\Exception
     */
    private function takeData($path)
    {
        $client = new Client([
            'baseUrl' => self::API_URL,
            'responseConfig' => [
                'format' => Client::FORMAT_JSON
            ],
        ]);
        $request = $client->createRequest()
            ->setUrl($path)
            ->setData([
                'apikey' => self::API_KEY,
                'format' => 'json',
                'affiliate_website_id' => $this->affiliate_website_id,
                'product_id' => $this->product_id,
                'search_period' => $this->search_period,
            ]);
        if ($this->search_period === 'specific_period') {
            $request->addData([
                'date_start' => $this->date_start,
                'date_end' => $this->date_end,
            ]);
        }
        //return json_encode($request->data);
        $response = $request->send();
        if ($response->isOk) {
            if ($response->data && isset($response->data['dataset']))
                return $response->data['dataset'];
        }
        throw new \yii\httpclient\Exception('Invalid Request');
    }

    public function parseDate($date)
    {
        $ts = parent::parseDate($date);
        return \Yii::$app->formatter->asDate($ts, 'yyyy-MM-dd');
    }

    public function setPeriod($date = 'today', $period = '')
    {
        $LPPeriods = [
            'today' => 'today',
            'yesterday' => 'yesterday',
            'this month' => 'current_month',
            'previous month' => 'last_month',
            'first day of last month' => 'last_month',
        ];
        if (isset($LPPeriods[$date])) {
            $this->search_period = $LPPeriods[$date];
            $this->date_start = $this->parseDate($date);
            $this->date_end = null;
            return;
        } else {
            $this->search_period = 'specific_period';
            $this->date_start = $this->parseDate($date);
            $this->date_end = $this->parseDate($this->date_start . ' ' . $period);
        }
    }

    /**
     * @param $data
     * @return array
     */
    private function prepareStats($data)
    {
        $attributes = [
            'player_id' => 'customer_id',
            'deposits_amount' => 'deposits_amount',
            'bonus_amount' => 'bonus_amount',
            'fees_amount' => 'fees_amount',
            'rakes_amount' => 'rakes_amount',
            'cash_bets_amount' => 'cash_bets_amount',
            'bonus_bets_amount' => 'bonus_bets_amount',
            'cash_wins_amount' => 'cash_wins_amount',
            'bonus_wins_amount' => 'bonus_wins_amount',
            'withdraw_amount' => 'withdraw_amount',
            'net_revenue' => 'net_revenue',
        ];
        return array_map(function ($row) use ($attributes) {
            $array = [];
            foreach ($attributes as $oldKey => $newKey) {
                if (array_key_exists($oldKey, $row)) {
                    $array[$newKey] = $row[$oldKey];
                }
            }
            $array['currency_code'] = static::CURRENCY_DEFAULT;
            return $array;
        }, $data);
    }

    private function prepareCustomers($data)
    {
        $attributes = [
            'player_id' => 'customer_id',
            'affiliate_custom1' => 'client_token',
            'register_date_iso' => 'register_date',
        ];
        return array_map(function ($row) use ($attributes) {
            $array = [];
            foreach ($attributes as $oldKey => $newKeys) {
                if (array_key_exists($oldKey, $row)) {
                    if (is_string($newKeys)) $array[$newKeys] = $row[$oldKey];
                    elseif (is_array($newKeys)) foreach ($newKeys as $newKey) $array[$newKey] = $row[$oldKey];
                }
            }
            return $array;
        }, $data);
    }
}