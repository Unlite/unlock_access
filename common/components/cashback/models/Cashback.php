<?php

namespace common\components\cashback\models;

use common\models\Site;
use yii\helpers\FileHelper;


/**
 * This is the model class for table "{{%cashback}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $object
 * @property integer $site_id
 * @property string $client_status
 * @property string $cron_status
 * @property string $percent
 * @property string $user_percent
 *
 * @property Site $site
 * @property CashbackStat[] $cashbackStats
 */
class Cashback extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cashback}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['object', 'percent', 'user_percent', 'name'], 'required'],
            [['site_id'], 'integer'],
            [['client_status', 'cron_status'], 'string'],
            [['percent', 'user_percent'], 'number'],
            [['name'], 'string', 'max' => 32],
            [['object'], 'string', 'max' => 128],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'object' => 'Object',
            'site_id' => 'Site ID',
            'client_status' => 'Client Status',
            'cron_status' => 'Cron Status',
            'percent' => 'Percent',
            'user_percent' => 'User Percent',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::className(), ['id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashbackStats()
    {
        return $this->hasMany(CashbackStat::className(), ['cashback_id' => 'id']);
    }
}
