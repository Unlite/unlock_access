<?php

namespace common\components\cashback\models;

use common\components\cashback\models\search\CashbackStatSearch;
use common\models\Client;
use common\models\User;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "{{%cashback_customer}}".
 *
 * @property integer $id
 * @property string $client_token
 * @property string $customer_id
 * @property integer $cashback_id
 * @property string $ip
 * @property string $created_at
 * @property string $updated_at
 * @property string $register_date
 *
 * @property float $summaryAmount
 * @property float $summaryPercent
 * @property string $currency
 *
 * @property Cashback $cashback
 * @property CashbackStat[] $cashbackStats
 * @property Client $client
 * @property User $user
 */
class CashbackCustomer extends ActiveRecord
{

	private $_summaryAmount;
	private $_summaryPercent;
	private $_currency;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cashback_customer}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'cashback_id'], 'required'],
            [['cashback_id'], 'integer'],
            [['register_date'], 'date', 'format' => 'yyyy-MM-dd'],
            [['client_token', 'customer_id'], 'string', 'max' => 32],
            [['ip'], 'string', 'max' => 15],
            [['summaryAmount','summaryPercent'],'number'],
            [['currency'],'string'],
            [['cashback_id', 'customer_id'], 'unique', 'targetAttribute' => ['cashback_id', 'customer_id'], 'message' => 'The combination of Customer ID and Cashback ID has already been taken.'],
            [['cashback_id'], 'exist', 'skipOnError' => false, 'targetClass' => Cashback::className(), 'targetAttribute' => ['cashback_id' => 'id']],
            [['client_token'], 'exist', 'skipOnError' => false, 'skipOnEmpty' => true, 'targetClass' => Client::className(), 'targetAttribute' => ['client_token' => 'token']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_token' => 'Client Token',
            'customer_id' => 'Customer ID',
            'cashback_id' => 'Cashback ID',
            'ip' => 'Ip',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'register_date' => 'Register Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashback()
    {
        return $this->hasOne(Cashback::className(), ['id' => 'cashback_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['token' => 'client_token']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id'])->via('client');
    }

	/**
	 * @return ActiveQuery;
	 */
	public function getCashbackStats()
	{
		return $this->hasMany(CashbackStat::class,['customer_id' => 'customer_id']);
    }

	public function getSummaryAmount()
	{
		$this->_summaryAmount = 0;
		foreach ($this->cashbackStats as $cashbackStat){
			$this->_summaryAmount += $cashbackStat->user_revenue;
		}
		return $this->_summaryAmount;
    }

	public function getSummaryPercent()
	{
		$this->_summaryPercent = 0;
		foreach ($this->cashbackStats as $cashbackStat){
			$this->_summaryPercent += $cashbackStat->user_percent;
		}
		$this->_summaryPercent = $this->_summaryPercent / ($this->cashbackStats ? count($this->cashbackStats) : 1);
		return $this->_summaryPercent ;
    }

	public function getCurrency()
	{
		$this->_currency = 'usd';
		if($currency_stat = $this->getCashbackStats()->one())
			$this->_currency = $currency_stat->currency_code;
		return $this->_currency ;
    }

	public function setSummaryAmount($summaryAmount)
	{
		$this->_summaryAmount = $summaryAmount;
    }

	public function setSummaryPercent($summaryPercent)
	{
		$this->_summaryPercent = $summaryPercent;
    }

	public function setCurrency($currency)
	{
		$this->_currency = $currency;
    }

}
