<?php

namespace common\components\cashback\models;

use common\models\Currency;
use common\components\cashback\models\queries\CashbackStatQuery;

/**
 * This is the model class for table "{{%cashback_stat}}".
 *
 * @property integer $id
 * @property integer $cashback_id
 * @property string $customer_id
 * @property string $currency
 * @property string $opening_balance_amount
 * @property string $fees_amount
 * @property string $banking_fees_amount
 * @property string $microgaming_fees_amount
 * @property string $rakes_amount
 * @property string $cash_bets_amount
 * @property string $bonus_bets_amount
 * @property string $cash_wins_amount
 * @property string $bonus_wins_amount
 * @property string $bonus_amount
 * @property string $deposits_amount
 * @property string $withdraw_amount
 * @property string $first_deposits_amount
 * @property string $closing_balance_amount
 * @property string $net_gaming
 * @property string $net_revenue
 * @property string $percent
 * @property string $revenue
 * @property string $user_percent
 * @property string $user_revenue
 * @property string $period
 * @property string $date_start
 * @property string $date_end
 * @property string $created_at
 * @property string $updated_at
 * @property boolean $calc
 * @property boolean $final
 * @property string $status
 * @property string $currency_code
 *
 * @property Cashback $cashback
 * @property Currency $currencyByCode
 */
class CashbackStat extends \yii\db\ActiveRecord
{
    const PERIOD_DAY = 'day';
    const PERIOD_MONTH = 'month';
    const PERIOD_CUSTOM = 'custom';

    const STATUS_APPROVED = 'approved';
    const STATUS_HOLD = 'hold';
    const STATUS_CLOSED = 'closed';
    const STATUS_REJECTED = 'rejected';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cashback_stat}}';
    }

    /**
     * @return CashbackStatQuery
     */
    public static function find()
    {
        return new CashbackStatQuery(get_called_class());
    }

    public static function periods()
    {
        return [self::PERIOD_DAY, self::PERIOD_MONTH, self::PERIOD_CUSTOM];
    }

    public static function statuses()
    {
        return [self::STATUS_APPROVED, self::STATUS_HOLD, self::STATUS_CLOSED, self::STATUS_REJECTED];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_start', 'period', 'calc', 'currency_code', 'cashback_id'], 'required'],
            [['cashback_id'], 'integer'],
            [['opening_balance_amount', 'fees_amount', 'banking_fees_amount', 'microgaming_fees_amount', 'rakes_amount', 'cash_bets_amount', 'bonus_bets_amount', 'cash_wins_amount', 'bonus_wins_amount', 'bonus_amount', 'deposits_amount', 'withdraw_amount', 'first_deposits_amount', 'closing_balance_amount', 'net_gaming', 'net_revenue', 'percent', 'revenue', 'user_percent', 'user_revenue'], 'number'],
            [['period'], 'in', 'range' => self::periods()],
            [['status'], 'in', 'range' => self::statuses()],
            [['calc', 'final'], 'boolean'],
            [['date_start', 'date_end'], 'date', 'format' => 'yyyy-MM-dd'],
            [['customer_id'], 'string', 'max' => 32],
            [['currency'], 'string', 'max' => 16],
            [['cashback_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cashback::className(), 'targetAttribute' => ['cashback_id' => 'id']],
            [['currency_code'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['currency_code' => 'code']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cashback_id' => 'Cashback ID',
            'customer_id' => 'Customer ID',
            'currency' => 'Currency',
            'opening_balance_amount' => 'Opening Balance Amount',
            'fees_amount' => 'Fees Amount',
            'banking_fees_amount' => 'Banking Fees Amount',
            'microgaming_fees_amount' => 'Microgaming Fees Amount',
            'rakes_amount' => 'Rakes Amount',
            'cash_bets_amount' => 'Cash Bets Amount',
            'bonus_bets_amount' => 'Bonus Bets Amount',
            'cash_wins_amount' => 'Cash Wins Amount',
            'bonus_wins_amount' => 'Bonus Wins Amount',
            'bonus_amount' => 'Bonus Amount',
            'deposits_amount' => 'Deposits Amount',
            'withdraw_amount' => 'Withdraw Amount',
            'first_deposits_amount' => 'First Deposits Amount',
            'closing_balance_amount' => 'Closing Balance Amount',
            'net_gaming' => 'Net Gaming',
            'net_revenue' => 'Net Revenue',
            'percent' => 'Percent',
            'revenue' => 'Revenue',
            'user_percent' => 'User Percent',
            'user_revenue' => 'User Revenue',
            'period' => 'Period',
            'date_start' => 'Date Start',
            'date_end' => 'Date End',
            'calc' => 'Is Calculated',
            'final' => 'Is Final',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public static function moneyAttributes()
    {
        return [
            'opening_balance_amount',
            'fees_amount',
            'banking_fees_amount',
            'microgaming_fees_amount',
            'rakes_amount',
            'cash_bets_amount',
            'bonus_bets_amount',
            'cash_wins_amount',
            'bonus_wins_amount',
            'bonus_amount',
            'deposits_amount',
            'withdraw_amount',
            'first_deposits_amount',
            'closing_balance_amount',
            'net_gaming',
            'net_revenue',
            'revenue',
            'user_revenue',
        ];
    }

    public static function percentAttributes()
    {
        return [
            'percent',
            'user_percent',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashback()
    {
        return $this->hasOne(Cashback::className(), ['id' => 'cashback_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencyByCode()
    {
        return $this->hasOne(Currency::className(), ['code' => 'currency_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashbackCustomer()
    {
        return $this->hasOne(CashbackCustomer::className(), ['customer_id' => 'customer_id']);
    }
}
