<?php

namespace common\components\cashback\models\queries;

use common\models\queries\traits\StatusColumnQueryTrait;
use yii\db\ActiveQuery;

/**
 * Class CashbackStatQuery
 * @package common\models\queries
 * @see \common\components\cashback\models\CashbackStat
 */
class CashbackStatQuery extends ActiveQuery
{
    use StatusColumnQueryTrait;

    // conditions appended by default (can be skipped)
    public function init()
    {
        parent::init();
    }

    public function isCalc($value = true)
    {
        return $this->andWhere(['calc' => $value]);
    }

    public function isFinal($value = true)
    {
        return $this->andWhere(['final' => $value]);
    }
}