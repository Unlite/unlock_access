<?php


namespace common\components\cashback\models\search;


use common\components\cashback\models\CashbackStat;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

class CashbackStatSearch extends Model
{
    public $client_tokens;
    public $cashback_ids;
    public $customer_ids;
    public $calc;
    public $currency_codes;
    public $group_by;

    const GROUP_BY_CASHBACK_ID = 'cashback_id';
    const GROUP_BY_CURRENCY_CODE = 'currency_code';
    const GROUP_BY_CUSTOMER_ID = 'customer_id';

    public function rules()
    {
        return [
            [['client_tokens', 'customer_ids', 'currency_codes'], 'each', 'rule' => ['string', 'length' => [1, 32]]],
            [['cashback_ids'], 'each', 'rule' => ['integer']],
            [['group_by'], 'each', 'rule' => ['in', 'range' => [
                static::GROUP_BY_CASHBACK_ID,
                static::GROUP_BY_CURRENCY_CODE,
                static::GROUP_BY_CUSTOMER_ID
            ]]],
            [['calc'], 'boolean'],
        ];
    }

    public function search($params, $formName = null)
    {
        $query = CashbackStat::find()->asArray();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params, $formName);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->innerJoinWith([
            'cashbackCustomer' => function ($query) {
                /** @var ActiveQuery $query */
                if(!$this->isEmpty($this->client_tokens))
                    $query->andOnCondition(['client_token' => $this->client_tokens]);
                return $query;
            },
            'cashback' => function ($query) {
                /** @var ActiveQuery $query */
                if(!$this->isEmpty($this->cashback_ids))
                    $query->andOnCondition(['cashback_id' => $this->cashback_ids]);
                return $query;
            }
        ]);

        if(!$this->isEmpty($this->group_by)){
            $query->groupBy(array_unique($this->group_by));
        }

        $query->andFilterWhere([
            'calc' => $this->calc,
            'currency_code' => $this->currency_codes,
            'customer_id' => $this->customer_ids,
        ]);
        return $dataProvider;
    }

    protected function isEmpty($value)
    {
        return $value === '' || $value === [] || $value === null || is_string($value) && trim($value) === '';
    }
}