<?php

namespace common\components\cashback\repositories;

use common\components\cashback\models\CashbackStat;
use yii\db\Expression;
use yii\db\Query;

class CashbackStatRepository
{

    /**
     * @param \DateTimeImmutable $date_start - first day of month
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public static function getMonthGroupSummary(\DateTimeImmutable $date_start, int $limit = 200, int $offset = 0)
    {
        $date_end = $date_start->setDate($date_start->format('Y'), $date_start->format('m'), $date_start->format('t'));
        $query = (new Query())
            ->select(static::selectSummaryFields())
            ->from('{{%cashback_stat}}')
            ->where([
                'and',
                ['calc' => 1],
                [
                    'or',
                    ['final' => 0],
                    ['final' => null]
                ],
                [
                    'between',
                    'date_start',
                    new Expression("CAST('" . $date_start->format('Y-m-d') . "' AS DATE)"),
                    new Expression("CAST('" . $date_end->format('Y-m-d') . "' AS DATE)")
                ],
            ])
            ->groupBy(['customer_id', 'cashback_id', 'currency_code', 'month'])
            ->offset($offset)
            ->limit($limit);
        return $query->all();
    }

    /**
     * @param \DateTimeImmutable $date_start - first day of month
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public static function getMonthSummary(\DateTimeImmutable $date_start, int $limit = 200, int $offset = 0)
    {
        $date_end = $date_start->setDate($date_start->format('Y'), $date_start->format('m'), $date_start->format('t'));
        $query = (new Query())
            ->select(static::selectSummaryFields())
            ->from('{{%cashback_stat}}')
            ->where([
                'and',
                ['calc' => 0],
                [
                    'or',
                    ['final' => 0],
                    ['final' => null]
                ],
                ['period' => CashbackStat::PERIOD_MONTH],
                [
                    'between',
                    'date_start',
                    new Expression("CAST('" . $date_start->format('Y-m-d') . "' AS DATE)"),
                    new Expression("CAST('" . $date_end->format('Y-m-d') . "' AS DATE)")
                ],
            ])
            ->groupBy(['customer_id', 'cashback_id', 'currency_code', 'month'])
            ->offset($offset)
            ->limit($limit);
        return $query->all();
    }

    /**
     * @param \DateTimeImmutable $date_start - first day of month
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public static function getFinalSummary(\DateTimeImmutable $date_start, int $limit = 200, int $offset = 0)
    {
        $date_end = $date_start->setDate($date_start->format('Y'), $date_start->format('m'), $date_start->format('t'));
        $query = (new Query())
            ->select(static::selectSummaryFields())
            ->from('{{%cashback_stat}}')
            ->where([
                'and',
				['final' => 1],
                ['period' => CashbackStat::PERIOD_MONTH],
                ['status' => CashbackStat::STATUS_HOLD],
                [
                    'between',
                    'date_start',
                    new Expression("CAST('" . $date_start->format('Y-m-d') . "' AS DATE)"),
                    new Expression("CAST('" . $date_end->format('Y-m-d') . "' AS DATE)")
                ],
            ])
            ->groupBy(['customer_id', 'cashback_id', 'currency_code', 'month'])
            ->offset($offset)
            ->limit($limit);
        return $query->all();
    }

    /**
     * @param \DateTimeImmutable $date_start - first day of month
     * @param int $cashback_id
     * @param string $customer_id
     * @param string $currency_code
     * @return array|bool
     */
    public static function getMonthIndividualSummary(\DateTimeImmutable $date_start, int $cashback_id, string $customer_id, string $currency_code)
    {
        $date_end = $date_start->setDate($date_start->format('Y'), $date_start->format('m'), $date_start->format('t'));
        return (new Query())
            ->select(static::selectSummaryFields())
            ->from('{{%cashback_stat}}')
            ->where([
                'and',
                ['cashback_id' => $cashback_id],
                ['customer_id' => $customer_id],
                ['final' => 1],
                [
                    'or',
                    ['calc' => 0],
                    ['calc' => null]
                ],
                ['currency_code' => $currency_code],
                [
                    'between',
                    'date_start',
                    new Expression("CAST('" . $date_start->format('Y-m-d') . "' AS DATE)"),
                    new Expression("CAST('" . $date_end->format('Y-m-d') . "' AS DATE)")
                ]
            ])
            ->groupBy(['customer_id', 'cashback_id', 'currency_code', 'month'])
            ->one();
    }

    protected static function selectSummaryFields($tableAlias = '', $resultAlias = '')
    {
        $result = [
            (($resultAlias ? ($resultAlias . '.') : '') . 'currency_code') => ($tableAlias ? ($tableAlias . '.') : '') . 'currency_code',
            (($resultAlias ? ($resultAlias . '.') : '') . 'cashback_id') => ($tableAlias ? ($tableAlias . '.') : '') . 'cashback_id',
            (($resultAlias ? ($resultAlias . '.') : '') . 'customer_id') => ($tableAlias ? ($tableAlias . '.') : '') . 'customer_id',
            (($resultAlias ? ($resultAlias . '.') : '') . 'month') => 'MONTH(' . ($tableAlias ? ($tableAlias . '.') : '') . 'date_start)',
        ];
        foreach (CashbackStat::moneyAttributes() as $moneyAttribute) {
            $keyName = ($resultAlias ? ($resultAlias . '.') : '') . $moneyAttribute;
            $valueName = 'SUM(' . ($tableAlias ? ($tableAlias . '.') : '') . $moneyAttribute . ')';
            $result[$keyName] = $valueName;
        }
        foreach (CashbackStat::percentAttributes() as $percentAttribute) {
            $keyName = ($resultAlias ? ($resultAlias . '.') : '') . $percentAttribute;
            $valueName = 'AVG(' . ($tableAlias ? ($tableAlias . '.') : '') . $percentAttribute . ')';
            $result[$keyName] = $valueName;
        }

        return $result;
    }
}