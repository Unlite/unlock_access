<?php

namespace common\components\cashback\services;


use common\components\cashback\repositories\CashbackStatRepository;
use common\components\cashback\models\CashbackStat;

class MonthSummaryService
{

    public function counting(int $year, int $month)
    {
        $date_time = (new \DateTimeImmutable());
//        if ((int)$date_time->format('Y') >= $year && (int)$date_time->format('m') >= $month)
//            throw new InvalidParamException('Year and month can\'t be more or equal of current.');

        $date_start = $date_time->setDate($year, $month, 1);
        $cashback_stats = CashbackStatRepository::getMonthGroupSummary($date_start);
        foreach ($cashback_stats as $cashback_stat) {
			/** @var CashbackStat|null $cashback_stat_entity */
            $cashback_stat_saved = CashbackStatRepository::getMonthIndividualSummary($date_start, $cashback_stat['cashback_id'], $cashback_stat['customer_id'], $cashback_stat['currency_code']);

			$cashback_stat_entity = new CashbackStat();
			$cashback_stat_entity->final = true;
			$cashback_stat_entity->calc = false;
			$cashback_stat_entity->status = CashbackStat::STATUS_HOLD;
			$cashback_stat_entity->date_start = $date_start->format('Y-m-d');

            if ($cashback_stat_saved && $cashback_stat_saved !== $cashback_stat) {
                $diff_attributes = $this->calculateMonthDiff($cashback_stat_saved, $cashback_stat);
                $cashback_stat_entity->setAttributes($diff_attributes);
				if (!$cashback_stat_entity->save(false)) {
					throw new \RuntimeException('Saving error.');
				}
            } elseif (!$cashback_stat_saved) {
            	$cashback_stat_entity->setAttributes($cashback_stat);
				if (!$cashback_stat_entity->save(false)) {
					throw new \RuntimeException('Saving error.');
				}
			}
        }
    }

    protected function calculateMonthDiff(array $source, array $new): array
    {
        $result = [];
        $moneyAttributes = CashbackStat::moneyAttributes();
        foreach ($new as $key => $value){
            if(in_array($key, $moneyAttributes)){
                $result[$key] = -((float)$source[$key] - (float)$new[$key]);
            }else {
                $result[$key] = $value;
            }
        }
        return $result;
    }
}