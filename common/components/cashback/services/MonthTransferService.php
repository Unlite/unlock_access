<?php

namespace common\components\cashback\services;


use common\components\cashback\models\Cashback;
use common\components\cashback\models\CashbackCustomer;
use common\components\cashback\models\queries\CashbackStatQuery;
use common\components\cashback\repositories\CashbackStatRepository;
use common\components\cashback\models\CashbackStat;
use common\models\CashbackTransfer;
use common\models\Transfer;
use yii\base\Component;
use yii\helpers\ArrayHelper;

class MonthTransferService
{

	public function createTransactions(int $year, int $month)
	{
		$date_time = (new \DateTimeImmutable());
		$date_start = $date_time->setDate($year, $month, 1);

		$cashbackStats = CashbackStatRepository::getFinalSummary($date_start);
		$balanceManager = \Yii::$app->get('balanceManager');
		$balanceManager->beginDbTransaction();
		try {
			foreach ($cashbackStats as $cashbackStat) {
				if($cashbackStat['user_revenue'] >= 0){
					$cashback_customer = CashbackCustomer::find()
						->where(['cashback_customer.customer_id' => $cashbackStat['customer_id'],'cashback_customer.cashback_id' => $cashbackStat['cashback_id']])
						->joinWith([
							'cashbackStats' => function(CashbackStatQuery $query) use ($cashbackStat){ return $query->isFinal()->andWhere([CashbackStat::tableName().'.'.'currency_code' => $cashbackStat['currency_code']])->andOnStatus(CashbackStat::STATUS_HOLD,CashbackStat::tableName());},
							'user'
						])
						->one();
					$start_date = time();
					$end_date = 0;
					if($cashback_customer) {
						foreach ($cashback_customer->cashbackStats as $customerCashbackStat) {
							$start_time = strtotime($customerCashbackStat->date_start);
							if ($start_date > $start_time)
								$start_date = $start_time;

							if ($customerCashbackStat->period == CashbackStat::PERIOD_MONTH) {
								$date = \DateTime::createFromFormat('Y-m-d', $customerCashbackStat->date_start);
								$date = $date->setDate($date->format('Y'), $date->format('m'), $date->format('t'))->format('Y-m-d');
								$end_time = strtotime($date);
							} else
								$end_time = strtotime($customerCashbackStat->date_end);
							if ($end_date < $end_time)
								$end_date = $end_time;

							$customerCashbackStat->status = CashbackStat::STATUS_APPROVED;
							$customerCashbackStat->save();
						}
						echo $comment = 'Cashback From ' . $cashback_customer->cashback->site->id . ':' . $cashback_customer->cashback->site->name . ' in period ' . date('Y-m-d', $start_date) . ' - ' . date('Y-m-d', $end_date) . ', Added ' . $cashbackStat['user_revenue'] . ' ' . $cashbackStat['currency_code'] . ' To ' . $cashback_customer->user->id . ':' . $cashback_customer->user->username;
						echo "\n";
						if ($cashbackStat['user_revenue'] > 0) {
							$transfer_id = $balanceManager->increase(
								['user_id' => $cashback_customer->user->id, 'currency_code' => $cashbackStat['currency_code']],
								$cashbackStat['user_revenue'],
								[
									'comment' => $comment,
									'type' => CashbackTransfer::TYPE,
									'customer_id' => $cashbackStat['customer_id'],
									'cashback_id' => $cashbackStat['cashback_id'],
									'net_revenue' => $cashbackStat['net_revenue'],
									'revenue' => $cashbackStat['revenue'],
									'user_revenue' => $cashbackStat['user_revenue'],
									'cashback_stat_ids' => join(',', ArrayHelper::getColumn($cashback_customer->cashbackStats, 'id')),
									'Date start' => date('Y-m-d', $start_date),
									'Date end' => date('Y-m-d', $end_date),
								]
							);
						}
					} else {
						echo "No user with such Cashback\\Customer ID";
					}
				}
			}
			$balanceManager->commitDbTransaction();
		} catch (\Exception $e) {
			$balanceManager->rollBackDbTransaction();
			throw $e;
		}
	}

	public function createFinalStats(int $year, int $month)
	{
		$date_time = (new \DateTimeImmutable());
		$date_start = $date_time->setDate($year, $month, 1);
		$cashback_stats = CashbackStatRepository::getMonthSummary($date_start);
		foreach ($cashback_stats as $cashback_stat) {
			$cashback_stat_saved = CashbackStatRepository::getMonthIndividualSummary($date_start, $cashback_stat['cashback_id'], $cashback_stat['customer_id'], $cashback_stat['currency_code']);

			$new_cashback_stat = new CashbackStat();
			$new_cashback_stat->final = true;
			$new_cashback_stat->calc = false;
			$new_cashback_stat->period = CashbackStat::PERIOD_MONTH;
			$new_cashback_stat->status = CashbackStat::STATUS_HOLD;
			$new_cashback_stat->date_start = $date_start->format('Y-m-d');

			if($cashback_stat_saved && $cashback_stat_saved !== $cashback_stat){
				$new_cashback_stat->setAttributes($this->calculateMonthDiff($cashback_stat_saved,$cashback_stat));
				if (!$new_cashback_stat->save(false)) {
					throw new \RuntimeException('Saving error.');
				}
			} elseif (!$cashback_stat_saved) {
				$new_cashback_stat->setAttributes($cashback_stat);
				if (!$new_cashback_stat->save(false)) {
					throw new \RuntimeException('Saving error.');
				}
			}
		}
	}

	protected function calculateMonthDiff(array $source, array $new): array
	{
		$result = [];
		$moneyAttributes = CashbackStat::moneyAttributes();
		foreach ($new as $key => $value){
			if(in_array($key, $moneyAttributes)){
				$result[$key] = -((float)$source[$key] - (float)$new[$key]) ;
			}else {
				$result[$key] = $value;
			}
		}
		return $result;
	}
}