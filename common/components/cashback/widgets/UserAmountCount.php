<?php

namespace common\components\cashback\widgets;

use common\models\CurrencyRelations;
use yii\base\InvalidParamException;
use yii\base\Widget;
use yii\db\Expression;
use yii\db\Query;

/**
 * UserAmountCount renders approximate amount of cashback for the current month
 *
 * Example:
 *
 * ~~~php
 * echo UserAmountCount::widget([
 *     'client_tokens' => ['da23....afs2', ...],
 *     'currency_code' => 'usd',
 * ]);
 * ~~~
 *
 */
class UserAmountCount extends Widget
{
    /**
     * Grouping result by `client_tokens` from `cashback_customer` table
     * @var array|string
     */
    public $client_tokens;
    /**
     * Currency code
     * @var string
     */
    public $currency_code;

    private $amount = 0;

    public function init()
    {
        parent::init();
        if ($this->client_tokens === null) throw new InvalidParamException('client_tokens must be required');

        if ($this->currency_code === null)
            $this->currency_code = 'usd';
    }

    public function run()
    {
        $this->calcAmount();
        return $this->amount;
    }

    protected function calcAmount()
    {
        $date_start = (new \DateTimeImmutable())->modify('first day of this month');
        //$date_end = $date_start->add(new \DateInterval('P1M'));
        $date_end = $date_start->setDate($date_start->format('Y'), $date_start->format('m'), $date_start->format('t'));
        $data = (new Query())->select([
            'currency_code' => 'cs.currency_code',
            'month' => 'MONTH(cs.date_start)',
            'net_revenue' => 'SUM(cs.net_revenue)',
            'revenue' => 'SUM(cs.revenue)',
            'user_revenue' => 'SUM(cs.user_revenue)',
        ])
            ->from('{{%cashback_customer}} cc')
            ->innerJoin('{{%cashback_stat}} cs', [
                'cc.customer_id' =>  new Expression('cs.customer_id'),
                'cc.cashback_id' =>  new Expression('cs.cashback_id')
            ])
            //->innerJoin('{{%cashback}} c', 'cs.cashback_id = c.id')
            ->where([
                'and',
                ['cs.calc' => 1],
                ['cc.client_token' => $this->client_tokens],
                ['between',
                    'cs.date_start',
                    new Expression("CAST('" . $date_start->format('Y-m-d') . "' AS DATE)"),
                    new Expression("CAST('" . $date_end->format('Y-m-d') . "' AS DATE)")
                ],
            ])
            ->groupBy(['cs.currency_code', 'month'])
            //->having(['month' => (new \DateTimeImmutable())->format('n')])
            ->all();
        foreach ($data as $row) {
            if ($row['currency_code'] === $this->currency_code) {
                $this->amount += $row['user_revenue'];
            } else {
                $exchangeRate = CurrencyRelations::getExchangeRate($row['currency_code'], $this->currency_code);
                $this->amount += $row['user_revenue'] * $exchangeRate;
            }
        }
    }
}