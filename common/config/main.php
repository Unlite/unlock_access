<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'balanceManager' => [
            'class' => 'common\components\balance\Manager'
        ]
    ],
    'modules' => [
        'away' => [
            'class' => 'common\modules\away\Module',
            'redirectDelay' => 5,
            'redirectType' => 'js',
            'models' => [
                'bonus' => [
                    'modelClass' => 'common\models\Bonuses',
                    'attribute' => 'url'
                ],
                'domain' => [
                    'modelClass' => 'common\models\Domain',
                    'attribute' => 'direct_url'
                ],
                'link' => [
                    'modelClass' => 'common\models\Link',
                    'attribute' => 'redirect_url'
                ],
            ]
        ]
    ]
];
