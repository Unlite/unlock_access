<?php

namespace common\models;

use common\models\queries\BlogPostQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii2mod\ftp\FtpClient;

/**
 * This is the model class for table "blog_post".
 *
 * @property int $id
 * @property string $title
 * @property string $short_description
 * @property string $text
 * @property int $created_at
 * @property int $updated_at
 * @property int $is_published
 * @property string $page_title
 * @property string $page_description
 * @property string $thumbnail_src
 * @property string $author_name
 * @property int $updated_date_time
 * @property string $slug
 * @property string $imageFile
 *
 * @property TagBlogPost[] $tagBlogPosts
 * @property Tag[] $tags
 * @property SiteBlogPost[] $siteBlogPosts
 * @property Site[] $sites
 * @property BlogPostBlogPostCategory[] $blogPostBlogPostCategories
 * @property BlogPostCategory[] $blogPostCategories
 */
class BlogPost extends \yii\db\ActiveRecord
{
	public $imageFile;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blog_post}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'short_description', 'text', 'is_published', 'updated_date_time', 'slug'], 'required'],
            [['text'], 'string'],
            [['created_at', 'updated_at', 'is_published', 'updated_date_time'], 'integer'],
            [['title', 'short_description'], 'string', 'max' => 255],
            [['imageFile'], 'file', 'extensions' => 'png, jpg, svg, jpeg, gif'],
            [['slug'], 'string', 'max' => 512],
            [['title', 'slug'], 'trim'],
            [['slug'], 'match', 'pattern' => '/^[a-z0-9]+(?:-[a-z0-9]+)*$/i'],
			[['slug'], 'unique'],
            [['page_title'], 'string', 'max' => 100],
            [['page_description'], 'string', 'max' => 300],
            [['thumbnail_src'], 'string', 'max' => 1024],
            [['author_name'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'short_description' => Yii::t('app', 'Short Description'),
            'text' => Yii::t('app', 'Text'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'is_published' => Yii::t('app', 'Is Published'),
            'page_title' => Yii::t('app', 'Page Title'),
            'page_description' => Yii::t('app', 'Page Description'),
            'thumbnail_src' => Yii::t('app', 'Thumbnail Src'),
            'author_name' => Yii::t('app', 'Author Name'),
            'updated_date_time' => Yii::t('app', 'Updated Date Time'),
            'imageFile' => 'Image file',
        ];
    }

	public function behaviors()
	{
		return [
			[
				'class' => TimestampBehavior::class,
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
	}

	public function beforeSave($insert)
	{
		$return = parent::beforeSave($insert);
		$this->imageFile = UploadedFile::getInstance($this, 'imageFile');
		if($this->imageFile){
			/* @var \yii2mod\ftp\FtpClient $ftp */
			$ftp = new FtpClient();
			$ftp->connect(Yii::$app->params['ftpHost']);
			$ftp->login(Yii::$app->params['ftpLogin'], Yii::$app->params['ftpPassword']);
			$ftp->pasv(true);
			if(!$ftp->isDir('blog')){
				$ftp->mkdir('blog');
			}
			$random_name = Yii::$app->security->generateRandomString(16).'.'.$this->imageFile->extension;
			if($ftp->put('blog/'.$random_name,$this->imageFile->tempName,FTP_BINARY)){
				$this->thumbnail_src = '//'.Yii::$app->params['cdnHost'].'/'.'blog/'.$random_name;
				return $return;
			} else {
				$this->addError('imageFile','Could not upload file to ftp');
				$return = false;
			}
		}
		return $return;
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTagBlogPosts()
    {
        return $this->hasMany(TagBlogPost::class, ['blog_post_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(Tag::class, ['id' => 'tag_id'])->viaTable('tag_blog_post', ['blog_post_id' => 'id']);
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */

	public function getSiteBlogPosts()
	{
		return $this->hasMany(SiteBlogPost::class,['blog_post_id'=>'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */

	public function getSites()
	{
		return $this->hasMany(Site::class, ['id' => 'site_id'])->viaTable(SiteBlogPost::tableName(), ['blog_post_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */

	public function getBlogPostBlogPostCategories()
	{
		return $this->hasMany(BlogPostBlogPostCategory::class,['blog_post_id'=>'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */

	public function getBlogPostCategories()
	{
		return $this->hasMany(BlogPostCategory::class, ['id' => 'blog_post_category_id'])->viaTable(BlogPostBlogPostCategory::tableName(), ['blog_post_id' => 'id']);
	}

	/**
	 * Generates links for sitemap.xml
	 * @return string
	 */
	public function getUrl()
	{
		return Url::to(['/blog/view', 'id' => $this->id, 'slug' => $this->slug]);
	}

    /**
     * @inheritdoc
     * @return \common\models\queries\BlogPostQuery the active query used by this AR class.
     */
    public static function find()
    {
		return Yii::createObject(BlogPostQuery::class, [get_called_class()]);
    }
}
