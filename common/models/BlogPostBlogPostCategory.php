<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "blog_post_blog_post_category".
 *
 * @property int $blog_post_id
 * @property int $blog_post_category_id
 *
 * @property BlogPostCategory $blogPostCategory
 * @property BlogPost $blogPost
 */
class BlogPostBlogPostCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%blog_post_blog_post_category}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['blog_post_id', 'blog_post_category_id'], 'required'],
            [['blog_post_id', 'blog_post_category_id'], 'integer'],
            [['blog_post_id', 'blog_post_category_id'], 'unique', 'targetAttribute' => ['blog_post_id', 'blog_post_category_id']],
            [['blog_post_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => BlogPostCategory::className(), 'targetAttribute' => ['blog_post_category_id' => 'id']],
            [['blog_post_id'], 'exist', 'skipOnError' => true, 'targetClass' => BlogPost::className(), 'targetAttribute' => ['blog_post_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'blog_post_id' => Yii::t('app', 'Blog Post ID'),
            'blog_post_category_id' => Yii::t('app', 'Blog Post Category ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogPostCategory()
    {
        return $this->hasOne(BlogPostCategory::className(), ['id' => 'blog_post_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogPost()
    {
        return $this->hasOne(BlogPost::className(), ['id' => 'blog_post_id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\queries\BlogPostBlogPostCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\BlogPostBlogPostCategoryQuery(get_called_class());
    }
}
