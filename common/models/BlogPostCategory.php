<?php

namespace common\models;

use common\models\queries\BlogPostCategoryQuery;
use Yii;

/**
 * This is the model class for table "blog_post_category".
 *
 * @property int $id
 * @property string $name
 *
 * @property BlogPostBlogPostCategory[] $blogPostBlogPostCategories
 * @property BlogPost[] $blogPosts
 */
class BlogPostCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%blog_post_category}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 64],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogPostBlogPostCategories()
    {
        return $this->hasMany(BlogPostBlogPostCategory::className(), ['blog_post_category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogPosts()
    {
        return $this->hasMany(BlogPost::className(), ['id' => 'blog_post_id'])->viaTable('blog_post_blog_post_category', ['blog_post_category_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\queries\BlogPostCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
		return Yii::createObject(BlogPostCategoryQuery::class, [get_called_class()]);
    }
}
