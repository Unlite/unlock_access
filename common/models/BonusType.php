<?php

namespace common\models;

use common\models\queries\BonusTypeQuery;
use Yii;

/**
 * This is the model class for table "bonus_type".
 *
 * @property integer $id
 * @property string $name
 * @property string $status
 *
 * @property Bonuses[] $bonuses
 */
class BonusType extends \yii\db\ActiveRecord
{
	const STATUS_ACTIVE = 'active';
	const STATUS_PAUSED = 'paused';

	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%bonus_type}}';
    }

    /**
     * @inheritdoc
     * @return BonusTypeQuery
     */
    public static function find()
    {
        return Yii::createObject(BonusTypeQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status','name'], 'required'],
			[['status'], 'in', 'range' => self::statuses()],
            [['name'], 'string', 'max' => 64],
        ];
    }

	/**
	 * All available statuses
	 * @return array
	 */
	public static function statuses()
	{
		return [self::STATUS_ACTIVE, self::STATUS_PAUSED];
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
			'status' => Yii::t('app','Status'),
        ];
    }

    /**
     * @return \common\models\queries\BonusesQuery|\yii\db\ActiveQuery
     */
    public function getBonuses()
    {
        return $this->hasMany(Bonuses::class, ['bonus_type_id' => 'id']);
    }
}
