<?php

namespace common\models;

use common\models\queries\BonusesQuery;
use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "bonuses".
 *
 * @property integer $id
 * @property integer $site_id
 * @property integer $bonus_type_id
 * @property string $text
 * @property string $url
 * @property string $promo_code
 * @property integer $start_at
 * @property integer $end_at
 * @property integer $is_promoted
 * @property string $short_text
 * @property string $status
 *
 * @property Site $site
 * @property BonusType $bonusType
 * @property OfferTypeBonuses[] $offerTypeBonuses
 * @property OfferTypeBonuses $offerTypeBonus
 * @property OfferType[] $offerTypes
 */
class Bonuses extends \yii\db\ActiveRecord
{
	const STATUS_ACTIVE = 'active';
	const STATUS_PAUSED = 'paused';

	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%bonuses}}';
    }

    /**
     * @inheritdoc
     * @return BonusesQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(BonusesQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id', 'bonus_type_id', 'is_promoted', 'status', 'short_text'], 'required'],
            [['site_id', 'bonus_type_id', 'start_at', 'end_at', 'is_promoted'], 'integer'],
            [['text', 'url'], 'string', 'max' => 1024],
			[['status'], 'in', 'range' => self::statuses()],
            [['promo_code'], 'string', 'max' => 255],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::class, 'targetAttribute' => ['site_id' => 'id']],
            [['bonus_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => BonusType::class, 'targetAttribute' => ['bonus_type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'site_id' => Yii::t('app', 'Site ID'),
            'bonus_type_id' => Yii::t('app', 'Bonus Type ID'),
            'text' => Yii::t('app', 'Text'),
            'url' => Yii::t('app', 'Url'),
            'promo_code' => Yii::t('app', 'Promo Code'),
            'start_at' => Yii::t('app', 'Start At'),
            'end_at' => Yii::t('app', 'End At'),
            'is_promoted' => Yii::t('app', 'Is Promoted'),
			'short_text' => Yii::t('app', 'Short Text'),
			'status' => Yii::t('app','Status'),
        ];
    }

	/**
	 * All available statuses
	 * @return array
	 */
	public static function statuses()
	{
		return [self::STATUS_ACTIVE, self::STATUS_PAUSED];
	}

    /**
     * @return \common\models\queries\SiteQuery|\yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::class, ['id' => 'site_id']);
    }

    /**
     * @return \common\models\queries\BonusTypeQuery|\yii\db\ActiveQuery
     */
    public function getBonusType()
    {
        return $this->hasOne(BonusType::class, ['id' => 'bonus_type_id']);
    }

    /**
     * @return \common\models\queries\OfferTypeBonusesQuery|\yii\db\ActiveQuery
     */
    public function getOfferTypeBonuses()
    {
        return $this->hasMany(OfferTypeBonuses::class, ['bonuses_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * TODO: Delete method. Use Bonuses::getOfferTypeBonuses()->one()
     * @deprecated
     */
    public function getOfferTypeBonus()
    {
        return $this->hasOne(OfferTypeBonuses::class, ['bonuses_id' => 'id']);
    }

    /**
     * @return \common\models\queries\OfferTypeQuery|\yii\db\ActiveQuery
     */
    public function getOfferTypes()
    {
        return $this->hasMany(OfferType::class, ['id' => 'offer_type_id'])->viaTable(OfferTypeBonuses::tableName(), ['bonuses_id' => 'id']);
    }

	/**
	 * Generates links for sitemap.xml
	 * @return string
	 */
	public function getUrl()
	{
		return Url::to(['/bonuses/view', 'id' => $this->id]);
	}
}
