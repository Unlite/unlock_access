<?php

namespace common\models;

use common\models\queries\TransferQuery;
use Yii;

/**
 * @inheritdoc
 */

class CashbackTransfer extends Transfer
{
	const TYPE = 'cashback';

	/**
	 * @inheritdoc
	 */
	public function init()
	{
		$this->type = self::TYPE;
		parent::init();
	}

	/**
	 * @inheritdoc
	 * @return TransferQuery|\yii\db\ActiveQuery
	 */
	public static function find()
	{
		return Yii::createObject(['class' => TransferQuery::class, 'type' => self::TYPE, 'tableName' => self::tableName()], [get_called_class()]);
	}

}
