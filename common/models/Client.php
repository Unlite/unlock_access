<?php

namespace common\models;

use common\components\cashback\models\CashbackCustomer;
use common\components\cashback\models\CashbackStat;
use common\models\queries\ClientQuery;
use Yii;
use yii\base\InvalidArgumentException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\web\Cookie;

/**
 * This is the model class for table "{{%client}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $token
 * @property string $secret
 * @property string $ip
 * @property string $extension_id
 * @property string $user_agent
 * @property integer $created_at
 * @property integer $last_online_at
 * @property bool $secure public read-only property of $_secure
 *
 * @property User $user
 * @property CashbackCustomer[] $cashbackCustomers
 * @property CashbackStat[] $cashbackStats
 * @method void touch(string $attribute)
 */
class Client extends ActiveRecord
{
    const TOKEN_LENGTH = 32;
    /**
     * @var bool secure marker.
     * If client was found only by token - false, if with secret - true
     */
    private $_secure = true;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client}}';
    }

    /**
     * @inheritdoc
     * @return ClientQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(ClientQuery::class, [get_called_class()]);
    }

    public static function getClientByCookies()
    {
        $clientToken = Yii::$app->request->cookies->getValue('client_token', false);
        $clientSecret = Yii::$app->request->cookies->getValue('client_secret', false);
        return static::getClient($clientToken, $clientSecret);
    }

    public static function getClient($clientToken = null, $clientSecret = null)
    {
        if($clientToken && strlen($clientToken) == static::TOKEN_LENGTH){
            if($clientSecret){
                $client = static::findOne(['token' => $clientToken, 'secret' => $clientSecret]);
                return $client;
            }
            $client = static::findOne(['token' => $clientToken]);
            if($client) $client->setSecure(false);
            return $client;
        }
        return null;
    }

    /**
     * Sets the _secure
     * @param bool $value the _secure
     * @return $this
     */
    private function setSecure(bool $value = true)
    {
        $this->_secure = $value;

        return $this;
    }

    public static function getClientByUser()
    {
        if (Yii::$app->user->isGuest) return null;
        $client = static::findOne(['user_id' => Yii::$app->user->id]);
        return $client ?: static::newRecord([
                'ip' => Yii::$app->request->userIP,
                'user_agent' => Yii::$app->request->userAgent,
                'user_id' => Yii::$app->user->id
            ]);
    }

    /**
     * Create new Client record
     * @param array|null $params
     * @param bool $cookies save token and secret in cookies
     * @return Client
     * @throws Exception
     */
    public static function newRecord($params = null, bool $cookies = false)
    {
        $client = new static($params);
        $client->token = $client->generateUniqueRandomString('token', static::TOKEN_LENGTH);
        $client->secret = Yii::$app->getSecurity()->generateRandomString(8);
        if ($client->save()) {
            if($cookies){
                static::addClientCookies($client);
            }
            return $client;
        }
        throw new Exception('Can\'t create new Client record');
    }

    public function generateUniqueRandomString($attribute, $length = 32)
    {
        $randomString = str_replace(['_', '-'], '0', strtolower(Yii::$app->getSecurity()->generateRandomString($length)));

        if (!$this->findOne([$attribute => $randomString]))
            return $randomString;
        else
            return $this->generateUniqueRandomString($attribute, $length);
    }

    /**
     * Save in cookies token and secret if secure
     * @param Client|array|string $client
     */
    public static function addClientCookies($client)
    {
        $clientToken = null;
        $clientSecret = null;
        if($client instanceof Client){
            $clientToken = $client->token;
            if($client->secure){
                $clientSecret = $client->secret;
            }
        }elseif(ArrayHelper::isAssociative($client) && isset($client['token'])){
            $clientToken = $client['token'];
            if(isset($client['secret'])){
                $clientSecret = $client['secret'];
            }
        }elseif (is_string($client) && strlen($client) == static::TOKEN_LENGTH){//Only token
            $clientToken = $client;
        }else{
            throw new InvalidArgumentException('Invalid type of param client. Available: Array, String, ' . self::className());
        }
        if($clientToken){
            Yii::$app->response->cookies->add(new Cookie([
                'name' => 'client_token',
                'value' => $clientToken,
                'expire' => time() + 30 * 24 * 60 *60,
            ]));
            if($clientSecret){
                Yii::$app->response->cookies->add(new Cookie([
                    'name' => 'client_secret',
                    'value' => $clientSecret,
                    'expire' => time() + 30 * 24 * 60 *60,
                ]));
            }else{
                Yii::$app->response->cookies->remove('client_secret');
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['token', 'secret'], 'required'],
            [['user_id'], 'integer'],
            [['token', 'extension_id'], 'string', 'length' => static::TOKEN_LENGTH],
            [['token'], 'unique'],
            [['secret'], 'string', 'length' => 8],
            [['ip'], 'string', 'max' => 15],
            [['user_agent'], 'string', 'max' => 256],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'last_online_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'token' => 'Token',
            'secret' => 'Secret',
            'ip' => 'Ip',
            'extension_id' => 'Extension ID',
            'user_agent' => 'User Agent',
            'created_at' => 'Created At',
            'last_online_at' => 'Last Online At',
        ];
    }

    /**
     * @return \common\models\queries\UserQuery|\yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \common\models\queries\UserQuery|\yii\db\ActiveQuery
     */
    public function getCashbackStats()
    {
        return $this->hasMany(CashbackStat::class, ['customer_id' => 'customer_id'])->via('cashbackCustomers');
    }

    /**
     * @return \common\models\queries\UserQuery|\yii\db\ActiveQuery
     */
    public function getCashbackCustomers()
    {
        return $this->hasMany(CashbackCustomer::class, ['client_token' => 'token']);
    }

    /**
     * Gets the _secure
     * @return bool
     */
    public function getSecure()
    {
        return $this->_secure;
    }
}
