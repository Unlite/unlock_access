<?php

namespace common\models;

use common\models\queries\CurrencyQuery;
use Yii;

/**
 * This is the model class for table "currency".
 *
 * @property string $code
 * @property string $name
 * @property bool $is_system
 *
 * @property PaymentSystemCurrency[] $paymentSystemCurrencies
 * @property SiteCurrency[] $siteCurrencies
 * @property CurrencyRelations[] $exchanges
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%currency}}';
    }

    /**
     * @inheritdoc
     * @return CurrencyQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(CurrencyQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code'], 'required'],
            [['code'], 'unique'],
            [['code'], 'string', 'max' => 5],
            [['name'], 'string', 'max' => 255],
            [['is_system'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code' => Yii::t('app', 'Code'),
            'name' => Yii::t('app', 'Name'),
            'is_system' => Yii::t('app', 'System'),
        ];
    }

    /**
     * @return \common\models\queries\PaymentSystemCurrencyQuery|\yii\db\ActiveQuery
     */
    public function getPaymentSystemCurrencies()
    {
        return $this->hasMany(PaymentSystemCurrency::class, ['currency_code' => 'code']);
    }

    /**
     * @return \common\models\queries\SiteCurrencyQuery|\yii\db\ActiveQuery
     */
    public function getSiteCurrencies()
    {
        return $this->hasMany(SiteCurrency::class, ['currency_code' => 'code']);
    }

    /**
     * @return \common\models\queries\CurrencyRelationsQuery|\yii\db\ActiveQuery
     */
    public function getExchanges()
    {
        return $this->hasMany(CurrencyRelations::class, ['currency_code' => 'code']);
    }
}
