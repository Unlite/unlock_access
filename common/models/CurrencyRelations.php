<?php

namespace common\models;

use common\models\queries\CurrencyRelationsQuery;
use yii\base\InvalidArgumentException;
use yii\base\InvalidParamException;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%currency_relations}}".
 *
 * @property string $currency_code
 * @property string $currency_code_r
 * @property string $value
 * @property string $exchange_commission
 *
 * @property Currency $currency
 * @property Currency $currencyR
 */
class CurrencyRelations extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%currency_relations}}';
    }

    /**
     * @inheritdoc
     * @return CurrencyRelationsQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return \Yii::createObject(CurrencyRelationsQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['currency_code', 'currency_code_r', 'value'], 'required'],
            [['value'], 'number'],
            [['exchange_commission'], 'number'],
            [['currency_code', 'currency_code_r'], 'string', 'max' => 5],
            [['currency_code', 'currency_code_r'], 'unique', 'targetAttribute' => ['currency_code', 'currency_code_r']],
            [['currency_code'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::class, 'targetAttribute' => ['currency_code' => 'code']],
            [['currency_code_r'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::class, 'targetAttribute' => ['currency_code_r' => 'code']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'currency_code' => 'Currency Code',
            'currency_code_r' => 'Currency Code R',
            'value' => 'Value',
            'exchange_commission' => 'Exchange Commission',
        ];
    }

    /**
     * @return \common\models\queries\CurrencyQuery|\yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::class, ['code' => 'currency_code']);
    }

    /**
     * @return \common\models\queries\CurrencyQuery|\yii\db\ActiveQuery
     */
    public function getCurrencyR()
    {
        return $this->hasOne(Currency::class, ['code' => 'currency_code_r']);
    }

    /**
     * @param string $currency_code
     * @param string $currency_code_r
     * @return \yii\db\ActiveQuery
     */
    public static function findByCodes($currency_code, $currency_code_r)
    {
        return static::find()->where(['currency_code' => $currency_code, 'currency_code_r' => $currency_code_r]);
    }

    /**
     * @param string|Currency $currency
     * @param string|Currency $currency_r
     * @return float
     * @throws \yii\db\Exception
     */
    public static function getExchangeRate($currency, $currency_r): float
    {
        if($currency instanceof Currency) $currency_code = $currency->code;
        elseif(is_string($currency)) $currency_code = $currency;
        else throw new InvalidArgumentException('currency must be instance of ' . Currency::class . ' or string');

        if($currency_r instanceof Currency) $currency_code_r = $currency_r->code;
        elseif(is_string($currency_r)) $currency_code_r = $currency_r;
        else throw new InvalidArgumentException('currency_r must be instance of ' . Currency::class . ' or string');

        if($currency_code === $currency_code_r) return 1;

        if ($currencyRelation = static::findByCodes($currency_code, $currency_code_r)->asArray()->one()) {
            return $currencyRelation['value'];
        }
        throw new \yii\db\Exception("Bundle $currency_code and $currency_code_r not found");
    }
}
