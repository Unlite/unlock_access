<?php

namespace common\models;

use common\models\queries\CustomListQuery;
use Yii;

/**
 * This is the model class for table "{{%custom_list}}".
 *
 * @property int $id
 * @property string $name
 * @property string $status
 * @property int $type
 *
 * @property CustomListSite[] $customListSites
 * @property Site[] $sites
 */
class CustomList extends \yii\db\ActiveRecord
{

	const TYPE_DISABLED = 'disabled';
	const TYPE_TOP = 'top';
	const TYPE_SIDEBAR = 'sidebar';
	const TYPE_PROMOTED = 'promoted';

	const STATUS_ACTIVE = 'active';
	const STATUS_PAUSED = 'paused';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%custom_list}}';
    }

    /**
     * @inheritdoc
     * @return CustomListQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(CustomListQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','status'], 'required'],
			[['status'], 'in', 'range' => self::statuses()],
            ['type','in','range' => [self::TYPE_DISABLED,self::TYPE_TOP,self::TYPE_SIDEBAR,self::TYPE_PROMOTED]],
            [['name'], 'string', 'max' => 255],
        ];
    }

	/**
	 * All available statuses
	 * @return array
	 */
	public static function statuses()
	{
		return [self::STATUS_ACTIVE, self::STATUS_PAUSED];
	}

	public static function getTypes()
	{
		return [
			'disabled' => 'Disabled',
			'top' => 'Top',
			'sidebar' => 'Sidebar',
			'promoted' => 'Promoted',
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'),
			'status' => Yii::t('app','Status'),
        ];
    }

    /**
     * @return \common\models\queries\CustomListSiteQuery|\yii\db\ActiveQuery
     */
    public function getCustomListSites()
    {
        return $this->hasMany(CustomListSite::class, ['custom_list_id' => 'id']);
    }

    /**
     * @return \common\models\queries\SiteQuery|\yii\db\ActiveQuery
     */
    public function getSites()
    {
        return $this->hasMany(Site::class, ['id' => 'site_id'])->viaTable(CustomListSite::tableName(), ['custom_list_id' => 'id']);
    }
}
