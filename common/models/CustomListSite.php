<?php

namespace common\models;

use common\models\queries\CustomListSiteQuery;
use Yii;

/**
 * This is the model class for table "{{%custom_list_site}}".
 *
 * @property int $custom_list_id
 * @property int $site_id
 *
 * @property Site $site
 * @property CustomList $customList
 */
class CustomListSite extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%custom_list_site}}';
    }

    /**
     * @inheritdoc
     * @return CustomListSiteQuery|yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(CustomListSiteQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['custom_list_id', 'site_id'], 'required'],
            [['custom_list_id', 'site_id'], 'integer'],
            [['custom_list_id', 'site_id'], 'unique', 'targetAttribute' => ['custom_list_id', 'site_id']],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::class, 'targetAttribute' => ['site_id' => 'id']],
            [['custom_list_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustomList::class, 'targetAttribute' => ['custom_list_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'custom_list_id' => Yii::t('app', 'Custom List ID'),
            'site_id' => Yii::t('app', 'Site ID'),
        ];
    }

    /**
     * @return \common\models\queries\SiteQuery|\yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::class, ['id' => 'site_id']);
    }

    /**
     * @return \common\models\queries\CustomListQuery|\yii\db\ActiveQuery
     */
    public function getCustomList()
    {
        return $this->hasOne(CustomList::class, ['id' => 'custom_list_id']);
    }
}
