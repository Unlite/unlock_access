<?php

namespace common\models;

use common\models\queries\DomainQuery;
use Yii;

/**
 * This is the model class for table "{{%domain}}".
 *
 * @property integer $id
 * @property integer $site_id
 * @property string $type
 * @property string $domain
 * @property string $direct_url
 * @property string $proxy_pattern
 * @property string $status
 *
 * @property Site $site
 * @property Link[] $links
 * @property DomainProxy[] $domainProxies
 * @property Proxy[] $proxies
 */
class Domain extends \yii\db\ActiveRecord
{
    const TYPE_MAIN = 'main';
    const TYPE_MIRROR = 'mirror';

    const STATUS_ACTIVE = 'active';
    const STATUS_PAUSED = 'paused';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%domain}}';
    }

    /**
     * @inheritdoc
     * @return DomainQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(DomainQuery::class, [get_called_class()]);
    }

    /**
     * All available types
     * @return array
     */
    public static function types()
    {
        return [self::TYPE_MAIN, self::TYPE_MIRROR];
    }

    /**
     * All available statuses
     * @return array
     */
    public static function statuses()
    {
        return [self::STATUS_ACTIVE, self::STATUS_PAUSED];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id', 'type', 'domain', 'status', 'direct_url'], 'required'],
            [['site_id'], 'integer'],
            [['type'], 'in', 'range' => self::types()],
            [['status'], 'in', 'range' => self::statuses()],
            [['domain'], 'string', 'max' => 256],
            [['proxy_pattern'], 'string', 'max' => 512],
            [['direct_url'], 'string', 'max' => 1024],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::class, 'targetAttribute' => ['site_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'site_id' => 'Site ID',
            'type' => 'Type',
            'domain' => 'Domain',
            'direct_url' => 'Direct Url',
            'proxy_pattern' => 'Proxy Pattern',
        ];
    }

    /**
     * @return \common\models\queries\SiteQuery|\yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::class, ['id' => 'site_id']);
    }

    /**
     * @return \common\models\queries\LinkQuery|\yii\db\ActiveQuery
     */
    public function getLinks()
    {
        return $this->hasMany(Link::class, ['domain_id' => 'id']);
    }

    /**
     * @return \common\models\queries\DomainProxyQuery|\yii\db\ActiveQuery
     */
    public function getDomainProxies()
    {
        return $this->hasMany(DomainProxy::class, ['domain_id' => 'id']);
    }

    /**
     * @return \common\models\queries\ProxyQuery|\yii\db\ActiveQuery
     */
    public function getProxies()
    {
        return $this->hasMany(Proxy::class, ['id' => 'proxy_id'])->viaTable(DomainProxy::tableName(), ['domain_id' => 'id']);
    }
}
