<?php

namespace common\models;

use common\models\queries\DomainProxyQuery;
use Yii;

/**
 * This is the model class for table "{{%domain_proxy}}".
 *
 * @property integer $domain_id
 * @property integer $proxy_id
 *
 * @property Domain $domain
 * @property Proxy $proxy
 */
class DomainProxy extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%domain_proxy}}';
    }

    /**
     * @return DomainProxyQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(DomainProxyQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['domain_id', 'proxy_id'], 'required'],
            [['domain_id', 'proxy_id'], 'integer'],
            [['domain_id'], 'exist', 'skipOnError' => true, 'targetClass' => Domain::class, 'targetAttribute' => ['domain_id' => 'id']],
            [['proxy_id'], 'exist', 'skipOnError' => true, 'targetClass' => Proxy::class, 'targetAttribute' => ['proxy_id' => 'id']],
            [['domain_id', 'proxy_id'], 'unique', 'targetAttribute' => ['domain_id', 'proxy_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'domain_id' => 'Domain ID',
            'proxy_id' => 'Proxy ID',
        ];
    }

    /**
     * @return \common\models\queries\DomainQuery|\yii\db\ActiveQuery
     */
    public function getDomain()
    {
        return $this->hasOne(Domain::class, ['id' => 'domain_id']);
    }

    /**
     * @return \common\models\queries\ProxyQuery|\yii\db\ActiveQuery
     */
    public function getProxy()
    {
        return $this->hasOne(Proxy::class, ['id' => 'proxy_id']);
    }
}
