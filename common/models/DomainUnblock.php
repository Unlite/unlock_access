<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%domain_unblock}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $pattern
 *
 * @property DomainUnblockProxy[] $domainUnblockProxies
 * @property Proxy[] $proxies
 * @deprecated
 */
class DomainUnblock extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%domain_unblock}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'pattern'], 'required'],
            [['name'], 'string', 'max' => 128],
            [['pattern'], 'string', 'max' => 512],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'pattern' => 'Pattern',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDomainUnblockProxies()
    {
        return $this->hasMany(DomainUnblockProxy::className(), ['domain_unblock_id' => 'id'])->inverseOf('domainUnblock');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProxies()
    {
        return $this->hasMany(Proxy::className(), ['id' => 'proxy_id'])->viaTable('{{%domain_unblock_proxy}}', ['domain_unblock_id' => 'id']);
    }
}
