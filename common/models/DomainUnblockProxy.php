<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%domain_unblock_proxy}}".
 *
 * @property integer $domain_unblock_id
 * @property integer $proxy_id
 *
 * @property DomainUnblock $domainUnblock
 * @property Proxy $proxy
 * @deprecated
 */
class DomainUnblockProxy extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%domain_unblock_proxy}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['domain_unblock_id', 'proxy_id'], 'required'],
            [['domain_unblock_id', 'proxy_id'], 'integer'],
            [['domain_unblock_id'], 'exist', 'skipOnError' => true, 'targetClass' => DomainUnblock::className(), 'targetAttribute' => ['domain_unblock_id' => 'id']],
            [['proxy_id'], 'exist', 'skipOnError' => true, 'targetClass' => Proxy::className(), 'targetAttribute' => ['proxy_id' => 'id']],
            [['domain_unblock_id', 'proxy_id'], 'unique', 'targetAttribute' => ['domain_unblock_id', 'proxy_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'domain_unblock_id' => 'Domain Unblock ID',
            'proxy_id' => 'Proxy ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDomainUnblock()
    {
        return $this->hasOne(DomainUnblock::className(), ['id' => 'domain_unblock_id'])->inverseOf('domainUnblockProxies');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProxy()
    {
        return $this->hasOne(Proxy::className(), ['id' => 'proxy_id'])->inverseOf('domainUnblockProxies');
    }
}
