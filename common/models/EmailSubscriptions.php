<?php

namespace common\models;

use common\models\queries\EmailSubscriptionsQuery;
use Yii;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "email_subscriptions".
 *
 * @property int $id
 * @property int $user_id
 * @property string $email
 * @property int $sub_promo
 * @property int $sub_system
 * @property string $token
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $user
 */
class EmailSubscriptions extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%email_subscriptions}}';
    }

    /**
     * @inheritdoc
     * @return EmailSubscriptionsQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(EmailSubscriptionsQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'sub_promo', 'sub_system', 'created_at', 'updated_at'], 'integer'],
            [['email'], 'required'],
            [['sub_promo'], 'default', 'value' => true],
            [['email'], 'string', 'max' => 255],
            [['email'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'token',
                ],
                'value' => function () {
                    return Yii::$app->security->generateRandomString(32);
                },
            ],
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'email' => 'Email',
            'sub_promo' => 'Sub Promo',
            'sub_system' => 'Sub System',
            'token' => 'Token',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }


    /**
     * @return \common\models\queries\UserQuery|\yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
