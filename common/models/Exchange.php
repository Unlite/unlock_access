<?php

namespace common\models;

use common\models\queries\TransferQuery;
use Yii;

/**
 * @inheritdoc
 * @property Exchange $transferTo
 * @property Exchange $transferCommission
 */

class Exchange extends Transfer
{
	const TYPE = 'exchange';

	/**
	 * @inheritdoc
	 */
	public function init()
	{
		$this->type = self::TYPE;
		parent::init();
	}

	/**
	 * @return \common\models\queries\TransferQuery|\yii\db\ActiveQuery
	 */
	public function getTransferTo()
	{
		return $this->hasOne(Exchange::class, ['parent_id' => 'id'])->andWhere(['!=','user_currency_id',$this->user_currency_id]);
	}

	/**
	 * @return \common\models\queries\TransferQuery|\yii\db\ActiveQuery
	 */
	public function getTransferCommission()
	{
		return $this->hasOne(Exchange::class, ['parent_id' => 'id'])->andWhere(['user_currency_id' => $this->user_currency_id]);
	}

	/**
	 * @inheritdoc
	 * @return TransferQuery|\yii\db\ActiveQuery
	 */
	public static function find()
	{
		return Yii::createObject(['class' => TransferQuery::class, 'type' => self::TYPE, 'tableName' => self::tableName()], [get_called_class()]);
	}

}
