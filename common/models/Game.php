<?php

namespace common\models;

use common\models\queries\GameQuery;
use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "game".
 *
 * @property integer $id
 * @property string $name
 * @property string $img_src
 * @property integer $game_provider_id
 * @property string $description
 * @property string $embedded_url
 * @property string $page_title
 * @property string $page_description
 * @property int $embedded_width
 * @property int $embedded_height
 * @property string $slug
 * @property string $status
 *
 * @property GameProvider $gameProvider
 * @property GameCategoryGame[] $gameCategoryGames
 * @property GameCategory[] $gameCategories
 * @property SiteGame[] $siteGames
 * @property Site[] $sites
 */
class Game extends \yii\db\ActiveRecord
{
	const STATUS_ACTIVE = 'active';
	const STATUS_PAUSED = 'paused';

	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%game}}';
    }

    /**
     * @inheritdoc
     * @return GameQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(GameQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug', 'status','game_provider_id'], 'required'],
            [['game_provider_id', 'embedded_width', 'embedded_height'], 'integer'],
            [['name'], 'string', 'max' => 64],
            [['slug'], 'string', 'max' => 128],
			[['status'], 'in', 'range' => self::statuses()],
            [['name', 'slug'], 'trim'],
            [['slug'], 'match', 'pattern' => '/^[a-z0-9]+(?:-[a-z0-9]+)*$/i'],
			[['slug'], 'unique'],
            [['img_src', 'description', 'embedded_url'], 'string', 'max' => 1024],
			[['page_title'], 'string', 'max' => 100],
			[['page_description'], 'string', 'max' => 300],
            [['game_provider_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameProvider::class, 'targetAttribute' => ['game_provider_id' => 'id']],
        ];
    }

	/**
	 * All available statuses
	 * @return array
	 */
	public static function statuses()
	{
		return [self::STATUS_ACTIVE, self::STATUS_PAUSED];
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'img_src' => Yii::t('app', 'Img Src'),
            'game_provider_id' => Yii::t('app', 'Game Provider ID'),
            'description' => Yii::t('app', 'Description'),
            'embedded_url' => Yii::t('app', 'Embedded Url'),
            'is_promoted' => Yii::t('app', 'Promoted'),
			'embedded_width' => Yii::t('app', 'Embedded Width'),
			'embedded_height' => Yii::t('app', 'Embedded Height'),
			'status' => Yii::t('app','Status'),
        ];
    }

    /**
     * @return \common\models\queries\GameProviderQuery|\yii\db\ActiveQuery
     */
    public function getGameProvider()
    {
        return $this->hasOne(GameProvider::class, ['id' => 'game_provider_id']);
    }

    /**
     * @return \common\models\queries\GameCategoryGameQuery|\yii\db\ActiveQuery
     */
    public function getGameCategoryGames()
    {
        return $this->hasMany(GameCategoryGame::class, ['game_id' => 'id']);
    }

    /**
     * @return \common\models\queries\GameCategoryQuery|\yii\db\ActiveQuery
     */
    public function getGameCategories()
    {
        return $this->hasMany(GameCategory::class, ['id' => 'game_category_id'])->viaTable(GameCategoryGame::tableName(), ['game_id' => 'id']);
    }

    /**
     * @return \common\models\queries\SiteGameQuery|\yii\db\ActiveQuery
     */
    public function getSiteGames()
    {
        return $this->hasMany(SiteGame::class, ['game_id' => 'id']);
    }

    /**
     * @return \common\models\queries\SiteQuery|\yii\db\ActiveQuery
     */
    public function getSites()
    {
        return $this->hasMany(Site::class, ['id' => 'site_id'])->viaTable(SiteGame::tableName(), ['game_id' => 'id']);
    }

	/**
	 * Generates links for sitemap.xml
	 * @return string
	 */
	public function getUrl()
	{
		return Url::to(['/game/view', 'id' => $this->id, 'slug' => $this->slug]);
	}
}
