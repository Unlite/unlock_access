<?php

namespace common\models;

use common\models\queries\GameCategoryQuery;
use Yii;

/**
 * This is the model class for table "game_category".
 *
 * @property integer $id
 * @property string $name
 * @property string $status
 *
 * @property GameCategoryGame[] $gameCategoryGames
 * @property Game[] $games
 * @property GameCategorySite[] $gameCategorySites
 * @property Site[] $sites
 */
class GameCategory extends \yii\db\ActiveRecord
{

	const TYPE_DISABLED = 'disabled';
	const TYPE_TOP = 'top';
	const TYPE_SIDEBAR = 'sidebar';
	const TYPE_SLIDER = 'slider';

	const STATUS_ACTIVE = 'active';
	const STATUS_PAUSED = 'paused';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%game_category}}';
    }

    /**
     * @inheritdoc
     * @return GameCategoryQuery|object
     */
    public static function find()
    {
        return Yii::createObject(GameCategoryQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status','name'], 'required'],
			['type','in','range' => [self::TYPE_DISABLED,self::TYPE_TOP,self::TYPE_SIDEBAR]],
			[['status'], 'in', 'range' => self::statuses()],
            [['name'], 'string', 'max' => 64],
        ];
    }

	/**
	 * All available statuses
	 * @return array
	 */
	public static function statuses()
	{
		return [self::STATUS_ACTIVE, self::STATUS_PAUSED];
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
			'status' => Yii::t('app','Status'),
        ];
    }

    /**
     * @return \common\models\queries\GameCategoryGameQuery|\yii\db\ActiveQuery
     */
    public function getGameCategoryGames()
    {
        return $this->hasMany(GameCategoryGame::class, ['game_category_id' => 'id']);
    }

    /**
     * @return \common\models\queries\GameQuery|\yii\db\ActiveQuery
     */
    public function getGames()
    {
        return $this->hasMany(Game::class, ['id' => 'game_id'])->viaTable(GameCategoryGame::tableName(), ['game_category_id' => 'id']);
    }

    /**
     * @return \common\models\queries\GameCategorySiteQuery|\yii\db\ActiveQuery
     */
    public function getGameCategorySites()
    {
        return $this->hasMany(GameCategorySite::class, ['game_category_id' => 'id']);
    }

    /**
     * @return \common\models\queries\SiteQuery|\yii\db\ActiveQuery
     */
    public function getSites()
    {
        return $this->hasMany(Site::class, ['id' => 'site_id'])->viaTable(GameCategorySite::tableName(), ['game_category_id' => 'id']);
    }
}
