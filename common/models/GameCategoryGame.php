<?php

namespace common\models;

use common\models\queries\GameCategoryGameQuery;
use Yii;

/**
 * This is the model class for table "game_category_game".
 *
 * @property integer $game_category_id
 * @property integer $game_id
 *
 * @property GameCategory $gameCategory
 * @property Game $game
 */
class GameCategoryGame extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%game_category_game}}';
    }

    /**
     * @return GameCategoryGameQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(GameCategoryGameQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['game_category_id', 'game_id'], 'required'],
            [['game_category_id', 'game_id'], 'integer'],
            [['game_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameCategory::class, 'targetAttribute' => ['game_category_id' => 'id']],
            [['game_id'], 'exist', 'skipOnError' => true, 'targetClass' => Game::class, 'targetAttribute' => ['game_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'game_category_id' => Yii::t('app', 'Game Category ID'),
            'game_id' => Yii::t('app', 'Game ID'),
        ];
    }

    /**
     * @return \common\models\queries\GameCategoryQuery|\yii\db\ActiveQuery
     */
    public function getGameCategory()
    {
        return $this->hasOne(GameCategory::class, ['id' => 'game_category_id']);
    }

    /**
     * @return \common\models\queries\GameQuery|\yii\db\ActiveQuery
     */
    public function getGame()
    {
        return $this->hasOne(Game::class, ['id' => 'game_id']);
    }
}
