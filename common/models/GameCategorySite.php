<?php

namespace common\models;

use common\models\queries\GameCategorySiteQuery;
use Yii;

/**
 * This is the model class for table "game_category_site".
 *
 * @property integer $game_category_id
 * @property integer $site_id
 * @property integer $is_auto_created
 *
 * @property Site $site
 * @property GameCategory $gameCategory
 */
class GameCategorySite extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%game_category_site}}';
    }

    /**
     * @inheritdoc
     * @return GameCategorySiteQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(GameCategorySiteQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['game_category_id', 'site_id', 'is_auto_created'], 'required'],
            [['game_category_id', 'site_id'], 'integer'],
            [['is_auto_created'], 'boolean'],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::class, 'targetAttribute' => ['site_id' => 'id']],
            [['game_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameCategory::class, 'targetAttribute' => ['game_category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'game_category_id' => Yii::t('app', 'Game Category ID'),
            'site_id' => Yii::t('app', 'Site ID'),
            'is_auto_created' => Yii::t('app', 'Is Auto Created'),
        ];
    }

    /**
     * @return \common\models\queries\SiteQuery|\yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::class, ['id' => 'site_id']);
    }

    /**
     * @return \common\models\queries\GameCategoryQuery|\yii\db\ActiveQuery
     */
    public function getGameCategory()
    {
        return $this->hasOne(GameCategory::class, ['id' => 'game_category_id']);
    }
}
