<?php

namespace common\models;

use common\models\queries\GameProviderQuery;
use Yii;

/**
 * This is the model class for table "game_provider".
 *
 * @property integer $id
 * @property string $name
 * @property string $img_src
 * @property string $description
 * @property string $short_description
 * @property string $status
 *
 * @property Game[] $games
 * @property GameProviderSite[] $gameProviderSites
 * @property Site[] $sites
 */
class GameProvider extends \yii\db\ActiveRecord
{
	const STATUS_ACTIVE = 'active';
	const STATUS_PAUSED = 'paused';

	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%game_provider}}';
    }

    /**
     * @inheritdoc
     * @return GameProviderQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(GameProviderQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'short_description','status'], 'required'],
			[['status'], 'in', 'range' => self::statuses()],
            [['name'], 'string', 'max' => 64],
            [['img_src', 'short_description'], 'string', 'max' => 1024],
            [['description'], 'string', 'max' => 10240],
        ];
    }

	/**
	 * All available statuses
	 * @return array
	 */
	public static function statuses()
	{
		return [self::STATUS_ACTIVE, self::STATUS_PAUSED];
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'img_src' => Yii::t('app', 'Img Src'),
            'description' => Yii::t('app', 'Description'),
            'short_description' => Yii::t('app', 'Short Description'),
			'status' => Yii::t('app','Status'),
        ];
    }

    /**
     * @return \common\models\queries\GameQuery|\yii\db\ActiveQuery
     */
    public function getGames()
    {
        return $this->hasMany(Game::class, ['game_provider_id' => 'id']);
    }

    /**
     * @return \common\models\queries\GameProviderSiteQuery|\yii\db\ActiveQuery
     */
    public function getGameProviderSites()
    {
        return $this->hasMany(GameProviderSite::class, ['game_provider_id' => 'id']);
    }

    /**
     * @return \common\models\queries\SiteQuery|\yii\db\ActiveQuery
     */
    public function getSites()
    {
        return $this->hasMany(Site::class, ['id' => 'site_id'])->viaTable(GameProviderSite::tableName(), ['game_provider_id' => 'id']);
    }
}
