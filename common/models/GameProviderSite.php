<?php

namespace common\models;

use common\models\queries\GameProviderSiteQuery;
use Yii;

/**
 * This is the model class for table "game_provider_site".
 *
 * @property integer $game_provider_id
 * @property integer $site_id
 * @property integer $is_auto_created
 *
 * @property Site $site
 * @property GameProvider $gameProvider
 */
class GameProviderSite extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%game_provider_site}}';
    }

    /**
     * @inheritdoc
     * @return GameProviderSiteQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(GameProviderSiteQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['game_provider_id', 'site_id', 'is_auto_created'], 'required'],
            [['game_provider_id', 'site_id'], 'integer'],
            [['is_auto_created'], 'boolean'],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::class, 'targetAttribute' => ['site_id' => 'id']],
            [['game_provider_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameProvider::class, 'targetAttribute' => ['game_provider_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'game_provider_id' => Yii::t('app', 'Game Provider ID'),
            'site_id' => Yii::t('app', 'Site ID'),
            'is_auto_created' => Yii::t('app', 'Is Auto Created'),
        ];
    }

    /**
     * @return \common\models\queries\SiteQuery|\yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::class, ['id' => 'site_id']);
    }

    /**
     * @return \common\models\queries\GameProviderQuery|\yii\db\ActiveQuery
     */
    public function getGameProvider()
    {
        return $this->hasOne(GameProvider::class, ['id' => 'game_provider_id']);
    }
}
