<?php

namespace common\models;

use common\models\queries\GeoQuery;
use Yii;

/**
 * This is the model class for table "{{%geo}}".
 *
 * @property string $code
 * @property string $name
 *
 * @property Proxy[] $proxies
 * @property SiteGeo[] $siteGeos
 * @property Site[] $sites
 */
class Geo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%geo}}';
    }

    /**
     * @inheritdoc
     * @return GeoQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(GeoQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code'], 'required'],
            [['code'], 'unique'],
            [['code'], 'string', 'max' => 5],
            [['name'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code' => 'Code',
            'name' => 'Name',
        ];
    }

    /**
     * @return \common\models\queries\ProxyQuery|\yii\db\ActiveQuery
     */
    public function getProxies()
    {
        return $this->hasMany(Proxy::class, ['geo_code' => 'code']);
    }

    /**
     * @return \common\models\queries\SiteGeoQuery|\yii\db\ActiveQuery
     */
    public function getSiteGeos()
    {
        return $this->hasMany(SiteGeo::class, ['geo_code' => 'code']);
    }

    /**
     * @return \common\models\queries\SiteQuery|\yii\db\ActiveQuery
     */
    public function getSites()
    {
        return $this->hasMany(Site::class, ['id' => 'site_id'])->viaTable(SiteGeo::tableName(), ['geo_code' => 'code']);
    }
}
