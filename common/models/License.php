<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%license}}".
 *
 * @property integer $site_id
 * @property string $geo_code
 *
 * @property Geo $geo
 * @property Site $site
 * @deprecated
 */
class License extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%license}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id', 'geo_code'], 'required'],
            [['site_id'], 'integer'],
            [['geo_code'], 'string', 'max' => 5],
            [['geo_code'], 'exist', 'skipOnError' => true, 'targetClass' => Geo::className(), 'targetAttribute' => ['geo_code' => 'code']],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'id']],
            [['site_id', 'geo_code'], 'unique', 'targetAttribute' => ['site_id', 'geo_code']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'site_id' => 'Site ID',
            'geo_code' => 'Geo Code',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeo()
    {
        return $this->hasOne(Geo::className(), ['code' => 'geo_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::className(), ['id' => 'site_id']);
    }
}
