<?php

namespace common\models;

use common\models\queries\LicensorQuery;
use Yii;

/**
 * This is the model class for table "licensor".
 *
 * @property integer $id
 * @property string $geo_code
 * @property string $name
 * @property string $status
 *
 * @property Geo $geo
 * @property SiteLicensor[] $siteLicensors
 * @property Site[] $sites
 */
class Licensor extends \yii\db\ActiveRecord
{
	const STATUS_ACTIVE = 'active';
	const STATUS_PAUSED = 'paused';

	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%licensor}}';
    }

    /**
     * @inheritdoc
     * @return LicensorQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(LicensorQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['geo_code','status', 'name'], 'required'],
            [['geo_code'], 'string', 'max' => 5],
			[['status'], 'in', 'range' => self::statuses()],
            [['name'], 'string', 'max' => 255],
            [['geo_code'], 'exist', 'skipOnError' => true, 'targetClass' => Geo::class, 'targetAttribute' => ['geo_code' => 'code']],
        ];
    }

	/**
	 * All available statuses
	 * @return array
	 */
	public static function statuses()
	{
		return [self::STATUS_ACTIVE, self::STATUS_PAUSED];
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'geo_code' => Yii::t('app', 'Geo Code'),
            'name' => Yii::t('app', 'Name'),
			'status' => Yii::t('app','Status'),
        ];
    }

    /**
     * @return \common\models\queries\GeoQuery|\yii\db\ActiveQuery
     */
    public function getGeo()
    {
        return $this->hasOne(Geo::class, ['code' => 'geo_code']);
    }

    /**
     * @return \common\models\queries\SiteLicensorQuery|\yii\db\ActiveQuery
     */
    public function getSiteLicensors()
    {
        return $this->hasMany(SiteLicensor::class, ['licensor_id' => 'id']);
    }

    /**
     * @return \common\models\queries\SiteQuery|\yii\db\ActiveQuery
     */
    public function getSites()
    {
        return $this->hasMany(Site::class, ['id' => 'site_id'])->viaTable(SiteLicensor::tableName(), ['licensor_id' => 'id']);
    }
}
