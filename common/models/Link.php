<?php

namespace common\models;

use common\models\queries\LinkQuery;
use Yii;

/**
 * This is the model class for table "{{%link}}".
 *
 * @property integer $id
 * @property integer $domain_id
 * @property string $redirect_url
 *
 * @property Domain $domain
 * @property LinkWildcard[] $linkWildcards
 */
class Link extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%link}}';
    }

    /**
     * @inheritdoc
     * @return LinkQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(LinkQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['domain_id', 'redirect_url'], 'required'],
            [['domain_id'], 'integer'],
            [['redirect_url'], 'string', 'max' => 1024],
            [['domain_id'], 'exist', 'skipOnError' => true, 'targetClass' => Domain::class, 'targetAttribute' => ['domain_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'domain_id' => 'Domain ID',
            'redirect_url' => 'Redirect Url',
        ];
    }

    /**
     * @return \common\models\queries\DomainQuery|\yii\db\ActiveQuery
     */
    public function getDomain()
    {
        return $this->hasOne(Domain::class, ['id' => 'domain_id']);
    }

    /**
     * @return \common\models\queries\LinkWildcardQuery|\yii\db\ActiveQuery
     */
    public function getLinkWildcards()
    {
        return $this->hasMany(LinkWildcard::class, ['link_id' => 'id']);
    }
}
