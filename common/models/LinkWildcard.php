<?php

namespace common\models;

use common\models\queries\LinkWildcardQuery;
use Yii;

/**
 * This is the model class for table "{{%link_wildcard}}".
 *
 * @property integer $id
 * @property integer $link_id
 * @property string $pattern
 *
 * @property Link $link
 */
class LinkWildcard extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%link_wildcard}}';
    }

    /**
     * @inheritdoc
     * @return LinkWildcardQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(LinkWildcardQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['link_id', 'pattern'], 'required'],
            [['link_id'], 'integer'],
            [['pattern'], 'string', 'max' => 1024],
            [['link_id'], 'exist', 'skipOnError' => true, 'targetClass' => Link::class, 'targetAttribute' => ['link_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'link_id' => 'Link ID',
            'pattern' => 'Pattern',
        ];
    }

    /**
     * @return \common\models\queries\LinkQuery|\yii\db\ActiveQuery
     */
    public function getLink()
    {
        return $this->hasOne(Link::class, ['id' => 'link_id']);
    }
}
