<?php

namespace common\models;

use common\models\queries\LocaleQuery;
use Yii;

/**
 * This is the model class for table "locale".
 *
 * @property string $code
 * @property string $name
 *
 * @property SiteLocale[] $siteLocales
 * @property Site[] $sites
 */
class Locale extends \yii\db\ActiveRecord
{
	const STATUS_ENABLED = 'enabled';
	const STATUS_DISABLED = 'disabled';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%locale}}';
    }

    /**
     * @inheritdoc
     * @return LocaleQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(LocaleQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'name'], 'required'],
            [['code'], 'unique'],
			[['status'], 'in', 'range' => static::statuses()],
            [['code'], 'string', 'max' => 5],
            [['name'], 'string', 'max' => 255],
        ];
    }

	/**
	 * All available statuses
	 * @return array
	 */
	public static function statuses()
	{
		return [self::STATUS_ENABLED, self::STATUS_DISABLED];
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code' => Yii::t('app', 'Code'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \common\models\queries\SiteLocaleQuery|\yii\db\ActiveQuery
     */
    public function getSiteLocales()
    {
        return $this->hasMany(SiteLocale::class, ['locale_code' => 'code']);
    }

    /**
     * @return \common\models\queries\SiteQuery|\yii\db\ActiveQuery
     */
    public function getSites()
    {
        return $this->hasMany(Site::class, ['id' => 'site_id'])->viaTable(SiteLocale::tableName(), ['locale_code' => 'code']);
    }
}
