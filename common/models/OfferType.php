<?php

namespace common\models;

use common\models\queries\OfferTypeQuery;
use Yii;

/**
 * This is the model class for table "offer_type".
 *
 * @property integer $id
 * @property string $name
 *
 * @property OfferTypeBonuses[] $offerTypeBonuses
 * @property Bonuses[] $bonuses
 */
class OfferType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%offer_type}}';
    }

    /**
     * @inheritdoc
     * @return OfferTypeQuery|yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(OfferTypeQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \common\models\queries\OfferTypeBonusesQuery|\yii\db\ActiveQuery
     */
    public function getOfferTypeBonuses()
    {
        return $this->hasMany(OfferTypeBonuses::class, ['offer_type_id' => 'id']);
    }

    /**
     * @return \common\models\queries\BonusesQuery|\yii\db\ActiveQuery
     */
    public function getBonuses()
    {
        return $this->hasMany(Bonuses::class, ['id' => 'bonuses_id'])->viaTable(OfferTypeBonuses::tableName(), ['offer_type_id' => 'id']);
    }
}
