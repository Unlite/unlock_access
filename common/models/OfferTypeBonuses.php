<?php

namespace common\models;

use common\models\queries\OfferTypeBonusesQuery;
use Yii;

/**
 * This is the model class for table "offer_type_bonuses".
 *
 * @property integer $offer_type_id
 * @property integer $bonuses_id
 * @property string $offer_text
 *
 * @property Bonuses $bonuses
 * @property OfferType $offerType
 */
class OfferTypeBonuses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%offer_type_bonuses}}';
    }

    /**
     * @inheritdoc
     * @return OfferTypeBonusesQuery|yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(OfferTypeBonusesQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['offer_type_id', 'bonuses_id', 'offer_text'], 'required'],
            [['offer_type_id', 'bonuses_id'], 'integer'],
            [['offer_text'], 'string', 'max' => 16],
            [['bonuses_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bonuses::class, 'targetAttribute' => ['bonuses_id' => 'id']],
            [['offer_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => OfferType::class, 'targetAttribute' => ['offer_type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'offer_type_id' => Yii::t('app', 'Offer Type ID'),
            'bonuses_id' => Yii::t('app', 'Bonuses ID'),
            'offer_text' => Yii::t('app', 'Offer Text'),
        ];
    }

    /**
     * @return \common\models\queries\BonusesQuery|\yii\db\ActiveQuery
     */
    public function getBonuses()
    {
        return $this->hasOne(Bonuses::class, ['id' => 'bonuses_id']);
    }

    /**
     * @return \common\models\queries\OfferTypeQuery|\yii\db\ActiveQuery
     */
    public function getOfferType()
    {
        return $this->hasOne(OfferType::class, ['id' => 'offer_type_id']);
    }
}
