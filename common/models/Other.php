<?php

namespace common\models;

use common\models\queries\TransferQuery;
use Yii;

/**
 * @inheritdoc
 * @property PaymentSystem $payment_system
 */

class Other extends Transfer
{
	const TYPE = 'other';

	/**
	 * @inheritdoc
	 */
	public function init()
	{
		$this->type = self::TYPE;
		parent::init();
	}

	public function rules()
	{
		return array_merge(parent::rules(),[
			['payment_system_id','integer'],
			[['payment_system_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentSystem::class, 'targetAttribute' => ['payment_system_id' => 'id']],
		]);
	}

	/**
	 * @return \common\models\queries\PaymentSystemQuery|\yii\db\ActiveQuery
	 */
	public function getPaymentSystem()
	{
		return $this->hasOne(PaymentSystem::class, ['id' => 'payment_system_id']);
	}

	/**
	 * @return \common\models\queries\PaymentSystemQuery|\yii\db\ActiveQuery
	 */
	public function getPaymentSystem()
	{
		return $this->hasOne(PaymentSystem::class, ['id' => 'payment_system_id']);
	}

	/**
	 * @inheritdoc
	 * @return TransferQuery|\yii\db\ActiveQuery
	 */
	public static function find()
	{
		return Yii::createObject(['class' => TransferQuery::class, 'type' => self::TYPE, 'tableName' => self::tableName()], [get_called_class()]);
	}

}
