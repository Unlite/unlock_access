<?php

namespace common\models;

use common\models\queries\PaymentSystemQuery;
use Yii;

/**
 * This is the model class for table "{{%payment}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $img_src
 * @property string $description
 * @property string $status
 *
 * @property SitePayment[] $sitePayments
 * @property Currency[] $currencies
 * @property PaymentSystemCurrency $paymentCurrency
 * @property Site[] $sites
 */
class PaymentSystem extends \yii\db\ActiveRecord
{
	const STATUS_ENABLED = 'enabled';
	const STATUS_DISABLED = 'disabled';

	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payment_system}}';
    }

    /**
     * @inheritdoc
     * @return PaymentSystemQuery|yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(PaymentSystemQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 256],
            [['img_src'], 'string', 'max' => 1024],
			[['description'], 'string', 'max' => 1024],
			[['status'], 'in', 'range' => static::statuses()],
            [['img_src'], 'url', 'defaultScheme' => 'http'],
        ];
    }

	/**
	 * All available statuses
	 * @return array
	 */
	public static function statuses()
	{
		return [self::STATUS_ENABLED, self::STATUS_DISABLED];
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'img_src' => 'Img Src',
            'status' => 'Status',
            'description' => 'Description',
        ];
    }

    /**
     * @return \common\models\queries\SitePaymentQuery|\yii\db\ActiveQuery
     */
    public function getSitePayments()
    {
        return $this->hasMany(SitePayment::class, ['payment_id' => 'id']);
    }

    /**
     * @return \common\models\queries\PaymentSystemCurrencyQuery|\yii\db\ActiveQuery
     */
    public function getPaymentCurrency()
    {
        return $this->hasMany(PaymentSystemCurrency::class, ['payment_system_id' => 'id']);
    }

    /**
     * @return \common\models\queries\CurrencyQuery|\yii\db\ActiveQuery
     */
    public function getCurrencies()
    {
        return $this->hasMany(Currency::class, ['code' => 'currency_code'])->viaTable(PaymentSystemCurrency::tableName(), ['payment_system_id' => 'id']);
    }

    /**
     * @return \common\models\queries\SiteQuery|\yii\db\ActiveQuery
     */
    public function getSites()
    {
        return $this->hasMany(Site::class, ['id' => 'site_id'])->viaTable(SitePayment::tableName(), ['payment_id' => 'id']);
    }
}
