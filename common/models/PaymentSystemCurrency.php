<?php

namespace common\models;

use common\models\queries\PaymentSystemCurrencyQuery;
use Yii;

/**
 * This is the model class for table "payment_system_currency".
 *
 * @property integer $payment_system_id
 * @property string $currency_code
 *
 * @property Currency $currency
 * @property PaymentSystem $paymentSystem
 */
class PaymentSystemCurrency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payment_system_currency}}';
    }

    /**
     * @inheritdoc
     * @return PaymentSystemCurrencyQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(PaymentSystemCurrencyQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_system_id', 'currency_code'], 'required'],
            [['payment_system_id'], 'integer'],
            [['currency_code'], 'string', 'max' => 5],
            [['currency_code'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::class, 'targetAttribute' => ['currency_code' => 'code']],
            [['payment_system_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentSystem::class, 'targetAttribute' => ['payment_system_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'payment_system_id' => Yii::t('app', 'PaymentSystem System ID'),
            'currency_code' => Yii::t('app', 'Currency Code'),
        ];
    }

    /**
     * @return \common\models\queries\CurrencyQuery|\yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::class, ['code' => 'currency_code']);
    }

    /**
     * @return \common\models\queries\PaymentSystemQuery|\yii\db\ActiveQuery
     */
    public function getPaymentSystem()
    {
        return $this->hasOne(PaymentSystem::class, ['id' => 'payment_system_id']);
    }
}
