<?php

namespace common\models;

use common\models\queries\ProxyQuery;
use Yii;

/**
 * This is the model class for table "{{%proxy}}".
 *
 * @property integer $id
 * @property string $geo_code
 * @property string $host
 * @property string $status
 * @property integer $port
 * @property string $type
 * @property string $login
 * @property string $password
 *
 * @property string $fullAddress
 *
 * @property DomainProxy[] $domainProxies
 * @property Domain[] $domains
 * @property Geo $geo
 */
class Proxy extends \yii\db\ActiveRecord
{

    const STATUS_ACTIVE = 'active';
    const STATUS_PAUSED = 'paused';

    const TYPE_PRIVATE = 'private';
    const TYPE_PUBLIC = 'public';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%proxy}}';
    }

    /**
     * @inheritdoc
     * @return ProxyQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(ProxyQuery::class, [get_called_class()]);
    }

    /**
     * All available statuses
     * @return array
     */
    public static function statuses()
    {
        return [self::STATUS_ACTIVE, self::STATUS_PAUSED];
    }

    /**
     * All available types
     * @return array
     */
    public static function types()
    {
        return [self::TYPE_PRIVATE, self::TYPE_PUBLIC];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['geo_code', 'host', 'type', 'port', 'status'], 'required'],
            [['port'], 'integer'],
            [['status'], 'in', 'range' => self::statuses()],
            [['geo_code'], 'string', 'max' => 5],
            [['host', 'login'], 'string', 'max' => 128],
            [['password'], 'string', 'max' => 256],
            [['geo_code'], 'exist', 'skipOnError' => true, 'targetClass' => Geo::class, 'targetAttribute' => ['geo_code' => 'code']],
            [['type'], 'in', 'range' => self::types()],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'geo_code' => 'Geo Code',
            'host' => 'Host',
            'port' => 'Port',
        ];
    }

    /**
     * Return full address of proxy. host + port
     * @return string
     */
    public function getFullAddress()
    {
        return $this->host . ':' . $this->port;
    }

    /**
     * @return \common\models\queries\DomainProxyQuery|\yii\db\ActiveQuery
     */
    public function getDomainProxies()
    {
        return $this->hasMany(DomainProxy::class, ['proxy_id' => 'id'])->inverseOf('proxy');
    }

    /**
     * @return \common\models\queries\DomainQuery|\yii\db\ActiveQuery
     */
    public function getDomains()
    {
        return $this->hasMany(Domain::class, ['id' => 'domain_id'])->viaTable(DomainProxy::tableName(), ['proxy_id' => 'id']);
    }

    /**
     * @return \common\models\queries\GeoQuery|\yii\db\ActiveQuery
     */
    public function getGeo()
    {
        return $this->hasOne(Geo::class, ['code' => 'geo_code']);
    }
}
