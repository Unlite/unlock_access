<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property int $id
 * @property string $key
 * @property string $group
 * @property string $value
 * @property int $serialized
 */
class Settings extends \yii\db\ActiveRecord
{
	public $_data;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%settings}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key', 'group', 'value', 'serialized'], 'required'],
            [['value'], 'string'],
            [['serialized'], 'integer'],
            [['key', 'group'], 'string', 'max' => 55],
			[['serialized'], 'default', 'value'=> 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'key' => Yii::t('app', 'Key'),
            'group' => Yii::t('app', 'Group'),
            'value' => Yii::t('app', 'Value'),
            'serialized' => Yii::t('app', 'Serialized'),
        ];
    }

	public function beforeValidate()
	{
		if($this->serialized == 1 && is_array($this->value)){
			$this->value = serialize($this->value);
		} return true;
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\SettingsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\SettingsQuery(get_called_class());
    }

	public function getData()
	{
		if($this->serialized){
			return unserialize($this->value);
		}
		else return $this->value;
	}

	public function setData($data)
	{
		if($this->serialized && is_array($data)){
			$this->_data = serialize($data);
		} else
			$this->_data = $data;
	}

}
