<?php

namespace common\models;

use common\components\cashback\models\Cashback;
use common\models\queries\SiteQuery;
use common\models\traits\TranslateRelationTrait;
use Yii;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\Url;

/**
 * This is the model class for table "{{%site}}".
 *
 * @property integer $id
 * @property string $name
 * @property double $predefined_rating
 * @property double $rating
 * @property integer $expire
 * @property string $type
 * @property string $status
 * @property string $aliases
 * @property string $slug
 *
 * @property Domain[] $domains
 * @property GameCategorySite[] $gameCategorySites
 * @property GameCategory[] $gameCategories
 * @property GameProviderSite[] $gameProviderSites
 * @property GameProvider[] $gameProviders
 * @property CustomListSite[] $customListSites
 * @property CustomList[] $customLists
 * @property Review[] $reviews
 * @property SiteImage[] $siteImages
 * @property SiteGeo[] $siteGeos
 * @property Geo[] $geos
 * @property SiteTranslation[] $siteTranslations
 * @property SiteLicensor[] $siteLicensors
 * @property License[] $licenses
 * @property Geo[] $licenseGeos
 * @property SiteGame[] $siteGames
 * @property SiteLocale[] $siteLocales
 * @property SiteCurrency[] $siteCurrencies
 * @property Game[] $games
 * @property SitePayment[] $sitePayments
 * @property PaymentSystem[] $payments
 * @property SiteSoft[] $siteSofts
 * @property Soft[] $softs
 * @property Currency[] $currencies
 * @property Site[] $relations
 * @property Locale[] $locales
 * @property Site[] $relationByL
 * @property Site[] $relationByH
 * @property SiteRelations[] $siteRelations
 * @property Bonuses[] $bonuses
 * @property SiteRelations[] $siteRelationByH
 * @property SiteRelations[] $siteRelationByL
 * @property SiteInfo $info
 * @property Cashback $cashback
 */
class Site extends ActiveRecord
{
	use TranslateRelationTrait;
    const TYPE_CASINO = 'casino';
    const TYPE_BETTING = 'betting';
    const TYPE_POKER = 'poker';
    const TYPE_OTHER = 'other';
    const TYPE_HIDDEN = 'hidden';

    const STATUS_ACTIVE = 'active';
    const STATUS_PAUSED = 'paused';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%site}}';
    }

    /**
     * @inheritdoc
     * @return SiteQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(SiteQuery::class, [get_called_class()]);
    }

    /**
     * All available types
     * @return array
     */
    public static function types()
    {
        return [self::TYPE_CASINO, self::TYPE_BETTING, self::TYPE_OTHER, self::TYPE_HIDDEN, self::TYPE_POKER];
    }

    /**
     * All available statuses
     * @return array
     */
    public static function statuses()
    {
        return [self::STATUS_ACTIVE, self::STATUS_PAUSED];
    }

	public function translateFields()
	{
		return [
			'short_description',
			'description',
			'title',
			'meta_title',
			'meta_description',
		];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type', 'status', 'slug'], 'required'],
            [['predefined_rating'], 'number', 'min' => 1, 'max' => 5],
            [['predefined_rating'], 'default', 'value' => 4],
            [['expire'], 'integer'],
            [['type'], 'in', 'range' => self::types()],
            [['status'], 'in', 'range' => self::statuses()],
            [['name'], 'string', 'max' => 64],
            [['slug'], 'string', 'max' => 128],
            [['name', 'slug'], 'trim'],
            [['slug'], 'match', 'pattern' => '/^[a-z0-9]+(?:-[a-z0-9]+)*$/i'],
            [['slug'], 'unique'],
            [['aliases'], 'string', 'max' => 1024],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'rating',
                ],
                'value' => function () {
                    return $this->predefined_rating;
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'predefined_rating' => 'Predefined Rating',
            'rating' => 'Rating',
            'expire' => 'Expire',
            'type' => 'Type',
            'status' => 'Status',
            'aliases' => 'Aliases',
        ];
    }

    /**
     * @return \common\models\queries\DomainQuery|\yii\db\ActiveQuery
     */
    public function getDomains()
    {
        return $this->hasMany(Domain::class, ['site_id' => 'id']);
    }


    /**
     * @return \common\models\queries\ReviewQuery|\yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(Review::class, ['site_id' => 'id']);
    }

    /**
     * @return \common\models\queries\SiteImageQuery|\yii\db\ActiveQuery
     */
    public function getSiteImages()
    {
        return $this->hasMany(SiteImage::class, ['site_id' => 'id']);
    }

    /**
     * @return \common\models\queries\SiteGeoQuery|\yii\db\ActiveQuery
     */
    public function getSiteGeos()
    {
        return $this->hasMany(SiteGeo::class, ['site_id' => 'id']);
    }

    /**
     * @return \common\models\queries\GeoQuery|\yii\db\ActiveQuery
     */
    public function getGeos()
    {
        return $this->hasMany(Geo::class, ['code' => 'geo_code'])->viaTable(SiteGeo::tableName(), ['site_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiteTranslations()
    {
        return $this->hasMany(SiteTranslation::class, ['site_id' => 'id']);
    }

    /**
     * @return \common\models\queries\LicensorQuery|\yii\db\ActiveQuery
     */
    public function getLicenses()
    {
        return $this->hasMany(Licensor::class, ['id' => 'licensor_id'])->viaTable(SiteLicensor::tableName(), ['site_id' => 'id']);
    }

    /**
     * @return \common\models\queries\SiteLicensorQuery|\yii\db\ActiveQuery
     */
    public function getSiteLicensors()
    {
        return $this->hasMany(SiteLicensor::class, ['site_id' => 'id']);
    }

    /**
     * @return \common\models\queries\GeoQuery|\yii\db\ActiveQuery
     */
    public function getLicenseGeos()
    {
        return $this->hasMany(Geo::class, ['code' => 'geo_code'])
            ->viaTable(Licensor::tableName(), ['site_id' => 'id'])
            ->viaTable(SiteLicensor::tableName(), ['site_id' => 'id']);
    }

    /**
     * @return \common\models\queries\SiteGameQuery|\yii\db\ActiveQuery
     */
    public function getSiteGames()
    {
        return $this->hasMany(SiteGame::class, ['site_id' => 'id']);
    }

    /**
     * @return \common\models\queries\GameQuery|\yii\db\ActiveQuery
     */
    public function getGames()
    {
        return $this->hasMany(Game::class, ['id' => 'game_id'])->viaTable(SiteGame::tableName(), ['site_id' => 'id']);
    }

    /**
     * @return \common\models\queries\SitePaymentQuery|\yii\db\ActiveQuery
     */
    public function getSitePayments()
    {
        return $this->hasMany(SitePayment::class, ['site_id' => 'id']);
    }

    /**
     * @return \common\models\queries\PaymentSystemQuery|\yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(PaymentSystem::class, ['id' => 'payment_id'])->viaTable(SitePayment::tableName(), ['site_id' => 'id']);
    }

    /**
     * @return \common\models\queries\LocaleQuery|\yii\db\ActiveQuery
     */
    public function getLocales()
    {
        return $this->hasMany(Locale::class, ['code' => 'locale_code'])->viaTable(SiteLocale::tableName(), ['site_id' => 'id']);
    }

    /**
     * @return \common\models\queries\SiteLocaleQuery|\yii\db\ActiveQuery
     */
    public function getSiteLocales()
    {
        return $this->hasMany(SiteLocale::class, ['site_id' => 'id']);
    }

    /**
     * @return \common\models\queries\CurrencyQuery|\yii\db\ActiveQuery
     */
    public function getCurrencies()
    {
        return $this->hasMany(Currency::class, ['code' => 'currency_code'])->viaTable(SiteCurrency::tableName(), ['site_id' => 'id']);
    }

    /**
     * @return \common\models\queries\SiteCurrencyQuery|\yii\db\ActiveQuery
     */
    public function getSiteCurrencies()
    {
        return $this->hasMany(SiteCurrency::class, ['site_id' => 'id']);
    }

    /**
     * @return \common\models\queries\SiteQuery|\yii\db\ActiveQuery
     */
    public function getRelations()
    {
        return $this->getRelationByL()->union($this->getRelationByH());
    }

    /**
     * @return \common\models\queries\SiteQuery|\yii\db\ActiveQuery
     */
    public function getRelationByL()
    {
        return $this->hasMany(Site::class, ['id' => 'site_id_h'])->viaTable(SiteRelations::tableName(), ['site_id_l' => 'id']);
    }

    /**
     * @return \common\models\queries\SiteQuery|\yii\db\ActiveQuery
     */
    public function getRelationByH()
    {
        return $this->hasMany(Site::class, ['id' => 'site_id_l'])->viaTable(SiteRelations::tableName(), ['site_id_h' => 'id']);
    }

    /**
     * @return \common\models\queries\SiteRelationsQuery|\yii\db\ActiveQuery
     */
    public function getSiteRelations()
    {
        return $this->getSiteRelationByL()->union($this->getSiteRelationByH());
    }

    /**
     * @return \common\models\queries\SiteRelationsQuery|\yii\db\ActiveQuery
     */
    public function getSiteRelationByL()
    {
        return $this->hasMany(SiteRelations::class, ['site_id_l' => 'id']);
    }

    /**
     * @return \common\models\queries\SiteRelationsQuery|\yii\db\ActiveQuery
     */
    public function getSiteRelationByH()
    {
        return $this->hasMany(SiteRelations::class, ['site_id_h' => 'id']);
    }

    /**
     * @return \common\models\queries\SiteSoftQuery|\yii\db\ActiveQuery
     */
    public function getSiteSofts()
    {
        return $this->hasMany(SiteSoft::class, ['site_id' => 'id']);
    }

    /**
     * @return \common\models\queries\SoftQuery|\yii\db\ActiveQuery
     */
    public function getSofts()
    {
        return $this->hasMany(Soft::class, ['id' => 'soft_id'])->viaTable(SiteSoft::tableName(), ['site_id' => 'id']);
    }

    /**
     * @return \common\models\queries\SiteInfoQuery|\yii\db\ActiveQuery
     */
    public function getInfo()
    {
        return $this->hasOne(SiteInfo::class, ['id' => 'id']);
    }

    /**
     * @return \common\models\queries\BonusesQuery|\yii\db\ActiveQuery
     */
    public function getBonuses()
    {
        return $this->hasMany(Bonuses::class, ['site_id' => 'id']);
    }

    /**
     * @return \common\models\queries\GameCategorySiteQuery|\yii\db\ActiveQuery
     */
    public function getGameCategorySites()
    {
        return $this->hasMany(GameCategorySite::class, ['site_id' => 'id']);
    }

    /**
     * @return \common\models\queries\GameCategoryQuery|\yii\db\ActiveQuery
     */
    public function getGameCategories()
    {
        return $this->hasMany(GameCategory::class, ['id' => 'game_category_id'])->viaTable(GameCategorySite::tableName(), ['site_id' => 'id']);
    }

    /**
     * @return \common\models\queries\GameProviderSiteQuery|\yii\db\ActiveQuery
     */
    public function getGameProviderSites()
    {
        return $this->hasMany(GameProviderSite::class, ['site_id' => 'id']);
    }

    /**
     * @return \common\models\queries\GameProviderQuery|\yii\db\ActiveQuery
     */
    public function getGameProviders()
    {
        return $this->hasMany(GameProvider::class, ['id' => 'game_provider_id'])->viaTable(GameProviderSite::tableName(), ['site_id' => 'id']);
    }

    /**
     * @return \common\models\queries\SiteBlogPostQuery|\yii\db\ActiveQuery
     */
    public function getSiteBlogPosts()
    {
        return $this->hasMany(SiteBlogPost::class, ['site_id' => 'id']);
    }

    /**
     * @return \common\models\queries\GameProviderQuery|\yii\db\ActiveQuery
     */
    public function getBlogPosts()
    {
        return $this->hasMany(BlogPost::class, ['id' => 'blog_post_id'])->viaTable(SiteBlogPost::tableName(), ['site_id' => 'id']);
    }

    /**
     * @return \common\models\queries\CustomListSiteQuery|\yii\db\ActiveQuery
     */
    public function getCustomListSites()
    {
        return $this->hasMany(CustomListSite::class, ['site_id' => 'id']);
    }

    /**
     * @return \common\models\queries\CustomListQuery|\yii\db\ActiveQuery
     */
    public function getCustomLists()
    {
        return $this->hasMany(CustomList::class, ['id' => 'custom_list_id'])->viaTable(CustomListSite::tableName(), ['site_id' => 'id']);
    }

    public function calculateRating()
    {
        self::calculateSiteRating($this->id);
    }

	/**
	 * Generates links for sitemap.xml
	 * @return string
	 */
	public function getUrl()
	{
		return Url::to(['/casino/view', 'id' => $this->id, 'slug' => $this->slug]);
	}

    /**
     * Calculate rating column
     * @param $site_id
     */
    public static function calculateSiteRating($site_id)
    {
        $predefined_weight = 10;
        $q = (new Query())->from(Review::tableName())
            ->where(['not', ['rating' => NULL]])
            ->andWhere(['<>', 'rating', 0])
            ->andWhere(['site_id' => $site_id]);
        $sum = (int)$q->sum('rating');
        $count = (int)$q->count();
        $expression = new Expression('(predefined_rating * :pw + :sum) / (:pw + :count)',
            [':pw' => $predefined_weight, ':sum' => $sum, ':count' => $count]);

        Yii::$app->db->createCommand()->update(Site::tableName(), [
            'rating' => $expression
        ], ['id' => $site_id])->execute();
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (!$insert && isset($changedAttributes['predefined_rating'])) {
            $this->calculateRating();
        }
    }

	public function getCashback()
	{
		return $this->hasOne(Cashback::class,['site_id' => 'id']);
    }


}
