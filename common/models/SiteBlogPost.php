<?php

namespace common\models;

use common\models\queries\SiteBlogPostQuery;
use Yii;

/**
 * This is the model class for table "site_blog_post".
 *
 * @property int $site_id
 * @property int $blog_post_id
 *
 * @property BlogPost $blogPost
 * @property Site $site
 */
class SiteBlogPost extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%site_blog_post}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id', 'blog_post_id'], 'required'],
            [['site_id', 'blog_post_id'], 'integer'],
            [['site_id', 'blog_post_id'], 'unique', 'targetAttribute' => ['site_id', 'blog_post_id']],
            [['blog_post_id'], 'exist', 'skipOnError' => true, 'targetClass' => BlogPost::className(), 'targetAttribute' => ['blog_post_id' => 'id']],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'site_id' => Yii::t('app', 'Site ID'),
            'blog_post_id' => Yii::t('app', 'Blog Post ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogPost()
    {
        return $this->hasOne(BlogPost::className(), ['id' => 'blog_post_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::className(), ['id' => 'site_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\SiteBlogPostQuery the active query used by this AR class.
     */
    public static function find()
    {
		return Yii::createObject(SiteBlogPostQuery::class, [get_called_class()]);
    }
}
