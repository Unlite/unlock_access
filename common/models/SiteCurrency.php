<?php

namespace common\models;

use common\models\queries\SiteCurrencyQuery;
use Yii;

/**
 * This is the model class for table "site_currency".
 *
 * @property integer $site_id
 * @property string $currency_code
 *
 * @property Currency $currency
 * @property Site $site
 */
class SiteCurrency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%site_currency}}';
    }

    /**
     * @inheritdoc
     * @return SiteCurrencyQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(SiteCurrencyQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id', 'currency_code'], 'required'],
            [['site_id'], 'integer'],
            [['currency_code'], 'string', 'max' => 5],
            [['currency_code'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::class, 'targetAttribute' => ['currency_code' => 'code']],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::class, 'targetAttribute' => ['site_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'site_id' => Yii::t('app', 'Site ID'),
            'currency_code' => Yii::t('app', 'Currency Code'),
        ];
    }

    /**
     * @return \common\models\queries\CurrencyQuery|\yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::class, ['code' => 'currency_code']);
    }

    /**
     * @return \common\models\queries\SiteQuery|\yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::class, ['id' => 'site_id']);
    }
}
