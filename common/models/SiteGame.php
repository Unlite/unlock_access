<?php

namespace common\models;

use common\models\queries\SiteGameQuery;
use Yii;
use common\models\GameCategorySite;
use common\models\GameProviderSite;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%site_game}}".
 *
 * @property integer $site_id
 * @property integer $game_id
 * @property integer $is_promoted
 *
 * @property Game $game
 * @property Site $site
 */
class SiteGame extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%site_game}}';
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\SiteGameQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(SiteGameQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id', 'game_id', 'is_promoted'], 'required'],
            [['site_id', 'game_id', 'is_promoted'], 'integer'],
            [['game_id'], 'exist', 'skipOnError' => true, 'targetClass' => Game::class, 'targetAttribute' => ['game_id' => 'id']],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::class, 'targetAttribute' => ['site_id' => 'id']],
            [['site_id', 'game_id'], 'unique', 'targetAttribute' => ['site_id', 'game_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'site_id' => 'Site ID',
            'game_id' => 'Game ID',
            'is_promoted' => 'Promoted',
        ];
    }

    /**
     * @return \common\models\queries\GameQuery|\yii\db\ActiveQuery
     */
    public function getGame()
    {
        return $this->hasOne(Game::class, ['id' => 'game_id']);
    }

    /**
     * @return \common\models\queries\SiteQuery|\yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::class, ['id' => 'site_id']);
    }

    public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);

        if ($this->isNewRecord) {
            if (GameProviderSite::find()->where(['game_provider_id' => $this->game->gameProvider->id])->count() == 0) {
                $gameProviderSite = new GameProviderSite();
                $gameProviderSite->game_provider_id = $this->game->gameProvider->id;
                $gameProviderSite->site_id = $this->site_id;
                $gameProviderSite->is_auto_created = 1;
                $gameProviderSite->save();
            }

            $searchedGameCategorySites = GameCategorySite::find()->where(['in', 'game_category_id', ArrayHelper::getColumn($this->game->gameCategoryGames, 'game_category_id')])->all();
            $gameCategorySites = $this->game->gameCategoryGames;
            if (count($gameCategorySites) != count($searchedGameCategorySites)) {
                foreach ($gameCategorySites as $key => $gameCategorySite) {
                    foreach ($searchedGameCategorySites as $searchedGameCategorySite) {
                        if ($searchedGameCategorySite->game_category_id == $gameCategorySite->game_category_id)
                            if (isset($gameCategorySite[$key])) unset($gameCategorySite[$key]);
                    }
                }
            }

            foreach ($gameCategorySites as $gameCategorySite) {
                $gameProviderSite = new GameCategorySite();
                $gameProviderSite->game_category_id = $gameCategorySite->game_category_id;
                $gameProviderSite->site_id = $this->site_id;
                $gameProviderSite->is_auto_created = 1;
                $gameProviderSite->save();
            }
        }
        return $return;
    }
}
