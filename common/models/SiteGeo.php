<?php

namespace common\models;

use common\models\queries\SiteGeoQuery;
use Yii;

/**
 * This is the model class for table "{{%site_geo}}".
 *
 * @property integer $site_id
 * @property boolean $is_allowed
 * @property string $geo_code
 *
 * @property Geo $geo
 * @property Site $site
 */
class SiteGeo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%site_geo}}';
    }


    /**
     * @inheritdoc
     * @return SiteGeoQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(SiteGeoQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id', 'geo_code', 'is_allowed'], 'required'],
            [['site_id'], 'integer'],
            [['is_allowed'], 'boolean'],
            [['geo_code'], 'string', 'max' => 5],
            [['geo_code'], 'exist', 'skipOnError' => true, 'targetClass' => Geo::class, 'targetAttribute' => ['geo_code' => 'code']],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::class, 'targetAttribute' => ['site_id' => 'id']],
            [['site_id', 'geo_code'], 'unique', 'targetAttribute' => ['site_id', 'geo_code']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'site_id' => 'Site ID',
            'geo_code' => 'Geo Code',
        ];
    }

    /**
     * @return \common\models\queries\GeoQuery|\yii\db\ActiveQuery
     */
    public function getGeo()
    {
        return $this->hasOne(Geo::class, ['code' => 'geo_code']);
    }

    /**
     * @return \common\models\queries\SiteQuery|\yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::class, ['id' => 'site_id']);
    }
}
