<?php

namespace common\models;

use common\models\queries\SiteImageQuery;
use Yii;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii2mod\ftp\FtpClient;

/**
 * This is the model class for table "site_image".
 *
 * @property integer $id
 * @property integer $site_id
 * @property string $type
 * @property string $src
 * @property string $imageFile
 *
 * @property Site $site
 */
class SiteImage extends \yii\db\ActiveRecord
{
	public $imageFile;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%site_image}}';
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\SiteImageQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(SiteImageQuery::class, [get_called_class()]);
    }

    public static function getDefaultTypes()
    {
        return ['rect' => 'rect', 'sqr' => 'sqr'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id', 'type'], 'required'],
            [['site_id'], 'integer'],
			[['imageFile'], 'file', 'extensions' => 'png, jpg, svg, jpeg, gif'],
            [['type'], 'string', 'max' => 128],
            [['src'], 'string', 'max' => 1024],
            [['src'], 'url', 'validSchemes' => ['http', 'https'], 'pattern' => '/^({schemes}:|)\/\/(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)(?::\d{1,5})?(?:$|[?\/#])/i'],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::class, 'targetAttribute' => ['site_id' => 'id']],
        ];
    }

	public function beforeSave($insert)
	{
		$return = parent::beforeSave($insert);
		$this->imageFile = UploadedFile::getInstance($this, 'imageFile');
		if($this->imageFile){
			/* @var \yii2mod\ftp\FtpClient $ftp */
			$ftp = new FtpClient();
			$ftp->connect(Yii::$app->params['ftpHost']);
			$ftp->login(Yii::$app->params['ftpLogin'], Yii::$app->params['ftpPassword']);
			$ftp->pasv(true);
			if(!$ftp->isDir('casino')){
				$ftp->mkdir('casino');
			}
			if(!$ftp->isDir('casino/'.$this->type)){
				$ftp->mkdir('casino/'.$this->type);
			}
			$random_name = Yii::$app->security->generateRandomString(16).'.'.$this->imageFile->extension;
			if($ftp->put('casino/'.$this->type.'/'.$random_name,$this->imageFile->tempName,FTP_BINARY)){
				$this->src = '//'.Yii::$app->params['cdnHost'].'/'.'casino/'.$this->type.'/'.$random_name;
				return $return;
			} else {
				$this->addError('imageFile','Could not upload file to ftp');
				$return = false;
			}
		}
		return $return;
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'site_id' => 'Site ID',
            'type' => 'Type',
            'src' => 'Src',
            'imageFile' => 'Image file',
        ];
    }

    /**
     * @return \common\models\queries\SiteQuery|\yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::class, ['id' => 'site_id']);
    }
}
