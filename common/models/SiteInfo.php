<?php

namespace common\models;

use common\models\queries\SiteInfoQuery;
use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%site_info}}".
 *
 * @property integer $id
 * @property string $casino_max_pay
 * @property string $casino_max_win
 * @property string $description
 * @property string $min_deposit
 * @property string $min_withdraw
 * @property string $max_withdraw
 * @property string $payout
 * @property integer $online_since
 * @property string $owner
 * @property string $email
 * @property string $support_types
 * @property string $review
 * @property integer $is_mobile_friendly
 * @property integer $best_support
 *
 * @property Site $site
 */
class SiteInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%site_info}}';
    }

    /**
     * @inheritdoc
     * @return SiteInfoQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(SiteInfoQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_mobile_friendly', 'best_support'], 'integer'],
            [['online_since'],'date','format' => 'php:Y'],
            [['casino_max_pay', 'casino_max_win'], 'string', 'max' => 64],
            [['description', 'min_deposit', 'min_withdraw', 'max_withdraw', 'payout', 'owner', 'email', 'support_types'], 'string', 'max' => 255],
			[['page_title'], 'string', 'max' => 100],
			[['page_description'], 'string', 'max' => 300],
			[['review'], 'string', 'max' => 15000],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::class, 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'casino_max_pay' => Yii::t('app', 'Casino Max Pay'),
            'casino_max_win' => Yii::t('app', 'Casino Max Win'),
            'description' => Yii::t('app', 'Description'),
            'min_deposit' => Yii::t('app', 'Min Deposit'),
            'min_withdraw' => Yii::t('app', 'Min Withdraw'),
            'max_withdraw' => Yii::t('app', 'Max Withdraw'),
            'payout' => Yii::t('app', 'Payout'),
            'online_since' => Yii::t('app', 'Online Since'),
            'owner' => Yii::t('app', 'Owner'),
            'email' => Yii::t('app', 'Email'),
            'support_types' => Yii::t('app', 'Support Types'),
            'is_mobile_friendly' => Yii::t('app', 'Is Mobile Friendly'),
            'best_support' => Yii::t('app', 'Best Support'),
			'page_title' => Yii::t('app', 'Page Title'),
			'page_description' => Yii::t('app', 'Page Description'),
			'review' => Yii::t('app', 'Review'),
        ];
    }

    /**
     * @return \common\models\queries\SiteQuery|\yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::class, ['id' => 'id']);
    }
}
