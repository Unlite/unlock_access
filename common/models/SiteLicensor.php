<?php

namespace common\models;

use common\models\queries\SiteLicensorQuery;
use Yii;

/**
 * This is the model class for table "site_licensor".
 *
 * @property integer $site_id
 * @property integer $licensor_id
 *
 * @property Licensor $licensor
 * @property Site $site
 */
class SiteLicensor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%site_licensor}}';
    }

    /**
     * @inheritdoc
     * @return SiteLicensorQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(SiteLicensorQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id', 'licensor_id'], 'required'],
            [['site_id', 'licensor_id'], 'integer'],
            [['licensor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Licensor::class, 'targetAttribute' => ['licensor_id' => 'id']],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::class, 'targetAttribute' => ['site_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'site_id' => Yii::t('app', 'Site ID'),
            'licensor_id' => Yii::t('app', 'Licensor ID'),
        ];
    }

    /**
     * @return \common\models\queries\LicensorQuery|\yii\db\ActiveQuery
     */
    public function getLicensor()
    {
        return $this->hasOne(Licensor::class, ['id' => 'licensor_id']);
    }

    /**
     * @return \common\models\queries\SiteQuery|\yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::class, ['id' => 'site_id']);
    }
}
