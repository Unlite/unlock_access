<?php

namespace common\models;

use common\models\queries\SiteLocaleQuery;
use Yii;

/**
 * This is the model class for table "site_locale".
 *
 * @property integer $site_id
 * @property string $locale_code
 *
 * @property Locale $locale
 * @property Site $site
 */
class SiteLocale extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%site_locale}}';
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\SiteLocaleQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(SiteLocaleQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id', 'locale_code'], 'required'],
            [['site_id'], 'integer'],
            [['locale_code'], 'string', 'max' => 5],
            [['locale_code'], 'exist', 'skipOnError' => true, 'targetClass' => Locale::class, 'targetAttribute' => ['locale_code' => 'code']],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::class, 'targetAttribute' => ['site_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'site_id' => Yii::t('app', 'Site ID'),
            'locale_code' => Yii::t('app', 'Locale Code'),
        ];
    }

    /**
     * @return \common\models\queries\LocaleQuery|\yii\db\ActiveQuery
     */
    public function getLocale()
    {
        return $this->hasOne(Locale::class, ['code' => 'locale_code']);
    }

    /**
     * @return \common\models\queries\SiteQuery|\yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::class, ['id' => 'site_id']);
    }
}
