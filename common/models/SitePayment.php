<?php

namespace common\models;

use common\models\queries\SitePaymentQuery;
use Yii;

/**
 * This is the model class for table "{{%site_payment}}".
 *
 * @property integer $site_id
 * @property integer $payment_id
 * @property bool $is_deposit [tinyint(1)]
 * @property bool $is_withdrawal [tinyint(1)]
 *
 * @property PaymentSystem $payment
 * @property Site $site
 */
class SitePayment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%site_payment}}';
    }

    /**
     * @inheritdoc
     * @return SitePaymentQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(SitePaymentQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id', 'payment_id', 'is_deposit', 'is_withdrawal'], 'required'],
            [['site_id', 'payment_id',], 'integer'],
            [['is_deposit', 'is_withdrawal'], 'boolean'],
            [['payment_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentSystem::class, 'targetAttribute' => ['payment_id' => 'id']],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::class, 'targetAttribute' => ['site_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'site_id' => Yii::t('app', 'Site ID'),
            'payment_id' => Yii::t('app', 'PaymentSystem ID'),
            'is_deposit' => Yii::t('app', 'Is Deposit'),
            'is_withdrawal' => Yii::t('app', 'Is Withdrawal'),
        ];
    }

    /**
     * @return \common\models\queries\PaymentSystemQuery|\yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(PaymentSystem::class, ['id' => 'payment_id']);
    }

    /**
     * @return \common\models\queries\SiteQuery|\yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::class, ['id' => 'site_id']);
    }
}
