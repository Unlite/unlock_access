<?php

namespace common\models;

use common\models\queries\SiteRelationsQuery;
use Yii;

/**
 * This is the model class for table "site_relations".
 *
 * @property integer $site_id_l
 * @property integer $site_id_h
 *
 * @property Site $siteH
 * @property Site $siteL
 */
class SiteRelations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%site_relations}}';
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\SiteRelationsQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(SiteRelationsQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id_l', 'site_id_h'], 'required'],
            [['site_id_l', 'site_id_h'], 'integer'],
            [['site_id_h'], 'exist', 'skipOnError' => true, 'targetClass' => Site::class, 'targetAttribute' => ['site_id_h' => 'id']],
            [['site_id_l'], 'exist', 'skipOnError' => true, 'targetClass' => Site::class, 'targetAttribute' => ['site_id_l' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'site_id_l' => Yii::t('app', 'Site Id L'),
            'site_id_h' => Yii::t('app', 'Site Id H'),
        ];
    }

    public function beforeValidate()
    {
        $min = min($this->site_id_l, $this->site_id_h);
        $max = max($this->site_id_l, $this->site_id_h);
        $this->site_id_l = $min;
        $this->site_id_h = $max;
        return parent::beforeValidate();
    }

    /**
     * @return \common\models\queries\SiteQuery|\yii\db\ActiveQuery
     */
    public function getSiteH()
    {
        return $this->hasOne(Site::class, ['id' => 'site_id_h']);
    }

    /**
     * @return \common\models\queries\SiteQuery|\yii\db\ActiveQuery
     */
    public function getSiteL()
    {
        return $this->hasOne(Site::class, ['id' => 'site_id_l']);
    }
}
