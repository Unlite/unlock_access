<?php

namespace common\models;

use common\models\queries\SiteSoftQuery;
use Yii;

/**
 * This is the model class for table "{{%site_soft}}".
 *
 * @property integer $site_id
 * @property integer $soft_id
 *
 * @property Site $site
 * @property Soft $soft
 */
class SiteSoft extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%site_soft}}';
    }

    /**
     * @inheritdoc
     * @return SiteSoftQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(SiteSoftQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id', 'soft_id'], 'required'],
            [['site_id', 'soft_id'], 'integer'],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::class, 'targetAttribute' => ['site_id' => 'id']],
            [['soft_id'], 'exist', 'skipOnError' => true, 'targetClass' => Soft::class, 'targetAttribute' => ['soft_id' => 'id']],
            [['site_id', 'soft_id'], 'unique', 'targetAttribute' => ['site_id', 'soft_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'site_id' => 'Site ID',
            'soft_id' => 'Soft ID',
        ];
    }

    /**
     * @return \common\models\queries\SiteQuery|\yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::class, ['id' => 'site_id']);
    }

    /**
     * @return \common\models\queries\SoftQuery|\yii\db\ActiveQuery
     */
    public function getSoft()
    {
        return $this->hasOne(Soft::class, ['id' => 'soft_id']);
    }
}
