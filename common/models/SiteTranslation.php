<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%site_translation}}".
 *
 * @property integer $site_id
 * @property string $geo_code
 * @property string $description
 * @property string $promo_text
 *
 * @property Geo $geo
 * @property Site $site
 * @deprecated
 */
class SiteTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%site_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id', 'geo_code'], 'required'],
            [['site_id'], 'integer'],
            [['geo_code'], 'string', 'max' => 5],
            [['description'], 'string', 'max' => 1024],
            [['promo_text'], 'string', 'max' => 256],
            [['geo_code'], 'exist', 'skipOnError' => true, 'targetClass' => Geo::class, 'targetAttribute' => ['geo_code' => 'code']],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::class, 'targetAttribute' => ['site_id' => 'id']],
            [['site_id', 'geo_code'], 'unique', 'targetAttribute' => ['site_id', 'geo_code']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'site_id' => 'Site ID',
            'geo_code' => 'Geo Code',
            'description' => 'Description',
            'promo_text' => 'Promo Text',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeo()
    {
        return $this->hasOne(Geo::class, ['code' => 'geo_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::class, ['id' => 'site_id']);
    }
}
