<?php

namespace common\models;

use common\models\queries\SoftQuery;
use Yii;

/**
 * This is the model class for table "{{%soft}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $img_src
 *
 * @property SiteSoft[] $siteSofts
 * @property Site[] $sites
 */
class Soft extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%soft}}';
    }

    /**
     * @inheritdoc
     * @return SoftQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(SoftQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 256],
            [['img_src'], 'string', 'max' => 1024],
            [['img_src'], 'url', 'defaultScheme' => 'http'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'img_src' => 'Img Src',
        ];
    }

    /**
     * @return \common\models\queries\SiteSoftQuery|\yii\db\ActiveQuery
     */
    public function getSiteSofts()
    {
        return $this->hasMany(SiteSoft::class, ['soft_id' => 'id']);
    }

    /**
     * @return \common\models\queries\SiteQuery|\yii\db\ActiveQuery
     */
    public function getSites()
    {
        return $this->hasMany(Site::class, ['id' => 'site_id'])->viaTable(SiteSoft::tableName(), ['soft_id' => 'id']);
    }
}
