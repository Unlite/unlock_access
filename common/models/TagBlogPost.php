<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tag_blog_post}}".
 *
 * @property int $tag_id
 * @property int $blog_post_id
 *
 * @property BlogPost $blogPost
 * @property Tag $tag
 */
class TagBlogPost extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tag_blog_post}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tag_id', 'blog_post_id'], 'required'],
            [['tag_id', 'blog_post_id'], 'integer'],
            [['tag_id', 'blog_post_id'], 'unique', 'targetAttribute' => ['tag_id', 'blog_post_id']],
            [['blog_post_id'], 'exist', 'skipOnError' => true, 'targetClass' => BlogPost::className(), 'targetAttribute' => ['blog_post_id' => 'id']],
            [['tag_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tag::className(), 'targetAttribute' => ['tag_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tag_id' => Yii::t('app', 'Tag ID'),
            'blog_post_id' => Yii::t('app', 'Blog Post ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogPost()
    {
        return $this->hasOne(BlogPost::className(), ['id' => 'blog_post_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTag()
    {
        return $this->hasOne(Tag::className(), ['id' => 'tag_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\TagBlogPostQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\TagBlogPostQuery(get_called_class());
    }
}
