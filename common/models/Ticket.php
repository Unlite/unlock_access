<?php

namespace common\models;

use common\models\queries\TicketQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "ticket".
 *
 * @property int $id
 * @property int $user_id
 * @property string $user_locale
 * @property string $status
 * @property string $title
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $user
 * @property Locale $userLocale
 * @property TicketMessage[] $ticketMessages
 */
class Ticket extends \yii\db\ActiveRecord
{
	const STATUS_NEW = 'new';
	const STATUS_OPENED = 'opened';
	const STATUS_CLOSED = 'closed';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%ticket}}';
    }
    /**
     * All available types
     * @return array
     */
    public static function statuses()
    {
        return [self::STATUS_NEW, self::STATUS_OPENED, self::STATUS_CLOSED];
    }

	public function behaviors()
	{
		return [
			[
				'class' => TimestampBehavior::class,
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
	}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'user_locale', 'status', 'title', 'created_at', 'updated_at'], 'required'],
            [['user_id', 'created_at', 'updated_at'], 'integer'],
            [['status'], 'string'],
            [['status'], 'in', 'range' => self::statuses()],
            [['user_locale'], 'string', 'max' => 5],
            [['title'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['user_locale'], 'exist', 'skipOnError' => true, 'targetClass' => Locale::className(), 'targetAttribute' => ['user_locale' => 'code']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'user_locale' => 'User Locale',
            'status' => 'Status',
            'title' => 'Title',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserLocale()
    {
        return $this->hasOne(Locale::className(), ['code' => 'user_locale']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTicketMessages()
    {
        return $this->hasMany(TicketMessage::className(), ['ticket_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\queries\TicketQuery the active query used by this AR class.
     */
    public static function find()
    {
		return Yii::createObject(TicketQuery::class, [get_called_class()]);
    }
}
