<?php

namespace common\models;

use common\models\queries\TicketImageQuery;
use Yii;

/**
 * This is the model class for table "ticket_image".
 *
 * @property int $id
 * @property int $ticket_message_id
 * @property string $src
 *
 * @property TicketMessage $ticketMessage
 */
class TicketImage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%ticket_image}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ticket_message_id', 'src'], 'required'],
            [['ticket_message_id'], 'integer'],
            [['src'], 'string'],
            [['ticket_message_id'], 'exist', 'skipOnError' => true, 'targetClass' => TicketMessage::className(), 'targetAttribute' => ['ticket_message_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ticket_message_id' => 'Ticket Message ID',
            'src' => 'Src',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTicketMessage()
    {
        return $this->hasOne(TicketMessage::className(), ['id' => 'ticket_message_id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\queries\TicketImageQuery the active query used by this AR class.
     */
    public static function find()
    {
		return Yii::createObject(TicketImageQuery::class, [get_called_class()]);
    }
}
