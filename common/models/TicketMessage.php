<?php

namespace common\models;

use common\models\queries\TicketMessageQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "ticket_message".
 *
 * @property int $id
 * @property int $ticket_id
 * @property int $user_id
 * @property string $status
 * @property string $message
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TicketImage[] $ticketImages
 * @property Ticket $ticket
 * @property User $user
 */
class TicketMessage extends \yii\db\ActiveRecord
{

	const STATUS_NEW = 'new';
	const STATUS_READ = 'read';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%ticket_message}}';
    }

	public function behaviors()
	{
		return [
			[
				'class' => TimestampBehavior::class,
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
	}
	/**
     * All available types
     * @return array
     */
    public static function statuses()
    {
        return [self::STATUS_NEW, self::STATUS_READ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ticket_id', 'user_id', 'status', 'message', 'created_at', 'updated_at'], 'required'],
            [['ticket_id', 'user_id', 'created_at', 'updated_at'], 'integer'],
            [['status', 'message'], 'string'],
			[['status'], 'in', 'range' => self::statuses()],
            [['ticket_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ticket::className(), 'targetAttribute' => ['ticket_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ticket_id' => 'Ticket ID',
            'user_id' => 'User ID',
            'status' => 'Status',
            'message' => 'Message',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTicketImages()
    {
        return $this->hasMany(TicketImage::className(), ['ticket_message_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTicket()
    {
        return $this->hasOne(Ticket::className(), ['id' => 'ticket_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\queries\TicketMessageQuery the active query used by this AR class.
     */
    public static function find()
    {
		return Yii::createObject(TicketMessageQuery::class, [get_called_class()]);
    }
}
