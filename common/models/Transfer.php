<?php

namespace common\models;

use common\models\queries\TransferQuery;
use Yii;

/**
 * This is the model class for table "{{%transfer}}".
 *
 * @property int $id
 * @property int $user_currency_id
 * @property string $type
 * @property string $status
 * @property string $amount
 * @property string $comment
 * @property int $parent_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Transfer $parent
 * @property Transfer[] $childs
 * @property UserCurrency $userCurrency
 */
class Transfer extends \yii\db\ActiveRecord
{

    const STATUS_HOLD = 'hold';
    const STATUS_APPROVED = 'approved';
    const STATUS_REJECTED = 'rejected';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%transfer}}';
    }

	public static function instantiate($row)
	{
		switch ($row['type']) {
			case Exchange::TYPE:
				return new Exchange();
			case CashbackTransfer::TYPE:
				return new CashbackTransfer();
			case WithdrawTransfer::TYPE:
				return new WithdrawTransfer();
			default:
				return new self;
		}
	}

    /**
     * @inheritdoc
     * @return TransferQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(TransferQuery::class, [get_called_class()]);
    }

//    /**
//     * All available types
//     * @return array
//     */
//    public static function types()
//    {
//        return [self::TYPE_CASHBACK, self::TYPE_EXCHANGE, self::TYPE_OTHER];
//    }

    /**
     * All available statuses
     * @return array
     */
    public static function statuses()
    {
        return [self::STATUS_APPROVED, self::STATUS_HOLD, self::STATUS_REJECTED];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_currency_id', 'amount', 'type', 'status'], 'required'],
            [['user_currency_id', 'parent_id'], 'integer'],
//            [['type'], 'in', 'range' => static::types()],
            [['status'], 'in', 'range' => static::statuses()],
            [['amount'], 'number'],
            [['created_at', 'updated_at'], 'date', 'format' => 'dd.MM.yyyy'],
            [['data'], 'string'],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Transfer::class, 'targetAttribute' => ['parent_id' => 'id']],
            [['user_currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserCurrency::class, 'targetAttribute' => ['user_currency_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_currency_id' => 'User Currency ID',
            'type' => 'Type',
            'status' => 'Status',
            'amount' => 'Amount',
            'Data' => 'Data',
            'parent_id' => 'Parent ID',
            'payment_system_id' => 'Payment System ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \common\models\queries\TransferQuery|\yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Transfer::class, ['id' => 'parent_id']);
    }

    /**
     * @return \common\models\queries\TransferQuery|\yii\db\ActiveQuery
     */
    public function getChilds()
    {
        return $this->hasMany(Transfer::class, ['parent_id' => 'id']);
    }

    /**
     * @return \common\models\queries\UserCurrencyQuery|\yii\db\ActiveQuery
     */
    public function getUserCurrency()
    {
        return $this->hasOne(UserCurrency::class, ['id' => 'user_currency_id']);
    }
}
