<?php

namespace common\models;

use common\models\queries\TranslateQuery;
use Yii;

/**
 * This is the model class for table "translate".
 *
 * @property int $id
 * @property string $locale_code
 * @property int $table_id
 * @property string $table
 * @property string $key
 * @property string $value
 * @property int $serialized
 *
 * @property Locale $locale
 */
class Translate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%translate}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['table_id', 'table', 'key'], 'required'],
            [['table_id', 'serialized'], 'integer'],
            [['value'], 'string'],
            [['locale_code'], 'string', 'max' => 5],
            [['table'], 'string', 'max' => 64],
            [['key'], 'string', 'max' => 255],
            [['locale_code'], 'exist', 'skipOnError' => true, 'targetClass' => Locale::className(), 'targetAttribute' => ['locale_code' => 'code']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'locale_code' => 'Locale Code',
            'table_id' => 'Table ID',
            'table' => 'Table',
            'key' => 'Key',
            'value' => 'Value',
            'serialized' => 'Serialized',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocale()
    {
        return $this->hasOne(Locale::className(), ['code' => 'locale_code']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\queries\TranslateQuery the active query used by this AR class.
     */
    public static function find()
    {
		return Yii::createObject(TranslateQuery::class, [get_called_class()]);
    }
}
