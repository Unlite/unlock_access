<?php

namespace common\models;

use common\models\queries\UserCurrencyQuery;
use Yii;

/**
 * This is the model class for table "{{%user_currency}}".
 *
 * @property int $id
 * @property int $user_id
 * @property string $currency_code
 * @property string $amount
 *
 * @property Transfer[] $transfers
 * @property WithdrawTransfer[] $withdraws
 * @property Exchange[] $exchanges
 * @property Currency $currency
 * @property User $user
 */
class UserCurrency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_currency}}';
    }

    /**
     * @inheritdoc
     * @return UserCurrencyQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(UserCurrencyQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'currency_code', 'amount'], 'required'],
            [['user_id'], 'integer'],
            [['amount'], 'number'],
            [['currency_code'], 'string', 'max' => 5],
            [['user_id', 'currency_code'], 'unique', 'targetAttribute' => ['user_id', 'currency_code']],
            [['currency_code'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::class, 'targetAttribute' => ['currency_code' => 'code']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'currency_code' => 'Currency Code',
            'amount' => 'Amount',
        ];
    }

    /**
     * @return \common\models\queries\TransferQuery|\yii\db\ActiveQuery
     */
    public function getTransfers()
    {
        return $this->hasMany(Transfer::class, ['user_currency_id' => 'id']);
    }

    /**
     * @return \common\models\queries\TransferQuery|\yii\db\ActiveQuery
     */
    public function getWithdraws()
    {
        return $this->hasMany(WithdrawTransfer::class, ['user_currency_id' => 'id'])->andWhere(['parent_id' => null]);
    }

    /**
     * @return \common\models\queries\TransferQuery|\yii\db\ActiveQuery
     */
    public function getExchanges()
    {
        return $this->hasMany(Exchange::class, ['user_currency_id' => 'id'])->andWhere(['parent_id' => null]);
    }

    /**
     * @return \common\models\queries\CurrencyQuery|\yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::class, ['code' => 'currency_code']);
    }

    /**
     * @return \common\models\queries\UserCurrencyQuery|\yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
