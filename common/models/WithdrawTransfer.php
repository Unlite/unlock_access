<?php

namespace common\models;

use common\models\queries\TransferQuery;
use Yii;

/**
 * @inheritdoc
 * @property PaymentSystem $payment_system
 * @property WithdrawTransfer $transferCommission
 */

class WithdrawTransfer extends Transfer
{
	const TYPE = 'withdraw';

	/**
	 * @inheritdoc
	 */
	public function init()
	{
		$this->type = self::TYPE;
		parent::init();
	}

	public function rules()
	{
		return array_merge(parent::rules(),[
			['payment_system_id','required'],
			['payment_system_id','integer'],
			[['payment_system_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentSystem::class, 'targetAttribute' => ['payment_system_id' => 'id']],
		]);
	}

	/**
	 * @return \common\models\queries\PaymentSystemQuery|\yii\db\ActiveQuery
	 */
	public function getPaymentSystem()
	{
		return $this->hasOne(PaymentSystem::class, ['id' => 'payment_system_id']);
	}

	/**
	 * @return \common\models\queries\TransferQuery|\yii\db\ActiveQuery
	 */
	public function getTransferCommission()
	{
		return $this->hasOne(WithdrawTransfer::class, ['parent_id' => 'id'])->andWhere(['user_currency_id' => $this->user_currency_id]);
	}

	/**
	 * @inheritdoc
	 * @return TransferQuery|\yii\db\ActiveQuery
	 */
	public static function find()
	{
		return Yii::createObject(['class' => TransferQuery::class, 'type' => self::TYPE, 'tableName' => self::tableName()], [get_called_class()]);
	}

}
