<?php

namespace common\models\forms;

use common\models\BlogPost;

/**
 * @property integer $udt
 * @property integer $updated_date_time
 *
 */
class BlogPostForm extends BlogPost
{
	public $_udt;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge([[['udt'], 'date','format' => 'php:d.m.Y']],parent::rules());
    }

	public function beforeValidate()
	{
		$this->updated_date_time = !empty($this->_udt) ? strtotime($this->_udt) : 0;
		return true;
    }

	public function getUdt()
	{
		return $this->isNewRecord ? date('d.m.Y') : date('d.m.Y',$this->updated_date_time);
    }

	public function setUdt($udt)
	{
		$this->_udt = $udt;
    }

}
