<?php

namespace common\models\forms;

use Yii;
use common\models\Bonuses;

/**
 * This is the model class for table "bonuses".
 *
 * @property integer $id
 * @property integer $site_id
 * @property string $type
 * @property string $text
 * @property string $url
 * @property string $promo_code
 * @property integer $start_at
 * @property integer $end_at
 *
 * @property Site $site
 */
class BonusesForm extends Bonuses
{
	public $_st;
	public $_et;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge([[['st', 'et'], 'date','format' => 'php:d.m.Y']],parent::rules());
    }

	public function beforeValidate()
	{
		$this->start_at = !empty($this->_st) ? strtotime($this->_st) : 0;
		$this->end_at = !empty($this->_et) ? strtotime($this->_et) : 0;
		return true;
    }

	public function getSt()
	{
		return $this->isNewRecord ? date('d.m.Y') : date('d.m.Y',$this->start_at);
    }

	public function getEt()
	{
		return $this->isNewRecord ? date('d.m.Y') : date('d.m.Y',$this->end_at);
    }

	public function setSt($st)
	{
		$this->_st = $st;
    }

	public function setEt($et)
	{
		$this->_et = $et;
    }

}
