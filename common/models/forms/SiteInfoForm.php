<?php

namespace common\models\forms;

use common\models\SiteInfo;

/**
 * @property integer $rua
 * @property integer $review_updated_at
 *
 */
class SiteInfoForm extends SiteInfo
{
	public $_rua;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge([[['rua'], 'date','format' => 'php:d.m.Y']],parent::rules());
    }

	public function beforeValidate()
	{
		$this->review_updated_at = !empty($this->_rua) ? strtotime($this->_rua) : 0;
		return true;
    }

	public function getRua()
	{
		return $this->isNewRecord ? date('d.m.Y') : date('d.m.Y',$this->review_updated_at);
    }

	public function setRua($rua)
	{
		$this->_rua = $rua;
    }

}
