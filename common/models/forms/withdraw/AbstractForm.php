<?php

namespace common\models\forms\withdraw;

use common\components\balance\WithdrawFormInterface;
use common\models\Currency;
use common\models\queries\CurrencyQuery;
use yii\base\Model;

abstract class AbstractForm extends Model implements WithdrawFormInterface
{
    public $currency_code;
    public $amount;

    public function rules()
    {
        return [
            ['currency', 'trim'],
            ['currency', 'required'],
            ['currency', 'string', 'length' => 3],
            [
                'currency',
                'exist',
                'targetClass' => Currency::class,
                'targetAttribute' => 'code',
                'filter' => function (CurrencyQuery $query) {
                    return $query->system();
                }
            ],
        ];
    }

    public static function currencyRules()
    {
        return [

        ];
    }
}