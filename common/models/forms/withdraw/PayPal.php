<?php

namespace common\models\forms\withdraw;


use common\components\balance\WithdrawFormInterface;
use yii\base\Model;

/**
 *
 * @property float $amount
 * @property void $extraData
 */
class PayPal extends AbstractForm
{
    const COMMISSION_RATE = 0.05;

    public $amount;
    public $target;

    public static function getName(): string
    {
        return 'paypal';
    }

    /**
     * @inheritdoc
     */
    public static function getAvailableCurrencies(): array
    {
        return ['usd', 'rub'];
    }

    public function rules()
    {
        return [
            ['amount', 'number'],
            ['target', 'trim'],
            ['target', 'string', 'max' => 128],
            [['amount', 'target'], 'required']
        ];
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @inheritdoc
     */
    public function getExtraData(): ?array
    {
        return [];
    }


    public static function getCommissionRate(): float
    {
        return static::COMMISSION_RATE;
    }
}