<?php

namespace common\models\queries;

/**
 * This is the ActiveQuery class for [[\common\models\BlogPostBlogPostCategory]].
 *
 * @see \common\models\BlogPostBlogPostCategory
 */
class BlogPostBlogPostCategoryQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \common\models\BlogPostBlogPostCategory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }


	/**
	 * @param $blog_post_category
	 * @param string $alias
	 * @return $this
	 */
	public function byBlogPostCategory($blog_post_category, $alias = '')
	{
		return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.blog_post_category_id' => $blog_post_category]);
	}

    /**
     * {@inheritdoc}
     * @return \common\models\BlogPostBlogPostCategory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
