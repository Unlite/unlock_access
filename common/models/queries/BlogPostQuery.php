<?php

namespace common\models\queries;

use common\models\BlogPost;
use yii\db\ActiveQuery;

/**
 * Class BlogPostQuery
 * @package common\models\queries
 * @see \common\models\BlogPost
 *
 * @method BlogPost[] all($db = null)
 * @method BlogPost one($db = null)
 */
class BlogPostQuery extends ActiveQuery
{
    /**
     * @param bool $is
     * @param string|null $alias
     * @return $this
     */
    public function published(bool $is = true, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.is_published' => $is]);
    }

	/**
	 * @param $value
	 * @param string $alias
	 * @return $this
	 */
	public function slug($value, $alias = '')
	{
		return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.slug' => $value]);
	}

    /**
     * @param string $alias
     * @return $this
     */
    public function active($alias = '')
    {
        return $this->published(true, $alias);
    }
}