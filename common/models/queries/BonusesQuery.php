<?php

namespace common\models\queries;

use common\models\Bonuses;
use yii\db\ActiveQuery;

/**
 * Class Bonuses
 * @package common\models\queries
 * @see \common\models\Bonuses
 *
 * @method Bonuses[] all($db = null)
 * @method Bonuses one($db = null)
 */
class BonusesQuery extends ActiveQuery
{
    /**
     * @param bool $is
     * @param null $alias
     * @return $this
     */
    public function actual(bool $is = true, $alias = null)
    {
        if ($is) return $this->andWhere([
            'and',
            ['<', ($alias ?: $this->modelClass::tableName()) . '.start_at', time()],
            ['>=', ($alias ?: $this->modelClass::tableName()) . '.end_at', strtotime("midnight")]
        ]);

        return $this->andWhere([
            'or',
            ['>', ($alias ?: $this->modelClass::tableName()) . '.start_at', time()],
            ['<', ($alias ?: $this->modelClass::tableName()) . '.end_at', strtotime("midnight")]
        ]);
    }

	/**
	 * @param bool $is
	 * @param string $alias
	 * @return $this
	 */
	public function active(bool $is = true, $alias = '')
	{
		if ($is) {
			return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.status' => $this->modelClass::STATUS_ACTIVE]);
		} else {
			return $this->andWhere(['!=', ($alias ?: $this->modelClass::tableName()) . '.status', $this->modelClass::STATUS_ACTIVE]);
		}
	}

	/**
	 * @param $value
	 * @param string $alias
	 * @return $this
	 */
	public function status($value, $alias = '')
	{
		return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.status' => $value]);
	}

    /**
     * @param bool $is
     * @param null $alias
     * @return $this
     */
    public function promoted(bool $is = true, $alias = null)
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.is_promoted' => $is]);
    }

    /**
     * @param $site_id
     * @param null $alias
     * @return $this
     */
    public function bySite($site_id, $alias = null)
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.site_id' => $site_id]);
    }

    /**
     * @param $bonus_type_id
     * @param null $alias
     * @return $this
     */
    public function byBonusType($bonus_type_id, $alias = null)
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.bonus_type_id' => $bonus_type_id]);
    }
}