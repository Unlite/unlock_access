<?php


namespace common\models\queries;

use common\models\Client;
use yii\db\ActiveQuery;

/**
 * Class ClientQuery
 * @package common\models\queries
 * @see \common\models\Client
 *
 * @method Client[] all($db = null)
 * @method Client one($db = null)
 */
class ClientQuery extends ActiveQuery
{
    /**
     * @param int $user_id
     * @param string|null $alias
     * @return $this
     */
    public function byUser($user_id, $alias = null)
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.user_id' => $user_id]);
    }

    /**
     * @param string $token
     * @param string|null $alias
     * @return $this
     */
    public function byToken(string $token, $alias = null)
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.token' => $token]);
    }
}