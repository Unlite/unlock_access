<?php

namespace common\models\queries;

use common\models\Currency;
use yii\db\ActiveQuery;

/**
 * Class CurrencyQuery
 * @package common\models\queries
 * @see \common\models\Currency
 *
 * @method Currency[] all($db = null)
 * @method Currency one($db = null)
 */
class CurrencyQuery extends ActiveQuery
{
	/**
	 * @param bool $is
	 * @param null $alias
	 * @return $this
	 */
	public function system(bool $is = true, $alias = null)
	{
		return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.is_system' => $is]);
	}
}