<?php

namespace common\models\queries;

use yii\db\ActiveQuery;

/**
 * Class CurrencyRelationsQuery
 * @package common\models\queries
 * @see \common\models\CurrencyRelations
 *
 * @method \common\models\CurrencyRelations[] all($db = null)
 * @method \common\models\CurrencyRelations one($db = null)
 */
class CurrencyRelationsQuery extends ActiveQuery
{

}