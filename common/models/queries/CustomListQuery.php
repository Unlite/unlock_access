<?php

namespace common\models\queries;

use common\models\CustomList;
use yii\db\ActiveQuery;

/**
 * Class CustomListQuery
 * @package common\models\queries
 * @see \common\models\CustomList
 *
 * @method CustomList[] all($db = null)
 * @method CustomList one($db = null)
 */
class CustomListQuery extends ActiveQuery
{
	/**
	 * @param bool $is
	 * @param string $alias
	 * @return $this
	 */
	public function active(bool $is = true, $alias = '')
	{
		if ($is) {
			return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.status' => $this->modelClass::STATUS_ACTIVE]);
		} else {
			return $this->andWhere(['!=', ($alias ?: $this->modelClass::tableName()) . '.status', $this->modelClass::STATUS_ACTIVE]);
		}
	}

	/**
	 * @param $value
	 * @param string $alias
	 * @return $this
	 */
	public function status($value, $alias = '')
	{
		return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.status' => $value]);
	}

    /**
     * @param bool $is
     * @param null $alias
     * @return $this
     */
    public function promoted(bool $is = true, $alias = null)
    {
        return $this->andWhere(['type' => CustomList::TYPE_PROMOTED]);
    }
}