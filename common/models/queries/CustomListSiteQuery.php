<?php

namespace common\models\queries;

use yii\db\ActiveQuery;

/**
 * Class CustomListSiteQuery
 * @package common\models\queries
 * @see \common\models\CustomListSite
 *
 * @method \common\models\CustomListSite[] all($db = null)
 * @method \common\models\CustomListSite one($db = null)
 */
class CustomListSiteQuery extends ActiveQuery
{
    /**
     * @param $site_id
     * @param string $alias
     * @return $this
     */
    public function bySite($site_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.site_id' => $site_id]);
    }

    /**
     * @param $custom_list_id
     * @param string $alias
     * @return $this
     */
    public function byCustomList($custom_list_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.custom_list_id' => $custom_list_id]);
    }
}