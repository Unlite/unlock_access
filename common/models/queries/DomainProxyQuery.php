<?php

namespace common\models\queries;

use common\models\DomainProxy;
use yii\db\ActiveQuery;

/**
 * Class DomainProxyQuery
 * @package common\models\queries
 * @see \common\models\DomainProxy
 *
 * @method DomainProxy[] all($db = null)
 * @method DomainProxy one($db = null)
 */
class DomainProxyQuery extends ActiveQuery
{
    /**
     * @param $domain_id
     * @param string $alias
     * @return $this
     */
    public function byDomain($domain_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.domain_id' => $domain_id]);
    }

    /**
     * @param $proxy_id
     * @param string $alias
     * @return $this
     */
    public function byProxy($proxy_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.proxy_id' => $proxy_id]);
    }
}