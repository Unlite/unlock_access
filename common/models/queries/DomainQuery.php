<?php

namespace common\models\queries;

use common\models\Domain;
use yii\db\ActiveQuery;

/**
 * Class DomainQuery
 * @package common\models\queries
 * @see \common\models\Domain
 *
 * @method Domain[] all($db = null)
 * @method Domain one($db = null)
 */
class DomainQuery extends ActiveQuery
{
    /**
     * @param bool $is
     * @param string $alias
     * @return $this
     */
    public function active(bool $is = true, $alias = '')
    {
        if ($is) return $this->status(Domain::STATUS_ACTIVE, $alias);

        return $this->andWhere(['!=', ($alias ?: $this->modelClass::tableName()) . '.status', Domain::STATUS_ACTIVE]);
    }

    /**
     * @param int $site_id
     * @param string $alias
     * @return $this
     */
    public function bySite($site_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.site_id' => $site_id]);
    }

    /**
     * @param string|array $value
     * @param string $alias
     * @return $this
     */
    public function status($value, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.status' => $value]);
    }

    /**
     * @param string|array $value
     * @param string $alias
     * @return $this
     */
    public function type($value, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.type' => $value]);
    }

    /**
     * @param string $alias
     * @return $this
     */
    public function typeMain($alias = '')
    {
        return $this->type(Domain::TYPE_MAIN, $alias);
    }

    /**
     * @param string $alias
     * @return $this
     */
    public function typeMirror($alias = '')
    {
        return $this->type(Domain::TYPE_MIRROR, $alias);
    }
}