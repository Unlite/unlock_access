<?php

namespace common\models\queries;

use common\models\EmailSubscriptions;
use yii\db\ActiveQuery;

/**
 * Class EmailSubscriptionsQuery
 * @package common\models\queries
 * @see \common\models\EmailSubscriptions
 *
 * @method EmailSubscriptions[] all($db = null)
 * @method EmailSubscriptions one($db = null)
 */
class EmailSubscriptionsQuery extends ActiveQuery
{
    /**
     * @param int $user_id
     * @param string $alias
     * @return $this
     */
    public function byUser($user_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.user_id' => $user_id]);
    }

    /**
     * @param string $email
     * @param string $token
     * @param string $alias
     * @return $this
     */
    public function byEmailAndToken($email, $token, $alias = '')
    {
        return $this->andWhere([
            ($alias ?: $this->modelClass::tableName()) . '.email' => $email,
            ($alias ?: $this->modelClass::tableName()) . '.token' => $token
        ]);
    }

    /**
     * @param bool $is
     * @param string $alias
     * @return $this
     */
    public function subPromo(bool $is = true, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.sub_promo' => $is]);
    }

    /**
     * @param bool $is
     * @param string $alias
     * @return $this
     */
    public function subSystem(bool $is = true, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.sub_system' => $is]);
    }
}