<?php

namespace common\models\queries;

use common\models\GameCategoryGame;
use yii\db\ActiveQuery;

/**
 * Class GameCategoryGameQuery
 * @package common\models\queries
 * @see \common\models\GameCategoryGame
 *
 * @method GameCategoryGame[] all($db = null)
 * @method GameCategoryGame one($db = null)
 */
class GameCategoryGameQuery extends ActiveQuery
{
    /**
     * @param $game_id
     * @param string $alias
     * @return $this
     */
    public function byGame($game_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.game_id' => $game_id]);
    }

    /**
     * @param $game_category_id
     * @param string $alias
     * @return $this
     */
    public function byGameCategory($game_category_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.game_category' => $game_category_id]);
    }
}