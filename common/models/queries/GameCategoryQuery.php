<?php

namespace common\models\queries;

use yii\db\ActiveQuery;
use common\models\GameCategory;
/**
 * Class GameCategoryQuery
 * @package common\models\queries
 * @see \common\models\GameCategory
 */
class GameCategoryQuery extends ActiveQuery
{
	/**
	 * @param bool $is
	 * @param string $alias
	 * @return $this
	 */
	public function active(bool $is = true, $alias = '')
	{
		if ($is) {
			return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.status' => $this->modelClass::STATUS_ACTIVE]);
		} else {
			return $this->andWhere(['!=', ($alias ?: $this->modelClass::tableName()) . '.status', $this->modelClass::STATUS_ACTIVE]);
		}
	}

	/**
	 * @param $value
	 * @param string $alias
	 * @return $this
	 */
	public function status($value, $alias = '')
	{
		return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.status' => $value]);
	}

	/**
	 * @return ActiveQuery
	 */
	public function sidebar()
	{
		return $this->andWhere(['type'=>GameCategory::TYPE_SIDEBAR]);
	}
	/**
	 * @return ActiveQuery
	 */
	public function slider()
	{
		return $this->andWhere(['type'=>GameCategory::TYPE_SLIDER]);
	}
}