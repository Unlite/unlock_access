<?php

namespace common\models\queries;

use common\models\GameCategorySite;
use yii\db\ActiveQuery;

/**
 * Class GameCategorySiteQuery
 * @package common\models\queries
 * @see \common\models\GameCategorySite
 *
 * @method GameCategorySite[] all($db = null)
 * @method GameCategorySite one($db = null)
 */
class GameCategorySiteQuery extends ActiveQuery
{
    /**
     * @param $site_id
     * @param string $alias
     * @return $this
     */
    public function bySite($site_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.site_id' => $site_id]);
    }

    /**
     * @param $game_category
     * @param string $alias
     * @return $this
     */
    public function byGameCategory($game_category, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.game_category_id' => $game_category]);
    }

    /**
     * @param bool $is
     * @param string $alias
     * @return $this
     */
    public function autoCreated(bool $is = true, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.is_auto_created' => $is]);
    }
}