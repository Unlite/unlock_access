<?php

namespace common\models\queries;

use yii\db\ActiveQuery;

/**
 * Class GameProviderQuery
 * @package common\models\queries
 * @see \common\models\GameProvider
 *
 * @method \common\models\GameProvider[] all($db = null)
 * @method \common\models\GameProvider one($db = null)
 */
class GameProviderQuery extends ActiveQuery
{
	/**
	 * @param bool $is
	 * @param string $alias
	 * @return $this
	 */
	public function active(bool $is = true, $alias = '')
	{
		if ($is) {
			return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.status' => $this->modelClass::STATUS_ACTIVE]);
		} else {
			return $this->andWhere(['!=', ($alias ?: $this->modelClass::tableName()) . '.status', $this->modelClass::STATUS_ACTIVE]);
		}
	}

	/**
	 * @param $value
	 * @param string $alias
	 * @return $this
	 */
	public function status($value, $alias = '')
	{
		return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.status' => $value]);
	}
}