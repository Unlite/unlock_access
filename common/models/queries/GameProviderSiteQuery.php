<?php

namespace common\models\queries;

use yii\db\ActiveQuery;

/**
 * Class GameProviderSiteQuery
 * @package common\models\queries
 * @see \common\models\GameProviderSite
 *
 * @method \common\models\GameProviderSite[] all($db = null)
 * @method \common\models\GameProviderSite one($db = null)
 */
class GameProviderSiteQuery extends ActiveQuery
{
    /**
     * @param bool $is
     * @param string $alias
     * @return $this
     */
    public function autoCreated($is = true, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.is_auto_created' => $is]);
    }

    /**
     * @param $site_id
     * @param string $alias
     * @return $this
     */
    public function bySite($site_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.site_id' => $site_id]);
    }

    /**
     * @param $game_provider_id
     * @param string $alias
     * @return $this
     */
    public function byGameProvider($game_provider_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.game_provider_id' => $game_provider_id]);
    }
}