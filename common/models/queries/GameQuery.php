<?php

namespace common\models\queries;

use yii\db\ActiveQuery;

/**
 * Class GameQuery
 * @package common\models\queries
 * @see \common\models\Game
 *
 * @method \common\models\Game[] all($db = null)
 * @method \common\models\Game one($db = null)
 */
class GameQuery extends ActiveQuery
{
	/**
	 * @param bool $is
	 * @param string $alias
	 * @return $this
	 */
	public function active(bool $is = true, $alias = '')
	{
		if ($is) {
			return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.status' => $this->modelClass::STATUS_ACTIVE]);
		} else {
			return $this->andWhere(['!=', ($alias ?: $this->modelClass::tableName()) . '.status', $this->modelClass::STATUS_ACTIVE]);
		}
	}

	/**
	 * @param $value
	 * @param string $alias
	 * @return $this
	 */
	public function status($value, $alias = '')
	{
		return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.status' => $value]);
	}

	/**
	 * @param $value
	 * @param string $alias
	 * @return $this
	 */
	public function slug($value, $alias = '')
	{
		return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.slug' => $value]);
	}

    /**
     * @param $game_provider_id
     * @param string $alias
     * @return $this
     */
    public function byGameProvider($game_provider_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.game_provider_id' => $game_provider_id]);
    }

}