<?php

namespace common\models\queries;

use yii\db\ActiveQuery;

/**
 * Class GeoQuery
 * @package common\models\queries
 * @see \common\models\Geo
 */
class GeoQuery extends ActiveQuery
{

	public function orderByName($sort = SORT_ASC)
	{
		return $this->orderBy(['name' => $sort]);
	}

	public function orderByCode($sort = SORT_ASC)
	{
		return $this->orderBy(['code' => $sort]);
	}
}