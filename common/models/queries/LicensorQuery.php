<?php

namespace common\models\queries;

use yii\db\ActiveQuery;

/**
 * Class LicensorQuery
 * @package common\models\queries
 * @see \common\models\Licensor
 *
 * @method \common\models\Licensor[] all($db = null)
 * @method \common\models\Licensor one($db = null)
 */
class LicensorQuery extends ActiveQuery
{
	/**
	 * @param bool $is
	 * @param string $alias
	 * @return $this
	 */
	public function active(bool $is = true, $alias = '')
	{
		if ($is) {
			return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.status' => $this->modelClass::STATUS_ACTIVE]);
		} else {
			return $this->andWhere(['!=', ($alias ?: $this->modelClass::tableName()) . '.status', $this->modelClass::STATUS_ACTIVE]);
		}
	}

	/**
	 * @param $value
	 * @param string $alias
	 * @return $this
	 */
	public function status($value, $alias = '')
	{
		return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.status' => $value]);
	}

    /**
     * @param $geo_code
     * @param string $alias
     * @return $this
     */
    public function byGeo($geo_code, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.geo_code' => $geo_code]);
    }
}