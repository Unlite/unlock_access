<?php

namespace common\models\queries;

use yii\db\ActiveQuery;

/**
 * Class LinkQuery
 * @package common\models\queries
 * @see \common\models\Link
 *
 * @method \common\models\Link[] all($db = null)
 * @method \common\models\Link one($db = null)
 */
class LinkQuery extends ActiveQuery
{
    /**
     * @param $domain_id
     * @param string $alias
     * @return $this
     */
    public function byDomain($domain_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.domain_id' => $domain_id]);
    }
}