<?php

namespace common\models\queries;

use yii\db\ActiveQuery;

/**
 * Class LinkWildcardQuery
 * @package common\models\queries
 * @see \common\models\LinkWildcard
 *
 * @method \common\models\LinkWildcard[] all($db = null)
 * @method \common\models\LinkWildcard one($db = null)
 */
class LinkWildcardQuery extends ActiveQuery
{
    /**
     * @param $link_id
     * @param string $alias
     * @return $this
     */
    public function byLink($link_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.link_id' => $link_id]);
    }
}