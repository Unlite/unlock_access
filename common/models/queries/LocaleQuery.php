<?php

namespace common\models\queries;

use common\models\Locale;
use common\models\queries\traits\StatusColumnQueryTrait;
use yii\db\ActiveQuery;

/**
 * Class LocaleQuery
 * @package common\models\queries
 * @see \common\models\Locale
 *
 * @method \common\models\Locale[] all($db = null)
 * @method \common\models\Locale one($db = null)
 */
class LocaleQuery extends ActiveQuery
{
	use StatusColumnQueryTrait;

	public function orderByName($sort = SORT_ASC)
	{
		return $this->orderBy(['name' => $sort]);
	}

	public function orderByCode($sort = SORT_ASC)
	{
		return $this->orderBy(['code' => $sort]);
	}

	public function enabled()
	{
		return $this->andStatus(Locale::STATUS_ENABLED);
	}
}