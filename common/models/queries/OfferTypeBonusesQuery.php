<?php

namespace common\models\queries;

use yii\db\ActiveQuery;

/**
 * Class OfferTypeBonusesQuery
 * @package common\models\queries
 * @see \common\models\OfferTypeBonuses
 *
 * @method \common\models\OfferTypeBonuses[] all($db = null)
 * @method \common\models\OfferTypeBonuses one($db = null)
 */
class OfferTypeBonusesQuery extends ActiveQuery
{
    /**
     * @param $offer_type_id
     * @param string $alias
     * @return $this
     */
    public function byOfferType($offer_type_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.offer_type_id' => $offer_type_id]);
    }

    /**
     * @param $bonus_id
     * @param string $alias
     * @return $this
     */
    public function byBonus($bonus_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.bonuses_id' => $bonus_id]);
    }
}