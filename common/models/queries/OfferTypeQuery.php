<?php

namespace common\models\queries;

use yii\db\ActiveQuery;

/**
 * Class OfferTypeQuery
 * @package common\models\queries
 * @see \common\models\OfferType
 *
 * @method \common\models\OfferType[] all($db = null)
 * @method \common\models\OfferType one($db = null)
 */
class OfferTypeQuery extends ActiveQuery
{

}