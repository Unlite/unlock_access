<?php

namespace common\models\queries;

use yii\db\ActiveQuery;

/**
 * Class PaymentSystemCurrencyQuery
 * @package common\models\queries
 * @see \common\models\PaymentSystemCurrency
 *
 * @method \common\models\PaymentSystemCurrency[] all($db = null)
 * @method \common\models\PaymentSystemCurrency one($db = null)
 */
class PaymentSystemCurrencyQuery extends ActiveQuery
{
    /**
     * @param $payment_system_id
     * @param string $alias
     * @return $this
     */
    public function byPaymentSystem($payment_system_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.payment_system_id' => $payment_system_id]);
    }

    /**
     * @param $currency_code
     * @param string $alias
     * @return $this
     */
    public function byCurrency($currency_code, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.currency_code' => $currency_code]);
    }
}