<?php

namespace common\models\queries;

use common\models\PaymentSystem;
use yii\db\ActiveQuery;

/**
 * Class PaymentSystemQuery
 * @package common\models\queries
 * @see \common\models\PaymentSystem
 *
 * @method \common\models\PaymentSystem[] all($db = null)
 * @method \common\models\PaymentSystem one($db = null)
 */
class PaymentSystemQuery extends ActiveQuery
{
	/**
	 * @param $value
	 * @param string $alias
	 * @return $this
	 */
	public function status($value, $alias = '')
	{
		return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.status' => $value]);
	}

	/**
	 * @param string $alias
	 * @return $this
	 */
	public function enabled($alias = '')
	{
		return $this->status(PaymentSystem::STATUS_ENABLED, $alias);
	}
}