<?php

namespace common\models\queries;

use yii\db\ActiveQuery;

/**
 * Class ProfileQuery
 * @package common\models\queries
 * @see \common\models\Profile
 *
 * @method \common\models\Profile[] all($db = null)
 * @method \common\models\Profile one($db = null)
 */
class ProfileQuery extends ActiveQuery
{
    /**
     * @param $user_id
     * @param string $alias
     * @return $this
     */
    public function byUser($user_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.user_id' => $user_id]);
    }
}