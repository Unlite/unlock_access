<?php

namespace common\models\queries;

use common\models\Proxy;
use yii\db\ActiveQuery;

/**
 * Class ProxyQuery
 * @package common\models\queries
 * @see \common\models\Proxy
 *
 * @method \common\models\Proxy[] all($db = null)
 * @method \common\models\Proxy one($db = null)
 */
class ProxyQuery extends ActiveQuery
{
    /**
     * @param bool $is
     * @param string $alias
     * @return $this
     */
    public function active(bool $is = true, $alias = '')
    {
        if ($is) return $this->status(Proxy::STATUS_ACTIVE, $alias);

        return $this->status(Proxy::STATUS_PAUSED, $alias);
    }

    /**
     * @param $geo_code
     * @param string $alias
     * @return $this
     */
    public function byGeo($geo_code, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.geo_code' => $geo_code]);
    }

    /**
     * @param $host
     * @param string $alias
     * @return $this
     */
    public function host($host, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.host' => $host]);
    }

    /**
     * @param $host
     * @param $port
     * @param string $alias
     * @return $this
     */
    public function fullAddress($host, $port, $alias = '')
    {
        return $this->andWhere([
            ($alias ?: $this->modelClass::tableName()) . '.host' => $host,
            ($alias ?: $this->modelClass::tableName()) . '.port' => $port
        ]);
    }

    /**
     * @param $value
     * @param string $alias
     * @return $this
     */
    public function status($value, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.status' => $value]);
    }

    /**
     * @param $value
     * @param string $alias
     * @return $this
     */
    public function type($value, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.type' => $value]);
    }

    /**
     * @param string $alias
     * @return $this
     */
    public function typePublic($alias = '')
    {
        return $this->type(Proxy::TYPE_PUBLIC, $alias);
    }

    /**
     * @param string $alias
     * @return $this
     */
    public function typePrivate($alias = '')
    {
        return $this->type(Proxy::TYPE_PRIVATE, $alias);
    }
}