<?php

namespace common\models\queries;

use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;

/**
 * Class ReviewQuery
 * @package common\models\queries
 * @see \common\models\Review
 *
 * @method \common\models\Review[] all($db = null)
 * @method \common\models\Review one($db = null)
 */
class ReviewQuery extends ActiveQuery
{
    /**
     * @param null $user_id
     * @param string $alias
     * @return $this
     */
    public function byUser($user_id = null, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.user_id' => $user_id]);
    }

    /**
     * @param $site_id
     * @param string $alias
     * @return $this
     */
    public function bySite($site_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.site_id' => $site_id]);
    }

    /**
     * @param $author
     * @param string $alias
     * @return $this
     */
    public function author($author, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.author' => $author]);
    }

    /**
     * @param $value
     * @param string $condition
     * @param string $alias
     * @return $this
     * @throws InvalidConfigException
     */
    public function rating($value, $condition = '=', $alias = '')
    {
        $conditions = ['=', '>=', '>', '<=', '<'];
        if (!in_array($condition, $conditions)) {
            throw new InvalidConfigException("Not available condition '{$condition}'");
        }
        return $this->andWhere([$condition, ($alias ?: $this->modelClass::tableName()) . '.rating', $value]);
    }

    /**
     * @param $value
     * @param bool $orEqual
     * @param string $alias
     * @return $this
     */
    public function ratingMoreThan($value, bool $orEqual = true, $alias = '')
    {
        return $this->rating($value, $orEqual ? '>=' : '>', $alias);
    }

    /**
     * @param $value
     * @param bool $orEqual
     * @param string $alias
     * @return $this
     */
    public function ratingLessThan($value, bool $orEqual = false, $alias = '')
    {
        return $this->rating($value, $orEqual ? '<=' : '<', $alias);
    }
}