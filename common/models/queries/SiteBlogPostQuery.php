<?php

namespace common\models\queries;

/**
 * This is the ActiveQuery class for [[\common\models\SiteBlogPost]].
 *
 * @see \common\models\SiteBlogPost
 */
class SiteBlogPostQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\SiteBlogPost[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\SiteBlogPost|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
