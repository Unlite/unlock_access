<?php

namespace common\models\queries;

use yii\db\ActiveQuery;

/**
 * Class SiteCurrencyQuery
 * @package common\models\queries
 * @see \common\models\SiteCurrency
 *
 * @method \common\models\SiteCurrency[] all($db = null)
 * @method \common\models\SiteCurrency one($db = null)
 */
class SiteCurrencyQuery extends ActiveQuery
{
    /**
     * @param $site_id
     * @param string $alias
     * @return $this
     */
    public function bySite($site_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.site_id' => $site_id]);
    }

    /**
     * @param $currency_code
     * @param string $alias
     * @return $this
     */
    public function byCurrency($currency_code, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.currency_code' => $currency_code]);
    }
}