<?php

namespace common\models\queries;

use yii\db\ActiveQuery;

/**
 * Class SiteGameQuery
 * @package common\models\queries
 * @see \common\models\SiteGame
 *
 * @method \common\models\SiteGame[] all($db = null)
 * @method \common\models\SiteGame one($db = null)
 */
class SiteGameQuery extends ActiveQuery
{
    /**
     * @param $site_id
     * @param string $alias
     * @return $this
     */
    public function bySite($site_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.site_id' => $site_id]);
    }

    /**
     * @param $game_id
     * @param string $alias
     * @return $this
     */
    public function byGame($game_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.game_id' => $game_id]);
    }


	/**
	 * @param bool $is
	 * @param string $alias
	 * @return $this
	 */
	public function promoted(bool $is = true, $alias = '')
	{
		return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.is_promoted' => $is]);
	}
}