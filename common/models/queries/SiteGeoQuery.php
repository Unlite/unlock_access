<?php

namespace common\models\queries;

use yii\db\ActiveQuery;

/**
 * Class SiteGeoQuery
 * @package common\models\queries
 * @see \common\models\SiteGeo
 *
 * @method \common\models\SiteGeo[] all($db = null)
 * @method \common\models\SiteGeo one($db = null)
 */
class SiteGeoQuery extends ActiveQuery
{
    /**
     * @param $site_id
     * @param string $alias
     * @return $this
     */
    public function bySite($site_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.site_id' => $site_id]);
    }

    /**
     * @param $geo_code
     * @param string $alias
     * @return $this
     */
    public function byGeo($geo_code, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.geo_code' => $geo_code]);
    }

    /**
     * @param bool $is
     * @param string $alias
     * @return $this
     */
    public function allowed(bool $is = true, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.is_allowed' => $is]);
    }
}