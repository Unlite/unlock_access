<?php

namespace common\models\queries;

use yii\db\ActiveQuery;

/**
 * Class SiteImageQuery
 * @package common\models\queries
 * @see \common\models\SiteImage
 *
 * @method \common\models\SiteImage[] all($db = null)
 * @method \common\models\SiteImage one($db = null)
 */
class SiteImageQuery extends ActiveQuery
{
    /**
     * @param $site_id
     * @param string $alias
     * @return $this
     */
    public function bySite($site_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.site_id' => $site_id]);
    }

    /**
     * @param $value
     * @param string $alias
     * @return $this
     */
    public function type($value, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.type' => $value]);
    }
}