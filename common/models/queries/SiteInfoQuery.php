<?php

namespace common\models\queries;

use yii\db\ActiveQuery;

/**
 * Class SiteInfoQuery
 * @package common\models\queries
 * @see \common\models\SiteInfo
 *
 * @method \common\models\SiteInfo[] all($db = null)
 * @method \common\models\SiteInfo one($db = null)
 */
class SiteInfoQuery extends ActiveQuery
{
    /**
     * @param $site_id
     * @param string $alias
     * @return $this
     */
    public function bySite($site_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.id' => $site_id]);
    }

    /**
     * @return $this
     */
    public function latest()
    {
        return $this->orderBy(['review_updated_at' => SORT_DESC]);
    }
}