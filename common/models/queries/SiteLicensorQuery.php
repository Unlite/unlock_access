<?php

namespace common\models\queries;

use yii\db\ActiveQuery;

/**
 * Class SiteLicensorQuery
 * @package common\models\queries
 * @see \common\models\SiteLicensor
 *
 * @method \common\models\SiteLicensor[] all($db = null)
 * @method \common\models\SiteLicensor one($db = null)
 */
class SiteLicensorQuery extends ActiveQuery
{
    /**
     * @param $site_id
     * @param string $alias
     * @return $this
     */
    public function bySite($site_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.site_id' => $site_id]);
    }

    /**
     * @param $licensor_id
     * @param string $alias
     * @return $this
     */
    public function byLicensor($licensor_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.licensor_id' => $licensor_id]);
    }
}