<?php

namespace common\models\queries;

use yii\db\ActiveQuery;

/**
 * Class SiteLocaleQuery
 * @package common\models\queries
 * @see \common\models\SiteLocale
 *
 * @method \common\models\SiteLocale[] all($db = null)
 * @method \common\models\SiteLocale one($db = null)
 */
class SiteLocaleQuery extends ActiveQuery
{
    /**
     * @param $site_id
     * @param string $alias
     * @return $this
     */
    public function bySite($site_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.site_id' => $site_id]);
    }

    /**
     * @param $locale_code
     * @param string $alias
     * @return $this
     */
    public function byLocale($locale_code, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.locale_code' => $locale_code]);
    }
}