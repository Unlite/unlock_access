<?php

namespace common\models\queries;

use yii\db\ActiveQuery;

/**
 * Class SitePaymentQuery
 * @package common\models\queries
 * @see \common\models\SitePayment
 *
 * @method \common\models\SitePayment[] all($db = null)
 * @method \common\models\SitePayment one($db = null)
 */
class SitePaymentQuery extends ActiveQuery
{
    /**
     * @param $site_id
     * @param string $alias
     * @return $this
     */
    public function bySite($site_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.site_id' => $site_id]);
    }

    /**
     * @param $payment_id
     * @param string $alias
     * @return $this
     */
    public function byPayment($payment_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.payment_id' => $payment_id]);
    }

    /**
     * @param bool $is
     * @param string $alias
     * @return $this
     */
    public function deposit(bool $is = true, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.is_deposit' => $is]);
    }

    /**
     * @param bool $is
     * @param string $alias
     * @return $this
     */
    public function withdrawal(bool $is = true, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.is_withdrawal' => $is]);
    }
}