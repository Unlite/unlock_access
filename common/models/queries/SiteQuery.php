<?php

namespace common\models\queries;

use common\models\Site;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;

/**
 * Class SiteQuery
 * @package common\models\queries
 * @see \common\models\Site
 *
 * @method Site[] all($db = null)
 * @method Site one($db = null)
 */
class SiteQuery extends ActiveQuery
{

    /**
     * @param bool $is
     * @param string $alias
     * @return $this
     */
    public function active(bool $is = true, $alias = '')
    {
        if ($is) {
            return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.status' => Site::STATUS_ACTIVE]);
        } else {
            return $this->andWhere(['!=', ($alias ?: $this->modelClass::tableName()) . '.status', Site::STATUS_ACTIVE]);
        }
    }

    /**
     * @param $value
     * @param string $alias
     * @return $this
     */
    public function status($value, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.status' => $value]);
    }

    /**
     * @param $value
     * @param string $alias
     * @return $this
     */
    public function slug($value, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.slug' => $value]);
    }

    /**
     * @param $value
     * @param string $alias
     * @return $this
     */
    public function type($value, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.type' => $value]);
    }

    /**
     * @param string $alias
     * @return $this
     */
    public function typeCasino($alias = '')
    {
        return $this->type(Site::TYPE_CASINO, $alias);
    }

    /**
     * @param string $alias
     * @return $this
     */
    public function typeBetting($alias = '')
    {
        return $this->type(Site::TYPE_BETTING, $alias);
    }

    /**
     * @param string $alias
     * @return $this
     */
    public function typePoker($alias = '')
    {
        return $this->type(Site::TYPE_POKER, $alias);
    }

    /**
     * @param string $alias
     * @return $this
     */
    public function typeOther($alias = '')
    {
        return $this->type(Site::TYPE_OTHER, $alias);
    }

    /**
     * @param string $alias
     * @return $this
     */
    public function typeHidden($alias = '')
    {
        return $this->type(Site::TYPE_HIDDEN, $alias);
    }

    /**
     * @param $value
     * @param string $condition
     * @param string $alias
     * @return $this
     * @throws InvalidConfigException
     */
    public function rating($value, $condition = '=', $alias = '')
    {
        $conditions = ['=', '>=', '>', '<=', '<'];
        if (!in_array($condition, $conditions)) {
            throw new InvalidConfigException("Not available condition '{$condition}'");
        }
        return $this->andWhere([$condition, ($alias ?: $this->modelClass::tableName()) . '.rating', $value]);
    }

    /**
     * @param $value
     * @param bool $orEqual
     * @param string $alias
     * @return $this
     */
    public function ratingMoreThan($value, bool $orEqual = true, $alias = '')
    {
        return $this->rating($value, $orEqual ? '>=' : '>', $alias);
    }

    /**
     * @param $value
     * @param bool $orEqual
     * @param string $alias
     * @return $this
     */
    public function ratingLessThan($value, bool $orEqual = false, $alias = '')
    {
        return $this->rating($value, $orEqual ? '<=' : '<', $alias);
    }

    /**
     * @return $this
     */
    public function highestRating()
    {
        return $this->orderBy(['rating'=> SORT_DESC]);
    }
}