<?php

namespace common\models\queries;

use yii\db\ActiveQuery;

/**
 * Class SiteRelationsQuery
 * @package common\models\queries
 * @see \common\models\SiteRelations
 *
 * @method \common\models\SiteRelations[] all($db = null)
 * @method \common\models\SiteRelations one($db = null)
 */
class SiteRelationsQuery extends ActiveQuery
{
    /**
     * @param $site_id
     * @param string $alias
     * @return $this
     */
    public function bySite($site_id, $alias = '')
    {
        return $this->andWhere([
            'or',
            [($alias ?: $this->modelClass::tableName()) . '.site_id_l' => $site_id],
            [($alias ?: $this->modelClass::tableName()) . '.site_id_h' => $site_id]
        ]);
    }

    /**
     * @param $site_id_l
     * @param string $alias
     * @return $this
     */
    public function siteIdL($site_id_l, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.site_id_l' => $site_id_l]);
    }

    /**
     * @param $site_id_h
     * @param string $alias
     * @return $this
     */
    public function siteIdH($site_id_h, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.site_id_h' => $site_id_h]);
    }
}