<?php

namespace common\models\queries;

use yii\db\ActiveQuery;

/**
 * Class SiteSoftQuery
 * @package common\models\queries
 * @see \common\models\SiteSoft
 *
 * @method \common\models\SiteSoft[] all($db = null)
 * @method \common\models\SiteSoft one($db = null)
 */
class SiteSoftQuery extends ActiveQuery
{
    /**
     * @param $site_id
     * @param string $alias
     * @return $this
     */
    public function bySite($site_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.site_id' => $site_id]);
    }

    /**
     * @param $soft_id
     * @param string $alias
     * @return $this
     */
    public function bySoft($soft_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.soft_id' => $soft_id]);
    }
}