<?php

namespace common\models\queries;

use yii\db\ActiveQuery;

/**
 * Class SoftQuery
 * @package common\models\queries
 * @see \common\models\Soft
 *
 * @method \common\models\Soft[] all($db = null)
 * @method \common\models\Soft one($db = null)
 */
class SoftQuery extends ActiveQuery
{

}