<?php

namespace common\models\queries;

/**
 * This is the ActiveQuery class for [[\common\models\Ticket]].
 *
 * @see \common\models\Ticket
 */
class TicketQuery extends \yii\db\ActiveQuery
{
	/**
	 * @param $value
	 * @param string $alias
	 * @return $this
	 */

	public function byUser($value, $alias = '')
	{
		return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.user_id' => $value]);
	}

    /**
     * {@inheritdoc}
     * @return \common\models\Ticket[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\Ticket|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
