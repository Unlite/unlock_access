<?php

namespace common\models\queries;

use common\models\Exchange;
use common\models\Other;
use common\models\Transfer;
use yii\db\ActiveQuery;

/**
 * Class TransferQuery
 * @package common\models\queries
 * @see \common\models\Transfer
 *
 * @method \common\models\Transfer[] all($db = null)
 * @method \common\models\Transfer one($db = null)
 */
class TransferQuery extends ActiveQuery
{
	public $type;
	public $tableName;

	public function prepare($builder)
	{
		if ($this->type !== null) {
			$this->andWhere(["$this->tableName.type" => $this->type]);
		}

		return parent::prepare($builder);
	}

	/**
     * @param integer|array $user_currency_id
     * @param string $alias
     * @return $this
     */
    public function byUserCurrency($user_currency_id, $alias = '')
    {
    	if(is_int($user_currency_id))
			return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.user_currency_id' => $user_currency_id]);
    	elseif(is_array($user_currency_id))
			return $this->andWhere(['in',($alias ?: $this->modelClass::tableName()) . '.user_currency_id', $user_currency_id]);
	}

    /**
     * @param $parent_id
     * @param string $alias
     * @return $this
     */
    public function byParent($parent_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.parent_id' => $parent_id]);
    }

    /**
     * @param $value
     * @param string $alias
     * @return $this
     */
    public function status($value, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.status' => $value]);
    }

    /**
     * @param $value
     * @param string $alias
     * @return $this
     */
    public function type($value, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.type' => $value]);
    }

    /**
     * @param string $alias
     * @return TransferQuery
     */
    public function statusApproved($alias = '')
    {
        return $this->status(Transfer::STATUS_APPROVED, $alias);
    }

    /**
     * @param string $alias
     * @return TransferQuery
     */
    public function statusHold($alias = '')
    {
        return $this->status(Transfer::STATUS_HOLD, $alias);
    }

    /**
     * @param string $alias
     * @return TransferQuery
     */
    public function statusRejected($alias = '')
    {
        return $this->status(Transfer::STATUS_REJECTED, $alias);
    }

    /**
     * @param string $alias
     * @return TransferQuery
     */
    public function typeCashback($alias = '')
    {
        return $this->type(Transfer::TYPE_CASHBACK, $alias);
    }

    /**
     * @param string $alias
     * @return TransferQuery
     */
    public function typeExchange($alias = '')
    {
        return $this->type(Exchange::TYPE, $alias);
    }

    /**
     * @param string $alias
     * @return TransferQuery
     */
    public function typeOther($alias = '')
    {
        return $this->type(Other::TYPE, $alias);
    }

    /**
     * @param string $alias
     * @return TransferQuery
     */
    public function main($alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.parent_id' => null]);
    }
}