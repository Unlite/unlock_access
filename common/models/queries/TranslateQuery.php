<?php

namespace common\models\queries;

/**
 * This is the ActiveQuery class for [[\common\models\Translate]].
 *
 * @see \common\models\Translate
 */
class TranslateQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \common\models\Translate[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

	/**
	 * @param $locale_code
	 * @param string $alias
	 * @return $this
	 */
	public function byLocale($locale_code, $alias = '')
	{
		return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.locale_code' => $locale_code]);
	}

    /**
     * {@inheritdoc}
     * @return \common\models\Translate|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
