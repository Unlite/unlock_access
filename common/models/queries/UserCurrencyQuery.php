<?php

namespace common\models\queries;

use yii\db\ActiveQuery;

/**
 * Class UserCurrencyQuery
 * @package common\models\queries
 * @see \common\models\UserCurrency
 *
 * @method \common\models\UserCurrency[] all($db = null)
 * @method \common\models\UserCurrency one($db = null)
 */
class UserCurrencyQuery extends ActiveQuery
{
    /**
     * @param $user_id
     * @param string $alias
     * @return $this
     */
    public function byUser($user_id, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.user_id' => $user_id]);
    }

    /**
     * @param $currency_code
     * @param string $alias
     * @return $this
     */
    public function byCurrency($currency_code, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.currency_code' => $currency_code]);
    }
}