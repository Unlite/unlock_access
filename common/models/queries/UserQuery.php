<?php

namespace common\models\queries;

use common\models\User;
use yii\db\ActiveQuery;

/**
 * Class UserQuery
 * @package common\models\queries
 * @see \common\models\User
 *
 * @method \common\models\User[] all($db = null)
 * @method \common\models\User one($db = null)
 */
class UserQuery extends ActiveQuery
{
    /**
     * @param bool $is
     * @param string $alias
     * @return $this
     */
    public function active(bool $is = true, $alias = '')
    {
        if ($is) return $this->status(User::STATUS_ACTIVE, $alias);
        else return $this->andWhere(['!=', ($alias ?: $this->modelClass::tableName()) . '.status', User::STATUS_ACTIVE]);
    }

    /**
     * @param bool $is
     * @param string $alias
     * @return $this
     */
    public function deleted(bool $is = true, $alias = '')
    {
        if ($is) return $this->status(User::STATUS_DELETED, $alias);
        else return $this->andWhere(['!=', ($alias ?: $this->modelClass::tableName()) . '.status', User::STATUS_DELETED]);
    }

    public function status($value, $alias = '')
    {
        $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.status' => $value]);
    }

    /**
     * @param $value
     * @param string $alias
     * @return $this
     */
    public function email($value, $alias = '')
    {
        return $this->andWhere([($alias ?: $this->modelClass::tableName()) . '.email' => $value]);
    }
}