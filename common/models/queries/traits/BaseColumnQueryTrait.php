<?php
namespace common\models\queries\traits;

use yii\db\ActiveQuery;

/**
 * Trait BaseEnumQueryTrait
 * @package common\models\queries\traits
 */
trait BaseColumnQueryTrait
{
    public function isInColumn($column, $value)
    {
        return $this->andWhere([$column => $value]);
    }

    /**
     * @param $condition
     * @param array $params
     * @return ActiveQuery
     */
    abstract public function andWhere($condition, $params = []);
}