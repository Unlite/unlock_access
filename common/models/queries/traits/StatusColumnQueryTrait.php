<?php


namespace common\models\queries\traits;


trait StatusColumnQueryTrait
{
    public function andStatus($value, $column = null){
        return $this->andWhere([($column ?: 'status') => $value]);
    }

    public function andOnStatus($value, $alias = null, $column = null){
        return $this->andWhere([($alias ? $alias . '.' : '') . ($column ?: 'status') => $value]);
    }
}