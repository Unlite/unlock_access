<?php

namespace common\models\queries\traits;


trait TypeColumnQueryTrait
{
    public function andType($value, $column = null){
        return $this->andWhere([($column ?: 'type') => $value]);
    }

    public function andOnType($value, $alias = null, $column = null){
        return $this->andWhere([($alias ? $alias . '.' : '') . ($column ?: 'type') => $value]);
    }
}