<?php

namespace common\models\traits;
use common\models\queries\TranslateQuery;
use common\models\Translate;
use yii\base\Model;

/**
 * Class TranslateRelationTrait
 * @property Translate $translate
 * @property Translate[] $translates
 */

trait TranslateRelationTrait
{
	/**
	 * @var Model $this
	 */
    public function getTranslates(){
    	return $this->hasMany(Translate::class,['table_id' => 'id'])->andWhere(['table' => self::getTableSchema()->fullName]);
    }
	/**
	 * @var Model $this
	 * @param string $key
	 * @return TranslateQuery
	 */
    public function getTranslate($key){
    	return $this->hasOne(Translate::class,['table_id' => 'id'])->andWhere(['table' => self::getTableSchema()->fullName,'key'=>$key]);
    }

	/**
	 * @var Model $this
	 * @return string
	 */
	public function translate($key)
	{
		$translate = $this->getTranslate($key)->andWhere(['locale_code' => \Yii::$app->language])->andWhere(['!=','value','null'])->one() ?: $this->getTranslate($key)->andWhere(['locale_code' => \Yii::$app->sourceLanguage])->one();
		$local = isset($this->{$key}) ? $this->{$key} : null;
		$translate = $translate ? $translate->value : $local;
		return $translate ;
    }

}