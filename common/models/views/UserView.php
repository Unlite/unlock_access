<?php

namespace common\models\views;


use common\models\Profile;
use common\models\User;
use yii\base\BaseObject;

/**
 * Class UserView
 * @package common\modules\profile\models
 *
 * @property string $surname
 * @property string $name
 * @property string $fullName
 * @property int $id
 * @property string $email
 * @property string $username
 */

class UserView extends BaseObject
{
    /**
     * @var User
     */
    private $user;
    /**
     * @var Profile
     */
    private $profile;

    /**
     * UserView constructor.
     * @param User $user
     * @param array $config
     */
    public function __construct(User $user, array $config = [])
    {
        $this->user = $user;
        parent::__construct($config);
    }

    /**
     * Gets user Id
     * @return integer
     */
    public function getId(): int
    {
        return $this->user->id;
    }

    /**
     * Gets user Username
     * @return string
     */
    public function getUsername(): string
    {
        return $this->user->username;
    }

    /**
     * Gets user Email
     * @return string
     */
    public function getEmail(): string
    {
        return $this->user->email;
    }

    /**
     * Gets user name
     * @return string
     */
    public function getName(): string
    {
        return $this->getProfile()->name ?: '';
    }

    /**
     * Gets user Surname
     * @return string
     */
    public function getSurname(): string
    {
        return $this->getProfile()->surname ?: '';
    }

    /**
     * Gets user full name
     * @return string
     */
    public function getFullName(): string
    {
        return $this->getSurname() . ' ' . $this->getName();
    }

    /**
     * Gets profile model of user
     * @return Profile
     */
    private function getProfile(): Profile
    {
        if (is_null($this->profile)){
            $this->profile = $this->user->profile;
        }
        return $this->profile;
    }

}