<?php

namespace common\modules\away;

use common\modules\away\interfaces\MacroInterface;
use yii\base\Configurable;
use yii\base\InvalidArgumentException;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecordInterface;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;

/**
 * go module definition class
 */
class Module extends \yii\base\Module
{
    const REDIRECT_TYPE_HTTP = 'http';
    const REDIRECT_TYPE_JS = 'js';
    const REDIRECT_TYPE_META = 'meta';

    /** @var string module ID */
    public static $moduleID = 'away';

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\away\controllers';

    /**
     * Array of macros, where key is replaceable part
     * Custom class example:
     * ```php
     * 'cid' => [
     *      'macroClass' => \common\modules\go\macros\ClientMacro::class,
     *      'singleton' => true, // cache result of macro
     *      [...]
     *  ],
     * ```
     * Simple example:
     * ```php
     * 'cid' => \common\modules\go\macros\ClientMacro::class
     * ```
     * Or function example:
     * ```php
     * 'rand' => function($min = 0, $max = 100){ return rand($min, $max)}
     * ```
     * @var array
     */
    public $macros = [
        'cid' => [
            'macroClass' => 'common\modules\away\macros\ClientMacro',
            'singleton' => true
        ],
        'rand' => 'common\modules\away\macros\RandomMacro',
    ];

    /**
     * Default value, which will replace all unloaded macros ({dsafas321af} => `$macrosDefaultValue`)
     * @var string
     */
    public $macrosDefaultValue = '';

    /**
     * Array of ActiveRecord models, where key is id of model
     * Example:
     * ```php
     *  'site' => [
     *      'modelClass' => 'app\models\Site',
     *      'attribute' => 'url'
     *  ],
     *  'link' => [
     *      'modelClass' => 'app\models\Link',
     *      'attribute' => 'custom_url'
     *  ]
     * ```
     * @var array
     */
    public $models = [];

    /**
     * Blacklist of domains
     * Example which block all custom urls with domains 'example.com' and all subdomains :
     * ```php
     *  [
     *      'example.com',
     *      '*.example.com',
     *  ]
     * ```
     * @see \yii\helpers\StringHelper::matchWildcard()
     * @var array
     */
    public $domainBlacklist = [];

    /**
     * Redirect type. Default: http
     * Available type:
     *  `http`(302),
     *  `meta` (by `<meta http-equiv="Refresh" content="0; url= http://www.example.com/ " />`),
     *  `js` (delayed redirect by `window.assign('http://www.example.com/')`)
     * @var string
     */
    public $redirectType;

    /**
     * Number of seconds before user will be redirected
     * Used if `$redirectType` is `meta` or `js`
     * @var int
     */
    public $redirectDelay = 5;

    private $modelsValid = false;
    private $macrosValid = false;

    /**
     * @return static
     */
    public static function instance()
    {
        $module = \Yii::$app->getModule(static::$moduleID);
        if ($module instanceof static) {
            return $module;
        }
        throw new InvalidArgumentException('Module not found. Add ' . static::class . ' module with ID:' . static::$moduleID);
    }

    public function init()
    {
        parent::init();
        if (!$this->redirectType) {
            $this->redirectType = static::REDIRECT_TYPE_JS;
        }
    }

    /**
     * @param $hash
     * @return string
     */
    public function encryptHash($hash)
    {
        return StringHelper::base64UrlEncode($this->xor($hash));
    }

    /**
     * @param $data
     * @return string
     */
    private function xor ($data)
    {
        $key = static::$moduleID;
        $outText = '';
        for ($i = 0; $i < strlen($data);) {
            for ($j = 0; $j < strlen($key); $j++, $i++) {
                if ($i >= strlen($data)) break;
                $outText .= $data{$i} ^ $key{$j};
            }
        }
        return $outText;
    }

    /**
     * @param $hash
     * @return string
     */
    public function decryptHash($hash)
    {
        return $this->xor(StringHelper::base64UrlDecode($hash));
    }

    /**
     * @param string $key
     * @return array|null model config
     */
    public function findModelByKey(string $key): ?array
    {
        $this->validateModels();
        return $this->models[$key] ?? null;
    }

    /**
     * Validate all models
     * @throws InvalidConfigException
     */
    public function validateModels(): void
    {
        if ($this->modelsValid) return;
        $classes = [];
        foreach ($this->models as $modelConfig) {
            if (!isset($modelConfig['modelClass'], $modelConfig['attribute'])) {
                throw new InvalidConfigException('Object configuration must be an array containing a "class" and "attribute" elements.');
            }
            if (!is_subclass_of($modelConfig['modelClass'], ActiveRecordInterface::class)) {
                throw new InvalidConfigException('Class must implement ActiveRecord');
            }
            if (in_array($modelConfig['modelClass'], $classes)) {
                throw new InvalidConfigException('Model ' . $modelConfig['modelClass'] . ' already used');
            }
            $classes[] = $modelConfig['modelClass'];
        }
        $this->modelsValid = true;
    }

    /**
     * Gets model key(id) by className
     * @param string $className
     * @return null|string
     */
    public function findModelKeyByClassName(string $className): ?string
    {
        $this->validateModels();
        foreach ($this->models as $key => $modelConfig) {
            if ($modelConfig['modelClass'] === $className || is_subclass_of($className, $modelConfig['modelClass'])) return $key;
        }
        return null;
    }

    /**
     * @param string $macroKey
     * @return array|null macro config
     */
    public function findMacroByKey(string $macroKey)
    {
        $this->validateMacros();
        return $this->macros[$macroKey] ?? null;
    }

    public function validateMacros(): void
    {
        if ($this->macrosValid) return;
        if (!ArrayHelper::isAssociative($this->macros)) {
            throw new InvalidConfigException('Macros array must be associative');
        }
        foreach ($this->macros as $macroConfig) {
            if (is_callable($macroConfig)) continue;

            if (is_string($macroConfig)) {
                if (!is_subclass_of($macroConfig, MacroInterface::class)) {
                    throw new InvalidArgumentException('Macro class must implements ' . MacroInterface::class);
                }
                continue;
            }

            if (is_array($macroConfig)) {
                if (!isset($macroConfig['macroClass'])) {
                    throw new InvalidConfigException('macroClass required');
                }
                if (!is_subclass_of($macroConfig['macroClass'], MacroInterface::class)) {
                    throw new InvalidArgumentException('Macro class must implements ' . MacroInterface::class);
                }
                $macroConfigArguments = array_diff_key($macroConfig, array_flip(['macroConfig', 'singleton']));
                if (count($macroConfigArguments) && !is_subclass_of($macroConfig['macroClass'], Configurable::class)) {
                    throw new InvalidArgumentException('Macro class must implements ' . Configurable::class);
                }
                continue;
            }
            throw new InvalidConfigException('Macro must be callable function or class name or configurable config');
        }
        $this->macrosValid = true;
    }
}
