<?php

namespace common\modules\away\controllers;

use common\modules\away\exceptions\InvalidUrlException;
use common\modules\away\exceptions\NotFoundException;
use common\modules\away\link\Link;
use common\modules\away\Module;
use common\modules\away\services\LinkService;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `away` module
 */
class DefaultController extends Controller
{
    /**
     * @var Module
     */
    public $module;
    /**
     * @var LinkService
     */
    private $linkService;

    public function __construct($id, Module $module, LinkService $linkService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->linkService = $linkService;
    }

    /**
     * Renders the index view for the module
     * @param null|string $hash
     * @param null|string $url
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionIndex($hash = null, $url = null)
    {
        if ($hash) {
            try {
                $link = $this->linkService->getModelLinkByHash($hash);

                if ($this->module->redirectType == Module::REDIRECT_TYPE_HTTP) {
                    return $this->redirect($url);
                }

                return $this->render('redirect', [
                    'link' => $link,
                    'redirect_type' => $this->module->redirectType,
                    'redirect_delay' => $this->module->redirectDelay,
                ]);
            } catch (NotFoundException $e) {
                throw new NotFoundHttpException('Url not found');
            }
        } elseif ($url) {
            try {
                $link = $this->linkService->getCustomLink($url);
                if ($this->module->redirectType == Module::REDIRECT_TYPE_HTTP) {
                    return $this->redirect($url);
                }
                return $this->render('redirect', [
                    'link' => $link,
                    'redirect_type' => $this->module->redirectType,
                    'redirect_delay' => $this->module->redirectDelay,
                ]);
            } catch (InvalidUrlException $e) {
                return $this->render('error', [
                    'back_url' => \Yii::$app->request->referrer ?? '/',
                    'error' => $e->getMessage(),
                ]);
            }
        }
        throw new NotFoundHttpException('Page not found');
    }
}
