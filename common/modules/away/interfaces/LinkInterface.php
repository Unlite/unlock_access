<?php

namespace common\modules\away\interfaces;


interface LinkInterface
{
    /**
     * Gets final url
     * @return string
     */
    public function getUrl(): string;

    public function __toString();
}