<?php

namespace common\modules\away\interfaces;


interface MacroInterface
{
    public function process(): string;

    /**
     * Gets description about macro.
     * You can use %id% in returned string, which will be replaced by macro ID in config
     * @return string
     */
    public static function description(): string;
}