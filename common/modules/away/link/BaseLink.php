<?php

namespace common\modules\away\link;

use common\modules\away\interfaces\LinkInterface;

abstract class BaseLink implements LinkInterface
{
    public function __toString()
    {
        return $this->getUrl();
    }
}