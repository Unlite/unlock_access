<?php

namespace common\modules\away\link;

class Link extends BaseLink
{
    private $url;

    public function __construct(string $url)
    {
        $this->url = $url;
    }

    public function getUrl(): string
    {
        return $this->url;
    }
}