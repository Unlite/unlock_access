<?php

namespace common\modules\away\link;

use common\modules\away\interfaces\MacroInterface;
use common\modules\away\Module;
use yii\base\Configurable;
use yii\base\InvalidArgumentException;
use yii\helpers\ArrayHelper;

class MacrosLink extends BaseLink
{
    private $url;
    /**
     * @var Module
     */
    private $module;
    /**
     * @var string
     */
    private $value;
    /**
     * @var array
     */
    private $singletonMacrosValue = [];

    /**
     * MacrosLink constructor.
     * @param string $url
     */
    public function __construct(string $url)
    {
        $this->url = $url;

        $this->module = Module::instance();
    }

    /**
     * @return string final url with replaced macros
     */
    public function getUrl(): string
    {
        if (!$this->value) {
            //Find and replace macros
            $this->value = preg_replace_callback("/\{([^\}]*)\}/", function ($matches) {
                if (!$matches[1]) return '';//If empty macro ({})

                $macroParts = explode(':', $matches[1]);// Split full macro by ':'

                $macroKey = $macroParts[0];//First item is macro key

                if (ArrayHelper::keyExists($macroKey, $this->module->macros)) {
                    $macroArguments = [];

                    if (count($macroParts) > 1) { // Check if there are additional arguments
                        $macroArguments = explode(',', $macroParts[1]);
                    }

                    return $this->callMacro($macroKey, $macroArguments);
                }

                return $this->module->macrosDefaultValue;
            }, $this->url);
        }
        return $this->value;
    }

    /**
     * @param $macroKey string key which existed in Module::macros
     * @param $arguments array of additional arguments to executable method
     * @return string
     */
    protected function callMacro(string $macroKey, array $arguments): string
    {
        $macroConfig = $this->module->findMacroByKey($macroKey);

        if ($macroConfig) {
            if (is_callable($macroConfig)) return call_user_func_array($macroConfig, $arguments);
            if (is_string($macroConfig)) return call_user_func_array([\Yii::createObject($macroConfig), 'process'], $arguments);
            if (is_array($macroConfig)) {
                $isSingleton = $macroConfig['singleton'] ?? false;
                if ($isSingleton && isset($this->singletonMacrosValue[$macroKey])) {
                    return $this->singletonMacrosValue[$macroKey];
                }

                $macroObj = \Yii::createObject($macroConfig['macroClass']);
                $macroConfigArguments = array_diff_key($macroConfig, array_flip(['macroClass', 'singleton']));
                if (count($macroConfigArguments)) {
                    \Yii::configure($macroObj, $macroConfigArguments);
                }
                $value = call_user_func_array([$macroObj, 'process'], $arguments);

                if ($isSingleton) $this->singletonMacrosValue[$macroKey] = $value;
                return $value;
            }
        }

        return $this->module->macrosDefaultValue;
    }
}