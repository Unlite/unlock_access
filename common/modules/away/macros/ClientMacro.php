<?php


namespace common\modules\away\macros;


use common\models\Client;
use common\modules\away\interfaces\MacroInterface;
use yii\base\BaseObject;

class ClientMacro extends BaseObject implements MacroInterface
{
    public static function description(): string
    {
        return "Return User Client token.";
    }

    public function process(): string
    {
        $client = Client::getClientByUser() ?: Client::getClientByCookies();
        if (!$client) {
            $client = Client::newRecord([
                'ip' => \Yii::$app->request->userIP,
                'user_agent' => \Yii::$app->request->userAgent,
            ], true);
        }
        return $client->token;
    }
}