<?php

namespace common\modules\away\macros;

use common\modules\away\interfaces\MacroInterface;
use yii\base\BaseObject;

class RandomMacro extends BaseObject implements MacroInterface
{
    public static function description(): string
    {
        return 'Return random integer in range. Pattern: {%id%:min,max}';
    }

    public function process(int $min = 0, int $max = 100): string
    {
        return rand($min, $max);
    }
}