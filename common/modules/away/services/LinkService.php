<?php

namespace common\modules\away\services;

use common\modules\away\exceptions\BlacklistUrlException;
use common\modules\away\exceptions\InvalidUrlException;
use common\modules\away\exceptions\NotFoundException;
use common\modules\away\interfaces\LinkInterface;
use common\modules\away\link\Link;
use common\modules\away\link\MacrosLink;
use common\modules\away\Module;
use yii\base\InvalidArgumentException;
use yii\db\ActiveRecordInterface;
use yii\helpers\StringHelper;
use yii\validators\UrlValidator;

class LinkService
{
    /**
     * @var Module
     */
    private $module;

    public function __construct()
    {
        $this->module = Module::instance();
    }

    /**
     * @param ActiveRecordInterface $model
     * @return string
     */
    public function generateModelLinkHash(ActiveRecordInterface $model): string
    {
        $keys = $model->getPrimaryKey(true);
        $modelKey = $this->module->findModelKeyByClassName(get_class($model));
        if (!$modelKey) throw new InvalidArgumentException('Add model ' . get_class($model) . ' to module config in your App.');
        array_unshift($keys, $modelKey);
        return $this->module->encryptHash(join('_', $keys));
    }

    /**
     * @param $hash
     * @return LinkInterface
     * @throws NotFoundException
     */
    public function getModelLinkByHash($hash): LinkInterface
    {
        $decryptedHash = $this->module->decryptHash($hash);
        $values = explode('_', $decryptedHash);
        if (count($values) < 2) throw new NotFoundException('Invalid hash. Few arguments');
        $modelKey = array_shift($values);

        $modelConfig = $this->module->findModelByKey($modelKey);
        if ($modelConfig) {
            /** @var ActiveRecordInterface $modelClass */
            list('modelClass' => $modelClass, 'attribute' => $attribute) = $modelConfig;
            $keys = $modelClass::primaryKey();
            if (count($keys) > 1) {
                if (count($keys) === count($values)) {
                    $model = $modelClass::findOne(array_combine($keys, $values));
                }
            } elseif (isset($values[0])) {
                $model = $modelClass::findOne($values[0]);
            }

            if (isset($model, $model->$attribute)) {
                return new MacrosLink($model->$attribute);
            }
        }

        throw new NotFoundException('Pair ' . $modelKey . ' and ' . join(', ', $values) . ' not found.');
    }

    /**
     * @param $url
     * @return string
     */
    public function generateCustomLinkHash(string $url): string
    {
        return $this->module->encryptHash($url);
    }

    /**
     * @param $hash
     * @return LinkInterface
     * @throws BlacklistUrlException
     * @throws InvalidUrlException
     */
    public function getCustomLink($hash): LinkInterface
    {
        $url = $this->module->decryptHash($hash);
        $validator = new UrlValidator();
        if ($validator->validate($url, $error)) {
            $parsed_url = parse_url($url);
            $isBlackList = false;
            foreach ($this->module->domainBlacklist as $blackDomain) {
                if (StringHelper::matchWildcard($blackDomain, $parsed_url['host'])) {
                    $isBlackList = true;
                    break;
                }
            }
            if ($isBlackList) throw new BlacklistUrlException('Host ' . $parsed_url['host'] . ' in blacklist.');
            return new Link($url);
        }

        throw new InvalidUrlException($error);
    }
}