<?php
/**
 * @var \yii\web\View $this
 * @var string $error
 * @var string $back_url
 */
?>
<div class="away-error">
    <p>
        Invalid url: <?= $error ?>
    </p>
    <p>
        You can <?= \yii\helpers\Html::a('go back', $back_url) ?>
    </p>
</div>
