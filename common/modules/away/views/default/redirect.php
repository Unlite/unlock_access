<?php
/**
 * @var \common\modules\away\interfaces\LinkInterface $link
 * @var \yii\web\View $this
 * @var string $redirect_type
 * @var int $redirect_delay
 */
if ($redirect_type === \common\modules\away\Module::REDIRECT_TYPE_META) {
    $this->registerMetaTag([
        'http-equiv' => 'Refresh',
        'content' => $redirect_delay . ';URL=' . $link
    ]);
} elseif ($redirect_type === \common\modules\away\Module::REDIRECT_TYPE_JS) {
    $redirectJS = <<<JS
    setTimeout(function() {
        window.location.replace("{$link}");
    }, {$redirect_delay} * 1000);
JS;
    $this->registerJs($redirectJS);
}
?>
<style>.subscribe {background-color: #fff;}</style>
<div class="site-error">
    <div class="container">
        <div class="error-bg error-bg--redirect">
            <div class="error-wrapper error-wrapper--redirect">
                <h1 class="error-title error-title--redirect">You will be redirected</h1>
                <p class="error-text error-text--redirect">To: <?= $link ?></p>
            </div>
        </div>
    </div>
</div>
