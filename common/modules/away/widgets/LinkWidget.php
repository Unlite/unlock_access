<?php

namespace common\modules\away\widgets;

use common\modules\away\Module;
use common\modules\away\services\LinkService;
use yii\base\Widget;

class LinkWidget extends Widget
{
    /**
     * @var string
     */
    public $url;
    /**
     * @var LinkService
     */
    private $linkService;

    public function __construct(LinkService $linkService, array $config = [])
    {
        parent::__construct($config);
        $this->linkService = $linkService;
    }

    public function run()
    {
        $hash = $this->linkService->generateCustomLinkHash($this->url);
        $path = Module::$moduleID . '/default/index';

        return \Yii::$app->urlManager->createUrl([$path, 'url' => $hash]);
    }
}