<?php

namespace common\modules\away\widgets;

use common\modules\away\interfaces\MacroInterface;
use common\modules\away\Module;
use yii\base\Widget;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;

class MacrosListWidget extends Widget
{
    const TOOLTIP_PLACEMENT_LEFT = 'left';
    const TOOLTIP_PLACEMENT_TOP = 'top';
    const TOOLTIP_PLACEMENT_RIGHT = 'right';
    const TOOLTIP_PLACEMENT_BOTTOM = 'bottom';
    /**
     * @var Module
     */
    private $module;

    public $tooltipPlacement;

    public $tooltipUse = true;

    public $separator = ',';

    public $options = [];

    public function init()
    {
        parent::init();
        $this->module = Module::instance();
        if ($this->tooltipPlacement) $this->tooltipPlacement = static::TOOLTIP_PLACEMENT_LEFT;
    }

    public function run()
    {
        if ($this->tooltipUse)
            $this->view->registerJs('$(function(){$(\'[data-toggle="tooltip"]\').tooltip()})');

        $elements = [];
        foreach (array_keys($this->module->macros) as $macroKey) {
            $macroConfig = $this->module->findMacroByKey($macroKey);
            $description = '';
            $params = [];

            if (is_callable($macroConfig)) {
                $reflection = new \ReflectionFunction($macroConfig);
                $params = $this->parseFunction($reflection);
                $description = 'Anonymous function';
            } elseif (is_string($macroConfig)) {
                /** @var MacroInterface $macroConfig */
                $reflection = new \ReflectionMethod($macroConfig, 'process');
                $params = $this->parseFunction($reflection);
                $description = str_replace('%id%', $macroKey, $macroConfig::description());
            } elseif (is_array($macroConfig)) {
                /** @var MacroInterface $macroClass */
                $macroClass = $macroConfig['macroClass'];
                $reflection = new \ReflectionMethod($macroClass, 'process');
                $params = $this->parseFunction($reflection);
                $description = str_replace('%id%', $macroKey, $macroClass::description());
            }
            $elements[] = $this->renderElement($macroKey, $params, $description);

        }
        return join($this->separator, $elements);
    }

    protected function renderElement(string $key, array $params = [], string $description = '')
    {

        $macroContent = '{' . $key . ($params ? ':' . join(',', $params) : '') . '}';
        return Html::tag('span', $macroContent, ArrayHelper::merge([
            'data-toggle' => 'tooltip',
            'class' => 'label label-default',
            'data-placement' => $this->tooltipPlacement,
            'title' => $description,
        ], $this->options));
    }

    protected function parseFunction(\ReflectionFunctionAbstract $reflector): array
    {
        $params = [];
        foreach ($reflector->getParameters() as $param) {
            if ($param->isVariadic()) {
                $params[] = '[...]';
                continue;
            }
            $output = '';
            if ($param->isDefaultValueAvailable()) $output .= '[';
            $output .= $param->name;
            if ($param->isDefaultValueAvailable()) {
                $output .= '=' . $param->getDefaultValue() . ']';
            }
            $params[] = $output;
        }
        return $params;
    }
}