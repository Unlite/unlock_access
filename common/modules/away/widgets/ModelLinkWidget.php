<?php

namespace common\modules\away\widgets;

use common\modules\away\Module;
use common\modules\away\services\LinkService;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\db\ActiveRecordInterface;

class ModelLinkWidget extends Widget
{
    /**
     * @var ActiveRecordInterface
     */
    public $model;
    /**
     * @var LinkService
     */
    private $linkService;

    public function __construct(LinkService $linkService, array $config = [])
    {
        parent::__construct($config);
        $this->linkService = $linkService;
    }

    public function init()
    {
        parent::init();
        if (!$this->model) throw new InvalidConfigException('Param model must be set');
        $this->linkService = new LinkService();
    }

    public function run()
    {
        $hash = $this->linkService->generateModelLinkHash($this->model);
        $path = Module::$moduleID . '/default/index';

        return \Yii::$app->urlManager->createUrl([$path, 'hash' => $hash]);
    }
}