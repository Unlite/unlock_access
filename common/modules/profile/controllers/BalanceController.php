<?php

namespace common\modules\profile\controllers;


use common\components\balance\ManagerInterface;
use common\models\Currency;
use common\models\Transfer;
use common\models\UserCurrency;
use common\modules\profile\models\forms\ExchangeForm;
use yii\base\Module;
use yii\web\Controller;

class BalanceController extends Controller
{

    public function actionIndex()
    {

    }

    public function actionExchange($currency_code = null)
    {
        if ($currency_code && $currency = Currency::find()->system()->andWhere(['code' => $currency_code])->one()) {
            $currencies = Currency::find()->system(true)->all();
            $userCurrencies = UserCurrency::find()->byUser(\Yii::$app->user->id)->indexBy('currency_code')->all();
            $userCurrency = UserCurrency::find()->byUser(\Yii::$app->user->id)->byCurrency($currency->code)->one();
            if (!$userCurrency) {
                $userCurrency = new UserCurrency();
                $userCurrency->user_id = \Yii::$app->user->id;
                $userCurrency->currency_code = $currency->code;
                $userCurrency->save(false);
            }
            $transfers = Transfer::find()->byUserCurrency($userCurrency->id)->all();
            $form = new ExchangeForm();
            $form->currency_code_from = $currency_code;
            if (\Yii::$app->request->isPost && $form->load(\Yii::$app->request->post())) {
                $form->currency_code_from = $currency_code;
                if ($form->validate()) {
                    /** @var ManagerInterface $balanceManager */
                    $balanceManager = \Yii::$app->get('balanceManager');
                    $balanceManager->exchange($userCurrency, $form->currency_code, $form->amount);
                    $this->refresh();
                }
            }
            return $this->render('exchange', [
                'currency' => $currency,
                'currencies' => $currencies,
                'userCurrencies' => $userCurrencies,
                'userCurrency' => $userCurrency,
                'transfers' => $transfers,
                'form' => $form
            ]);
        }
        return $this->redirect(['/profile/balance/exchange', 'currency_code' => 'usd']);
    }
}