<?php

namespace common\modules\profile\controllers;


use common\components\cashback\models\search\CashbackStatSearch;
use common\models\Client;
use yii\db\Query;
use yii\web\Controller;

class CashbackController extends Controller
{
    public function actionIndex()
    {
        $clients = Client::find()->select(['token'])->byUser(\Yii::$app->user->getId())->column();
        return $this->render('index', [
            'data' => [
                'clients' => $clients
            ]
        ]);
    }
}