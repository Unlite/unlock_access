<?php

namespace common\modules\profile\controllers;

use common\models\User;
use common\models\views\UserView;
use yii\web\Controller;

/**
 * Default controller for the `profile` module
 */
class DashboardController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {

        /** @var User $user */
        $user = \Yii::$app->user->identity;
        return $this->render('index', [
            'user' => new UserView($user)
        ]);
    }
}
