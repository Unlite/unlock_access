<?php

namespace common\modules\profile\models\forms;


use common\models\Currency;
use common\models\CurrencyRelations;
use common\models\queries\CurrencyQuery;
use common\models\UserCurrency;
use yii\base\Model;

class ExchangeForm extends Model
{
    /**
     * @var string
     */
    public $exchange_from;
    /**
     * @var string
     */
    public $exchange_to;
    public $amount;

	/**
	 * Variables for frontend
	 */
	public $commission;
	public $commission_amount;
	public $exchange_from_currency;
	public $exchange_to_currency;
	public $exchange_amount;
	public $exchange_rate;

    public function rules()
    {
        return [
            [['amount','exchange_from','exchange_to'], 'required'],
			[
				['exchange_from'],
                'exist',
                'targetAttribute' => 'code',
                'targetClass' => Currency::class, 'filter' => function (CurrencyQuery $query) {
					return $query->system();
				}
            ],
            [
                ['exchange_to'],
                'exist',
                'targetAttribute' => 'code',
                'targetClass' => Currency::class, 'filter' => function (CurrencyQuery $query) {
					return $query->system();
				}
            ],
            [['amount'], 'number', 'min' => 1],
            [['amount'], 'validateAmount', 'skipOnEmpty' => false, 'skipOnError' => false, 'params' => ['currencyAttributeFrom' => 'exchange_from', 'currencyAttribute' => 'exchange_to']],
			[['exchange_from'], 'compare', 'compareAttribute' => 'exchange_to', 'operator' => '!=', 'message' => 'Please choose different wallets'],
			];
    }

    public function validateAmount($attribute, $params, \yii\validators\InlineValidator $validator)
    {
        $amount = $this->$attribute;
        $currencyCodeFrom = $this->{$params['currencyAttributeFrom']};
        $relation = CurrencyRelations::findByCodes($currencyCodeFrom, $this->{$params['currencyAttribute']})->one();
        $this->commission = $relation->exchange_commission ?: 0;
        $userCurrency = UserCurrency::find()->byUser(\Yii::$app->user->id)->byCurrency($currencyCodeFrom)->one();
        $userAmount = $userCurrency ? $userCurrency->amount : 0;
        if ((float)$amount > (float)$userAmount) {
            $miss = (float)$amount - (float)$userAmount;
            $validator->addError($this, $attribute, "You don't have enough money. You are missing {$miss} {$currencyCodeFrom}");
        }
    }

	public function afterValidate()
	{
		if(!$this->hasErrors()) {

			$this->exchange_from_currency = Currency::findOne($this->exchange_from);
			$this->exchange_to_currency = Currency::findOne($this->exchange_to);
			$this->exchange_rate = CurrencyRelations::findOne([
				'currency_code' => $this->exchange_from_currency->code,
				'currency_code_r' => $this->exchange_to_currency->code])->value;
			$this->commission_amount = ($this->amount * $this->commission) * $this->exchange_rate;
			$this->exchange_amount = ($this->amount - ($this->amount * $this->commission)) * $this->exchange_rate;
		}
		return $this->hasErrors();
	}

}