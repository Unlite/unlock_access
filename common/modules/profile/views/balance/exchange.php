<?php
/**
 * @var $this \yii\web\View
 * @var $currency \common\models\Currency
 * @var $currencies \common\models\Currency[]
 * @var $userCurrency \common\models\UserCurrency
 * @var $userCurrencies \common\models\UserCurrency[]
 * @var $transfers \common\models\Transfer[]
 * @var $form \common\modules\profile\models\forms\ExchangeForm
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

?>
<div class="profile-balance-exchange">
    <h1><?= $currency->name ?></h1>
    <div class="row">
        <div class="col-6">
            <p>Amount: <?= ($userCurrency ? $userCurrency->amount : '0') ?></p>
            <p>
                List:
                <?php foreach ($currencies as $currency) {
                    echo $currency->name . ': ';
                    if (isset($userCurrencies[$currency->code])) {
                        echo $userCurrencies[$currency->code]->amount;
                    } else {
                        echo '0';
                    }
                    echo ' | ';
                } ?>
            </p>
        </div>
        <div class="col-6"><?php $activeForm = ActiveForm::begin([
                'id' => 'login-form',
                'options' => ['class' => 'form-horizontal'],
            ]) ?>
            <?= $activeForm->field($form, 'currency_code_from')->hiddenInput() ?>
            <?= $activeForm->field($form, 'currency_code')->dropDownList(\yii\helpers\ArrayHelper::map($currencies, 'code', 'name')) ?>
            <?= $activeForm->field($form, 'amount')->textInput() ?>

            <div class="form-group">
                <div class="col-lg-offset-1 col-lg-11">
                    <?= Html::submitButton('Exchange', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
            <?php ActiveForm::end() ?></div>
    </div>


    <?php foreach ($transfers as $transfer) { ?>
        <div class="row">
            <div class="col-3"><?= \Yii::$app->formatter->asDatetime($transfer->created_at) ?></div>
            <div class="col-2"><?= $transfer->type ?></div>
            <div class="col-2"><?= $transfer->amount ?></div>
            <div class="col-3"><?= $transfer->data ?></div>
            <div class="col-2"><?= $transfer->status ?></div>
        </div>
    <?php } ?>
</div>
