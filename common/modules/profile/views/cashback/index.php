<?php
/**
 * @var $this \yii\web\View
 * @var $data array
 */

use common\components\cashback\widgets\UserAmountCount;
use common\widgets\Scoreboard;
use yii\helpers\Url;

?>
<div class="top-info">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <span class="top-info-text">Alert information</span>
            </div>
        </div>
    </div>
</div>
<div class="narrow-content">
    <div class="user-balance">
        <div class="row justify-content-md-between">
            <div class="balance-item decoration col-md-4 col-sm-12">
                <h4>Cashback</h4>
                <div class="balance justify-content-between">
                    <div class="balance-num">
                        <div class="balance-num-inner">
                            $
                            <span class="balance-num-line"></span>
                        </div>
                    </div>
                    <?php Scoreboard::begin([
                        'number' => UserAmountCount::widget(['client_tokens' => $data['clients']]),
                        'precision' => 2,
                    ]); ?>
                    <div class="balance-num">
                        <div class="balance-num-inner">
                            {number0}
                            <span class="balance-num-line"></span>
                        </div>
                    </div>
                    <div class="balance-num">
                        <div class="balance-num-inner">
                            {number1}
                            <span class="balance-num-line"></span>
                        </div>
                    </div>
                    <div class="balance-num">
                        <div class="balance-num-inner">
                            {number2}
                            <span class="balance-num-line"></span>
                        </div>
                    </div>
                    <div class="balance-num">
                        <div class="balance-num-inner">
                            {number3}
                            <span class="balance-num-line"></span>
                        </div>
                    </div>
                    <?php Scoreboard::end(); ?>
                </div>
                <p><a href="#">cashback issue</a></p>
            </div>
            <div class="balance-item decoration col-md-4 col-sm-12">
                <h4>Refferal bonuses</h4>
                <div class="balance justify-content-between">
                    <div class="balance-num">
                        <div class="balance-num-inner">
                            $
                            <span class="balance-num-line"></span>
                        </div>
                    </div>
                    <?php Scoreboard::begin([
                        'number' => rand(1, 99999)/100,
                        'precision' => 2,
                    ]); ?>
                    <div class="balance-num">
                        <div class="balance-num-inner">
                            {number0}
                            <span class="balance-num-line"></span>
                        </div>
                    </div>
                    <div class="balance-num">
                        <div class="balance-num-inner">
                            {number1}
                            <span class="balance-num-line"></span>
                        </div>
                    </div>
                    <div class="balance-num">
                        <div class="balance-num-inner">
                            {number2}
                            <span class="balance-num-line"></span>
                        </div>
                    </div>
                    <div class="balance-num">
                        <div class="balance-num-inner">
                            {number3}
                            <span class="balance-num-line"></span>
                        </div>
                    </div>
                    <?php Scoreboard::end(); ?>
                </div>
                <p><a href="#">refferial link</a></p>
            </div>
            <div class="balance-item decoration col-md-4 col-sm-12">
                <h4>balance</h4>
                <div class="balance justify-content-between">
                    <div class="balance-num">
                        <div class="balance-num-inner">
                            $
                            <span class="balance-num-line"></span>
                        </div>
                    </div>
                    <?php Scoreboard::begin([
                        'number' => rand(1, 9999)/10,
                        'precision' => 1,
                    ]); ?>
                    <div class="balance-num">
                        <div class="balance-num-inner">
                            {number0}
                            <span class="balance-num-line"></span>
                        </div>
                    </div>
                    <div class="balance-num">
                        <div class="balance-num-inner">
                            {number1}
                            <span class="balance-num-line"></span>
                        </div>
                    </div>
                    <div class="balance-num">
                        <div class="balance-num-inner">
                            {number2}
                            <span class="balance-num-line"></span>
                        </div>
                    </div>
                    <div class="balance-num">
                        <div class="balance-num-inner">
                            {number3}
                            <span class="balance-num-line"></span>
                        </div>
                    </div>
                    <?php Scoreboard::end(); ?>
                </div>
                <p><a href="#">payment information</a></p>
            </div>
        </div>
    </div>
</div>

<div class="narrow-content">
    <div class="the-best-wrap decoration">
        <h2 class="title-icon"><span>The best cashback in this month: (or instant deals)</span></h2>
        <div class="the-best row justify-content-md-between">
            <div class="the-best-item the-best-pink col-md-4">
                <div class="the-best-content">
                    <div class="the-best-img">
                        <img src="<?=Url::to(['img/casino/the-best-img.png'])?>" alt="">
                    </div>
                    <div class="the-best-text">
                        <div class="the-best-title">Malina casino</div>
                        <div class="the-best-result">15%</div>
                        <div class="the-best-small">cashback in month</div>
                    </div>
                </div>
            </div>
            <div class="the-best-item col-md-4">
                <div class="the-best-content">
                    <div class="the-best-img">
                        <img src="<?=Url::to(['img/casino/the-best-img.png'])?>" alt="">
                    </div>
                    <div class="the-best-text">
                        <div class="the-best-title">Malina casino</div>
                        <div class="the-best-result">25$</div>
                        <div class="the-best-small">giveaway for dep over 100$</div>
                    </div>
                </div>
            </div>
            <div class="the-best-item the-best-blue col-md-4">
                <div class="the-best-content">
                    <div class="the-best-img">
                        <img src="<?=Url::to(['img/casino/the-best-img.png'])?>" alt="">
                    </div>
                    <div class="the-best-text">
                        <div class="the-best-title">Malina casino</div>
                        <div class="the-best-result">20%</div>
                        <div class="the-best-small">cashback in month</div>
                    </div>
                </div>
            </div>
        </div>
        <a href="#" class="button button-inline">Show all</a>
    </div>
</div>

<div class="narrow-content">
    <div class="table-slider-wrap">
        <div class="table-slider table-slider-play">
            <div class="table-slider-header row">
                <div class="table-slider-col col-4"><div class="table-slider-text">name casino</div></div>
                <div class="table-slider-col col-4"><div class="table-slider-text">cashback, %</div></div>
                <div class="table-slider-col col-4"><div class="table-slider-text">Cashback amount</div></div>
            </div>
            <div class="table-slider-item">
                <div class="table-slider-title">
                    <div>
                        <a href="#" class="button toggle-button"></a>
                        <div class="toggle-line row">
                            <div class="table-slider-col col-4"><div class="table-slider-text">Frank casino</div></div>
                            <div class="table-slider-col col-4"><div class="table-slider-text">74%</div></div>
                            <div class="table-slider-col col-4"><div class="table-slider-text">$665</div></div>
                        </div>
                    </div>
                    <a href="#" class="button link-button"><div class="table-slider-text">play</div></a>
                </div>
                <div class="table-slider-content">
                    <div class="table-slider-line row">
                        <div class="table-slider-col col-4"><div class="table-slider-text">01.02.2017 – 02.03.2017</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">15</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">$55</div></div>
                    </div>
                    <div class="table-slider-line row">
                        <div class="table-slider-col col-4"><div class="table-slider-text">01.02.2017 – 02.03.2017</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">15%</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">$55</div></div>
                    </div>
                </div>
            </div>
            <div class="table-slider-item">
                <div class="table-slider-title">
                    <div>
                        <a href="#" class="button toggle-button"></a>
                        <div class="toggle-line row">
                            <div class="table-slider-col col-4"><div class="table-slider-text">Frank casino</div></div>
                            <div class="table-slider-col col-4"><div class="table-slider-text">74%</div></div>
                            <div class="table-slider-col col-4"><div class="table-slider-text">$665</div></div>
                        </div>
                    </div>
                    <a href="#" class="button link-button"><div class="table-slider-text">play</div></a>
                </div>
                <div class="table-slider-content">
                    <div class="table-slider-line row">
                        <div class="table-slider-col col-4"><div class="table-slider-text">01.02.2017 – 02.03.2017</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">15</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">$55</div></div>
                    </div>
                    <div class="table-slider-line row">
                        <div class="table-slider-col col-4"><div class="table-slider-text">01.02.2017 – 02.03.2017</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">15%</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">$55</div></div>
                    </div>
                </div>
            </div>
            <div class="table-slider-item">
                <div class="table-slider-title">
                    <div>
                        <a href="#" class="button toggle-button"></a>
                        <div class="toggle-line row">
                            <div class="table-slider-col col-4"><div class="table-slider-text">Frank casino</div></div>
                            <div class="table-slider-col col-4"><div class="table-slider-text">74%</div></div>
                            <div class="table-slider-col col-4"><div class="table-slider-text">$665</div></div>
                        </div>
                    </div>
                    <a href="#" class="button link-button"><div class="table-slider-text">play</div></a>
                </div>
                <div class="table-slider-content">
                    <div class="table-slider-line row">
                        <div class="table-slider-col col-4"><div class="table-slider-text">01.02.2017 – 02.03.2017</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">15</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">$55</div></div>
                    </div>
                    <div class="table-slider-line row">
                        <div class="table-slider-col col-4"><div class="table-slider-text">01.02.2017 – 02.03.2017</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">15%</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">$55</div></div>
                    </div>
                </div>
            </div>
            <div class="table-slider-item">
                <div class="table-slider-title">
                    <div>
                        <a href="#" class="button toggle-button"></a>
                        <div class="toggle-line row">
                            <div class="table-slider-col col-4"><div class="table-slider-text">Frank casino</div></div>
                            <div class="table-slider-col col-4"><div class="table-slider-text">74%</div></div>
                            <div class="table-slider-col col-4"><div class="table-slider-text">$665</div></div>
                        </div>
                    </div>
                    <a href="#" class="button link-button"><div class="table-slider-text">play</div></a>
                </div>
                <div class="table-slider-content">
                    <div class="table-slider-line row">
                        <div class="table-slider-col col-4"><div class="table-slider-text">01.02.2017 – 02.03.2017</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">15</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">$55</div></div>
                    </div>
                    <div class="table-slider-line row">
                        <div class="table-slider-col col-4"><div class="table-slider-text">01.02.2017 – 02.03.2017</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">15%</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">$55</div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="table-slider-wrap">
        <div class="table-slider">
            <div class="table-slider-header row">
                <div class="table-slider-col col-12"><div class="table-slider-text">Refferial link</div></div>
            </div>
            <div class="table-slider-item">
                <div class="table-slider-title">
                    <div>
                        <a href="#" class="button toggle-button"></a>
                        <div class="toggle-line row">
                            <div class="table-slider-col col-4"><div class="table-slider-text">period time</div></div>
                            <div class="table-slider-col col-4"><div class="table-slider-text">number of refs</div></div>
                            <div class="table-slider-col col-4"><div class="table-slider-text">income money</div></div>
                        </div>
                    </div>
                </div>
                <div class="table-slider-content">
                    <div class="table-slider-line row">
                        <div class="table-slider-col col-4"><div class="table-slider-text">01.02.2017 – 02.03.2017</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">15</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">$55</div></div>
                    </div>
                    <div class="table-slider-line row">
                        <div class="table-slider-col col-4"><div class="table-slider-text">01.02.2017 – 02.03.2017</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">15%</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">$55</div></div>
                    </div>
                </div>
            </div>
            <div class="table-slider-item">
                <div class="table-slider-title">
                    <div>
                        <a href="#" class="button toggle-button"></a>
                        <div class="toggle-line row">
                            <div class="table-slider-col col-4"><div class="table-slider-text">period time</div></div>
                            <div class="table-slider-col col-4"><div class="table-slider-text">number of refs</div></div>
                            <div class="table-slider-col col-4"><div class="table-slider-text">income money</div></div>
                        </div>
                    </div>
                </div>
                <div class="table-slider-content">
                    <div class="table-slider-line row">
                        <div class="table-slider-col col-4"><div class="table-slider-text">01.02.2017 – 02.03.2017</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">15</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">$55</div></div>
                    </div>
                    <div class="table-slider-line row">
                        <div class="table-slider-col col-4"><div class="table-slider-text">01.02.2017 – 02.03.2017</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">15%</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">$55</div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>