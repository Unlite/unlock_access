<?php
/**
 * @var $this \yii\web\View
 * @var $user \common\models\views\UserView
 */

$this->title = $user->username;
?>
<div class="profile-default-index">
    <p>
        Hello <?= $user->username ?>.
    </p>
    <?= \yii\widgets\DetailView::widget([
        'model' => $user,
        'attributes' => [
            'id',
            'username',
            'email',
            'name',
        ],
    ]) ?>
</div>
