<?php

namespace common\services;

use common\models\Client;
use common\models\EmailSubscriptions;
use common\models\forms\SignUpForm;
use common\models\Profile;
use common\models\Ticket;
use common\models\TicketImage;
use common\models\TicketMessage;
use common\models\User;
use common\models\UserCurrency;
use portal\models\forms\TicketForm;
use yii\web\UploadedFile;
use yii2mod\ftp\FtpClient;
use Yii;

class TicketService
{
    public function createTicket(TicketForm $form)
    {
        if ($form->validate()) {
        	if(!$form->ticket){
				$ticket = new Ticket([
					'title' => $form->title,
					'status' => Ticket::STATUS_OPENED,
					'user_id' => \Yii::$app->user->id,
					'user_locale' => \Yii::$app->language,
				]);
				if(!$ticket->save(false))
					return $form->addError('ticket','Could not create Ticket');
				$form->ticket = $ticket;
			}
			$ticket_message = new TicketMessage([
				'ticket_id' => $form->ticket->id,
				'user_id' => \Yii::$app->user->id,
				'status' => TicketMessage::STATUS_READ,
				'message' => $form->message
			]);
			if($ticket_message->save(false)) {
				$form->images = UploadedFile::getInstances($form, 'images');
				foreach ($form->images as $image) {
					$img = new TicketImage([
						'ticket_message_id' => $ticket_message->id
					]);
					/* @var \yii2mod\ftp\FtpClient $ftp */
					$ftp = new FtpClient();
					$ftp->connect(Yii::$app->params['ftpHost']);
					$ftp->login(Yii::$app->params['ftpLogin'], Yii::$app->params['ftpPassword']);
					$ftp->pasv(true);
					if (!$ftp->isDir('user')) {
						$ftp->mkdir('user');
					}
					if (!$ftp->isDir('user/ticket')) {
						$ftp->mkdir('user/ticket');
					}
					$random_name = Yii::$app->security->generateRandomString(16) . '.' . $image->extension;
					if ($ftp->put('user/ticket/' . $random_name, $image->tempName, FTP_BINARY)) {
						$img->src = '//' . Yii::$app->params['cdnHost'] . '/user/ticket/' . $random_name;
						$img->save();
					} else {
						$form->addError('images', 'Could not upload files to ftp');
					}
				}
			} else {
				$form->addError('message','Could not create Message');
			}
			return $form;
		}
        return $form;
    }

}