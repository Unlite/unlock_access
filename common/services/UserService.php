<?php

namespace common\services;

use common\models\Client;
use common\models\EmailSubscriptions;
use common\models\forms\SignUpForm;
use common\models\Profile;
use common\models\User;
use common\models\UserCurrency;

class UserService
{
    public function requestSignup(SignUpForm $form)
    {
        if ($form->validate()) {
            $user = User::requestSignup(
                $form->username,
                $form->email,
                $form->password
            );
            if ($user->save(false)) {
                //Add Profile record
                $profile = new Profile(['user_id' => $user->id]);
                $profile->save(false);
                //Add UserCurrency record
                $userCurrency = new UserCurrency(['user_id' => $user->id,'currency_code'=>'usd','amount'=>0]);
				$userCurrency->save(false);

                //Client
                $isConsole = \Yii::$app instanceof \yii\console\Application;//Check application.
                $client = $isConsole ? null : Client::getClientByCookies();//If console, skip cookies
                if($client && $client->secure && !$client->user_id){
                    $client->user_id = $user->id;
                    $client->save();
                }else{
                    Client::newRecord([
                        'user_id' => $user->id,
                        'ip' => $isConsole ? '' : \Yii::$app->request->userIP,
                        'user_agent' => $isConsole ? '' : \Yii::$app->request->userAgent,
                    ], !$isConsole);
                }

                //EmailSubscriptions
                $emailSubscription = EmailSubscriptions::find()->where(['email' => $user->email])->one();
                if ($emailSubscription) {
                    if (!$emailSubscription->user || !$emailSubscription->user_id) { // If emailSubscription doesn't have a user, we update existed entity with new user_id
                        $emailSubscription->setAttributes([
                            'user_id' => $user->id,
                            'email' => $user->email,
                        ]);
                        $emailSubscription->save(false);
                    } else { // If emailSubscription have a user but user email was changed, we will update entity with actual user email and create a new one EmailSubscription for new user
                        $emailSubscription->setAttributes([
                            'email' => $emailSubscription->user->email,
                        ]);
                        $emailSubscription->save(false);

                        $emailSubscription = new EmailSubscriptions([
                            'email' => $user->email,
                            'user_id' => $user->id,
                            'sub_promo' => true,
                        ]);
                        $emailSubscription->save(false);
                    }
                } else { // Create a new entity
                    $emailSubscription = new EmailSubscriptions([
                        'email' => $user->email,
                        'user_id' => $user->id,
                        'sub_promo' => true,
                    ]);
                    $emailSubscription->save(false);
                }
                return $user;
            }else{
                throw new \RuntimeException('Can\'t save user.');
            }
        }
        return null;
    }

    public function userDelete(User $user)
    {
        $profile = Profile::findOne(['user_id' => $user->id]);
        $clients = Client::findAll(['user_id' => $user->id]);
        $emailSubscription = EmailSubscriptions::findOne(['user_id' => $user->id]);

        $transaction = User::getDb()->beginTransaction();

        try{
            $profile->delete();
            foreach ($clients as $client){
                $client->user_id = null;
                $client->save(false);
            }
            $emailSubscription->user_id = null;
            $emailSubscription->save(false);
            $user->delete();
            $transaction->commit();
            return true;
        }catch (\Exception $e){
            $transaction->rollBack();
            return false;
        }
    }
}