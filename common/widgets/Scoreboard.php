<?php

namespace common\widgets;

use yii\bootstrap\Widget;

/**
 * Scoreboard renders scoreboard.
 *
 * The following example will split `number` and replace `{number%}` parts in content enclosed between the [[begin()]]
 * and [[end()]]:
 *
 * ~~~php
 * Scoreboard::begin([
 *     'number' => 456.321,
 *     'precision' => 2,
 * ]);
 *
 * echo '| {number0} | {number1} | {number2} | {number3} | {number4} | {number5} |';
 *
 * Modal::end();
 * ~~~
 * Result:
 * ~~~
 * | 4 | 5 | 6 | . | 3 | 2 |
 * ~~~
 */
class Scoreboard extends Widget
{
    public $number = 0;
    public $template;
    public $replaceFull;
    public $replaceByParts;
    public $content;
    public $precision;

    public function init()
    {
        parent::init();
        if ($this->replaceFull == null)
            $this->replaceFull = '{number}';
        if ($this->replaceByParts == null)
            $this->replaceByParts = '{number%}';
        if ($this->precision == null)
            $this->precision = 1;
        if ($this->template === null)
            $this->template = '<span title="{full_value}">{value}</span>';

        if ($this->content == null)
            ob_start();
    }

    public function run()
    {
        if ($this->content == null)
            $this->content = ob_get_clean();
        if (!$this->content)
            $this->content = $this->replaceFull;
        $this->parseContent();
        return $this->content;
    }

    private function parseContent()
    {
        $fullValue = str_ireplace(['{full_value}', '{value}'], round($this->number, $this->precision), $this->template);
        $this->content = str_ireplace($this->replaceFull, $fullValue, $this->content);

        //Search parts {amount0} {amount1} ...
        $hasParts = false;
        $countParts = 0;
        while (stripos($this->content, str_ireplace('%', $countParts, $this->replaceByParts)) !== false) {
            $hasParts = true;
            $countParts++;
        }
        if ($hasParts && $countParts >= 1) {
            $max = (int)join(array_fill(0, $countParts, '9'));
            $precision = ($this->precision >= $countParts - 2) ? $countParts - 2 : $this->precision;
            $amount = (string)static::formatNumber($this->number, $max, $precision);
            $zero = join(array_fill(0, strlen((string)$max) - strlen($amount), '0'));
            $amount = $zero . $amount;

            for ($i = 0; $i < $countParts; $i++) {
                $partValue = str_ireplace(['{full_value}', '{value}'], [$this->number, substr($amount, $i, 1)], $this->template);
                $this->content = str_ireplace(str_ireplace('%', $i, $this->replaceByParts), $partValue, $this->content);
            }
        }
    }

    protected static function formatNumber(float $amount, int $max, int $precision = 1)
    {
        if ($precision < 0) $precision = 0;
        if ($max < $amount) return $max;

        if ($precision == 0) return floor($amount);

        if (floor($max/10) / pow(10, $precision) > $amount) return round($amount, $precision);
        else return static::formatNumber($amount, $max, --$precision);
    }
}