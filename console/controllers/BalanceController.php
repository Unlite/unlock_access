<?php

namespace console\controllers;


use common\components\balance\ManagerInterface;
use common\models\Currency;
use yii\console\Controller;
use yii\console\ExitCode;

class BalanceController extends Controller
{
    use SelectTrait, UsersTrait;

    public function actionIncrease($username = null, $currency_code = null, $amount = null)
    {
        if ($username)
            $user = $this->findUser($username, 'username');
        else
            $user = $this->findUser();
        if ($username)
            $currency = $this->findCurrency($currency_code);
        else
            $currency = $this->findCurrency();
        if (!$user || !$currency) {
            $this->stderr(($user ? 'Currency' : 'User') . ' not found.' . PHP_EOL);
            return ExitCode::UNSPECIFIED_ERROR;
        }
        $amount = $amount ?? $this->prompt('Enter amount:', ['required' => true, 'validator' => function ($input, &$error) {
                if (!is_numeric($input)) {
                    $error = 'Amount type must be numeric';
                    return false;
                }
                return true;
            }]);
        $comment = $this->prompt('Comment:') ?: '';

        /** @var ManagerInterface $balanceManager */
        $balanceManager = \Yii::$app->get('balanceManager');
        $transfer_id = $balanceManager->increase(['user_id' => $user->id, 'currency_code' => $currency->code], $amount, ['comment' => $comment]);
        $this->stdout('Transfer ID:' . $transfer_id . PHP_EOL);

        return ExitCode::OK;
    }

    private function findCurrency($searchValue = null)
    {
        if (!$searchValue) $searchValue = $this->prompt('Code:', ['required' => true]);

        return Currency::findOne(['code' => $searchValue]);
    }
}