<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 29.11.2017
 * Time: 17:21
 */

namespace console\controllers;


use common\components\cashback\base\BaseCashback;
use common\components\cashback\models\Cashback;
use common\components\cashback\services\MonthSummaryService;
use common\components\cashback\services\MonthTransferService;
use yii\console\Controller;
use yii\console\Exception;

class CashbackController extends Controller
{

    /**
     * Get stats by date. Arguments: id day
     * @param $id
     * @param string $day
     */
    public function actionGetDayStats($id, $day = 'yesterday')
    {
        $model = $this->findModel($id);
        $dataLoader = $model->takeStatsByDay($day);
        $dataLoader->saveStats();
        print_r($dataLoader->data);
    }

    /**
     * Get stats by month. Arguments: id month
     * @param $id
     * @param string $month
     */
    public function actionGetMonthStats($id, $month = 'first day of last month')
    {
        $model = $this->findModel($id);
        $dataLoader = $model->takeStatsByMonth($month);
        $dataLoader->saveStats();
        print_r($dataLoader->data);
    }

    /**
     * Get customers registrations by date. Arguments: id day
     * @param $id
     * @param string $day
     */
    public function actionGetDayCustomers($id, $day = 'yesterday')
    {
        $model = $this->findModel($id);
        $dataLoader = $model->takeCustomersByDay($day);
        $dataLoader->saveCustomers();
        print_r($dataLoader->data);
    }

    /**
     * Get customers registrations by month. Arguments: id month
     * @param $id
     * @param string $month
     */
    public function actionGetMonthCustomers($id, $month = 'first day of last month')
    {
        $model = $this->findModel($id);
        $dataLoader = $model->takeCustomersByMonth($month);
        $dataLoader->saveCustomers();
        print_r($dataLoader->data);
    }

    public function actionCalculateMonth($year = null, $month = null)
    {
        if(!$year || !$month)
        {
            $date = (new \DateTimeImmutable());
            $year = (int)$date->format('Y');
            $month = (int)$date->format('m') - 1;
        }
        (new MonthSummaryService())->counting($year, $month);
    }

	public function actionCreateFinalStats($year = null, $month = null)
	{
		if(!$year || !$month)
		{
			$date = (new \DateTimeImmutable());
			$year = (int)$date->format('Y');
			$month = (int)$date->format('m') - 1;
		}
		(new MonthSummaryService())->counting($year, $month);
		(new MonthTransferService())->createFinalStats($year, $month);
    }

	public function actionCreateTransfers($year = null, $month = null)
	{
		if(!$year || !$month)
		{
			$date = (new \DateTimeImmutable());
			$year = (int)$date->format('Y');
			$month = (int)$date->format('m') - 1;
		}
		(new MonthTransferService())->createTransactions($year, $month);
    }

    /**
     * @param $id
     * @return BaseCashback
     * @throws Exception
     */
    private function findModel($id)
    {
        $cashbackModel = Cashback::find()->where(['or', ['id' => $id], ['name' => $id]])->one();
//        echo $cashbackModel->createCommand()->sql;
//        exit;
        if(!$cashbackModel) throw new Exception("Cashback model with id '{$id}' not found");

        $model = new $cashbackModel->object(['cashbackModel' => $cashbackModel]);

        if(!($model instanceof BaseCashback)) throw new Exception("Cashback object must be instance of BaseCashback");
        return $model;
    }
}