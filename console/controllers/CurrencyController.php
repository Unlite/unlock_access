<?php

namespace console\controllers;


use common\models\Currency;
use common\models\CurrencyRelations;
use Swap\Builder;
use yii\console\Controller;
use yii\helpers\Console;

class CurrencyController extends Controller
{

    public function actionIndex()
    {
        $command = $this->ansiFormat('yii user/', Console::FG_YELLOW);
        $actionLoad = $this->ansiFormat('load', Console::FG_GREEN);
        $this->stdout("{$actionLoad} \t -> \t Load currencies exchanges. {$command}{$actionLoad}" . PHP_EOL);
    }

    public function actionLoad()
    {
        $swap = (new Builder())
            ->add('google')
//            ->add('forge', ['api_key' => 'QHRhSjkhAJIVOMbFmdezK9EREPN3sEqR'])
//            ->add('fixer')
            //->add('russian_central_bank')
            ->build();
        $currency_codes = Currency::find()->select(['code'])->system()->column();
        foreach ($currency_codes as $currency_code) {
            foreach ($currency_codes as $currency_code_r) {
                $value = 1;
                $currencyRelation = CurrencyRelations::findByCodes($currency_code, $currency_code_r)->one();
                if (!$currencyRelation) {
                    $currencyRelation = new CurrencyRelations([
                        'currency_code' => $currency_code,
                        'currency_code_r' => $currency_code_r,
                        'value' => $value
                    ]);
                }
                if ($currency_code !== $currency_code_r) {
                    try {
                        $value = $swap->latest(strtoupper($currency_code . '/' . $currency_code_r))->getValue();
                        $currencyRelation->value = $value;
                    } catch (\Exception $e) {
                        $this->stdout("$currency_code/$currency_code_r: " . $e->getMessage() . PHP_EOL);
                    }
                }
                if($currencyRelation->save()){
                    $this->stdout("$currency_code/$currency_code_r: " . $value . ". Saved" . PHP_EOL);
                }else{
                    $this->stdout("$currency_code/$currency_code_r: " . $value . ". Not saved" . PHP_EOL);
                }
            }
        }
    }
}