<?php

namespace console\controllers;

/**
 * Trait SelectTrait
 * @package console\controllers
 */
trait SelectTrait
{

    /**
     * @param string $prompt
     * @param array $options
     * @param bool $exit
     * @param string $exitOption
     * @return string
     */
    public function select($prompt, $options = [], bool $exit = false, string $exitOption = 'quit')
    {
        if ($exit) {
            $options[$exitOption] = 'Exit from application';
        }
        $value = parent::select($prompt, $options);
        if ($value === $exitOption) exit;
        return $value;
    }
}