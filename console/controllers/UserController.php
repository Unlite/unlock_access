<?php

namespace console\controllers;


use common\models\forms\SignUpForm;
use common\models\User;
use common\services\UserService;
use Yii;
use yii\base\Module;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;

class UserController extends Controller
{
    use UsersTrait, SelectTrait;
    private $service;

    public function __construct($id, Module $module, UserService $service, array $config = [])
    {
        $this->service = $service;
        parent::__construct($id, $module, $config);
    }

    /**
     * Help method
     */
    public function actionIndex()
    {
        $command = $this->ansiFormat('yii user/', Console::FG_YELLOW);
        $actionCreate = $this->ansiFormat('create', Console::FG_GREEN);
        $this->stdout("{$actionCreate} \t -> \t Create new user. {$command}{$actionCreate} [name] [email] [password]" . PHP_EOL);
        $actionDelete = $this->ansiFormat('delete', Console::FG_GREEN);
        $this->stdout("{$actionDelete} \t -> \t Delete user. {$command}{$actionDelete} [username]" . PHP_EOL);
        $actionUserRoles = $this->ansiFormat('user-roles', Console::FG_GREEN);
        $this->stdout("{$actionUserRoles} \t -> \t Shows user roles. {$command}{$actionUserRoles} [username]" . PHP_EOL);
        $actionRoles = $this->ansiFormat('roles', Console::FG_GREEN);
        $this->stdout("{$actionRoles} \t -> \t Shows all roles. {$command}{$actionRoles}" . PHP_EOL);
        $actionRoleAdd = $this->ansiFormat('role-add', Console::FG_GREEN);
        $this->stdout("{$actionRoleAdd} \t -> \t Add role to user. {$command}{$actionRoleAdd} [username] [roleName]" . PHP_EOL);
        $actionRoleAssign = $this->ansiFormat('role-assign', Console::FG_GREEN);
        $this->stdout("{$actionRoleAssign} \t -> \t Assign role to user. {$command}{$actionRoleAssign} [username] [roleName]" . PHP_EOL);
        $actionRoleRevoke = $this->ansiFormat('role-revoke', Console::FG_GREEN);
        $this->stdout("{$actionRoleRevoke} \t -> \t Revoke user role. {$command}{$actionRoleRevoke} [username] [roleName]" . PHP_EOL);
        $actionRoleRevokeAll = $this->ansiFormat('role-revoke-all', Console::FG_GREEN);
        $this->stdout("{$actionRoleRevokeAll} \t -> \t Revoke all user roles. {$command}{$actionRoleRevokeAll} [username]" . PHP_EOL);
    }


    /**
     * Create new user. Arguments: [username] [email] [password]
     * @param string|null $username
     * @param string|null $email
     * @param string|null $password
     * @return int
     */
    public function actionCreate($username = null, $email = null, $password = null, $day = null, $month = null, $year = null)
    {
        $form = new SignUpForm([
            'username' => $username ?: $this->prompt('Username:', ['required' => true]),
            'email' => $email ?: $this->prompt('Email:', ['required' => true]),
            'password' => $password ?: $this->prompt('Password:', ['required' => true]),
            'day' => $day ?: $this->prompt('Day:', ['required' => true]),
            'month' => $month ?: $this->prompt('Month:', ['required' => true]),
            'year' => $year ?: $this->prompt('Year:', ['required' => true]),
        ]);
        $user = $this->service->requestSignup($form);
        if (!$user) {
            foreach ($form->getFirstErrors() as $attribute => $error) {
                $this->stderr("$attribute: $error" . PHP_EOL);
            }
            return ExitCode::UNSPECIFIED_ERROR;
        }

        if ($this->confirm('Add role?', false)) {
            $this->assign($user);
        }
        return ExitCode::OK;
    }

    /**
     * Hard delete user. Arguments: [username]
     * @param string|null $username
     * @return int
     */
    public function actionDelete($username = null)
    {
        if ($username)
            $user = $this->findUser($username, 'username');
        else
            $user = $this->findUser();
        if (!$user) {
            $this->stderr('User not found.' . PHP_EOL);
            return ExitCode::UNSPECIFIED_ERROR;
        }

        $username = $this->ansiFormat($user->username, Console::FG_YELLOW);
        if ($this->confirm("Are you sure you want to delete the user {$username}?")) {
            if ($this->service->userDelete($user)) {
                $this->stdout("User {$username} deleted!" . PHP_EOL);
                return ExitCode::OK;
            } else {
                $this->stdout("User {$username} can't be deleted!" . PHP_EOL);
                return ExitCode::UNSPECIFIED_ERROR;
            }
        }
        return ExitCode::OK;
    }

    /**
     * Shows user roles. Arguments [username]
     * @param string|null $username
     * @return int
     */
    public function actionUserRoles($username = null)
    {
        if ($username)
            $user = $this->findUser($username, 'username');
        else
            $user = $this->findUser();
        if (!$user) {
            $this->stderr('User not found.' . PHP_EOL);
            return ExitCode::UNSPECIFIED_ERROR;
        }

        $roles = ArrayHelper::map($this->userRoles($user), 'name', 'description');
        if (!$roles) {
            $this->stdout($this->ansiFormat($user->username, Console::FG_GREEN) . ' haven\'t roles' . PHP_EOL);
            return ExitCode::OK;
        }
        $this->stdout($this->ansiFormat($user->username, Console::FG_GREEN) . ' roles:' . PHP_EOL);
        foreach ($roles as $name => $description) {
            $this->stdout($this->ansiFormat($name, Console::FG_PURPLE) . ' => ' . $this->ansiFormat($description, Console::FG_YELLOW) . PHP_EOL);
        }
        return ExitCode::OK;
    }

    /**
     * Shows roles
     * @return int
     */
    public function actionRoles()
    {
        $roles = ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description');
        foreach ($roles as $name => $description) {
            $this->stdout($this->ansiFormat($name, Console::FG_PURPLE) . ' => ' . $this->ansiFormat($description, Console::FG_YELLOW) . PHP_EOL);
        }
        return ExitCode::OK;
    }

    /**
     * Add Role to User. Arguments: [Username] [RoleName]
     * @param string|null $username
     * @param string|null $roleName
     * @return int
     */
    public function actionRoleAdd($username = null, $roleName = null)
    {
        if ($username)
            $user = $this->findUser($username, 'username');
        else
            $user = $this->findUser();
        if (!$user) {
            $this->stderr('User not found.' . PHP_EOL);
            return ExitCode::UNSPECIFIED_ERROR;
        }
        $this->assign($user, false, $roleName);
        return ExitCode::OK;
    }

    /**
     * Assign Role to User. Arguments: [Username] [RoleName]
     * @param string|null $username
     * @param string|null $roleName
     * @return int
     */
    public function actionRoleAssign($username = null, $roleName = null)
    {
        if ($username)
            $user = $this->findUser($username, 'username');
        else
            $user = $this->findUser();

        if (!$user) {
            $this->stderr('User not found.' . PHP_EOL);
            return ExitCode::UNSPECIFIED_ERROR;
        }
        $this->assign($user, true, $roleName);
        return ExitCode::OK;
    }

    /**
     * Assign Role to User. Arguments: [Username] [RoleName]
     * @param string|null $username
     * @param string|null $roleName
     * @return int
     */
    public function actionRoleRevoke($username = null, $roleName = null)
    {
        if ($username)
            $user = $this->findUser($username, 'username');
        else
            $user = $this->findUser();

        if (!$user) {
            $this->stderr('User not found.' . PHP_EOL);
            return ExitCode::UNSPECIFIED_ERROR;
        }
        $this->revokeRole($user, $roleName);
        return ExitCode::OK;
    }

    /**
     * Assign Role to User. Arguments: [Username]
     * @param string|null $username
     * @return int
     */
    public function actionRoleRevokeAll($username = null)
    {
        if ($username)
            $user = $this->findUser($username, 'username');
        else
            $user = $this->findUser();

        if (!$user) {
            $this->stderr('User not found.' . PHP_EOL);
            return ExitCode::UNSPECIFIED_ERROR;
        }
        $this->revokeRoles($user);
        return ExitCode::OK;
    }

    /**
     * @param User $user
     * @param bool $revokeRoles
     * @param string|null $roleName
     * @return bool
     */
    private function assign(User $user, $revokeRoles = false, $roleName = null): bool
    {
        if ($revokeRoles) $this->revokeRoles($user);

        $username = $this->ansiFormat($user->username, Console::FG_YELLOW);
        $userRoles = $this->userRoles($user);
        $roleName = $roleName ?: $this->select('Choose role:',
            ArrayHelper::map(
                $this->userRoles($user, false),
                'name',
                'description'
            ),
            true
        );

        if (ArrayHelper::keyExists($roleName, $userRoles)) {
            $this->stdout("{$username} already have role " . $this->ansiFormat($roleName, Console::FG_PURPLE) . PHP_EOL);
            return true;
        }

        $role = Yii::$app->authManager->getRole($roleName);
        if (!$role) {
            $this->stderr("Role {$roleName} not found" . PHP_EOL);
            return false;
        }

        Yii::$app->authManager->assign($role, $user->id);
        $this->stdout($this->ansiFormat($roleName, Console::FG_PURPLE) . ' role ' . ($revokeRoles ? 'assigned' : 'added') . " to user {$username}" . PHP_EOL);

        return true;
    }

    /**
     * @param User $user
     * @return bool
     */
    private function revokeRoles(User $user): bool
    {
        return Yii::$app->authManager->revokeAll($user->id);
    }

    /**
     * @param User $user
     * @param string|null $roleName
     * @return bool
     */
    private function revokeRole(User $user, $roleName = null)
    {
        $username = $this->ansiFormat($user->username, Console::FG_YELLOW);
        $userRoles = Yii::$app->authManager->getRolesByUser($user->id);
        $roleName = $roleName ?: $this->select('Choose role:',
            ArrayHelper::map($this->userRoles($user), 'name', 'description'), true);

        if (!ArrayHelper::keyExists($roleName, $userRoles)) {
            $this->stdout("{$username} haven't role " . $this->ansiFormat($roleName, Console::FG_PURPLE) . PHP_EOL);
            return true;
        }
        $role = Yii::$app->authManager->getRole($roleName);
        if (!$role) {
            $this->stderr("Role {$roleName} not found" . PHP_EOL);
            return false;
        }

        return Yii::$app->authManager->revoke($role, $user->id);
    }

    /**
     * @param User $user
     * @param bool $have
     * @return array
     */
    private function userRoles(User $user, $have = true)
    {
        return $have
            ? array_intersect_key(Yii::$app->authManager->getRoles(), Yii::$app->authManager->getRolesByUser($user->id))
            : array_diff_key(Yii::$app->authManager->getRoles(), Yii::$app->authManager->getRolesByUser($user->id));
    }

}