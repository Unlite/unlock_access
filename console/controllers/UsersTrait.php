<?php

namespace console\controllers;


use common\models\User;

trait UsersTrait
{
    /**
     * @param string|null $searchValue
     * @param string|null $method
     * @return null|User
     */
    private function findUser($searchValue = null, $method = null)
    {
        $methods = ['username', 'email', 'id'];
        if (!$method || !in_array($method, $methods)) {
            $this->stdout('Choose search method:' . PHP_EOL);
            $this->stdout('    -> 0 - by username' . PHP_EOL);
            $this->stdout('    -> 1 - by email' . PHP_EOL);
            $this->stdout('    -> 2 - by id' . PHP_EOL);
            $methodKey = $this->select('Find by:', $methods, true);
            $method = $methods[$methodKey];
        }
        if (!$searchValue) $searchValue = $this->prompt(lcfirst($method) . ':', ['required' => true]);

        return User::findOne([$method => $searchValue]);
    }


}