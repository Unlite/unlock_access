<?php

use yii\db\Migration;

/**
 * Handles the creation of table `site`.
 */
class m170724_140538_create_site_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%site}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(64)->notNull(),
            'img_src' => $this->string(1024),
            'predefined_rating' => $this->float()->notNull()->defaultValue(4),
            'rating' => $this->float()->notNull()->defaultValue(4),
            'expire' => $this->integer()->notNull()->defaultValue(3600),
            'type' => "ENUM('casino','betting','poker','other') NOT NULL",
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%site}}');
    }
}
