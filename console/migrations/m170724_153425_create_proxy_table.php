<?php

use yii\db\Migration;

/**
 * Handles the creation of table `proxy`.
 * Has foreign keys to the tables:
 *
 * - `geo`
 */
class m170724_153425_create_proxy_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%proxy}}', [
            'id' => $this->primaryKey(),
            'geo_code' => $this->string(5)->notNull(),
            'host' => $this->string(128)->notNull(),
            'port' => $this->integer(),
        ], $tableOptions);

        // creates index for column `geo_code`
        $this->createIndex(
            'idx-proxy-geo_code',
            '{{%proxy}}',
            'geo_code'
        );

        // add foreign key for table `geo`
        $this->addForeignKey(
            'fk-proxy-geo_code',
            '{{%proxy}}',
            'geo_code',
            '{{%geo}}',
            'code',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%proxy}}');
    }
}
