<?php

use yii\db\Migration;

/**
 * Handles the creation of table `review`.
 * Has foreign keys to the tables:
 *
 * - `site`
 */
class m170724_154016_create_review_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%review}}', [
            'id' => $this->primaryKey(),
            'site_id' => $this->integer()->notNull(),
            'author' => $this->string(64),
            'text' => $this->text()->notNull(),
            'rating' => $this->float()->notNull(),
            'type' => "ENUM('generated','user') NOT NULL",
        ], $tableOptions);

        // creates index for column `site_id`
        $this->createIndex(
            'idx-review-site_id',
            '{{%review}}',
            'site_id'
        );

        // add foreign key for table `site`
        $this->addForeignKey(
            'fk-review-site_id',
            '{{%review}}',
            'site_id',
            '{{%site}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%review}}');
    }
}
