<?php

use yii\db\Migration;

/**
 * Handles the creation of table `domain`.
 * Has foreign keys to the tables:
 *
 * - `site`
 */
class m170725_014832_create_domain_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%domain}}', [
            'id' => $this->primaryKey(),
            'site_id' => $this->integer()->notNull(),
            'type' => "ENUM('main','mirror') NOT NULL",
            'domain' => $this->string(256)->notNull(),
            'direct_url' => $this->string(1024)->notNull(),
        ], $tableOptions);

        // creates index for column `site_id`
        $this->createIndex(
            'idx-domain-site_id',
            '{{%domain}}',
            'site_id'
        );

        // add foreign key for table `site`
        $this->addForeignKey(
            'fk-domain-site_id',
            '{{%domain}}',
            'site_id',
            '{{%site}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%domain}}');
    }
}
