<?php

use yii\db\Migration;

/**
 * Handles the creation of table `site_translation`.
 * Has foreign keys to the tables:
 *
 * - `site`
 * - `geo`
 */
class m170725_020835_create_site_translation_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%site_translation}}', [
            'site_id' => $this->integer()->notNull(),
            'geo_code' => $this->string(5)->notNull(),
            'description' => $this->string(1024)->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('pk-site_translation', '{{%site_translation}}', ['site_id', 'geo_code']);

        // creates index for column `site_id`
        $this->createIndex(
            'idx-site_translation-site_id',
            '{{%site_translation}}',
            'site_id'
        );

        // add foreign key for table `site`
        $this->addForeignKey(
            'fk-site_translation-site_id',
            '{{%site_translation}}',
            'site_id',
            '{{%site}}',
            'id',
            'CASCADE'
        );

        // creates index for column `geo_code`
        $this->createIndex(
            'idx-site_translation-geo_code',
            '{{%site_translation}}',
            'geo_code'
        );

        // add foreign key for table `geo`
        $this->addForeignKey(
            'fk-site_translation-geo_code',
            '{{%site_translation}}',
            'geo_code',
            '{{%geo}}',
            'code',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%site_translation}}');
    }
}
