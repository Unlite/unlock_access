<?php

use yii\db\Migration;

/**
 * Handles the creation of table `site_geo`.
 * Has foreign keys to the tables:
 *
 * - `site`
 * - `geo`
 */
class m170725_022816_create_junction_table_for_site_and_geo_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%site_geo}}', [
            'site_id' => $this->integer(),
            'geo_code' => $this->string(5),
        ], $tableOptions);

        $this->addPrimaryKey('pk-site_geo', '{{%site_geo}}', ['site_id', 'geo_code']);

        // creates index for column `site_id`
        $this->createIndex(
            'idx-site_geo-site_id',
            '{{%site_geo}}',
            'site_id'
        );

        // add foreign key for table `site`
        $this->addForeignKey(
            'fk-site_geo-site_id',
            '{{%site_geo}}',
            'site_id',
            '{{%site}}',
            'id',
            'CASCADE'
        );

        // creates index for column `geo_id`
        $this->createIndex(
            'idx-site_geo-geo_code',
            '{{%site_geo}}',
            'geo_code'
        );

        // add foreign key for table `geo`
        $this->addForeignKey(
            'fk-site_geo-geo_code',
            '{{%site_geo}}',
            'geo_code',
            '{{%geo}}',
            'code',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%site_geo}}');
    }
}
