<?php

use yii\db\Migration;

/**
 * Handles the creation of table `domain_unblock`.
 */
class m170725_023817_create_domain_unblock_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%domain_unblock}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(128)->notNull()->unique(),
            'pattern' => $this->string(512)->notNull(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%domain_unblock}}');
    }
}
