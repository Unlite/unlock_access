<?php

use yii\db\Migration;

/**
 * Handles the creation of table `domain_unblock_proxy`.
 * Has foreign keys to the tables:
 *
 * - `domain_unblock`
 * - `proxy`
 */
class m170725_025904_create_junction_table_for_domain_unblock_and_proxy_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%domain_unblock_proxy}}', [
            'domain_unblock_id' => $this->integer(),
            'proxy_id' => $this->integer(),
        ], $tableOptions);

        $this->addPrimaryKey('pk-domain_unblock_proxy', '{{%domain_unblock_proxy}}', ['domain_unblock_id', 'proxy_id']);

        // creates index for column `domain_unblock_id`
        $this->createIndex(
            'idx-domain_unblock_proxy-domain_unblock_id',
            '{{%domain_unblock_proxy}}',
            'domain_unblock_id'
        );

        // add foreign key for table `domain_unblock`
        $this->addForeignKey(
            'fk-domain_unblock_proxy-domain_unblock_id',
            '{{%domain_unblock_proxy}}',
            'domain_unblock_id',
            '{{%domain_unblock}}',
            'id',
            'CASCADE'
        );

        // creates index for column `proxy_id`
        $this->createIndex(
            'idx-domain_unblock_proxy-proxy_id',
            '{{%domain_unblock_proxy}}',
            'proxy_id'
        );

        // add foreign key for table `proxy`
        $this->addForeignKey(
            'fk-domain_unblock_proxy-proxy_id',
            '{{%domain_unblock_proxy}}',
            'proxy_id',
            '{{%proxy}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%domain_unblock_proxy}}');
    }
}
