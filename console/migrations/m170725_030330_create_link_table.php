<?php

use yii\db\Migration;

/**
 * Handles the creation of table `link`.
 * Has foreign keys to the tables:
 *
 * - `domain`
 */
class m170725_030330_create_link_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%link}}', [
            'id' => $this->primaryKey(),
            'domain_id' => $this->integer()->notNull(),
            'redirect_url' => $this->string(1024)->notNull(),
        ], $tableOptions);

        // creates index for column `domain_id`
        $this->createIndex(
            'idx-link-domain_id',
            '{{%link}}',
            'domain_id'
        );

        // add foreign key for table `domain`
        $this->addForeignKey(
            'fk-link-domain_id',
            '{{%link}}',
            'domain_id',
            '{{%domain}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%link}}');
    }
}
