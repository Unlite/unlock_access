<?php

use yii\db\Migration;

/**
 * Handles the creation of table `link_wildcard`.
 * Has foreign keys to the tables:
 *
 * - `link`
 */
class m170725_030757_create_link_wildcard_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%link_wildcard}}', [
            'id' => $this->primaryKey(),
            'link_id' => $this->integer()->notNull(),
            'pattern' => $this->string(1024)->notNull(),
        ], $tableOptions);

        // creates index for column `link_id`
        $this->createIndex(
            'idx-link_wildcard-link_id',
            '{{%link_wildcard}}',
            'link_id'
        );

        // add foreign key for table `link`
        $this->addForeignKey(
            'fk-link_wildcard-link_id',
            '{{%link_wildcard}}',
            'link_id',
            '{{%link}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%link_wildcard}}');
    }
}
