<?php

use yii\db\Migration;

/**
 * Handles adding aliases to table `site`.
 */
class m170731_203950_add_aliases_column_to_site_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%site}}', 'aliases', $this->string(1024));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%site}}', 'aliases');
    }
}
