<?php

use yii\db\Migration;

class m170822_161731_alter_column_type_in_site_table extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%site}}', 'type', "ENUM('casino','betting','poker','other','hidden') NOT NULL DEFAULT 'other'");
    }

    public function down()
    {
        $this->alterColumn('{{%site}}', 'type', "ENUM('casino','betting','poker','other') NOT NULL");
    }
}
