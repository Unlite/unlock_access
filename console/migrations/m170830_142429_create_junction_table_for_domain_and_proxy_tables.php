<?php

use yii\db\Migration;

/**
 * Handles the creation of table `domain_proxy`.
 * Has foreign keys to the tables:
 *
 * - `domain`
 * - `proxy`
 */
class m170830_142429_create_junction_table_for_domain_and_proxy_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%domain_proxy}}', [
            'domain_id' => $this->integer(),
            'proxy_id' => $this->integer(),
        ], $tableOptions);

        $this->addPrimaryKey('pk-domain_proxy', '{{%domain_proxy}}', ['domain_id', 'proxy_id']);

        // creates index for column `domain_id`
        $this->createIndex(
            'idx-domain_proxy-domain_id',
            '{{%domain_proxy}}',
            'domain_id'
        );

        // add foreign key for table `domain`
        $this->addForeignKey(
            'fk-domain_proxy-domain_id',
            '{{%domain_proxy}}',
            'domain_id',
            '{{%domain}}',
            'id',
            'CASCADE'
        );

        // creates index for column `proxy_id`
        $this->createIndex(
            'idx-domain_proxy-proxy_id',
            '{{%domain_proxy}}',
            'proxy_id'
        );

        // add foreign key for table `proxy`
        $this->addForeignKey(
            'fk-domain_proxy-proxy_id',
            '{{%domain_proxy}}',
            'proxy_id',
            '{{%proxy}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%domain_proxy}}');
    }
}
