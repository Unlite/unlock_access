<?php

use yii\db\Migration;

/**
 * Handles adding proxy_pattern to table `domain`.
 */
class m170830_164545_add_proxy_pattern_column_to_domain_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%domain}}', 'proxy_pattern', $this->string(512));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%domain}}', 'proxy_pattern');
    }
}
