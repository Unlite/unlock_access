<?php

use yii\db\Migration;

/**
 * Handles adding type to table `proxy`.
 */
class m170929_155723_add_type_column_to_proxy_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%proxy}}', 'type', "ENUM('private','public') NOT NULL DEFAULT 'private'");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%proxy}}', 'type');
    }
}
