<?php

use yii\db\Migration;

class m171003_130746_add_column_status_in_site_domain_proxy_tables extends Migration
{
	public function up()
	{
		$this->addColumn('{{%site}}', 'status', "ENUM('active','paused') NOT NULL DEFAULT 'active'");
		$this->addColumn('{{%domain}}', 'status', "ENUM('active','paused') NOT NULL DEFAULT 'active'");
		$this->addColumn('{{%proxy}}', 'status', "ENUM('active','paused') NOT NULL DEFAULT 'active'");
	}

	public function down()
	{
		$this->dropColumn('{{%site}}', 'status');
		$this->dropColumn('{{%domain}}', 'status');
		$this->dropColumn('{{%proxy}}', 'status');
	}
}
