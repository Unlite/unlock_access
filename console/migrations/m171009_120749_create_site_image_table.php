<?php

use yii\db\Migration;

/**
 * Handles the creation of table `site_image`.
 * Has foreign keys to the tables:
 *
 * - `site`
 */
class m171009_120749_create_site_image_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%site_image}}', [
            'id' => $this->primaryKey(),
            'site_id' => $this->integer()->notNull(),
            'type' => $this->string(128)->notNull(),
            'src' => $this->string(1024)->notNull(),
        ], $tableOptions);

        // creates index for column `site_id`
        $this->createIndex(
            'idx-site_image-site_id',
            '{{%site_image}}',
            'site_id'
        );

        // add foreign key for table `site`
        $this->addForeignKey(
            'fk-site_image-site_id',
            '{{%site_image}}',
            'site_id',
            '{{%site}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%site_image}}');
    }
}
