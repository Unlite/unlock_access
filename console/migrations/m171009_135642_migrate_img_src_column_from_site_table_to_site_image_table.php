<?php

use yii\db\Migration;
use yii\db\Query;

class m171009_135642_migrate_img_src_column_from_site_table_to_site_image_table extends Migration
{
    public function up()
    {
        $query = new Query;
        $newLogos = [];
        $logos = $query->select(['id', 'img_src'])->from('{{%site}}')->all();
        foreach ($logos as $logo){
            if($logo['img_src']) $newLogos[] = [$logo['id'], $logo['img_src'], 'logo'];
        }
        $this->db->createCommand()->batchInsert('{{%site_image}}', ['site_id', 'src', 'type'], $newLogos)->execute();
    }

    public function down()
    {

    }

}
