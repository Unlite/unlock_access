<?php

use yii\db\Migration;

/**
 * Handles dropping img_src from table `site`.
 */
class m171009_143122_drop_img_src_column_from_site_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('{{%site}}', 'img_src');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('{{%site}}', 'img_src', $this->string(1024));
    }
}
