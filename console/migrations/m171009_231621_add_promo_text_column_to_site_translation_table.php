<?php

use yii\db\Migration;

/**
 * Handles adding promo_text to table `site_translation`.
 */
class m171009_231621_add_promo_text_column_to_site_translation_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%site_translation}}', 'promo_text', $this->string(256));
        $this->alterColumn('{{%site_translation}}', 'description', $this->string(1024));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%site_translation}}', 'promo_text');
        $this->alterColumn('{{%site_translation}}', 'description', $this->string(1024)->notNull());
    }
}
