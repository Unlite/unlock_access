<?php

use yii\db\Migration;

/**
 * Handles the creation of table `game`.
 */
class m171012_113816_create_game_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%game}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(245),
            'img_src' => $this->string(1024),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%game}}');
    }
}
