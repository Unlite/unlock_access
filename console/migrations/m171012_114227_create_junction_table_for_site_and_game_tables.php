<?php

use yii\db\Migration;

/**
 * Handles the creation of table `site_game`.
 * Has foreign keys to the tables:
 *
 * - `site`
 * - `game`
 */
class m171012_114227_create_junction_table_for_site_and_game_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%site_game}}', [
            'site_id' => $this->integer(),
            'game_id' => $this->integer(),
        ], $tableOptions);

        $this->addPrimaryKey('pk-site_game', '{{%site_game}}', ['site_id', 'game_id']);

        // creates index for column `site_id`
        $this->createIndex(
            'idx-site_game-site_id',
            '{{%site_game}}',
            'site_id'
        );

        // add foreign key for table `site`
        $this->addForeignKey(
            'fk-site_game-site_id',
            '{{%site_game}}',
            'site_id',
            '{{%site}}',
            'id',
            'CASCADE'
        );

        // creates index for column `game_id`
        $this->createIndex(
            'idx-site_game-game_id',
            '{{%site_game}}',
            'game_id'
        );

        // add foreign key for table `game`
        $this->addForeignKey(
            'fk-site_game-game_id',
            '{{%site_game}}',
            'game_id',
            '{{%game}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%site_game}}');
    }
}
