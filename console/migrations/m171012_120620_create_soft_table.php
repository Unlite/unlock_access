<?php

use yii\db\Migration;

/**
 * Handles the creation of table `soft`.
 */
class m171012_120620_create_soft_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%soft}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(256),
            'img_src' => $this->string(1024),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%soft}}');
    }
}
