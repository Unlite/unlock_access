<?php

use yii\db\Migration;

/**
 * Handles the creation of table `site_soft`.
 * Has foreign keys to the tables:
 *
 * - `site`
 * - `soft`
 */
class m171012_120730_create_junction_table_for_site_and_soft_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%site_soft}}', [
            'site_id' => $this->integer(),
            'soft_id' => $this->integer(),
        ], $tableOptions);

        $this->addPrimaryKey('pk-site_soft', '{{%site_soft}}', ['site_id', 'soft_id']);

        // creates index for column `site_id`
        $this->createIndex(
            'idx-site_soft-site_id',
            '{{%site_soft}}',
            'site_id'
        );

        // add foreign key for table `site`
        $this->addForeignKey(
            'fk-site_soft-site_id',
            '{{%site_soft}}',
            'site_id',
            '{{%site}}',
            'id',
            'CASCADE'
        );

        // creates index for column `soft_id`
        $this->createIndex(
            'idx-site_soft-soft_id',
            '{{%site_soft}}',
            'soft_id'
        );

        // add foreign key for table `soft`
        $this->addForeignKey(
            'fk-site_soft-soft_id',
            '{{%site_soft}}',
            'soft_id',
            '{{%soft}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%site_soft}}');
    }
}
