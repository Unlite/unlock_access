<?php

use yii\db\Migration;

/**
 * Handles the creation of table `license`.
 * Has foreign keys to the tables:
 *
 * - `site`
 * - `geo`
 */
class m171012_120912_create_junction_table_license_for_site_and_geo_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%license}}', [
            'site_id' => $this->integer(),
            'geo_code' => $this->string(5),
        ], $tableOptions);

        $this->addPrimaryKey('pk-license', '{{%license}}', ['site_id', 'geo_code']);

        // creates index for column `license`
        $this->createIndex(
            'idx-license-site_id',
            '{{%license}}',
            'site_id'
        );

        // add foreign key for table `license`
        $this->addForeignKey(
            'fk-license-site_id',
            '{{%license}}',
            'site_id',
            '{{%site}}',
            'id',
            'CASCADE'
        );

        // creates index for column `geo_code`
        $this->createIndex(
            'idx-license-geo_code',
            '{{%license}}',
            'geo_code'
        );

        // add foreign key for table `geo`
        $this->addForeignKey(
            'fk-license-geo_code',
            '{{%license}}',
            'geo_code',
            '{{%geo}}',
            'code',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%license}}');
    }
}
