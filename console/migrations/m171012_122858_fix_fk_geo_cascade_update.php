<?php

use yii\db\Migration;

class m171012_122858_fix_fk_geo_cascade_update extends Migration
{
    public function safeUp()
    {
        // drops foreign key for table `geo`
        $this->dropForeignKey(
            'fk-proxy-geo_code',
            '{{%proxy}}'
        );
        // add foreign key for table `geo`
        $this->addForeignKey(
            'fk-proxy-geo_code',
            '{{%proxy}}',
            'geo_code',
            '{{%geo}}',
            'code',
            'CASCADE',
            'CASCADE'
        );
        // drops foreign key for table `geo`
        $this->dropForeignKey(
            'fk-site_translation-geo_code',
            '{{%site_translation}}'
        );
        // add foreign key for table `geo`
        $this->addForeignKey(
            'fk-site_translation-geo_code',
            '{{%site_translation}}',
            'geo_code',
            '{{%geo}}',
            'code',
            'CASCADE',
            'CASCADE'
        );
        // drops foreign key for table `geo`
        $this->dropForeignKey(
            'fk-site_geo-geo_code',
            '{{%site_geo}}'
        );
        // add foreign key for table `geo`
        $this->addForeignKey(
            'fk-site_geo-geo_code',
            '{{%site_geo}}',
            'geo_code',
            '{{%geo}}',
            'code',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {

    }
}
