<?php

use yii\db\Migration;

/**
 * Handles the creation of table `site_payment`.
 * Has foreign keys to the tables:
 *
 * - `site`
 * - `payment`
 */
class m171012_130339_create_junction_site_and_payment_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%site_payment}}', [
            'site_id' => $this->integer(),
            'payment_id' => $this->integer(),
        ], $tableOptions);

        $this->addPrimaryKey('pk-site_payment', '{{%site_payment}}', ['site_id', 'payment_id']);

        // creates index for column `site_id`
        $this->createIndex(
            'idx-site_payment-site_id',
            '{{%site_payment}}',
            'site_id'
        );

        // add foreign key for table `site`
        $this->addForeignKey(
            'fk-site_payment-site_id',
            '{{%site_payment}}',
            'site_id',
            '{{%site}}',
            'id',
            'CASCADE'
        );

        // creates index for column `payment_id`
        $this->createIndex(
            'idx-site_payment-payment_id',
            '{{%site_payment}}',
            'payment_id'
        );

        // add foreign key for table `payment`
        $this->addForeignKey(
            'fk-site_payment-payment_id',
            '{{%site_payment}}',
            'payment_id',
            '{{%payment}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%site_payment}}');
    }
}
