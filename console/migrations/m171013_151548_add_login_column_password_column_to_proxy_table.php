<?php

use yii\db\Migration;

/**
 * Handles adding login and password to table `proxy`.
 */
class m171013_151548_add_login_column_password_column_to_proxy_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%proxy}}', 'login', $this->string(128));
        $this->addColumn('{{%proxy}}', 'password', $this->string(256));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%proxy}}', 'login');
        $this->dropColumn('{{%proxy}}', 'password');
    }
}
