<?php

use yii\db\Migration;
use yii\db\Query;
use yii\db\Schema;

/**
 * Handles the creation of table `site_info`.
 * Has foreign keys to the tables:
 *
 * - `site`
 */
class m171017_154655_create_site_info_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%site_info}}', [
            'id' => $this->primaryKey(),
            'casino_max_pay' => $this->string(64),
            'casino_max_win' => $this->string(64),
        ], $tableOptions);

        // add foreign key for table `site`
        $this->addForeignKey(
            'fk-site_info-id',
            '{{%site_info}}',
            'id',
            '{{%site}}',
            'id',
            'CASCADE'
        );

        $sites = (new Query())->select(['id'])->from('{{%site}}')->all();
        $this->db->createCommand()->batchInsert('{{%site_info}}', ['id'], $sites)->execute();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%site_info}}');
    }
}
