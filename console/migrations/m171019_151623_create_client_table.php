<?php

use yii\db\Migration;

/**
 * Handles the creation of table `client`.
 * Has foreign keys to the tables:
 *
 * - `user`
 */
class m171019_151623_create_client_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%client}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->defaultValue(null),
            'token' => $this->string(32)->notNull()->unique(),
            'secret' => $this->string(8)->notNull(),
            'ip' => $this->string(15),
            'extension_id' => $this->string(32),
            'user_agent' => $this->string(256),
            'created_at' => $this->integer(),
            'last_online_at' => $this->integer(),
        ], $tableOptions);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-client-user_id',
            '{{%client}}',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-client-user_id',
            '{{%client}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%client}}');
    }
}
