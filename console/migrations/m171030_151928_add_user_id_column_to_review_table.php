<?php

use yii\db\Migration;

/**
 * Handles adding user_id to table `review`.
 * Has foreign keys to the tables:
 *
 * - `user`
 */
class m171030_151928_add_user_id_column_to_review_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%review}}', 'user_id', $this->integer());

        $this->addColumn('{{%review}}', 'created_at', $this->integer()->defaultValue(0));

        $this->addColumn('{{%review}}', 'updated_at', $this->integer()->defaultValue(0));

        $this->alterColumn('{{%review}}', 'text', $this->text());

        $this->alterColumn('{{%review}}', 'rating', $this->decimal(5,2));

        $this->dropColumn('{{%review}}', 'type');

        // creates index for column `user_id`
        $this->createIndex(
            'idx-review-user_id',
            '{{%review}}',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-review-user_id',
            '{{%review}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-review-user_id',
            '{{%review}}'
        );

        $this->alterColumn('{{%review}}', 'rating', $this->float()->notNull()->defaultValue(4));

        $this->alterColumn('{{%review}}', 'text', $this->text()->notNull());

        $this->dropColumn('{{%review}}', 'created_at');

        $this->dropColumn('{{%review}}', 'updated_at');

        $this->dropColumn('{{%review}}', 'user_id');
    }
}
