<?php

use yii\db\Migration;

class m171031_151142_alter_rating_column_in_site_table extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%site}}', 'rating', $this->decimal(5,2));

        $this->alterColumn('{{%site}}', 'predefined_rating', $this->decimal(5,2));
    }

    public function safeDown()
    {
        $this->alterColumn('{{%site}}', 'rating', $this->float()->notNull()->defaultValue(4));

        $this->alterColumn('{{%site}}', 'predefined_rating', $this->float()->notNull()->defaultValue(4));
    }
}
