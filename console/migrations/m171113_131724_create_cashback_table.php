<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation of table `cashback`.
 * Has foreign keys to the tables:
 *
 * - `site`
 */
class m171113_131724_create_cashback_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%cashback}}', [
            'id' => $this->string(32),
            'object' => $this->string(128)->notNull(),
            'site_id' => $this->integer(),
            'client_status' => "ENUM('active', 'paused') NOT NULL DEFAULT 'active'",
            'cron_status' => "ENUM('active', 'paused') NOT NULL DEFAULT 'active'",
            'percent' => $this->decimal(6, 3)->notNull(),
            'user_percent' => $this->decimal(6, 3)->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('pk-cashback', '{{%cashback}}', 'id');

        // creates index for column `site_id`
        $this->createIndex(
            'idx-cashback-site_id',
            '{{%cashback}}',
            'site_id'
        );

        // add foreign key for table `site`
        $this->addForeignKey(
            'fk-cashback-site_id',
            '{{%cashback}}',
            'site_id',
            '{{%site}}',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%cashback}}');
    }
}
