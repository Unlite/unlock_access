<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cashback_stat`.
 * Has foreign keys to the tables:
 *
 * - `cashback`
 */
class m171113_142450_create_cashback_stat_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%cashback_stat}}', [
            'id' => $this->primaryKey(),
            'cashback_id' => $this->string(32)->notNull(),
            'customer_id' => $this->string(32),
            'currency' => $this->string(16),
            'opening_balance_amount' => $this->decimal(15, 3),
            //
            'fees_amount' => $this->decimal(15, 3),
            'banking_fees_amount' => $this->decimal(15, 3),
            'microgaming_fees_amount' => $this->decimal(15, 3),
            'rakes_amount' => $this->decimal(15, 3),
            'cash_bets_amount' => $this->decimal(15, 3),
            'bonus_bets_amount' => $this->decimal(15, 3),
            'cash_wins_amount' => $this->decimal(15, 3),
            'bonus_wins_amount' => $this->decimal(15, 3),
            'bonus_amount' => $this->decimal(15, 3),
            'deposits_amount' => $this->decimal(15, 3),
            'withdraw_amount' => $this->decimal(15, 3),
            'first_deposits_amount' => $this->decimal(15, 3),
            //
            'closing_balance_amount' => $this->decimal(15, 3),
            'net_gaming' => $this->decimal(15, 3),
            'net_revenue' => $this->decimal(15, 3),
            'percent' => $this->decimal(6, 3),
            'revenue' => $this->decimal(15, 3),
            'user_percent' => $this->decimal(6, 3),
            'user_revenue' => $this->decimal(15, 3),
            'period' => "ENUM('day', 'month', 'custom') NOT NULL DEFAULT 'day'",
            'date_start' => $this->date()->notNull(),
            'date_end' => $this->date(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
        ], $tableOptions);

        // creates index for column `period`
        $this->createIndex(
            'idx-cashback_stat-period',
            '{{%cashback_stat}}',
            'period'
        );

        // creates index for column `customer_id`
        $this->createIndex(
            'idx-cashback_stat-customer_id',
            '{{%cashback_stat}}',
            'customer_id'
        );

        // creates index for column `cashback_id`
        $this->createIndex(
            'idx-cashback_stat-cashback_id',
            '{{%cashback_stat}}',
            'cashback_id'
        );

        // add foreign key for table `cashback`
        $this->addForeignKey(
            'fk-cashback_stat-cashback_id',
            '{{%cashback_stat}}',
            'cashback_id',
            '{{%cashback}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%cashback_stat}}');
    }
}
