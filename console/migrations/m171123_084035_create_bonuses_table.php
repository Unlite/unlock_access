<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bonuses`.
 * Has foreign keys to the tables:
 *
 * - `site`
 */
class m171123_084035_create_bonuses_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%bonuses}}', [
            'id' => $this->primaryKey(),
            'site_id' => $this->integer()->notNull(),
            'type' => $this->string()->notNull(),
            'text' => $this->string(1024),
            'url' => $this->string(1024),
            'promo_code' => $this->string(256),
            'start_at' => $this->integer(),
            'end_at' => $this->integer(),
        ], $tableOptions);

        // creates index for column `site_id`
        $this->createIndex(
            'idx-bonuses-site_id',
            '{{%bonuses}}',
            'site_id'
        );

        // add foreign key for table `site`
        $this->addForeignKey(
            'fk-bonuses-site_id',
            '{{%bonuses}}',
            'site_id',
            '{{%site}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%bonuses}}');
    }
}
