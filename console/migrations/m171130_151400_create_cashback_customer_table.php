<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cashback_customer`.
 * Has foreign keys to the tables:
 *
 * - `client`
 * - `cashback`
 */
class m171130_151400_create_cashback_customer_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%cashback_customer}}', [
            'id' => $this->primaryKey(),
            'client_token' => $this->string(32),
            'customer_id' => $this->string(32)->notNull(),
            'cashback_id' => $this->string(32)->notNull(),
            'ip' => $this->string(15),
            'register_date' => $this->date(),

            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
        ], $tableOptions);

        // creates unique index for columns `cashback_id` and `customer_id`
        $this->createIndex(
            'unq-cashback_customer-cashback_id-customer_id',
            '{{%cashback_customer}}',
            ['cashback_id', 'customer_id'],
            true
        );

        // creates index for column `client_token`
        $this->createIndex(
            'idx-cashback_customer-client_token',
            '{{%cashback_customer}}',
            'client_token'
        );

        // add foreign key for table `client`
        $this->addForeignKey(
            'fk-cashback_customer-client_token',
            '{{%cashback_customer}}',
            'client_token',
            '{{%client}}',
            'token',
            'CASCADE'
        );

        // creates index for column `cashback_id`
        $this->createIndex(
            'idx-cashback_customer-cashback_id',
            '{{%cashback_customer}}',
            'cashback_id'
        );

        // add foreign key for table `cashback`
        $this->addForeignKey(
            'fk-cashback_customer-cashback_id',
            '{{%cashback_customer}}',
            'cashback_id',
            '{{%cashback}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%cashback_customer}}');
    }
}
