<?php

use yii\db\Migration;

/**
 * Handles the creation of table `locale`.
 */
class m171229_094357_create_locale_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}

		$this->createTable('{{%locale}}', [
            'code' => $this->string(5)->unsigned(),
            'name' => $this->string(255)->notNull(),
        ],$tableOptions);

		$this->addPrimaryKey('pk-locale', '{{%locale}}', 'code');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%locale}}');
    }
}
