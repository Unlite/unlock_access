<?php

use yii\db\Migration;

/**
 * Handles the creation of table `site_locale`.
 * Has foreign keys to the tables:
 *
 * - `site`
 * - `locale`
 */
class m171229_095420_create_site_locale_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%site_locale}}', [
            'site_id' => $this->integer(11)->notNull(),
            'locale_code' => $this->string(5)->notNull(),
        ], $tableOptions);

        // creates index for column `site_id`
        $this->createIndex(
            'idx-site_locale-site_id',
            '{{%site_locale}}',
            'site_id'
        );

        // add foreign key for table `site`
        $this->addForeignKey(
            'fk-site_locale-site_id',
            '{{%site_locale}}',
            'site_id',
            '{{%site}}',
            'id',
            'CASCADE'
        );

        // creates index for column `locale_code`
        $this->createIndex(
            'idx-site_locale-locale_code',
            '{{%site_locale}}',
            'locale_code'
        );

        // add foreign key for table `locale`
        $this->addForeignKey(
            'fk-site_locale-locale_code',
            '{{%site_locale}}',
            'locale_code',
            '{{%locale}}',
            'code',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%site_locale}}');
    }
}
