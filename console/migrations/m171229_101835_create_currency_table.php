<?php

use yii\db\Migration;

/**
 * Handles the creation of table `currency`.
 */
class m171229_101835_create_currency_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}

		$this->createTable('{{%currency}}', [
            'code' => $this->string(5)->unsigned(),
            'name' => $this->string(255),
        ],$tableOptions);


		$this->addPrimaryKey('pk-currency', '{{%currency}}', 'code');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%currency}}');
    }
}
