<?php

use yii\db\Migration;

/**
 * Handles the creation of table `site_currency`.
 * Has foreign keys to the tables:
 *
 * - `site`
 * - `currency`
 */
class m171229_102011_create_site_currency_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%site_currency}}', [
            'site_id' => $this->integer()->notNull(),
            'currency_code' => $this->string(5)->notNull(),
        ], $tableOptions);

        // creates index for column `site_id`
        $this->createIndex(
            'idx-site_currency-site_id',
            '{{%site_currency}}',
            'site_id'
        );

        // add foreign key for table `site`
        $this->addForeignKey(
            'fk-site_currency-site_id',
            '{{%site_currency}}',
            'site_id',
            '{{%site}}',
            'id',
            'CASCADE'
        );

        // creates index for column `currency_code`
        $this->createIndex(
            'idx-site_currency-currency_code',
            '{{%site_currency}}',
            'currency_code'
        );

        // add foreign key for table `currency`
        $this->addForeignKey(
            'fk-site_currency-currency_code',
            '{{%site_currency}}',
            'currency_code',
            '{{%currency}}',
            'code',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%site_currency}}');
    }
}
