<?php

use yii\db\Migration;

/**
 * Handles the creation of table `payment_system`.
 */
class m171229_102309_rename_payment_to_payment_system_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
		$this->renameTable('{{%payment}}','{{%payment_system}}');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
		$this->renameTable('{{%payment_system}}','{{%payment}}');
    }
}
