<?php

use yii\db\Migration;

/**
 * Handles the creation of table `site_payment_system`.
 * Has foreign keys to the tables:
 *
 * - `site`
 * - `payment_system`
 */
class m171229_104155_create_site_payment_system_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}

		$this->createTable('{{%site_payment_system}}', [
            'site_id' => $this->integer()->notNull(),
            'payment_system_id' => $this->integer()->notNull(),
            'is_deposit' => $this->smallinteger(1)->notNull(),
            'is_withdrawal' => $this->smallinteger(1)->notNull(),
        ],$tableOptions);

        // creates index for column `site_id`
        $this->createIndex(
            'idx-site_payment_system-site_id',
            '{{%site_payment_system}}',
            'site_id'
        );

        // add foreign key for table `site`
        $this->addForeignKey(
            'fk-site_payment_system-site_id',
            '{{%site_payment_system}}',
            'site_id',
            '{{%site}}',
            'id',
            'CASCADE'
        );

        // creates index for column `payment_system_id`
        $this->createIndex(
            'idx-site_payment_system-payment_system_id',
            '{{%site_payment_system}}',
            'payment_system_id'
        );

        // add foreign key for table `payment_system`
        $this->addForeignKey(
            'fk-site_payment_system-payment_system_id',
            '{{%site_payment_system}}',
            'payment_system_id',
            '{{%payment_system}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%site_payment_system}}');
    }
}
