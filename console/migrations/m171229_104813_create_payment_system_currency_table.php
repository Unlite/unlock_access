<?php

use yii\db\Migration;

/**
 * Handles the creation of table `payment_system_currency`.
 * Has foreign keys to the tables:
 *
 * - `payment_system`
 * - `currency`
 */
class m171229_104813_create_payment_system_currency_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}

		$this->createTable('{{%payment_system_currency}}', [
            'payment_system_id' => $this->integer()->notNull(),
            'currency_code' => $this->string(5)->notNull(),
        ], $tableOptions);

        // creates index for column `payment_system_id`
        $this->createIndex(
            'idx-payment_system_currency-payment_system_id',
            '{{%payment_system_currency}}',
            'payment_system_id'
        );

        // add foreign key for table `payment_system`
        $this->addForeignKey(
            'fk-payment_system_currency-payment_system_id',
            '{{%payment_system_currency}}',
            'payment_system_id',
            '{{%payment_system}}',
            'id',
            'CASCADE'
        );

        // creates index for column `currency_code`
        $this->createIndex(
            'idx-payment_system_currency-currency_code',
            '{{%payment_system_currency}}',
            'currency_code'
        );

        // add foreign key for table `currency`
        $this->addForeignKey(
            'fk-payment_system_currency-currency_code',
            '{{%payment_system_currency}}',
            'currency_code',
            '{{%currency}}',
            'code',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%payment_system_currency}}');
    }
}
