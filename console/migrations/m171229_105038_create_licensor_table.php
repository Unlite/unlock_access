<?php

use yii\db\Migration;

/**
 * Handles the creation of table `licensor`.
 * Has foreign keys to the tables:
 *
 * - `geo`
 */
class m171229_105038_create_licensor_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%licensor}}', [
            'id' => $this->primaryKey(),
            'geo_code' => $this->string(5)->notNull(),
            'name' => $this->string(255)->notNull(),
        ], $tableOptions);

        // creates index for column `geo_code`
        $this->createIndex(
            'idx-licensor-geo_code',
            '{{%licensor}}',
            'geo_code'
        );

        // add foreign key for table `geo`
        $this->addForeignKey(
            'fk-licensor-geo_code',
            '{{%licensor}}',
            'geo_code',
            '{{%geo}}',
            'code',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%licensor}}');
    }
}
