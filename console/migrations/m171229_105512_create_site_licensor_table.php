<?php

use yii\db\Migration;

/**
 * Handles the creation of table `site_licensor`.
 * Has foreign keys to the tables:
 *
 * - `site`
 * - `licensor`
 */
class m171229_105512_create_site_licensor_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%site_licensor}}', [
            'site_id' => $this->integer()->notNull(),
            'licensor_id' => $this->integer()->notNull(),
        ], $tableOptions);

        // creates index for column `site_id`
        $this->createIndex(
            'idx-site_licensor-site_id',
            '{{%site_licensor}}',
            'site_id'
        );

        // add foreign key for table `site`
        $this->addForeignKey(
            'fk-site_licensor-site_id',
            '{{%site_licensor}}',
            'site_id',
            '{{%site}}',
            'id',
            'CASCADE'
        );

        // creates index for column `licensor_id`
        $this->createIndex(
            'idx-site_licensor-licensor_id',
            '{{%site_licensor}}',
            'licensor_id'
        );

        // add foreign key for table `licensor`
        $this->addForeignKey(
            'fk-site_licensor-licensor_id',
            '{{%site_licensor}}',
            'licensor_id',
            '{{%licensor}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%site_licensor}}');
    }
}
