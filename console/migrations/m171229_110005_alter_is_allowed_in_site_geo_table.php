<?php

use yii\db\Migration;

/**
 * Class m171229_110005_alter_is_allowed_in_site_geo_table
 */
class m171229_110005_alter_is_allowed_in_site_geo_table extends Migration
{
    public function up()
    {
		$this->addColumn('{{%site_geo}}','is_allowed', $this->boolean()->notNull()->defaultValue(1));
    }

    public function down()
    {
		$this->dropColumn('{{%site_geo}}','is_allowed');
    }
}
