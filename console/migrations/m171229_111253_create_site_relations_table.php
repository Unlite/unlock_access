<?php

use yii\db\Migration;

/**
 * Handles the creation of table `site_relations`.
 * Has foreign keys to the tables:
 *
 * - `site`
 * - `site`
 */
class m171229_111253_create_site_relations_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%site_relations}}', [
            'site_id_l' => $this->integer()->notNull(),
            'site_id_h' => $this->integer()->notNull(),
        ], $tableOptions);

        // creates index for column `site_id_l`
        $this->createIndex(
            'idx-site_relations-site_id_l',
            '{{%site_relations}}',
            'site_id_l'
        );

        // add foreign key for table `site`
        $this->addForeignKey(
            'fk-site_relations-site_id_l',
            '{{%site_relations}}',
            'site_id_l',
            '{{%site}}',
            'id',
            'CASCADE'
        );

        // creates index for column `site_id_h`
        $this->createIndex(
            'idx-site_relations-site_id_h',
            '{{%site_relations}}',
            'site_id_h'
        );

        // add foreign key for table `site`
        $this->addForeignKey(
            'fk-site_relations-site_id_h',
            '{{%site_relations}}',
            'site_id_h',
            '{{%site}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%site_relations}}');
    }
}
