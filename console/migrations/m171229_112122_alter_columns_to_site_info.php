<?php

use yii\db\Migration;

/**
 * Class m171229_112122_alter_columns_to_site_info
 */
class m171229_112122_alter_columns_to_site_info extends Migration
{
    public function up()
    {
        $this->addColumn('{{%site_info}}', 'description', $this->string(255)->notNull());
        $this->addColumn('{{%site_info}}', 'min_deposit', $this->string(255)->notNull());
        $this->addColumn('{{%site_info}}', 'min_withdraw', $this->string(255)->notNull());
        $this->addColumn('{{%site_info}}', 'max_withdraw', $this->string(255)->notNull());
        $this->addColumn('{{%site_info}}', 'payout', $this->string(255)->notNull());
        $this->addColumn('{{%site_info}}', 'online_since', $this->integer(11)->notNull());
        $this->addColumn('{{%site_info}}', 'owner', $this->string(255)->notNull());
        $this->addColumn('{{%site_info}}', 'email', $this->string(255)->notNull());
        $this->addColumn('{{%site_info}}', 'support_types', $this->string(255)->notNull());
    }

    public function down()
    {
        $this->dropColumn('{{%site_info}}', 'description');
        $this->dropColumn('{{%site_info}}', 'min_deposit');
        $this->dropColumn('{{%site_info}}', 'min_withdraw');
        $this->dropColumn('{{%site_info}}', 'max_withdraw');
        $this->dropColumn('{{%site_info}}', 'payout');
        $this->dropColumn('{{%site_info}}', 'online_since');
        $this->dropColumn('{{%site_info}}', 'owner');
        $this->dropColumn('{{%site_info}}', 'email');
        $this->dropColumn('{{%site_info}}', 'support_types');
    }
}
