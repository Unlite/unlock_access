<?php

use yii\db\Migration;

/**
 * Class m180110_112527_alter_is_modile_friendly_to_site_info_table
 */
class m180110_112527_alter_is_modile_friendly_to_site_info_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%site_info}}', 'is_mobile_friendly', $this->boolean()->notNull());
    }

    public function down()
    {
        $this->dropColumn('{{%site_info}}', 'is_mobile_friendly');
    }
}
