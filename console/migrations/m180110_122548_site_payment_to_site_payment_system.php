<?php

use yii\db\Migration;

/**
 * Class m180110_122548_site_payment_to_site_payment_system
 */
class m180110_122548_site_payment_to_site_payment_system extends Migration
{
    public function up()
    {
        $this->dropTable('{{%site_payment_system}}');
        $this->addColumn('{{%site_payment}}', 'is_deposit', $this->boolean()->notNull());
        $this->addColumn('{{%site_payment}}', 'is_withdrawal', $this->boolean()->notNull());
    }

    public function down()
    {
        $this->dropColumn('{{%site_payment}}', 'is_deposit');
        $this->dropColumn('{{%site_payment}}', 'is_withdrawal');

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%site_payment_system}}', [
            'site_id' => $this->integer()->notNull(),
            'payment_system_id' => $this->integer()->notNull(),
            'is_deposit' => $this->smallinteger(1)->notNull(),
            'is_withdrawal' => $this->smallinteger(1)->notNull(),
        ], $tableOptions);

        // creates index for column `site_id`
        $this->createIndex(
            'idx-site_payment_system-site_id',
            '{{%site_payment_system}}',
            'site_id'
        );

        // add foreign key for table `site`
        $this->addForeignKey(
            'fk-site_payment_system-site_id',
            '{{%site_payment_system}}',
            'site_id',
            '{{%site}}',
            'id',
            'CASCADE'
        );

        // creates index for column `payment_system_id`
        $this->createIndex(
            'idx-site_payment_system-payment_system_id',
            '{{%site_payment_system}}',
            'payment_system_id'
        );

        // add foreign key for table `payment_system`
        $this->addForeignKey(
            'fk-site_payment_system-payment_system_id',
            '{{%site_payment_system}}',
            'payment_system_id',
            '{{%payment_system}}',
            'id',
            'CASCADE'
        );
    }
}
