<?php

use yii\db\Migration;

/**
 * Class m180110_163926_alter_pk_to_tables
 */
class m180110_163926_alter_pk_to_tables extends Migration
{

    public function up()
    {
        $this->addPrimaryKey('pk-payment_system_currency', '{{%payment_system_currency}}', ['payment_system_id', 'currency_code']);
        $this->addPrimaryKey('pk-site_currency', '{{%site_currency}}', ['site_id', 'currency_code']);
        $this->addPrimaryKey('pk-site_locale', '{{%site_locale}}', ['site_id', 'locale_code']);
        $this->addPrimaryKey('pk-site_licensor', '{{%site_licensor}}', ['site_id', 'licensor_id']);
    }

    public function down()
    {
        $this->dropPrimaryKey('pk-payment_system_currency', '{{%payment_system_currency}}');
        $this->dropPrimaryKey('pk-site_currency', '{{%site_currency}}');
        $this->dropPrimaryKey('pk-site_locale', '{{%site_locale}}');
        $this->dropPrimaryKey('pk-site_licensor', '{{%site_licensor}}');
    }
}
