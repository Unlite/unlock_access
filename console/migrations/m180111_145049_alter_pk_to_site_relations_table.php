<?php

use yii\db\Migration;

/**
 * Class m180111_145049_alter_pk_to_site_relations_table
 */
class m180111_145049_alter_pk_to_site_relations_table extends Migration
{
    public function up()
    {
        $this->addPrimaryKey('pk-site_relations', '{{%site_relations}}', ['site_id_l', 'site_id_h']);
    }

    public function down()
    {
        $this->dropPrimaryKey('pk-site_relations', '{{%site_relations}}');
    }
}
