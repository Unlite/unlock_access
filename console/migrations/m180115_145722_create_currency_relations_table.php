<?php

use yii\db\Migration;

/**
 * Handles the creation of table `currency_relations`.
 * Has foreign keys to the tables:
 *
 * - `currency`
 * - `currency`
 */
class m180115_145722_create_currency_relations_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%currency_relations}}', [
            'currency_code' => $this->string(5)->notNull(),
            'currency_code_r' => $this->string(5)->notNull(),
            'value' => $this->decimal(19,10)->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('pk-currency_relations', '{{%currency_relations}}', ['currency_code', 'currency_code_r']);

        // creates index for column `currency_code`
        $this->createIndex(
            'idx-currency_relations-currency_code',
            '{{%currency_relations}}',
            'currency_code'
        );

        // add foreign key for table `currency`
        $this->addForeignKey(
            'fk-currency_relations-currency_code',
            '{{%currency_relations}}',
            'currency_code',
            '{{%currency}}',
            'code',
            'CASCADE',
            'CASCADE'
        );

        // creates index for column `currency_code_r`
        $this->createIndex(
            'idx-currency_relations-currency_code_r',
            '{{%currency_relations}}',
            'currency_code_r'
        );

        // add foreign key for table `currency`
        $this->addForeignKey(
            'fk-currency_relations-currency_code_r',
            '{{%currency_relations}}',
            'currency_code_r',
            '{{%currency}}',
            'code',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%currency_relations}}');
    }
}
