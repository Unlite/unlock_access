<?php

use yii\db\Migration;

/**
 * Class m180116_142016_change_column_online_since_to_datetime
 */
class m180116_142016_change_column_online_since_to_datetime extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%site_info}}', 'online_since', $this->dateTime());
    }

    public function down()
    {
        $this->alterColumn('{{%site_info}}', 'online_since', $this->integer(11));
    }
}
