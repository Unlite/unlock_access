<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_currency`.
 * Has foreign keys to the tables:
 *
 * - `user`
 * - `currency`
 */
class m180116_150739_create_user_currency_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_currency}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'currency_code' => $this->string(5)->notNull(),
            'amount' => $this->decimal(19, 6)->notNull(),
        ], $tableOptions);

        // creates unique index for columns `user_id` and `currency_code`
        $this->createIndex(
            'unq-user_currency-user_id-currency_code',
            '{{%user_currency}}',
            ['user_id', 'currency_code'],
            true
        );

        // creates index for column `user_id`
        $this->createIndex(
            'idx-user_currency-user_id',
            '{{%user_currency}}',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user_currency-user_id',
            '{{%user_currency}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // creates index for column `currency_code`
        $this->createIndex(
            'idx-user_currency-currency_code',
            '{{%user_currency}}',
            'currency_code'
        );

        // add foreign key for table `currency`
        $this->addForeignKey(
            'fk-user_currency-currency_code',
            '{{%user_currency}}',
            'currency_code',
            '{{%currency}}',
            'code',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_currency}}');
    }
}
