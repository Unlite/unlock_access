<?php

use yii\db\Migration;

/**
 * Handles the creation of table `transfer`.
 * Has foreign keys to the tables:
 *
 * - `user_currency`
 * - `transfer`
 */
class m180117_145918_create_transfer_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%transfer}}', [
            'id' => $this->primaryKey(),
            'user_currency_id' => $this->integer(11)->notNull(),
            'type' => "ENUM('exchange', 'cashback', 'other') NOT NULL DEFAULT 'other'",
            'status' => "ENUM('hold', 'approved', 'rejected') NOT NULL DEFAULT 'hold'",
            'amount' => $this->decimal(19,6)->notNull(),
            'comment' => $this->string(255),
            'parent_id' => $this->integer(11),

            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
        ], $tableOptions);

        // creates index for column `user_currency_id`
        $this->createIndex(
            'idx-transfer-user_currency_id',
            '{{%transfer}}',
            'user_currency_id'
        );

        // add foreign key for table `user_currency`
        $this->addForeignKey(
            'fk-transfer-user_currency_id',
            '{{%transfer}}',
            'user_currency_id',
            '{{%user_currency}}',
            'id',
            'CASCADE'
        );

        // creates index for column `parent_id`
        $this->createIndex(
            'idx-transfer-parent_id',
            '{{%transfer}}',
            'parent_id'
        );

        // add foreign key for table `transfer`
        $this->addForeignKey(
            'fk-transfer-parent_id',
            '{{%transfer}}',
            'parent_id',
            '{{%transfer}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%transfer}}');
    }
}
