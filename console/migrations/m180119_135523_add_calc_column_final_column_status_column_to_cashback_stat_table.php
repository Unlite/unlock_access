<?php

use yii\db\Migration;

/**
 * Handles adding calc_column_final_column_status to table `cashback_stat`.
 */
class m180119_135523_add_calc_column_final_column_status_column_to_cashback_stat_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%cashback_stat}}', 'calc', $this->boolean()->notNull());
        $this->addColumn('{{%cashback_stat}}', 'currency_code', $this->string(5));
        $this->addColumn('{{%cashback_stat}}', 'final', $this->boolean());
        $this->addColumn('{{%cashback_stat}}', 'status', "ENUM('approved', 'hold', 'closed', 'rejected')");

        // creates index for column `currency_code`
        $this->createIndex(
            'idx-cashback_stat-currency_code',
            '{{%cashback_stat}}',
            'currency_code'
        );

        // add foreign key for table `currency`
        $this->addForeignKey(
            'fk-cashback_stat-currency_code',
            '{{%cashback_stat}}',
            'currency_code',
            '{{%currency}}',
            'code',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `currency`
        $this->dropForeignKey(
            'fk-cashback_stat-currency_code',
            '{{%cashback_stat}}'
        );

        $this->dropColumn('{{%cashback_stat}}', 'calc');
        $this->dropColumn('{{%cashback_stat}}', 'currency_code');
        $this->dropColumn('{{%cashback_stat}}', 'final');
        $this->dropColumn('{{%cashback_stat}}', 'status');
    }
}
