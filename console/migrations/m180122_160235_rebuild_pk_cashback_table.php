<?php

use yii\db\Migration;
use yii\db\Query;
use yii\db\Schema;

/**
 * Class m180122_160235_rebuild_pk_cashback_table
 */
class m180122_160235_rebuild_pk_cashback_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        //Add column `name` to `cashback` table
        $this->addColumn('{{%cashback}}', 'name', $this->string(32)->unique()->after('id'));
        //Duplicate values from `id` column
        $this->db->createCommand()->update('{{%cashback}}', ['name' => new \yii\db\Expression('id')])->execute();

        // drops foreign key for table `cashback` from `cashback_stat` table
        $this->dropForeignKey(
            'fk-cashback_stat-cashback_id',
            '{{%cashback_stat}}'
        );

        // drops index for column `cashback_id` from `cashback_stat` table
        $this->dropIndex(
            'idx-cashback_stat-cashback_id',
            '{{%cashback_stat}}'
        );

        // drops foreign key for table `cashback` from `cashback_customer` table
        $this->dropForeignKey(
            'fk-cashback_customer-cashback_id',
            '{{%cashback_customer}}'
        );

        // drops index for column `cashback_id` from `cashback_customer` table
        $this->dropIndex(
            'idx-cashback_customer-cashback_id',
            '{{%cashback_customer}}'
        );

        $this->dropPrimaryKey('pk-cashback', '{{%cashback}}');
        $this->dropColumn('{{%cashback}}', 'id');
        $this->addColumn('{{%cashback}}', 'id', $this->integer(11)->first());
        $this->addPrimaryKey('pk-cashback', '{{%cashback}}', 'id');

        //Truncate tables
        $this->truncateTable('{{%cashback_stat}}');
        $this->truncateTable('{{%cashback_customer}}');

        //Add temp columns to `cashback_stat` and `cashback_customer` tables
        $this->addColumn('{{%cashback_stat}}', 'cashback_id_temp', $this->integer(11)->notNull()->after('id'));
        $this->addColumn('{{%cashback_customer}}', 'cashback_id_temp', $this->integer(11)->notNull()->after('id'));
        //Drops columns
        $this->dropColumn('{{%cashback_stat}}', 'cashback_id');
        $this->dropColumn('{{%cashback_customer}}', 'cashback_id');
        //Rename Columns
        $this->renameColumn('{{%cashback_stat}}', 'cashback_id_temp', 'cashback_id');
        $this->renameColumn('{{%cashback_customer}}', 'cashback_id_temp', 'cashback_id');

        //Create Indexes
        //creates index for column `cashback_id` to `cashback_stat` table
        $this->createIndex(
            'idx-cashback_stat-cashback_id',
            '{{%cashback_stat}}',
            'cashback_id'
        );
        // add foreign key for table `cashback` to `cashback_stat` table
        $this->addForeignKey(
            'fk-cashback_stat-cashback_id',
            '{{%cashback_stat}}',
            'cashback_id',
            '{{%cashback}}',
            'id',
            'CASCADE'
        );
        //creates index for column `cashback_id` to `cashback_customer` table
        $this->createIndex(
            'idx-cashback_customer-cashback_id',
            '{{%cashback_customer}}',
            'cashback_id'
        );
        // add foreign key for table `cashback` to `cashback_customer` table
        $this->addForeignKey(
            'fk-cashback_customer-cashback_id',
            '{{%cashback_customer}}',
            'cashback_id',
            '{{%cashback}}',
            'id',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        // drops foreign key for table `cashback` from `cashback_stat` table
        $this->dropForeignKey(
            'fk-cashback_stat-cashback_id',
            '{{%cashback_stat}}'
        );

        // drops index for column `cashback_id` from `cashback_stat` table
        $this->dropIndex(
            'idx-cashback_stat-cashback_id',
            '{{%cashback_stat}}'
        );

        // drops foreign key for table `cashback` from `cashback_customer` table
        $this->dropForeignKey(
            'fk-cashback_customer-cashback_id',
            '{{%cashback_customer}}'
        );

        // drops index for column `cashback_id` from `cashback_customer` table
        $this->dropIndex(
            'idx-cashback_customer-cashback_id',
            '{{%cashback_customer}}'
        );

        $this->alterColumn('{{%cashback}}', 'id', $this->integer()->notNull());//delete AUTO_INCREMENT
        $this->dropPrimaryKey('pk-cashback', '{{%cashback}}');
        $this->dropColumn('{{%cashback}}', 'id');
        $this->addColumn('{{%cashback}}', 'id', $this->string(32)->first());

        //Duplicate values from `name` column
        $this->db->createCommand()->update('{{%cashback}}', ['id' => new \yii\db\Expression('name')])->execute();

        $this->addPrimaryKey('pk-cashback', '{{%cashback}}', 'id');
        //Drops column `name` from `cashback` table
        $this->dropColumn('{{%cashback}}', 'name');

        //Truncate tables
        $this->truncateTable('{{%cashback_stat}}');
        $this->truncateTable('{{%cashback_customer}}');

        //Add temp columns to `cashback_stat` and `cashback_customer` tables
        $this->addColumn('{{%cashback_stat}}', 'cashback_id_temp', $this->string(32)->notNull()->after('id'));
        $this->addColumn('{{%cashback_customer}}', 'cashback_id_temp', $this->string(32)->notNull()->after('id'));
        //Drops columns
        $this->dropColumn('{{%cashback_stat}}', 'cashback_id');
        $this->dropColumn('{{%cashback_customer}}', 'cashback_id');
        //Rename Columns
        $this->renameColumn('{{%cashback_stat}}', 'cashback_id_temp', 'cashback_id');
        $this->renameColumn('{{%cashback_customer}}', 'cashback_id_temp', 'cashback_id');

        //Create Indexes
        //creates index for column `cashback_id` to `cashback_stat` table
        $this->createIndex(
            'idx-cashback_stat-cashback_id',
            '{{%cashback_stat}}',
            'cashback_id'
        );
        // add foreign key for table `cashback` to `cashback_stat` table
        $this->addForeignKey(
            'fk-cashback_stat-cashback_id',
            '{{%cashback_stat}}',
            'cashback_id',
            '{{%cashback}}',
            'id',
            'CASCADE'
        );
        //creates index for column `cashback_id` to `cashback_customer` table
        $this->createIndex(
            'idx-cashback_customer-cashback_id',
            '{{%cashback_customer}}',
            'cashback_id'
        );
        // add foreign key for table `cashback` to `cashback_customer` table
        $this->addForeignKey(
            'fk-cashback_customer-cashback_id',
            '{{%cashback_customer}}',
            'cashback_id',
            '{{%cashback}}',
            'id',
            'CASCADE'
        );

    }
}
