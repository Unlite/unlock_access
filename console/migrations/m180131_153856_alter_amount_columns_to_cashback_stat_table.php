<?php

use yii\db\Migration;

/**
 * Class m180131_153856_alter_amount_columns_to_cashback_stat_table
 */
class m180131_153856_alter_amount_columns_to_cashback_stat_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('{{%cashback_stat}}', 'opening_balance_amount', $this->decimal(19, 6));
        $this->alterColumn('{{%cashback_stat}}', 'fees_amount', $this->decimal(19, 6));
        $this->alterColumn('{{%cashback_stat}}', 'banking_fees_amount', $this->decimal(19, 6));
        $this->alterColumn('{{%cashback_stat}}', 'microgaming_fees_amount', $this->decimal(19, 6));
        $this->alterColumn('{{%cashback_stat}}', 'rakes_amount', $this->decimal(19, 6));
        $this->alterColumn('{{%cashback_stat}}', 'cash_bets_amount', $this->decimal(19, 6));
        $this->alterColumn('{{%cashback_stat}}', 'bonus_bets_amount', $this->decimal(19, 6));
        $this->alterColumn('{{%cashback_stat}}', 'cash_wins_amount', $this->decimal(19, 6));
        $this->alterColumn('{{%cashback_stat}}', 'bonus_wins_amount', $this->decimal(19, 6));
        $this->alterColumn('{{%cashback_stat}}', 'bonus_amount', $this->decimal(19, 6));
        $this->alterColumn('{{%cashback_stat}}', 'deposits_amount', $this->decimal(19, 6));
        $this->alterColumn('{{%cashback_stat}}', 'withdraw_amount', $this->decimal(19, 6));
        $this->alterColumn('{{%cashback_stat}}', 'first_deposits_amount', $this->decimal(19, 6));
        $this->alterColumn('{{%cashback_stat}}', 'closing_balance_amount', $this->decimal(19, 6));
        $this->alterColumn('{{%cashback_stat}}', 'net_gaming', $this->decimal(19, 6));
        $this->alterColumn('{{%cashback_stat}}', 'net_revenue', $this->decimal(19, 6));
        $this->alterColumn('{{%cashback_stat}}', 'revenue', $this->decimal(19, 6));
        $this->alterColumn('{{%cashback_stat}}', 'user_revenue', $this->decimal(19, 6));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('{{%cashback_stat}}', 'opening_balance_amount', $this->decimal(15, 3));
        $this->alterColumn('{{%cashback_stat}}', 'fees_amount', $this->decimal(15, 3));
        $this->alterColumn('{{%cashback_stat}}', 'banking_fees_amount', $this->decimal(15, 3));
        $this->alterColumn('{{%cashback_stat}}', 'microgaming_fees_amount', $this->decimal(15, 3));
        $this->alterColumn('{{%cashback_stat}}', 'rakes_amount', $this->decimal(15, 3));
        $this->alterColumn('{{%cashback_stat}}', 'cash_bets_amount', $this->decimal(15, 3));
        $this->alterColumn('{{%cashback_stat}}', 'bonus_bets_amount', $this->decimal(15, 3));
        $this->alterColumn('{{%cashback_stat}}', 'cash_wins_amount', $this->decimal(15, 3));
        $this->alterColumn('{{%cashback_stat}}', 'bonus_wins_amount', $this->decimal(15, 3));
        $this->alterColumn('{{%cashback_stat}}', 'bonus_amount', $this->decimal(15, 3));
        $this->alterColumn('{{%cashback_stat}}', 'deposits_amount', $this->decimal(15, 3));
        $this->alterColumn('{{%cashback_stat}}', 'withdraw_amount', $this->decimal(15, 3));
        $this->alterColumn('{{%cashback_stat}}', 'first_deposits_amount', $this->decimal(15, 3));
        $this->alterColumn('{{%cashback_stat}}', 'closing_balance_amount', $this->decimal(15, 3));
        $this->alterColumn('{{%cashback_stat}}', 'net_gaming', $this->decimal(15, 3));
        $this->alterColumn('{{%cashback_stat}}', 'net_revenue', $this->decimal(15, 3));
        $this->alterColumn('{{%cashback_stat}}', 'revenue', $this->decimal(15, 3));
        $this->alterColumn('{{%cashback_stat}}', 'user_revenue', $this->decimal(15, 3));
    }

}
