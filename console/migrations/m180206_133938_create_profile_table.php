<?php

use yii\db\Migration;

/**
 * Handles the creation of table `profile`.
 */
class m180206_133938_create_profile_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%profile}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(11)->notNull(),
            'name' => $this->string(32),
            'surname' => $this->string(32),
        ], $tableOptions);

        $this->createIndex('idx-profile-user_id', '{{%profile}}', 'user_id');
        $this->addForeignKey('fk-profile-user_id', '{{%profile}}', 'user_id', '{{%user}}', 'id', 'CASCADE');

        $ids = (new \yii\db\Query())->select(['id'])->from('{{%user}}')->orderBy(['id' => SORT_ASC])->all();
        $this->db->createCommand()
            ->batchInsert('{{%profile}}', ['user_id'], $ids)
            ->execute();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%profile}}');
    }
}
