<?php

use yii\db\Migration;

/**
 * Handles the creation of table `game_provider`.
 */
class m180208_124826_create_game_provider_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}

		$this->createTable('{{%game_provider}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(64)->notNull(),
            'img_src' => $this->string(1024),
            'description' => $this->string(1024),
        ],$tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%game_provider}}');
    }
}
