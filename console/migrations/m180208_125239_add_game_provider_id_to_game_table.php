<?php

use yii\db\Migration;

/**
 * Class m180208_125239_add_game_provider_id_to_game_table
 */
class m180208_125239_add_game_provider_id_to_game_table extends Migration
{
	/**
	 * @inheritdoc
	 */
	public function up()
	{
		$this->addColumn('{{%game}}','game_provider_id',$this->integer(11));
		$this->addColumn('{{%game}}','description',$this->string(1024));
		$this->addColumn('{{%game}}','embedded_url',$this->string(255));
		$this->addForeignKey('fk_game_game_provider_id','{{%game}}','game_provider_id','game_provider','id');
	}

	/**
	 * @inheritdoc
	 */
	public function down()
	{
	    $this->dropForeignKey('fk_game_game_provider_id', '{{%game}}');
		$this->dropColumn('{{%game}}','game_provider_id');
		$this->dropColumn('{{%game}}','description');
		$this->dropColumn('{{%game}}','embedded_url');
	}
}
