<?php

use yii\db\Migration;

/**
 * Handles the creation of table `email_subscriptions`.
 * Has foreign keys to the tables:
 *
 * - `user`
 */
class m180213_111915_create_email_subscriptions_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}

        $this->createTable('{{%email_subscriptions}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(11),
            'email' => $this->string(255)->unique()->notNull(),
            'sub_promo' => $this->boolean()->notNull(),
            'sub_system' => $this->boolean(),
            'token' => $this->string(32)->notNull(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
        ],$tableOptions);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-email_subscriptions-user_id',
            '{{%email_subscriptions}}',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-email_subscriptions-user_id',
            '{{%email_subscriptions}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%email_subscriptions}}');
    }
}
