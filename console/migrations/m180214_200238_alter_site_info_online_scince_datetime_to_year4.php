<?php

use yii\db\Migration;

/**
 * Class m180214_200238_alter_site_info_online_scince_datetime_to_year4
 */
class m180214_200238_alter_site_info_online_scince_datetime_to_year4 extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->alterColumn('{{%site_info}}', 'online_since', 'Year(4)');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->alterColumn('{{%site_info}}', 'online_since', $this->dateTime());
    }
}
