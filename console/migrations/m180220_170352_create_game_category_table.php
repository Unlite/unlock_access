<?php

use yii\db\Migration;

/**
 * Handles the creation of table `game_category`.
 */
class m180220_170352_create_game_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}

        $this->createTable('{{%game_category}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(64)->notNull(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%game_category}}');
    }
}
