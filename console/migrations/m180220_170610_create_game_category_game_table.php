<?php

use yii\db\Migration;

/**
 * Handles the creation of table `game_category_game`.
 * Has foreign keys to the tables:
 *
 * - `game_category`
 * - `game`
 */
class m180220_170610_create_game_category_game_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%game_category_game}}', [
            'game_category_id' => $this->integer(11)->notNull(),
            'game_id' => $this->integer(11)->notNull(),
        ]);

        // creates index for column `game_category_id`
        $this->createIndex(
            'idx-game_category_game-game_category_id',
            '{{%game_category_game}}',
            'game_category_id'
        );

        // add foreign key for table `game_category`
        $this->addForeignKey(
            'fk-game_category_game-game_category_id',
            '{{%game_category_game}}',
            'game_category_id',
            '{{%game_category}}',
            'id',
            'CASCADE'
        );

        // creates index for column `game_id`
        $this->createIndex(
            'idx-game_category_game-game_id',
            '{{%game_category_game}}',
            'game_id'
        );

        // add foreign key for table `game`
        $this->addForeignKey(
            'fk-game_category_game-game_id',
            '{{%game_category_game}}',
            'game_id',
            '{{%game}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%game_category_game}}');
    }
}
