<?php

use yii\db\Migration;

/**
 * Class m180220_170843_alter_table_game_column_name
 */
class m180220_170843_alter_table_game_column_name extends Migration
{

    public function up()
    {
		$this->alterColumn('{{%game}}','name',$this->string(64)->notNull());
    }

    public function down()
    {
		$this->alterColumn('{{%game}}','name',$this->string(245));
    }
}
