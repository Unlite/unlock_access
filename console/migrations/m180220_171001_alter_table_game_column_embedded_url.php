<?php

use yii\db\Migration;

/**
 * Class m180220_171001_alter_table_game_column_embedded_url
 */
class m180220_171001_alter_table_game_column_embedded_url extends Migration
{

	public function up()
	{
		$this->alterColumn('{{%game}}','embedded_url',$this->string(1024));
	}

	public function down()
	{
		$this->alterColumn('{{%game}}','embedded_url',$this->string(255));
	}
}
