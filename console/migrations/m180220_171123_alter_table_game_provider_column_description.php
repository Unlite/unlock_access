<?php

use yii\db\Migration;

/**
 * Class m180220_171123_alter_table_game_provider_column_description
 */
class m180220_171123_alter_table_game_provider_column_description extends Migration
{

    public function up()
    {
        $this->alterColumn('{{%game_provider}}', 'description', $this->string(10240));
    }

    public function down()
    {
        $this->alterColumn('{{%game_provider}}', 'description', $this->string(1024));
    }
}
