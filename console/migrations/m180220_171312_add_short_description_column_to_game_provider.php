<?php

use yii\db\Migration;

/**
 * Class m180220_171312_add_short_description_column_to_game_provider
 */
class m180220_171312_add_short_description_column_to_game_provider extends Migration
{

	public function up()
	{
		$this->addColumn('{{%game_provider}}','short_description',$this->string(1024)->notNull());
	}

	public function down()
	{
		$this->dropColumn('{{%game_provider}}','short_description');
	}
}
