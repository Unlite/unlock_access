<?php

use yii\db\Migration;

/**
 * Class m180220_172210_alter_table_game_category_game_pk
 */
class m180220_172210_alter_table_game_category_game_pk extends Migration
{

    public function up()
    {
        $this->addPrimaryKey('pk-game_category_game', '{{%game_category_game}}', ['game_category_id', 'game_id']);
    }

    public function down()
    {
        $this->dropPrimaryKey('pk-game_category_game', '{{%game_category_game}}');
    }
}
