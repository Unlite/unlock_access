<?php

use yii\db\Migration;

/**
 * Handles dropping type from table `bonuses`.
 */
class m180222_123608_drop_type_column_from_bonuses_table extends Migration
{
	public function up()
	{
		$this->dropColumn('{{%bonuses}}','type');
	}

	public function down()
	{
		$this->addColumn('{{%bonuses}}','type',$this->string(1024)->notNull());
	}
}
