<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bonus_type`.
 */
class m180222_124256_create_bonus_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%bonus_type}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(64)->notNull(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%bonus_type}}');
    }
}
