<?php

use yii\db\Migration;

/**
 * Class m180222_124333_alter_bonus_type_id_column_to_bonuses_table
 */
class m180222_124333_alter_bonus_type_id_column_to_bonuses_table extends Migration
{
	public function up()
	{
		$this->addColumn('{{%bonuses}}','bonus_type_id',$this->integer(11)->notNull()->after('site_id'));
	}

	public function down()
	{
		$this->dropColumn('{{%bonuses}}','bonus_type_id');
	}
}
