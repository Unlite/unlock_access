<?php

use yii\db\Migration;

/**
 * Class m180222_125232_alter_bonuses_promo_code_change_column_type
 */
class m180222_125232_alter_bonuses_promo_code_change_column_type extends Migration
{

    public function up()
    {
        $this->alterColumn('{{%bonuses}}', 'promo_code', $this->string(255));
    }

    public function down()
    {
        $this->alterColumn('{{%bonuses}}', 'promo_code', $this->string(256));
    }
}
