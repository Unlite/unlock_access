<?php

use yii\db\Migration;

/**
 * Class m180222_125510_add_offer_to_bonuses_table
 */
class m180222_125510_add_offer_to_bonuses_table extends Migration
{
	public function up()
	{
		$this->addColumn('{{%bonuses}}','offer',$this->string(255)->notNull()->after('bonus_type_id'));
	}

	public function down()
	{
		$this->dropColumn('{{%bonuses}}','offer');
	}
}
