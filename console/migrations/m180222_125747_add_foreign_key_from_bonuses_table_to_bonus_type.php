<?php

use yii\db\Migration;

/**
 * Class m180222_125747_add_foreign_key_from_bonuses_table_to_bonus_type
 */
class m180222_125747_add_foreign_key_from_bonuses_table_to_bonus_type extends Migration
{
	public function up()
	{
		$this->truncateTable('{{%bonuses}}');
		$this->addForeignKey('fk_bonuses_bonus_type_id_bonus_type_id','{{%bonuses}}','bonus_type_id','{{%bonus_type}}','id');
	}

	public function down()
	{
		$this->dropForeignKey('fk_bonuses_bonus_type_id_bonus_type_id','{{%bonuses}}');
	}
}
