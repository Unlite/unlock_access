<?php

use yii\db\Migration;

/**
 * Class m180301_104449_delte_offer_column_from_bonuses_table
 */
class m180301_104449_delte_offer_column_from_bonuses_table extends Migration
{
	public function up()
	{
		$this->dropColumn('{{%bonuses}}','offer');
	}

	public function down()
	{
		$this->addColumn('{{%bonuses}}','offer',$this->string(255)->notNull());
	}
}
