<?php

use yii\db\Migration;

/**
 * Handles the creation of table `offer_type`.
 */
class m180301_104639_create_offer_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%offer_type}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(32)->notNull(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%offer_type}}');
    }
}
