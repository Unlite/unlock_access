<?php

use yii\db\Migration;

/**
 * Handles the creation of table `offer_type_bonuses`.
 * Has foreign keys to the tables:
 *
 * - `offer_type`
 * - `bonuses`
 */
class m180301_105306_create_junction_table_for_offer_type_and_bonuses_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%offer_type_bonuses}}', [
            'offer_type_id' => $this->integer(),
            'bonuses_id' => $this->integer(),
            'offer_text' => $this->string(16)->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('pk-offer_type_bonuses', '{{%offer_type_bonuses}}', ['offer_type_id', 'bonuses_id']);

        // creates index for column `offer_type_id`
        $this->createIndex(
            'idx-offer_type_bonuses-offer_type_id',
            '{{%offer_type_bonuses}}',
            'offer_type_id'
        );

        // add foreign key for table `offer_type`
        $this->addForeignKey(
            'fk-offer_type_bonuses-offer_type_id',
            '{{%offer_type_bonuses}}',
            'offer_type_id',
            '{{%offer_type}}',
            'id',
            'CASCADE'
        );

        // creates index for column `bonuses_id`
        $this->createIndex(
            'idx-offer_type_bonuses-bonuses_id',
            '{{%offer_type_bonuses}}',
            'bonuses_id'
        );

        // add foreign key for table `bonuses`
        $this->addForeignKey(
            'fk-offer_type_bonuses-bonuses_id',
            '{{%offer_type_bonuses}}',
            'bonuses_id',
            '{{%bonuses}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%offer_type_bonuses}}');
    }
}
