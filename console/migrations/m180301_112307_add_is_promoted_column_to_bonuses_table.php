<?php

use yii\db\Migration;

/**
 * Handles adding is_promoted to table `bonuses`.
 */
class m180301_112307_add_is_promoted_column_to_bonuses_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%bonuses}}', 'is_promoted', $this->boolean()->notNull());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%bonuses}}', 'is_promoted');
    }
}
