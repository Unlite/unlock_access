<?php

use yii\db\Migration;

/**
 * Handles the creation of table `game_provider_site`.
 * Has foreign keys to the tables:
 *
 * - `game_provider`
 * - `site`
 */
class m180301_121127_create_junction_table_for_game_provider_and_site_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
		
        $this->createTable('{{%game_provider_site}}', [
            'game_provider_id' => $this->integer(),
            'site_id' => $this->integer(),
            'is_auto_created' => $this->boolean()->notNull(),
        ], $tableOptions);

		$this->addPrimaryKey('pk-game_provider_site', '{{%game_provider_site}}', ['game_provider_id', 'site_id']);

        // creates index for column `game_provider_id`
        $this->createIndex(
            'idx-game_provider_site-game_provider_id',
            '{{%game_provider_site}}',
            'game_provider_id'
        );

        // add foreign key for table `game_provider`
        $this->addForeignKey(
            'fk-game_provider_site-game_provider_id',
            '{{%game_provider_site}}',
            'game_provider_id',
            '{{%game_provider}}',
            'id',
            'CASCADE'
        );

        // creates index for column `site_id`
        $this->createIndex(
            'idx-game_provider_site-site_id',
            '{{%game_provider_site}}',
            'site_id'
        );

        // add foreign key for table `site`
        $this->addForeignKey(
            'fk-game_provider_site-site_id',
            '{{%game_provider_site}}',
            'site_id',
            '{{%site}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%game_provider_site}}');
    }
}
