<?php

use yii\db\Migration;

/**
 * Handles the creation of table `custom_list`.
 */
class m180312_135543_create_custom_list_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%custom_list}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'isActive' => $this->integer(1)->notNull()->defaultValue(1),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%custom_list}}');
    }
}
