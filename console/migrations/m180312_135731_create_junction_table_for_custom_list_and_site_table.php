<?php

use yii\db\Migration;

/**
 * Handles the creation of table `custom_list_site`.
 * Has foreign keys to the tables:
 *
 * - `custom_list`
 * - `site`
 */
class m180312_135731_create_junction_table_for_custom_list_and_site_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%custom_list_site}}', [
            'custom_list_id' => $this->integer(),
            'site_id' => $this->integer(),
        ], $tableOptions);
        $this->addPrimaryKey('pk-custom_list_site', '{{%custom_list_site}}', ['custom_list_id', 'site_id']);

        // creates index for column `custom_list_id`
        $this->createIndex(
            'idx-custom_list_site-custom_list_id',
            '{{%custom_list_site}}',
            'custom_list_id'
        );

        // add foreign key for table `custom_list`
        $this->addForeignKey(
            'fk-custom_list_site-custom_list_id',
            '{{%custom_list_site}}',
            'custom_list_id',
            '{{%custom_list}}',
            'id',
            'CASCADE'
        );

        // creates index for column `site_id`
        $this->createIndex(
            'idx-custom_list_site-site_id',
            '{{%custom_list_site}}',
            'site_id'
        );

        // add foreign key for table `site`
        $this->addForeignKey(
            'fk-custom_list_site-site_id',
            '{{%custom_list_site}}',
            'site_id',
            '{{%site}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%custom_list_site}}');
    }
}
