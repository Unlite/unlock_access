<?php

use yii\db\Migration;

/**
 * Handles adding is_promoted to table `game`.
 */
class m180329_141531_add_is_promoted_column_to_game_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('game', 'is_promoted', $this->integer(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('game', 'is_promoted');
    }
}
