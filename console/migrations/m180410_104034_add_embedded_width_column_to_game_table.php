<?php

use yii\db\Migration;

/**
 * Handles adding embedded_width to table `game`.
 */
class m180410_104034_add_embedded_width_column_to_game_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('game', 'embedded_width', $this->integer(11));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('game', 'embedded_width');
    }
}
