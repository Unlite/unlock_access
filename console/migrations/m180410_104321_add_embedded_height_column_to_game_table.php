<?php

use yii\db\Migration;

/**
 * Handles adding embedded_height to table `game`.
 */
class m180410_104321_add_embedded_height_column_to_game_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('game', 'embedded_height', $this->integer(11));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('game', 'embedded_height');
    }
}
