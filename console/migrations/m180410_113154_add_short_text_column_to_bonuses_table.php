<?php

use yii\db\Migration;

/**
 * Handles adding short_text to table `bonuses`.
 */
class m180410_113154_add_short_text_column_to_bonuses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('bonuses', 'short_text', $this->string(255)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('bonuses', 'short_text');
    }
}
