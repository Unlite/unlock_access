<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tag_blog_post`.
 * Has foreign keys to the tables:
 *
 * - `tag`
 * - `blog_post`
 */
class m180410_114855_create_junction_table_for_tag_and_blog_post_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}

        $this->createTable('{{%tag_blog_post}}', [
            'tag_id' => $this->integer(),
            'blog_post_id' => $this->integer(),
        ],$tableOptions);

		$this->addPrimaryKey('pk-tag_blog_post', '{{%tag_blog_post}}', ['tag_id', 'blog_post_id']);

        // creates index for column `tag_id`
        $this->createIndex(
            'idx-tag_blog_post-tag_id',
            '{{%tag_blog_post}}',
            'tag_id'
        );

        // add foreign key for table `tag`
        $this->addForeignKey(
            'fk-tag_blog_post-tag_id',
            '{{%tag_blog_post}}',
            'tag_id',
            'tag',
            'id',
            'CASCADE'
        );

        // creates index for column `blog_post_id`
        $this->createIndex(
            'idx-tag_blog_post-blog_post_id',
            '{{%tag_blog_post}}',
            'blog_post_id'
        );

        // add foreign key for table `blog_post`
        $this->addForeignKey(
            'fk-tag_blog_post-blog_post_id',
            '{{%tag_blog_post}}',
            'blog_post_id',
            'blog_post',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `tag`
        $this->dropForeignKey(
            'fk-tag_blog_post-tag_id',
            '{{%tag_blog_post}}'
        );

        // drops index for column `tag_id`
        $this->dropIndex(
            'idx-tag_blog_post-tag_id',
            '{{%tag_blog_post}}'
        );

        // drops foreign key for table `blog_post`
        $this->dropForeignKey(
            'fk-tag_blog_post-blog_post_id',
            '{{%tag_blog_post}}'
        );

        // drops index for column `blog_post_id`
        $this->dropIndex(
            'idx-tag_blog_post-blog_post_id',
            '{{%tag_blog_post}}'
        );

        $this->dropTable('{{%tag_blog_post}}');
    }
}
