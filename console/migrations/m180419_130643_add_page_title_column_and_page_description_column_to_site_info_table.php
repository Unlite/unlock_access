<?php

use yii\db\Migration;

/**
 * Handles adding page_title_column_and_page_description to table `site_info`.
 */
class m180419_130643_add_page_title_column_and_page_description_column_to_site_info_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('site_info', 'page_title', $this->string(100));
        $this->addColumn('site_info', 'page_description', $this->string(300));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('site_info', 'page_title');
        $this->dropColumn('site_info', 'page_description');
    }
}
