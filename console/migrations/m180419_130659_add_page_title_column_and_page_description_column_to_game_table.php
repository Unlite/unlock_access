<?php

use yii\db\Migration;

/**
 * Handles adding page_title_column_and_page_description to table `game`.
 */
class m180419_130659_add_page_title_column_and_page_description_column_to_game_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('game', 'page_title', $this->string(100));
        $this->addColumn('game', 'page_description', $this->string(300));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('game', 'page_title');
        $this->dropColumn('game', 'page_description');
    }
}
