<?php

use yii\db\Migration;

/**
 * Handles adding page_title_column_and_page_description to table `blog_post`.
 */
class m180419_130711_add_page_title_column_and_page_description_column_to_blog_post_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('blog_post', 'page_title', $this->string(100));
        $this->addColumn('blog_post', 'page_description', $this->string(300));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('blog_post', 'page_title');
        $this->dropColumn('blog_post', 'page_description');
    }
}
