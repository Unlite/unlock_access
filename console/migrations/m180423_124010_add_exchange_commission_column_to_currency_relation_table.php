<?php

use yii\db\Migration;

/**
 * Handles adding exchange_commission to table `currency_relation`.
 */
class m180423_124010_add_exchange_commission_column_to_currency_relation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%currency_relations}}', 'exchange_commission', $this->decimal(4, 3)->notNull()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%currency_relations}}', 'exchange_commission');
    }
}
