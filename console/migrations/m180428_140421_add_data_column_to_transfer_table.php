<?php

use yii\db\Migration;

/**
 * Handles adding data to table `transfer`.
 */
class m180428_140421_add_data_column_to_transfer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%transfer}}', 'data', $this->text());
        $this->dropColumn('{{%transfer}}', 'comment');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%transfer}}', 'comment', $this->string(255));
        $this->dropColumn('{{%transfer}}', 'data');
    }
}
