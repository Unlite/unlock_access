<?php

use yii\db\Migration;

/**
 * Class m180503_162940_update_user_currency_table
 */
class m180503_162940_update_user_currency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%user_currency}}', 'amount', $this->decimal(19, 6)->notNull()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%user_currency}}', 'amount', $this->decimal(19, 6)->notNull());
    }
}
