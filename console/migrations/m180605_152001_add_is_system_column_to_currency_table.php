<?php

use yii\db\Migration;

/**
 * Handles adding is_system to table `currency`.
 */
class m180605_152001_add_is_system_column_to_currency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%currency}}', 'is_system', $this->boolean()->notNull()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%currency}}', 'is_system');
    }
}
