<?php

use yii\db\Migration;

/**
 * Handles adding review to table `site_info`.
 */
class m180722_131346_add_review_column_to_site_info_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('site_info', 'review', $this->string(2000));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('site_info', 'review');
    }
}
