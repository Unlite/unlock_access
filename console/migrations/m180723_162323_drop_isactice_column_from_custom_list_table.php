<?php

use yii\db\Migration;

/**
 * Handles dropping isactice from table `custom_list`.
 */
class m180723_162323_drop_isactice_column_from_custom_list_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('custom_list', 'isActive');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('custom_list', 'isActive', $this->integer(1));
    }
}
