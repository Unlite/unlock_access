<?php

use yii\db\Migration;

/**
 * Handles adding type to table `custom_list`.
 */
class m180723_162704_add_type_column_to_custom_list_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('custom_list', 'type', "ENUM('disabled','top','sidebar','promoted') DEFAULT 'sidebar'");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('custom_list', 'type');
    }
}
