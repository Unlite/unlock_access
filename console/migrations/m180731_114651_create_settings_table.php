<?php

use yii\db\Migration;

/**
 * Handles the creation of table `settings`.
 */
class m180731_114651_create_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'key' => $this->string(55)->notNull(),
            'group' => $this->string(55)->notNull(),
            'value' => $this->text()->notNull(),
            'serialized' => $this->integer(1)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('settings');
    }
}
