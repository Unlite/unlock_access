<?php

use yii\db\Migration;

/**
 * Handles adding type to table `game_category`.
 */
class m180801_160332_add_type_column_to_game_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->addColumn('game_category', 'type', "ENUM('disabled','sidebar','top') DEFAULT 'sidebar'");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropColumn('game_category', 'type');
	}
}
