<?php

use yii\db\Migration;

/**
 * Handles adding review_updated_at to table `site_info`.
 */
class m180813_151538_add_review_updated_at_column_to_site_info_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('site_info', 'review_updated_at', $this->integer(10));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('site_info', 'review_updated_at');
    }
}
