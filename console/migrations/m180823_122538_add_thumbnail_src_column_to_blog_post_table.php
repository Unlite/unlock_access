<?php

use yii\db\Migration;

/**
 * Handles adding thumbnail_src to table `blog_post`.
 */
class m180823_122538_add_thumbnail_src_column_to_blog_post_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('blog_post', 'thumbnail_src', $this->string(1024));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('blog_post', 'thumbnail_src');
    }
}
