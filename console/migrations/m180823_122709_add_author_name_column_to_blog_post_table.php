<?php

use yii\db\Migration;

/**
 * Handles adding author_name to table `blog_post`.
 */
class m180823_122709_add_author_name_column_to_blog_post_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('blog_post', 'author_name', $this->string(64));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('blog_post', 'author_name');
    }
}
