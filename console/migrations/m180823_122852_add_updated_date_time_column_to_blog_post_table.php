<?php

use yii\db\Migration;

/**
 * Handles adding updated_date_time to table `blog_post`.
 */
class m180823_122852_add_updated_date_time_column_to_blog_post_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('blog_post', 'updated_date_time', $this->integer(10)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('blog_post', 'updated_date_time');
    }
}
