<?php

use yii\db\Migration;

/**
 * Handles dropping is_promoted from table `game`.
 */
class m181001_113344_drop_is_promoted_column_from_game_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('game', 'is_promoted');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('game', 'is_promoted', $this->integer(1));
    }
}
