<?php

use yii\db\Migration;

/**
 * Handles adding is_promoted to table `site_game`.
 */
class m181001_113701_add_is_promoted_column_to_site_game_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('site_game', 'is_promoted', $this->integer(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('site_game', 'is_promoted');
    }
}
