<?php

use yii\db\Migration;

/**
 * Handles the creation of table `site_blog_post`.
 * Has foreign keys to the tables:
 *
 * - `site`
 * - `blog_post`
 */
class m181008_134020_create_junction_table_for_site_and_blog_post_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('site_blog_post', [
            'site_id' => $this->integer(),
            'blog_post_id' => $this->integer(),
            'PRIMARY KEY(site_id, blog_post_id)',
        ]);

        // creates index for column `site_id`
        $this->createIndex(
            'idx-site_blog_post-site_id',
            'site_blog_post',
            'site_id'
        );

        // add foreign key for table `site`
        $this->addForeignKey(
            'fk-site_blog_post-site_id',
            'site_blog_post',
            'site_id',
            'site',
            'id',
            'CASCADE'
        );

        // creates index for column `blog_post_id`
        $this->createIndex(
            'idx-site_blog_post-blog_post_id',
            'site_blog_post',
            'blog_post_id'
        );

        // add foreign key for table `blog_post`
        $this->addForeignKey(
            'fk-site_blog_post-blog_post_id',
            'site_blog_post',
            'blog_post_id',
            'blog_post',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `site`
        $this->dropForeignKey(
            'fk-site_blog_post-site_id',
            'site_blog_post'
        );

        // drops index for column `site_id`
        $this->dropIndex(
            'idx-site_blog_post-site_id',
            'site_blog_post'
        );

        // drops foreign key for table `blog_post`
        $this->dropForeignKey(
            'fk-site_blog_post-blog_post_id',
            'site_blog_post'
        );

        // drops index for column `blog_post_id`
        $this->dropIndex(
            'idx-site_blog_post-blog_post_id',
            'site_blog_post'
        );

        $this->dropTable('site_blog_post');
    }
}
