<?php

use yii\db\Migration;
use yii\db\Query;
use yii\helpers\Inflector;

/**
 * Handles adding slug to table `site`.
 */
class m181018_111323_add_slug_column_to_site_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        //Add slug attribute
        $this->addColumn('{{%site}}', 'slug', $this->string(128)->notNull()->defaultValue('empty'));

        //Find all site names
        $query = new Query;
        $sites = $query->select(['id', 'name'])->from('{{%site}}');
        //Convert value of name attribute and save to slug attribute
        Inflector::$transliterator = 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;';
        foreach ($sites->each() as $site) {
            $this->db->createCommand()->update('{{%site}}', ['slug' => Inflector::slug($site['name']) ?? 'empty'], ['id' => $site['id']])->execute();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%site}}', 'slug');
    }
}
