<?php

use yii\db\Migration;
use yii\db\Query;
use yii\helpers\Inflector;

/**
 * Handles adding slug to table `game`.
 */
class m181018_194411_add_slug_column_to_game_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%game}}', 'slug', $this->string(128)->notNull()->defaultValue('empty'));

        //Find all site names
        $query = (new Query())->select(['id', 'name'])->from('{{%game}}');
        //Convert value of name attribute and save to slug attribute
        Inflector::$transliterator = 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;';
        foreach ($query->each() as $model) {
            $this->db->createCommand()->update('{{%game}}', ['slug' => Inflector::slug($model['name']) ?? 'empty'], ['id' => $model['id']])->execute();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%game}}', 'slug');
    }
}
