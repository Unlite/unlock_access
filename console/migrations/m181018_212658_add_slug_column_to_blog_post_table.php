<?php

use yii\db\Migration;
use yii\db\Query;
use yii\helpers\Inflector;

/**
 * Handles adding slug to table `blog_post`.
 */
class m181018_212658_add_slug_column_to_blog_post_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%blog_post}}', 'slug', $this->string(512)->notNull()->defaultValue('empty'));

        //Find all site names
        $query = (new Query())->select(['id', 'title'])->from('{{%blog_post}}');
        //Convert value of name attribute and save to slug attribute
        Inflector::$transliterator = 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;';
        foreach ($query->each() as $model) {
            $this->db->createCommand()->update('{{%blog_post}}', ['slug' => Inflector::slug($model['title']) ?? 'empty'], ['id' => $model['id']])->execute();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%blog_post}}', 'slug');
    }
}
