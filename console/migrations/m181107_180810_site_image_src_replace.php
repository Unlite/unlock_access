<?php

use yii\db\Migration;

/**
 * Class m181107_180810_site_image_src_replace
 */
class m181107_180810_site_image_src_replace extends Migration
{

	// CDN folder structure change, need to be changed at the ftp to.
    public function up()
    {
		$this->execute("UPDATE site_image SET src = REPLACE(src,'/casino/logo-sqr/','/casino/sqr/')");
		$this->execute("UPDATE site_image SET src = REPLACE(src,'/casino/logo-rect/','/casino/rect/')");
    }

    public function down()
    {
		$this->execute("UPDATE site_image SET src = REPLACE(src,'/casino/sqr/','/casino/logo-sqr/')");
		$this->execute("UPDATE site_image SET src = REPLACE(src,'/casino/rect/','/casino/logo-rect/')");
    }
}
