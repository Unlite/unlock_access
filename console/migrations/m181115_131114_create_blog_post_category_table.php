<?php

use yii\db\Migration;

/**
 * Handles the creation of table `blog_post_category`.
 */
class m181115_131114_create_blog_post_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
        $this->createTable('blog_post_category', [
            'id' => $this->primaryKey(),
            'name' => $this->string(64)->notNull(),
        ],$tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('blog_post_category');
    }
}
