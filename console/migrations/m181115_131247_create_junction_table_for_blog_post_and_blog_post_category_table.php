<?php

use yii\db\Migration;

/**
 * Handles the creation of table `blog_post_blog_post_category`.
 * Has foreign keys to the tables:
 *
 * - `blog_post`
 * - `blog_post_category`
 */
class m181115_131247_create_junction_table_for_blog_post_and_blog_post_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('blog_post_blog_post_category', [
            'blog_post_id' => $this->integer(),
            'blog_post_category_id' => $this->integer(),
            'PRIMARY KEY(blog_post_id, blog_post_category_id)',
        ]);

        // creates index for column `blog_post_id`
        $this->createIndex(
            'idx-blog_post_blog_post_category-blog_post_id',
            'blog_post_blog_post_category',
            'blog_post_id'
        );

        // add foreign key for table `blog_post`
        $this->addForeignKey(
            'fk-blog_post_blog_post_category-blog_post_id',
            'blog_post_blog_post_category',
            'blog_post_id',
            'blog_post',
            'id',
            'CASCADE'
        );

        // creates index for column `blog_post_category_id`
        $this->createIndex(
            'idx-blog_post_blog_post_category-blog_post_category_id',
            'blog_post_blog_post_category',
            'blog_post_category_id'
        );

        // add foreign key for table `blog_post_category`
        $this->addForeignKey(
            'fk-blog_post_blog_post_category-blog_post_category_id',
            'blog_post_blog_post_category',
            'blog_post_category_id',
            'blog_post_category',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `blog_post`
        $this->dropForeignKey(
            'fk-blog_post_blog_post_category-blog_post_id',
            'blog_post_blog_post_category'
        );

        // drops index for column `blog_post_id`
        $this->dropIndex(
            'idx-blog_post_blog_post_category-blog_post_id',
            'blog_post_blog_post_category'
        );

        // drops foreign key for table `blog_post_category`
        $this->dropForeignKey(
            'fk-blog_post_blog_post_category-blog_post_category_id',
            'blog_post_blog_post_category'
        );

        // drops index for column `blog_post_category_id`
        $this->dropIndex(
            'idx-blog_post_blog_post_category-blog_post_category_id',
            'blog_post_blog_post_category'
        );

        $this->dropTable('blog_post_blog_post_category');
    }
}
