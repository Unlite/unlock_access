<?php

use yii\db\Migration;

/**
 * Class m181205_184106_blog_thumbnail_src_replace
 */
class m181205_184106_blog_thumbnail_src_replace extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->execute("UPDATE blog_post SET thumbnail_src = REPLACE(thumbnail_src,'/casino/logo-sqr/','/casino/sqr/')");
		$this->execute("UPDATE blog_post SET thumbnail_src = REPLACE(thumbnail_src,'/casino/logo-rect/','/casino/rect/')");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->execute("UPDATE blog_post SET thumbnail_src = REPLACE(thumbnail_src,'/casino/sqr/','/casino/logo-sqr/')");
		$this->execute("UPDATE blog_post SET thumbnail_src = REPLACE(thumbnail_src,'/casino/rect/','/casino/logo-rect/')");
    }

}
