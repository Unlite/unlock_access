<?php

use yii\db\Migration;

/**
 * Class m181217_173508_alter_table_site_info_column_review
 */
class m181217_173508_alter_table_site_info_column_review extends Migration
{
	public function up()
	{
		$this->alterColumn('{{%site_info}}','review',$this->string(15000));
	}

	public function down()
	{
		$this->alterColumn('{{%site_info}}','review',$this->string(2000));
	}
}
