<?php

use yii\db\Migration;

/**
 * Handles adding status to table `custom_list`.
 */
class m190306_134501_add_status_column_to_custom_list_game_provider_licensor_game_game_category_bonuses_bonus_type_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->addColumn('{{%custom_list}}', 'status', "ENUM('active','paused') NOT NULL DEFAULT 'active'");
		$this->addColumn('{{%game_provider}}', 'status', "ENUM('active','paused') NOT NULL DEFAULT 'active'");
		$this->addColumn('{{%licensor}}', 'status', "ENUM('active','paused') NOT NULL DEFAULT 'active'");
		$this->addColumn('{{%game}}', 'status', "ENUM('active','paused') NOT NULL DEFAULT 'active'");
		$this->addColumn('{{%game_category}}', 'status', "ENUM('active','paused') NOT NULL DEFAULT 'active'");
		$this->addColumn('{{%bonuses}}', 'status', "ENUM('active','paused') NOT NULL DEFAULT 'active'");
		$this->addColumn('{{%bonus_type}}', 'status', "ENUM('active','paused') NOT NULL DEFAULT 'active'");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

		$this->dropColumn('{{%custom_list}}', 'status');
		$this->dropColumn('{{%game_provider}}', 'status');
		$this->dropColumn('{{%licensor}}', 'status');
		$this->dropColumn('{{%game}}', 'status');
		$this->dropColumn('{{%game_category}}', 'status');
		$this->dropColumn('{{%bonuses}}', 'status');
		$this->dropColumn('{{%bonus_type}}', 'status');
    }
}
