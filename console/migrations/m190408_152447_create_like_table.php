<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%like}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 * - `{{%review}}`
 */
class m190408_152447_create_like_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
        $this->createTable('{{%like}}', [
            'user_id' => $this->integer()->notNull(),
            'review_id' => $this->integer()->notNull(),
			'PRIMARY KEY(user_id, review_id)'
        ],$tableOptions);

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-like-user_id}}',
            '{{%like}}',
            'user_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-like-user_id}}',
            '{{%like}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `review_id`
        $this->createIndex(
            '{{%idx-like-review_id}}',
            '{{%like}}',
            'review_id'
        );

        // add foreign key for table `{{%review}}`
        $this->addForeignKey(
            '{{%fk-like-review_id}}',
            '{{%like}}',
            'review_id',
            '{{%review}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-like-user_id}}',
            '{{%like}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-like-user_id}}',
            '{{%like}}'
        );

        // drops foreign key for table `{{%review}}`
        $this->dropForeignKey(
            '{{%fk-like-review_id}}',
            '{{%like}}'
        );

        // drops index for column `review_id`
        $this->dropIndex(
            '{{%idx-like-review_id}}',
            '{{%like}}'
        );

        $this->dropTable('{{%like}}');
    }
}
