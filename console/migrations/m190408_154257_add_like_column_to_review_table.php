<?php

use yii\db\Migration;

/**
 * Handles adding like to table `{{%review}}`.
 */
class m190408_154257_add_like_column_to_review_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%review}}', 'like', $this->integer(11)->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%review}}', 'like');
    }
}
