<?php

use yii\db\Migration;

/**
 * Class m190408_163441_change_description_column_in_site_info_table
 */
class m190408_163441_change_description_column_in_site_info_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->alterColumn('site_info','description',$this->string(255)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->alterColumn('site_info','description',$this->string(255)->notNull());
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190408_163441_change_description_column_in_site_info_table cannot be reverted.\n";

        return false;
    }
    */
}
