<?php

use yii\db\Migration;

/**
 * Class m190415_140645_update_type_column_in_transfer_table
 */
class m190415_140645_update_type_column_in_transfer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->alterColumn('transfer','type',"ENUM('exchange', 'cashback', 'withdraw', 'other') NOT NULL DEFAULT 'other'");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->alterColumn('transfer','type',"ENUM('exchange', 'cashback', 'other') NOT NULL DEFAULT 'other'");
    }

}
