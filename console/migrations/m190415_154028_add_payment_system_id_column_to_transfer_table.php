<?php

use yii\db\Migration;

/**
 * Handles adding payment_system_id to table `{{%transfer}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%payment_system}}`
 */
class m190415_154028_add_payment_system_id_column_to_transfer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%transfer}}', 'payment_system_id', $this->integer(11)->null()->after('parent_id'));

        // creates index for column `payment_system_id`
        $this->createIndex(
            '{{%idx-transfer-payment_system_id}}',
            '{{%transfer}}',
            'payment_system_id'
        );

        // add foreign key for table `{{%payment_system}}`
        $this->addForeignKey(
            '{{%fk-transfer-payment_system_id}}',
            '{{%transfer}}',
            'payment_system_id',
            '{{%payment_system}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%payment_system}}`
        $this->dropForeignKey(
            '{{%fk-transfer-payment_system_id}}',
            '{{%transfer}}'
        );

        // drops index for column `payment_system_id`
        $this->dropIndex(
            '{{%idx-transfer-payment_system_id}}',
            '{{%transfer}}'
        );

        $this->dropColumn('{{%transfer}}', 'payment_system_id');
    }
}
