<?php

use yii\db\Migration;

/**
 * Handles adding status to table `{{%payment_system}}`.
 */
class m190423_122249_add_status_column_to_payment_system_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->addColumn('{{%payment_system}}', 'status', "ENUM('enabled','disabled') DEFAULT 'disabled'");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropColumn('{{%payment_system}}', 'status');
    }
}
