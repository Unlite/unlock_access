<?php

use yii\db\Migration;

/**
 * Handles adding description to table `{{%payment_system}}`.
 */
class m190423_124848_add_description_column_to_payment_system_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%payment_system}}', 'description', $this->string(1024)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%payment_system}}', 'description');
    }
}
