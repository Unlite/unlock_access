<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%translate}}`.
 */
class m190513_110737_create_translate_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
        $this->createTable('{{%translate}}', [
            'id' => $this->primaryKey(),
            'table_id' => $this->integer(11)->notNull(),
            'table' => $this->string(64)->notNull(),
            'key' => $this->string(255)->notNull(),
            'value' => $this->text(),
            'serialized' => $this->integer(1)->defaultValue(0),
        ],$tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%translate}}');
    }
}
