<?php

use yii\db\Migration;

/**
 * Handles adding geo_code to table `{{%translate}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%geo}}`
 */
class m190513_123713_add_geo_code_column_to_translate_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%translate}}', 'geo_code',$this->string(5)->after('id'));

        // creates index for column `geo_code`
        $this->createIndex(
            '{{%idx-translate-geo_code}}',
            '{{%translate}}',
            'geo_code'
        );

        // add foreign key for table `{{%geo}}`
        $this->addForeignKey(
            '{{%fk-translate-geo_code}}',
            '{{%translate}}',
            'geo_code',
            '{{%geo}}',
            'code',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%geo}}`
        $this->dropForeignKey(
            '{{%fk-translate-geo_code}}',
            '{{%translate}}'
        );

        // drops index for column `geo_code`
        $this->dropIndex(
            '{{%idx-translate-geo_code}}',
            '{{%translate}}'
        );

        $this->dropColumn('{{%translate}}', 'geo_code');
    }
}
