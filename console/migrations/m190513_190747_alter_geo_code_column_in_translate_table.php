<?php

use yii\db\Migration;

/**
 * Class m190513_190747_alter_geo_code_column_in_translate_table
 */
class m190513_190747_alter_geo_code_column_in_translate_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    	$this->truncateTable('translate');
		// drops foreign key for table `{{%geo}}`
		$this->dropForeignKey(
			'{{%fk-translate-geo_code}}',
			'{{%translate}}'
		);
		$this->dropColumn('{{%translate}}','geo_code');

		$this->addColumn('{{%translate}}','locale_code',$this->string(5)->notNull()->after('id'));
		// add foreign key for table `{{%geo}}`
		$this->addForeignKey(
			'{{%fk-translate-locale_code}}',
			'{{%translate}}',
			'locale_code',
			'{{%locale}}',
			'code',
			'CASCADE'
		);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->truncateTable('translate');

		// drops foreign key for table `{{%geo}}`
		$this->dropForeignKey(
			'{{%fk-translate-locale_code}}',
			'{{%translate}}'
		);
		$this->dropColumn('{{%translate}}','locale_code');

		$this->addColumn('{{%translate}}','geo_code',$this->string(5)->notNull()->after('id'));
		// add foreign key for table `{{%geo}}`
		$this->addForeignKey(
			'{{%fk-translate-geo_code}}',
			'{{%translate}}',
			'geo_code',
			'{{%geo}}',
			'code',
			'CASCADE'
		);

    }

}
