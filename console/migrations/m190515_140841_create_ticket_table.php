<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ticket}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 * - `{{%locale}}`
 */
class m190515_140841_create_ticket_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
        $this->createTable('{{%ticket}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(11)->notNull(),
            'user_locale' => $this->string(5)->notNull(),
			'status' => "ENUM('new','opened','closed') NOT NULL",
            'title' => $this->string(255)->notNull(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
        ],$tableOptions);

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-ticket-user_id}}',
            '{{%ticket}}',
            'user_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-ticket-user_id}}',
            '{{%ticket}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `user_locale`
        $this->createIndex(
            '{{%idx-ticket-user_locale}}',
            '{{%ticket}}',
            'user_locale'
        );

        // add foreign key for table `{{%locale}}`
        $this->addForeignKey(
            '{{%fk-ticket-user_locale}}',
            '{{%ticket}}',
            'user_locale',
            '{{%locale}}',
            'code',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-ticket-user_id}}',
            '{{%ticket}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-ticket-user_id}}',
            '{{%ticket}}'
        );

        // drops foreign key for table `{{%locale}}`
        $this->dropForeignKey(
            '{{%fk-ticket-user_locale}}',
            '{{%ticket}}'
        );

        // drops index for column `user_locale`
        $this->dropIndex(
            '{{%idx-ticket-user_locale}}',
            '{{%ticket}}'
        );

        $this->dropTable('{{%ticket}}');
    }
}
