<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ticket_message}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%ticket}}`
 * - `{{%user}}`
 */
class m190516_112641_create_ticket_message_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ticket_message}}', [
            'id' => $this->primaryKey(),
            'ticket_id' => $this->integer(11)->notNull(),
            'user_id' => $this->integer(11)->notNull(),
			'status' => "ENUM('new','read') NOT NULL",
            'message' => $this->text()->notNull(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
        ]);

        // creates index for column `ticket_id`
        $this->createIndex(
            '{{%idx-ticket_message-ticket_id}}',
            '{{%ticket_message}}',
            'ticket_id'
        );

        // add foreign key for table `{{%ticket}}`
        $this->addForeignKey(
            '{{%fk-ticket_message-ticket_id}}',
            '{{%ticket_message}}',
            'ticket_id',
            '{{%ticket}}',
            'id',
            'CASCADE'
        );

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-ticket_message-user_id}}',
            '{{%ticket_message}}',
            'user_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-ticket_message-user_id}}',
            '{{%ticket_message}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%ticket}}`
        $this->dropForeignKey(
            '{{%fk-ticket_message-ticket_id}}',
            '{{%ticket_message}}'
        );

        // drops index for column `ticket_id`
        $this->dropIndex(
            '{{%idx-ticket_message-ticket_id}}',
            '{{%ticket_message}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-ticket_message-user_id}}',
            '{{%ticket_message}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-ticket_message-user_id}}',
            '{{%ticket_message}}'
        );

        $this->dropTable('{{%ticket_message}}');
    }
}
