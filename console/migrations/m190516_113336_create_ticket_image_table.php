<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ticket_image}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%ticket_message}}`
 */
class m190516_113336_create_ticket_image_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ticket_image}}', [
            'id' => $this->primaryKey(),
            'ticket_message_id' => $this->integer(11)->notNull(),
            'src' => $this->text()->notNull(),
        ]);

        // creates index for column `ticket_message_id`
        $this->createIndex(
            '{{%idx-ticket_image-ticket_message_id}}',
            '{{%ticket_image}}',
            'ticket_message_id'
        );

        // add foreign key for table `{{%ticket_message}}`
        $this->addForeignKey(
            '{{%fk-ticket_image-ticket_message_id}}',
            '{{%ticket_image}}',
            'ticket_message_id',
            '{{%ticket_message}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%ticket_message}}`
        $this->dropForeignKey(
            '{{%fk-ticket_image-ticket_message_id}}',
            '{{%ticket_image}}'
        );

        // drops index for column `ticket_message_id`
        $this->dropIndex(
            '{{%idx-ticket_image-ticket_message_id}}',
            '{{%ticket_image}}'
        );

        $this->dropTable('{{%ticket_image}}');
    }
}
