<?php

use yii\db\Migration;

/**
 * Class m190519_231159_user_fill_empty_releations
 */
class m190519_231159_user_fill_empty_relations extends Migration
{

    public function up()
    {
        $users = \common\models\User::find()->all();
        $isConsole = \Yii::$app instanceof \yii\console\Application;//Check application.
        foreach($users as $user){
            if($user->client == null){

                //Client
                $client = $isConsole ? null : \common\models\Client::getClientByCookies();//If console, skip cookies
                if($client && $client->secure && !$client->user_id){
                    $client->user_id = $user->id;
                    $client->save();
                }else{
                    \common\models\Client::newRecord([
                        'user_id' => $user->id,
                        'ip' => $isConsole ? '' : \Yii::$app->request->userIP,
                        'user_agent' => $isConsole ? '' : \Yii::$app->request->userAgent,
                    ], !$isConsole);
                }
            }
            if($user->userCurrencies == null || empty($user->userCurrencies)){
                $userCurrency = new \common\models\UserCurrency(['user_id' => $user->id,'currency_code'=>'usd','amount'=>0]);
                $userCurrency->save(false);
            }
        }
    }

    public function down()
    {
        echo "m190519_231159_user_fill_empty_relations cannot be reverted.\n";

        return false;
    }
}
