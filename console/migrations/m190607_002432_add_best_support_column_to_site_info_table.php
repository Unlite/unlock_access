<?php

use yii\db\Migration;

/**
 * Handles adding best_support to table `{{%site_info}}`.
 */
class m190607_002432_add_best_support_column_to_site_info_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%site_info}}', 'best_support', $this->integer(1)->after('is_mobile_friendly'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%site_info}}', 'best_support');
    }
}
