<?php

use yii\db\Migration;

/**
 * Handles adding status to table `{{%locale}}`.
 */
class m190614_165613_add_status_column_to_locale_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%locale}}', 'status', "ENUM('enabled','disabled') DEFAULT 'disabled'");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%locale}}', 'status');
    }
}
