<?php

use yii\db\Migration;

/**
 * Class m190716_010502_add_bets10_cashback
 */
class m190716_010502_add_bets10_cashback extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->insert('cashback',[
			'id' => '0',
			'name' => 'Affiliya:Bets10',
			'object' => 'common\\components\\cashback\\Bets10',
			'client_status' => 'active',
			'cron_status' => 'active',
			'percent' => '1.000',
			'user_percent' => '1.000',
		]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    	$this->delete('cashback',['id'=>0]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190716_010502_add_bets10_cashback cannot be reverted.\n";

        return false;
    }
    */
}
