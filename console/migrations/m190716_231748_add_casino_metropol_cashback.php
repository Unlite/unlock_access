<?php

use yii\db\Migration;

/**
 * Class m190716_231748_add_casino_metropol_cashback
 */
class m190716_231748_add_casino_metropol_cashback extends Migration
{
	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->insert('cashback',[
			'id' => '1',
			'name' => 'Affiliya:CasinoMetropol',
			'object' => 'common\\components\\cashback\\CasinoMetropol',
			'client_status' => 'active',
			'cron_status' => 'active',
			'percent' => '1.000',
			'user_percent' => '1.000',
		]);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->delete('cashback',['id'=>1]);
	}

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190716_231748_add_casino_metropol_cashback cannot be reverted.\n";

        return false;
    }
    */
}
