<?php

use yii\db\Migration;

/**
 * Class m190716_231801_add_casino_maxi_cashback
 */
class m190716_231801_add_casino_maxi_cashback extends Migration
{
	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->insert('cashback',[
			'id' => '2',
			'name' => 'Affiliya:CasinoMaxi',
			'object' => 'common\\components\\cashback\\CasinoMaxi',
			'client_status' => 'active',
			'cron_status' => 'active',
			'percent' => '1.000',
			'user_percent' => '1.000',
		]);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->delete('cashback',['id'=>2]);
	}
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190716_231801_add_casino_maxi_cashback cannot be reverted.\n";

        return false;
    }
    */
}
