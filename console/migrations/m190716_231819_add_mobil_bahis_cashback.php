<?php

use yii\db\Migration;

/**
 * Class m190716_231819_add_mobil_bahis_cashback
 */
class m190716_231819_add_mobil_bahis_cashback extends Migration
{
	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->insert('cashback',[
			'id' => '3',
			'name' => 'Affiliya:MobilBahis',
			'object' => 'common\\components\\cashback\\MobilBahis',
			'client_status' => 'active',
			'cron_status' => 'active',
			'percent' => '1.000',
			'user_percent' => '1.000',
		]);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->delete('cashback',['id'=>3]);
	}

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190716_231819_add_mobil_bahis_cashback cannot be reverted.\n";

        return false;
    }
    */
}
