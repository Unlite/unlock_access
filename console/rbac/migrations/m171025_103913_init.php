<?php

use yii2mod\rbac\migrations\Migration;

class m171025_103913_init extends Migration
{
    public function safeUp()
    {
        $role = $this->findRole('admin');
        if ($role) {
            $this->updateRole($role, 'admin has all available permissions.');
        } else {
            $role = $this->createRole('admin', 'admin has all available permissions.');
        }

        $permission = $this->findPermission('/*');
        if ($permission) {
            $this->updatePermission($permission, 'Global permission to backend. by migration');
        } else {
            $permission = $this->createPermission('/*', 'Global permission to backend. by migration');
        }

        $this->addChild($role, $permission);
    }

    public function safeDown()
    {
        if ($this->findPermission('/*')) $this->removePermission('/*');
        if ($this->findRole('admin')) $this->removeRole('admin');
    }
}