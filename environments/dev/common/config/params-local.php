<?php
return [
    'cookieValidationKey' => '',
    'cookieDomain' => '.unblock.test',
    'backendHostInfo' => 'http://admin.unblock.test',
    'frontendHostInfo' => 'http://vpn.unblock.test',
    'portalHostInfo' => 'http://unblock.test',
];
