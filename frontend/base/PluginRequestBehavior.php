<?php


namespace frontend\base;

use common\models\Client;
use Yii;
use yii\base\Application;
use yii\base\BootstrapInterface;

class PluginRequestBehavior implements BootstrapInterface
{

    /**
     * Bootstrap method to be called during application bootstrap stage.
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        if ($userExtID = $app->request->get('ueid')) {
            $app->response->cookies->add(new \yii\web\Cookie([
                'name' => 'client_ext_id',
                'value' => $userExtID,
                'expire' => time() + 30 * 24 * 60 * 60,
            ]));
        }
        $clientToken = $app->request->get('uid', Yii::$app->request->cookies->getValue('client_token', false));
        $client = Client::getClientByCookies() ?: Client::getClientByUser() ?: Client::getClient($clientToken);
        if ($client) {
            $client->extension_id = $app->request->cookies->getValue('client_ext_id', Yii::$app->params['extension_id']);
            $client->ip = Yii::$app->request->userIP;
            $client->user_agent = Yii::$app->request->userAgent;
            $client->save();
            $client->touch('last_online_at');
            if($client->secure){
                Client::addClientCookies($client);
            }elseif ($client->token != $clientToken){
                Client::addClientCookies($clientToken);
            }
        }
    }
}