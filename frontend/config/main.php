<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
        'frontend\base\PluginRequestBehavior'
    ],
    'controllerNamespace' => 'frontend\controllers',
    'defaultRoute' => 'main',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'cookieValidationKey' => $params['cookieValidationKey'],
            'as sypexGeo' => [
                'class' => 'omnilight\sypexgeo\GeoBehavior',
                'sypexGeo' => [
                    'database' => '@common/data/sypexgeo/SxGeo_2017_07_13.dat',
                ]
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => [
                'name' => '_identity',
                'httpOnly' => true,
                'domain' => $params['cookieDomain']
            ],
            'loginUrl' => ['user/login']
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => '_session',
            'cookieParams' => [
                'domain' => $params['cookieDomain'],
                'httpOnly' => true,
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'main/error',
        ],
        'backendUrlManager' => require __DIR__ . '/url-manager.php',
        'frontendUrlManager' => require __DIR__ . '/../../frontend/config/url-manager.php',
        'portalUrlManager' => require __DIR__ . '/../../portal/config/url-manager.php',
        'urlManager' => function () {
            return Yii::$app->get('frontendUrlManager');
        },
		'i18n' => [
			'translations' => [
				'front' => [
					'class' => 'yii\i18n\PhpMessageSource',
					'basePath' => '@app/messages',
					'sourceLanguage' => 'en-GB',
				],
			],
		],
        'geoIP' => [
            'class' => 'omnilight\sypexgeo\SypexGeo',
            'database' => '@common/data/sypexgeo/SxGeo_2017_07_13.dat',
        ]
    ],
    'params' => $params,
    'modules' => [
        'api' => [
            'class' => 'frontend\modules\api\Module',
        ],
    ]
];
