<?php

/** @var array $params */

return [
    'class' => \yii\web\UrlManager::class,
    'hostInfo' => $params['frontendHostInfo'],
    'baseUrl' => '',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' => [
        'profile' => 'profile/dashboard/index',
        '<_a:login|logout|ajax-login|ajax-logout>' => 'user/<_a>',
        'away' => 'away/default/index',
    ],
];