<?php

namespace frontend\controllers;

use common\models\Domain;
use common\models\Link;
use common\models\Proxy;
use common\modules\away\link\MacrosLink;
use frontend\models\forms\UrlForm;
use frontend\models\search\SiteSearch;
use frontend\models\Site;
use Yii;
use yii\base\DynamicModel;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class SiteController extends \yii\web\Controller
{
    /**
     * @return string
     */
    public function actionCasino()
    {
        $userCountry = Yii::$app->request->getCountry() ?: 'gb';

        $searchModel = new SiteSearch();
        $dataProvider = $searchModel->search(ArrayHelper::merge(Yii::$app->request->queryParams, ['SiteSearch' => ['type' => Site::TYPE_CASINO, 'geo_code' => $userCountry]]));
        if (!$dataProvider->count) {
            $dataProvider = $searchModel->search(ArrayHelper::merge(Yii::$app->request->queryParams, ['SiteSearch' => ['type' => Site::TYPE_CASINO, 'geo_code' => 'gb']]));
        }
        if(Yii::$app->request->isAjax){
            return $this->renderAjax('_listView', [
                'dataProvider' => $dataProvider,
            ]);
        }
        return $this->render('casino', [
            'dataProvider' => $dataProvider,
            'geo_code' => $userCountry,
            'urlForm' => new UrlForm(),
        ]);
    }

    public function actionAccess($id = null, $domain = null)
    {
        if (!$id && !$domain) throw new NotFoundHttpException('The requested page does not exist.');

        $extension_id = Yii::$app->request->cookies->getValue('client_ext_id', Yii::$app->params['extension_id']);

        $response = ['list' => [], 'redirect_url' => null];
        $sites = [];
        if ($id) {
            $site = $this->findModel($id);
            $sites[] = $site;
            if (in_array($site->type, [Site::TYPE_BETTING, Site::TYPE_POKER, Site::TYPE_CASINO])) {
                $sites = ArrayHelper::merge($sites, Site::findAll(['type' => Site::TYPE_HIDDEN]));
            }
            foreach ($sites as $site) {
                /** @var Site $site */
                foreach ($site->domains as $domain) {
                    /** @var Domain $domain */
                    if ($domain->proxy_pattern && $domain->proxies) {
                        if ($domain->type == Domain::TYPE_MAIN && !$response['redirect_url']) $response['redirect_url'] = (string)Yii::createObject(MacrosLink::class, [$domain->direct_url]);
                        $domain_unblock_array = [
                            'name' => $domain->domain,
                            'type' => $site->type,
                            'pattern' => $domain->proxy_pattern,
                            'proxy' => $domain->getProxies()
                                ->select(["CONCAT(host,':', port) as address", "login", "password"])
                                ->where(['status' => Proxy::STATUS_ACTIVE])
                                ->asArray()->all(),
                        ];
                        //$domain_unblock_array['proxy'] = ArrayHelper::getColumn($domain->proxies, 'fullAddress');
                        $response['list'][] = $domain_unblock_array;
                    }
                }
            }
        } elseif ($domain) {
            $response['redirect_url'] = 'http://' . $domain . '/';
            $response['list'][] = [
                'name' => $domain,
                'type' => Site::TYPE_OTHER,
                'pattern' => "shExpMatch(host, '{$domain}') || shExpMatch(host, '*.{$domain}')",
                'proxy' => Proxy::find()->select(["CONCAT(host,':', port) as address", "login", "password"])
                    ->where(['status' => Proxy::STATUS_ACTIVE, 'type' => Proxy::TYPE_PUBLIC])
                    ->asArray()->all(),
            ];
        }

        return $this->render('access', [
            'sites' => $response,
            'extension_id' => $extension_id,
        ]);
    }

    public function actionCheck()
    {
        $urlForm = new UrlForm();
        if ($urlForm->load(Yii::$app->request->queryParams) && $urlForm->validate()) {
            $site = $urlForm->find();
            if ($site) return $this->redirect(['access', 'id' => $site->id]);
            else return $this->redirect(['access', 'domain' => $urlForm->url]);
        } else {
            return $this->redirect(['index']);
        }
    }

    public function actionIndex()
    {
        $userCountry = Yii::$app->request->getCountry() ?: 'gb';

        $searchModel = new SiteSearch();
        $dataProvider = $searchModel->search(ArrayHelper::merge(Yii::$app->request->queryParams, ['SiteSearch' => ['type' => Site::TYPE_OTHER, 'geo_code' => $userCountry]]));
        if (!$dataProvider->count) {
            $dataProvider = $searchModel->search(ArrayHelper::merge(Yii::$app->request->queryParams, ['SiteSearch' => ['type' => Site::TYPE_OTHER, 'geo_code' => 'gb']]));
        }

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'geo_code' => $userCountry,
            'urlForm' => new UrlForm(),
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id)
        ]);
    }

	public function actionReview($id)
	{
		return $this->render('reviews',[
			'model' => $this->findModel($id)
		]);
    }

    public function actionSettings()
    {
        return $this->render('settings', [
            'extension_id' => Yii::$app->request->cookies->getValue('ueid', Yii::$app->params['extension_id'])
        ]);
    }

    public function actionAjaxSiteList($q = null)
    {
        //TODO: Refactor + make search model
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => []];
        if (!is_null($q)) {
            $sites = Site::find()
                ->select(['site.id AS id', 'site.name AS name', 'domain.domain AS domain', 'site_image.src AS img_src'])
                ->joinWith([
                    'domains' => function ($query) use ($q) {
                        /** @var $query ActiveQuery */
                        $query->onCondition(['like', 'domain.domain', $q])
                            ->limit(1);
                    },
                ], false)
                ->joinWith([
                    'siteImages' => function ($query) {
                        /** @var $query ActiveQuery */
                        $query->onCondition(['site_image.type' => 'logo'])
                            ->limit(1);
                    },
                ], false)
                ->where(['site.status' => Site::STATUS_ACTIVE])
                ->andWhere(['or', ['like', 'site.name', $q], ['like', 'site.aliases', $q]])
                ->groupBy('site.id')
                ->limit(20)
                ->asArray()->all();
            $out['results'] = array_values($sites);

            $model = DynamicModel::validateData(['url'=>$q], [
                ['url', 'url', 'defaultScheme' => 'http'],
                ['url', 'filter', 'filter' => function ($value) {
                    $parts = explode('/', $value, 4);
                    $domain = $parts[2];
                    return $domain;
                }]
            ]);

            if (!$model->hasErrors()) {
                array_unshift($out['results'], ['id' => $q, 'name' => 'Custom domain: ' . $q, 'domain' => $q, 'img_src' => null]);
            }

        }
        return $out;
    }


    /**
     * Finds the Site model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Site the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Site::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
