<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 25.07.2017
 * Time: 18:09
 */

namespace frontend\models;


use common\models\SiteTranslation;

/**
 * This is the extended model class for table "{{%site}}".
 *
 * @property string $description
 * @property string $promoText
 * @property string $rank
 * @property SiteTranslation $translation
 */
class Site extends \common\models\Site
{
	public function getTranslation($geo_code = 'gb')
	{
		/** @var SiteTranslation $translation */
		$translation = $this->hasOne(SiteTranslation::className(), ['site_id' => 'id'])
			->inverseOf('site')
			->andWhere(['geo_code' => $geo_code])
			->one();
		if (!$translation) {
			$translation = $this->hasOne(SiteTranslation::className(), ['site_id' => 'id'])
				->inverseOf('site')
				->andWhere(['geo_code' => 'gb'])
				->one();
		}
		if (!$translation) {
			$translation = $this->hasOne(SiteTranslation::className(), ['site_id' => 'id'])
				->inverseOf('site')
				->one();
		}
		return $translation;
	}

	/**
	 * @param string $geo_code
	 * @return string
	 */
	public function getDescription($geo_code = 'gb')
	{
		$translation = $this->getTranslation($geo_code);
		return $translation ? $translation->description : '';
	}

	/**
	 * @param string $geo_code
	 * @return string
	 */
	public function getPromoText($geo_code = 'gb')
	{
		$translation = $this->getTranslation($geo_code);
		return $translation ? $translation->promo_text : '';
	}

	/**
	 * @param float $rank
	 * @return string
	 */
	public function getRank($rank = null)
	{
		$category_class = 'gold';
		if (!$rank) {
			if ($this->rating < 3.5) $category_class = 'bronze';
			elseif ($this->rating < 4.5) $category_class = 'silver';
		} else {
			if ($rank < 3.5) $category_class = 'bronze';
			elseif ($rank < 4.5) $category_class = 'silver';
		}
		return $category_class;
	}
}