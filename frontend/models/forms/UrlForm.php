<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 22.09.2017
 * Time: 15:45
 */

namespace frontend\models\forms;


use frontend\models\Site;
use yii\base\Model;
use yii\db\ActiveQuery;

class UrlForm extends Model
{
    public $url;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url'], 'required'],
            ['url', 'url', 'defaultScheme' => 'http'],
            ['url', 'filter', 'filter' => function ($value) {
                $parts = explode('/', $value, 4);
                $domain = $parts[2];
                return $domain;
            }],
        ];
    }

    /**
     * @return null|\yii\db\ActiveRecord|Site
     */
    public function find()
    {
        return Site::find()->innerJoinWith([
            'domains' => function ($query) {
                /** @var $query ActiveQuery */
                $query->onCondition(['like', 'domain', $this->url]);
            },
        ])->one();
        //return Domain::find()->where(['like', 'domain', $this->url])->all();
    }
}