<?php


namespace frontend\models\search;


use common\models\Geo;
use frontend\models\Site;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class SiteSearch extends Model
{
    public $type;
    public $geo_code;

    public function rules()
    {
        return [
            ['type', 'required'],

            ['geo_code', 'string', 'length' => [2,5]],
            ['geo_code', 'exist', 'skipOnError' => true, 'targetClass' => Geo::className(), 'targetAttribute' => ['geo_code' => 'code']],
        ];
    }

    public function search($params)
    {
        $query = Site::find()->where(['status' => Site::STATUS_ACTIVE]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 21,
            ],
            'sort' => [
                'attributes' => ['rating'],
                'defaultOrder' => ['rating' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        if($this->geo_code){
            $query->innerJoinWith('geos')
                ->andWhere(['geo.code' => $this->geo_code]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'type' => $this->type,
        ]);

        return $dataProvider;
    }
}