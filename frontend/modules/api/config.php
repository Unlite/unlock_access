<?php

return [
    'defaultRoute' => 'main',
    'components' => [
        'sypexGeo' => [
            'class' => 'omnilight\sypexgeo\SypexGeo',
            'database' => '@common/data/sypexgeo/SxGeo_2017_07_13.dat',
        ]
    ],
    'params' => [
        // list of parameters
    ],
];