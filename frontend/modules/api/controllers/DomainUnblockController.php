<?php

namespace frontend\modules\api\controllers;


use common\models\Domain;
use yii\helpers\ArrayHelper;
use yii\rest\Controller;
use yii\web\Response;

class DomainUnblockController extends Controller
{
    /**
     * Behaviors
     *
     * @return mixed
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return [];
    }

    public function actionGetAll()
    {
        \Yii::$app->response->format = Response::FORMAT_RAW;
        $domains = ArrayHelper::getColumn(Domain::find()->all(), function (Domain $model, $default) {
            return '.' . $model->domain;
        });
        return join("\n", $domains);
    }
}