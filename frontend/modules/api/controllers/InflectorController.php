<?php

namespace frontend\modules\api\controllers;

use yii\filters\Cors;
use yii\helpers\Inflector;
use yii\rest\Controller;
use yii\web\Response;

/**
 * Class InflectorController
 * @package frontend\modules\api\controllers
 */
class InflectorController extends Controller
{
    /**
     * Behaviors
     *
     * @return mixed
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'corsFilter' => [
                'class' => Cors::class,
                'cors' => [
                    'Origin' => [\Yii::$app->params['backendHostInfo'], \Yii::$app->params['frontendHostInfo'], \Yii::$app->params['portalHostInfo']]
                ]
            ]
        ];
    }

    public function actionSlug($string = '')
    {
        Inflector::$transliterator = 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;';
        return ['slug' => Inflector::slug($string)];
    }
}