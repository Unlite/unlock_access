<?php

namespace frontend\modules\api\controllers;


use common\models\Client;
use Yii;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Main controller for the `api` module
 */
class MainController extends Controller
{
    /**
     * Behaviors
     *
     * @return mixed
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'reg' => ['POST'],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        return [];
    }

    /**
     * Registration Client by extension
     * @return array
     * @throws HttpException
     */
    public function actionReg()
    {
        $client = Client::getClientByCookies() ?: Client::getClientByUser();
        if($client && !$client->secure){ // Have only client_token cookies (not trusted)
            throw new HttpException(208, 'You already registered');
        }elseif ($client){ // Have client_token and client_secret cookies or not guest (trusted)
            $client->extension_id = Yii::$app->request->post('extension_id', Yii::$app->params['extension_id']);
            $client->touch('last_online_at');
        }else{ // Guest and haven't cookies (client_token and client_secret)
            $client = Client::newRecord([
                'extension_id' => Yii::$app->request->post('extension_id', Yii::$app->params['extension_id']),
                'ip' => Yii::$app->request->userIP,
                'user_agent' => Yii::$app->request->userAgent,
            ], true);
        }

        Yii::$app->response->statusCode = 201;
        return [
            'status' => 201,
            'uid' => $client->token,
            'secret' => $client->secret,
        ];
    }

}
