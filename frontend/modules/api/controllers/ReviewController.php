<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 29.08.2017
 * Time: 16:27
 */

namespace frontend\modules\api\controllers;


use frontend\modules\api\models\Review;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\web\ServerErrorHttpException;

class ReviewController extends ActiveController
{
    public $modelClass = 'frontend\modules\api\models\Review';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actions()
    {
        $actions = parent::actions();

        // disable the "delete" and "create" actions
        unset($actions['delete'], $actions['update']);

        // customize the data provider preparation with the "prepareDataProvider()" method
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    public function prepareDataProvider()
    {
        $query = Review::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);
        $query->andFilterWhere(['site_id' => \Yii::$app->getRequest()->getQueryParam('site_id')]);
        return $dataProvider;
        // prepare and return a data provider for the "index" action
    }
//    /**
//     * Behaviors
//     *
//     * @return mixed
//     */
//    public function behaviors()
//    {
//        return [
//            [
//                'class' => 'yii\filters\ContentNegotiator',
//                'formats' => [
//                    'application/json' => Response::FORMAT_JSON,
//                ],
//            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'create' => ['POST'],
//                ],
//            ],
//        ];
//    }
//
//    public function actionIndex($site_id = null)
//    {
//        $q = Review::find()->orderBy(['id' => SORT_DESC])->limit(20);
//        if($site_id) $q->where(['site_id' => $site_id]);
//        return [
//            'status' => 200,
//            'items' => $q->all()
//        ];
//    }
//
//    public function actionCreate()
//    {
//        $model = new Review();
//        $post = ArrayHelper::merge(\Yii::$app->request->post(), ['type' => Review::TYPE_USER]);
//        if($model->load($post, '') && $model->save()){
//            \Yii::$app->response->statusCode = 201;
//            return ['status'=> 201, 'message' => 'Success added'];
//        }elseif (!$model->hasErrors()) {
//            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
//        }
//        return $model;
//    }
}