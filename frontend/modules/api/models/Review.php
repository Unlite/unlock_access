<?php

namespace frontend\modules\api\models;


use yii\helpers\ArrayHelper;

class Review extends \common\models\Review
{
    public function fields()
    {
        return ['site_id', 'author', 'text', 'rating'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge([['type', 'filter', 'filter' => function ($value) {return self::TYPE_USER;}]], parent::rules());
    }
}