<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $loginForm \common\models\forms\LoginForm */
/* @var $regForm \common\models\forms\SignUpForm */
/* @var $passwordResetRequestForm \common\models\forms\PasswordResetRequestForm */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;


$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
Modal::begin([
    'id' => 'userModal',
    'header' => '<nav id="w2" class="navbar-static-top navbar-inverse navbar" role="navigation">
        <div class="navbar-header"><a class="navbar-brand" href="/"><img src="/img/logo.png"></a></div>
        <div id="w2-collapse" class="collapse navbar-collapse pull-right">
            <ul id="w4" class="navbar-nav navbar-padding-left nav" role="tablist">
                <li class="active"><a href="#loginform-tab" aria-controls="loginform-tab" role="tab" data-toggle="tab">Login</a></li>
                <li><a href="#signupform-tab" aria-controls="signupform-tab" role="tab" data-toggle="tab">Registration</a></li>
            </ul>
        </div>
</nav>',
    'footer' => '<h5>' . Yii::t('front', 'By clicking submit you are agreeing to the Terms and Conditions') . '</h5>',
    'size' => 'modal-sm',
    'options' => ['class' => 'fade modal in default-modal modal-scrollable']
]);
?>

<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="loginform-tab">
        <div class="site-login">
            <div class="row">
                <div class="col-md-offset-3 col-md-6">
                    <div class="col-xs-12">
                        <?php $form = ActiveForm::begin([
                            'id' => 'login-form',
                            'action' => '/user/ajax-login',
                            'enableAjaxValidation' => true,
                            'validateOnBlur' => false,
                            'validateOnChange' => false,
                            'validateOnSubmit' => true,
                            'fieldConfig' => [
                                'template' => "{input}\n{hint}\n{error}",
                            ]
                        ]); ?>

                        <?= $form->field($loginForm, 'username', ['inputOptions' => [
                            'placeholder' => $loginForm->getAttributeLabel('username'),
                        ]])->textInput(['autofocus' => true, 'placeholder' => Yii::t('front', 'Login')]) ?>

                        <?= $form->field($loginForm, 'password', ['inputOptions' => [
                            'placeholder' => $loginForm->getAttributeLabel('password'),
                        ]])->passwordInput(['placeholder' => Yii::t('front', 'Password')]) ?>

                        <h5 class="pull-right">
                            <?= Html::a(Yii::t('front', 'Forgot your password?'), ['#'],['id'=>'reset-password','data'=>['toggle'=>'modal','target'=>'#resetModal']]) ?>
                        </h5>
                        <div class="clearfix"></div>
                        <div class="form-group button-form-group">
                            <?= Html::submitButton('Login', ['class' => 'btn btn-login', 'name' => 'login-button']) ?>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div role="tabpanel" class="tab-pane" id="signupform-tab">
        <div class="site-signup">
            <div class="row">
                <div class="col-md-offset-3 col-md-6">
                    <div class="col-xs-12">
                        <?php $form = ActiveForm::begin([
                            'id' => 'signup-form',
                            'action' => '/user/ajax-signup',
                            'enableAjaxValidation' => true,
                            'validateOnBlur' => false,
                            'validateOnChange' => false,
                            'validateOnSubmit' => true,
                            'fieldConfig' => [
                                'template' => "{input}\n{hint}\n{error}",
                            ]
                        ]); ?>

                        <?= $form->field($regForm, 'username',['inputOptions' => [
                            'placeholder' => $loginForm->getAttributeLabel('username'),
                        ]])->textInput(['autofocus' => true]) ?>

                        <?= $form->field($regForm, 'email',['inputOptions' => [
                            'placeholder' => $loginForm->getAttributeLabel('email'),
                        ]])->textInput() ?>

                        <?= $form->field($regForm, 'password',['inputOptions' => [
                            'placeholder' => $loginForm->getAttributeLabel('password'),
                        ]])->passwordInput() ?>

                        <div class="form-group button-form-group">
                            <?= Html::submitButton('Signup', ['class' => 'btn btn-login', 'name' => 'signup-button']) ?>
                        </div>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php Modal::end() ?>
<?php Modal::begin([
    'id' => 'resetModal',
    'header' => '<nav id="w2" class="navbar-static-top navbar-inverse navbar" role="navigation">
        <div class="navbar-header"><a class="navbar-brand" href="/"><img src="/img/logo.png"></a></div>
</nav>',
    'size' => 'modal-sm',
    'options' => ['class' => 'fade modal in default-modal']
]);
?>

        <div class="site-reset">
            <div class="row">
                    <div class="col-xs-12">
                        <h3><?=Yii::t('front','Forgot Password')?></h3>
                        <h5 class="hint-block"><?=Yii::t('front','We\'ll email you the instructions to reset your password')?></h5>
                        <?php $form = ActiveForm::begin([
                            'id' => 'reset-form',
                            'action' => '/user/request-password-reset',
                            //'enableAjaxValidation' => true,
                            'validateOnBlur' => false,
                            'validateOnChange' => false,
                            'validateOnSubmit' => true,
                            'fieldConfig' => [
                                'template' => "{input}\n{hint}\n{error}",
                            ]
                        ]); ?>

                        <?= $form->field($passwordResetRequestForm, 'email', ['inputOptions' => [
                            'placeholder' => $passwordResetRequestForm->getAttributeLabel('email'),
                        ]])->textInput(['autofocus' => true, 'placeholder' => $passwordResetRequestForm->getAttributeLabel('email')]) ?>

                        <div class="clearfix"></div>
                        <div class="form-group button-form-group flex-center">
                            <?= Html::submitButton('Reset password', ['class' => 'btn btn-login', 'name' => 'login-button']) ?>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
            </div>
        </div>
<?php Modal::end() ?>
