<?php

/* @var $this \yii\web\View */

/* @var $content string */

use common\widgets\Alert;
use frontend\assets\AppAsset;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700|Roboto:300,400,700&amp;subset=cyrillic,cyrillic-ext,latin-ext,vietnamese"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500" rel="stylesheet">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => '<img src="/img/logo.png">',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-static-top navbar-inverse',
        ],
    ]);

    //	$menuItem = ['<div class="slider-block"><label class="switch"><input type="checkbox"><span class="slider round"></span></label></div>'];
    $menuItem = ['<div class="onoffswitch">
    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" checked>
    <label class="onoffswitch-label" for="myonoffswitch">
        <span class="onoffswitch-inner"></span>
        <span class="onoffswitch-switch"></span>
    </label>
</div>'];

    echo Nav::widget([
        'options' => ['class' => 'navbar-left'],
        'items' => $menuItem,
    ]);

    $menuItems = [
        ['label' => Yii::t('front', 'All'), 'url' => ['/site/index'], 'options' => ['class' => 'pull-left']],
        ['label' => Yii::t('front', 'Casinos'), 'url' => ['/site/casino']],
        ['label' => Yii::t('front', 'Bets'), 'url' => '#'],
        ['label' => Yii::t('front', 'CASHBACK'), 'url' => '#'],
    ];
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-padding-left'],
        'items' => $menuItems,
    ]);

    if (Yii::$app->user->isGuest) {
        //$menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
//		$menuItems = [['label' => 'Login', 'url' => ['/main/login']]];
        $menuItems = ['<li>' . Html::a(Yii::t('front', 'LOGIN'), '#', [
                // other options
                'data' => [
                    'toggle' => 'modal',
                    'target' => '#userModal',
                ],
            ]) . '</li>'];
    } else {
        //$menuItems = [['label' => 'Logout', 'url' => ['/main/logout']]];
        $menuItems = ['<li>'
            . Html::a('Logout', ['user/logout'], [
                'data' => [
                    'method' => 'post',
                ],
            ])
            . '</li>'];
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();

    ?>

    <?= Alert::widget() ?>
    <?= $content ?>
    <?= $this->render('_auth', [
        'loginForm' => new \common\models\forms\LoginForm(),
        'regForm' => new \common\models\forms\SignUpForm(),
        'passwordResetRequestForm' => new \common\models\forms\PasswordResetRequestForm(),
    ]) ?>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
