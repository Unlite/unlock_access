<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>

<div class="banner" style="background-image: url('/img/header-img.png')">
    <div class="container img-text">
        <span class="text top">Лучшие игры</span>
        <span class="text center">Заманчивые бонусы</span>
        <span class="text bottom">Безупречная репутация</span>
        <a href=""><span class="link play"><?php echo Yii::t('front','Play!')?>Играть!</span></a>
    </div>
</div>
<div class="container">

    <div class="site-category">
<!--        <div class="col-xs-12">-->
<!--            <div class="row">-->
<!--                <div class="col-sm-6 col-lg-4">-->
<!--                    <div class="row">-->
<!--                        <h2>--><?//= Yii::t('front', 'All Casinos for {country}:', ['country' => 'Russia']) ?><!--</h2></div>-->
<!--                </div>-->
<!--                <div class="col-sm-6 col-lg-8">-->
<!--                    <div class="row">-->
<!--                        <div class="col-lg-6 ">-->
<!--                            <div class="row">-->
<!--								--><?php //$form = ActiveForm::begin([
//									'action' => ['/site/check'],
//									'method' => 'get',
//									'options' => ['class' => 'form-inline']
//								]); ?>
<!--                                <div class="search">-->
<!--									--><?//= $form->field($urlForm, 'url', ['template' => '{input}']) ?>
<!---->
<!--                                    <div class="form-group">-->
<!--										--><?//= Html::submitButton(Yii::t('front', '<img src="/img/search.png">'), ['class' => 'search-btn']) ?>
<!--                                    </div>-->
<!--                                </div>-->
<!--								--><?php //ActiveForm::end(); ?>
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
        <div class="col-xs-12 category-item-list">
            <div class="row">
                <div class="col-sm-6 col-lg-4">
                        <h2><?= Yii::t('front', 'All Casinos for {country}:', ['country' => 'Russia']) ?></h2>
                </div>
                <div class="col-sm-6 col-lg-4">
								<?php $form = ActiveForm::begin([
									'action' => ['/site/check'],
									'method' => 'get',
									'options' => ['class' => 'form-inline']
								]); ?>
                                <div class="search">
									<?= $form->field($urlForm, 'url', ['template' => '{input}']) ?>

                                    <div class="form-group">
										<?= Html::submitButton(Yii::t('front', '<img src="/img/search.png">'), ['class' => 'search-btn']) ?>
                                    </div>
                                </div>
								<?php ActiveForm::end(); ?>
                </div>
                <div class="col-sm-6 col-lg-4">
                </div>
                <div class="col-sm-6 col-lg-4 ">
                    <div class="category-item category-gold">
                        <div class="item-padding">
                            <a href="/main/item">
                                <div class="item-content">
                                    <img class="icon" src="/img/frank.png">
                                    <h2 class="title">Frank Casino</h2>
                                    <h2 class="rating lato-regular">4.9</h2>
                                    <img class="screen" src="/img/croup_13.png">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4 ">
                    <div class="category-item category-silver">
                        <div class="item-padding">
                            <a href="/main/item">
                                <div class="item-content">
                                    <img class="icon" src="/img/malina.png">
                                    <h2 class="title">Malina Casino</h2>
                                    <h2 class="rating lato-regular">3.9</h2>
                                    <img class="screen" src="/img/croup_13.png">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class=" category-item category-bronze">
                        <div class="item-padding">
                            <a href="/main/item">
                                <div class="item-content">
                                    <img class="icon" src="/img/logo-bet.png">
                                    <h2 class="title">Bet At Home</h2>
                                    <h2 class="rating lato-regular">2.9</h2>
                                    <img class="screen" src="/img/croup_13.png">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class=" category-item category-bronze">
                        <div class="item-padding">
                            <a href="/main/item">
                                <div class="item-content">
                                    <img class="icon" src="/img/logo-bet.png">
                                    <h2 class="title">Bet At Home</h2>
                                    <h2 class="rating lato-regular">2.9</h2>
                                    <img class="screen" src="/img/croup_13.png">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class=" category-item category-bronze">
                        <div class="item-padding">
                            <a href="/main/item">
                                <div class="item-content">
                                    <img class="icon" src="/img/logo-bet.png">
                                    <h2 class="title">Bet At Home</h2>
                                    <h2 class="rating lato-regular">2.9</h2>
                                    <img class="screen" src="/img/croup_13.png">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class=" category-item category-bronze">
                        <div class="item-padding">
                            <a href="/main/item">
                                <div class="item-content">
                                    <img class="icon" src="/img/logo-bet.png">
                                    <h2 class="title">Bet At Home</h2>
                                    <h2 class="rating lato-regular">2.9</h2>
                                    <img class="screen" src="/img/croup_13.png">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>


</div>
