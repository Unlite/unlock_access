<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>

<div class="banner" style="background-image: url('/img/header-img.png')">
    <div class="container img-text">
        <span class="text top">Лучшие игры</span>
        <span class="text center">Заманчивые бонусы</span>
        <span class="text bottom">Безупречная репутация</span>
        <a href=""><span class="link play"><?php //echo Yii::t('front','Play!')?>Играть!</span></a>
    </div>
</div>
<div class="container info-container">

    <div class="site-category">
        <div class="row info-item-list">
            <div class="col-sm-6 col-lg-4 ">
                <div class="info-item">
                    <h2><?= Yii::t('front', 'Available Games:') ?></h2>
                    <div class="games">
                        <a href="#">
                            <img src="/img/croup_06.png">
                        </a>
                        <a href="#">
                            <img src="/img/croup_08.png">
                        </a>
                        <a href="#">
                            <img src="/img/croup_10.png">
                        </a>
                        <a href="#">
                            <img src="/img/croup_12.png">
                        </a>
                        <a href="#">
                            <img src="/img/croup_19.png">
                        </a>
                        <a href="#">
                            <img src="/img/croup_20.png">
                        </a>
                        <a href="#">
                            <img src="/img/croup_21.png">
                        </a>
                        <a href="#">
                            <img src="/img/croup_22.png">
                        </a>
                    </div>
                    <div class="pagination-block">
                        <ul class="pagination-list">
                            <li class="pagination-item active">
                                <a href="#"></a>
                            </li>
                            <li class="pagination-item">
                                <a href="#"></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-4 ">
                <div class="info-item">
                    <h2>License:</h2>
                    <div class="info-button-row">
                        <a class="custom-btn btn-white" href="#">Country</a>
                        <a class="custom-btn btn-white" href="#">Country</a>
                        <a class="custom-btn btn-white" href="#">Country</a>
                    </div>
                    <h2>User review:</h2>
                    <div class="rating-bar">
                        <div class="rating rating-green" style="width: 90%"></div>
                        <div class="rating rating-red" style="width: 10%"></div>
                    </div>
                    <h2>90% - Positive</h2>
                    <div class="center-button-container">
                        <a class="custom-btn btn-white custom-btn-lg" href="#">Write / Watch</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-4 ">
                <div class="info-item">
                    <div class="proofits">
                        <div class="col-xs-6">
                            <div class="row">
                                <h2>Max Win</h2>
                                <div class="cash">
                                    <h3>1000$</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="row">
                                <h2>Max Pay</h2>
                                <div class="cash">
                                    <h3>1000$</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h2>Available payment system</h2>
                    <div class="payments">
                        <a href="#">
                            <img src="/img/croup_24.png">
                        </a>
                        <a href="#">
                            <img src="/img/croup_26.png">
                        </a>
                        <a href="#">
                            <img src="/img/croup_28.png">
                        </a>
                        <a href="#">
                            <img src="/img/croup_30.png">
                        </a>
                        <a href="#">
                            <img src="/img/croup_32.png">
                        </a>
                    </div>
                    <div class="pagination-block">
                        <ul class="pagination-list">
                            <li class="pagination-item active">
                                <a href="#"></a>
                            </li>
                            <li class="pagination-item">
                                <a href="#"></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>


</div>
