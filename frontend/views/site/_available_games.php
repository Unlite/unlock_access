<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->registerJs('$(function () { 
    $("[data-toggle=\'tooltip\']").tooltip(); 
});', \yii\web\View::POS_LOAD)
?>
<div>
    <h2><?= Yii::t('front', 'Available Games:') ?></h2>
    <div class="games">

        <a href="#">
			<?= Html::tag('span', '<img src="/img/croup_03.png">', [
				'title' => 'This is a test tooltip',
				'data-toggle' => 'tooltip',
				'style' => 'text-decoration: underline; cursor:pointer;'
			]); ?>
        </a>
        <a href="#">
			<?= Html::tag('span', '<img src="/img/croup_05.png">', [
				'title' => 'This is a test tooltip',
				'data-toggle' => 'tooltip',
				'style' => 'text-decoration: underline; cursor:pointer;'
			]); ?>
        </a>
        <a href="#">
			<?= Html::tag('span', '<img src="/img/croup_09.png">', [
				'title' => 'This is a test tooltip',
				'data-toggle' => 'tooltip',
				'style' => 'text-decoration: underline; cursor:pointer;'
			]); ?>
        </a>
        <a href="#">
			<?= Html::tag('span', '<img src="/img/croup_11.png">', [
				'title' => 'This is a test tooltip',
				'data-toggle' => 'tooltip',
				'style' => 'text-decoration: underline; cursor:pointer;'
			]); ?>
        </a>
        <a href="#">
			<?= Html::tag('span', '<img src="/img/croup_18.png">', [
				'title' => 'This is a test tooltip',
				'data-toggle' => 'tooltip',
				'style' => 'text-decoration: underline; cursor:pointer;'
			]); ?>
        </a>
        <a href="#">
			<?= Html::tag('span', '<img src="/img/croup_19.png">', [
				'title' => 'This is a test tooltip',
				'data-toggle' => 'tooltip',
				'style' => 'text-decoration: underline; cursor:pointer;'
			]); ?>
        </a>
        <a href="#">
			<?= Html::tag('span', '<img src="/img/croup_20.png">', [
				'title' => 'This is a test tooltip',
				'data-toggle' => 'tooltip',
				'style' => 'text-decoration: underline; cursor:pointer;'
			]); ?>
        </a>
        <a href="#">
			<?= Html::tag('span', '<img src="/img/croup_22.png">', [
				'title' => 'This is a test tooltip',
				'data-toggle' => 'tooltip',
				'style' => 'text-decoration: underline; cursor:pointer;'
			]); ?>
        </a>
        <a href="#">
			<?= Html::tag('span', '<img src="/img/croup_23.png">', [
				'title' => 'This is a test tooltip',
				'data-toggle' => 'tooltip',
				'style' => 'text-decoration: underline; cursor:pointer;'
			]); ?>
        </a>
    </div>
</div>
<?php // echo Html::tag('span', 'tooltip', [
//	'title'=>'This is a test tooltip',
//	'data-toggle'=>'tooltip',
//	'style'=>'text-decoration: underline; cursor:pointer;'
//]); ?>
