<?php
/* @var \frontend\models\Site $model */
?>
<div class="bonus-list">
    <h2><?= Yii::t('front', 'Available bonuses:') ?></h2>
    <?php
    $bonuses = [
        ['title' => 'Bonus type'],
        ['title' => 'Bonus type'],
        ['title' => 'Bonus type'],
    ];
    foreach ($bonuses as $key => $bonus) { ?>
        <div class="bonus-block <?= $model->rank ?>">
            <div class="bonus-text"><?= $bonus['title'] ?></div>
            <button data-toggle="modal" data-target="<?= '#bonus-' . $key ?>" class="bonus-button">Activate</button>
            <div class="inset-border"></div>
        </div>
    <?php } ?>

</div>