<?php
use yii\bootstrap\Modal;
use yii\helpers\Html;
/* @var \frontend\models\Site $model */

$bonuses = [
    ['title' => 'Bonus type'],
    ['title' => 'Bonus type'],
    ['title' => 'Bonus type'],
];


$favicon = $model->getSiteImages()->type('logo')->one()->src ?? '/img/frank.png';

foreach ($bonuses as $key => $bonus) {

    Modal::begin([
        'id' => 'bonus-' . $key,
        'header' => '<nav id="w2" class="navbar-static-top navbar-inverse navbar" role="navigation">
    <div class="navbar-header"><a class="navbar-brand" href="/"><img src="' . $favicon . '"></a></div>
    <div class="collapse navbar-collapse ">
        <ul id="" class="navbar-nav nav">
            <li><h4>Type of bonus</h4></li>
            <li><h6>05.06.2017–05.06.2018</h6></li>
        </ul>
    </div>
</nav>',
        'footer' => '',
        'size' => 'modal-sm',
        'options' => ['class' => 'fade modal in bonus-modal']
    ]);
    ?>


    <div class="bonus-block">
        <p>Some AWESOME TEXT</p>
        <button class="btn btn-login"><?= Yii::t('front', 'Get it') ?></button>
    </div>
    <?php Modal::end() ?>
<?php } ?>
