<?php
use yii\bootstrap\Carousel;
use yii\helpers\Html;

/**
 * @var \yii\web\View $this
 * @var \frontend\models\Site|null $model
 * @var \frontend\models\Site[]|null $models
 * @var \yii\data\ActiveDataProvider|null $dataProvider
 */

if(!isset($carouselItems))
	$carouselItems = [];
$carouselCount = 3;
$models = [];
if(isset($dataProvider))
	$models = $dataProvider->getModels();
elseif(isset($model))
	$models = [$model];
foreach ($models as $model) {
	if (0 > --$carouselCount) break;
	$promoText = explode('|', $model->getPromoText(Yii::$app->request->getCountry() ?: 'gb') ?: '');

    $promoSrc = $model->getSiteImages()->type('promo')->one()->src ?? '/img/header-img.png';

	$carouselItems[] = [
        'content' => Html::a("<div class=\"banner\" style=\"background-image: url('" . $promoSrc . "')\">
                            <div class=\"container img-text\">
                                <span class=\"text top\">" . (@$promoText[0] ?: '') . "</span>
                                <span class=\"text center\">" . (@$promoText[1] ?: '') . "</span>
                                <span class=\"text bottom\">" . (@$promoText[2] ?: '') . "</span>
                                <span onclick=\"window.location.href='" . \yii\helpers\Url::to(['site/access', 'id' => $model->id]) . "';\" class=\"link play\">" . Yii::t('front', 'Play!') . "</span>
                                <div class=\"rating-box\"><span class=\"rating " . $model->rank . "\">" . Yii::$app->formatter->asDecimal($model->rating, 1) . "</span></div>
                            </div>  
                        </div>", ['site/view', 'id' => $model->id])
	];
};
echo Carousel::widget([
	'items' => $carouselItems,
	'options' => ['class' => 'carousel slide'],
	'controls' => ['<span><img src="/img/arrow-left.png"></span>', '<span><img src="/img/arrow-right.png"></span>'],
	'showIndicators' => false,
]); ?>