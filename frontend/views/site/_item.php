<?php
use frontend\models\Site;

/* @var $this yii\web\View */
/* @var $model \frontend\models\Site */

$favicon = $model->getSiteImages()->type('logo')->one()->src ?? '/img/frank.png';
$screen = $model->getSiteImages()->type('preview')->one()->src ?? '/img/croup_13.png';

$url = ($model->type == Site::TYPE_CASINO ? '/site/view' : '/site/access');
?>
<div class="col-sm-6 col-lg-4 ">
    <div class="category-item <?= $model->type == Site::TYPE_OTHER ? 'silver' : $model->rank?>">
        <div class="item-padding">
            <a href="<?= \yii\helpers\Url::to([$url, 'id' => $model->id]) ?>">
                <div class="item-content">
                    <img class="icon" src="<?= $favicon ?>">
                    <h2 class="title"><?= $model->name ?></h2>
					<?php if($model->type !== Site::TYPE_OTHER) {?><h2 class="rating lato-regular"><?= Yii::$app->formatter->asDecimal($model->rating, 1) ?></h2><?php } ?>
                    <img class="screen" src="<?= $screen ?>">
                </div>
            </a>
			<?php if($model->type !== Site::TYPE_OTHER) {?>
            <a href="<?= \yii\helpers\Url::to(['site/access', 'id' => $model->id]) ?>">
                <div class="triangle <?= $model->rank ?>"><img src="/img/more.png"></div>
            </a>
            <?php } ?>
        </div>
    </div>
</div>
