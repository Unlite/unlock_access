<?php
/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */
use yii\widgets\ListView;

?>

<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_item',
    'itemOptions' => ['class' => 'item'],
    'emptyText' => false,
    'summary' => '',
    'pager' => [
        'class' => \kop\y2sp\ScrollPager::className(),
        //'negativeMargin' => 250,
        'noneLeftText' => '',
        'triggerOffset' => 3,
        'enabledExtensions' => [
            \kop\y2sp\ScrollPager::EXTENSION_TRIGGER,
            \kop\y2sp\ScrollPager::EXTENSION_SPINNER,
            \kop\y2sp\ScrollPager::EXTENSION_NONE_LEFT,
            \kop\y2sp\ScrollPager::EXTENSION_PAGING,
            //\kop\y2sp\ScrollPager::EXTENSION_HISTORY
        ]
        //'eventOnScroll' => "function(sO,sT){console.log(sO,sT)}",
    ]
]); ?>
