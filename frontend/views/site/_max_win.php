<div>
    <div class="proofits">
        <div class="col-xs-6">
            <div class="row">
                <h2>Max Win</h2>
                <div class="cash">
                    <svg width="121" height="125" xmlns="http://www.w3.org/2000/svg">
                        <path id="svg_2"
                              d="m99.462811,108.595034c-34.504132,28.512398 -92.148762,7.644629 -94.628101,-44.6281c2.066116,-61.570249 73.966944,-77.066117 104.132234,-37.190083"
                              opacity="1" fill-opacity="null" stroke-opacity="null"
                              stroke-width="5" stroke="#b56a1c" fill="none"/>
                    </svg>
                    <h3>1000$</h3>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="row">
                <h2>Max Pay</h2>
                <div class="cash">
                    <svg width="121" height="125" xmlns="http://www.w3.org/2000/svg">
                        <path id="svg_2"
                              d="m99.462811,108.595034c-34.504132,28.512398 -92.148762,7.644629 -94.628101,-44.6281c2.066116,-61.570249 73.966944,-77.066117 104.132234,-37.190083"
                              opacity="1" fill-opacity="null" stroke-opacity="null"
                              stroke-width="5" stroke="#b56a1c" fill="none"/>
                    </svg>
                    <h3>1000$</h3>
                </div>
            </div>
        </div>
    </div>
    <h2>Available payment system</h2>
    <div class="payments">
        <a href="#">
            <img src="/img/croup_24.png">
        </a>
        <a href="#">
            <img src="/img/croup_26.png">
        </a>
        <a href="#">
            <img src="/img/croup_28.png">
        </a>
        <a href="#">
            <img src="/img/croup_30.png">
        </a>
        <a href="#">
            <img src="/img/croup_32.png">
        </a>
    </div>
</div>