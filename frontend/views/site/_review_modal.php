<?php
use kartik\rating\StarRating;
use yii\bootstrap\Modal;

/* @var \frontend\models\Site $model */
/* @var $this yii\web\View */

$favicon = $model->getSiteImages()->type('logo')->one()->src ?? '/img/frank.png';

$this->registerJs('
$(\'#review-add\').on(\'change keyup keydown paste cut\', \'textarea\', function () {
        $(this).height(0).height(this.scrollHeight);
    }).find(\'textarea\').change();
', \yii\web\View::POS_READY);

Modal::begin([
	'id' => 'review-add',
	'header' => '<nav id="w2" class="navbar-static-top navbar-inverse navbar" role="navigation">
                    <div class="navbar-header"><a class="navbar-brand" href="/"><img src="' . $favicon . '"></a></div>
                    <div class="collapse navbar-collapse ">
                        <h3>' . $model->name . '</h3>
                    </div>
                    </nav>',
	'footer' => '',
	'size' => 'modal-sm',
	'options' => ['class' => 'fade modal in review-modal']
]);
?>


<div class="review-block">
    <div class="response-input">
        <label for="review_text">Response</label>
        <textarea id="review_text" name="review_text">texttexttexttexttexttexttexttexttexttexttexttexttexttexttextterrrrrrrrrrrrr
xttexttexttexttext
texttexttexttexttextte
xttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttextt
exttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttext

        </textarea>
    </div>
    <div class="rating">
    <?php
        echo StarRating::widget([
			'name' => 'rating_1',
			'pluginOptions' => [
				'showClear' => false,
				'showCaption' => false,
				'containerClass' => 'gold',
				'emptyStar' => '<i class=\'rating-filled\'></i>',
				'filledStar' => '<i class=\'rating-filled-silver\'></i>',
				'min' => 0,
				'max' => 5,
				'stars' => 5,
				'step' => 0.5,
			],
			'pluginEvents' => [
				"rating:change" => new \yii\web\JsExpression("function(event, value, caption) { 
                    if(value < 1) {
                        $(event.target).rating('update', 1);
                    }
				}"),
				"rating:hover" => new \yii\web\JsExpression("function(event, value, caption, target) { 
                    $(event.target).closest('.rating-container').removeClass('bronze');
                    $(event.target).closest('.rating-container').removeClass('silver');
                    $(event.target).closest('.rating-container').removeClass('gold');
                    if(value < 3.5) {
                        $(event.target).closest('.rating-container').addClass('bronze')
                    } else if(value >= 3.5 && value < 4.5) {
                        $(event.target).closest('.rating-container').addClass('silver')
                    } else {
                        $(event.target).closest('.rating-container').addClass('gold')
                    }
                }"),
				"rating:hoverleave" => new \yii\web\JsExpression("function(event, target) { 
                    var value = $(event.target).val();
                    $(event.target).closest('.rating-container').removeClass('bronze');
                    $(event.target).closest('.rating-container').removeClass('silver');
                    $(event.target).closest('.rating-container').removeClass('gold');
                    if(value < 3.5) {
                        $(event.target).closest('.rating-container').addClass('bronze')
                    } else if(value >= 3.5 && value < 4.5) {
                        $(event.target).closest('.rating-container').addClass('silver')
                    } else {
                        $(event.target).closest('.rating-container').addClass('gold')
                    }
				}"),
			]
		]);

		?>
        <button class="btn btn-login"><?= Yii::t('front', 'Submit') ?></button>
    </div>
</div>
<?php Modal::end() ?>
