<?php
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\web\JsExpression;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $geo_code string */
/* @var $urlForm \frontend\models\forms\UrlForm */

$url = \yii\helpers\Url::to(['/site/access']);
$formatJs = <<<JS
var formatRepo = function (repo) {
    if (repo.loading) {
        return repo.text;
    }
    var markup =
        '<div class="row">' + 
            '<div class="col-sm-12">' +
                (repo.img_src ? '<img src="' + repo.img_src + '" class="img-rounded" style="width:30px" />' : '') +
                '<b style="margin-left:5px">' + repo.name + '</b>' + 
            '</div>' +
        '</div>';
    return '<div style="overflow:hidden;">' + markup + '</div>';
};
var formatRepoSelection = function (repo) {
    if(repo.img_src) return '<img src="' + repo.img_src + '" class="img-rounded" style="width:16px" />' +
                '<b style="margin-left:5px">' + repo.name + '</b>';
    if(parseInt(repo.id)) return repo.name;
    
    return repo.id || repo.text;
}
JS;

// Register the formatting script
$this->registerJs($formatJs, View::POS_HEAD);

?>
<?php $form = ActiveForm::begin([
    'action' => ['/site/check'],
    'method' => 'get',
    'options' => ['class' => 'form-inline']
]); ?>
<div class="search">
	<?php // = $form->field($urlForm, 'url', ['template' => '{input}'])->textInput(['placeholder' => Yii::t('front', 'Enter site to access')]) ?>
    <?= $form->field($urlForm, 'url', ['template' => '{input}'])->widget(Select2::className(), [
        'value' =>  $urlForm->url,
        'initValueText' => $urlForm->url,
        'options' => ['placeholder' => 'Enter site to access'],
        'theme' => Select2::THEME_BOOTSTRAP,
        //'hideSearch' => true,
        //'readonly' => true,
        'pluginOptions' => [
            'allowClear' => false,
            'minimumInputLength' => 2,
            'ajax' => [
                'url' => \yii\helpers\Url::to(['/site/ajax-site-list']),
                'dataType' => 'json',
                'delay' => 250,
                'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                'cache' => true
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('formatRepo'),
            'templateSelection' => new JsExpression('formatRepoSelection'),
        ],
        'pluginEvents' => [
            "select2:selecting" => "function(event) { if(parseInt(event.params.args.data.id)) location.href = '{$url}?id=' + event.params.args.data.id }",
        ],
//        'addon' => [
//            'append' => [
//                'content' => Html::icon('search'),
//                'asButton' => true
//            ],
//        ]
    ]);
    ?>
    <div class="form-group">
		<?= Html::submitButton('<img src="/img/search.png">', ['class' => 'search-btn']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
