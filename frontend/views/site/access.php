<?php
/* @var $this yii\web\View */
/* @var $sites null|array */
/* @var $extension_id string */
?>
    <h1>site/access</h1>

    <p>
        Redirecting
    </p>
    <pre>
        <?php var_dump($sites); ?>
    </pre>

<?php
$jsonSites = \yii\helpers\Json::encode($sites);
$installUrl = \yii\helpers\Url::to(['/main/install']);
$js = <<<JS
if(chrome && chrome.runtime && chrome.runtime.sendMessage){
    chrome.runtime.sendMessage('{$extension_id}', {cmd: 'sendSites', data: JSON.parse(`{$jsonSites}`)}, r => {
        console.log('sended', r);
        if(!r) location.replace('{$installUrl}');
    })
};
JS;
$this->registerJs($js);

