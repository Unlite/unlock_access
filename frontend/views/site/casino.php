<?php

use yii\bootstrap\Carousel;
use yii\widgets\ListView;
use yii\helpers\Html;
use common\models\Site;

/* @var $this yii\web\View */
/* @var $geo_code string */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $urlForm \frontend\models\forms\UrlForm */

$this->title = 'Casino';

echo $this->render('_carousel',['dataProvider' => $dataProvider]);

$country = \common\models\Geo::findOne(['code' => $geo_code])->name;
$countryWords = str_word_count($country, 1);
$countryName = '';
if (count($countryWords) > 1 && strlen($country) > 13) {
    foreach ($countryWords as $word) {
        if (ctype_upper($word[0])) $countryName .= $word[0];
    }
} else {
    $countryName = $country;
}
?>


<div class="container">
    <div class="site-category">
        <div class="col-xs-12 category-item-list">
            <div class="row">
                <div class="col-sm-6 col-lg-4">
                    <h2><?= Yii::t('front', 'All Casinos for {country}:', ['country' => $countryName]) ?></h2>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <?= $this->render('_search', ['urlForm' => $urlForm, 'geo_code' => $geo_code]); ?>
                </div>
            </div>
            <div class="row">
                <?= $this->render('_listView', [
                    'dataProvider' => $dataProvider,
                ]) ?>
            </div>
        </div>
    </div>
</div>
