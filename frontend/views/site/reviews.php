<?php
use yii\bootstrap\Carousel;

/* @var $this yii\web\View */
/* @var $model \frontend\models\Site */

$this->title = $model->name;
$promoText = explode('|', $model->getPromoText(Yii::$app->request->getCountry() ?: 'gb') ?: '');
/* @var $promo \common\models\SiteImage */
$promoSrc = $model->getSiteImages()->type('promo')->one()->src ?? '/img/header-img.png';
?>

<div class="banner" style="background-image: url('<?= $promoSrc ?>')">
    <div class="container img-text">
        <span class="text top"><?= (@$promoText[0] ?: '') ?></span>
        <span class="text center"><?= (@$promoText[1] ?: '') ?></span>
        <span class="text bottom"><?= (@$promoText[2] ?: '') ?></span>
		<?= \yii\bootstrap\Html::a('<span class="link play">' . Yii::t('front', 'Play!') . '</span>', ['/site/access', 'id' => $model->id]) ?>
        <div class="rating-box"><span class="rating <?= $model->rank ?>"><?= ($model->rating ?: '') ?></span></div>
    </div>
</div>
<div class="container info-container">

    <div class="site-category">
        <div class="row info-item-list">
            <div class="col-sm-6 col-lg-4 ">
                <div class="review-item gold">
                    <div class="border">
                        <div class="padding">
                            <div class="review-content">
                                <h3>User</h3>
                                <p>texttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttext<br/>
                                    texttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttext<br/>
                                    texttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttext<br/>
                                    texttexttexttext<br/>
                                    texttexttexttexttexttexttexttext<br/>
                                    texttexttexttexttexttexttexttext</p>
                                <div class="review-rating">5</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-4 ">
                <div class="review-item silver">
                    <div class="border">
                        <div class="padding">

                            <div class="review-content">
                            <h3>User</h3>
                            <p>texttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttext<br/>
                                texttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttext<br/>
                                texttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttext<br/>
                                texttexttexttext<br/>
                                texttexttexttexttexttexttexttext<br/>
                                texttexttexttexttexttexttexttext</p>
                            <div class="review-rating">5</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-4 ">

                <div class="review-item bronze">
                    <div class="border">
                        <div class="padding">

                            <div class="review-content">
                            <h3>User</h3>
                            <p>texttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttext<br/>
                                texttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttext<br/>
                                texttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttext<br/>
                                texttexttexttext<br/>
                                texttexttexttexttexttexttexttext<br/>
                                texttexttexttexttexttexttexttext</p>
                            <div class="review-rating">5</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <?=$this->render('_bonuses_modal',['model'=>$model])?>


</div>
