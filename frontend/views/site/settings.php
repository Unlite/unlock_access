<?php
/* @var $this yii\web\View */
/* @var $extension_id string */

$installUrl = \yii\helpers\Url::to(['/main/install']);
$js = <<<JS
(() => {
    function sendMessage(msg) {
        return new Promise(resolve => {
            chrome.runtime.sendMessage('{$extension_id}', msg,
                (resp) => {
                    resolve(resp);
                });
        })
    }

    sendMessage({cmd: "getRules"}).then(resp => {
        console.log('sended', resp);
        if(!resp) {
            location.replace('{$installUrl}');
            return;
        }
        let model = new ViewModel(resp);
        ko.applyBindings(model);
    });

    var ViewModel = function (rules) {
        var self = this;
        this.Rules = ko.observableArray(rules);
        this.removeRule = function (item) {
            sendMessage({cmd: "removeItem", itemName: item.name}).then(resp => {
                console.log("resp:", resp);
                self.Rules.remove(item);
            });
        }
    }
})();
JS;

$this->registerJs($js, \yii\web\View::POS_READY);
$this->registerJsFile('//knockoutjs.com/downloads/knockout-3.4.2.js');
?>
<div class="container">

    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                <tr>
                    <th>Rule Name</th>
                    <th></th>
                </tr>
                </thead>
                <tbody data-bind="foreach: { data: Rules, as: 'rule' }">
                <tr>
                    <td data-bind="text: rule.name"></td>
                    <td><a href='#' data-bind="click: $root.removeRule">x</a></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>
