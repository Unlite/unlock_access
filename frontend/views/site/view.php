<?php
use yii\bootstrap\Carousel;

/* @var $this yii\web\View */
/* @var $model \frontend\models\Site */

$this->title = $model->name;
$promoText = explode('|', $model->getPromoText(Yii::$app->request->getCountry() ?: 'gb') ?: '');
/* @var $promo \common\models\SiteImage */
$promo = $model->getSiteImages()->type('promo')->one() ?: null;
?>

<?=$this->render('_carousel',['model'=>$model])?>
<div class="container info-container">

    <div class="site-category">
        <div class="row info-item-list">
            <div class="col-sm-6 col-lg-4 ">
                <div class="info-item <?= $model->rank ?>">
                    <div class="border">
                        <div class="padding">
							<?php
							$carouselItems = [
								$this->render('_available_games'),
								$this->render('_software'),
							]

							?>
							<?= Carousel::widget([
								'items' => $carouselItems,
								'options' => ['class' => 'carousel slide', 'data-interval' => 'false'],
								'showIndicators' => true,
							]); ?>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-sm-6 col-lg-4 ">
                <div class="info-item <?= $model->rank ?>">
                    <div class="border">
                        <div class="padding">
							<?php
							$carouselItems = [
								$this->render('_max_win'),
								$this->render('_bonuses', ['model' => $model]),
							]

							?>
							<?= Carousel::widget([
								'items' => $carouselItems,
								'options' => ['class' => 'carousel slide', 'data-interval' => 'false'],
								'showIndicators' => true,
							]); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-4 ">

                <div class="info-item <?= $model->rank ?>">
                    <div class="border">
                        <div class="padding">
                            <div>
                                <h2>License:</h2>
                                <div class="info-button-row">
                                    <a class="custom-btn btn-white" href="#">Curacao</a>
                                    <a class="custom-btn btn-white" href="#">Malta</a>
                                    <a class="custom-btn btn-white" href="#">Belgium</a>
                                </div>
                                <h2>User review:</h2>
                                <div class="rating-bar">
                                    <div style="width: 90%">
                                        <div class="rating rating-left"></div>
                                    </div>
                                    <div style="width: 10%">
                                        <div class="rating rating-right"></div>
                                    </div>
                                </div>
                                <h2>90% - Positive</h2>
                                <div class="center-button-container">
                                    <a data-toggle="modal" data-target="#review-add" class="custom-btn btn-white custom-btn-lg" href="#">Write / Watch</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <?=$this->render('_bonuses_modal',['model'=>$model])?>
    <?=$this->render('_review_modal',['model'=>$model])?>


</div>
