<?php

namespace portal\assets;
use Yii;
use yii\web\AssetBundle;

/**
 * Main portal application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/slick.css',
		'css/slick-theme.css',
//		'js/owl-carousel/owl.carousel.min.css',
//		'js/owl-carousel/owl.theme.default.min.css',
//        'css/bootstrap.min.css', 
        'js/bootstrap-select/bootstrap-select.min.css',
//		'css/forms.css',
		'css/pagination-main.css',
//		'css/promo.css',
		'css/style.scss',
        'css/_media_portal.scss',
//		'css/registration.css',
//		'css/custom_style.css',
        

    ];
    public $js = [
//    	'js/jquery-3.3.1.min.js',
//    	'js/popper.min.js',
    	'js/bootstrap.min.js',
        'js/slick.min.js',
//    	'js/jquery.sticky-kit.min.js',
//    	'js/owl-carousel/owl.carousel.min.js',
		'js/bootstrap-select/bootstrap-select.min.js',
		'js/jquery.twbsPagination.min.js',
		'js/main.js',
//    	'js/uploader.js'
        'js/mobile-nav.js',
    	'js/popup.js',
    	'js/login-tab.js',
    	'js/filter-dropdown.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];

}
