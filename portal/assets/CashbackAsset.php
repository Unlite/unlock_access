<?php

namespace portal\assets;
use Yii;
use yii\web\AssetBundle;

/**
 * Main portal application asset bundle.
 */
class CashbackAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
		'css/slick.css',
		'css/slick-theme.css',
		'css/main.scss',
		'css/_media.scss',
    ];
    public $js = [
//    	'js/jquery-3.3.1.min.js',
    	'js/slick.min.js',
    	'js/mobile-nav.js',
    	'js/popup.js',
    	'js/login-tab.js',
    	'js/main-cashback.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];

}
