<?php

namespace portal\bootstrap;

use \common\models\queries\BonusesQuery as CommonBonusesQuery;
use \common\models\queries\SiteQuery as CommonSiteQuery;
use \common\models\queries\BonusTypeQuery as CommonBonusTypeQuery;
use \common\models\queries\CustomListQuery as CommonCustomListQuery;
use \common\models\queries\GameCategoryQuery as CommonGameCategoryQuery;
use \common\models\queries\GameProviderQuery as CommonGameProviderQuery;
use \common\models\queries\GameQuery as CommonGameQuery;
use \common\models\queries\LicensorQuery as CommonLicensorQuery;
use \common\models\queries\BlogPostQuery as CommonBlogPostQuery;
use \common\models\queries\UserCurrencyQuery as CommonUserCurrencyQuery;
use \common\models\queries\TicketQuery as CommonTicketQuery;
use common\components\Portal;
use portal\models\query\BonusesQuery;
use portal\models\query\SiteQuery;
use portal\models\query\BonusTypeQuery;
use portal\models\query\CustomListQuery;
use portal\models\query\GameCategoryQuery;
use portal\models\query\GameProviderQuery;
use portal\models\query\GameQuery;
use portal\models\query\LicensorQuery;
use portal\models\query\BlogPostQuery;
use portal\models\query\UserCurrencyQuery;
use portal\models\query\TicketQuery;
use yii\base\Application;
use yii\base\BootstrapInterface;
use yii\helpers\ArrayHelper;
use yii\widgets\LinkPager;
use portal\widgets\LinkPager as CustomLinkPager;
class Setup implements BootstrapInterface
{

	/**
	 * Bootstrap method to be called during application bootstrap stage.
	 * @param Application $app the application currently running
	 */
	public function bootstrap($app)
	{
		$container = \Yii::$container;

		// Replace \common\models\queries\BonusesQuery by \portal\models\query\BonusesQuery
		$container->set(CommonBonusesQuery::class, [
			'class' => BonusesQuery::class
		]);

		// Replace \common\models\queries\SiteQuery by \portal\models\query\SiteQuery
		$container->set(CommonSiteQuery::class, [
			'class' => SiteQuery::class
		]);

		// Replace \common\models\queries\BonusTypeQuery by \portal\models\query\BonusTypeQuery
		$container->set(CommonBonusTypeQuery::class, [
			'class' => BonusTypeQuery::class
		]);

		// Replace \common\models\queries\CustomListQuery by \portal\models\query\CustomListQuery
		$container->set(CommonCustomListQuery::class, [
			'class' => CustomListQuery::class
		]);

		// Replace \common\models\queries\GameCategoryQuery by \portal\models\query\GameCategoryQuery
		$container->set(CommonGameCategoryQuery::class, [
			'class' => GameCategoryQuery::class
		]);

		// Replace \common\models\queries\GameProviderQuery by \portal\models\query\GameProviderQuery
		$container->set(CommonGameProviderQuery::class, [
			'class' => GameProviderQuery::class
		]);

		// Replace \common\models\queries\GameQuery by \portal\models\query\GameQuery
		$container->set(CommonGameQuery::class, [
			'class' => GameQuery::class
		]);

		// Replace \common\models\queries\LicensorQuery by \portal\models\query\LicensorQuery
		$container->set(CommonLicensorQuery::class, [
			'class' => LicensorQuery::class
		]);

		// Replace \common\models\queries\BlogPostQuery by \portal\models\query\BlogPostQuery
		$container->set(CommonBlogPostQuery::class, [
			'class' => BlogPostQuery::class
		]);

		// Replace \common\models\queries\UserCurrencyQuery by \portal\models\query\UserCurrencyQuery
		$container->set(CommonUserCurrencyQuery::class, [
			'class' => UserCurrencyQuery::class
		]);

		// Replace \common\models\queries\TicketQuery by \portal\models\query\TicketQuery
		$container->set(CommonTicketQuery::class, [
			'class' => TicketQuery::class
		]);

		// Replace default settings and class for LinkPager
		$container->set(LinkPager::class, function ($container, $params, $args) {
			return new CustomLinkPager(ArrayHelper::merge([
				'prevPageCssClass' => 'page-item prev',
				'nextPageCssClass' => 'page-item next',
//				'nextPageLabel' => '',
//				'prevPageLabel' => '',
				'options' => ['class'=>'pagination'],
				'linkOptions' => ['class'=>'page-link'],
				'linkContainerOptions' => ['class'=>'page-item'],
				'prevPageHtmlOptions' => ['rel'=>'prev'],
				'nextPageHtmlOptions' => ['rel'=>'next'],
			], $args));
		});

		//Initiate portal settings
		\Yii::$app->params['portal'] = new Portal();
	}
}