<?php

namespace portal\components;

use Yii;
use yii\web\UrlManager;
use yii\web\UrlRuleInterface;

class LocaleUrlRule implements UrlRuleInterface
{

	/**
	 * @param UrlManager $manager
	 * @param string $route
	 * @param array $params
	 * @return bool|string
	 */

	public function createUrl($manager, $route, $params)
	{
		if(Yii::$app->request->get('locale') != null){
			$params['locale'] = Yii::$app->request->get('locale');
			return count($params) ? $route.'?'.http_build_query($params) : $route;
		}
		return false;  // это правило не подходит
	}

	/**
	 * @param UrlManager $manager
	 * @param \yii\web\Request $request
	 * @return bool
	 */
	public function parseRequest($manager, $request)
	{
		return false;  // это правило не подходит
	}
}