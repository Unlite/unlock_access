<?php

namespace portal\components;

use Yii;
use yii\web\UrlManager;
use yii\web\UrlRuleInterface;

class PageUrlRule implements UrlRuleInterface
{

	/**
	 * @param UrlManager $manager
	 * @param string $route
	 * @param array $params
	 * @return bool|string
	 */

	public function createUrl($manager, $route, $params)
	{
		if(isset($params['page'])){
			if($params['page'] == 1){
				unset($params['page']);
				return count($params) ? $route.'?'.http_build_query($params) : $route;
			}
		}
		return false;  // это правило не подходит
	}

	/**
	 * @param UrlManager $manager
	 * @param \yii\web\Request $request
	 * @return bool
	 */
	public function parseRequest($manager, $request)
	{
		$params = $request->getQueryParams();
		if(isset($params['page']) && $params['page'] > 1){
			\Yii::$app->view->registerMetaTag(['name'=>'robots','content'=>'noindex, follow']);
		}
		return false;  // это правило не подходит
	}
}