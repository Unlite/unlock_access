<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-portal',
    'basePath' => dirname(__DIR__),
    'language' => 'en',
    'sourceLanguage' => 'en',
    'bootstrap' => [
        'log',
        frontend\base\PluginRequestBehavior::class,
        'portal\bootstrap\Setup',
    ],
    'controllerNamespace' => 'portal\controllers',
    'defaultRoute' => 'site',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-portal',
            'cookieValidationKey' => $params['cookieValidationKey'],
            'as sypexGeo' => [
                'class' => 'omnilight\sypexgeo\GeoBehavior',
                'sypexGeo' => [
                    'database' => '@common/data/sypexgeo/SxGeo_2017_07_13.dat',
                ]
            ],
        ],
        'formatter' => [
			'class' => 'yii\i18n\Formatter',
			'currencyCode' => 'usd',
		],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => [
                'name' => '_identity',
                'httpOnly' => true,
                'domain' => $params['cookieDomain']
            ],
            'loginUrl' => ['site']
        ],
        'session' => [
            // this is the name of the session cookie used for login on the portal
            'name' => '_session',
            'cookieParams' => [
                'domain' => $params['cookieDomain'],
                'httpOnly' => true,
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'backendUrlManager' => require __DIR__ . '/url-manager.php',
        'frontendUrlManager' => require __DIR__ . '/../../frontend/config/url-manager.php',
        'portalUrlManager' => require __DIR__ . '/../../portal/config/url-manager.php',
        'urlManager' => function () {
            return Yii::$app->get('portalUrlManager');
        },
		'i18n' => [
			'translations' => [
				'front' => [
					'class' => 'yii\i18n\PhpMessageSource',
					'basePath' => '@app/messages',
					'sourceLanguage' => 'en',
				],
				'cashback' => [
					'class' => 'yii\i18n\PhpMessageSource',
					'basePath' => '@app/messages',
					'sourceLanguage' => 'en',
				],
			],
		],
        'geoIP' => [
            'class' => 'omnilight\sypexgeo\SypexGeo',
            'database' => '@common/data/sypexgeo/SxGeo_2017_07_13.dat',
        ],
		'assetManager' => [
			'converter' => 'lucidtaz\yii2scssphp\ScssAssetConverter',
		],
    ],
    'params' => $params,
    'modules' => [
        'api' => [
            'class' => 'frontend\modules\api\Module',
        ],
		'cashback' => [
            'class' => 'portal\modules\cashback\Module',
        ],
        'profile' => [
            'class' => 'common\modules\profile\Module',
        ],
    ]
];
