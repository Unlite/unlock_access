<?php

use portal\models\forms\SearchForm;

/** @var array $params */

$search = 'search/<c:('.join('|',array_keys(SearchForm::getCategories())).')>/<q:.+>';
$rules = [
	$search => 'search/index',
	['class'=>'portal\components\PageUrlRule'],
	['class'=>'portal\components\LocaleUrlRule'],
	'profile' => 'profile/dashboard/index',
	'away' => 'away/default/index',
	'sitemap.xml'=>'site/map',
	'game' => 'game/index',
	'post' => 'blog/index',
	'login' => 'site/login',
	'logout' => 'site/logout',
	'signup' => 'site/signup',
	'ajax-login' => 'site/ajax-login',
	'ajax-signup' => 'site/ajax-signup',
	'<controller>/<page:\d+>' => '<controller>/index',
	'<module>' => '<module>',
	'<controller>' => '<controller>/index',
	'<controller>/' => '<controller>/index',
	'<action>' => 'site/<action>',
	'<controller>/<action>/<page:\d+>' => '<controller>/<action>',
	'<module>/<controller>/<id:\d+>' => '<module>/<controller>/view',
	'<module:cashback>/<controller:ticket>/<status:'.join('|',\common\models\Ticket::statuses()).'>' => '<module>/<controller>',
	'<module:cashback>/<controller:ticket>/<action>' => '<module>/<controller>/<action>',
	'<module:cashback>/<controller:ticket>/<action>/<id:\d+>' => '<module>/<controller>/view',
	'<module>/<controller>/<action>/<id:\d+>' => '<module>/<controller>/<action>',
	'<module:cashback>/<controller:billing>/<code:\w{3}>/<action>/<id:\d+>' => '<module>/<controller>/<action>/',
	'<module:cashback>/<controller:billing>/<code:\w{3}>' => '<module>/<controller>/index',
	'<module:cashback>/<controller:billing>/<code:\w{3}>/<action>' => '<module>/<controller>/<action>',
	'<module:cashback>/<controller:billing>/<action>' => '<module>/<controller>/<action>',
	'<module:cashback>/<controller:casino|game>/<slug>' => '<module>/<controller>/view',
	'<module:cashback>/<controller>' => '<module>/<controller>/index',
	'<controller>/<slug>' => '<controller>/view',
	'<controller>/<slug>/<action>' => '<controller>/<action>',
	'<module>/<controller>/<action>' => '<module>/<controller>/<index>',
	'<_c:casino|game|blog>/<slug>' => '<_c>/view',
];

return [
	'class' => 'codemix\localeurls\UrlManager',
    'hostInfo' => $params['portalHostInfo'],
    'baseUrl' => '',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
	'languages' => ['en', 'fr', 'de', 'tr'],
	'enableDefaultLanguageUrlCode' => false,
    'rules' => $rules
];