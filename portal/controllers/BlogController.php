<?php

namespace portal\controllers;

use portal\models\search\BlogPostSearch;
use common\models\BlogPost;
use Yii;
use yii\data\Pagination;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * BlogPostController implements the CRUD actions for BlogPost model.
 */
class BlogController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BlogPost models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BlogPostSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,'blog');
		$pages = new Pagination(['totalCount' => $dataProvider->query->count(),'forcePageParam' => false]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'pages' => $pages,
        ]);
    }

    /**
     * Displays a single BlogPost model.
     * @param string $slug
     * @return mixed
     */
    public function actionView($slug = '')
    {
        $model = $this->findModel($slug);
        if ($model->slug === $slug) {
            return $this->render('view', [
                'model' => $this->findModel($slug)
            ]);
        } else {
            return $this->redirect(ArrayHelper::merge(Yii::$app->request->get(), ['blog/view', 'slug' => $model->slug]), 301);
        }
    }

    /**
     * Finds the BlogPost model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $slug
     * @return BlogPost the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($slug)
    {
        if (($model = BlogPost::find()->slug($slug)->one()) !== null) {
            return $model;
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
