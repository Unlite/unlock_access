<?php

namespace portal\controllers;

use portal\models\search\BonusesSearch;
use Yii;
use yii\data\Pagination;

class BonusesController extends \yii\web\Controller
{
    public function actionIndex()
    {
		$searchModel = new BonusesSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'bonuses');

		$bonusTypeProvider = new \yii\data\ActiveDataProvider([
			'query' => \common\models\BonusType::find(),
			'pagination' => false,
		]);

		$pages = new Pagination(['totalCount' => $dataProvider->query->count(),'forcePageParam' => false]);

		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'pages' => $pages,
			'bonusTypeProvider' => $bonusTypeProvider,
		]);
    }

}
