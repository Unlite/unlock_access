<?php

namespace portal\controllers;

use common\models\CustomList;
use common\models\Site;
use portal\models\search\SiteSearch;
use Yii;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

class CasinoController extends \yii\web\Controller
{
	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}

	public function actionIndex()
	{
		$searchModel = new SiteSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'site');

		$gameProviderProvider = new \yii\data\ActiveDataProvider([
			'query' => \common\models\GameProvider::find(),
			'pagination' => false,
		]);
		$licensorProvider = new \yii\data\ActiveDataProvider([
			'query' => \common\models\Licensor::find(),
			'pagination' => false,
		]);
		$customListProvider = new \yii\data\ActiveDataProvider([
			'query' => \common\models\CustomList::find()->where(['type' => CustomList::TYPE_SIDEBAR]),
			'pagination' => false,
		]);

		$pages = new Pagination(['totalCount' => $dataProvider->query->count(),'forcePageParam' => false]);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'pages' => $pages,
			'gameProviderProvider' => $gameProviderProvider,
			'licensorProvider' => $licensorProvider,
			'customListProvider' => $customListProvider,
		]);
	}

    public function actionView($slug)
    {
        $model = $this->findModel($slug);
        if ($model->slug === $slug) {
            return $this->render('view', [
                'model' => $this->findModel($slug)
            ]);
        } else {
            return $this->redirect(ArrayHelper::merge(Yii::$app->request->get(), ['casino/view', 'slug' => $model->slug]), 301);
        }
	}

    public function actionBonuses($slug)
    {
		$model = $this->findModel($slug);
		if ($model->slug === $slug) {
			return $this->render('bonuses', [
				'model' => $this->findModel($slug)
			]);
		} else {
			return $this->redirect(ArrayHelper::merge(Yii::$app->request->get(), ['casino/view', 'slug' => $model->slug]), 301);
		}
    }

    public function actionNews($slug)
    {
		$model = $this->findModel($slug);
		if ($model->slug === $slug) {
			return $this->render('news', [
				'model' => $this->findModel($slug)
			]);
		} else {
			return $this->redirect(ArrayHelper::merge(Yii::$app->request->get(), ['casino/view', 'slug' => $model->slug]), 301);
		}
    }

	public function actionReview($slug)
	{
		$model = $this->findModel($slug);
		if ($model->slug === $slug) {
			return $this->render('review', [
				'model' => $this->findModel($slug)
			]);
		} else {
			return $this->redirect(ArrayHelper::merge(Yii::$app->request->get(), ['casino/view', 'slug' => $model->slug]), 301);
		}
    }

	/**
     * Finds the Site model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $slug
     * @return Site the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($slug)
    {
        if (($model = Site::find()->slug($slug)->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
