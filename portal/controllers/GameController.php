<?php

namespace portal\controllers;

use common\models\Game;
use common\models\GameCategory;
use portal\models\search\GameSearch;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * GameController implements the CRUD actions for Game model.
 */
class GameController extends Controller
{

	public function beforeAction($action)
	{
		return $this->goHome();
	}

    /**
     * Lists all Game models.
     * @return mixed
     */
    public function actionIndex()
    {
		$searchModel = new GameSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'game');

		$gameProviderProvider = new \yii\data\ActiveDataProvider([
			'query' => \common\models\GameProvider::find(),
			'pagination' => false,
		]);
		$sideBarGameCategoryProvider = new \yii\data\ActiveDataProvider([
			'query' => GameCategory::find()->sidebar(),
			'pagination' => false,
		]);

		$pages = new Pagination(['totalCount' => $dataProvider->query->count()]);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'pages' => $pages,
			'gameProviderProvider' => $gameProviderProvider,
			'sideBarGameCategoryProvider' => $sideBarGameCategoryProvider,
		]);
    }

    /**
     * Displays a single Game model.
     * @param string $slug
     * @return mixed
     */
    public function actionView($slug = '')
    {
        $model = $this->findModel($slug);
        if ($model->slug === $slug) {
            return $this->render('view', [
                'model' => $this->findModel($slug)
            ]);
        } else {
            return $this->redirect(ArrayHelper::merge(Yii::$app->request->get(), ['game/view', 'slug' => $model->slug]), 301);
        }
   	}

    /**
     * Finds the Game model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $slug
     * @return Game the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($slug)
    {
        if (($model = Game::find()->slug($slug)->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
