<?php

namespace portal\controllers;

use backend\models\search\ReviewSearch;
use common\models\Like;
use common\models\Review;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * ReviewController implements the CRUD actions for Review model.
 */
class ReviewController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','like'],
						'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Review models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReviewSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,'review');
		$pages = new Pagination(['totalCount' => $dataProvider->query->count(),'forcePageParam' => false]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'pages' => $pages,
        ]);
    }

	/**
	 * Increase Like counter
	 * @param integer $id
	 */
	public function actionLike($id)
	{
		$like = new Like(['user_id'=>Yii::$app->user->id,'review_id' => $id]);
		if($like->save()){
			Review::updateAllCounters(['like' => 1],['id' => $id]);
		}
	}

	/**
	 * Decrease Like counter
	 * @param integer $id
	 */
	public function actionUnlike($id)
	{
		if($like = Like::findOne(['user_id'=>Yii::$app->user->id,'review_id' => $id])){
			$like->delete();
			Review::updateAllCounters(['like' => -1],['id' => $id]);
		}
	}

    /**
     * Finds the Review model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Review the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Review::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
