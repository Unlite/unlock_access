<?php

namespace portal\controllers;

use common\models\BlogPost;
use common\models\Bonuses;
use common\models\Game;
use common\models\Site;
use portal\models\forms\SearchForm;
use yii\data\Pagination;
use Yii;

class SearchController extends \yii\web\Controller
{
	/**
	 * @param $c string search category
	 * @param $q string search query
	 * @return string
	 */
    public function actionIndex($c = 'index', $q = '')
    {
		$searchForm = new SearchForm(['q'=>$q,'c'=>$c]);

		if(Yii::$app->request->post()){
			$searchForm->load(Yii::$app->request->post());
			if($q != $searchForm->q || $c != $searchForm->c){
			return $this->redirect(['/search/'.$searchForm->c.'/'.$searchForm->q]);
			}
		}

		$searchForm->search();

        return $this->render('index', [
            'searchForm' => $searchForm
        ]);
//        return $this->render(in_array($c,array_keys(SearchForm::getCategories())) ? $c :'index', [
//            'searchForm' => $searchForm
//        ]);
    }
}
