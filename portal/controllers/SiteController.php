<?php

namespace portal\controllers;

use common\models\BlogPost;
use common\models\Bonuses;
use common\models\Game;
use common\services\UserService;
use portal\models\search\BonusesSearch;
use portal\models\search\SiteSearch;
use common\models\Site;
use Yii;
use yii\base\InvalidParamException;
use yii\base\Module;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use common\models\forms\LoginForm;
use common\models\forms\PasswordResetRequestForm;
use common\models\forms\ResetPasswordForm;
use common\models\forms\SignUpForm;
use yii\web\HttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\Pagination;

class SiteController extends \yii\web\Controller
{
    /**
     * @var UserService
     */
    private $service;
    private $mapItems = [];

    public function __construct($id, Module $module, UserService $service, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

	public function behaviors()
	{
		return [
			'access' => [
                'class' => AccessControl::class,
				'only' => ['signup'],
				'rules' => [
					[
						'actions' => ['signup'],
						'allow' => true,
						'roles' => ['?'],
					],
					[
						'actions' => ['logout'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
                'class' => VerbFilter::class,
				'actions' => [
					'ajax-login' => ['post'],
				],
			],
		];
	}

	public function actionError()
	{
		$files = \yii\helpers\FileHelper::findFiles('../views/main/errors',['only'=>['*.php'],'recursive'=>false]);
		foreach ($files as $index => $file) {
			$name = substr($file, strrpos($file, '/') + 1);
			if($name == Yii::$app->getErrorHandler()->exception->statusCode.'.php'){
			return $this->render('/main/errors/'.$name);
			}
		}
	}

	public function actionIndex()
	{
		return $this->render('index');
	}

    public function actionSettings()
	{
		return $this->render('settings');
	}

	public function actionAbout()
	{
		return $this->render('index');
    }


	public function actionContact()
	{
		return $this->render('index');
    }


	public function actionFaq()
	{
		return $this->render('index');
    }


	public function actionTermsOfService()
	{
		return $this->render('index');
    }


	public function actionPrivacy()
	{
		return $this->render('index');
    }


	public function actionMap()
	{
		$cache = Yii::$app->cache;
		$data = $cache->get('sitemap.xml');
		if ($data === false) {
			$this->addModels(BlogPost::find());
			$this->addModels(Site::find());
			$this->addModels(Game::find());
			$this->addModels(Bonuses::find());
			$cache->set('sitemap.xml',$this->mapItems,86400);
		} else {
			$this->mapItems = $data;
		}

		return $this->renderXml();
    }

	/**
	 * @param Query $query
	 */
	private function addModels($query)
	{
		$host = Yii::$app->request->hostInfo;
		foreach ($query->each() as $model) {
			$item = array(
				'loc' => $host . $model->getUrl(),
			);

			if ($model->hasAttribute('updated_date_time'))
				$item['lastmod'] = $this->dateToW3C($model->updated_date_time);
			else
				$item['lastmod'] = $this->dateToW3C();

			$this->mapItems[] = $item;
		}
	}

    private function renderXml(){
		Yii::$app->response->format = Response::FORMAT_RAW;
		$headers = Yii::$app->response->headers;
		$headers->add('Content-Type', 'text/xml');

		$dom = new \DOMDocument('1.0', 'utf-8');
		$urlset = $dom->createElement('urlset');
		$urlset->setAttribute('xmlns','http://www.sitemaps.org/schemas/sitemap/0.9');
		foreach($this->mapItems as $item)
		{
			$url = $dom->createElement('url');

			foreach ($item as $key=>$value)
			{
				$elem = $dom->createElement($key);
				$elem->appendChild($dom->createTextNode($value));
				$url->appendChild($elem);
			}

			$urlset->appendChild($url);
		}
		$dom->appendChild($urlset);

		return $dom->saveXML();
	}

	private function dateToW3C($date = null)
	{
		if (is_int($date))
			return date(DATE_W3C, $date);
		else
			return date(DATE_W3C, time());
	}

	/**
	 * Logs in a user.
	 *
	 * @return mixed
	 */
	public function actionLogin()
	{
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new LoginForm();
		if ($model->load(Yii::$app->request->post()) && $model->login()) {
			return $this->goBack();
		} else {
			return $this->render('login', [
				'model' => $model,
			]);
		}
	}

	public function actionAjaxLogin()
	{
		if (Yii::$app->request->isAjax) {
			$model = new LoginForm();
			if ($model->load(Yii::$app->request->post()) && $model->login()) {
				return $this->redirect(Yii::$app->request->referrer ?: ['/main']);
			} else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return ActiveForm::validate($model);
			}
		} else {
			throw new HttpException(404, 'Page not found');
		}
	}

	/**
	 * Logs out the current user.
	 *
	 * @return mixed
	 */
	public function actionLogout()
	{
		Yii::$app->user->logout();

		return $this->goHome();
	}


	/**
	 * Signs user up.
	 *
	 * @return mixed
	 */
	public function actionSignup()
	{
		$form = new SignUpForm();
		if ($form->load(Yii::$app->request->post())) {
			if ($user = $this->service->requestSignup($form)) {
				if (Yii::$app->getUser()->login($user)) {

					return $this->goHome();
				}
			}
		}

		return $this->render('signup', [
			'model' => $form,
		]);
	}

	/**
	 * Signs user up ajax.
	 * @return mixed
	 * @throws HttpException
	 */
	public function actionAjaxSignup()
	{
		if (Yii::$app->request->isAjax) {
			$form = new SignUpForm();
			if ($form->load(Yii::$app->request->post()) && ($user = $this->service->requestSignup($form))) {
				if(Yii::$app->getUser()->login($user)) return $this->redirect(Yii::$app->request->referrer ?: ['/']);
			}
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($form);

		} else {
			throw new HttpException(404, 'Page not found');
		}
	}

	/**
	 * Requests password reset.
	 *
	 * @return mixed
	 */
	public function actionRequestPasswordReset()
	{
		$model = new PasswordResetRequestForm();
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			if ($model->sendEmail()) {
				Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

				return $this->goHome();
			} else {
				Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
			}
		}

		return $this->render('requestPasswordResetToken', [
			'model' => $model,
		]);
	}

	/**
	 * Resets password.
	 *
	 * @param string $token
	 * @return mixed
	 * @throws BadRequestHttpException
	 */
	public function actionResetPassword($token)
	{
		try {
			$model = new ResetPasswordForm($token);
		} catch (InvalidParamException $e) {
			throw new BadRequestHttpException($e->getMessage());
		}

		if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
			Yii::$app->session->setFlash('success', 'New password saved.');

			return $this->goHome();
		}

		return $this->render('resetPassword', [
			'model' => $model,
		]);
	}
}
