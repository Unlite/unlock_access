<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 07.06.2019
 * Time: 01:42:01
 */
namespace portal\models\forms;
use common\models\PaymentSystem;
use common\models\UserCurrency;
use yii\base\Model;

class PaymentForm extends Model
{
	use ValidateAmountTrait;

	public $payment_system_id;
	public $amount;
	public $total;
	public $user_currency_id;
	public $commission;
	public $commission_total;

	public function rules()
	{
		return [
			[['payment_system_id','amount'], 'required'],
			[['payment_system_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentSystem::class, 'targetAttribute' => ['payment_system_id' => 'id']],
			[['user_currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserCurrency::class, 'targetAttribute' => ['user_currency_id' => 'id']],
			[['amount'], 'number'],
			[['amount'], 'number','min' => 50],
			[['amount'], 'validateAmount', 'skipOnEmpty' => false, 'skipOnError' => false, 'params' => ['userCurrencyAttributeFrom' => 'user_currency_id']],
		];
	}

}