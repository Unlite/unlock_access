<?php
namespace portal\models\forms;

use portal\models\search\BlogPostSearch;
use portal\models\search\BonusesSearch;
use portal\models\search\GameSearch;
use portal\models\search\SiteSearch;
use yii\data\ActiveDataProvider;
use yii\base\Model;

class SearchForm extends Model
{
    public $q = '';
    public $c = '';

	/**
	 * @var $siteDataProvider SiteSearch
	 */
    public $siteDataProvider;

	/**
	 * @var $gameDataProvider GameSearch
	 */
    public $gameDataProvider;

	/**
	 * @var $bonusesDataProvider BonusesSearch
	 */
    public $bonusesDataProvider;

	/**
	 * @var $blogPostDataProvider BlogPostSearch
	 */
    public $blogPostDataProvider;


    const CAT_INDEX = 'index';
    const CAT_CASINO = 'casino';
    const CAT_GAMES = 'game';
    const CAT_BONUSES = 'bonuses';
    const CAT_BLOG = 'blog';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['q'], 'required'],
            ['q', 'string', 'min' => 3],
			['c', 'safe']
        ];
    }

	public static function getCategories()
	{
		return [
			self::CAT_INDEX => 'All categories',
			self::CAT_CASINO => 'Casino',
			self::CAT_GAMES => 'Games',
			self::CAT_BONUSES => 'Bonuses',
			self::CAT_BLOG => 'News',
		];
	}

	/**
	 * Creates data provider instances with search query applied
	 */
	public function search()
	{
			$search = new SiteSearch();
			$this->siteDataProvider = $search->search(['SiteSearch' => ['query'=>$this->q]]);

			$search = new GameSearch();
			$this->gameDataProvider = $search->search(['GameSearch' => ['query'=>$this->q]]);

			$search = new BonusesSearch();
			$this->bonusesDataProvider = $search->search(['BonusesSearch' => ['query'=>$this->q]]);

			$search = new BlogPostSearch();
			$this->blogPostDataProvider = $search->search(['BlogPostSearch' => ['query'=>$this->q]]);
    }
}