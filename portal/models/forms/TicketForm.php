<?php
namespace portal\models\forms;

use common\models\Ticket;
use common\models\TicketMessage;
use portal\models\search\SiteSearch;
use yii\base\Model;

/**
 * Class TicketForm
 * @package portal\models\forms
 * @property Ticket $ticket
 */
class TicketForm extends Model
{

	public $title;
	public $message;
	public $images;
	public $ticket;

	/**
	 * @var $siteDataProvider SiteSearch
	 */
    public $siteDataProvider;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title','message'], 'required'],
			[['title'], 'string', 'max' => 255],
			[['message'], 'string'],
			[['images'], 'file', 'maxFiles' => 3],
        ];
    }

	public function beforeValidate()
	{
		if($this->ticket){
			$this->title = $this->ticket->title;
		}
		return true;
    }

}