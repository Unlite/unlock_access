<?php
namespace portal\models\forms;
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 04.06.2019
 * Time: 23:12:45
 */

trait ValidateAmountTrait
{
	public function validateAmount($attribute, $params, \yii\validators\InlineValidator $validator)
	{
		$amount = $this->$attribute;
		$this->commission = $this::getCommissionRate();
		$userCurrency = \common\models\UserCurrency::find()->byUser(\Yii::$app->user->id)->andWhere(['id'=>$this->{$params['userCurrencyAttributeFrom']}])->one();
		$userAmount = $userCurrency ? $userCurrency->amount : 0;
		$code = $userCurrency->currency_code;
		if ((float)$amount > (float)$userAmount) {
			$miss = (float)$amount - (float)$userAmount;
			$validator->addError($this, $attribute, "You don't have enough money. You are missing {$miss} {$code}");
		}
		$this->commission_total = ((float)$amount * (float)$this->commission);
		$this->total = $amount - ((float)$amount * (float)$this->commission);
	}

}