<?php
namespace portal\models\forms\payments;

use common\components\balance\WithdrawFormInterface;
use common\models\Currency;
use portal\models\forms\PaymentForm;
use yii\helpers\ArrayHelper;

class DebitCardsForm extends PaymentForm implements WithdrawFormInterface
{
	public $cardNumber;
	public $expiryYear;
	public $expiryMonth;
	public $name;
	public $template = 'debit_cards';

	const NAME = 'Debit Cards';
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(
        	parent::rules(),
			[
				[['cardNumber','payment_system_id','amount','name','expiryYear'],'required'],
				[
					['cardNumber'],
					'k-card',
					'holderAttribute' => 'name',
					'expiryYearAttribute' => 'expiryYear',
					'expiryMonthAttribute' => 'expiryMonth',
					'validateCVV' => false,
				],
				[['name'], 'string'],
				[['expiryYear'], 'string'],
				[['expiryMonth'], 'string'],

			]
		);
    }

	public static function getMonths()
	{
		return [
			'01' => '01',
			'02' => '02',
			'03' => '03',
			'04' => '04',
			'05' => '05',
			'06' => '06',
			'07' => '07',
			'08' => '08',
			'09' => '09',
			'10' => '10',
			'11' => '11',
			'12' => '12',
		];
    }

	public static function getYears()
	{
		$years = [];
		$year = date("Y");
		$year = (int) $year;
		for ($i = 0; $i < 15 ; $i++){
			$years[$year + $i] = $year+$i;
		}
		return $years;
    }

	/**
	 * @inheritdoc
	 */
	public static function getName() : string {
    	return self::NAME;
	}

	/**
	 * @inheritdoc
	 */
	public function getId() : int {
    	return $this->payment_system_id;
	}

	/**
	 * @inheritdoc
	 */
	public static function getCommissionRate() : float {
		return 0.05;
	}

	/**
	 * @inheritdoc
	 */
	public static function getAvailableCurrencies() : array {
		return Currency::find()->system()->all();
	}

	/**
	 * @inheritdoc
	 */
	public function getAmount() : float {
		return $this->amount;
	}

	/**
	 * @inheritdoc
	 */
	public function getExtraData() : array {
		return [
			'Card Number' => $this->cardNumber,
			'Name' => $this->name,
			'Expired Year' => $this->expiryYear,
			'Expired Month' => $this->expiryMonth,
			'Amount' => $this->amount,
		];
	}

}