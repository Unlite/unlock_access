<?php
namespace portal\models\forms\payments;

use common\components\balance\WithdrawFormInterface;
use common\models\Currency;
use portal\models\forms\PaymentForm;
use portal\models\forms\ValidateAmountTrait;
use yii\helpers\ArrayHelper;

class PayoneerForm extends PaymentForm implements WithdrawFormInterface
{
	use ValidateAmountTrait;

	public $email;
	public $template = 'payoneer';

	const NAME = 'Payoneer';
    /**
     * @inheritdoc
     */
    public function rules()
    {
		return ArrayHelper::merge(
			parent::rules(),[
				[['email'], 'required'],
				[['email'], 'email'],
				[['email'], 'string'],
			]
		);
    }


	/**
	 * @inheritdoc
	 */
	public static function getName() : string {
    	return self::NAME;
	}

	/**
	 * @inheritdoc
	 */
	public function getId() : int {
    	return $this->payment_system_id;
	}

	/**
	 * @inheritdoc
	 */
	public static function getCommissionRate() : float {
		return 0.06;
	}

	/**
	 * @inheritdoc
	 */
	public static function getAvailableCurrencies() : array {
		return Currency::find()->system()->all();
	}

	/**
	 * @inheritdoc
	 */
	public function getAmount() : float {
		return $this->amount;
	}

	/**
	 * @inheritdoc
	 */
	public function getExtraData() : array {
		return [
			'Amount' => $this->amount,
			'Email' => $this->email,
		];
	}

}