<?php
namespace portal\models\forms\payments;

use common\components\balance\WithdrawFormInterface;
use common\models\Currency;
use portal\models\forms\PaymentForm;
use yii\helpers\ArrayHelper;

class WebMoneyForm extends PaymentForm implements WithdrawFormInterface
{
	public $wallet_id;
	public $template = 'web_money';

	const NAME = 'WebMoney';
    /**
     * @inheritdoc
     */
    public function rules()
    {
		return ArrayHelper::merge(
			parent::rules(),[
				[['wallet_id'], 'required'],
				['wallet_id','match','pattern'=>'/^[A-z][0-9]{12}\z/'],
				[['wallet_id'], 'string'],
			]
		);
    }


	/**
	 * @inheritdoc
	 */
	public static function getName() : string {
    	return self::NAME;
	}

	/**
	 * @inheritdoc
	 */
	public function getId() : int {
    	return $this->payment_system_id;
	}

	/**
	 * @inheritdoc
	 */
	public static function getCommissionRate() : float {
		return 0;
	}

	/**
	 * @inheritdoc
	 */
	public static function getAvailableCurrencies() : array {
		return Currency::find()->system()->all();
	}

	/**
	 * @inheritdoc
	 */
	public function getAmount() : float {
		return $this->amount;
	}

	/**
	 * @inheritdoc
	 */
	public function getExtraData() : array {
		return [
			'Wallet' => $this->wallet_id,
			'Amount' => $this->amount,
		];
	}

	public function afterValidate()
	{
		$this->wallet_id = strtoupper($this->wallet_id);
		return true;
	}

}