<?php
namespace portal\models\forms\payments;

use common\components\balance\WithdrawFormInterface;
use common\models\Currency;
use common\models\Geo;
use portal\models\forms\PaymentForm;
use portal\models\forms\ValidateAmountTrait;
use yii\helpers\ArrayHelper;

class WireTransferForm extends PaymentForm implements WithdrawFormInterface
{
	use ValidateAmountTrait;

	public $email;
	public $phone;
	public $firstName;
	public $middleName;
	public $lastName;
	public $company;
	public $address;
	public $address2;
	public $city;
	public $zip;
	public $country;

	public $accountNumber;
	public $swift;
	public $bikCode;
	public $correspondentAccount;
	public $inn;
	public $kpp;
	public $bankName;
	public $bankAddress;
	public $bankAddress2;
	public $bankCityProvinceZip;

	public $template = 'wire_transfer';

	const NAME = 'Wire Transfer';
    /**
     * @inheritdoc
     */
    public function rules()
    {
		return ArrayHelper::merge(
			parent::rules(),[
				[[
					'email','phone','firstName','middleName','lastName','company','address','address2','city','zip','country',
					'accountNumber','swift','bikCode','correspondentAccount','inn','kpp','bankName','bankAddress','bankAddress2','bankCityProvinceZip'
				], 'required'],
				[['email'], 'email'],
				[['accountNumber'], 'string', 'length' => [12, 12]],
				[['accountNumber'], 'match', 'pattern' => '/^[0-9]*$/i'],
				[['country'], 'exist', 'skipOnError' => true, 'targetClass' => Geo::class, 'targetAttribute' => ['country' => 'code']],
				[['email','phone','firstName','middleName','lastName','company','address','address2','city','zip','country',], 'string'],
			]
		);
    }


	/**
	 * @inheritdoc
	 */
	public static function getName() : string {
    	return self::NAME;
	}

	/**
	 * @inheritdoc
	 */
	public function getId() : int {
    	return $this->payment_system_id;
	}

	/**
	 * @inheritdoc
	 */
	public static function getCommissionRate() : float {
		return 0.06;
	}

	/**
	 * @inheritdoc
	 */
	public static function getAvailableCurrencies() : array {
		return Currency::find()->system()->all();
	}

	/**
	 * @inheritdoc
	 */
	public function getAmount() : float {
		return $this->amount;
	}

	/**
	 * @inheritdoc
	 */
	public function getExtraData() : array {
		return [
			'Amount' => $this->amount,
			'email' => $this->email,
			'phone' => $this->phone,
			'firstName' => $this->firstName,
			'middleName' => $this->middleName,
			'lastName' => $this->lastName,
			'company' => $this->company,
			'address' => $this->address,
			'address2' => $this->address2,
			'city' => $this->city,
			'zip' => $this->zip,
			'country' => $this->country,
			'accountNumber' => $this->accountNumber,
			'swift' => $this->swift,
			'bikCode' => $this->bikCode,
			'correspondentAccount' => $this->correspondentAccount,
			'inn' => $this->inn,
			'kpp' => $this->kpp,
			'bankName' => $this->bankName,
			'bankAddress' => $this->bankAddress,
			'bankAddress2' => $this->bankAddress2,
			'bankCityProvinceZip' => $this->bankCityProvinceZip,
		];
	}

}