<?php

namespace portal\models\query;

use common\models\BlogPost;

/**
 * Class BlogPostQuery
 * @package common\models\queries
 * @see \common\models\BlogPost
 *
 * @method BlogPost[] all($db = null)
 * @method BlogPost one($db = null)
 */
class BlogPostQuery extends \common\models\queries\BlogPostQuery
{
	public function init()
	{
		parent::init();
		return $this->active();
	}
}