<?php

namespace portal\models\query;

/**
 * Class BonusesTypeQuery
 * @inheritdoc
 * @package portal\models\query
 */
class BonusTypeQuery extends \common\models\queries\BonusTypeQuery
{
	public function init()
	{
		parent::init();
		return $this->active();
	}
}