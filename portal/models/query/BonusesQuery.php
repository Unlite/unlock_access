<?php

namespace portal\models\query;

/**
 * Class BonusesQuery
 * @inheritdoc
 * @package portal\models\query
 */
class BonusesQuery extends \common\models\queries\BonusesQuery
{
	public function init()
	{
        parent::init();
		return $this->actual()->active();
	}
}
