<?php

namespace portal\models\query;

use common\models\CustomList;
use yii\db\ActiveQuery;

/**
 * Class CustomListQuery
 * @package common\models\queries
 * @see \common\models\CustomList
 *
 * @method CustomList[] all($db = null)
 * @method CustomList one($db = null)
 */
class CustomListQuery extends \common\models\queries\CustomListQuery
{
	public function init()
	{
		parent::init();
		return $this->active();
	}
}