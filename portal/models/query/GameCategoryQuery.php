<?php

namespace portal\models\query;
use yii\db\ActiveQuery;
use common\models\GameCategory;
/**
 * This is the ActiveQuery class for [[\portal\models\Site]].
 *
 * @see \common\models\GameCategory
 */
class GameCategoryQuery extends \common\models\queries\GameCategoryQuery
{
	public function init()
	{
		parent::init();
		return $this->active();
	}

	/**
	 * @return ActiveQuery
	 */
	public function sidebar()
	{
		return $this->andWhere(['type'=>GameCategory::TYPE_SIDEBAR]);
	}
}
