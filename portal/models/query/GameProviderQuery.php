<?php

namespace portal\models\query;

/**
 * Class GameProviderQuery
 * @package common\models\queries
 * @see \common\models\GameProvider
 *
 * @method \common\models\GameProvider[] all($db = null)
 * @method \common\models\GameProvider one($db = null)
 */
class GameProviderQuery extends \common\models\queries\GameProviderQuery
{
	public function init()
	{
		parent::init();
		return $this->active();
	}
}