<?php

namespace portal\models\query;

/**
 * Class GameQuery
 * @package common\models\queries
 * @see \common\models\Game
 *
 * @method \common\models\Game[] all($db = null)
 * @method \common\models\Game one($db = null)
 */
class GameQuery extends \common\models\queries\GameQuery
{
	public function init()
	{
		parent::init();
		return $this->active();
	}
}