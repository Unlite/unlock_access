<?php

namespace portal\models\query;

/**
 * Class LicensorQuery
 * @package common\models\queries
 * @see \common\models\Licensor
 *
 * @method \common\models\Licensor[] all($db = null)
 * @method \common\models\Licensor one($db = null)
 */
class LicensorQuery extends \common\models\queries\LicensorQuery
{
	public function init()
	{
		parent::init();
		return $this->active();
	}
}