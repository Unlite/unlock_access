<?php

namespace portal\models\query;

use common\models\PaymentSystem;
use yii\db\ActiveQuery;

/**
 * Class PaymentSystemQuery
 * @package common\models\queries
 * @see \common\models\PaymentSystem
 *
 * @method \common\models\PaymentSystem[] all($db = null)
 * @method \common\models\PaymentSystem one($db = null)
 */
class PaymentSystemQuery extends \common\models\queries\PaymentSystemQuery
{
	/**
	 * @return $this
	 */
	public function init()
	{
		parent::init();
		return $this->enabled();
	}
}