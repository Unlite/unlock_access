<?php

namespace portal\models\query;

/**
 * This is the ActiveQuery class for [[\portal\models\Site]].
 *
 * @see \common\models\Site
 */
class SiteQuery extends \common\models\queries\SiteQuery
{
	public function init()
	{
        parent::init();
        return $this->typeCasino()->active();
	}
}
