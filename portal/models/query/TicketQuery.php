<?php

namespace portal\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\Ticket]].
 *
 * @see \common\models\Ticket
 */
class TicketQuery extends \common\models\queries\TicketQuery
{
    public function init()
    {
        return $this->byUser(\Yii::$app->user->id);
    }

}
