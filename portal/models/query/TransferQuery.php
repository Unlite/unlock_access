<?php

namespace portal\models\query;

/**
 * Class TransferQuery
 * @package \portal\models\query
 * @see \common\models\Transfer
 *
 * @method \common\models\Transfer[] all($db = null)
 * @method \common\models\Transfer one($db = null)
 */
class TransferQuery extends \common\models\queries\TransferQuery
{

}