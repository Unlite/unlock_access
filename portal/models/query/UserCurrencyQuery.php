<?php

namespace portal\models\query;

use yii\db\ActiveQuery;

/**
 * Class UserCurrencyQuery
 * @package \portal\models\queries
 * @see \common\models\UserCurrency
 *
 * @method \common\models\UserCurrency[] all($db = null)
 * @method \common\models\UserCurrency one($db = null)
 */
class UserCurrencyQuery extends \common\models\queries\UserCurrencyQuery
{
	public function init()
	{
		parent::init();
		return $this->byUser(\Yii::$app->user->id);
	}
}