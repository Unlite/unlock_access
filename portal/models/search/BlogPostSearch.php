<?php

namespace portal\models\search;

use common\models\queries\BlogPostBlogPostCategoryQuery;
use common\models\queries\BlogPostCategoryQuery;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\BlogPost;
use yii\data\Sort;

/**
 * BlogPostSearch represents the model behind the search form of `common\models\BlogPost`.
 */
class BlogPostSearch extends BlogPost
{
	/** @var string text search by name, aliases and more... */
	public $query;
	/** @var integer Category id */
	public $category;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
			[['category'], 'integer', 'min' => 1],
			[['query'], 'string'],
			[['query'], 'trim'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $formName = null)
    {

        $query = BlogPost::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

		$dataProvider->setSort(new Sort([
			'defaultOrder' => [
				'id' => SORT_DESC,
			],
		]));

        $this->load($params, $formName);

		$joins = [];

		if ($this->category) {
			$joins['blogPostBlogPostCategories'] = function (BlogPostBlogPostCategoryQuery $query) {
				$query->byBlogPostCategory($this->category);
			};
		}

		$query->joinWith($joins, false);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(
        	['like', 'title', $this->query]
		);

        return $dataProvider;
    }
}
