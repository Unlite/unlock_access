<?php

namespace portal\models\search;

use common\models\queries\OfferTypeBonusesQuery;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Bonuses;
use yii\data\Sort;

/**
 * BonusesSearch represents the model behind the search form about `common\models\Bonuses`.
 */
class BonusesSearch extends Model
{
    /** @var string text search by text, promo_code and more ... */
    public $query;

    /** @var integer BonusType id */
    public $bonus_type;

    /** @var integer OfferType id */
    public $offer_type;

    /** @var integer Site id */
    public $site;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bonus_type', 'site', 'offer_type'], 'integer', 'min' => 1],
            [['query'], 'string'],
            [['query'], 'trim'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @param null $formName
     * @return ActiveDataProvider
     */
    public function search($params, $formName = null)
    {
        $query = Bonuses::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort(new Sort([
            'defaultOrder' => [
                'promoted' => SORT_DESC,
            ],
            'attributes' => [
                'promoted' => [
                    'asc' => ['is_promoted' => SORT_ASC],
                    'desc' => ['is_promoted' => SORT_DESC],
                ]
            ]
        ]));

        $this->load($params, $formName);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }


		// Prepare joins. Require only if used in search
		$joins = [
			'site',
			'bonusType',
		];

		if ($this->offer_type) {
			$joins['offerTypeBonuses'] = function (OfferTypeBonusesQuery $query) {
				$query->byOfferType($this->offer_type);
			};
		}

        $query->joinWith($joins, true);


        // grid filtering conditions
        $query->andFilterWhere([
            'bonus_type_id' => $this->bonus_type,
            'site_id' => $this->site,
        ]);

        $query->andFilterWhere([
            'or',
            ['like', 'text', $this->query],
            ['like', 'promo_code', $this->query],
            ['like', 'bonus_type.name', $this->query],
            ['like', 'site.name', $this->query],
            ['like', 'site.aliases', $this->query],
        ]);

        $query->groupBy(['id']);

        return $dataProvider;
    }
}
