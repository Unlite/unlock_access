<?php

namespace portal\models\search;

use common\models\queries\GameCategorySiteQuery;
use common\models\queries\GameProviderSiteQuery;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Game;
use yii\data\Sort;

/**
 * GameSearch represents the model behind the search form about `common\models\Game`.
 */
class GameSearch extends Model
{
    /** @var string text search by name and more ... */
    public $query;

    /** @var integer GameProvider id */
    public $provider;

    /** @var integer Category id */
    public $category;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['provider', 'category'], 'integer', 'min' => 1],
            [['query'], 'string'],
            [['query'], 'trim'],
        ];
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @param null $formName
     * @return ActiveDataProvider
     */
    public function search($params, $formName = null)
    {
        $query = Game::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort(new Sort([
			'defaultOrder' => [
				'id' => SORT_DESC
			],
        ]));

        $this->load($params, $formName);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }
        // add conditions that should always apply here

        $query->joinWith([
            'gameCategoryGames'
        ]);


		if ($this->provider) {
			$joins['GameProvider'] = function (GameProviderSiteQuery $query) {
				$query->byGameProvider($this->provider);
			};
		}

		if ($this->category) {
			$joins['GameCategories'] = function (GameCategorySiteQuery $query) {
				$query->byGameCategory($this->category);
			};
		}

        // grid filtering conditions
        $query->andFilterWhere([
            'game_category_game.game_category_id' => $this->category,
            'game_provider_id' => $this->provider,
        ]);

        $query->andFilterWhere([
            'or',
            ['like', 'name', $this->query],
            ['like', 'description', $this->query],
        ]);

        $query->groupBy(['id']);

        return $dataProvider;
    }
}
