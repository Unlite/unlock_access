<?php

namespace portal\models\search;

use common\components\cashback\models\Cashback;
use common\models\queries\CustomListSiteQuery;
use common\models\queries\GameProviderSiteQuery;
use common\models\queries\SiteLicensorQuery;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Site;
use yii\data\Sort;
use yii\db\ActiveQuery;

/**
 * SiteSearch represents the model behind the search form about `portal\models\Site`.
 */
class SiteSearch extends Model
{
    /** @var string text search by name, aliases and more... */
    public $query;

    /** @var integer GameProvider id */
    public $game_provider;

    /** @var integer Licensor id */
    public $licensor;

    /** @var integer CustomList id */
    public $custom_list;

    /** @var boolean filter by available cashbacks */
    public $cashback;

    /** @var number rating number value. More or equal */
    public $rating;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['game_provider', 'licensor', 'custom_list'], 'integer', 'min' => 1],
            [['rating'], 'number', 'min' => 0, 'max' => 5],
            [['query'], 'string'],
            [['query'], 'trim'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @param null $formName
     * @return ActiveDataProvider
     */
    public function search($params, $formName = null)
    {
        $query = Site::find();

        $dataProvider = new ActiveDataProvider(['query' => $query]);

        $dataProvider->setSort(new Sort([
            'defaultOrder' => [
                'rating' => SORT_DESC,
				'id' => SORT_DESC
            ],
            'attributes' => [
                'rating',
                'id'
            ]
        ]));

        $this->load($params, $formName);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }
        // Prepare joins. Require only if used in search
        $joins = ['domains'];

        if ($this->game_provider) {
            $joins['gameProviderSites'] = function (GameProviderSiteQuery $query) {
                $query->byGameProvider($this->game_provider);
            };
        }
        if ($this->licensor) {
            $joins['siteLicensors'] = function (SiteLicensorQuery $query) {
                $query->byLicensor($this->licensor);
            };
        }
        if ($this->custom_list) {
            $joins['customListSites'] = function (CustomListSiteQuery $query) {
                $query->byCustomList($this->custom_list);
            };
        }
		if ($this->cashback) {
			$query->innerJoinWith(['cashback' => function (ActiveQuery $query) {
				$query->andWhere(['>', 'cashback.site_id', 0]);
			}]);
		}
        $query->joinWith($joins, false);

		$query->andWhere(['or', ['like','site.name',$this->query], ['like','domain.domain',$this->query]]);


        if ($this->rating) {
            $query->ratingMoreThan($this->rating, true);
        }

        $query->andFilterWhere([
            'or',
            ['like', 'site.name', $this->query],
            ['like', 'site.aliases', $this->query]
        ]);

        $query->groupBy(['site.id']);
        return $dataProvider;
    }
}
