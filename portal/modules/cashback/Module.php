<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 02.04.2019
 * Time: 18:04:19
 */
namespace portal\modules\cashback;

class Module extends \yii\base\Module{

	/**
	 * @inheritdoc
	 */
	public $controllerNamespace = 'portal\modules\cashback\controllers';

	/**
	 * @inheritdoc
	 */
	public function init()
	{
		parent::init();

		$this->layout = 'main';
		\Yii::configure($this, require(__DIR__ . '/config.php'));
	}
}