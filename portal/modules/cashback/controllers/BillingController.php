<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 02.04.2019
 * Time: 18:23:18
 */

namespace portal\modules\cashback\controllers;


use common\components\balance\Manager;
use common\components\balance\ManagerInterface;
use common\models\Exchange;
use common\models\PaymentSystem;
use common\models\UserCurrency;
use common\modules\profile\models\forms\ExchangeForm;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use Yii;
use yii\web\Response;
use yii\web\View;
use yii\widgets\ActiveForm;
use common\models\Currency;

class BillingController extends Controller
{

	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => false,
						'roles' => ['?'],
					],
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	public function actionIndex($code = null)
	{
		if($code == null)
			$code = Yii::$app->formatter->currencyCode;
		$userCurrencyDataProvider = new ActiveDataProvider(['query' => UserCurrency::find()]);
		$model = $this->findOne($code);
		$userTransferDataProvider = new ActiveDataProvider(['query' => $model->getWithdraws()]);
		return $this->render('index',[
			'model' => $model,
			'userCurrencyDataProvider' => $userCurrencyDataProvider,
			'userTransferDataProvider' => $userTransferDataProvider,
		]);
	}

	public function actionPayment($code)
	{
		$userCurrencyDataProvider = new ActiveDataProvider(['query' => UserCurrency::find()]);
		$model = $this->findOne($code);
		$userTransferDataProvider = new ActiveDataProvider(['query' => $model->getWithdraws()]);
		return $this->render('payment',[
			'model' => $model,
			'userCurrencyDataProvider' => $userCurrencyDataProvider,
			'userTransferDataProvider' => $userTransferDataProvider,
			'code' => $code,
		]);
	}

	/**
	 * @param string $id PaymentSystem
	 * @param string $code PaymentSystem name
	 * @throws NotFoundHttpException
	 * @return ActiveForm::validate()
	 */
	public function actionPaymentForm($id,$code)
	{
		if($payment = PaymentSystem::findOne($id)){
			$payment_name =str_replace(' ','',$payment->name);
			$class = 'portal\models\forms\payments\\' . $payment_name . 'Form';
		}
		if(class_exists($class)){
			$form = new $class();
			$currency = UserCurrency::find()->where(['user_id'=>Yii::$app->user->id,'currency_code' => $code])->one();
			Yii::$app->response->format = Response::FORMAT_JSON;
			$form->payment_system_id = $id;
			$form->user_currency_id = $currency->id;

			if (Yii::$app->request->isAjax && Yii::$app->request->isPost && $form->load(Yii::$app->request->post())) {
				if ($form->validate()) {
					return ['result'=>'success','data' => $this->renderPartial('payment-check/'.$form->template,['model' => $form])];
				} else {
					return ActiveForm::validate($form);
				}
			} else {
				Yii::$app->response->format = Response::FORMAT_HTML;
				return $this->renderPartial('payment-form/'.$form->template,['model' => $form]);
			}
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	/**
	 * @param string $id PaymentSystem
	 * @param string $code PaymentSystem name
	 * @throws NotFoundHttpException
	 * @return ActiveForm::validate()
	 */
	public function actionPaymentSubmit($id,$code)
	{
		if($payment = PaymentSystem::findOne($id)){
			$payment_name =str_replace(' ','',$payment->name);
			$class = 'portal\models\forms\payments\\' . $payment_name . 'Form';
		}
		if(class_exists($class)){
			$form = new $class();
			$currency = UserCurrency::find()->where(['user_id'=>Yii::$app->user->id,'currency_code' => $code])->one();
			Yii::$app->response->format = Response::FORMAT_JSON;
			if (Yii::$app->request->isAjax && Yii::$app->request->isPost && $form->load(Yii::$app->request->post()) && $currency) {
				$form->user_currency_id = $currency->id;
				if ($form->validate()) {
					/** @var Manager $balanceManager */
					$balanceManager = \Yii::$app->get('balanceManager');
					$balanceManager->withdraw($currency, $form);
					return ['result'=>'success','data' => $this->renderPartial('payment-success/'.$form->template,['model'=>$form])];
				} else {
					return ActiveForm::validate($form);
				}
			}
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	public function actionExchange($code)
	{
		if ($code && $currency = Currency::find()->system()->andWhere(['code' => $code])->one()) {
			$userCurrencyDataProvider = new ActiveDataProvider(['query' => UserCurrency::find()]);
			$userCurrency = UserCurrency::find()->byUser(\Yii::$app->user->id)->byCurrency($currency->code)->one();
			if (!$userCurrency) {
				$userCurrency = new UserCurrency();
				$userCurrency->user_id = \Yii::$app->user->id;
				$userCurrency->currency_code = $currency->code;
				$userCurrency->save(false);
			}

			$userTransferDataProvider = new ActiveDataProvider(['query' => Exchange::find()->byParent(null)->andWhere(['in','user_currency_id',ArrayHelper::getColumn(UserCurrency::find()->select('id')->byUser(\Yii::$app->user->id)->all(),'id')])]);

			$form = new ExchangeForm();
			if (Yii::$app->request->isAjax && Yii::$app->request->isPost && $form->load(Yii::$app->request->post())) {
				Yii::$app->response->format = Response::FORMAT_JSON;
				if ($form->validate()) {
					return ['result' => 'success', 'data' => $this->renderPartial('exchange/_check', ['model' => $form])];
				} else {
					return ActiveForm::validate($form);
				}
			}
			return $this->render('exchange', [
				'model' => $userCurrency,
				'userCurrencyDataProvider' => $userCurrencyDataProvider,
				'userTransferDataProvider' => $userTransferDataProvider,
				'form' => $form,
				'code' => $code,
			]);
		}
	}

	public function actionExchangeSubmit()
	{
		$form = new ExchangeForm();

		if (Yii::$app->request->isAjax && Yii::$app->request->isPost && $form->load(Yii::$app->request->post()) && $currency = Currency::find()->system()->andWhere(['code' => $form->exchange_from])->one()) {
			$userCurrency = UserCurrency::find()->byUser(\Yii::$app->user->id)->byCurrency($currency->code)->one();
			if (!$userCurrency) {
				$userCurrency = new UserCurrency();
				$userCurrency->user_id = \Yii::$app->user->id;
				$userCurrency->currency_code = $currency->code;
				$userCurrency->save(false);
			}

			Yii::$app->response->format = Response::FORMAT_JSON;
			if($form->validate()){
				/** @var ManagerInterface $balanceManager */
				$balanceManager = \Yii::$app->get('balanceManager');
				$balanceManager->exchange($userCurrency, $form->exchange_to, $form->amount);
				return ['result'=>'success','data' => $this->renderPartial('exchange/_success',['model'=>$form])];
			} else {
				return ActiveForm::validate($form);
			}
		}
	}

	public function actionView()
	{
		$model = '';
		return $this->render('view',['model' => $model]);
	}

	/**
	 * @param $code
	 * @return UserCurrency|null
	 * @throws NotFoundHttpException
	 */
	public function findOne($code)
	{
		if (($model = UserCurrency::find()->byCurrency($code)->one()) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}