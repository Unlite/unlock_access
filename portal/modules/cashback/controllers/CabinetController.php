<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 02.04.2019
 * Time: 18:23:18
 */

namespace portal\modules\cashback\controllers;


use common\components\cashback\models\queries\CashbackStatQuery;
use common\models\Client;
use common\models\UserCurrency;
use yii\base\Controller;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\ActiveQuery;
use yii\filters\AccessControl;

class CabinetController extends Controller
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => false,
						'roles' => ['?'],
					],
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	public function actionIndex()
	{
		$userCurrency = UserCurrency::find()->byUser(\Yii::$app->user->id)->byCurrency('usd')->one();
		$client = Client::find()->byUser(\Yii::$app->user->id)->one();
		$cashbackCustomersDataProvider = new ActiveDataProvider(['query' => $client->getCashbackCustomers()]);
		$cashbackStat = $client->cashbackStats;


		return $this->render('index',[
			'userCurrency' => $userCurrency,
			'cashbackStat' => $cashbackStat,
			'cashbackCustomersDataProvider' => $cashbackCustomersDataProvider,
		]);
	}
	public function actionView()
	{
		$model = '';
		return $this->render('view',['model' => $model]);
	}
}