<?php

namespace portal\modules\cashback\controllers;

use common\models\Site;
use portal\models\search\SiteSearch;
use Yii;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class CasinoController extends \yii\web\Controller
{
	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}

	public function actionIndex()
	{
		$searchModel = new SiteSearch(['cashback'=>true]);

		$dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'site');

		$pages = new Pagination(['totalCount' => $dataProvider->query->count()]);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'pages' => $pages,
		]);
	}

    public function actionView($slug)
    {
        $model = $this->findModel($slug);
        if ($model->slug === $slug) {
            return $this->render('view', [
                'model' => $model
            ]);
        } else {
            return $this->redirect(ArrayHelper::merge(Yii::$app->request->get(), ['casino/view', 'slug' => $model->slug]), 301);
        }
	}

	/**
     * Finds the Site model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $slug
     * @return Site the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($slug)
    {
        if (($model = Site::find()->slug($slug)->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
