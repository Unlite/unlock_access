<?php

/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 02.04.2019
 * Time: 18:05:07
 */
namespace portal\modules\cashback\controllers;

use yii\filters\AccessControl;

class SiteController extends \yii\web\Controller
{

	public function actionIndex()
	{
		if(\Yii::$app->user->isGuest)
		return $this->render('index');
		else $this->redirect(['/cashback/cabinet']);
	}
}