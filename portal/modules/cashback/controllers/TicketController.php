<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 02.04.2019
 * Time: 18:23:18
 */

namespace portal\modules\cashback\controllers;


use backend\models\search\TicketSearch;
use common\models\TicketMessage;
use common\services\TicketService;
use portal\models\forms\TicketForm;
use Yii;
use common\models\Ticket;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class TicketController extends Controller
{

	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => false,
						'roles' => ['?'],
					],
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	/**
	 * Lists all Ticket models.
	 * @return mixed
	 */
	public function actionIndex($status = null)
	{
		$searchModel = new TicketSearch(['status' => $status]);
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$model = new TicketForm();

		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$ticketService = new TicketService();
			if(!$ticketService->createTicket($model)->hasErrors()){
				Yii::$app->session->setFlash('Your ticket was created successfully');
				$this->redirect(['/cashback/ticket/view','id'=>$model->ticket->id]);
			}
		}

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'model' => $model,
		]);
	}

	/**
	 * Displays a single Ticket model.
	 * @param integer $id
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView($id)
	{
		$searchModel = new TicketSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$model = new TicketForm();
		$model->ticket = $this->findModel($id);
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$ticketService = new TicketService();
			if(!$ticketService->createTicket($model)->hasErrors()){
				Yii::$app->session->setFlash('Your ticket was created successfully');
			}
			$model = new TicketForm();
			$model->ticket = $this->findModel($id);
		}
		return $this->render('view', [
			'dataProvider' => $dataProvider,
			'model' => $model,
		]);
	}

	public function actionClose($id)
	{
		if($model = $this->findModel($id)){
			$model->status = Ticket::STATUS_CLOSED;
			$model->save();
		}
		$this->redirect(['/cashback/ticket']);
	}

	/**
	 * Finds the Ticket model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Ticket the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Ticket::findOne($id)) !== null) {
			return $model;
		}

		throw new NotFoundHttpException('The requested page does not exist.');
	}
}