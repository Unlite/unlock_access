<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 02.04.2019
 * Time: 18:24:57
 * @var \common\models\UserCurrency $model
 * @var \yii\web\View $this
 * @var string $code
 */
?>
    <div class="choosen-wallet">
        <h2 class="choosen-wallet-title"><?=Yii::t('cashback','Balance of {code} wallet',['code' => $model->currency_code])?></h2>
        <div class="choosen-wallet-middle">
            <div class="choosen-wallet-currency"><?=\portal\components\CurrencySymbolHelper::symbol($model->currency_code)?></div>
            <div class="choosen-wallet-balance"><?=Yii::$app->formatter->asDecimal($model->amount,2)?></div>
        </div>
        <div class="choosen-wallet-buttons">
            <a href="<?=\yii\helpers\Url::to(['/cashback/billing/payment','code'=>$model->currency_code])?>" class="choosen-wallet-button"><?=Yii::t('cashback','Make Payment')?></a>
            <a href="<?=\yii\helpers\Url::to(['/cashback/billing/exchange','code'=>$model->currency_code])?>" class="choosen-wallet-button"><?=Yii::t('cashback','Exchange Money')?></a>
        </div>
    </div>
