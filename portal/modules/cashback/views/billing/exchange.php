<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 02.04.2019
 * Time: 18:24:57
 * @var \yii\data\ActiveDataProvider $userCurrencyDataProvider
 * @var \yii\data\ActiveDataProvider $userTransferDataProvider
 * @var \common\models\UserCurrency $model
 * @var \yii\web\View $this
 * @var string $code
 */

use yii\widgets\ListView;
?>
<?=$this->render('user-currency/index',['dataProvider' => $userCurrencyDataProvider])?>
<div class="payments">
	<div class="payments-wrap">
		<div class="payments-left">
			<div class="payments-left-wrapper">
				<?=$this->render('_wallet',['model' => $model]); ?>
			</div>
			<div class="history">
				<div class="history-wrapper">
					<h2 class="history-title"><?=Yii::t('cashback','Your Exchange Amount History')?></h2>

					<table class="history-table" cellspacing="10" cellpadding="5">
						<tbody>
						<tr>
							<th><?=Yii::t('cashback','date')?></th>
							<th><?=Yii::t('cashback','from')?></th>
							<th><?=Yii::t('cashback','to')?></th>
							<th><?=Yii::t('cashback','commission')?></th>
							<th><?=Yii::t('cashback','status')?></th>
						</tr>
						<?=ListView::widget([
							'dataProvider' => $userTransferDataProvider,
							'itemView' => 'transfer/_exchange',
							'emptyText' => "",
							'options' => [
								'class' => 'wallets-row',
							],
							'summary' => '',
							'layout' => '{items}'
						]);
						?>
						</tbody>
					</table>
<!--					<button class="history-all">Show all</button>-->
				</div>
			</div>
		</div>
		<div class="payments-right">
			<!--         with data input-->
			<div class="pay">
				<ul class="pay-breadcrumb">
					<li class="pay-breadcrumb-item pay-breadcrumb-item--active"><?=Yii::t('cashback','Data input')?></li>
					<li class="pay-breadcrumb-item"><?=Yii::t('cashback','Confirmation')?></li>
					<li class="pay-breadcrumb-item"><?=Yii::t('cashback','Result')?></li>
				</ul>
				<div class="pay-wrapper">
					<div id="form">
						<?=$this->render('exchange/_form',['model'=>$form])?>
                    </div>
                    <div id="exchange-check">

                    </div>

					<div id="exchange-result">

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->registerJsFile("/js/payment.js");?>
<?php $this->registerJs(<<<JS

slider = new Slider(
    {
        selector:'.pay-wrapper',
        check: {
            selector:'#exchange-check',
            confirm_selector:'.confirm-submit',
        },
        result: {
            selector:'#exchange-result',
        }
    },
    {
        selector:'.pay-breadcrumb',
        active_class:'pay-breadcrumb-item--active',
    },   
    {
        selector:'#form form',
        handler:'#form',
        currency:'$code',
    },

);
JS
); ?>