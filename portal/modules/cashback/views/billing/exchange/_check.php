<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 30.04.2019
 * Time: 00:15:54
/* @var $this yii\web\View */
use portal\components\CurrencySymbolHelper;
/* @var $model \common\modules\profile\models\forms\ExchangeForm */
?>


<div id="exchange-confirm">
    <div class="confirm">
        <ul class="confirm-list">
            <li class="confirm-item">
                <p class="confirm-subtitle"><?=Yii::t('cashback','Exchange From')?></p>
                <p class="confirm-info"><?=$model->exchange_from_currency->name?></p>
            </li>
            <li class="confirm-item">
                <p class="confirm-subtitle"><?=Yii::t('cashback','Exchange To')?></p>
                <p class="confirm-info"><?=$model->exchange_to_currency->name?></p>
            </li>
            <li class="confirm-item">
                <p class="confirm-subtitle"><?=Yii::t('cashback','Amount')?></p>
                <p class="confirm-info"><?=Yii::$app->formatter->asCurrency($model->amount,$model->exchange_from)?></p>
            </li>
            <li class="confirm-item">
                <p class="exchange-rate"><?=Yii::t('cashback','Exchange rate: {currency_from} = {currency_to}',['{currency_from}' => CurrencySymbolHelper::symbol($model->exchange_from_currency->code).'1','{currency_to}' => Yii::$app->formatter->asCurrency(1*$model->exchange_rate,$model->exchange_to) ])?> </p>
                <p class="confirm-subtitle"><?=Yii::t('cashback','Exchange Amount')?></p>
                <p class="confirm-info"><?=Yii::$app->formatter->asCurrency($model->exchange_amount,$model->exchange_to)?></p>
            </li>
            <li class="confirm-item">
                <p class="exchange-rate"><?=Yii::t('cashback','Commission: {commission}%',['{commission}' => $model->commission * 100])?></p>
                <p class="confirm-subtitle"><?=Yii::t('cashback','Commission')?></p>
                <p class="confirm-info"><?=Yii::$app->formatter->asCurrency(1*$model->commission_amount,$model->exchange_to)?></p>
            </li>
        </ul>
        <div class="confirm-buttons">
            <button class="confirm-close" onclick="slider.prevSlide()"><?=Yii::t('cashback','Close')?></button>
            <button class="confirm-submit" type="submit" onclick="slider.submit('<?=\yii\helpers\Url::to(['/cashback/billing/exchange-submit'])?>')"><?=Yii::t('cashback','Submit')?></button>
        </div>
    </div>
</div>