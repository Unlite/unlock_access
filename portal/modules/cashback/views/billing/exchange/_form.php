<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 30.04.2019
 * Time: 00:15:54
/* @var $this yii\web\View */
/* @var $model \common\modules\profile\models\forms\ExchangeForm */
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\UserCurrency;
?>

<?php $form = ActiveForm::begin(["class"=>"pay-content", "id"=>"payment-exchange","enableClientValidation"=>false]); ?>

	<ul class="exchange">
		<?= $form->field($model, 'exchange_from',['options' => ['tag'=> 'li','class'=>'exchange-item']])
            ->dropDownList(ArrayHelper::map(
                    \common\models\Currency::find()->andWhere([
                            'in',
                            'code',
                            ArrayHelper::getColumn(\common\models\UserCurrency::find()->select('currency_code')->byUser(Yii::$app->user->id)->all(),'currency_code'),
                        ])->all(),
                        'code',
                        'name'
            ),
                ['class' => 'exchange-select'])->label(Yii::t('cashback','Exchange from'),['class' => 'exchange-subtitle']) ?>
		<?= $form->field($model, 'exchange_to',['options' => ['tag'=> 'li','class'=>'exchange-item']])->dropDownList(ArrayHelper::map(\common\models\Currency::find()->system()->all(),'code','name'),['class' => 'exchange-select'])->label(Yii::t('cashback','Exchange to'),['class' => 'exchange-subtitle']) ?>
		<?= $form->field($model, 'amount',['options' => ['tag'=> 'li','class'=>'exchange-item']])->textInput(['class' => 'exchange-input','type'=>'number','step' => '0.01'])->label(Yii::t('cashback','Amount'),['class' => 'data-subtitle']) ?>
	</ul>
	<div class="data-buttons">
		<?= Html::submitButton(Yii::t('cashback','Submit'), ['class' => 'data-submit']) ?>
	</div>

<?php ActiveForm::end(); ?>