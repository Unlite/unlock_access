<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 30.04.2019
 * Time: 00:15:54
/* @var $this yii\web\View */
/* @var $model \portal\models\forms\ExchangeForm */
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\UserCurrency;
?>

<div class="result">
    <div class="result-wrap">
        <div class="result-img result-img--success"></div>
        <p class="result-text result-text--success">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
    </div>
</div>