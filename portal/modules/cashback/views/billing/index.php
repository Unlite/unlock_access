<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 02.04.2019
 * Time: 18:24:52
 * @var \yii\data\ActiveDataProvider $userCurrencyDataProvider
 * @var \yii\data\ActiveDataProvider $userTransferDataProvider
 * @var \common\models\UserCurrency $model
 */

use yii\widgets\ListView;

?>
<?=$this->render('user-currency/index',['dataProvider' => $userCurrencyDataProvider])?>
<div class="payments">
    <div class="payments-wrapper">
        <div class="container">
			<?=$this->render('_wallet',['model' => $model]); ?>
        </div>
        <div class="history">
            <div class="container">
                <h2 class="history-title"><?=Yii::t('cashback','Your Payment Amount History')?></h2>

                <table class="history-table" cellspacing="10" cellpadding="5">
                    <tbody>
                        <col border="1"  height="100%" span="4">
                        <tr>
                            <th><?=Yii::t('cashback','date')?></th>
                            <th><?=Yii::t('cashback','payment method')?></th>
                            <th><?=Yii::t('cashback','summary')?></th>
                            <th><?=Yii::t('cashback','status')?></th>
                        </tr>
						<?=ListView::widget([
							'dataProvider' => $userTransferDataProvider,
							'itemView' => 'transfer/_withdraw',
							'emptyText' => "",
							'options' => [
								'class' => 'wallets-row',
							],
							'summary' => '',
							'layout' => '{items}'
						]);
						?>
                    </tbody>
                </table>
<!--                <button class="history-all">Show all</button>-->
            </div>
        </div>
    </div>
</div>
