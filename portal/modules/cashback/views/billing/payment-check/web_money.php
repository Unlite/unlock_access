<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 23.04.2019
 * Time: 17:11:21
 * @var $model \portal\models\forms\payments\WebMoneyForm
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$payment_system = \common\models\PaymentSystem::findOne($model->payment_system_id);
?>
<div class="confirm">
    <ul class="confirm-list">
        <li class="confirm-item" id="payment-method">
            <p class="confirm-subtitle"><?=Yii::t('cashback','Payment Method')?></p>
            <p class="confirm-info"><?=$payment_system->name?></p>
        </li>
        <li class="confirm-item" id="payment-card">
            <p class="confirm-subtitle"><?=Yii::t('cashback','Wallet ID')?></p>
            <p class="confirm-info"><?=$model->wallet_id?></p>
        </li>
        <li class="confirm-item" id="payment-amount">
            <p class="confirm-subtitle"><?=Yii::t('cashback','Amount')?></p>
            <p class="confirm-info"><?=Yii::$app->formatter->asCurrency($model->amount,Yii::$app->request->get('code'))?></p>
        </li>
        <li class="confirm-item">
            <p class="exchange-rate"><?=Yii::t('cashback','Commission: {commission}%',['{commission}' => $model->commission * 100])?></p>
            <p class="confirm-subtitle"><?=Yii::t('cashback','Commission')?></p>
            <p class="confirm-info"><?=Yii::$app->formatter->asCurrency(1*$model->commission_total,Yii::$app->request->get('code'))?></p>
        </li>
        <li class="confirm-item" id="payment-amount">
            <p class="confirm-subtitle"><?=Yii::t('cashback','Total')?></p>
            <p class="confirm-info"><?=Yii::$app->formatter->asCurrency($model->total,Yii::$app->request->get('code'))?></p>
        </li>
    </ul>
    <div class="confirm-buttons">
        <button class="confirm-close" onclick="slider.prevSlide()"><?=Yii::t('cashback','Close')?></button>
        <button class="confirm-submit" onclick="slider.submit('<?=\yii\helpers\Url::to(['/cashback/billing/payment-submit','code'=>Yii::$app->request->get('code'),'id'=>$model->payment_system_id])?>')"><?=Yii::t('cashback','Submit')?></button>
    </div>
</div>