<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 23.04.2019
 * Time: 17:11:21
 * @var $model \portal\models\forms\payments\WireTransferForm
 *
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$payment_system = \common\models\PaymentSystem::findOne($model->payment_system_id);
$country = \common\models\Geo::findOne($model->country);
?>
<div class="confirm">
    <ul class="confirm-list">
        <li class="confirm-item" id="payment-method">
            <p class="confirm-subtitle"><?=Yii::t('cashback','Payment Method')?></p>
            <p class="confirm-info"><?=$payment_system->name?></p>
        </li>
        <li class="confirm-item" id="payment-card">
            <p class="confirm-subtitle"><?=Yii::t('cashback','Email')?></p>
            <p class="confirm-info"><?=$model->email?></p>
        </li>
        <li class="confirm-item" id="payment-card">
            <p class="confirm-subtitle"><?=Yii::t('cashback','Phone')?></p>
            <p class="confirm-info"><?=$model->phone?></p>
        </li>
        <li class="confirm-item" id="payment-card">
            <p class="confirm-subtitle"><?=Yii::t('cashback','First Name')?></p>
            <p class="confirm-info"><?=$model->firstName?></p>
        </li>
        <li class="confirm-item" id="payment-card">
            <p class="confirm-subtitle"><?=Yii::t('cashback','Middle Name')?></p>
            <p class="confirm-info"><?=$model->middleName?></p>
        </li>
        <li class="confirm-item" id="payment-card">
            <p class="confirm-subtitle"><?=Yii::t('cashback','Last Name')?></p>
            <p class="confirm-info"><?=$model->lastName?></p>
        </li>
        <li class="confirm-item" id="payment-card">
            <p class="confirm-subtitle"><?=Yii::t('cashback','Company')?></p>
            <p class="confirm-info"><?=$model->company?></p>
        </li>
        <li class="confirm-item" id="payment-card">
            <p class="confirm-subtitle"><?=Yii::t('cashback','Address')?></p>
            <p class="confirm-info"><?=$model->address?></p>
        </li>
        <li class="confirm-item" id="payment-card">
            <p class="confirm-subtitle"><?=Yii::t('cashback','Address 2')?></p>
            <p class="confirm-info"><?=$model->address2?></p>
        </li>
        <li class="confirm-item" id="payment-card">
            <p class="confirm-subtitle"><?=Yii::t('cashback','City')?></p>
            <p class="confirm-info"><?=$model->city?></p>
        </li>
        <li class="confirm-item" id="payment-card">
            <p class="confirm-subtitle"><?=Yii::t('cashback','ZIP')?></p>
            <p class="confirm-info"><?=$model->zip?></p>
        </li>
        <li class="confirm-item" id="payment-card">
            <p class="confirm-subtitle"><?=Yii::t('cashback','Country')?></p>
            <p class="confirm-info"><?= $country ? $country->name : $model->country?></p>
        </li>
        <li class="confirm-item" id="payment-card">
            <p class="confirm-subtitle"><?=Yii::t('cashback','Account Number')?></p>
            <p class="confirm-info"><?=$model->accountNumber?></p>
        </li>
        <li class="confirm-item" id="payment-card">
            <p class="confirm-subtitle"><?=Yii::t('cashback','SWIFT')?></p>
            <p class="confirm-info"><?=$model->swift?></p>
        </li>
        <li class="confirm-item" id="payment-card">
            <p class="confirm-subtitle"><?=Yii::t('cashback','Bik code')?></p>
            <p class="confirm-info"><?=$model->bikCode?></p>
        </li>
        <li class="confirm-item" id="payment-card">
            <p class="confirm-subtitle"><?=Yii::t('cashback','Correspondent Account')?></p>
            <p class="confirm-info"><?=$model->correspondentAccount?></p>
        </li>
        <li class="confirm-item" id="payment-card">
            <p class="confirm-subtitle"><?=Yii::t('cashback','INN')?></p>
            <p class="confirm-info"><?=$model->inn?></p>
        </li>
        <li class="confirm-item" id="payment-card">
            <p class="confirm-subtitle"><?=Yii::t('cashback','KPP')?></p>
            <p class="confirm-info"><?=$model->kpp?></p>
        </li>
        <li class="confirm-item" id="payment-card">
            <p class="confirm-subtitle"><?=Yii::t('cashback','Bank Name')?></p>
            <p class="confirm-info"><?=$model->bankName?></p>
        </li>
        <li class="confirm-item" id="payment-card">
            <p class="confirm-subtitle"><?=Yii::t('cashback','Bank Address')?></p>
            <p class="confirm-info"><?=$model->bankAddress?></p>
        </li>
        <li class="confirm-item" id="payment-card">
            <p class="confirm-subtitle"><?=Yii::t('cashback','Bank Address 2')?></p>
            <p class="confirm-info"><?=$model->bankAddress2?></p>
        </li>
        <li class="confirm-item" id="payment-card">
            <p class="confirm-subtitle"><?=Yii::t('cashback','Bank City,Province,Zip')?></p>
            <p class="confirm-info"><?=$model->bankCityProvinceZip?></p>
        </li>
        <li class="confirm-item" id="payment-amount">
            <p class="confirm-subtitle"><?=Yii::t('cashback','Amount')?></p>
            <p class="confirm-info"><?=Yii::$app->formatter->asCurrency($model->amount,Yii::$app->request->get('code'))?></p>
        </li>
        <li class="confirm-item">
            <p class="exchange-rate"><?=Yii::t('cashback','Commission: {commission}%',['{commission}' => $model->commission * 100])?></p>
            <p class="confirm-subtitle"><?=Yii::t('cashback','Commission')?></p>
            <p class="confirm-info"><?=Yii::$app->formatter->asCurrency(1*$model->commission_total,Yii::$app->request->get('code'))?></p>
        </li>
        <li class="confirm-item" id="payment-amount">
            <p class="confirm-subtitle"><?=Yii::t('cashback','Total')?></p>
            <p class="confirm-info"><?=Yii::$app->formatter->asCurrency($model->total,Yii::$app->request->get('code'))?></p>
        </li>
    </ul>
    <div class="confirm-buttons">
        <button class="confirm-close" onclick="slider.prevSlide()"><?=Yii::t('cashback','Close')?></button>
        <button class="confirm-submit" onclick="slider.submit('<?=\yii\helpers\Url::to(['/cashback/billing/payment-submit','code'=>Yii::$app->request->get('code'),'id'=>$model->payment_system_id])?>')"><?=Yii::t('cashback','Submit')?></button>
    </div>
</div>