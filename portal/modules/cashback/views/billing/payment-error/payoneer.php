<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 23.04.2019
 * Time: 17:11:21
 * @var $model \portal\models\forms\payments\PayoneerForm
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="result">
    <div class="result-wrap">
        <div class="result-img result-img--fail"></div>
        <p class="result-text result-text--fail"><?=Yii::t('cashback','The transaction was unsuccessful due to an unknown error. Please re-enter your payment information or try a different payment method.')?></p>
    </div>
</div>