<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 23.04.2019
 * Time: 17:11:21
 * @var $model \portal\models\forms\payments\PayPalForm
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>


<?php $form = ActiveForm::begin(["class"=>"pay-content", "id"=>"payment-data" ,'enableAjaxValidation' => true]); ?>

    <ul class="data">
        <?= $form->field($model, 'payment_system_id',['enableAjaxValidation' => true])->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'name',['enableAjaxValidation' => true,'options' => ['tag'=> 'li','class'=>'data-item']])->textInput(['class' => 'data-input'])->label(Yii::t('cashback','Name'),['class' => 'data-subtitle']) ?>
        <?= $form->field($model, 'surname',['enableAjaxValidation' => true,'options' => ['tag'=> 'li','class'=>'data-item']])->textInput(['class' => 'data-input'])->label(Yii::t('cashback','Surname'),['class' => 'data-subtitle']) ?>
        <?= $form->field($model, 'email',['enableAjaxValidation' => true,'options' => ['tag'=> 'li','class'=>'data-item']])->textInput(['class' => 'data-input'])->label(Yii::t('cashback','Email'),['class' => 'data-subtitle']) ?>
        <?= $form->field($model, 'phone',['enableAjaxValidation' => true,'options' => ['tag'=> 'li','class'=>'data-item']])->textInput(['class' => 'data-input'])->label(Yii::t('cashback','Phone'),['class' => 'data-subtitle']) ?>
        <?= $form->field($model, 'delivery',['enableAjaxValidation' => true,'options' => ['tag'=> 'li','class'=>'data-item']])->textInput(['class' => 'data-input'])->label(Yii::t('cashback','Billing Address'),['class' => 'data-subtitle']) ?>
		<?= $form->field($model, 'amount',['enableAjaxValidation' => true,'options' => ['tag'=> 'li','class'=>'data-item']])->textInput(['class' => 'data-input'])->label(Yii::t('cashback','Amount'),['class' => 'data-subtitle']) ?>

    </ul>
    <div class="data-buttons">
        <button class="data-close" type="button" onclick="slider.prevSlide()"><?=Yii::t('cashback','Close')?></button>
		<?= Html::submitButton(Yii::t('cashback','Submit'), ['class' => 'data-submit']) ?>
    </div>

<?php ActiveForm::end(); ?>
