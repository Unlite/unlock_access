<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 23.04.2019
 * Time: 17:11:21
 * @var $model \portal\models\forms\payments\PayoneerForm
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>


<?php $form = ActiveForm::begin(["class"=>"pay-content", "id"=>"payment-data" ,'enableAjaxValidation' => true]); ?>

    <ul class="data">
        <?= $form->field($model, 'payment_system_id',['enableAjaxValidation' => true])->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'email',['enableAjaxValidation' => true,'options' => ['tag'=> 'li','class'=>'data-item']])->textInput(['class' => 'data-input'])->label(Yii::t('cashback','Email'),['class' => 'data-subtitle']) ?>
        <?= $form->field($model, 'phone',['enableAjaxValidation' => true,'options' => ['tag'=> 'li','class'=>'data-item']])->textInput(['class' => 'data-input'])->label(Yii::t('cashback','Phone'),['class' => 'data-subtitle']) ?>
        <?= $form->field($model, 'firstName',['enableAjaxValidation' => true,'options' => ['tag'=> 'li','class'=>'data-item']])->textInput(['class' => 'data-input'])->label(Yii::t('cashback','First Name'),['class' => 'data-subtitle']) ?>
        <?= $form->field($model, 'middleName',['enableAjaxValidation' => true,'options' => ['tag'=> 'li','class'=>'data-item']])->textInput(['class' => 'data-input'])->label(Yii::t('cashback','Middle Name'),['class' => 'data-subtitle']) ?>
        <?= $form->field($model, 'lastName',['enableAjaxValidation' => true,'options' => ['tag'=> 'li','class'=>'data-item']])->textInput(['class' => 'data-input'])->label(Yii::t('cashback','Last Name'),['class' => 'data-subtitle']) ?>
        <?= $form->field($model, 'company',['enableAjaxValidation' => true,'options' => ['tag'=> 'li','class'=>'data-item']])->textInput(['class' => 'data-input'])->label(Yii::t('cashback','Company'),['class' => 'data-subtitle']) ?>
        <?= $form->field($model, 'address',['enableAjaxValidation' => true,'options' => ['tag'=> 'li','class'=>'data-item']])->textInput(['class' => 'data-input'])->label(Yii::t('cashback','Address'),['class' => 'data-subtitle']) ?>
        <?= $form->field($model, 'address2',['enableAjaxValidation' => true,'options' => ['tag'=> 'li','class'=>'data-item']])->textInput(['class' => 'data-input'])->label(Yii::t('cashback','Address 2'),['class' => 'data-subtitle']) ?>
        <?= $form->field($model, 'city',['enableAjaxValidation' => true,'options' => ['tag'=> 'li','class'=>'data-item']])->textInput(['class' => 'data-input'])->label(Yii::t('cashback','City'),['class' => 'data-subtitle']) ?>
        <?= $form->field($model, 'zip',['enableAjaxValidation' => true,'options' => ['tag'=> 'li','class'=>'data-item']])->textInput(['class' => 'data-input'])->label(Yii::t('cashback','ZIP'),['class' => 'data-subtitle']) ?>
        <?= $form->field($model, 'country',['enableAjaxValidation' => true,'options' => ['tag'=> 'li','class'=>'data-item']])->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Geo::find()->all(),'code','name'),['class' => 'exchange-select'])->label(Yii::t('cashback','Country'),['class' => 'data-subtitle'])?>
        <?= $form->field($model, 'accountNumber',['enableAjaxValidation' => true,'options' => ['tag'=> 'li','class'=>'data-item']])->textInput(['class' => 'data-input'])->label(Yii::t('cashback','Account Number'),['class' => 'data-subtitle']) ?>
        <?= $form->field($model, 'swift',['enableAjaxValidation' => true,'options' => ['tag'=> 'li','class'=>'data-item']])->textInput(['class' => 'data-input'])->label(Yii::t('cashback','SWIFT'),['class' => 'data-subtitle']) ?>
        <?= $form->field($model, 'bikCode',['enableAjaxValidation' => true,'options' => ['tag'=> 'li','class'=>'data-item']])->textInput(['class' => 'data-input'])->label(Yii::t('cashback','Bik code'),['class' => 'data-subtitle']) ?>
        <?= $form->field($model, 'correspondentAccount',['enableAjaxValidation' => true,'options' => ['tag'=> 'li','class'=>'data-item']])->textInput(['class' => 'data-input'])->label(Yii::t('cashback','Correspondent Account'),['class' => 'data-subtitle']) ?>
        <?= $form->field($model, 'inn',['enableAjaxValidation' => true,'options' => ['tag'=> 'li','class'=>'data-item']])->textInput(['class' => 'data-input'])->label(Yii::t('cashback','INN'),['class' => 'data-subtitle']) ?>
        <?= $form->field($model, 'kpp',['enableAjaxValidation' => true,'options' => ['tag'=> 'li','class'=>'data-item']])->textInput(['class' => 'data-input'])->label(Yii::t('cashback','KPP'),['class' => 'data-subtitle']) ?>
        <?= $form->field($model, 'bankName',['enableAjaxValidation' => true,'options' => ['tag'=> 'li','class'=>'data-item']])->textInput(['class' => 'data-input'])->label(Yii::t('cashback','Bank Name'),['class' => 'data-subtitle']) ?>
        <?= $form->field($model, 'bankAddress',['enableAjaxValidation' => true,'options' => ['tag'=> 'li','class'=>'data-item']])->textInput(['class' => 'data-input'])->label(Yii::t('cashback','Bank Address'),['class' => 'data-subtitle']) ?>
        <?= $form->field($model, 'bankAddress2',['enableAjaxValidation' => true,'options' => ['tag'=> 'li','class'=>'data-item']])->textInput(['class' => 'data-input'])->label(Yii::t('cashback','Bank Address 2'),['class' => 'data-subtitle']) ?>
        <?= $form->field($model, 'bankCityProvinceZip',['enableAjaxValidation' => true,'options' => ['tag'=> 'li','class'=>'data-item']])->textInput(['class' => 'data-input'])->label(Yii::t('cashback','Bank City,Province,Zip'),['class' => 'data-subtitle']) ?>
		<?= $form->field($model, 'amount',['enableAjaxValidation' => true,'options' => ['tag'=> 'li','class'=>'data-item']])->textInput(['class' => 'data-input'])->label(Yii::t('cashback','Amount'),['class' => 'data-subtitle']) ?>

    </ul>
    <div class="data-buttons">
        <button class="data-close" type="button" onclick="slider.prevSlide()"><?=Yii::t('cashback','Close')?></button>
		<?= Html::submitButton(Yii::t('cashback','Submit'), ['class' => 'data-submit']) ?>
    </div>

<?php ActiveForm::end(); ?>
