<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 23.04.2019
 * Time: 17:11:21
 * @var $model \portal\models\forms\payments\PayPalForm
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="result">
    <div class="result-wrap">
        <div class="result-img result-img--success"></div>
        <p class="result-text result-text--success"><?=Yii::t('cashback','Your transaction was successful! The funds will be transferred to your account in accordance with your preferred payment method. Thank you for using Croupierica!')?></p>
    </div>
</div>