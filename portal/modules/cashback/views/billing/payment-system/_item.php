<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 23.04.2019
 * Time: 15:17:11
 * @var \common\models\PaymentSystem $model
 */
use yii\helpers\Url;
?>
<li class="pay-item" data-name="<?= $model->name ?>" data-id="<?= $model->id?>" onclick="slider.loadForm(this,'<?=Url::to(['/cashback/billing/payment-form/','id' => $model->id,'code' => Yii::$app->request->get('code')])?>')" style="background-image: url('<?= $model->img_src ?>')">
    <p class="pay-subtitle"><?= $model->name ?></p>
    <p class="pay-text"><?= $model->description ?></p>
    <button class="pay-btn"></button>
</li>
