<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 15.04.2019
 * Time: 16:54:57
 * @var $model \common\models\Exchange
 */
?>
<tr>
	<td><?=Yii::$app->formatter->asDate($model->created_at,'dd.MM.YYYY')?> - <?=Yii::$app->formatter->asDate($model->updated_at,'dd.MM.YYYY')?></td>
	<td><?=Yii::$app->formatter->asCurrency($model->amount,$model->userCurrency->currency_code)?></td>
	<td><?=Yii::$app->formatter->asCurrency($model->transferTo->amount,$model->transferTo->userCurrency->currency_code)?></td>
	<td><?=Yii::$app->formatter->asCurrency($model->transferCommission->amount,$model->transferCommission->userCurrency->currency_code)?></td>
	<td><?=$model->status?></td>
</tr>
