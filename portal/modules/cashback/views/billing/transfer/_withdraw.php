<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 15.04.2019
 * Time: 16:54:57
 * @var $model \common\models\WithdrawTransfer
 */
?>
<tr>
	<td><?=Yii::$app->formatter->asDate($model->created_at,'dd.MM.YYYY')?> - <?=Yii::$app->formatter->asDate($model->updated_at,'dd.MM.YYYY')?></td>
	<td><?=$model->paymentSystem->name?></td>
	<td><?=Yii::$app->formatter->asCurrency($model->amount + ($model->transferCommission ? $model->transferCommission->amount : 0),$model->userCurrency->currency_code)?></td>
	<td><?=$model->status?></td>
</tr>
