<?php
/**
 * @var $model \common\models\UserCurrency
 */
?>
<div class="wallets-item <?=((Yii::$app->request->get('code') == $model->currency_code) || (Yii::$app->request->get('code') == null && $model->currency_code == Yii::$app->formatter->currencyCode))  ? 'wallets-item--active' : ''?>">
	<div class="wallets-currency"><?=$model->currency_code?></div>
	<a href="<?=\yii\helpers\Url::to(['/cashback/billing/index','code'=>$model->currency_code])?>" class="wallets-balance"><?=Yii::$app->formatter->asDecimal($model->amount,2)?></a>
</div>