<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 04.04.2019
 * Time: 13:51:31
 */
/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 */
use yii\widgets\ListView;
?>
<div class="wallets">
	<div class="container">
		<div class="wallets-wrapper">
			<h1 class="wallets-title"><?=Yii::t('cashback','Choose Wallet')?></h1>
				<?= ListView::widget([
					'dataProvider' => $dataProvider,
					'itemView' => '_item.php',
					'emptyText' => "",
					'options' => [
					        'class' => 'wallets-row',
                    ],
					'summary' => '',
					'layout' => '{items}'
				]);
				?>
		</div>
	</div>
</div>
