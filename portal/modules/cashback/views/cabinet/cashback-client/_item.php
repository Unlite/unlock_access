<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 08.05.2019
 * Time: 22:22:41
 * @var \common\components\cashback\models\CashbackCustomer $model
 *
 */
?>
<div class="cashback-table-row">
	<div class="cashback-table-summary">
		<p class="cashback-table-name"><?=$model->cashback->site->name	?></p>
		<p class="cashback-table-summary-percent"><?=$model->summaryPercent?><span>%</span></p>
		<p class="cashback-table-summary-payments"><?=$model->summaryAmount?><span><?=\portal\components\CurrencySymbolHelper::symbol($model->currency)?></span></p>
		<button class="cashback-table-showdetails js-dropdown-btn"></button>
	</div>
	<?=\yii\helpers\Html::a(Yii::t('cashback','Play'),['/cashback/casino/view','slug' => $model->cashback->site->slug],['class' => 'cashback-table-link'])?>
	<div class="cashback-table-details js-dropdown-cnt">
		<?= \yii\widgets\ListView::widget([
			'dataProvider' => new \yii\data\ActiveDataProvider([
				'query' => $model->getCashbackStats()
			]),
			'itemView' => '/cabinet/cashback-stat/_item',
			'emptyText' => "",
			'summary' => '',
			'layout' => '{items}'
		]);
		?>

	</div>
</div>
