<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 08.05.2019
 * Time: 22:22:46
 * @var \common\components\cashback\models\CashbackStat $model
 */

?>
<div class="cashback-table-details-row">
	<p class="cashback-table-details-date"><?=$model->date_start?> - <?=$model->date_end?></p>
	<p class="cashback-table-details-percent"><?=Yii::$app->formatter->asDecimal($model->user_percent,2)?>%</p>
	<p class="cashback-table-details-payments"><?=Yii::$app->formatter->asCurrency($model->user_revenue,$model->currency_code)?></p>
</div>
