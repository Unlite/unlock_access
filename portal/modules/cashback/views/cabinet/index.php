<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 02.04.2019
 * Time: 18:24:52
 */
/** @var \common\models\UserCurrency $userCurrency */
/** @var \common\components\cashback\models\CashbackStat[] $cashbackStat */
/** @var \yii\data\ActiveDataProvider $cashbackCustomersDataProvider */
$amount = 0;
foreach ($cashbackStat as $stat){
    if($stat->currency_code != 'usd'){
		$relation = \common\models\CurrencyRelations::findByCodes($stat->currency_code, 'usd')->one();
		$amount += $stat->user_revenue * $relation->value;
    } else
        $amount += $stat->user_revenue;
}
use yii\helpers\Html;
?>
<div class="balance">
    <div class="container">
        <div class="balance-wrapper">
            <div class="balance-item">
                <p class="balance-title"><?=Yii::t('cashback','Cashback')?></p>
                <div class="balance-middle">
                    <div class="balance-currency">$</div>
                    <div class="balance-value"><?=Yii::$app->formatter->asDecimal($amount,2)?></div>
                </div>
                <p class="balance-descr"><?=Yii::t('cashback','cashback issue')?></p>
            </div>
            <!--
            <div class="balance-item">
                <p class="balance-title">Referal bonuses</p>
                <div class="balance-middle">
                    <div class="balance-currency">$</div>
                    <div class="balance-value">10.8</div>
                </div>
                <p class="balance-descr">referal link</p>
            </div>
            -->
            <div class="balance-item">
                <p class="balance-title"><?=Yii::t('cashback','Balance')?></p>
                <div class="balance-middle">
                    <div class="balance-currency"><?=\portal\components\CurrencySymbolHelper::symbol($userCurrency->currency_code)?></div>
                    <div class="balance-value"><?=Yii::$app->formatter->asDecimal($userCurrency->amount,2)?></div>
                </div>
                <p class="balance-descr"><?=Yii::t('cashback','payment information')?></p>
            </div>
        </div>
    </div>
</div>

<div class="cashback-table">
    <div class="container">
        <div class="cashback-table-wrapper">
            <div class="cashback-table-row">
                <div class="cashback-table-title">
                    <p><?=Yii::t('cashback','Name casino')?></p>
                    <p><?=Yii::t('cashback','Cashback, %')?></p>
                    <p><?=Yii::t('cashback','Cashback amount')?></p>
                </div>
            </div>

			<?= \yii\widgets\ListView::widget([
				'dataProvider' => $cashbackCustomersDataProvider,
				'itemView' => 'cashback-client/_item',
				'emptyText' => "",
				'summary' => '',
				'layout' => '{items}'
			]);
			?>
        </div>
    </div>
</div>
<!--
<div class="referals">
    <div class="container">
        <div class="referals-wrapper">
            <h2 class="referals-title">Referal link</h2>
            <div class="referals-table">
                <div class="referals-table-categories">
                    <p>Period of time</p>
                    <p>Numbers of refs</p>
                    <p>Income money</p>
                    <button class="referals-table-showdetails js-dropdown-btn"></button>
                </div>
                <div class="referals-table-details js-dropdown-cnt">
                    <div class="referals-table-row">
                        <p class="referals-table-date">01.02.2017 - 02.03.2017</p>
                        <p class="referals-table-number">4</p>
                        <p class="referals-table-payments">54$</p>
                    </div>
                    <div class="referals-table-row">
                        <p class="referals-table-date">01.02.2017 - 02.03.2017</p>
                        <p class="referals-table-number">4</p>
                        <p class="referals-table-payments">54$</p>
                    </div>
                    <div class="referals-table-row">
                        <p class="referals-table-date">01.02.2017 - 02.03.2017</p>
                        <p class="referals-table-number">4</p>
                        <p class="referals-table-payments">54$</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
-->

<div class="slides">
    <div class="container">

        <div class="slider slick-slider">
			<?= \yii\widgets\ListView::widget([
				'dataProvider' => new \yii\data\ArrayDataProvider([
					'allModels' => \common\models\Site::find()->innerJoinWith([
					        'cashback' => function(\yii\db\ActiveQuery $query) {
                                    $query->andWhere(['>', 'cashback.site_id', 0])->orderBy(['id'=>SORT_ASC]);
                            }])->limit(5)->all(),
				]),
				'itemView' => 'site/_slider_item',
				'emptyText' => "",
				'summary' => '',
				'layout' => '{items}',
				'itemOptions' => ['class'=>'slider-item'],
			]);
			?>
        </div>
    </div>
</div>
<?php $this->registerJs("$('.slider').slick({infinite:true,slidesToShow:3,slidesToScroll:1,responsive: [{
            breakpoint: 1120,
            settings: {
                slidesToShow: 1,
                adaptiveHeight: true,
            }
        }]
        });", \yii\web\View::POS_END) ?>
<?php $this->registerJs("
$('.js-dropdown-btn').on('click',
function(){
$(this).toggleClass('js-dropdown-btn--active').parent().siblings().closest('.js-dropdown-cnt').slideToggle(500);  
});", \yii\web\View::POS_END) ?>
