<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 08.05.2019
 * Time: 18:55:44
 * @var $model \common\models\Site
 */
use yii\helpers\Html;
?>
<p class="slider-title"><?=$model->name?></p>
<div class="slider-middle">
	<div class="slider-image">
		<?= Html::img('@web/img/mybet.png', ['alt' => '']) ?>
	</div>
	<div class="slider-payments">
		<p class="slider-payments--how">25$</p>
		<p class="slider-payments--what">giveaway for dep over 100$</p>
	</div>
</div>
<?=Html::a('More',['/cashback/casino/view','slug'=>$model->slug],['class' => 'slider-link'])?>
