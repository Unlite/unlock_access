<?php
use yii\helpers\Url;
?>
<div class="top-info">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <span class="top-info-text"><?=Yii::t('cashback','Alert information')?></span>
            </div>
        </div>
    </div>
</div>
<div class="narrow-content">
    <div class="user-balance">
        <div class="row justify-content-md-between">
            <div class="balance-item decoration col-md-4 col-sm-12">
                <h4><?=Yii::t('cashback','Referral cashback')?></h4>
                <div class="balance justify-content-between">
                    <div class="balance-num">
                        <div class="balance-num-inner">
                            $
                            <span class="balance-num-line"></span>
                        </div>
                    </div>
                    <div class="balance-num">
                        <div class="balance-num-inner">
                            2
                            <span class="balance-num-line"></span>
                        </div>
                    </div>
                    <div class="balance-num">
                        <div class="balance-num-inner">
                            5
                            <span class="balance-num-line"></span>
                        </div>
                    </div>
                    <div class="balance-num">
                        <div class="balance-num-inner">
                            .
                            <span class="balance-num-line"></span>
                        </div>
                    </div>
                    <div class="balance-num">
                        <div class="balance-num-inner">
                            4
                            <span class="balance-num-line"></span>
                        </div>
                    </div>
                </div>
                <p><a href="#"><?=Yii::t('cashback','cashback issue')?></a></p>
            </div>
            <div class="balance-item decoration col-md-4 col-sm-12">
                <h4><?=Yii::t('cashback','Referral bonuses')?></h4>
                <div class="balance justify-content-between">
                    <div class="balance-num">
                        <div class="balance-num-inner">
                            $
                            <span class="balance-num-line"></span>
                        </div>
                    </div>
                    <div class="balance-num">
                        <div class="balance-num-inner">
                            1
                            <span class="balance-num-line"></span>
                        </div>
                    </div>
                    <div class="balance-num">
                        <div class="balance-num-inner">
                            0
                            <span class="balance-num-line"></span>
                        </div>
                    </div>
                    <div class="balance-num">
                        <div class="balance-num-inner">
                            .
                            <span class="balance-num-line"></span>
                        </div>
                    </div>
                    <div class="balance-num">
                        <div class="balance-num-inner">
                            8
                            <span class="balance-num-line"></span>
                        </div>
                    </div>
                </div>
                <p><a href="#"><?=Yii::t('cashback','referral link')?></a></p>
            </div>
            <div class="balance-item decoration col-md-4 col-sm-12">
                <h4>balance</h4>
                <div class="balance justify-content-between">
                    <div class="balance-num">
                        <div class="balance-num-inner">
                            $
                            <span class="balance-num-line"></span>
                        </div>
                    </div>
                    <div class="balance-num">
                        <div class="balance-num-inner">
                            6
                            <span class="balance-num-line"></span>
                        </div>
                    </div>
                    <div class="balance-num">
                        <div class="balance-num-inner">
                            9
                            <span class="balance-num-line"></span>
                        </div>
                    </div>
                    <div class="balance-num">
                        <div class="balance-num-inner">
                            .
                            <span class="balance-num-line"></span>
                        </div>
                    </div>
                    <div class="balance-num">
                        <div class="balance-num-inner">
                            7
                            <span class="balance-num-line"></span>
                        </div>
                    </div>
                </div>
                <p><a href="#"><?=Yii::t('cashback','payment information')?></a></p>
            </div>
        </div>
    </div>
</div>

<div class="narrow-content">
    <div class="the-best-wrap decoration">
        <h2 class="title-icon"><span><?=Yii::t('cashback','The best cashback in this month: (or instant deals)')?></span></h2>
        <div class="the-best row justify-content-md-between">
            <div class="the-best-item the-best-pink col-md-4">
                <div class="the-best-content">
                    <div class="the-best-img">
                        <img src="<?=Url::to(['img/casino/the-best-img.png'])?>" alt="">
                    </div>
                    <div class="the-best-text">
                        <div class="the-best-title">Malina casino</div>
                        <div class="the-best-result">15%</div>
                        <div class="the-best-small">cashback in month</div>
                    </div>
                </div>
            </div>
            <div class="the-best-item col-md-4">
                <div class="the-best-content">
                    <div class="the-best-img">
                        <img src="<?=Url::to(['img/casino/the-best-img.png'])?>" alt="">
                    </div>
                    <div class="the-best-text">
                        <div class="the-best-title">Malina casino</div>
                        <div class="the-best-result">25$</div>
                        <div class="the-best-small">giveaway for dep over 100$</div>
                    </div>
                </div>
            </div>
            <div class="the-best-item the-best-blue col-md-4">
                <div class="the-best-content">
                    <div class="the-best-img">
                        <img src="<?=Url::to(['img/casino/the-best-img.png'])?>" alt="">
                    </div>
                    <div class="the-best-text">
                        <div class="the-best-title">Malina casino</div>
                        <div class="the-best-result">20%</div>
                        <div class="the-best-small">cashback in month</div>
                    </div>
                </div>
            </div>
        </div>
        <a href="#" class="button button-inline"><?=Yii::t('cashback','Show all')?></a>
    </div>
</div>

<div class="narrow-content">
    <div class="table-slider-wrap">
        <div class="table-slider table-slider-play">
            <div class="table-slider-header row">
                <div class="table-slider-col col-4"><div class="table-slider-text"><?=Yii::t('cashback','name casino')?></div></div>
                <div class="table-slider-col col-4"><div class="table-slider-text"><?=Yii::t('cashback','cashback, %')?></div></div>
                <div class="table-slider-col col-4"><div class="table-slider-text"><?=Yii::t('cashback','Cashback amount')?></div></div>
            </div>
            <div class="table-slider-item">
                <div class="table-slider-title">
                    <div>
                        <a href="#" class="button toggle-button"></a>
                        <div class="toggle-line row">
                            <div class="table-slider-col col-4"><div class="table-slider-text">Frank casino</div></div>
                            <div class="table-slider-col col-4"><div class="table-slider-text">74%</div></div>
                            <div class="table-slider-col col-4"><div class="table-slider-text">$665</div></div>
                        </div>
                    </div>
                    <a href="#" class="button link-button"><div class="table-slider-text"><?=Yii::t('cashback','play')?></div></a>
                </div>
                <div class="table-slider-content">
                    <div class="table-slider-line row">
                        <div class="table-slider-col col-4"><div class="table-slider-text">01.02.2017 – 02.03.2017</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">15</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">$55</div></div>
                    </div>
                    <div class="table-slider-line row">
                        <div class="table-slider-col col-4"><div class="table-slider-text">01.02.2017 – 02.03.2017</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">15%</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">$55</div></div>
                    </div>
                </div>
            </div>
            <div class="table-slider-item">
                <div class="table-slider-title">
                    <div>
                        <a href="#" class="button toggle-button"></a>
                        <div class="toggle-line row">
                            <div class="table-slider-col col-4"><div class="table-slider-text">Frank casino</div></div>
                            <div class="table-slider-col col-4"><div class="table-slider-text">74%</div></div>
                            <div class="table-slider-col col-4"><div class="table-slider-text">$665</div></div>
                        </div>
                    </div>
                    <a href="#" class="button link-button"><div class="table-slider-text"><?=Yii::t('cashback','play')?></div></a>
                </div>
                <div class="table-slider-content">
                    <div class="table-slider-line row">
                        <div class="table-slider-col col-4"><div class="table-slider-text">01.02.2017 – 02.03.2017</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">15</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">$55</div></div>
                    </div>
                    <div class="table-slider-line row">
                        <div class="table-slider-col col-4"><div class="table-slider-text">01.02.2017 – 02.03.2017</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">15%</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">$55</div></div>
                    </div>
                </div>
            </div>
            <div class="table-slider-item">
                <div class="table-slider-title">
                    <div>
                        <a href="#" class="button toggle-button"></a>
                        <div class="toggle-line row">
                            <div class="table-slider-col col-4"><div class="table-slider-text">Frank casino</div></div>
                            <div class="table-slider-col col-4"><div class="table-slider-text">74%</div></div>
                            <div class="table-slider-col col-4"><div class="table-slider-text">$665</div></div>
                        </div>
                    </div>
                    <a href="#" class="button link-button"><div class="table-slider-text"><?=Yii::t('cashback','play')?></div></a>
                </div>
                <div class="table-slider-content">
                    <div class="table-slider-line row">
                        <div class="table-slider-col col-4"><div class="table-slider-text">01.02.2017 – 02.03.2017</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">15</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">$55</div></div>
                    </div>
                    <div class="table-slider-line row">
                        <div class="table-slider-col col-4"><div class="table-slider-text">01.02.2017 – 02.03.2017</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">15%</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">$55</div></div>
                    </div>
                </div>
            </div>
            <div class="table-slider-item">
                <div class="table-slider-title">
                    <div>
                        <a href="#" class="button toggle-button"></a>
                        <div class="toggle-line row">
                            <div class="table-slider-col col-4"><div class="table-slider-text">Frank casino</div></div>
                            <div class="table-slider-col col-4"><div class="table-slider-text">74%</div></div>
                            <div class="table-slider-col col-4"><div class="table-slider-text">$665</div></div>
                        </div>
                    </div>
                    <a href="#" class="button link-button"><div class="table-slider-text"><?=Yii::t('cashback','play')?></div></a>
                </div>
                <div class="table-slider-content">
                    <div class="table-slider-line row">
                        <div class="table-slider-col col-4"><div class="table-slider-text">01.02.2017 – 02.03.2017</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">15</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">$55</div></div>
                    </div>
                    <div class="table-slider-line row">
                        <div class="table-slider-col col-4"><div class="table-slider-text">01.02.2017 – 02.03.2017</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">15%</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">$55</div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="table-slider-wrap">
        <div class="table-slider">
            <div class="table-slider-header row">
                <div class="table-slider-col col-12"><div class="table-slider-text"><?=Yii::t('cashback','referral link')?></div></div>
            </div>
            <div class="table-slider-item">
                <div class="table-slider-title">
                    <div>
                        <a href="#" class="button toggle-button"></a>
                        <div class="toggle-line row">
                            <div class="table-slider-col col-4"><div class="table-slider-text"><?=Yii::t('cashback','period time')?></div></div>
                            <div class="table-slider-col col-4"><div class="table-slider-text"><?=Yii::t('cashback','number of refs')?></div></div>
                            <div class="table-slider-col col-4"><div class="table-slider-text"><?=Yii::t('cashback','income money')?></div></div>
                        </div>
                    </div>
                </div>
                <div class="table-slider-content">
                    <div class="table-slider-line row">
                        <div class="table-slider-col col-4"><div class="table-slider-text">01.02.2017 – 02.03.2017</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">15</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">$55</div></div>
                    </div>
                    <div class="table-slider-line row">
                        <div class="table-slider-col col-4"><div class="table-slider-text">01.02.2017 – 02.03.2017</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">15%</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">$55</div></div>
                    </div>
                </div>
            </div>
            <div class="table-slider-item">
                <div class="table-slider-title">
                    <div>
                        <a href="#" class="button toggle-button"></a>
                        <div class="toggle-line row">
                            <div class="table-slider-col col-4"><div class="table-slider-text"><?=Yii::t('cashback','period time')?></div></div>
                            <div class="table-slider-col col-4"><div class="table-slider-text"><?=Yii::t('cashback','number of refs')?></div></div>
                            <div class="table-slider-col col-4"><div class="table-slider-text"><?=Yii::t('cashback','income money')?></div></div>
                        </div>
                    </div>
                </div>
                <div class="table-slider-content">
                    <div class="table-slider-line row">
                        <div class="table-slider-col col-4"><div class="table-slider-text">01.02.2017 – 02.03.2017</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">15</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">$55</div></div>
                    </div>
                    <div class="table-slider-line row">
                        <div class="table-slider-col col-4"><div class="table-slider-text">01.02.2017 – 02.03.2017</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">15%</div></div>
                        <div class="table-slider-col col-4"><div class="table-slider-text">$55</div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>