<?php
use yii\helpers\Url;
?>
<div class="container how-it-works">
    <h1 class="promo-title colorful-text"><?=Yii::t('cashback','Croupierica cashback')?></h1>
    <div class="promo-title-small"><?=Yii::t('cashback','How it works!')?></div>
    <div class="steps-list">
        <div class="steps-item step-1">
            <div class="step">
                <span>1</span>
            </div>
            <div class="step-text">
                <?=Yii::t('cashback','Create an account')?>
            </div>
        </div>
        <div class="steps-item step-2">
            <div class="step">
                <span>2</span>
            </div>
            <div class="step-text">
                <?=Yii::t('cashback','Choose a casino from our list of offers and play')?>
            </div>
        </div>
        <div class="steps-item step-3">
            <div class="step">
                <span>3</span>
            </div>
            <div class="step-text">
                <?=Yii::t('cashback','Get cashback to your account')?>
            </div>
        </div>
    </div>
    <a href="#" class="button button-reg"><?=Yii::t('cashback','registration')?></a>
</div>
<div class="our-deals">
    <div class="our-deals-gray"></div>
    <div class="our-deals-overlay"></div>
    <div class="container">
        <h2 class="promo-title white-text"><?=Yii::t('cashback','Our deals')?></h2>
        <div class="promo-title-small white-text"><?=Yii::t('cashback','Hottest deals in gambling industry!')?></div>
        <div class="our-deals-list">
            <div class="deals-item-wrap">
                <div class="our-deals-item decoration">
                    <div class="our-deals-logo">
                        <img src="<?=Url::to(['/img/promo/casino/10bet.png']);?>" alt="">
                    </div>
                    <div class="our-deals-bonus">
                        <div class="casino-cashback-wrap">
                            <div class="casino-cashback">
                                <div class="cashback-col">
                                    <div class="cashback-result">15%</div>
                                    <div class="cashback-text">cashback in month</div>
                                </div>
                                <div class="cashback-col cashback-plus">+</div>
                                <div class="cashback-col">
                                    <div class="cashback-result">50</div>
                                    <div class="cashback-text">free spins</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="deals-item-wrap">
                <div class="our-deals-item decoration">
                    <div class="our-deals-logo">
                        <img src="<?=Url::to(['/img/promo/casino/winner.png']);?>" alt="">
                    </div>
                    <div class="our-deals-bonus">
                        <div class="casino-cashback-wrap">
                            <div class="casino-cashback">
                                <div class="cashback-col">
                                    <div class="cashback-result">15%</div>
                                    <div class="cashback-text">cashback in month</div>
                                </div>
                                <div class="cashback-col cashback-plus">+</div>
                                <div class="cashback-col">
                                    <div class="cashback-result">100</div>
                                    <div class="cashback-text">free spins</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="deals-item-wrap">
                <div class="our-deals-item decoration">
                    <div class="our-deals-logo">
                        <img src="<?=Url::to(['/img/promo/casino/roxy-palace.png']);?>" alt="">
                    </div>
                    <div class="our-deals-bonus">
                        <div class="casino-cashback-wrap">
                            <div class="casino-cashback">
                                <div class="cashback-col">
                                    <div class="cashback-result">15%</div>
                                    <div class="cashback-text">cashback in month</div>
                                </div>
                                <div class="cashback-col cashback-plus">+</div>
                                <div class="cashback-col">
                                    <div class="cashback-result">100</div>
                                    <div class="cashback-text">free spins</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="deals-item-wrap">
                <div class="our-deals-item decoration">
                    <div class="our-deals-logo">
                        <img src="<?=Url::to(['/img/promo/casino/10bet.png']);?>" alt="">
                    </div>
                    <div class="our-deals-bonus">
                        <div class="casino-cashback-wrap">
                            <div class="casino-cashback">
                                <div class="cashback-col">
                                    <div class="cashback-result">15%</div>
                                    <div class="cashback-text">cashback in month</div>
                                </div>
                                <div class="cashback-col cashback-plus">+</div>
                                <div class="cashback-col">
                                    <div class="cashback-result">100</div>
                                    <div class="cashback-text">free spins</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="deals-item-wrap">
                <div class="our-deals-item decoration">
                    <div class="our-deals-logo">
                        <img src="<?=Url::to(['/img/promo/casino/winner.png']);?>" alt="">
                    </div>
                    <div class="our-deals-bonus">
                        <div class="casino-cashback-wrap">
                            <div class="casino-cashback">
                                <div class="cashback-col">
                                    <div class="cashback-result">15%</div>
                                    <div class="cashback-text">cashback in month</div>
                                </div>
                                <div class="cashback-col cashback-plus">+</div>
                                <div class="cashback-col">
                                    <div class="cashback-result">100</div>
                                    <div class="cashback-text">free spins</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="deals-item-wrap">
                <div class="our-deals-item decoration">
                    <div class="our-deals-logo">
                        <img src="<?=Url::to(['/img/promo/casino/roxy-palace.png']);?>" alt="">
                    </div>
                    <div class="our-deals-bonus">
                        <div class="casino-cashback-wrap">
                            <div class="casino-cashback">
                                <div class="cashback-col">
                                    <div class="cashback-result">15%</div>
                                    <div class="cashback-text">cashback in month</div>
                                </div>
                                <div class="cashback-col cashback-plus">+</div>
                                <div class="cashback-col">
                                    <div class="cashback-result">100</div>
                                    <div class="cashback-text">free spins</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a href="#" class="button button-reg"><?=Yii::t('cashback','registration')?></a>
    </div>
</div>
<div class="container our-partners">
    <h2 class="promo-title colorful-text"><?=Yii::t('cashback','Our partners')?></h2>
    <div class="promo-title-small"><?=Yii::t('cashback','Choose your casino!')?></div>
    <div class="partners-list">
        <div class="partner">
            <a href="">
                <img src="<?=Url::to(['/img/promo/casino/10bet.png']);?>" alt="">
            </a>
        </div>
        <div class="partner">
            <img src="<?=Url::to(['/img/promo/casino/10bet.png']);?>" alt="">
        </div>
        <div class="partner">
            <img src="<?=Url::to(['/img/promo/casino/10bet.png']);?>" alt="">
        </div>
        <div class="partner">
            <img src="<?=Url::to(['/img/promo/casino/10bet.png']);?>" alt="">
        </div>
        <div class="partner">
            <img src="<?=Url::to(['/img/promo/casino/10bet.png']);?>" alt="">
        </div>
        <div class="partner">
            <img src="<?=Url::to(['/img/promo/casino/10bet.png']);?>" alt="">
        </div>
        <div class="partner">
            <img src="<?=Url::to(['/img/promo/casino/10bet.png']);?>" alt="">
        </div>
        <div class="partner">
            <img src="<?=Url::to(['/img/promo/casino/10bet.png']);?>" alt="">
        </div>
        <div class="partner">
            <img src="<?=Url::to(['/img/promo/casino/10bet.png']);?>" alt="">
        </div>
        <div class="partner">
            <img src="<?=Url::to(['/img/promo/casino/10bet.png']);?>" alt="">
        </div>
        <div class="partner">
            <img src="<?=Url::to(['/img/promo/casino/10bet.png']);?>" alt="">
        </div>
        <div class="partner">
            <img src="<?=Url::to(['/img/promo/casino/10bet.png']);?>" alt="">
        </div>
        <div class="partner">
            <img src="<?=Url::to(['/img/promo/casino/10bet.png']);?>" alt="">
        </div>
        <div class="partner">
            <img src="<?=Url::to(['/img/promo/casino/10bet.png']);?>" alt="">
        </div>
        <div class="partner">
            <img src="<?=Url::to(['/img/promo/casino/10bet.png']);?>" alt="">
        </div>
        <div class="partner">
            <img src="<?=Url::to(['/img/promo/casino/10bet.png']);?>" alt="">
        </div>
        <div class="partner">
            <img src="<?=Url::to(['/img/promo/casino/10bet.png']);?>" alt="">
        </div>
        <div class="partner">
            <img src="<?=Url::to(['/img/promo/casino/10bet.png']);?>" alt="">
        </div>
        <div class="partner">
            <img src="<?=Url::to(['/img/promo/casino/10bet.png']);?>" alt="">
        </div>
        <div class="partner">
            <img src="<?=Url::to(['/img/promo/casino/10bet.png']);?>" alt="">
        </div>
        <div class="partner">
            <img src="<?=Url::to(['/img/promo/casino/10bet.png']);?>" alt="">
        </div>
        <div class="partner">
            <img src="<?=Url::to(['/img/promo/casino/10bet.png']);?>" alt="">
        </div>
        <div class="partner">
            <img src="<?=Url::to(['/img/promo/casino/10bet.png']);?>" alt="">
        </div>
        <div class="partner">
            <img src="<?=Url::to(['/img/promo/casino/10bet.png']);?>" alt="">
        </div>
        <div class="partner">
            <img src="<?=Url::to(['/img/promo/casino/10bet.png']);?>" alt="">
        </div>
    </div>
    <a href="#" class="button button-reg"><?=Yii::t('cashback','registration')?></a>
</div>