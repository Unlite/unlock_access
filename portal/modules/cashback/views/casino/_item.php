<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 04.04.2019
 * Time: 14:19:02
 */

use common\modules\away\widgets\ModelLinkWidget;
use yii\bootstrap\Html;
use yii\helpers\Url;

/* @var $model common\models\Site */
/* @var $this \yii\web\View */

$rect = $model->getSiteImages()->type('rect')->one() ? $model->getSiteImages()->type('rect')->one()->src : '/img/spin-palace.png';
$mainDomain = $model->getDomains()->typeMain()->one();

$playUrl = $mainDomain ? ModelLinkWidget::widget(['model' => $mainDomain]) : ['casino/view', 'slug' => $model->slug];
?>


<div class="casino-item">
		<div class="logo">
			<img src="<?=$rect?>" alt="" class="casino-img">
			<div class="logo-rate"><?= number_format($model->rating, 1, '.', '') ?></div>
		</div>
		<div class="casino-info">
			<h2 class="casino-title"><?= Html::a($model->name, ['/cashback/casino/view', 'slug' => $model->slug]) ?></h2>
			<div class="casino-subtitle"><?=Yii::t('cashback','Cashback')?></div>
			<p class="casino-text"><?=$model->info['description']?></p>
		</div>
		<div class="casino-sale">
            <div class="casino-offer">
                <div class="casino-offer-text">10%</div>
                <div class="casino-offer-name">Cashback</div>
            </div>
            <div class="casino-offer-text">+</div>
            <div class="casino-offer">
                <div class="casino-offer-text">10%</div>
                <div class="casino-offer-name">Cashback</div>
            </div>
		</div>
		<div class="casino-links">
			<a href="#" class="casino-link light-link js-casino-popup-show"><?=Yii::t('cashback','More')?></a>
			<?= Html::a(Yii::t('cashback','play'), $playUrl, ['class' => 'casino-link']) ?>
		</div>
</div>