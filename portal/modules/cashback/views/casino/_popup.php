<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 04.04.2019
 * Time: 14:37:20
 */
use yii\helpers\Html;
?>
<div class="casino-popup js-popup">
    <div class="casino-popup-overlay js-popup-close"></div>
    <div class="casino-popup-content">
        <button class="casino-popup-close js-popup-close">&times;</button>
        <div class="casino-popup-top">
            <div class="casino-popup-image">
                <?= Html::img('@web/img/spin-palace.png', ['alt' => '']) ?>
            </div>

            <div class="casino-popup-title">
                <h1 class="casino-popup-name">Mybet Casino</h1>
                <h2 class="casino-popup-category">Cashback</h2>
            </div>

        </div>
        <div class="casino-popup-descr">
            <p><?=Yii::t('cashback','You probably already know how it works. You pay, you get a small percentage of spent money back. What about online casinos? Those guys are usually doing things differently. Some casinos give you cashback on losses only, while some do not mind covering even your winning bets. Some websites have dedicated promotions, while some promise you rebate on a weekly basis. It\'s all about what you need and what you choose in the end, but we decided to streamline and simplify that part. Welcome to Croupierica Cashback!')?></p>
            <p><?=Yii::t('cashback','All you need to enjoy all the benefits is to create an account with us, choose the partner casino you deem appropriate and sign up with it using our links. Voila, now you are a part of our cashback system, and whatever you play, you can be certain that the promised percentage will be returned to your wallet. Sounds good? I bet it does!')?> </p>
        </div>


        <div class="casino-popup-slider">
            <div class="js-casino-popup-slider">
                <div class="casino-sale">
                    <div class="casino-offer">
                        <div class="casino-offer-text">10%</div>
                        <div class="casino-offer-name">Cashback</div>
                    </div>
                    <div class="casino-offer-text">+</div>
                    <div class="casino-offer">
                        <div class="casino-offer-text">10%</div>
                        <div class="casino-offer-name">Cashback</div>
                    </div>
                </div>
                <div class="casino-sale">
                    <div class="casino-offer">
                        <div class="casino-offer-text">10%</div>
                        <div class="casino-offer-name">Cashback</div>
                    </div>
                    <div class="casino-offer-text">+</div>
                    <div class="casino-offer">
                        <div class="casino-offer-text">10%</div>
                        <div class="casino-offer-name">Cashback</div>
                    </div>
                </div>
                <div class="casino-sale">
                    <div class="casino-offer">
                        <div class="casino-offer-text">10%</div>
                        <div class="casino-offer-name">Cashback</div>
                    </div>
                    <div class="casino-offer-text">+</div>
                    <div class="casino-offer">
                        <div class="casino-offer-text">10%</div>
                        <div class="casino-offer-name">Cashback</div>
                    </div>
                </div>
            </div>
        </div>
        <a href="" class="casino-popup-link"><?=Yii::t('cashback','Play')?></a>
    </div>
</div>
