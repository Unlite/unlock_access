<?php
use yii\widgets\ListView;

/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \yii\web\View $this
 */
echo $this->render('_popup')
?>
<div class="cashback">
	<div class="container">
		<div class="cashback-wrapper">
			<h1 class="cashback-title"><?=Yii::t('cashback','Cashback')?></h1>
			<p class="cashback-text"><?=Yii::t('cashback','You probably already know how it works. You pay, you get a small percentage of spent money back. What about online casinos? Those guys are usually doing things differently. Some casinos give you cashback on losses only, while some do not mind covering even your winning bets. Some websites have dedicated promotions, while some promise you rebate on a weekly basis. It\'s all about what you need and what you choose in the end, but we decided to streamline and simplify that part. Welcome to Croupierica Cashback!')?></p>
		</div>
	</div>
</div>
<div class="casino">
	<div class="container">
		<div class="casino-wrapper">
			<ul class="casino-toggle">
				<li class="casino-toggler casino-toggler--active casino-toggler--icon1">
					Category
				</li>
				<li class="casino-toggler casino-toggler--icon2">
					Category
				</li>
				<li class="casino-toggler casino-toggler--icon3">
					Category
				</li>
				<li class="casino-toggler casino-toggler--icon4">
					Category
				</li>
			</ul>
			<?= ListView::widget([
				'dataProvider' => $dataProvider,
				'itemView' => '_item',
				'emptyText' => "",
				'summary' => '',
				'layout' => '{items}'
			]);
			?>
		</div>
	</div>
</div>