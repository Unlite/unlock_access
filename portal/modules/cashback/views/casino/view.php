<div class="info">
	<div class="container">
		<div class="info-wrapper">
			<div class="info-top">
				<div class="logo">
					<img src="/img/spin-palace.png" alt="" class="info-img">
					<div class="logo-rate">4.7</div>
				</div>
				<h2 class="info-title">Spin Palace Casino</h2>
				<button class="info-btn" id="info-btn"></button>
			</div>
			<p class="info-intro" id="info-intro">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one<br /> Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. </p>
			<div class="info-text" id="info-text">
				<p class="info-text-paragraph">If you enjoy the atmosphere of refreshing light-heartedness that only enhances the overall gambling experience, this casino is a great pick.</p>

				<p class="info-text-paragraph">One of the younger and more relaxed players on the modern online casino market, Cashmio quickly charmed its way into the charts and hearts of the online gambling community. The quirky and fun approach combined with great banking and a large game library can do wonders as you will see from our Cashmio Casino review!</p>

				<h3 class="info-text-title">First Impression</h3>

				<p class="info-text-paragraph">Cashmio Casino is remarkably light both on general layout and interface. The colors are unique for an online casino, as are the mascots of the website - the titular cashmios. You'll be finding those all over the website, in the promotions, casino section and About Us page. They look a bit like minions from that animated movie and bring a lot of extra light-heartedness to the overall atmosphere. The interface is carefully thought through and user-friendly as a result. It takes no more than a minute to create an account, and each step you are going to take will be perfectly clear and simple.</p>

				<h3 class="info-text-title">Mobile Version</h3>

				<p class="info-text-paragraph">Cashmio Casino is completely mobile-friendly and can be used without any issues on all modern pocket devices on iOS and Android. Everything you need is a simple browser, no extra installations are necessary. The experience is almost identical, though not all of the games have mobile-ready versions.</p>

				<h3 class="info-text-title">Software</h3>

				<p class="info-text-paragraph">Cashmio Casino games are numerous and of marvelous quality, thanks to its extensive list of software providers. It has both super popular hit makers and more obscure studios. NetEnt, Microgaming, Barcrest Games, Evolution Gaming, Play'n GO, Yggdrasil Gaming and many many others have helped to craft an outstanding library of titles. Most of the games are available for free in fun mode to check them out before depositing, and you can also check the information on RTP, provider, lines and bets amount. The fun never stops at Cashmio Casino thanks to hundreds of slots, including some amazing progressive jackpots like Mega Moolah and Arabian Nights. Table games fans will also find something to enjoy with virtual roulettes and blackjacks, as well as live casino versions of those games. If you are into poker (video or classic), it's better to find another website though, that's a bummer.</p>

				<h3 class="info-text-title">Promotions</h3>

				<p class="info-text-paragraph">Cashmio Casino bonuses are combined into quite an interesting system that provides members with enjoyable activities that bring you rewards for your gameplay (whether you're winning or losing). But let's start with more traditional systems. First of all, new members are eligible for a no deposit bonus when they sign up. After claiming it you will receive 20 free spins on Aloha: Cluster Pays slot. You can win up to €100 using this bonus after clearing 45x wagering requirement. After that, you are free to claim a match-up welcome offer up to €100, as well as 100 free spins. The wagering requirement is 25x for both initial deposit and bonus amount, which is a bit bigger than how we like it, but still reasonable. When all is said and done, welcome to the daily activities that will reward you for clearing challenges on different games.</p>

				<h3 class="info-text-title">Banking</h3>

				<p class="info-text-paragraph">Cashmio Casino offers the following banking methods for every member: direct bank transfer (via Trustly), credit/debit cards (VISA, MasterCard), and e-wallets (Skrill, Neteller, EcoPayz). You can also deposit via pre-paid voucher paysafecard. Cashmio Casino does not take any fees for neither deposits nor cashouts. Payouts are generally really quick (especially if you are using e-wallets) and all transactions of €50,000 or more are divided into installments and paid over 10 months.</p>

				<h3 class="info-text-title">Customer Support</h3>

				<p class="info-text-paragraph">Cashmio Casino support team is available via live chat or e-mail. Usually, it doesn't take long for the agents to assist you and all of them are generally professional and helpful. Sadly, you can receive live assistance from 09:00 to 01:00 (CET) only, making any of your late night gambling issues and questions wait until the morning.</p>

				<h3 class="info-text-title">License and Fair Play</h3>

				<p class="info-text-paragraph">Cashmio Casino is licensed through Malta Gaming Authority, UK Gambling Commission, and Swedish Gambling Authority. All of those are well-established and reputable governing bodies that offer their utmost assistance to the player in the case of the casino breaching their terms.</p>

				<p class="info-text-paragraph">All the games are regularly audited and proven fair by independent third-party companies.</p>

				<h3 class="info-text-title">Conclusion</h3>

				<p class="info-text-paragraph">Cashmio Casino is one of the best gambling websites out there that is definitely worth your time and money.</p>
			</div>
		</div>
	</div>
</div>
<div class="offer">
	<div class="container">
		<div class="offer-wrapper">
			<p class="offer-phrase">CASHBACK <span class="colorish">20%</span> and <span class="colorish">100</span> FREE SPINS</p>
			<a href="#" class="offer-link">Get</a>
		</div>
	</div>
</div>
<div class="overview">
	<div class="container">
		<div class="overview-list">
			<div class="card">
				<h3 class="card-title">Overview</h3>
				<div class="card-info">
					<h4 class="card-subtitle">WEBSITE</h4>
					<p class="card-text">barbadoscasino.com</p>
					<h4 class="card-subtitle">SOFTWARE</h4>
					<p class="card-text">Microgaming, Net Entertainment, Evolution Gaming, Play’n GO, Amaya, NextGen Gaming, Nyx Interactive, Elk Studios</p>
					<h4 class="card-subtitle">LICENCE</h4>
					<p class="card-text">Malta Gaming Authority, UK Gambling Commission
					</p>
					<h4 class="card-subtitle">CURRENCIES</h4>
					<p class="card-text">AUD, CAD, EUR, GBP, NOK, SEK, USD</p>
					<h4 class="card-subtitle">LANGUAGES</h4>
					<p class="card-text">German, English, Finnish, Norwegian, Swedish</p>
				</div>
			</div>
			<div class="card">
				<h3 class="card-title">DEPOSIT / WITHDRAWAL</h3>
				<div class="card-info">
					<h4 class="card-subtitle">DEPOSIT METHODS</h4>
					<p class="card-text">PaySafe Card, NETELLER, Skrill, Trustly, Euteller, EcoPayz, Giropay, Zimpler, EntroPay, InstaDebit, POLi, Visa, Mastercard, Maestro, Nordea, UseMyFunds, Click and Buy, EPS, Entercash, Fast Bank Transfers , SIRU Mobile, Boleto, Ticket Premium, Payr, Sofortuberweisung, Transferencia Bancaria Local-LobaNet, Mister Cash, Fund Send, Bank Wire Transfer, Ukash, Skrill 1-Tap</p>
					<h4 class="card-subtitle">DEPOSIT METHODS</h4>
					<p class="card-text">NETELLER, Skrill, EcoPayz, EntroPay, InstaDebit, Visa, Mastercard, Click and Buy, Entercash, Boleto, Bank Wire Transfer, Skrill 1-Tap, Danske Bank</p>
					<h4 class="card-subtitle">MAX WITHDRAWAL</h4>
					<p class="card-text">7000 USD per month</p>
					<h4 class="card-subtitle">PAYOUT</h4>
					<p class="card-text">48 hours pending period</p>
				</div>
			</div>
			<div class="card">
				<h3 class="card-title">ADDITIONAL</h3>
				<div class="card-info">
					<h4 class="card-subtitle">ONLINE</h4>
					<p class="card-text">Since 2017</p>
					<h4 class="card-subtitle">OWNER</h4>
					<p class="card-text">Aspire Global International Ltd Casinos</p>
					<h4 class="card-subtitle">EMAIL</h4>
					<p class="card-text colorish">care@Barbadoscasino.com</p>
					<h4 class="card-subtitle">RESTRICTED COUNTRIES</h4>
					<p class="card-text">Afghanistan, American Samoa, Australia, Belgium, Bulgaria, Bouvet Island, Cyprus, Czech Republic, Denmark, Algeria, Ecuador, Estonia, Western Sahara, Spain, Ethiopia, Faroe Islands, France, French Republic, Greenland, Greece, South Georgia and the South Sandwich Islands, Guam, Hong Kong, Hungary, Indonesia, Ireland, Israel, India, British Indian Ocean Territory, Iraq, Iran, Jordan, Comoros the, North Korea, South Korea, Kuwait, Libyan Arab Jamahiriya, Montenegro, Marshall Islands, Myanmar, Northern Mariana Islands, Malaysia, New Caledonia, Nigeria, Philippines, Pakistan, Poland, Puerto Rico, Palestinian Territory, Portugal, Romania, Sudan, Singapore, Slovenia, Somalia, Somali Republic, Syrian Arab Republic, Turkey, United States of America, Vatican, United States Virgin Islands, Yemen, South Africa
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="casino">
	<div class="container">
		<div class="casino-wrapper">
			<div class="casino-item">
				<div class="casino-left">
					<div class="logo">
						<img src="/img/spin-palace.png" alt="" class="casino-img">
						<div class="logo-rate">4.7</div>
					</div>
					<div class="casino-info">
						<h2 class="casino-title">SPIN PALACE CASINO</h2>
						<div class="casino-subtitle">Cashback</div>
						<p class="casino-text">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one</p>
					</div>
				</div>
				<div class="casino-right">
					<div class="casino-sale">
						<span class="casino-percent">15%</span>
						<span class="casino-spins">+ 50</span>
					</div>
					<a href="#" class="casino-link">Get Cashback</a>
				</div>
			</div>
			<div class="casino-item">
				<div class="casino-left">
					<div class="logo">
						<img src="/img/spin-palace.png" alt="" class="casino-img">
						<div class="logo-rate">4.7</div>
					</div>
					<div class="casino-info">
						<h2 class="casino-title">SPIN PALACE CASINO</h2>
						<div class="casino-subtitle">Cashback</div>
						<p class="casino-text">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one</p>
					</div>
				</div>
				<div class="casino-right">
					<div class="casino-sale">
						<span class="casino-percent">15% </span>
						<span class="casino-spins">+ 50</span>
					</div>
					<a href="#" class="casino-link">Get Cashback</a>
				</div>
			</div>
			<div class="casino-item">
				<div class="casino-left">
					<div class="logo">
						<img src="/img/spin-palace.png" alt="" class="casino-img">
						<div class="logo-rate">4.7</div>
					</div>
					<div class="casino-info">
						<h2 class="casino-title">SPIN PALACE CASINO</h2>
						<div class="casino-subtitle">Cashback</div>
						<p class="casino-text">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one</p>
					</div>
				</div>
				<div class="casino-right">
					<div class="casino-sale">
						<span class="casino-percent">15%</span>
					</div>
					<a href="#" class="casino-link">Get Cashback</a>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="slides">
	<div class="container">
		<div class="slider slick-slider">
			<div class="slider-item">
				<p class="slider-title">Mybet casino</p>
				<div class="slider-middle">
					<div class="slider-image">
						<img src="/img/spin-palace.png" alt="">
					</div>
					<div class="slider-payments">
						<p class="slider-payments--how">25$</p>
						<p class="slider-payments--what">giveaway for dep over 100$</p>
					</div>
				</div>
				<a href="" class="slider-link">More</a>
			</div>
			<div class="slider-item">
				<p class="slider-title">Spin palace casino</p>
				<div class="slider-middle">
					<div class="slider-image">
						<img src="" alt="">
					</div>
					<div class="slider-payments">
						<p class="slider-payments--how">25$</p>
						<p class="slider-payments--what">giveaway for dep over 100$</p>
					</div>
				</div>
				<a href="" class="slider-link">More</a>
			</div>
			<div class="slider-item">
				<p class="slider-title">Pp casino</p>
				<div class="slider-middle">
					<div class="slider-image">
						<img src="/img/spin-palace.png" alt="">
					</div>
					<div class="slider-payments">
						<p class="slider-payments--how">25%</p>
						<p class="slider-payments--what">cashback in&nbsp;a&nbsp;month</p>
					</div>
				</div>
				<a href="" class="slider-link">More</a>
			</div>
			<div class="slider-item">
				<p class="slider-title">One More Casino</p>
				<div class="slider-middle">
					<div class="slider-image">
						<img src="/img/spin-palace.png" alt="">
					</div>
					<div class="slider-payments">
						<p class="slider-payments--how">25%</p>
						<p class="slider-payments--what">cashback in&nbsp;a&nbsp;month</p>
					</div>
				</div>
				<a href="" class="slider-link">More</a>
			</div>
			<div class="slider-item">
				<p class="slider-title">Two More Casino</p>
				<div class="slider-middle">
					<div class="slider-image">
						<img src="/img/spin-palace.png" alt="">
					</div>
					<div class="slider-payments">
						<p class="slider-payments--how">25%</p>
						<p class="slider-payments--what">cashback in&nbsp;a&nbsp;month</p>
					</div>
				</div>
				<a href="" class="slider-link">More</a>
			</div>
		</div>
	</div>
</div>