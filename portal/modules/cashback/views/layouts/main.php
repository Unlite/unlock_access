<?php
/* @var $this \yii\web\View */
/* @var $content string */
use portal\assets\CashbackAsset;
use yii\helpers\Html;
use yii\helpers\Url;

CashbackAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <title><?= Html::encode($this->title) ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="icon" href="/img/favicon/favicon.ico">
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>

</head>

<body>
    <?php $this->beginBody() ?>
    <header class="header">
        <div class="container">
            <div class="toggler" id="toggler">
                <span></span>
            </div>
            <a class="header-title" href="<?=Url::to(['/'])?>">CROUPIERICA</a>

            <div class="header-background " id="header-bg">
                <?= \yii\widgets\Menu::widget([
						'items' => [
							['label' => Yii::t('cashback', 'Cabinet'), 'url' => ['/cashback/cabinet'] ],
							['label' => Yii::t('cashback', 'Casino'), 'url' => ['/cashback/casino'] ],
							['label' => Yii::t('cashback', 'Crouperica portal'), 'url' => ['/']],
						],
						'options' => [
							'class' => 'header-nav header-middle',
							'id' => 'nav',
						],
						'linkTemplate' => Html::a('{label}','{url}',['class'=>'header-link']),
						'activeCssClass'=>'active',
						'itemOptions'=>['class'=>'header-nav-item']

					]);
					?>
            </div>
            <?php if(Yii::$app->user->isGuest) {?>
            <?= \yii\widgets\Menu::widget([
					'items' => [
						['label' => Yii::t('cashback', 'Log In'),  'options' => ['class' => 'header-end-item js-login-popup-show js-header-log']],
						['label' => Yii::t('cashback', 'Registration'),  'options' => ['class' => 'header-end-item js-login-popup-show js-header-reg']],
						['label' => '', 'url' => '#','template' => '', 'options' => ['class' => 'header-end-item header-language header-language--usa'] ],
					],
					'options' => [
						'class' => 'header-nav header-end',
					],
					'linkTemplate' => Html::a('{label}','{url}',['class'=>'']),
					'activeCssClass'=>'',
					'itemOptions'=>['class'=>'header-end-item']

				]);
				?>
            <?php } else {?>
            <?= \yii\widgets\Menu::widget([
					'items' => [
						['label' => Yii::t('cashback', 'billing'), 'url' => ['/cashback/billing'], 'template' => Html::a('{label}','{url}',['class' => 'header-btn-link header-btn-link--billing'])],
						['label' => Yii::t('cashback', 'tickets'), 'url' => ['/cashback/ticket'], 'template' => Html::a('{label}','{url}',['class' => 'header-btn-link header-btn-link--tickets'])],
						['label' => Yii::t('cashback', 'settings'), 'url' => ['/site/settings'], 'template' => Html::a('{label}','{url}',['class' => 'header-btn-link header-btn-link--settings'])],
						['label' => Yii::t('cashback', 'exit'), 'url' => ['/logout'], 'template' => Html::a('{label}','{url}',['class' => 'header-btn-link header-btn-link--exit'])],
					],
					'options' => [
						'class' => 'header-menu',
					],
					'activeCssClass'=>'header-btn-link--active',
					'itemOptions'=>['class'=>'header-btn']

				]);
				?>
            <?php } ?>

        </div>
    </header>
    <main class="main">
        <?=$content?>
    </main>

    <footer class="footer">
        <div class="container">
            <div class="footer-wrapper">
                <div class="footer-about">
                    <p class="footer-title"><?=Yii::t('cashback','Croupierica')?></p>
                    <p class="footer-text"><?=Yii::t('cashback','Our website is dedicated to gathering every piece of information there is on online casinos and internet gambling in general. Our team is tirelessly spinning the virtual reels and surfing the web to deliver you honest impressions and the latest news, so you can always find something interesting and useful about your hobby.')?></p>
                    <p class="footer-text"><?=Yii::t('cashback','By using the website, you agree to our terms of use and confirm that you are over the age of 18.')?></p>
                </div>
                <div class="footer-about">
                    <p class="footer-title"><?=Yii::t('cashback','Cashback')?></p>
                    <p class="footer-text"><?=Yii::t('cashback','We know how important it is to have another chance after a bitter losing streak, that\'s why we created the cashback system that will allow you to get back some of the funds that you wagered. Make sure to always use links from Croupierica to gain additional advantage and get another go at winning, whether you are a passionate gambler or just playing for fun.')?></p>
                </div>

                <div class="footer-links-wrap">
                	<div class="footer-links">
                	    <a href="<?=Url::to(['site/signup'])?>" class="footer-link"><?=Yii::t('cashback','Create an Account')?></a>
                	    <a href="<?=Url::to(['/casino'])?>" class="footer-link"><?=Yii::t('cashback','Casino')?></a>
                	    <a href="<?=Url::to(['/game'])?>" class="footer-link"><?=Yii::t('cashback','Games')?></a>
                	    <a href="<?=Url::to(['/bonuses'])?>" class="footer-link"><?=Yii::t('cashback','Bonuses')?></a>
                	    <a href="<?=Url::to(['/blog'])?>" class="footer-link"><?=Yii::t('cashback','Tips&amp;guides')?></a>
                	    <a href="<?=Url::to(['/cashback/'])?>" class="footer-link"><?=Yii::t('cashback','Cashback')?></a>
                	    <a href="<?=Yii::$app->frontendUrlManager->createAbsoluteUrl('/')?>" class="footer-link"><?=Yii::t('cashback','Vpn')?></a>
                	</div>
                	
                	<div class="footer-links">
                	
                	    <a href="<?=Url::to(['site/about'])?>" class="footer-link"><?=Yii::t('cashback','About')?></a>
                	    <a href="<?=Url::to(['site/contact'])?>" class="footer-link"><?=Yii::t('cashback','Contact')?></a>
                	    <a href="<?=Url::to(['site/faq'])?>" class="footer-link"><?=Yii::t('cashback','Faq')?></a>
                	    <a href="<?=Url::to(['site/terms-of-service'])?>" class="footer-link"><?=Yii::t('cashback','Terms of service (terms of use)')?></a>
                	    <a href="<?=Url::to(['site/privacy'])?>" class="footer-link"><?=Yii::t('cashback','Privacy')?></a>
                	    <a href="<?=Url::to(['site/map'])?>" class="footer-link"><?=Yii::t('cashback','Sitemap')?></a>
                	    <div class="footer-icons">
                	        <div class="footer-icon footer-icon--eighteen"></div>
                	        <div class="footer-icon footer-icon--gamcare"></div>
                	        <div class="footer-icon footer-icon--bga"></div>
                	    </div>
                	</div>
                </div>
            </div>
        </div>
    </footer>
    <div class="login-popup js-popup">
        <div class="login-popup-overlay js-popup-close"></div>
        <div class="login-popup-content">
            <button class="login-popup-close js-popup-close">×</button>
            <?php if(Yii::$app->user->isGuest) {
		echo $this->render('@app/views/layouts/_new_auth',['loginForm' => new \common\models\forms\LoginForm(),'signupForm' =>new \common\models\forms\SignUpForm()]);
	} ?>
        </div>
    </div>
    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>
