<?php
use yii\bootstrap\Html;

/* @var $model common\models\Site */
$favicon = $model->getSiteImages()->type('rect')->one()->src ?? Url::to(['img/casino/malina-casino.png']);
?>


    
<?= Html::img($favicon,['class' => 'second-img']) ?>
<div class="second-text"><?= $model->name ?></div>
