<?php
use yii\helpers\Html;
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 02.04.2019
 * Time: 18:05:49
 */
?>
<section class="first">
  <div class="container">
    <div class="first-left">
      <h1 class="first-title">CROUPIERICA</h1>
      <h2 class="first-subtitle">RETURN MONEY FOR PURCHASES</h2>
      <p class="first-text">You probably already know how it works. You pay, you get a small percentage of spent money back. What about online casinos? Those guys are usually doing things differently. Some casinos give you cashback on losses only, while some do not mind covering even your winning bets. Some websites have dedicated promotions, while some promise you rebate on a weekly basis. It's all about what you need and what you choose in the end, but we decided to streamline and simplify that part. Welcome to Croupierica Cashback!<br />
        All you need to enjoy all the benefits is to create an account with us, choose the partner casino you deem appropriate and sign up with it using our links. Voila, now you are a part of our cashback system, and whatever you play, you can be certain that the promised percentage will be returned to your wallet. Sounds good? I bet it does! </p>
    </div>
    <div class="first-right">
      <?=$this->render('@app/views/layouts/_new_auth',['loginForm' => new \common\models\forms\LoginForm(),'signupForm' =>new \common\models\forms\SignUpForm()])?>
    </div>
  </div>
</section>
<section class="second">
  <div class="container">
    <h2 class="second-title">Our partners</h2>
    <ul class="second-list">
      <li class="second-item">
        <?= Html::img('@web/img/cashback-land/bet.png', ['class' => 'second-img']) ?>
        <div class="second-text">10 Bet</div>
      </li>
      <li class="second-item">
        <?= Html::img('@web/img/cashback-land/bet.png', ['class' => 'second-img']) ?>

        <div class="second-text">10 Bet</div>
      </li>
      <li class="second-item">
        <?= Html::img('@web/img/cashback-land/bet.png', ['class' => 'second-img']) ?>

        <div class="second-text">10 Bet</div>
      </li>
      <li class="second-item">
        <?= Html::img('@web/img/cashback-land/bet.png', ['class' => 'second-img']) ?>

        <div class="second-text">10 Bet</div>
      </li>
      <li class="second-item">
        <?= Html::img('@web/img/cashback-land/bet.png', ['class' => 'second-img']) ?>

        <div class="second-text">10 Bet</div>
      </li>
      <li class="second-item">
        <?= Html::img('@web/img/cashback-land/bet.png', ['class' => 'second-img']) ?>

        <div class="second-text">10 Bet</div>
      </li>
    </ul>
  </div>
</section>
<section class="third">
  <div class="container">
    <h2 class="third-title">8,042 645 USERS</h2>
    <h3 class="third-subtitle"><?=Yii::t('cashback','Why they choose us?')?></h3>
    <p class="third-text"><?=Yii::t('cashback','Gambling might be (and usually is) quite a costly hobby. Dedicated players spend hundreds and thousands of hard-earned dollars in order to try and catch the bluebird while getting constant powerful thrills. Some of those players think that the excitement and the possibility of a win worth it all, while the others can\'t let go of the hopes of hitting it big.<br />Whichever group you associate with, we can all agree that getting back some of the lost money is good. The solution to that was found by restless promotional teams of gambling websites, who decided (rightfully so) that their members are tired from the typical bonus offers with shady terms that promise a lot but bring you nothing. Now a lot of online casinos have cashback offers that reward loyal players and give another chance to a losing one. We decided that it\'s better to have a unified system that will help everyone benefit without any holdbacks.')?></p>
  </div>
</section>
<section class="fourth">
  <div class="container">
    <div class="fourth-list">
      <div class="fourth-item">
        <?= Html::img('@web/img/cashback-land/acc.svg', ['class' => 'fourth-img']) ?>
        <h3 class="fourth-title"><?=Yii::t('cashback','CREATE AN <br />ACCOUNT')?></h3>
        <p class="fourth-text"><?=Yii::t('cashback','Sign up with Croupierica and you\'ll always have another go at winning! It\'s quick, simple and fun. Make use of our extensive database of casinos and games and get even closer to winning.')?><br /><br />

          <?=Yii::t('cashback','Play at online casinos using our links and you\'ll be eligible to get back a percentage of your wagers.')?></p>
      </div>
      <div class="fourth-item">
        <?= Html::img('@web/img/cashback-land/play.svg', ['class' => 'fourth-img']) ?>
        <h3 class="fourth-title"><?=Yii::t('cashback','CHOOSE A CASINO FROM OUR <br />LIST OF OFFERS AND PLAY')?></h3>
        <p class="fourth-text fourth-text--center"><?=Yii::t('cashback','The idea behind Croupierica was not only to gather as much useful information about online gambling as possible, but also give our members an advantage over the average Joe. All you need to enjoy all the benefits is to create an account with us, choose the partnered casino and sign up with it using our links. Now you are a part of our cashback system, and whatever you play, you can be certain that the promised percentage will be returned to your wallet.')?> </p>
      </div>
      <div class="fourth-item">
        <?= Html::img('@web/img/cashback-land/cash.svg', ['class' => 'fourth-img']) ?>
        <h3 class="fourth-title"><?=Yii::t('cashback','GET CASHBACK TO YOUR <br />ACCOUNT')?></h3>
        <p class="fourth-text"><?=Yii::t('cashback','After you\'ve created an account with us, chosen the online casino and signed up using our link, just start playing for real money! Spin those slots, place those bets! After you think that you\'ve played enough and it is time to reload your weapon, go into your profile, check the amount of money you\'ve earned and start cashing it out. Yep, it is that easy. No one wants tedious procedures and slow service, and we know it, that\'s why we simplified the process to an extreme!')?></p>
      </div>
    </div>
  </div>
</section>
<section class="fifth first">
  <div class="container">
    <div class="first-left">
      <h1 class="first-title"><?=Yii::t('cashback','CROUPIERICA')?></h1>
      <p class="first-text"><?=Yii::t('cashback','This website was created by gamblers for gamblers, that\'s why our team perfectly understands the needs and woes of fellow online casino fans. Be it general trustworthiness of the gambling websites, their promotions, recent news of the industry - we\'ve got you covered. But the main attraction is, of course, the cashback that we thought through for all of you passionate players out there.')?> <br /><br />

        <?=Yii::t('cashback','If you are still on the fence regarding some of our terms or have any questions in general, do not hesitate to contact us using the form below.')?></p>
    </div>
    <div class="first-right">
      <?=$this->render('@app/views/layouts/_new_auth',['loginForm' => new \common\models\forms\LoginForm(),'signupForm' =>new \common\models\forms\SignUpForm()])?>
    </div>
  </div>
</section>
<?php $this->registerJs(" $('.second-list').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    responsive: [
      {
        breakpoint: 1120,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
    }
  ]
  });
  $('.fourth-list').slick({
    dots: false,
    infinite: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1120,
        settings: {
          infinite: true,
          autoplay: true,
          autoplaySpeed: 16000,
          slidesToShow: 1,
          slidesToScroll: 1
        }
    }
  ]
  });", \yii\web\View::POS_END) ?>
