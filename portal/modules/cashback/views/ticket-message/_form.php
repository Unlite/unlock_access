<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \portal\models\forms\TicketForm */
?>


    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'message')->textarea(['rows' => 5,'cols' => 60,'placeholder' => 'Text here','class' => 'chat-text'])->label(false) ?>
    <?= $form->field($model, 'images[]',[
        'options' => ['style' => 'display:none']
    ])->fileInput([
        'multiple' => true,
    ])->label(false) ?>
    <span class="chat-attach creating-attach " onclick="document.getElementById('ticketform-images').click()"></span>
<?= Html::submitButton('Send', ['class' => 'creating-btn']) ?>
    <?php ActiveForm::end(); ?>
<div class="creating-item creating-files" style="display: none;">
    <p class="creating-subtitle"><?=Yii::t('cashback','Attached files:')?></p>
    <div id="files" class="creating-files-list">
    </div>
</div>
<?php $this->registerJsFile('/js/image_preview.js',['pos' => \yii\web\View::POS_END]);
