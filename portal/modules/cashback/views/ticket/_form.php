<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 16.05.2019
 * Time: 22:59:32
 * @var $this yii\web\View
 * @var $model \portal\models\forms\TicketForm
 * @var $form yii\widgets\ActiveForm
 */
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<div class="creating">
	<div class="creating-body">
		<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
		<div class="creating-item">
			<p class="creating-subtitle"><?=Yii::t('cashback','Topic:')?></p>
			<?= $form->field($model, 'title')->textarea([
				'class'=>'creating-text creating-text--topic',
				'placeholder' => 'Topic',
				'cols' => '30',
				'rows' => '1',
			])->label(false) ?>
		</div>
		<div class="creating-item">
			<p class="creating-subtitle"><?=Yii::t('cashback','Issue:')?></p>
			<?= $form->field($model, 'message')->textarea([
				'class'=>'creating-text creating-text--content',
				'placeholder' => 'Text here',
				'cols' => '0',
				'rows' => '7',
			])->label(false) ?>
			<?= $form->field($model, 'images[]',[
				'options' => ['style' => 'display:none']
			])->fileInput([
				'multiple' => true,
			])->label(false) ?>
			<span class="creating-attach" onclick="document.getElementById('ticketform-images').click()"></span>
		</div>
		<div class="creating-item creating-files"  style="display: none;">
			<p class="creating-subtitle"><?=Yii::t('cashback','Attached files:')?></p>
			<div id="files" class="creating-files-list">
			</div>
		</div>
		<?= Html::submitButton(Yii::t('cashback','Create'), ['class' => 'creating-btn']) ?>
		<?php ActiveForm::end(); ?>

	</div>
</div>

<?php $this->registerJsFile('/js/image_preview.js');

