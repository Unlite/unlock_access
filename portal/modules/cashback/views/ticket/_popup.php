<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 04.04.2019
 * Time: 14:37:20
 */
?>
<?php
use yii\helpers\Html;
?>
<div class="chat-popup js-popup">
   <div class="chat-popup-overlay js-popup-close"></div>
    <div class="chat-popup-content">
        <div class="chat-popup-top">
            <button class="chat-popup-photo chat-popup-photo--upload">
                Upload a photo
            </button>
            <button class="chat-popup-photo chat-popup-photo--take">
                Take a photo
            </button>
        </div>
        <div class="chat-popup-nofiles">
            No uploaded photos or drop files here
        </div>
    </div>
</div>
