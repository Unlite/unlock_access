<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 02.04.2019
 * Time: 18:24:52
 * @var \backend\models\search\TicketSearch $searchModel
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \yii\web\View $this
 */
use yii\helpers\Html;
use yii\widgets\ListView;
?>
<div class="tickets">
    <div class="tickets-wrap">
        <div class="tickets-left">
            <div class="status">
                <?=Html::a(Yii::t('cashback','Open tickets'),['/cashback/ticket/','status'=>\common\models\Ticket::STATUS_OPENED],['class' => 'status-title status-title--open'])?>
                <?=Html::a(Yii::t('cashback','Closed tickets'),['/cashback/ticket/','status'=>\common\models\Ticket::STATUS_CLOSED],['class' => 'status-title status-title--closed'])?>
            </div>
            <div class="create">
                <?=Html::a(Yii::t('cashback','Create new tickets'),['/cashback/ticket'],['class' => 'create-btn'])?>
            </div>
            <div class="re">
				<?=ListView::widget([
					'dataProvider' => $dataProvider,
					'itemView' => '_item',
					'emptyText' => "",
					'options' => [
						'class' => 're-list',
						'tag' => 'ul',
					],
					'summary' => '',
					'layout' => '{items}'
				]);
				?>
            </div>
        </div>
        <div class="tickets-right message">
            <?=$this->render('_form',['model' => $model])?>
        </div>
    </div>
</div>
