<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 17.05.2019
 * Time: 02:43:13
 * @var \common\models\TicketMessage $model
 */
use yii\helpers\Html;
?>

<div class="msg <?=$model->ticket->user_id == Yii::$app->user->id ? 'msg-out' : 'msg-in'?>">
	<p class="msg-title <?=$model->ticket->user_id == Yii::$app->user->id ? '' : 'msg-title--in'?>"><?=$model->ticket->user_id == Yii::$app->user->id ? Yii::t('cashback','User') : Yii::t('cashback','Support Agent')?></p>
	<p class="msg-text"><?=$model->message?></p>
	<?php if($model->ticketImages) {?>
		<div class="msg-attach">
			<?php foreach ($model->ticketImages as $ticketImage) {?>
				<div class="msg-attach-img">
					<?= Html::img($ticketImage->src) ?>
				</div>
			<?php } ?>
		</div>
	<?php } ?>
</div>
