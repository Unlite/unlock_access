<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 02.04.2019
 * Time: 18:24:57
 * @var \portal\models\forms\TicketForm $model
 * @var \yii\data\ActiveDataProvider $dataProvider
 */
use yii\helpers\Html;
use yii\widgets\ListView;
?>
<div class="tickets">
    <div class="tickets-wrap">
        <div class="tickets-left">
            <div class="status">
				<?=Html::a(Yii::t('cashback','Open tickets'),['/cashback/ticket','status'=>\common\models\Ticket::STATUS_OPENED],['class' => 'status-title status-title--open'])?>
				<?=Html::a(Yii::t('cashback','Closed tickets'),['/cashback/ticket','status'=>\common\models\Ticket::STATUS_CLOSED],['class' => 'status-title status-title--closed'])?>
            </div>
            <div class="create">
				<?=Html::a(Yii::t('cashback','Create new tickets'),['/cashback/ticket'],['class' => 'create-btn'])?>
            </div>
            <div class="re">
				<?=ListView::widget([
					'dataProvider' => $dataProvider,
					'itemView' => '_item',
					'emptyText' => "",
					'options' => [
						'class' => 're-list',
						'tag' => 'ul',
					],
					'summary' => '',
					'layout' => '{items}'
				]);
				?>
            </div>
        </div>
        <div class="tickets-right message">
            <div class="chat">
                <div class="creating-head">
                    <div class="creating-left">
                        <div class="creating-pre">RE:</div>
                        <p class="creating-title"><?=$model->ticket->title?></p>
                    </div>

                    <?php if($model->ticket->status !== \common\models\Ticket::STATUS_CLOSED) echo Html::a(Yii::t('cashback','Close ticket'),['/cashback/ticket/close','id'=>$model->ticket->id],['class' => 'status-title status-title--close'])?>
                </div>
                <div class="chat-body">
                    <div class="chat-content">
						<?=ListView::widget([
							'dataProvider' => new \yii\data\ActiveDataProvider([
								'query' => $model->ticket->getTicketMessages()
							]),
							'itemView' => 'ticket-message/_item',
							'emptyText' => "",
							'options' => [
								'class' => 're-list',
								'tag' => 'ul',
							],
							'summary' => '',
							'layout' => '{items}'
						]);
						?>
                    </div>
                    <div class="chat-text-wrap">
						<?=$this->render('/ticket-message/_form',['model' => $model])?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

