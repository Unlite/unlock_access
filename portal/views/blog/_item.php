<?php

use yii\bootstrap\Html;

/* @var $model common\models\BlogPost */
?>
<div class="news-line-item <?= $model->thumbnail_src ? : '' ?>">
    <?php if ($model->thumbnail_src) { ?>
            <?= Html::a(Html::img($model->thumbnail_src), ['/blog/view', 'slug' => $model->slug],['class' => 'news-line-img']); ?>
    <?php } ?>
    <div class="news-line-content">
        <?= Html::a('<h2 class="news-line-title">' . $model->title . '</h2>', ['/blog/view', 'slug' => $model->slug]) ?>
        <div class="news-line-hash">
            <?php foreach ($model->tags as $tag) { ?>
                <a href="">#<?= $tag->name ?></a>
            <?php } ?>
        </div>
    </div>
</div>
