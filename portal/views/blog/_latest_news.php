<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 23.08.2018
 * Time: 16:59:42
 */
/* @var $model \common\models\Site */
/* @var $blogPostProvider \yii\data\ActiveDataProvider */
/* @var $amount */
use yii\widgets\ListView;

if(!isset($blogPostProvider)){
	$models = isset($model) ?
		$model->getBlogPosts()->innerJoinWith(['tags'  => function($query) { return $query->where(['name' => 'news']);}])->orderBy(['updated_date_time' => SORT_DESC])->limit(isset($amount) ? $amount : 6)->all()
		: \common\models\BlogPost::find()->innerJoinWith(['tags'  => function($query) { return $query->where(['name' => 'news']);}])->orderBy(['updated_date_time' => SORT_DESC])->limit(isset($amount) ? $amount : 6)->all();
	$blogPostProvider = new \yii\data\ArrayDataProvider([
		'allModels' => $models
	]);
}

?>

<?php if($blogPostProvider->totalCount) {?>
    <h2 class="latest-title"><?=Yii::t('app','Latest news')?></h2>

    <?= ListView::widget([
		'dataProvider' => $blogPostProvider,
		'itemView' => '/blog/_latest_news_item',
		'emptyText' => "",
		'summary' => '',
		'options' => ['class'=>'latest-news-list'],
		'itemOptions' => ['class'=>'latest-news-item']
	]);
	?>
<?php } ?>