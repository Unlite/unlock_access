<?php
use yii\helpers\Html;
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 13.08.2018
 * Time: 19:02:04
 */

/* @var $model \common\models\BlogPost */

?>

<a class="latest-news-img" href="<?= \yii\helpers\Url::to(['/blog/view', 'slug' => $model->slug]) ?>">
        <img src="<?= $model->thumbnail_src ?>">
</a>
<div class="latest-news-content">
    <h3 class="latest-news-title mobile"><?= $model->title ?></h3>
    <div class="news-hash">
        <?php foreach ($model->tags as $tag) { ?>
            <a href="">#<?= $tag->name ?></a>
        <?php } ?>
    </div>
    <p class="">
    <?= Html::a(\yii\helpers\StringHelper::truncate($model->short_description, 106),['/blog/view', 'slug' => $model->slug],['class' => 'latest-news-text desktop']) ?>
    </p>
</div>
