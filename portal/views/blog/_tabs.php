<?php
use yii\helpers\Url;
$category = Yii::$app->request->get('blog') && isset(Yii::$app->request->get('blog')['category']) ? Yii::$app->request->get('blog')['category'] : null
?>

<div class="left-column">
    <div class="cashback-menu-wrap">
        <div class="cashback-menu">
            <a href="<?=Url::to(['/blog/'])?>" class="<?=Url::isRelative('blog/index') && $category == null ? 'active' : ''?> cashback-all cashback-link">
                <span><?=Yii::t('app', 'All');?></span>
            </a>
            <a href="<?=Url::to(['/blog/index','blog[category]' => Yii::$app->params['portal']->get('top_cashback_game_category.data')])?>"
               class="<?=$category == Yii::$app->params['portal']->get('top_cashback_game_category.data') ? 'active' : ''?> cashback-star cashback-link">
                <span><?=Yii::t('app', 'Casino news');?></span>
            </a>
            <a href="<?=Url::to(['/blog/index','blog[category]'=>Yii::$app->params['portal']->get('instant_details_game_category.data')])?>"
               class="<?=$category == Yii::$app->params['portal']->get('instant_details_game_category.data') ? 'active' : ''?> cashback-deals cashback-link">
                <span><?=Yii::t('app', 'Guides');?></span>
            </a>
            <a href="<?=Url::to(['/blog/index','blog[category]'=>Yii::$app->params['portal']->get('new_cashback_games_category.data')])?>"
               class="<?=$category == Yii::$app->params['portal']->get('new_cashback_games_category.data') ? 'active' : ''?> cashback-new cashback-link">
                <span><?=Yii::t('app', 'Promotions');?></span>
            </a>
        </div>
    </div>
</div>