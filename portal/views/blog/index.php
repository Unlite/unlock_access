<?php

use yii\widgets\ListView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\SiteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $pages yii\data\ActiveDataProvider */
$this->title = Yii::$app->params['portal']->get('page_attribute_tips_and_guides_title.data.en');
$this->registerMetaTag(['name'=>'title','content' => Yii::$app->params['portal']->get('page_attribute_tips_and_guides_title.data.en')]);
$this->registerMetaTag(['name'=>'description','content' => Yii::$app->params['portal']->get('page_attribute_tips_and_guides_description.data.en')]);
?>


<div class="container">
    <div class="cashback-wrap tips-bg">
        <div class="cashback-info">
            <?=$this->render('/search/_search')?>
            <h1 class="cashback-title"><?=Yii::t('app','tips & guides')?></h1>
            <p class="cashback-info-text">
                <?=Yii::t('app','Everything you wanted to know about the online gambling world is gathered here. Latest news about the legal status of gambling activities, new technologies, openings and closings of the websites, their policies – be always aware of what\'s going on with your hobby. Our team has also created some guides for both newcomers and veteran gamblers so that your playtime can be more meaningful, and your losses less painful.')?>
            </p>
        </div>
        </div>
        <div class="container two-columns">
            <div class="row">
				<?=$this->render('_tabs')?>
                <div class="right-column">
                    <div class="croupierica-choice-top"></div>
                </div>
            </div>
            <div class="row">
               <div class="left-column">
                    <div class="left-column-content">
                        <section class="news-wrap">
							<?= ListView::widget([
								'dataProvider' => $dataProvider,
								'itemView' => '_item',
								'emptyText' => "",
								'summary' => '',
								'layout' => '{items}'
							]);
							?>
                        </section>
                    </div>
                </div>
                <div class="right-column">
					<?php echo $this->render('/custom-list/_promoted')?>
                    <div class="sidebar-menu-wrap"></div>
                </div>
                
            </div>
        </div>
</div>
<?=\portal\widgets\LinkPager::widget(['pagination'=>$pages])?>