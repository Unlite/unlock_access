<?php
/* @var $this yii\web\View */
/* @var $model common\models\BlogPost */

$this->title = $model->page_title;
$this->registerMetaTag(['name'=>'description','content'=>$model->page_description]);
?>
<section class="container news-article-wrap">
    <article class="news-article">
        <h1 class="news-article-title"><?=$model->title?></h1>
        <?php if ($model->tags) { ?>
            <div class="news-hash">
                <?php foreach ($model->tags as $tag){ ?>
                    <a href="">#<?=$tag->name?></a>
                <?php } ?>
            </div>
        <?php } ?>
        <div class="news-date"><?=Yii::$app->formatter->asDatetime($model->updated_date_time,'php: d F Y');?></div>
        <p class="news-article-text"><?= \yii\helpers\Markdown::process($model->text, 'gfm') ?>
        <?php if ($model->author_name) { ?>
        <p class="news-article-author"><?=$model->author_name?></p>
        <?php } ?>
    </article>
</section>
<section class="latest-news container latest-news--mb">
    <?=$this->render('/blog/_latest_news',['amount'=>3])?>
</section>
<?=$this->render('/site/_latest_casino_reviews',['amount'=>3])?>
