<?php

/* @var $model common\models\BonusType */
$bonus_type = Yii::$app->request->get('bonus') && isset(Yii::$app->request->get('bonus')['bonus_type']) ? Yii::$app->request->get('bonus')['bonus_type'] : null
?>
<li>
    <a class="<?=$bonus_type == $model->id ? 'active' : ''?>" href="<?= \yii\helpers\Url::to(['', 'bonus[bonus_type]' => $model->id]) ?>"><?= $model->name ?></a>
</li>
