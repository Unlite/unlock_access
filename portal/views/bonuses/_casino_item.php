<?php

use common\modules\away\widgets\ModelLinkWidget;
use yii\bootstrap\Html;

/* @var $site common\models\Site */
/* @var $model common\models\Bonuses */
/* @var $site \common\models\Site */
/* @var $this \yii\web\View */
/* @var $favicon string */
/* @var $screen string */
/* @var $sqr string */
if (!isset($site))
    $site = $model->site;
$favicon = $site->getSiteImages()->type('rect')->one() ? $site->getSiteImages()->type('rect')->one()->src : \yii\helpers\Url::to(['img/casino/malina-casino.png']);
$screen = $site->getSiteImages()->type('preview')->one() ? $site->getSiteImages()->type('preview')->one()->src : \yii\helpers\Url::to(['img/casino/malina-casino.png']);
$sqr = $site->getSiteImages()->type('sqr')->one() ? $site->getSiteImages()->type('sqr')->one()->src : \yii\helpers\Url::to(['img/casino/malina-casino.png']);

$outLink = ModelLinkWidget::widget(['model' => $model]);
$bonuses = $model->offerTypeBonuses;
$bonus = array_pop($bonuses);
?>
<div class="casino-item">
    <div class="logo">
        <?=Html::img($favicon,['class' => 'casino-img desktop'])?>
        <img src="<?= $sqr ?>" class="casino-img mobile">
        <div class="casino-raiting">
            <div class="logo-rate">
                <span><?= number_format($site->rating, 1, '.', '') ?></span>
            </div>
        </div>
    </div>
    <div class="casino-info">
        <h3 class="casino-title"><a href="<?= \yii\helpers\Url::to(['/casino/view', 'slug' => $site->slug,]); ?>"><?= $site->name ?></a></h3>
        <div class="casino-subtitle"><?= $model->bonusType->name ?></div>
        <p class="casino-text"><?= \yii\helpers\StringHelper::truncate($model->text, 250) ?></p>
    </div>
    <div class="casino-offer">
            <div class="casino-offer-text"><?= $bonus->offer_text ?></div>
            <div class="casino-offer-name"><?= $bonus->offerType->name ?></div>
    </div>
    <div class="casino-links">
        <?= Html::a(Yii::t('app', Yii::t('app','more')), ['casino/bonuses', 'slug' => $site->slug], ['class' => 'casino-link casino-link--blue']) ?>
        <?= Html::a(Yii::t('app', Yii::t('app','play')), $outLink, ['class' => 'casino-link', 'data' => ['toggle' => 'modal', 'target' => '#md' . $model->id]]) ?>
    </div>
</div>
<?= $this->render('_modal', ['site' => $site, 'model' => $model, 'favicon' => $favicon, 'outLink' => $outLink]); ?>
