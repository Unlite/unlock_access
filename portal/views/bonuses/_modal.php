<?php
use common\modules\away\widgets\ModelLinkWidget;
/* @var $model common\models\Bonuses */
/* @var $site \common\models\Site */
/* @var $this \yii\web\View */
/* @var $favicon string */
/* @var $outLink string */
/* @var $screen string */
/* @var $sqr string */
if(!isset($site))
	$site = $model->site;
$favicon = $site->getSiteImages()->type('rect')->one() ? $site->getSiteImages()->type('rect')->one()->src : \yii\helpers\Url::to(['img/casino/malina-casino.png']);
$screen = $site->getSiteImages()->type('preview')->one() ? $site->getSiteImages()->type('preview')->one()->src : \yii\helpers\Url::to(['img/casino/malina-casino.png']);
$sqr = $site->getSiteImages()->type('sqr')->one() ? $site->getSiteImages()->type('sqr')->one()->src : \yii\helpers\Url::to(['img/casino/malina-casino.png']);
$outLink = ModelLinkWidget::widget(['model' => $model]);
?>

<div class="main-modal modal fade" id="<?= 'md' . $model->id ?>" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="casino-modal">
                    <div class="casino-modal-header">
                        <div class="casino-modal-img">
                            <img src="<?= $favicon ?>" alt="">
                        </div>
                        <div class="casino-modal-right">
                            <h2 class="casino-modal-title"><?= $site->name ?></h2>
                            <div class="casino-modal-title-note"><?= $model->bonusType->name ?></div>
                        </div>
                    </div>
                    <div class="casino-modal-content">
                        <p class="casino-modal-text"><?= $model->text ?></p>
                        <div class="casino-offer-text"><?= $model->offerTypeBonus->offer_text ?></div>
                        <div class="casino-offer-name"><?= $model->offerTypeBonus->offerType->name ?></div>
                        <div class="button-wrap">
                            <?= \yii\helpers\Html::a(Yii::t('app','PLAY'), $outLink, ['class' => 'casino-link', 'target' => '_blank']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>