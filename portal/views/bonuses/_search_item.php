<?php

use common\modules\away\widgets\ModelLinkWidget;
use yii\bootstrap\Html;

/* @var $model common\models\Bonuses */
/* @var $site \common\models\Site */
/* @var $favicon string */
/* @var $outLink string */
/* @var $sqr string */
$site = $model->site;
$favicon = $site->getSiteImages()->type('rect')->one() ? $site->getSiteImages()->type('rect')->one()->src : \yii\helpers\Url::to(['img/casino/malina-casino.png']);
$screen = $site->getSiteImages()->type('preview')->one() ? $site->getSiteImages()->type('preview')->one()->src : \yii\helpers\Url::to(['img/casino/malina-casino.png']);
$sqr = $site->getSiteImages()->type('sqr')->one() ? $site->getSiteImages()->type('sqr')->one()->src : \yii\helpers\Url::to(['img/casino/get-cashback-malina.png']);
$outLink = ModelLinkWidget::widget(['model' => $model]);
?>


    <div class="logo">
        <?=Html::img($favicon,['class' => 'casino-img desktop'])?>
        <img src="<?= $sqr ?>" class="casino-img mobile">
        <div class="logo-rate"><?= number_format($site->rating, 1, '.', '') ?></div>
    </div>
    <div class="casino-info">
        <h2 class="casino-title"><?= $site->name ?></h2>
        <div class="casino-subtitle"><?= $model->bonusType->name ?></div>
        <p class="casino-text"><?= \yii\helpers\StringHelper::truncate($model->text, 140) ?></p>
    </div>
    <div class="casino-offer">
        <div class="casino-offer-text"><?= $model->offerTypeBonus->offer_text ?></div>
        <div class="casino-offer-name"><?= $model->offerTypeBonus->offerType->name ?></div>
    </div>
    <div class="casino-links">
        <?= Html::a(Yii::t('app','more'), ['/casino/view', 'slug' => $site->slug], ['class' => 'casino-link casino-link--blue']) ?>
        <?= Html::a(Yii::t('app','play'), $outLink, ['class' => 'casino-link', 'data' => ['toggle' => 'modal', 'target' => '#md' . $model->id]]) ?>
    </div>

<?= $this->render('_modal', ['site' => $site, 'model' => $model, 'favicon' => $favicon, 'outLink' => $outLink]); ?>
