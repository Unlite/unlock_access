<?php

use common\modules\away\widgets\ModelLinkWidget;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;

/* @var $site common\models\Site */
/* @var $model common\models\Bonuses */
/* @var $site \common\models\Site */
/* @var $this \yii\web\View */
/* @var $favicon string */
/* @var $screen string */
/* @var $sqr string */
if(!isset($site))
    $site = $model->site;
$favicon = $site->getSiteImages()->type('rect')->one() ? $site->getSiteImages()->type('rect')->one()->src : \yii\helpers\Url::to(['img/casino/malina-casino.png']);
$screen = $site->getSiteImages()->type('preview')->one() ? $site->getSiteImages()->type('preview')->one()->src : \yii\helpers\Url::to(['img/casino/malina-casino.png']);
$sqr = $site->getSiteImages()->type('sqr')->one() ? $site->getSiteImages()->type('sqr')->one()->src : \yii\helpers\Url::to(['img/casino/malina-casino.png']);

$outLink = ModelLinkWidget::widget(['model' => $model]);
$bonuses = $model->offerTypeBonuses;
$bonus = array_pop($bonuses);
?>
<div class="casino-item decoration">
    <div class="row justify-content-md-between">
        <div class="casino-logo-column">
            <div class="casino-logo-wrap">
                <div class="casino-logo"><img src="<?=$favicon?>"></div>
                <div class="casino-raiting">
                    <div class="casino-raiting-inner">
                        <span><?= number_format($site->rating, 1, '.', '') ?></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="casino-about-column col-xl-5 col-lg-7 col-md-8">
            <h3><?= $site->name ?></h3>
            <div class="note-title"><?= $model->bonusType->name ?></div>
            <p><?= \yii\helpers\StringHelper::truncate($model->text, 140) ?></p>
        </div>
        <div class="casino-cashback-column col-xl-2 col-lg-2 col-md-12">
            <div class="casino-cashback-wrap">
                <div class="casino-cashback">
                    <div class="cashback-col">
                        <div class="cashback-result"><?= $bonus->offer_text ?></div>
                        <div class="cashback-text"><?= $bonus->offerType->name ?></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cashback-buttons-column col-xl-2 col-md-12">
            <div class="cashback-buttons">
				<?= Html::a(Yii::t('app','play'), $outLink, ['class' => 'button', 'data' => ['toggle' => 'modal', 'target' => '#md' . $model->id]]) ?>
            </div>
        </div>
    </div>
</div>
<?= $this->render('_modal', ['site' => $site, 'model' => $model, 'favicon' => $favicon, 'outLink' => $outLink]); ?>

