<?php
use yii\helpers\Url;
$offer_type = Yii::$app->request->get('bonuses') && isset(Yii::$app->request->get('bonuses')['offer_type']) ? Yii::$app->request->get('bonuses')['offer_type'] : null
?>

<div class="left-column">
    <div class="cashback-menu-wrap">
        <div class="cashback-menu">
            <a href="<?=Url::to(['bonuses/'])?>"
               class="<?=Url::isRelative('bonuses/index') && $offer_type == null ? 'active' : ''?> cashback-all cashback-link">
                <span><?=Yii::t('app', 'All');?></span>
            </a>
            <a href="<?=Url::to(['bonuses/index','bonuses[offer_type]' => Yii::$app->params['portal']->get('top_cashback_offer_type.data')])?>"
               class="<?=$offer_type == Yii::$app->params['portal']->get('top_cashback_offer_type.data') ? 'active' : ''?> cashback-star cashback-link">
                <span><?=Yii::t('app', 'Exclusive');?></span>
            </a>
            <a href="<?=Url::to(['bonuses/index','bonuses[offer_type]'=>Yii::$app->params['portal']->get('free_money_offer_type.data')])?>"
               class="<?=$offer_type == Yii::$app->params['portal']->get('free_money_offer_type.data') ? 'active' : ''?> cashback-deals cashback-link">
                <span><?=Yii::t('app', 'Deposit bonus');?></span>
            </a>
            <a href="<?=Url::to(['bonuses/index','bonuses[offer_type]'=>Yii::$app->params['portal']->get('free_spins_offer_type.data')])?>"
               class="<?=$offer_type == Yii::$app->params['portal']->get('free_spins_offer_type.data') ? 'active' : ''?> cashback-spins cashback-link">
                <span><?=Yii::t('app', 'Free spins');?></span>
            </a>
        </div>
    </div>
</div>