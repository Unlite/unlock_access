<?php

use yii\widgets\ListView;
use yii\helpers\Url;

/**
 * @var $this yii\web\View
 * @var $pages \yii\data\Pagination
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $bonusTypeProvider yii\data\ActiveDataProvider
 */
$this->title = Yii::$app->params['portal']->get('page_attribute_bonuses_title.data.en');
$this->registerMetaTag(['name'=>'title','content' => Yii::$app->params['portal']->get('page_attribute_bonuses_title.data.en')]);
$this->registerMetaTag(['name'=>'description','content' => Yii::$app->params['portal']->get('page_attribute_bonuses_description.data.en')]);
?>
<div class="container">
    <div class="cashback-wrap bonuses-bg">
        <div class="cashback-info">
           <?=$this->render('/search/_search')?>
            <h1 class="cashback-title"><?=Yii::t('app','Bonuses category')?></h1>
            <p class="cashback-info-text">
                <?=Yii::t('app','It\'s hard to imagine a successful online casino without decent promotions. It\'s hard to imagine a passionate online gambler who doesn\'t enjoy participating in those. In this section of Croupierica you\'ll find some of the bigger and better promotional offers, including cashbacks, free spins, and deposit bonuses. Some of those are exclusive to our website, so make sure to only use links provided by Croupierica!')?>
            </p>
        </div>
        </div>
        <div class="container two-columns">
            <div class="row">
                <?=$this->render('_tabs')?>
                <div class="right-column">
					<div class="croupierica-choice-top"></div>
                </div>
            </div>
            <div class="row">
               <div class="left-column">
                    <div class="left-column-content">
                        <div class="casino-list">
							<?php
							echo ListView::widget([
								'dataProvider' => $dataProvider,
								'itemView' => '/bonuses/_item',
								'emptyText' => "",
								'summary' => '',
								'layout' => '{items}'
							]);
							?>
                        </div>
                    </div>
                </div>
                <div class="right-column">
					<?php echo $this->render('/custom-list/_promoted')?>
                    <div class="sidebar-menu-wrap">
                        <div class="sidebar-menu-title"><?=Yii::t('app', 'All filters');?></div>
                        <div class="sidebar-menu">
                            <div class="sidebar-category-title"><?=Yii::t('app', 'Bonus type');?></div>
                            <ul class="sidebar-menu-inner">
								<?= ListView::widget([
									'dataProvider' => $bonusTypeProvider,
									'itemView' => '/bonus-type/_filter_item',
									'emptyText' => "",
									'summary' => '',
								]);
								?>
                            </ul>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    
</div>
<?=\portal\widgets\LinkPager::widget(['pagination'=>$pages])?>


