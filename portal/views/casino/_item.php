<?php

use common\modules\away\widgets\ModelLinkWidget;
use yii\bootstrap\Html;
use yii\helpers\Url;

/* @var $model common\models\Site */
$favicon = $model->getSiteImages()->type('rect')->one() ? $model->getSiteImages()->type('rect')->one()->src : Url::to(['img/casino/malina-casino.png']);
$sqr= $model->getSiteImages()->type('sqr')->one() ? $model->getSiteImages()->type('sqr')->one()->src : Url::to(['img/casino/malina-casino.png']);
//UNUSED// $screen = $model->getSiteImages()->type('preview')->one()->src ?? Url::to(['img/casino/malina-casino.png']);

$mainDomain = $model->getDomains()->typeMain()->one();

$playUrl = $mainDomain ? ModelLinkWidget::widget(['model' => $mainDomain]) : ['casino/view', 'slug' => $model->slug];
?>

<div class="casino-item">
    <div class="logo">
        <?= Html::a(Html::img($favicon,['class' => 'casino-img']), ['casino/view', 'slug' => $model->slug],['class' =>  "desktop"]) ?>
        <?= Html::a(Html::img($sqr,['class' => 'casino-img ']), ['casino/view', 'slug' => $model->slug], ['class' => "mobile"]) ?>
        <div class="logo-rate"><span><?= number_format($model->rating, 1, '.', '') ?></span></div>
    </div>
    <div class="casino-info">
        <h2 class="casino-title"><?= Html::a($model->name, ['casino/view', 'slug' => $model->slug], ['class' => 'casino-title']) ?></h2>
        <ul class="casino-icons">
            <?php if ($model->info->is_mobile_friendly) { ?>
            <li class="casino-icons-item"><?= Html::img('@web/img/icons/mobile2.svg') ?></li>
            <?php } ?>
            <?php if ($model->info->best_support) { ?>
            <li class="casino-icons-item"><?= Html::img('@web/img/icons/mini-support.svg') ?></li>
            <?php } ?>
            <?php if ($model->cashback) { ?>
            <li class="casino-icons-item"><?= Html::img('@web/img/icons/mini-cashback.svg') ?></li>
            <?php } ?>

			<?php
			$has_welcome_deposite = false;
			foreach ($model->bonuses as $bonus) {
				if ($bonus->offerTypes != null && !empty($bonus->offerTypes)){
				    foreach ($bonus->offerTypes as $offerType){
				        if($offerType->name == 'Deposit')
					        $has_welcome_deposite = true;
                    }
                }
			}
			?>
            <?php if ($has_welcome_deposite) { ?>
            <li class="casino-icons-item"><?= Html::img('@web/img/icons/mini-bonus.svg') ?></li>
            <?php } ?>

            <?php
            $has_bitcoin = false;
			foreach ($model->payments as $payment) {
				if ($payment->name == 'Bitcoin')
					$has_bitcoin = true;
			}
            ?>
			<?php if ($has_bitcoin) { ?>
            <li class="casino-icons-item"><?= Html::img('@web/img/icons/mini-bit.svg') ?></li>
            <?php } ?>
        </ul>
        <p class="casino-text"><?= Html::a( \yii\helpers\StringHelper::truncate($model->info['description'], 166) , ['casino/view', 'slug' => $model->slug]) ?></p>
    </div>
    <div class="casino-links">
        <?= Html::a(Yii::t('app','more'), ['casino/view', 'slug' => $model->slug], ['class' => 'casino-link casino-link--blue']) ?>
        <?= Html::a(Yii::t('app','play'), $playUrl, ['class' => 'casino-link']) ?>
    </div>
</div>
