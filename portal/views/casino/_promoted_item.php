<?php

use common\modules\away\widgets\ModelLinkWidget;
use yii\helpers\Url;

/* @var $model common\models\Site */
$sqr = $model->getSiteImages()->type('sqr')->one() ? $model->getSiteImages()->type('sqr')->one()->src : Url::to(['img/casino/malina-casino.png']);
$rect = $model->getSiteImages()->type('rect')->one() ? $model->getSiteImages()->type('rect')->one()->src : Url::to(['img/casino/malina-casino.png']);
//UNUSED// $screen = $model->getSiteImages()->type('preview')->one()->src ?? Url::to(['img/casino/malina-casino.png']);

$mainDomain = $model->getDomains()->typeMain()->one();

$playUrl = $mainDomain ? ModelLinkWidget::widget(['model' => $mainDomain]) : Url::to(['casino/view', 'slug' => $model->slug]);
?>
<a href="<?=$playUrl?>" class="croupierica-choice-item">
    <div class="croupierica-choice-logo">
        <img src="<?=$rect?>">
    </div>
    <div class="croupierica-choice-name"><?=$model->name?></div>
    <div class="croupierica-choice-btn"><?=Yii::t('app','play')?></div>
</a>
