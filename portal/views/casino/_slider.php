<?php
use yii\widgets\ListView;
use common\models\Site;
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 23.08.2018
 * Time: 16:53:11
 */
?>


<div class="container">
    <div class="slider slick-slider">
        <?= ListView::widget([
			'dataProvider' => new \yii\data\ArrayDataProvider([
				'allModels' => Site::find()->with(['customLists' => function(\common\models\queries\CustomListQuery $query) {return $query->promoted();}])->limit(5)->all(),
			]),
			'itemView' => '/casino/_slider_item',
			'emptyText' => "",
			'summary' => '',
			'layout' => '{items}',
			'itemOptions' => ['class'=>'slider-item'],
			'options' => ['id'=>'the-best-slider','class'=>'the-best-slider']
		]);
		?>
    </div>

</div>
