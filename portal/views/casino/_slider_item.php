<?php

use yii\bootstrap\Html;
use yii\helpers\Url;

/* @var $model common\models\Site */
$favicon = $model->getSiteImages()->type('rect')->one()->src ?? Url::to(['img/casino/malina-casino.png']);
//UNUSED// $screen = $model->getSiteImages()->type('preview')->one()->src ?? Url::to(['img/casino/malina-casino.png']);
?>
<a href="<?= \yii\helpers\Url::to(['/casino/view', 'slug' => $model->slug]); ?>">
    <p class="slider-title"><?= $model->name ?></p>
    <div class="slider-center">
        <div class="slider-image">
            <img src="<?= $favicon ?>">
        </div>
    </div>
</a>
<?= Html::a(Yii::t('app', 'more'), ['casino/view', 'slug' => $model->slug], ['class' => 'slider-link']) ?>
