<?php
use yii\helpers\Url;
$custom_list = Yii::$app->request->get('site') && isset(Yii::$app->request->get('site')['custom_list']) ? Yii::$app->request->get('site')['custom_list'] : null
?>

<div class="left-column">
    <div class="cashback-menu-wrap">
        <div class="cashback-menu">
            <a href="<?=Url::to(['casino/'])?>" class="<?=Url::isRelative('casino/index') && $custom_list == null ? 'active' : ''?> cashback-all cashback-link">
                <?=Yii::t('app', 'All');?>
            </a>
            <a href="<?=Url::to(['casino/index','site[custom_list]' => Yii::$app->params['portal']->get('top_10_custom_list.data')])?>"
               class="<?=$custom_list == Yii::$app->params['portal']->get('top_10_custom_list.data') ? 'active' : ''?> cashback-star cashback-link">
                <?=Yii::t('app', 'top 10');?>
            </a>
            <a href="<?=Url::to(['casino/index','site[custom_list]'=>Yii::$app->params['portal']->get('top_highrollers_custom_list.data')])?>"
               class="<?=$custom_list == Yii::$app->params['portal']->get('top_highrollers_custom_list.data') ? 'active' : ''?> cashback-high cashback-link">
                <?=Yii::t('app', 'top highrollers');?>
            </a>
            <a href="<?=Url::to(['casino/index','site[custom_list]'=>Yii::$app->params['portal']->get('mobile_casinos_custom_list.data')])?>"
               class="<?=$custom_list == Yii::$app->params['portal']->get('mobile_casinos_custom_list.data') ? 'active' : ''?> cashback-mobile cashback-link">
                <?=Yii::t('app', 'mobile casinos');?>
            </a>
        </div>
    </div>
</div>