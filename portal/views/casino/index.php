<?php

use yii\helpers\Url;
use yii\widgets\ListView;

/**
 * @var $this yii\web\View
 * @var $searchModel frontend\models\search\SiteSearch
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $pages \yii\data\Pagination
 * @var $gameProviderProvider yii\data\ActiveDataProvider
 * @var $customListProvider yii\data\ActiveDataProvider
 * @var $licensorProvider yii\data\ActiveDataProvider
 */
$this->title = Yii::$app->params['portal']->get('page_attribute_casinos_title.data.en');
$this->registerMetaTag(['name'=>'title','content' => Yii::$app->params['portal']->get('page_attribute_casinos_title.data.en')]);
$this->registerMetaTag(['name'=>'description','content' => Yii::$app->params['portal']->get('page_attribute_casinos_description.data.en')]);
?>

<div class="container">
    <div class="cashback-wrap casino-bg">
        <div class="cashback-info">
            <?=$this->render('/search/_search')?>
            <h1 class="cashback-title desktop"><?=Yii::t('app','Casino category')?></h1>
            <h1 class="cashback-title mobile"><?=Yii::t('app','Casino all')?></h1>
            <p class="cashback-info-text"><?=Yii::t('app','At Croupierica, we have gathered and reviewed some of the best (and worst) casinos in the game, so our members and guests were always able to decide whether or not particular websites worth their attention and money. Here you will find an extensive amount of information on everything online casinos have to offer: banking methods, software providers, available languages, restricted countries, etc. Feel free to look through our materials in your search for a perfect place to gamble and have fun!')?></p>

        </div>
    </div>

    <div class="two-columns">
        <div class="row">
            <?=$this->render('_tabs')?>
            <div class="right-column">
                <div class="croupierica-choice-top">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="left-column">
                <div class="left-column-content">
                    <div class=" casino-list">
                        <?= ListView::widget([
									'dataProvider' => $dataProvider,
									'itemView' => '/casino/_item',
									'emptyText' => "",
									'summary' => '',
									'layout' => '{items}'
								]);
								?>
                    </div>
                </div>
            </div>
            <div class="right-column">
                <?php echo $this->render('/custom-list/_promoted')?>

                <div class="sidebar-menu-wrap">
                    <div class="sidebar-menu-title active"><?=Yii::t('app','All filters')?></div>
                    <div class="sidebar-menu">
                        <div class="sidebar-category-title active"><?= Yii::t('app', 'Online casinos') ?></div>
                        <ul class="sidebar-menu-inner">
                            <?= ListView::widget([
										'dataProvider' => $customListProvider,
										'itemView' => '/custom-list/_filter_item',
										'emptyText' => "",
										'summary' => '',
									]); ?>
                        </ul>
                    </div>
                    <div class="sidebar-menu">
                        <div class="sidebar-category-title active"><?= Yii::t('app', 'Providers') ?></div>
                        <ul class="sidebar-menu-inner">
                            <?= ListView::widget([
										'dataProvider' => $gameProviderProvider,
										'itemView' => '/game-provider-site/_filter_item',
										'emptyText' => "",
										'summary' => '',
									]);
									?>
                        </ul>
                    </div>
                    <div class="sidebar-menu">
                        <div class="sidebar-category-title active"><?= Yii::t('app', 'Licensed') ?></div>
                        <ul class="sidebar-menu-inner">
                            <?= ListView::widget([
										'dataProvider' => $licensorProvider,
										'itemView' => '/site-licensor/_filter_item',
										'emptyText' => "",
										'summary' => '',
									]);
									?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?=\portal\widgets\LinkPager::widget(['pagination'=>$pages])?>
