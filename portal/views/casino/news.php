<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 22.04.2019
 * Time: 14:51:42
 */
?>

<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\DetailView;
use yii\widgets\ListView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\Site */

$this->title = $model->info->page_title;

$this->registerMetaTag(['name'=>'description','content'=>$model->info->page_description]);
$favicon = $model->getSiteImages()->type('rect')->one() ? $model->getSiteImages()->type('rect')->one()->src : \yii\helpers\Url::to(['img/casino/malina-casino.png']);
$sqr = $model->getSiteImages()->type('sqr')->one() ? $model->getSiteImages()->type('sqr')->one()->src : \yii\helpers\Url::to(['img/casino/malina-casino.png']);

?>

<div class="info">
    <div class="container">
        <div class="info-wrapper info-wrapper--child">
            <div class="info-top info-top--child">
                <div class="logo">
                    <img src="<?= $favicon ?>" class="info-img">
                    <div class="logo-rate logo-rate--large">
                        <div class="casino-raiting-inner">
                            <span><?= number_format($model->rating, 1, '.', '') ?></span>
                        </div>
                    </div>
                </div>
                <div class="info-title">
                    <span class="info-title-pre"><?=Yii::t('app','All news for')?></span> <br/> <span class="info-title-name"><?= $model->name ?></span>
                </div>
                <a href="<?= \yii\helpers\Url::to(['casino/view', 'slug' => $model->slug]) ?>" class="info-btn--back"><?=Yii::t('app','Back to casino')?></a>
            </div>
        </div>
    </div>
</div>

<div class="news-about">
    <div class="container">
        <?=$this->render('/blog/_latest_news')?>
    </div>
</div>
