<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 22.04.2019
 * Time: 14:51:42
 */
?>

<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\DetailView;
use yii\widgets\ListView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\Site */

$this->title = $model->info->page_title;

$this->registerMetaTag(['name'=>'description','content'=>$model->info->page_description]);
$favicon = $model->getSiteImages()->type('rect')->one() ? $model->getSiteImages()->type('rect')->one()->src : \yii\helpers\Url::to(['img/casino/malina-casino.png']);
$sqr = $model->getSiteImages()->type('sqr')->one() ? $model->getSiteImages()->type('sqr')->one()->src : \yii\helpers\Url::to(['img/casino/malina-casino.png']);

?>

<div class="info info--child">
    <div class="container">
        <div class="info-wrapper info-wrapper--child">
            <div class="info-top info-top--child">
                <div class="logo">
                    <img src="<?= $favicon ?>" class="info-img">
                    <div class="logo-rate logo-rate--large">
                        <div class="casino-raiting-inner">
                            <span><?= number_format($model->rating, 1, '.', '') ?></span>
                        </div>
                    </div>
                </div>
                <div class="info-title">
                    <span class="info-title-pre"><?=Yii::t('app','All user reviews for')?></span> <br/> <span class="info-title-name"><?= $model->name ?></span>
                </div>
                <a href="<?= \yii\helpers\Url::to(['casino/view', 'slug' => $model->slug]) ?>" class="info-btn--back"><?=Yii::t('app','Back to casino')?></a>
            </div>
        </div>
    </div>
</div>
<div class="user-reviews">
    <div class="container">
        <div class="myreview-list">
            <div class="review review-start">
                <span class="review-start-phrase"><?=Yii::t('app','Write your opinion about')?></span>
                <span class="review-start-phrase review-start-phrase--big"><?=Yii::t('app','Casino')?></span>
                <button class="review-start-btn"><?=Yii::t('app','Write review')?></button>
            </div>
            <?php $lastUpdatedSiteProvider = new \yii\data\ArrayDataProvider([
                'allModels' => \common\models\SiteInfo::find()->latest()->all(),
                'pagination' => [
                        'pageSize' => 8
                ]
            ]); ?>
            <?= ListView::widget([
                'dataProvider' => $lastUpdatedSiteProvider,
                'itemView' => '/review/_item',
                'emptyText' => "",
                'summary' => '',
                'options' => ['tag' => false],
                'itemOptions' => ['class' => 'review']
            ]);
            ?></div>
    </div>
</div>

<?php $this->registerJs("function down(){
var y=(440 - $('.list-view').height());
$('.review-search-down').on('click', function() {
if (y<0) {
y = y + 50;
$('.list-view').animate({'bottom':y},100);
}
if (y>=0) {
return false;
}
})
};

$('.review-search-down').on('click', function() {
document.getElementById('d').scrollTop += 100;
});
", \yii\web\View::POS_END) ?>


<?php $this->registerJsFile('js/jquery-ui.min.js',['depends' => \portal\assets\AppAsset::className()])?>
<?php $this->registerJsFile('js/touch.js',['depends' => \portal\assets\AppAsset::className()])?>
<?php $this->registerJsFile('js/baron.js',['depends' => \portal\assets\AppAsset::className()])?>
<?php $this->registerJs("$('#widget').draggable();") ?>
<?php $this->registerJs("$( function() {
    var handle = $( '#custom-handle' );
    $('#slider').slider({create: function() {handle.text($(this).slider('value'));},
      slide: function(event, ui){handle.text( ui.value );
          document.getElementById('review-range').value = ''+ ui.value +'';
      }
    });
  });
", \yii\web\View::POS_END) ?>
