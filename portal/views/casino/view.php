<?php

use common\modules\away\widgets\ModelLinkWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\Site */

$this->title = $model->translate('title');

$this->registerMetaTag(['name'=>'title','content'=>$model->translate('meta_title')]);
$this->registerMetaTag(['name'=>'description','content'=>$model->translate('meta_description')]);
$favicon = $model->getSiteImages()->type('rect')->one() ? $model->getSiteImages()->type('rect')->one()->src : \yii\helpers\Url::to(['img/casino/malina-casino.png']);
$sqr = $model->getSiteImages()->type('sqr')->one() ? $model->getSiteImages()->type('sqr')->one()->src : \yii\helpers\Url::to(['img/casino/malina-casino.png']);
$mainDomain = $model->getDomains()->typeMain()->one();

$playUrl = $mainDomain ? ModelLinkWidget::widget(['model' => $mainDomain]) : ['casino/view', 'slug' => $model->slug];
?>

<div class="info">
    <div class="container">
        <div class="info-wrapper">
            <div class="info-top">
                <div class="logo">
                    <img src="<?= $favicon ?>" class="info-img">
                    <div class="logo-rate logo-rate--large">
                        <div class="casino-raiting-inner">
                            <span><?= number_format($model->rating, 1, '.', '') ?></span>
                        </div>
                    </div>
                </div>
                <div class="info-title">
                    <h3><?= $model->name ?></h3>
                </div>
                <a href="#casino-about" class="info-btn"><?=Yii::t('app','');?></a>
            </div>
            <div>
                <p class="info-intro"><?=$model->translate('description')?></p>
                <div id="casino-about" class="info-text">
                    <?=\yii\helpers\Markdown::process($model->info->review,'gfm')?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$offers = [];
$bonus = $model->getBonuses()->promoted()->limit(1)->one() ?: $model->getBonuses()->limit(1)->one();
if ($bonus){
    foreach ($bonus->offerTypeBonuses as $offerTypeBonus) {
        $offers[] = '<span class="colorish">'.$offerTypeBonus->offer_text . '</span><span> </span><span>' . $offerTypeBonus->offerType->name . '</span>';
    }
}
if ($offers) {
    ?>
<div class="offer">
    <div class="container offer-wrapper">
        <div class="offer-phrase">
            <div>
                <?= join($offers, '<span> and </span>') ?>
            </div>
        </div>
        <?= \yii\bootstrap\Html::a('Get', $playUrl, ['class' => 'offer-link']) ?>
    </div>
</div>
<?php } ?>

<div class="overview">
    <div class="container">
        <div class="overview-list">
            <div class="card">
                <h3 class="card-title"><?=Yii::t('app','OVERVIEW')?></h3>
                <?php if ($mainDomain = $model->getDomains()->typeMain()->one()) { ?>
                <b class="card-subtitle"><?=Yii::t('app','Website')?></b>
                <p>
                     <?= Html::a($mainDomain->domain, \common\modules\away\widgets\ModelLinkWidget::widget(['model' => $mainDomain])) ?>
                </p>
                <?php } ?>

                <?php if ($model->siteGames) { ?>
                <b class="card-subtitle"><?=Yii::t('app','All Games')?></b>
                <p>
                     <?= count($model->siteGames) ?>
                </p>
                <?php } ?>

                <?php if ($model->gameCategories) { ?>
                <b class="card-subtitle"><?=Yii::t('app','Type of Games')?></b>
                <p>
                     <?= join(', ', ArrayHelper::getColumn($model->gameCategories, 'name')) ?>
                </p>
                <?php } ?>

                <?php if ($model->gameProviders) { ?>
                 <b class="card-subtitle"><?=Yii::t('app','Software')?></b>
                <p>
                   <?= join(', ', ArrayHelper::getColumn($model->gameProviders, 'name')) ?>
                </p>
                <?php } ?>
                <?php if ($model->licenses) { ?>
                <b class="card-subtitle"><?=Yii::t('app','Licence')?></b>
                <p>
                     <?= join(', ', ArrayHelper::getColumn($model->licenses, 'name')) ?>
                </p>
                <?php } ?>
                <?php if ($model->siteGames) { ?>
                <b class="card-subtitle"><?=Yii::t('app','Support')?></b>
                <p>
                     <?= $model->info->support_types ?>
                </p>
                <?php } ?>
                <?php if ($model->currencies) { ?>
                <b class="card-subtitle"><?=Yii::t('app','Currencies')?></b>
                <p>
                     <?= strtoupper(join(', ', ArrayHelper::getColumn($model->currencies, 'name'))) ?>
                </p>
                <?php } ?>

                <?php if ($model->locales) { ?>
                <b class="card-subtitle"><?=Yii::t('app','Languages')?></b>
                <p>
                     <?= join(', ', ArrayHelper::getColumn($model->locales, 'name')) ?>
                </p>
                <?php } ?>

            </div>
            <?php
        $siteDepositPayments = $model->getSitePayments()
            ->deposit()
            ->joinWith(['payment'])->all();
        $siteWithdrawalPayments = $model->getSitePayments()
            ->withdrawal()
            ->joinWith(['payment'])->all();
        ?>
            <div class="card">
                <?php if ($siteDepositPayments
                    || $siteWithdrawalPayments
                    || $model->info->min_deposit
                    || $model->info->min_withdraw
                    || $model->info->max_withdraw
                    || $model->info->payout) { ?>
                <h3 class="card-title"><?=Yii::t('app','DEPOSIT / WITHDRAWAL')?></h3>
                <?php if ($siteDepositPayments) { ?>
                <b class="card-subtitle"><?=Yii::t('app','Deposit Methods')?></b>
                <p>
                    
                    <?= join(', ', ArrayHelper::getColumn($siteDepositPayments, 'payment.name')) ?>
                </p>
                <?php } ?>

                <?php if ($siteWithdrawalPayments) { ?>
                <b class="card-subtitle"><?=Yii::t('app','Withdrawal Methods')?></b>
                <p>
                    
                    <?= join(', ', ArrayHelper::getColumn($siteWithdrawalPayments, 'payment.name')) ?>
                </p>
                <?php } ?>
                <?php if ($model->info->min_deposit) { ?>
                <b class="card-subtitle"><?=Yii::t('app','Min. Deposit')?></b>
                <p>
                     <?= $model->info->min_deposit ?>
                </p>
                <?php } ?>
                <?php if ($model->info->min_withdraw) { ?>
                <b class="card-subtitle"><?=Yii::t('app','Min. Withdrawal')?></b>
                <p>
                     <?= $model->info->min_withdraw ?>
                </p>
                <?php } ?>
                <?php if ($model->info->max_withdraw) { ?>
                <b class="card-subtitle"><?=Yii::t('app','Max Withdrawal')?></b>
                <p>
                     <?= $model->info->max_withdraw ?>
                </p>
                <?php } ?>
                <?php if ($model->info->payout) { ?>
                <b class="card-subtitle"><?=Yii::t('app','Payout')?></b>
                <p>
                     <?= $model->info->payout ?>
                </p>
                <?php } ?>
                <?php } ?>
            </div>
            <div class="card">
                <h3 class="card-title"><?=Yii::t('app','Additional')?></h3>
                <?php if ($model->info->online_since) { ?>
                <b class="card-subtitle"><?=Yii::t('app','Online')?></b>
                <p> Since <?= $model->info->online_since ?></p>
                <?php } ?>
                <?php if ($model->info->owner) { ?>
                <b class="card-subtitle"><?=Yii::t('app','Owner')?></b>
                <p> <?= $model->info->owner ?></p>
                <?php } ?>
                <?php if ($model->info->email) { ?>
                <b class="card-subtitle"><?=Yii::t('app','Email')?></b>
                <p>
                     <a href="mailto:<?= $model->info->email ?>"><?= $model->info->email ?></a>
                </p>
                <?php } ?>
                <?php if ($model->relations) { ?>
                <b class="card-subtitle"><?=Yii::t('app','Sister Casinos')?></b>
                <p>
                     <?= join(', ', ArrayHelper::getColumn($model->relations, 'name')) ?>
                </p>
                <?php } ?>
                <?php if ($model->getSiteGeos()->allowed(false)->count()) { ?>
                <b class="card-subtitle"><?=Yii::t('app','Restricted Countries')?></b>
                <p>
                    <?= join(', ', ArrayHelper::getColumn($model->getSiteGeos()->joinWith(['geo'])->allowed(false)->all(), 'geo.name')) ?>
                </p>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?php $actualBonusesProvider = new \yii\data\ActiveDataProvider([
	'query' => $model->getBonuses()->limit(3),
	'pagination' => false,
]); ?>
<?php if ($actualBonusesProvider->getCount()) { ?>
<div class="container casino-list available-bonuses">
    <h2 class="casino-note-title casino-view-title"><a href="<?= \yii\helpers\Url::to(['bonuses', 'slug' => $model->slug]) ?>"><?=Yii::t('app','Available bonuses for {name}',['name'=> $model->name ])?></a></h2>
    <?= ListView::widget([
        'dataProvider' => $actualBonusesProvider,
        'itemView' => '../bonuses/_casino_item',
        'viewParams' => [
            'site' => $model,
            'favicon' => $favicon
        ],
        'emptyText' => "",
        'summary' => '',

    ]);
    ?>
</div>
<?php } ?>
<?php $blogPostProvider = new \yii\data\ActiveDataProvider([
        'query'=> $model->getBlogPosts()->innerJoinWith(['tags'  => function($query) { return $query->where(['name' => 'news']);}])->orderBy(['updated_date_time' => SORT_DESC])->limit(3)
])
?>
<?php if($blogPostProvider->getCount() > 0){ ?>
<div class="news-about">
    <div class="container">
        <h2 class="news-about-title casino-view-title">
            <a href="<?= \yii\helpers\Url::to(['news', 'slug' => $model->slug]) ?>">
                <?=Yii::t('app','News about {name}',['name'=> $model->name ])?>
            </a>
        </h2>
        <?=$this->render('/blog/_latest_news',['blogPostProvider'=>$blogPostProvider])?>

    </div>
</div>
<?php } ?>
<!--
<div class="user-reviews">
    <div class="container">
       <h2 class="casino-view-title"><a href="<?php //echo  \yii\helpers\Url::to(['review', 'slug' => $model->slug]) ?>"CASINO USER REVIEWS</a></h2>
<?php //$lastUpdatedSiteProvider = new \yii\data\ArrayDataProvider([
//            'allModels' => \common\models\SiteInfo::find()->latest()->limit(isset($amount) ? $amount : 3)->all(),
//        ]); ?>
                <?php //echo ListView::widget([
//            'dataProvider' => $lastUpdatedSiteProvider,
//            'itemView' => '/review/_item',
//            'emptyText' => "",
//            'summary' => '',
//            'options' => ['class'=>'myreview-list'],
//            'itemOptions'=>['class'=>'review']
//        ]);
        ?>
    </div>
</div>
-->
<?php if(Yii::$app->request->get('target') == 'more') $this->registerJs("$('.info-btn')[0].click();");?>
<?php $this->registerJs("$('.info-btn').on('click',
function(){
$(this).toggleClass('info-btn--active');
});
$('.overview-list').slick({
        infinite:false,
        slidesToShow:3,
        slidesToScroll:1,
        arrows:false,
        responsive: [{
            breakpoint: 1120,
            settings: {
                infinite:true,
                slidesToShow: 1,
                dots:true,
                adaptiveHeight:true,
            }
        }]
})", \yii\web\View::POS_END) ?>
<div class="slides slides--blue">
    <?=$this->render('_slider')?>
</div>
<?php $this->registerJsFile('js/baron.js',['depends' => \portal\assets\AppAsset::className()])?>
