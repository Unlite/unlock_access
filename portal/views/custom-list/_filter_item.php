<?php

/* @var $model common\models\CustomList */
$custom_list = Yii::$app->request->get('site') && isset(Yii::$app->request->get('site')['custom_list']) ? Yii::$app->request->get('site')['custom_list'] : null

?>
<li>
    <a class="<?=$custom_list == $model->id ? 'active' : ''?>" href="<?= \yii\helpers\Url::to(['/casino', 'site[custom_list]' => $model->id]) ?>"><?= $model->name ?></a>
</li>
