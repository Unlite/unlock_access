<?php use common\models\Site;?>
<div class="croupierica-choice-wrap">
    <div class="croupierica-choice-title"><?=Yii::t('app','Croupierica choice')?></div>
    <?php echo \yii\widgets\ListView::widget([
		'dataProvider' => new \yii\data\ArrayDataProvider([
			'allModels' => Site::find()->with(['customLists' => function(\common\models\queries\CustomListQuery $query) {return $query->promoted();}])->limit(5)->all(),
        ]),
		'itemView' => '/casino/_promoted_item',
		'emptyText' => "",
		'summary' => '',
    ])?>
</div>