<?php

/* @var $model common\models\GameCategory */
$category = Yii::$app->request->get('game') && isset(Yii::$app->request->get('game')['category']) ? Yii::$app->request->get('game')['category'] : null

?>
<li>
    <a class="<?=$category == $model->id ? 'active' : ''?>" href="<?= \yii\helpers\Url::to(['', 'game[category]' => $model->id]) ?>"><?= $model->name ?></a>
</li>
