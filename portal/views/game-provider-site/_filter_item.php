<?php

/* @var $model common\models\GameProvider */
$game_provider = Yii::$app->request->get('site') && isset(Yii::$app->request->get('site')['game_provider']) ? Yii::$app->request->get('site')['game_provider'] : null
?>
<li>
    <a class="<?=$game_provider == $model->id ? 'active' : ''?>" href="<?= \yii\helpers\Url::to(['', 'site[game_provider]' => $model->id]) ?>"><?= $model->name ?></a>
</li>
