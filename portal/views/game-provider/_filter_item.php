<?php
/* @var $model common\models\GameProvider */
$provider = Yii::$app->request->get('game') && isset(Yii::$app->request->get('game')['provider']) ? Yii::$app->request->get('game')['provider'] : null
?>
<li>
    <a class="<?=$provider == $model->id ? 'active' : ''?>" href="<?= \yii\helpers\Url::to(['', 'game[provider]' => $model->id]) ?>"><?= $model->name ?></a>
</li>
