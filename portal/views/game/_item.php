<?php

/* @var $model common\models\Game */
?>
<a href="<?= \yii\helpers\Url::to(['game/view', 'slug' => $model->slug]) ?>">
    <div class="casino-logo">
        <img src="<?= $model->img_src ?: \yii\helpers\Url::to(['img/casino/10bet.png']) ?>" alt="">
    </div>
    <div class="casino-logo-name"><?= $model->name ?></div>
</a>
