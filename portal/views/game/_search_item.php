<?php
/* @var $model common\models\Game */
?>
    <div class="slots-item-inner decoration">
        <h3><?=$model->name?> <span><?=Yii::t('app','slot')?></span></h3>
        <div class="the-best-logo">
            <a href="<?= \yii\helpers\Url::to(['game/view', 'slug' => $model->slug]) ?>">
                <img src="<?=$model->img_src ?: \yii\helpers\Url::to(['img/casino/10bet.png'])?>">
            </a>
        </div>
        <div class="btn-wrap">
            <a href="<?= \yii\helpers\Url::to(['game/view', 'slug' => $model->slug]) ?>"
               class="more-btn">play</a>
        </div>
    </div>
