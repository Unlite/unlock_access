<?php
use yii\widgets\ListView;
use common\models\Game;
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 23.08.2018
 * Time: 16:53:11
 */
?>

<section class="the-best-slider-wrap">
	<div class="container">
		<?= ListView::widget([
			'dataProvider' => new \yii\data\ArrayDataProvider([
				'allModels' => Game::find()->with(['gameCategories' => function(\common\models\queries\GameCategoryQuery $query) {return $query->slider();}])->limit(5)->all(),
			]),
			'itemView' => '_slider_item',
			'emptyText' => "",
			'summary' => '',
			'layout' => '{items}',
			'itemOptions' => ['class'=>'item decoration'],
			'options' => ['id'=>'the-best-slider','class'=>'the-best-slider owl-carousel owl-theme']
		]);
		?>
	</div>
</section>
