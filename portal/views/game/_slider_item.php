<?php

use yii\bootstrap\Html;
use yii\helpers\Url;

/* @var $model common\models\Game */
?>

    <h3><?= $model->name ?> <span><?=Yii::t('app','Slot')?></span></h3>
    <div class="the-best-logo">
        <a href="<?=Url::to(['game/view', 'slug' => $model->slug])?>">
            <img src="<?= $model->img_src ?>">
        </a>
    </div>
    <div class="btn-wrap">
        <?= Html::a(Yii::t('app', 'play'), ['game/view', 'slug' => $model->slug], ['class' => 'more-btn']) ?>
    </div>