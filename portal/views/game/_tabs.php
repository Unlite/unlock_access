<?php
use yii\helpers\Url;
$category = Yii::$app->request->get('game') && isset(Yii::$app->request->get('game')['category']) ? Yii::$app->request->get('game')['category'] : null
?>

<div class="left-column">
    <div class="cashback-menu-wrap">
        <div class="cashback-menu">
            <a href="<?=Url::to(['game/index'])?>"  class="<?=Url::isRelative('game/index') && $category == null ? 'active' : ''?> cashback-all cashback-link">
                <span><?=Yii::t('app','All');?></span>
            </a>
            <a href="<?=Url::to(['/game/index','game[category]' => Yii::$app->params['portal']->get('games_new_category.data')])?>"
               class="<?=$category == Yii::$app->params['portal']->get('games_new_category.data') ? 'active' : ''?> cashback-new cashback-link">
                <span><?=Yii::t('app','New');?></span>
            </a>
            <a href="<?=Url::to(['/game/index','game[category]'=>Yii::$app->params['portal']->get('games_players_choice_category.data')])?>"
               class="<?=$category == Yii::$app->params['portal']->get('games_players_choice_category.data') ? 'active' : ''?> cashback-star cashback-link">
                <span><?=Yii::t('app','Players\' choice');?></span>
            </a>
            <a href="<?=Url::to(['/game/index','game[category]'=>Yii::$app->params['portal']->get('games_mobile_games_category.data')])?>"
               class="<?=$category == Yii::$app->params['portal']->get('games_mobile_games_category.data') ? 'active' : ''?> cashback-mobile cashback-link">
                <span><?=Yii::t('app','Mobile games');?></span>
            </a>
        </div>
    </div>
</div>