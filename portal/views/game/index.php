<?php

use yii\widgets\ListView;
use yii\helpers\Url;
use common\models\Game;
/**
 * @var $this yii\web\View
 * @var $pages \yii\data\Pagination
 * @var $searchModel frontend\models\search\SiteSearch
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $gameCategoryProvider yii\data\ActiveDataProvider
 * @var $sideBarGameCategoryProvider yii\data\ActiveDataProvider
 * @var $gameProviderProvider yii\data\ActiveDataProvider
 */
$this->title = Yii::$app->params['portal']->get('page_attribute_games_title.data.en');
$this->registerMetaTag(['name'=>'title','content' => Yii::$app->params['portal']->get('page_attribute_games_title.data.en')]);
$this->registerMetaTag(['name'=>'description','content' => Yii::$app->params['portal']->get('page_attribute_games_description.data.en')]);
?>

<div class="container">
    <div class="cashback-wrap game-bg">
        <div class="cashback-info">
           	<?=$this->render('/search/_search')?>
            <h1 class="cashback-title"><?=Yii::t('app','Games category')?></h1>
            <p class="cashback-info-text">
                <?=Yii::t('app','Online casinos are all about the games! But how do you decide what to play if there are thousands of available titles on the market? Don\'t worry, here comes our games section. We have collected and organized every piece of software: from slot machines to video poker. You\'ll be able to directly (and free of charge) play the games to decide whether you should invest your time and money. Those who prefer gambling on the go are welcome in the mobile games section, where you can play through your phone browser without any setbacks.')?>
            </p>
        </div>
     </div>
        <div class="container two-columns">
            <div class="row">
				<?=$this->render('_tabs')?>
                <div class="right-column">
                    <div class="croupierica-choice-top">
                    </div>
                </div>
            </div>
            <div class="row">
               <div class="left-column">
                    <div class="left-column-content">
                            <?php
                                echo ListView::widget([
                                    'dataProvider' => $dataProvider,
                                    'itemView' => '/game/_item',
                                    'emptyText' => "",
                                    'summary' => '',
                                    'itemOptions' => [
                                        'class' => 'casino-logo-item'
                                    ],
                                    'options' => [
                                        'class'=>'casino-logos-list'
                                    ],
									'layout' => '{items}'
                                ]);
                            ?>
                    </div>
                </div>
                <div class="right-column">
					<?php echo $this->render('/custom-list/_promoted')?>
                    <div class="sidebar-menu-wrap">
                        <div class="sidebar-menu-title"><?=Yii::t('app','All filters')?></div>
                        <div class="sidebar-menu">
                            <div class="sidebar-category-title"><?=Yii::t('app','Category')?></div>
                            <ul class="sidebar-menu-inner">
								<?= ListView::widget([
									'dataProvider' => $sideBarGameCategoryProvider,
									'itemView' => '/game-category/_filter_item',
									'emptyText' => "",
									'summary' => '',
								]); ?>
                            </ul>
                        </div>
                        <div class="sidebar-menu">
                            <div class="sidebar-category-title"><?=Yii::t('app','provider')?></div>
                            <ul class="sidebar-menu-inner">
								<?= ListView::widget([
									'dataProvider' => $gameProviderProvider,
									'itemView' => '/game-provider/_filter_item',
									'emptyText' => "",
									'summary' => '',
								]); ?>
                            </ul>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
</div>
<?=\portal\widgets\LinkPager::widget(['pagination'=>$pages])?>

