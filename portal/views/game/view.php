<?php

use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model common\models\Game */
/* @var $highestRankedSite common\models\Site */
/* @var $highestRankedSiteRct \common\models\SiteImage */
$this->title = $model->page_title ?: $model->name;
$this->registerMetaTag(['name'=>'description','content'=>$model->page_description]);
$highestRankedSite = $model->getSites()->highestRating()->one();
if($highestRankedSite){
    $highestRankedSiteUrl = $highestRankedSite->getDomains()->typeMain()->one() ? $highestRankedSite->getDomains()->typeMain()->one()->direct_url : null;
    $highestRankedSiteRct = $highestRankedSite->getSiteImages()->type('rect')->one() ? $highestRankedSite->getSiteImages()->type('rect')->one()->src : null;
}
?>

<div class="container сasino-overview">
</div>
<div class="container profitable-slot">
    <div class="decoration">
        <div class="row">
            <div class="profitable-text col-lg-<?=($highestRankedSite && $highestRankedSiteUrl) ? '8' : '12'?>">
                <h3><?= $this->title ?></h3>
                <p>
                    <?=$model->description?>
                </p>
                <div class="profitable-img">
					<?php if($model->embedded_url) { ?>
                        <iframe frameborder='0' height='<?=$model->embedded_height?>' src='<?=$model->embedded_url?>' width='<?=$model->embedded_width?>'></iframe>
					<?php } else {
						echo \yii\helpers\Html::img($model->img_src);
					} ?>
                </div>
            </div>
            <?php if ($highestRankedSite && $highestRankedSiteUrl) { ?>
                <div class="profitable-cashback col-lg-4">
                    <div class="button-wrap">
                            <a href="<?=$highestRankedSiteUrl?>" class="button"><?=Yii::t('app','PLAY FOR REAL');?></a>
                    </div>
                    <div class="get-cashback">
                        <div>
                            <div class="the-best-item">
                                <div class="get-cashback-title"><?=Yii::t('app','Top casino for this slot:')?></div>
                                <div class="casino-logo-wrap">
                                    <div class="casino-logo"><img src="<?=$highestRankedSiteRct?>"></div>
                                    <div class="casino-raiting">
                                        <div class="casino-raiting-inner">
                                            <span><?= number_format($highestRankedSite->rating, 1, '.', '') ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			<?php } ?>
        </div>
    </div>
</div>




<div class="container casino-list avalaible-bonuses">
	<?php
	$siteIDs = $model->getSites()->select(['id'])->column();
    if ($siteIDs) { ?>
        <h2><?=Yii::t('app','Where to play this slot:')?></h2>
	<?php } ?>
	<?php
	$DataProvider = new \yii\data\ActiveDataProvider([
		'query' => \common\models\Bonuses::find()->bySite($siteIDs),
		'pagination' => [
			'pageSize' => 50,
		],
	]);
	echo ListView::widget([
		'dataProvider' => $DataProvider,
		'itemView' => '../bonuses/_item',
		'emptyText' => "",
		'summary' => '',
	]);
	?>
</div>

<?php
echo ListView::widget([
	'dataProvider' => $DataProvider,
	'itemView' => '../bonuses/_modal',
	'emptyText' => "",
	'summary' => '',
]);
?>

<?=$this->render('/game/_slider')?>
