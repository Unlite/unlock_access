<?php
/**
 * @var $loginForm LoginForm
 * @var $signupForm SignUpForm
 */
use common\models\forms\LoginForm;
use common\models\forms\SignUpForm;
?>


        <div class="form-wrapper">
         <h3 class="form-title">CROUPIERICA</h3>
          <?= isset($loginForm) ? '<h4 class="form-subtitle js-form-tab js-form-tab-log">'.Yii::t('app','LOG IN').'</h4>' : ''?>
          <?= isset($signupForm) ? '<h4 class="form-subtitle js-form-tab js-form-tab--active js-form-tab-reg">'.Yii::t('app','SIGN UP').'</h4>' : ''?>
            <?=isset($loginForm) ? $this->render('_sign_in',['loginForm' => $loginForm]) : ''?>
            <?=isset($signupForm) ? $this->render('_sign_up',['signupForm' => $signupForm]) : ''?>
          </div>

