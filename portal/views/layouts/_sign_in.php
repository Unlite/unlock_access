<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 20.05.2019
 * Time: 01:34:35
 * @var $loginForm \backend\models\forms\LoginForm
 */
 use yii\widgets\ActiveForm;

?>


<?php $form = ActiveForm::begin(['action' => ['/site/ajax-login'],"id"=>"login" ,'enableAjaxValidation' => true,'options' => [
	'class' => 'form  js-form js-form-log'
]]); ?>
<span class="form-name"><?=Yii::t('app','username')?></span>
<?= $form->field($loginForm, 'username',['enableAjaxValidation' => true])->textInput(['class'=>'form-input'])->label(false) ?>
<span class="form-name"><?=Yii::t('app','password')?></span>
<?= $form->field($loginForm, 'password',['enableAjaxValidation' => true])->passwordInput(['class'=>'form-input'])->label(false) ?>

<div class="form-blue"></div>
<p class="form-note"><?=Yii::t('app','By clicking Sign Up, you are indicating that you have read and agree to the Terms of Service and Privacy Policy')?></p>
<?= \yii\bootstrap\Html::submitButton('Log In', ['class' => 'form-submit']) ?>
<?php ActiveForm::end(); ?>

