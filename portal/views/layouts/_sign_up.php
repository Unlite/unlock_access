<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 20.05.2019
 * Time: 01:35:42
 * @var $signupForm \common\models\forms\SignUpForm
 */

?>


<?php $form = \yii\widgets\ActiveForm::begin(['action' => ['/site/ajax-signup'],"id"=>"registration" ,'enableAjaxValidation' => true,'options' => [
	'class' => 'form js-form js-form--active js-form-reg'
]]); ?>
<span class="form-name"><?=Yii::t('app','username')?></span>
<?= $form->field($signupForm, 'username',['enableAjaxValidation' => true])->textInput(['class'=>'form-input'])->label(false) ?>
<span class="form-name"><?=Yii::t('app','password')?></span>
<?php
?>
<?= $form->field($signupForm, 'password',['enableAjaxValidation' => true])->passwordInput(['class'=>'form-input'])->label(false) ?>
<?= $form->field($signupForm,'month')->dropDownList(
	array_combine(range(1,12),range(1,12)),
	['class'=>'form-select','prompt'=>'month'])->label(false);?>

<?= $form->field($signupForm,'day')->dropDownList(
	array_combine(range(1,31),range(1,31)),
	['class'=>'form-select','prompt'=>'day'])->label(false);?>

<?= $form->field($signupForm,'year')->dropDownList(
	array_combine(range(1900,(int) $year = date("Y")),range(1900,(int) $year = date("Y"))),
	['class'=>'form-select','prompt'=>'year'])->label(false);?>
<br>
<span class="form-name"><?=Yii::t('app','email')?></span>
<?= $form->field($signupForm, 'email',['enableAjaxValidation' => true])->input('email',['class'=>'form-input'])->label(false) ?>
<div class="form-blue"></div>
<p class="form-note"><?=Yii::t('app','By clicking Sign Up, you are indicating that you have read and agree to the Terms of Service and Privacy Policy')?></p>
<?= \yii\helpers\Html::submitButton('Sign Up', ['class' => 'form-submit']) ?>
<?php \yii\widgets\ActiveForm::end(); ?>
