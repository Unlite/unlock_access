<?php

/* @var $this \yii\web\View */
/* @var $content string */

use common\widgets\Alert;
use portal\assets\AppAsset;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use portal\assets\CashbackAsset;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700&amp;subset=latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Neucha" rel="stylesheet">
	<?= Html::csrfMetaTags() ?>

    <title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="layout">
    <header class="header">
		<div class="container">
			<div class="toggler" id="toggler">
				<span></span>
			</div>
			<a class="header-title" href="<?=Url::to(['/'])?>">CROUPIERICA</a>
			<div class="header-background " id="header-bg">
					<?= \yii\widgets\Menu::widget([
						'items' => [
							['label' => Yii::t('app', 'Casino'), 'url' => ['casino/index'],'items' => [
                                ['label' => Yii::t('app','Casino')],
                                ['label' => Yii::t('app','All'), 'url' => ['casino/index']],
                                ['label' => Yii::t('app','Top 10'), 'url' => ['casino/index','site[custom_list]'=>Yii::$app->params['portal']->get('top_10_custom_list.data')]],
                                ['label' => Yii::t('app','Top highrollers'), 'url' => ['casino/index','site[custom_list]'=>Yii::$app->params['portal']->get('top_highrollers_custom_list.data')]],
                                ['label' => Yii::t('app','Mobile Casinos'), 'url' => ['casino/index','site[custom_list]'=>Yii::$app->params['portal']->get('mobile_casinos_custom_list.data')]],

                            ],

                                ],
							['label' => Yii::t('app', 'Games'), 'url' => ['game/index'],'items' => [
                                ['label' => Yii::t('app','Games')],
                                ['label' => Yii::t('app','All'), 'url' => ['game/index']],
                                ['label' => Yii::t('app','New'), 'url' => ['game/index','game[category]'=>Yii::$app->params['portal']->get('games_new_category.data')]],
                                ['label' => Yii::t('app','Players choice'), 'url' => ['game/index','game[category]'=>Yii::$app->params['portal']->get('games_players_choice_category.data')]],
                                ['label' => Yii::t('app','Mobile Games'), 'url' => ['game/index','game[category]'=>Yii::$app->params['portal']->get('games_mobile_games_category.data')]],

                            ] ],
							['label' => Yii::t('app', 'Bonuses'), 'url' => ['bonuses/index'],'items' => [
                                ['label' => Yii::t('app','Bonuses')],
                                ['label' => Yii::t('app','All'), 'url' => ['bonuses/index']],
                                ['label' => Yii::t('app','Exclusive'), 'url' => ['bonuses/index','bonuses[offer_type]'=>Yii::$app->params['portal']->get('top_cashback_offer_type.data')]],
                                ['label' => Yii::t('app','Deposit bonus'), 'url' => ['bonuses/index','bonuses[offer_type]'=>Yii::$app->params['portal']->get('free_money_offer_type.data')]],
                                ['label' => Yii::t('app','Free spins'), 'url' => ['bonuses/index','bonuses[offer_type]'=>Yii::$app->params['portal']->get('free_spins_offer_type.data')]],

                            ]],
							['label' => Yii::t('app', 'Tips&Guides'), 'url' => ['/blog/index'],'items' => [
                                ['label' => Yii::t('app','Tips&Guides')],
                                ['label' => Yii::t('app','All'), 'url' => ['/blog']],
                                ['label' => Yii::t('app','Casino news'), 'url' => ['/blog/index','blog[category]' => Yii::$app->params['portal']->get('top_cashback_game_category.data')]],
                                ['label' => Yii::t('app','Guides'), 'url' => ['/blog/index','blog[category]' => Yii::$app->params['portal']->get('instant_details_game_category.data')]],
                                ['label' => Yii::t('app','Promotions'), 'url' => ['/blog/index','blog[category]' => Yii::$app->params['portal']->get('new_cashback_games_category.data')]],

                            ] ],
							['label' => Yii::t('app', 'Cashback'), 'url' => '/cashback','items' => [
                                ['label' => Yii::t('app','Cashback')],
                                ['label' => Yii::t('app','Cabinet'), 'url' => ['/cashback/cabinet']],
                                ['label' => Yii::t('app','Casino'), 'url' => ['/cashback/casino']],

                            ]],
						],
						'options' => [
							'class' => 'header-nav header-middle',
							'id' => 'nav',
						],
						'linkTemplate' => Html::a('{label}','{url}',['class'=>'header-link']),
						'submenuTemplate' => '<ul class="header-category">{items}</ul>'."\n",
						'activeCssClass'=>'active',
						'firstItemCssClass' => 'first-item',
						'itemOptions'=>['class'=>'header-nav-item']
					]);
					?>
			</div>
			<?php if(Yii::$app->user->isGuest) {?>
				<?= \yii\widgets\Menu::widget([
					'items' => [
						['label' => Yii::t('app', 'Log In'),  'options' => ['class' => 'header-end-item js-login-popup-show js-header-log']],
						['label' => Yii::t('app', 'Registration'),  'options' => ['class' => 'header-end-item js-login-popup-show js-header-reg']],
						['label' => '', 'url' => '#','template' => '', 'options' => ['class' => 'header-end-item header-language header-language--usa'] ],
					],
					'options' => [
						'class' => 'header-nav header-end',
					],
					'linkTemplate' => Html::a('{label}','{url}',['class'=>'']),
					'activeCssClass'=>'',
					'itemOptions'=>['class'=>'header-end-item']

				]);
				?>
			<?php } else {?>
				<?= \yii\widgets\Menu::widget([
					'items' => [
						['label' => Yii::t('app', 'reviews'), 'url' => ['/site/review'], 'template' => Html::a('{label}','{url}',['class' => 'header-btn-link header-btn-link--reviews'])],
						['label' => Yii::t('app', 'settings'), 'url' => ['/site/settings'], 'template' => Html::a('{label}','{url}',['class' => 'header-btn-link header-btn-link--settings'])],
						['label' => Yii::t('app', 'exit'), 'url' => ['/logout'], 'template' => Html::a('{label}','{url}',['class' => 'header-btn-link header-btn-link--exit'])],
					],
					'options' => [
						'class' => 'header-menu',
					],
					'activeCssClass'=>'active',
					'itemOptions'=>['class'=>'header-btn']

				]);
				?>
			<?php } ?>

		</div>
	</header>
    <div class="content-layout">
        <?=$content?>
    </div>
    <section class="subscribe">
        <div class="container subscribe-container">
                <p class="subscribe-text"><?=Yii::t('app','Subscribe for Croupierica Newsletter with your e-mail address and you will always be informed about online gambling, new website features, and active promotions.')?></p>
                <form id="subscribe" class="subscribe-form">
                        <input name="email" placeholder="<?=Yii::t('app','Email')?>" required="required" type="email" class="subscribe-form-input">
                    <button type="submit" class="subscribe-form-btn"><?=Yii::t('app','subscribe')?></button>
                </form>
        </div>
    </section>
    <footer class="footer">
		<div class="container">
			<div class="footer-wrapper">
				<div class="footer-about">
					<p class="footer-title">Croupierica</p>
					<p class="footer-text"><?=Yii::t('app','Our website is dedicated to gathering every piece of information there is on online casinos and internet gambling in general. Our team is tirelessly spinning the virtual reels and surfing the web to deliver you honest impressions and the latest news, so you can always find something interesting and useful about your hobby.')?></p>
					<p class="footer-text"><?=Yii::t('app','By using the website, you agree to our terms of use and confirm that you are over the age of 18.')?>	</p></div>
				<div class="footer-about">
					<p class="footer-title"><?=Yii::t('app','Cashback')?></p>
					<p class="footer-text"><?=Yii::t('app','We know how important it is to have another chance after a bitter losing streak, that\'s why we created the cashback system that will allow you to get back some of the funds that you wagered. Make sure to always use links from Croupierica to gain additional advantage and get another go at winning, whether you are a passionate gambler or just playing for fun.')?></p>
				</div>

				<div class="footer-links-wrap">
				    <div class="footer-links">
				        <a href="<?=Url::to(['site/signup'])?>" class="footer-link"><?=Yii::t('app','Create an Account')?></a>
				        <a href="<?=Url::to(['/casino/index'])?>" class="footer-link"><?=Yii::t('app','Casino')?></a>
				        <a href="<?=Url::to(['/game/index'])?>" class="footer-link"><?=Yii::t('app','Games')?></a>
				        <a href="<?=Url::to(['/bonuses/index'])?>" class="footer-link"><?=Yii::t('app','Bonuses')?></a>
				        <a href="<?=Url::to(['/blog/index'])?>" class="footer-link"><?=Yii::t('app','Tips&amp;guides')?></a>
				        <a href="<?=Url::to(['/cashback/'])?>" class="footer-link"><?=Yii::t('app','Cashback')?></a>
				        <a href="<?=Yii::$app->frontendUrlManager->createAbsoluteUrl('/')?>" class="footer-link"><?=Yii::t('app','Vpn')?></a>
				    </div>
				    
				    <div class="footer-links">
				    
				        <a href="<?=Url::to(['site/about'])?>" class="footer-link"><?=Yii::t('app','About')?></a>
				        <a href="<?=Url::to(['site/contact'])?>" class="footer-link"><?=Yii::t('app','Contact')?></a>
				        <a href="<?=Url::to(['site/faq'])?>" class="footer-link"><?=Yii::t('app','Faq')?></a>
				        <a href="<?=Url::to(['site/terms-of-service'])?>" class="footer-link"><?=Yii::t('app','Terms of service (terms of use)')?></a>
				        <a href="<?=Url::to(['site/privacy'])?>" class="footer-link"><?=Yii::t('app','Privacy')?></a>
				        <a href="<?=Url::to(['site/map'])?>" class="footer-link"><?=Yii::t('app','Sitemap')?></a>
				        <div class="footer-icons">
				            <div class="footer-icon footer-icon--eighteen"></div>
				            <div class="footer-icon footer-icon--gamcare"></div>
				            <div class="footer-icon footer-icon--bga"></div>
				        </div>
				    </div>
				</div>
			</div>
		</div>
	</footer>
</div>
<div class="login-popup js-popup">
    <div class="login-popup-overlay js-popup-close"></div>
    <div class="login-popup-content">
        <button class="login-popup-close js-popup-close">×</button>
        <?php if(Yii::$app->user->isGuest) {
                echo $this->render('_new_auth',['loginForm' => new \common\models\forms\LoginForm(),'signupForm' =>new \common\models\forms\SignUpForm()]);
        } ?>
    </div>
    </div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
