<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

?>
<style>.subscribe {background-color: #fff;}</style>
<div class="site-error">
	<div class="container">
		<div class="error-bg error-bg--redirect">
			<div class="error-wrapper error-wrapper--redirect">
				<h1 class="error-title error-title--redirect"><?=Yii::t('app','You will be redirected')?></h1>
				<p class="error-text error-text--redirect">To: </p>
			</div>
		</div>
	</div>
</div>

