<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

?>
<style>.subscribe {background-color: #fff;}</style>
<div class="site-error">
	<div class="container">
		<div class="error-bg error-bg--403">
			<div class="error-wrapper error-wrapper--403">
				<h1 class="error-title error-title--403">ERROR 403</h1>
				<h2 class="error-subtitle error-subtitle--403"><?=Yii::t('app','Forbidden')?></h2>
			</div>
		</div>
	</div>

</div>

