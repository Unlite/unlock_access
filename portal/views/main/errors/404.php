<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

?>
<style>.subscribe {background-color: #fff;}</style>
<div class="site-error">
	<div class="container">
		<div class="error-bg error-bg--404">
			<div class="error-wrapper error-wrapper--404">
				<h1 class="error-title error-title--404">404</h1>
				<h2 class="error-subtitle error-subtitle--404"><?=Yii::t('app','Oops! Page not found')?></h2>
				<p class="error-text error-text--404"><?=Yii::t('app','Perhaps you can try to refresh the page, sometimes it works')?> </p>
				<button class="error-btn"><?=Yii::t('app','Retry')?></button>
			</div>
		</div>
	</div>

</div>
<?php $this->registerJs("$('.error-btn').on('click',
function(){
location.reload();
});", \yii\web\View::POS_END) ?>

