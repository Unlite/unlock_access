<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

?>
<style>.subscribe {background-color: #fff;}</style>
<div class="site-error">
	<div class="container">
		<div class="error-bg error-bg--500">
			<div class="error-wrapper error-wrapper--500">
				<h1 class="error-title error-title--500">ERROR 500</h1>
				<h2 class="error-subtitle error-subtitle--500"><?=Yii::t('app','INTERNAL SERVER ERROR')?></h2>
			</div>
		</div>
	</div>

</div>

