<?php
use yii\bootstrap\Html;
use yii\helpers\Url;

/* @var $model common\models\Site */
$favicon = $model->getSiteImages()->type('rect')->one()->src ?? Url::to(['img/casino/malina-casino.png']);
$sqr= $model->getSiteImages()->type('sqr')->one()->src ?? Url::to(['img/casino/get-cashback-malina.png']);
//UNUSED// $screen = $model->getSiteImages()->type('preview')->one()->src ?? Url::to(['img/casino/malina-casino.png']);
?>

<div class="review-casino">
    <div class="review-casino-img">
        <?= Html::img($favicon,['class' => 'casino-img desktop']) ?>
        <?= Html::img($sqr,['class' => 'casino-img mobile']) ?>
    </div>
    <h2 class="review-casino-title"><?= $model->name ?></h2>
</div>
