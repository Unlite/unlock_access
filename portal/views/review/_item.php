<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 13.08.2018
 * Time: 18:25:44
 */

/* @var $model \common\models\SiteInfo */
$site = $model->site;
if ($site) {
    $favicon = $site->getSiteImages()->type('rect')->one()->src ?? \yii\helpers\Url::to(['img/casino/malina-casino.png']);
    $sqr = $site->getSiteImages()->type('sqr')->one()->src ?? \yii\helpers\Url::to(['img/casino/get-cashback-malina.png']);
    $rect = $site->getSiteImages()->type('rect')->one()->src ?? \yii\helpers\Url::to(['img/casino/malina-casino.png']);
}
?>


    <div class="review-status review-status--approved">
        <?=Yii::t('app','Approved')?>
    </div>
    <div class="review-casino review-casino--edit review-casino--nobg">
        <div class="review-casino-img">
            <?= \yii\helpers\Html::img($sqr, ['class' => 'casino-img']) ?> 
        </div>
        <h2 class="review-casino-title"><?= $site->name ?></h2>
        <button class="review-casino-btn review-casino-btn--edit"></button>
    </div>
    <div class="review-text" id="wrapper">
        <div class="review-scroller" id="scroller">
            <p class="review-text-item review-content desktop" id="content">
                <?= \yii\helpers\StringHelper::basename($model->description) ?>
            </p>
            <p class="review-text-item review-content mobile" id="content">
                <?= \yii\helpers\StringHelper::truncate($model->description, 208) ?>
            </p>
            <a href="<?= \yii\helpers\Url::to(['/casino/view', 'slug' => $site->slug]); ?>" class="review-more mobile"><?=Yii::t('app','Read more')?></a>
            <div class='scroller__bar'></div>
        </div>
    </div>
    <div class="review-rate review-rate--blue">
        <div class="review-rate-number">3.9</div>
        <div class="review-rate-text">Not Bad</div>
    </div>
    <div class="review-like">
        299
    </div>

<?php $this->registerJs("
baron($('.review-text'), {
	scroller: '.review-scroller',
	container: '.review-content',
	bar: '.scroller__bar'
});
", \yii\web\View::POS_READY) ?>