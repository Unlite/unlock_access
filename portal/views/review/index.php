<?php
use yii\widgets\ListView;
/**
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $pages \yii\data\Pagination
 */
?>
<div class="container">
	<h1 class="myreview-title">
		<?=Yii::t('app','My Reviews:')?>
	</h1>
	<div class="myreview-list">
		<div class="review review-start">
			<span class="review-start-phrase"><?=Yii::t('app','Write your opinion about')?></span>
			<span class="review-start-phrase review-start-phrase--big"><?=Yii::t('app','Casino')?></span>
			<button class="review-start-btn"><?=Yii::t('app','Write review')?></button>
		</div>
		<div class="review review-search">
			<?=$this->render('/search/_search_review')?>
			<div class="review-casino-list">
				<?= ListView::widget([
					'dataProvider' => new \yii\data\ActiveDataProvider(['query' => \common\models\Site::find()->limit(10)]),
					'itemView' => '_casino_item',
					'emptyText' => "",
					'summary' => '',
					'layout' => '{items}',
					'options' => ['class' => 'review-list-view', 'id' => 'd'],
					'itemOptions'=>['class'=>'']

				]);
				?>
			</div>
			<button class="review-search-down" id="d">
			</button>

		</div>
		<div class="review review-textarea">
			<form action="" method="post" class="review-textarea-form">
				<div class="review-casino review-casino--nobg">
					<div class="review-casino-img">
						<img class="casino-img" src="/img/casino/get-cashback-malina.png" alt=""> </div>
					<h2 class="review-casino-title">Casino X</h2>
				</div>
				<textarea name="" id="" cols="20" rows="17" class="review-textarea-item" placeholder="<?=Yii::t('app','Text here')?>"></textarea>
				<div class="review-range-wrapper">
					<input hidden type="range" min="0" max="5" step="0.1" id="review-range">
					<div id="slider" class="review-range">
						<div id="custom-handle" class="ui-slider-handle review-range-handle"></div>
					</div>
				</div>
				<button class="review-textarea-btn" type="submit"><?=Yii::t('app','Set')?></button>
			</form>
		</div>

	</div>
	<?php $lastUpdatedSiteProvider = new \yii\data\ArrayDataProvider([
		'allModels' => \common\models\SiteInfo::find()->latest()->limit(isset($amount) ? $amount : 3)->all(),
	]); ?>
	<?= ListView::widget([
		'dataProvider' => $lastUpdatedSiteProvider,
		'itemView' => '/review/_item',
		'emptyText' => "",
		'summary' => '',
		'options' => ['class'=>'myreview-list'],
		'itemOptions'=>['class'=>'review']
	]);
	?>
</div>
<?=\portal\widgets\LinkPager::widget(['pagination'=>$pages])?>

<?php $this->registerJs("function down(){
var y=(440 - $('.list-view').height());
$('.review-search-down').on('click', function() {
if (y<0) {
y = y + 50;
$('.list-view').animate({'bottom':y},100);
}
if (y>=0) {
return false;
}
})
};

$('.review-search-down').on('click', function() {
document.getElementById('d').scrollTop += 100;
});
", \yii\web\View::POS_END) ?>


<?php $this->registerJsFile('js/jquery-ui.min.js',['depends' => \portal\assets\AppAsset::className()])?>
<?php $this->registerJsFile('js/touch.js',['depends' => \portal\assets\AppAsset::className()])?>
<?php $this->registerJsFile('js/baron.js',['depends' => \portal\assets\AppAsset::className()])?>
<?php $this->registerJs("$('#widget').draggable();") ?>
<?php $this->registerJs("$( function() {
    var handle = $( '#custom-handle' );
    $('#slider').slider({create: function() {handle.text($(this).slider('value'));},
      slide: function(event, ui){handle.text( ui.value );
          document.getElementById('review-range').value = ''+ ui.value +'';
      }
    });
  });
", \yii\web\View::POS_END) ?>

