<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 03.09.2018
 * Time: 17:19:34
 */
use yii\widgets\ActiveForm;
use portal\models\forms\SearchForm;
use yii\helpers\Url;

/**
 * @var $searchForm SearchForm
 * @var $this \yii\web\View
 */

?>

<section class="search-form-wrap">
    <div class="container">
        <?php $form = ActiveForm::begin([
			'action' => [Url::current()],
			'method' => 'post',
			'options' => [
                'class' => 'search-form',
            ],
			'fieldConfig' => [
				'template' => "{input}",
				'options' => [
					'tag'=>false
				]
			]
		]); ?>
        <h1 class="search-title">Search</h1>
        <div class="search-line">
            <div class="search-input-wrap">
                <?= $form->field($searchForm, 'q')->textInput(['id'=>'search','type' => 'search', 'class' => 'search-input', 'placeholder' => Yii::t('app', 'Search')])->label(false) ?>
            </div>
            <button type="submit" class="search-btn"></button>
        </div>
        <?php ActiveForm::end() ?>
        <div class="search-category-list">
            <h2 class=" search-category-title mobile"><?=Yii::t('app','Search category')?></h2>
            <a href="<?= Url::to(['search/' . SearchForm::CAT_INDEX . '/' . Yii::$app->request->get('q')]) ?>" class="search-category-item  search-category-item--all"><?=Yii::t('app','All')?></a>
            <a href="<?= Url::to(['search/' . SearchForm::CAT_CASINO . '/' . Yii::$app->request->get('q')]) ?>"  class="search-category-item search-category-item--active search-category-item--casino"><?=Yii::t('app','Casino')?></a>
            <a href="<?= Url::to(['search/' . SearchForm::CAT_GAMES . '/' . Yii::$app->request->get('q')]) ?>"  class="search-category-item  search-category-item--games"><?=Yii::t('app','Games')?></a>
            <a href="<?= Url::to(['search/' . SearchForm::CAT_BONUSES . '/' . Yii::$app->request->get('q')]) ?>"  class="search-category-item search-category-item--bonuses"><?=Yii::t('app','Bonuses')?></a>
            <a href="<?= Url::to(['search/' . SearchForm::CAT_BLOG . '/' . Yii::$app->request->get('q')]) ?>"  class="search-category-item search-category-item--news"><?=Yii::t('app','News')?></a>
            <a href="#"  class="search-category-item search-category-item--reviews"><?=Yii::t('app','Reviews')?></a>
        </div>
    </div>
</section>
