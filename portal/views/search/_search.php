<?php
use portal\models\forms\SearchForm;
use yii\widgets\ActiveForm;

/* @var $search \portal\models\forms\SearchForm */
?>
<div class="cashback-search">
	<?php $form = ActiveForm::begin([
		'action' => ['/search/index/'],
		'method' => 'post',
		'options' => ['class' => 'cashback-search-form'],
        'fieldConfig' => [
            'template' => "{input}",
            'options' => [
                'tag'=>false
            ]
        ]
	]); ?>
    <div class="form-group">
        <label>
			<?= $form->field(new SearchForm(), 'q')->textInput(['type' => 'search', 'class' => 'form-control', 'placeholder' => Yii::t('app', '')])->label(false) ?>
        </label>
         <button class="btn-search" type="submit"></button>

    </div>
	<?php ActiveForm::end() ?>
</div>