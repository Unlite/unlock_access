<?php
use portal\models\forms\SearchForm;
use yii\widgets\ActiveForm;

/* @var $search \portal\models\forms\SearchForm */
?>
<div class="review-search-item">
	<?php $form = ActiveForm::begin([
		'action' => ['/search/index/'],
		'method' => 'post',
		'options' => ['class' => 'review-search-form'],
        'fieldConfig' => [
            'template' => "{input}",
            'options' => [
                'tag'=>false
            ]
        ]
	]); ?>
    <div class="form-group">
			<?= $form->field(new SearchForm(), 'q')->textInput(['type' => 'search', 'class' => 'form-control', 'placeholder' => Yii::t('app', '')])->label(false) ?>
             <button class="btn-search" type="submit"></button>

    </div>
	<?php ActiveForm::end() ?>
</div>