<?php
use portal\models\forms\SearchForm;
use yii\data\Pagination;
use yii\helpers\Url;
use yii\widgets\ListView;

/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 29.08.2018
 * Time: 19:39:40
 * @var $results
 * @var $searchForm \portal\models\forms\SearchForm;
 */
$this->title = Yii::$app->params['portal']->get('page_attribute_search_title.data.en');
$this->registerMetaTag(['name'=>'title','content' => Yii::$app->params['portal']->get('page_attribute_search_title.data.en')]);
$this->registerMetaTag(['name'=>'description','content' => Yii::$app->params['portal']->get('page_attribute_search_description.data.en')]);
?>


<div class="content-layout">
	<?= $this->render('_full_search', ['searchForm' => $searchForm]) ?>
    <?php if($searchForm->validate()){?>
    <div class="search-list">
		<?php if (($searchForm->c == SearchForm::CAT_INDEX || $searchForm->c == SearchForm::CAT_CASINO) && $searchForm->siteDataProvider->getTotalCount() > 0) { ?>
            <div class="search-results-item">
                    <div class="container search-results-header">
                            <a href="<?= Url::to(['search/' . SearchForm::CAT_CASINO . '/' . Yii::$app->request->get('q')]) ?>" class="search-results-title">
								<span class="search-results-name"><?=Yii::t('app','Casino')?></span>
                               <?= Yii::t('app', '<span class="search-results-amount">[{amount} results]</span>', [
									'amount' => $searchForm->siteDataProvider->getTotalCount()
								]) ?>
                            </a>
                    </div>
                <div id="search-casino-results" class="search-results">
					<?php
					if ($searchForm->c == SearchForm::CAT_INDEX) {
						$searchForm->siteDataProvider->pagination->pageSize = 4;
						$searchForm->siteDataProvider->totalCount = 4;
						} else {
							$searchForm->blogPostDataProvider->pagination = false;
                        }
					echo ListView::widget([
						'dataProvider' => $searchForm->siteDataProvider,
						'itemView' => '../casino/_search_item',
						'emptyText' => "",
						'summary' => '',
						'options' => ['class' => 'container casino-list casino-list--search '],
						'itemOptions' => ['class' => 'casino-item casino-item--search']
					]);
					?>
                </div>
            </div>
		<?php } ?>
		<?php if (($searchForm->c == SearchForm::CAT_INDEX || $searchForm->c == SearchForm::CAT_GAMES) && $searchForm->gameDataProvider->getTotalCount() > 0) { ?>
            <div class="search-results-item">
                    <div class="container search-results-header">
                            <a href="<?= Url::to(['search/' . SearchForm::CAT_GAMES . '/' . Yii::$app->request->get('q')]) ?>" class="search-results-title">
                                <span class="search-results-name"><?=Yii::t('app','Casino slots')?></span>
								<?= Yii::t('app', '<span class="search-results-amount">[{amount} results]</span>', [
									'amount' => $searchForm->gameDataProvider->getTotalCount()
								]) ?>
                            </a>
                    </div>
                <div id="search-slots-results" class="search-results">
                    <div class="container">
						<?php
						if ($searchForm->c == SearchForm::CAT_INDEX) {
							$searchForm->gameDataProvider->pagination->pageSize = 6;
							$searchForm->gameDataProvider->totalCount = 6;
						} else {
							$searchForm->blogPostDataProvider->pagination = false;
                        }
						echo ListView::widget([
							'dataProvider' => $searchForm->gameDataProvider,
							'itemView' => '../game/_search_item',
							'emptyText' => "",
							'summary' => '',
							'options' => ['class' => 'slots-list casino-list--search'],
							'itemOptions' => ['class' => 'slots-item casino-item--search']
						]);
						?>
                    </div>
                </div>
            </div>
		<?php } ?>
		<?php if (($searchForm->c == SearchForm::CAT_INDEX || $searchForm->c == SearchForm::CAT_BONUSES) && $searchForm->bonusesDataProvider->getTotalCount() > 0) { ?>
            <div class="search-results-item">

				
                    <div class="container search-results-header">
                            <a href="<?= Url::to(['search/' . SearchForm::CAT_BONUSES . '/' . Yii::$app->request->get('q')]) ?>" class="search-results-title">
                                <span class="search-results-name"><?=Yii::t('app','Casino bonuses')?></span>
								<?= Yii::t('app', '<span class="search-results-amount">[{amount} results]</span>', [
									'amount' => $searchForm->bonusesDataProvider->getTotalCount()
								]) ?>
                            </a>
                    </div>
                <div id="search-bonuses-results" class="search-results">
					<?php
					if ($searchForm->c == SearchForm::CAT_INDEX) {
						$searchForm->bonusesDataProvider->pagination->pageSize = 4;
						$searchForm->bonusesDataProvider->totalCount = 4;
						} else {
							$searchForm->blogPostDataProvider->pagination = false;
                        }

					echo ListView::widget([
						'dataProvider' => $searchForm->bonusesDataProvider,
						'itemView' => '../bonuses/_search_item',
						'emptyText' => "",
						'summary' => '',
						'options' => ['class' => 'container casino-list casino-list--search'],
						'itemOptions' => ['class' => 'casino-item casino-item-bonuses casino-item--search']
					]);
					?>
                </div>
            </div>
		<?php } ?>
		<?php if (($searchForm->c == SearchForm::CAT_INDEX || $searchForm->c == SearchForm::CAT_BLOG) && $searchForm->blogPostDataProvider->getTotalCount() > 0) { ?>
            <div class="search-results-item">
				
                    <div class="container search-results-header">
                            <a href="<?= Url::to(['search/' . SearchForm::CAT_BLOG . '/' . Yii::$app->request->get('q')]) ?>" class="search-results-title">
                                <span class="search-results-name"><?=Yii::t('app','News')?></span>
								<?= Yii::t('app', '<span class="search-results-amount">[{amount} results]</span>', [
									'amount' => $searchForm->blogPostDataProvider->getTotalCount()
								]) ?>
                            </a>
                    </div>
				
                <div id="search-news-results" class="search-results">
                    <section class="container news-wrap">
						<?php
						if ($searchForm->c == SearchForm::CAT_INDEX) {
							$searchForm->blogPostDataProvider->pagination->pageSize = 6;
							$searchForm->blogPostDataProvider->totalCount = 6;
						} else {
							$searchForm->blogPostDataProvider->pagination = false;
                        }
						echo ListView::widget([
							'dataProvider' => $searchForm->blogPostDataProvider,
							'itemView' => '../blog/_search_item',
							'emptyText' => "",
							'summary' => '',
							'options' => ['class' => 'news'],
							'itemOptions' => ['class' => 'news-item']
						]);
						?>
                    </section>
                </div>
            </div>
		<?php } ?>
    </div>

	<?php } ?>
</div>
<?php switch ($searchForm->c) {
	case (SearchForm::CAT_CASINO):
		$pages = new Pagination(['totalCount' => $searchForm->siteDataProvider->query->count()]);
		break;
	case (SearchForm::CAT_GAMES):
		$pages = new Pagination(['totalCount' => $searchForm->gameDataProvider->query->count()]);
		break;
	case (SearchForm::CAT_BONUSES):
		$pages = new Pagination(['totalCount' => $searchForm->bonusesDataProvider->query->count()]);
		break;
	case (SearchForm::CAT_BLOG):
		$pages = new Pagination(['totalCount' => $searchForm->blogPostDataProvider->query->count()]);
		break;
}

if(isset($pages))
	\portal\widgets\LinkPager::widget(['pagination' => $pages])
?>
