<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 13.08.2018
 * Time: 18:25:44
 */

/* @var $model \common\models\SiteInfo */
$site = $model->site;
if ($site) {
    $favicon = $site->getSiteImages()->type('rect')->one() ? $site->getSiteImages()->type('rect')->one()->src : \yii\helpers\Url::to(['img/casino/malina-casino.png']);
    $sqr = $site->getSiteImages()->type('sqr')->one() ? $site->getSiteImages()->type('sqr')->one()->src : \yii\helpers\Url::to(['img/casino/malina-casino.png']);
    $rect = $site->getSiteImages()->type('rect')->one() ? $site->getSiteImages()->type('rect')->one()->src : \yii\helpers\Url::to(['img/casino/malina-casino.png']);
}
?>

<div class="latest-review-item">
    <span class="latest-review-img"><?= \yii\helpers\Html::img($rect) ?></span>
    <div class="latest-review-info">
        <h3 class="latest-review-title"> <?= $site->name ?></h3>
        <p class="latest-review-text"><?= \yii\helpers\StringHelper::truncate($model->description, 250) ?></p>
    </div>
    <a href="<?= \yii\helpers\Url::to(['/casino/view', 'slug' => $site->slug]); ?>" class="latest-review-link"><?=Yii::t('app','Read')?></a>
</div>
