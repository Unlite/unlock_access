<?php
use common\modules\away\widgets\ModelLinkWidget;
use yii\bootstrap\Html;
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 13.08.2018
 * Time: 18:25:44
 */

/* @var $model \common\models\SiteInfo */
$site = $model->site;
if ($site) {
    $favicon = $site->getSiteImages()->type('rect')->one()->src ?? \yii\helpers\Url::to(['img/casino/malina-casino.png']);
    $sqr = $site->getSiteImages()->type('sqr')->one()->src ?? \yii\helpers\Url::to(['img/casino/the-best-img.png']);
    $rect = $site->getSiteImages()->type('rect')->one()->src ?? \yii\helpers\Url::to(['img/casino/malina-casino.png']);
}
$mainDomain = $site->getDomains()->typeMain()->one();

$playUrl = $mainDomain ? ModelLinkWidget::widget(['model' => $mainDomain]) : ['casino/view', 'slug' => $site->slug];
?>

<div class="casino-item">
    <div class="logo">
        <a href="<?= \yii\helpers\Url::to(['/casino/view', 'slug' => $site->slug]); ?>">
            <?= \yii\helpers\Html::img($rect, ['class' => 'casino-img desktop'])?>
            <?= \yii\helpers\Html::img($sqr, ['class' => 'casino-img mobile'])?>
        </a>
        <div class="logo-rate"><span><?= number_format($site->rating, 1, '.', '') ?></span></div>
    </div>
    <div class="casino-info">
        <h2 class="casino-title"><a href="<?= \yii\helpers\Url::to(['/casino/view', 'slug' => $site->slug]); ?>"><?= $site->name ?></a></h2>
        <ul class="casino-icons">
            <li class="casino-icons-item"><?= Html::img('@web/img/icons/mini-cashback.svg') ?></li>
            <li class="casino-icons-item"><?= Html::img('@web/img/icons/mini-cashback.svg') ?></li>
            <li class="casino-icons-item"><?= Html::img('@web/img/icons/mini-cashback.svg') ?></li>
            <li class="casino-icons-item"><?= Html::img('@web/img/icons/mini-cashback.svg') ?></li>
            <li class="casino-icons-item"><?= Html::img('@web/img/icons/mini-cashback.svg') ?></li>
        </ul>
        <p class="casino-text"><?= \yii\helpers\StringHelper::truncate($model->description, 250) ?></p>
    </div>
    <div class="casino-links">
        <a href="<?= \yii\helpers\Url::to(['/casino/view', 'slug' => $site->slug]); ?>" class="casino-link casino-link--blue"><?=Yii::t('app','More')?></a>
        <?= Html::a(Yii::t('app','play'), $playUrl, ['class' => 'casino-link']) ?>
    </div>
</div>