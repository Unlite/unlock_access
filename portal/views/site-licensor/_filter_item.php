<?php

/* @var $model common\models\Licensor */
$licensor = Yii::$app->request->get('site') && isset(Yii::$app->request->get('site')['licensor']) ? Yii::$app->request->get('site')['licensor'] : null

?>
<li>
    <a class="<?=$licensor == $model->id ? 'active' : ''?>" href="<?= \yii\helpers\Url::to(['', 'site[licensor]' => $model->id]) ?>"><?= $model->name ?></a>
</li>
