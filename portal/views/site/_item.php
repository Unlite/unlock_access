<?php

use common\modules\away\widgets\ModelLinkWidget;
use yii\bootstrap\Html;
use yii\helpers\Url;

/* @var $model common\models\Site */
$favicon = $model->getSiteImages()->type('rect')->one()->src ?? Url::to(['img/casino/malina-casino.png']);
//UNUSED// $screen = $model->getSiteImages()->type('preview')->one()->src ?? Url::to(['img/casino/malina-casino.png']);

$mainDomain = $model->getDomains()->typeMain()->one();

$playUrl = $mainDomain ? ModelLinkWidget::widget(['model' => $mainDomain]) : ['casino/view', 'slug' => $model->slug];
?>

<div class="casino-item without-bonuses decoration">
    <div class="row">
        <div class="casino-logo-column">
            <div class="casino-logo-wrap">
                <div class="casino-logo">
                    <img src="<?= $favicon ?>">
                </div>
                <div class="casino-raiting">
                    <div class="casino-raiting-inner">
                        <span><?= number_format($model->rating, 1, '.', '') ?></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="casino-about-column">
            <h3><?= $model->name ?></h3>
            <p><?= $model->info['description'] ?></p>
        </div>
        <div class="cashback-buttons-column">
            <div class="cashback-buttons">
                <?= Html::a(Yii::t('app','more'), ['casino/view', 'slug' => $model->slug], ['class' => 'button-transparent']) ?>
				<?= Html::a(Yii::t('app','play'), $playUrl, ['class' => 'button']) ?>
            </div>
        </div>
    </div>
</div>

<div class="casino-item decoration">
    <div class="row justify-content-md-between">
        <div class="casino-logo-column">
            <div class="casino-logo-wrap">
                <div class="casino-logo"><img src="<?= $favicon ?>"></div>
                <div class="casino-raiting">
                    <div class="casino-raiting-inner">
                        <span><?= number_format($model->rating, 1, '.', '') ?></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="casino-about-column col-xl-6 col-lg-9 col-md-8">
            <h3><?= $model->name ?></h3>
            <p><?= $model->info['description'] ?></p>
        </div>
        <div class="cashback-buttons-column col-xl-3 col-md-12">
            <div class="cashback-buttons">
                <?= Html::a(Yii::t('app','more'), ['casino/view', 'slug' => $model->slug], ['class' => 'button-transparent']) ?>
                <?= Html::a(Yii::t('app','play'), $playUrl, ['class' => 'button']) ?>
            </div>
        </div>
    </div>
</div>
