<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 23.08.2018
 * Time: 17:16:35
 */
/* @var $amount */
use yii\widgets\ListView;
?>
<section class="latest-reviews latest-reviews--mb container">
	<h2 class="latest-title"><?=Yii::t('app','Latest casino reviews')?></h2>
	<?php $lastUpdatedSiteProvider = new \yii\data\ArrayDataProvider([
		'allModels' => \common\models\SiteInfo::find()->latest()->limit(isset($amount) ? $amount : 6)->all(),
	]); ?>
	<?= ListView::widget([
		'dataProvider' => $lastUpdatedSiteProvider,
		'itemView' => '/site-info/_review_item',
		'emptyText' => "",
		'summary' => '',
		'options' => ['class'=>'review-list']
	]);
	?>
</section>
