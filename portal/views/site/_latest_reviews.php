<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 23.08.2018
 * Time: 17:16:35
 */
/* @var $amount */
use yii\widgets\ListView;
$lastUpdatedSiteProvider = new \yii\data\ArrayDataProvider([
	'allModels' => \common\models\SiteInfo::find()->latest()->limit(isset($amount) ? $amount : 4)->all(),
]);
if($lastUpdatedSiteProvider->getCount() > 0) {
?>
<section class="latest-reviews container">
	<h2 class="latest-title"><?=Yii::t('app','Latest casino reviews')?></h2>

	<?= ListView::widget([
		'dataProvider' => $lastUpdatedSiteProvider,
		'itemView' => '/site-info/_review_item_btn',
		'emptyText' => "",
		'summary' => '',
		'options' => ['class'=>'review-list']
	]);
	?>
</section>
<?php } ?>
