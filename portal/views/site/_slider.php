<?php
use common\models\CustomList;
use yii\widgets\ListView;
use common\models\Site;
/* @var $this yii\web\View */
?>
<section>
    <div class="container">
			<div class="big-slider">
                <div class="big-slider-item big-slider-item--one">
                    <p class="big-slider-text big-slider-text--big"><?=Yii::t('app','Croupierica is online')?></p>
                    <p class="big-slider-text"><?=Yii::t('app','Cashback is available!')?></p>
                </div>
                <div class="big-slider-item big-slider-item--two">
                    <p class="big-slider-text"><?=Yii::t('app','BREAKING NEWS OF ONLINE GAMBLING WORLD')?></p>
                    <p class="big-slider-text"><?=Yii::t('app','EXPLORE OUR DATABASE AND FIND YOUR FAVORITE WEBSITES')?></p>

                </div>
            </div>
    </div>
</section>