<?php
/* @var $this yii\web\View */
/* @var $site \common\models\Site */
/* @var $model \common\models\SiteInfo */
use common\modules\away\widgets\ModelLinkWidget;
use yii\bootstrap\Html;
use yii\helpers\Url;

$favicon = $model->getSiteImages()->type('rect')->one()->src ?? \yii\helpers\Url::to(['img/casino/malina-casino.png']);
$sqr = $model->getSiteImages()->type('sqr')->one()->src ?? \yii\helpers\Url::to(['img/casino/malina-casino.png']);
$site = $model;
if ($site) {
    $favicon = $site->getSiteImages()->type('rect')->one()->src ?? \yii\helpers\Url::to(['img/casino/malina-casino.png']);
    $sqr = $site->getSiteImages()->type('sqr')->one()->src ?? \yii\helpers\Url::to(['img/casino/malina-casino.png']);
    $rect = $site->getSiteImages()->type('rect')->one()->src ?? \yii\helpers\Url::to(['img/casino/malina-casino.png']);
}
$mainDomain = $site->getDomains()->typeMain()->one();
$playUrl = $mainDomain ? ModelLinkWidget::widget(['model' => $mainDomain]) : ['casino/view', 'slug' => $site->slug];
; ?>
<div class="item big-slider-item">
    <div class="logo big-slider-logo">
        <a href="/casino/vulkan-24?target=more"><img class="casino-img" src="<?=$favicon?>" alt=""></a>
        <div class="logo-rate logo-rate--large"><span><?= number_format($model->rating, 1, '.', '') ?></span></div>
    </div>
    <div class="casino-links">
        <a href="<?= \yii\helpers\Url::to(['casino/view', 'slug' => $model->slug]) ?>" class="casino-link casino-link--blue"><?=Yii::t('app','More')?></a>
        <?= Html::a(Yii::t('app','play'), $playUrl, ['class' => 'casino-link']) ?>
    </div>
</div>