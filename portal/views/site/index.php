<?php
use yii\widgets\ListView;
/* @var $this yii\web\View 
* @var $dataProvider yii\data\ActiveDataProvider
*/
$this->title = Yii::$app->params['portal']->get('page_attribute_home_title.data.en');
$this->registerMetaTag(['name'=>'title','content' => Yii::$app->params['portal']->get('page_attribute_home_title.data.en')]);
$this->registerMetaTag(['name'=>'description','content' => Yii::$app->params['portal']->get('page_attribute_home_description.data.en')]);
?>
<?=$this->render('_slider')?>
<?=$this->render('_latest_reviews',['amount'=>4])?>
<section class="container casino-list latest-bonuses">
	<?php $actualBonusesProvider = new \yii\data\ActiveDataProvider([
		'query' => \common\models\Bonuses::find(),
		'pagination' => [
			'pageSize' => 4,
		],
        'totalCount' => 4
	]); ?>
	<?php if ($actualBonusesProvider->getCount()) { ?>
        <h2 class="latest-title"><?=Yii::t('app','Latest bonuses')?></h2>
	<?php } ?>
	<?= ListView::widget([
		'dataProvider' => $actualBonusesProvider,
		'itemView' => '../bonuses/_casino_item',
		'emptyText' => "",
		'summary' => '',
	]);
	?>

</section>
<div class="slides">
    <?=$this->render('/casino/_slider')?>
</div>
<section class="latest-news container">
    <?=$this->render('/blog/_latest_news',['amount'=>6])?>
</section>