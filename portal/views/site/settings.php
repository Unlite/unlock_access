<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 22.04.2019
 * Time: 14:51:42
 */
?>
<div class="container">
    <form action="" method="post" class="change">
        <h2 class="change-title">Change email</h2>
        <label class="change-subtitle">new email
            <input required type="text" class="change-input"></label>
        <label class="change-subtitle">confirm
            <input type="text" class="change-input">
        </label>
        <label class="change-subtitle">password
            <input required type="text" class="change-input">
        </label>
        <button class="change-btn" type="submit">Save changes</button>
    </form>
    <form action="" method="post" class="change">
        <h2 class="change-title">Change password</h2>
        <label class="change-subtitle">current password
            <input required type="text" class="change-input">
        </label>
        <label class="change-subtitle">new password
            <input required type="text" class="change-input">
        </label>
        <label class="change-subtitle">confirm password
            <input required type="text" class="change-input">
        </label>
        <button class="change-btn" type="submit">Save changes</button>
    </form>
    <form action="" method="post" class="unsub">
        <h2 class="change-title">Email unsubscribe</h2>
        <p class="unsub-text">Still want to unsubscribe? No problem. Enter your email address below and select whether
            you'd like to snooze or remove yourself from our malling list completely.
            Resubscribe anytime you would like to receive extra discounts on your pet supplies.</p>
        <div class="unsub-bottom">
            <input  required type="text" class="unsub-input">
            <button class="unsub-btn" type="submit">Submit</button>
        </div>
    </form>
</div>
