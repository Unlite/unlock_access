$(document).ready(function() {
    $(".sidebar-category-title").on("click", function(event) {
      $(this).toggleClass('active').siblings('.sidebar-menu-inner').toggleClass('active').slideToggle(500);
    });
    $(".sidebar-menu-title").on("click", function(event) {
      $(this).toggleClass('active').siblings('.sidebar-menu').toggleClass('active').slideToggle(500);
    });
    if ($(window).width() <= 1120 ) {
        $(".sidebar-menu-inner").removeClass("active");
        $(".sidebar-category-title").removeClass("active");
    }

    var mainMenu = $("#main-menu");

    $(window).resize(function(){
        if ($(window).width() <= 1120) {
            $(".sidebar-menu-inner").removeClass("active");
            $(".sidebar-menu-inner").hide();
            $(".sidebar-category-title").removeClass('active');

        } else {
            if( !$(".sidebar-menu-inner").hasClass('active') ) {
                $(".sidebar-category-title").addClass("active");
                $(".sidebar-menu-inner").addClass("active");
                $(".sidebar-menu-inner").show();
            }
        }
    });

//    $(document).on("click", ".sidebar-menu.active .sidebar-category-title", function(){
//        $(this).toggleClass('active');
//        $(this).parents(".sidebar-menu.active").find(".sidebar-menu-inner").slideToggle("slow");
//    });
});