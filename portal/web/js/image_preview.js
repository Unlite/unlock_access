/**
 * Created by Freelancer on 17.05.2019.
 */
document.getElementById('ticketform-images').addEventListener('change',function(e) {
    if (this.files && this.files.length > 0) {
        div = document.getElementById('files');
        div.innerHTML = '';

        images = [];
        Object.values(this.files).forEach(function(v) {
            const reader = new FileReader();
            let img = document.createElement('img');
            img.className = 'creating-files-item';

            reader.onload = function(e) {
                img.setAttribute('src', e.target.result);
            };
            reader.readAsDataURL(v);
            images.push(img);
        });
        images.forEach(function(v) {
            div.appendChild(v);
        });
        document.querySelector('.creating-files').style = 'display:block'
    }
})