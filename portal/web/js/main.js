$(function () {
    $(".toggle-button, .toggle-line").click(function (e) {
        e.preventDefault();

        var sliderItem = $(this).parents(".table-slider-item");
        sliderItem.find(".toggle-button, .toggle-line").toggleClass("active");
        sliderItem.find(".table-slider-content").slideToggle("slow");
    });

    // var header = $("#header");

//    header.stick_in_parent();

    // $(window).scroll(function () {
    //     var top = $(this).scrollTop();
    //
    //     if (top >= 10) {
    //         header.addClass('active');
    //     } else {
    //         header.removeClass('active');
    //     }
    // });

    // $(".main-menu.navbar-light .navbar-toggler").click(function () {
    //
    //     if (!$('.main-menu-overlay').length) {
    //         $('body').append("<div class='main-menu-overlay'></div>");
    //     } else {
    //         $(".main-menu-overlay").remove();
    //     };
    //
    // });


    // var mainMenu = $("#main-menu");

    // $(window).resize(function () {
    //
    //     if ($(window).width() > 991) {
    //         if ($('.main-menu-overlay').length) {
    //             $(".main-menu-overlay").remove()
    //         }
    //     }
    //
    //     if ($(window).width() <= 991) {
    //         if ($(".main-menu-overlay").length == 0 && $(mainMenu).hasClass("show")) {
    //             $('body').append("<div class='main-menu-overlay'></div>");
    //         }
    //     }
    //
    //
    // });

    $("#casino-slider").slick({
        infinite: true,
        arrows: true,
        dots: false,
        slidesToShow:1
    });

    $(".info-btn").click(function (e) {
        e.preventDefault();

        var sliderLink = $(this).parents('.casino-item-one').find(".info-btn");

        sliderLink.toggleClass("active");

        if ($(this).hasClass("active")) {
            sliderLink.text("Hide");
        } else {
            sliderLink.text("Show more");
        }

        var href = $(this).attr("href");
        $(href).slideToggle();
    });


//    $(".search-results-title").click(function (e) {
//        e.preventDefault();
//        var href = $(this).attr("href");
//
//        $(href).slideToggle();
//    });

    $(document).ready(function() {
        $(".big-slider").slick({infinite:true,slidesToShow:1,slidesToScroll:1});
    });

    $("#the-best-slider").slick({
        infinite:true,
        slidesToShow:3,
        slidesToScroll:1,
        autoplay:true,
        responsive: [{
            breakpoint: 1120,
            settings: {
                slidesToShow: 1,
                adaptiveHeight: true,
            }
        }]
    });

    $('#pagination').twbsPagination({
        totalPages: 35,
        visiblePages: 5,
        prev: '<span aria-hidden="true" class="prev-btn"></span>',
        next: '<span aria-hidden="true" class="next-btn"></span>',
        first: "",
        last: "",
        href: false,
    });


    //   $(".reg").click()


    $(".multy-tab.active").show();

    var body = document.body,
        multyLinkNumber, multyTabNewNumber;

    var multyLinkWidth = $(".multy-link").outerWidth();
    $("#multy-bar").outerWidth(multyLinkWidth / 2);

    function multyBar() {
        var multyFormMenu = $(".multy-form-menu"),
            multyFormMenuWidth = multyFormMenu.outerWidth();

        multyLinkWidth = $('.multy-link[data-multy-link="' + multyTabNewNumber + '"]').outerWidth();

        multyBarWidth = ($('.multy-link[data-multy-link="' + multyTabNewNumber + '"]').offset().left - $(".multy-form-menu").offset().left) + multyLinkWidth / 2;
        multyBarWidthPersent = multyBarWidth * 100 / multyFormMenuWidth + "%";
        $("#multy-bar").outerWidth(multyBarWidthPersent);
    }


    $(body).on("click", ".multy-link", function (e) {
        e.preventDefault();

        var currentMultyLinkNumber = $(".multy-link.active").data("multy-link");

        if ($(this).hasClass("check")) {
            multyLinkNumber = $(this).data("multy-link");

            if (currentMultyLinkNumber != multyLinkNumber) {
                $(".multy-link, .multy-tab").removeClass("active");
                $(".multy-tab").slideUp(500);

                $('.multy-link[data-multy-link="' + multyLinkNumber + '"]').addClass("active");
                $('.multy-tab[data-multy-form="' + multyLinkNumber + '"]').addClass("active").slideDown(500);
            }

            var multyFormMenu = $(".multy-form-menu"),
                multyFormMenuWidth = multyFormMenu.outerWidth();

            multyLinkWidth = $(this).outerWidth();

            multyBarWidth = ($(this).offset().left - $(".multy-form-menu").offset().left) + multyLinkWidth / 2;
            multyBarWidthPersent = multyBarWidth * 100 / multyFormMenuWidth + "%";
            $("#multy-bar").outerWidth(multyBarWidthPersent);
        }
    });

    $(".radio").change(function () {
        multyLinkNumber = $(".multy-link").data("multy-link");
        multyTabNewNumber = multyLinkNumber + 1;

        $('.multy-link[data-multy-link="' + multyLinkNumber + '"]').addClass("check").removeClass("active");
        $('.multy-tab[data-multy-form="' + multyLinkNumber + '"]').removeClass("active").slideUp(500);


        $('.multy-link[data-multy-link="' + multyTabNewNumber + '"]').addClass("active");
        $('.multy-tab[data-multy-form="' + multyTabNewNumber + '"]').addClass("active").slideDown(500);

        multyBar()
    });

    $(body).on("click", ".multy-button", function () {
        var multyTabCurrentNumber = $(this).parents(".multy-tab").data("multy-form"),
            multyButton = $(this).data("button");

        if (multyButton == "next") {
            multyTabNewNumber = multyTabCurrentNumber + 1;
            $('.multy-link[data-multy-link="' + multyTabCurrentNumber + '"]').addClass("check");
        } else if (multyButton == "close") {
            return;
        } else {
            multyTabNewNumber = multyTabCurrentNumber - 1;
        }

        $('.multy-link[data-multy-link="' + multyTabCurrentNumber + '"]').removeClass("active");
        $('.multy-tab[data-multy-form="' + multyTabCurrentNumber + '"]').removeClass("active").slideUp(500);

        $('.multy-link[data-multy-link="' + multyTabNewNumber + '"]').addClass("active");
        $('.multy-tab[data-multy-form="' + multyTabNewNumber + '"]').addClass("active").slideDown(500);

        multyBar()
    });

});

// $('.cashback-search-form').on('submit', function (e) {
//     e.preventDefault();
// });
