$(function () {


    var toggler = document.getElementById('toggler');
    toggler.addEventListener('click', mainNavVisibleToggle);

    $(".header-background").on("click", function (event) {
      if ($(event.target).hasClass("header-background--visible")) {
        toggler.classList.toggle('toggler--close');
        document.getElementById('nav').classList.toggle('header-middle--visible');
        document.getElementById('header-bg').classList.toggle('header-background--visible');
      }
    });

    function mainNavVisibleToggle(e) {
      e.preventDefault();
      toggler.classList.toggle('toggler--close');
      document.getElementById('nav').classList.toggle('header-middle--visible');
      document.getElementById('header-bg').classList.toggle('header-background--visible');

    }


    $('.header-category').on("touchend", function(event) {
        if (!$(event.target).hasClass('active')) {
            $(this).addClass('active');
                $(this).parent().siblings().hide();
        }
        else {
            return false;
        }
        if ($(event.target).hasClass('first-item')) {
            $('.header-category').removeClass('active');
                $('.header-category').parent().siblings().show();
        }
        // $(this).toggleClass('active');
        // $(this).parent().siblings().toggle();
    });
});
