/**
 * Created by Freelancer on 25.04.2019.
 */
class Slider {
    /**
     * @param slider
     * @param tabs
     */
    constructor(slider,tabs,form) {
        self = this;
        this.slider = slider;
        this.tabs = tabs;
        this.form = form;
        const slick_track = '.slick-track'

        if(!this.checkInputData())
            return false;

        if($(this.form.selector).length > 0){
            this.form.element = document.querySelector(this.form.selector);
            $(this.form.element).on('submit',function (event) {
                event.preventDefault();
                self.validate();
            });
        }

        $(this.slider.selector).slick({
            dots: false,
            arrows: false,
            infinite: false,
            speed: 300,
            slidesToShow: 1,
            swipe: false,
            adaptiveHeight: true,
        });

        this.slider.element = document.querySelector(this.slider.selector+' '+slick_track);
        this.slider.slides = document.querySelector(this.slider.selector+' '+slick_track).childNodes;
        var sub_tabs = [];

        document.querySelector(this.tabs.selector).childNodes.forEach(function (e) {
            if(e.nodeType == Node.ELEMENT_NODE){
                sub_tabs.push(e);
            }
        });
        this.tabs.tabs = sub_tabs;
    };

    slideTo (number){
        this.activeTab(number);
        $(this.slider.selector).slick('slickGoTo', number);
        this.slider.active_tab = number;
    };

    nextSlide () {
        if(this.slider.slides.length >= this.slider.active_tab + 1)
        this.slideTo(this.slider.active_tab+1);
    }

    prevSlide () {
        if(this.slider.slides.length-1 >= this.slider.active_tab-1)
        this.slideTo(this.slider.active_tab-1);
    }

    activeTab (number) {
        self = this;
        this.tabs.tabs.forEach(function(element){
            if($(element).hasClass(self.tabs.active_class)){
                $(element).toggleClass(self.tabs.active_class);
            }
        });
        $(this.tabs.tabs[number]).addClass(self.tabs.active_class)
    };
    
    loadForm (element,link) {
        self = this;
        $.get(link, function(success){
            $(self.form.handler).html(success);
            self.form.element = document.querySelector(self.form.selector);
            $(self.form.element).on('submit',function (event) {
                event.preventDefault();
                self.validate();
            });
            self.nextSlide();
        },'html');
        
    };

    submit  (url) {
        self = this;
        $.post({
            type:document.querySelector(this.form.selector).getAttribute('method'),
            url: url,
            data:$(this.form.element ? this.form.element : this.form.selector).serialize(),
            format:'json',
            error:function(data){
                console.log('error');
            },
            success:function(data){
               if(data.result && data.result == 'success'){
                   $(self.slider.result.selector).html(data.data);
                   self.nextSlide();
                   setTimeout(function () {
                       window.location = window.location;
                   },4000)
               }else{
                   self.prevSlide();
                   self.displayErrors(data);
               }
            },
        });
    };
    
    validate () {
        self = this;

        $.post({
            type:document.querySelector(this.form.selector).getAttribute('method'),
            url:document.querySelector(this.form.selector).getAttribute('action'),
            data:$(this.form.element ? this.form.element : this.form.selector).serialize(),
            format:'raw',
            error:function(data){

            },
            success:function(data){
                if(data.result && data.result == 'success'){
                    $(self.slider.check.selector).html(data.data);
                    self.slider.check.element = document.querySelector(self.slider.check.selector).firstElementChild;
                    self.nextSlide();
                }else{
                    self.displayErrors(data);
                }
            },
        });
    };

    displayErrors (data) {
        this.emptyErrors();
        $.each(data, function(key, val) {
            $('#'+key).parent().find('.help-block').html(val[0]);
            $('#'+key).parent().find('.form-group').addClass('has-error');
        });
        $.each($(this.slider.selector),function (key,val) {
            val.slick.resize();
        })
    }

    emptyErrors () {
        $(this.form.selector).find('.help-block').each(function (i,v) {
            $(v).html('');
        })
    };

    resize () {
        $(this.slider.selector)[0].slick.refresh();
    }

    
    checkInputData () {
        this.errors = [];

        if(!this.slider.selector){
            this.errors.push('First param should contain "selector" subparam');
        }
        if(!this.slider.active_tab){
            this.slider.active_tab = 0;
        }
        if(!this.slider.result){
            this.errors.push('First param "slider.result" should contain subparam');
        }
        if(!this.slider.result.selector){
            this.errors.push('Second param "slider.result.selector" should contain subparam');
        }
        if(!this.tabs.selector){
            this.errors.push('Second param should contain "selector" subparam');
        }
        if(!this.tabs.active_class){
            this.errors.push('Second param should contain "active_class" subparam');
        }
        if(!this.form){
            this.errors.push('Third param should be');
        }
        if(!this.form.selector){
            this.errors.push('Third param should contain "selector" subparam');
        }
        if(!this.form.currency){
            this.errors.push('Third param should contain "currency" subparam');
        }
        if(this.errors.length > 0){
            this.errors.forEach(function (element) {
              console.error(element);
            });
            return false;
        }
        return true;
    }
}