function casinoPopupToggling() {
    var casinoPopupSlider;
    $('.js-popup-close').on('click', function () {
        $('.js-popup.shown').removeClass('shown').fadeOut(300);
    })
    $('.js-casino-popup-show').on('click', function () {
        $('.casino-popup').addClass('shown').fadeIn(300);

        if (!casinoPopupSlider) casinoPopupSlider = $('.js-casino-popup-slider').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1
        })
    })
}

casinoPopupToggling();

function loginPopup() {
    $('.js-popup-close').on('click', function () {
        $('.js-popup.shown').removeClass('shown').fadeOut(300);
    })
    $('.js-login-popup-show').on('click', function () {
        if ($(this).hasClass('js-header-log')) {
            $('.js-form-tab-log').addClass('js-form-tab--active');
            $('.js-form-tab-reg').removeClass('js-form-tab--active');
            $('.js-form-log').addClass('js-form--active');
            $('.js-form-reg').removeClass('js-form--active');
        } else {
            $('.js-form-tab-reg').addClass('js-form-tab--active');
            $('.js-form-tab-log').removeClass('js-form-tab--active');
            $('.js-form-reg').addClass('js-form--active');
            $('.js-form-log').removeClass('js-form--active');
        };
        $('.login-popup').addClass('shown').fadeIn(300);
    })
}

loginPopup();

function attachPopup() {
    $('.js-popup-close').on('click', function () {
        $('.js-popup.shown').removeClass('shown').fadeOut(300);
        $('.js-attach-popup-show').removeClass('js-attach-popup-show--active');
    })
    $('.js-attach-popup-show').on('click', function () {
        $('.chat-popup').addClass('shown').fadeIn(300);
        $('.js-attach-popup-show').addClass('js-attach-popup-show--active');

    })
}

attachPopup();