<?php

/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 16.04.2018
 * Time: 13:12:17
 */

namespace portal\widgets;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class LinkPager extends \yii\widgets\LinkPager
{
	/**
	 * @var array HTML attributes for the previous link in a pager container tag.
	 * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
	 */
	public $prevPageHtmlOptions = ['rel' => 'prev'];
	/**
	 * @var array HTML attributes for the next link in a pager container tag.
	 * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
	 */
	public $nextPageHtmlOptions = ['rel' => 'next'];
	/**
	 * @var string the CSS class for the "previous" page button.
	 */
	public $prevPageCssClass = 'page-item prev';
	/**
	 * @var string the CSS class for the "next" page button.
	 */
	public $nextPageCssClass = 'page-item next';
	/**
	 * @var string|bool the label for the "next" page button. Note that this will NOT be HTML-encoded.
	 * If this property is false, the "next" page button will not be displayed.
	 */
	public $nextPageLabel = '<span aria-hidden="true" class="next-btn">></span>';
	/**
	 * @var string|bool the text label for the previous page button. Note that this will NOT be HTML-encoded.
	 * If this property is false, the "previous" page button will not be displayed.
	 */
	public $prevPageLabel = '<span aria-hidden="true" class="prev-btn"><</span>';
	/**
	 * @var array HTML attributes for the pager container tag.
	 * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
	 */
	public $options = ['class' => 'pagination'];
	/**
	 * @var array HTML attributes for the link in a pager container tag.
	 * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
	 */
	public $linkOptions = ['class' => 'page-link'];
	/**
	 * @var array HTML attributes which will be applied to all link containers
	 * @since 2.0.13
	 */
	public $linkContainerOptions = ['class' => 'page-item'];
	/**
	 * @var array the container of pagination
	 */
	public $container = [
		'div' => [
			'options' => [
				'class' => 'pagination-wrap'
			],
			'child' => [
				'div' => [
					'options' => [
						'class' => 'pagination-list'
					]
				]
			]
		]
	];

	/**
	 * Renders the page buttons.
	 * @return string the rendering result
	 */
	protected function renderPageButtons()
	{
		$pageCount = $this->pagination->getPageCount();
		if ($pageCount < 2 && $this->hideOnSinglePage) {
			return '';
		}

		$buttons = [];
		$currentPage = $this->pagination->getPage();

		// first page
		$firstPageLabel = $this->firstPageLabel === true ? '1' : $this->firstPageLabel;
		if ($firstPageLabel !== false) {
			$buttons[] = $this->renderPageButton($firstPageLabel, 0, $this->firstPageCssClass, $currentPage <= 0, false);
		}

		// prev page
		if ($this->prevPageLabel !== false) {
			if (($page = $currentPage - 1) < 0) {
				$page = 0;
			}
			$buttons[] = $this->renderPageButton($this->prevPageLabel, $page, $this->prevPageCssClass, $currentPage <= 0, false, $this->prevPageHtmlOptions);
		}

		// internal pages
		list($beginPage, $endPage) = $this->getPageRange();
		for ($i = $beginPage; $i <= $endPage; ++$i) {
			$buttons[] = $this->renderPageButton($i + 1, $i, null, $this->disableCurrentPageButton && $i == $currentPage, $i == $currentPage);
		}

		// next page
		if ($this->nextPageLabel !== false) {
			if (($page = $currentPage + 1) >= $pageCount - 1) {
				$page = $pageCount - 1;
			}
			$buttons[] = $this->renderPageButton($this->nextPageLabel, $page, $this->nextPageCssClass, $currentPage >= $pageCount - 1, false, $this->nextPageHtmlOptions);
		}

		// last page
		$lastPageLabel = $this->lastPageLabel === true ? $pageCount : $this->lastPageLabel;
		if ($lastPageLabel !== false) {
			$buttons[] = $this->renderPageButton($lastPageLabel, $pageCount - 1, $this->lastPageCssClass, $currentPage >= $pageCount - 1, false);
		}

		$options = $this->options;
		$tag = ArrayHelper::remove($options, 'tag', 'ul');
		$result = $this->renderNestedContainer($this->container,Html::tag($tag, implode("\n", $buttons), $options));
		return $result;
	}

	/**
	 * Render container for pagination
	 * @param $buttons string stringified pagination buttons
	 */
	protected function renderContainer($buttons)
	{
		foreach ($this->container as $container){

		}
	}

	/**
	 * Recursively render nested containers
	 * @param $container array
	 * @param $buttons string
	 * @param $rendered_container string
	 * @return string
	 */
	protected function renderNestedContainer($container,$buttons,$rendered_container = ''){
		foreach ($container as $tag => $container_options){
			$rendered_container .= Html::tag($tag,isset($container_options['child']) ? $this->renderNestedContainer($container_options['child'],$buttons) : $buttons,$container_options['options']);
		}
		if(!isset($container['child']))
			return $rendered_container;
	}

	/**
	 * Renders a page button.
	 * You may override this method to customize the generation of page buttons.
	 * @param string $label the text label for the button
	 * @param int $page the page number
	 * @param string $class the CSS class for the page button.
	 * @param bool $disabled whether this page button is disabled
	 * @param bool $active whether this page button is active
	 * @param array $linkOptions custom link html attribute options
	 * @return string the rendering result
	 */
	protected function renderPageButton($label, $page, $class, $disabled, $active, $linkOptions = null)
	{
		$options = $this->linkContainerOptions;
		$linkWrapTag = ArrayHelper::remove($options, 'tag', 'li');
		Html::addCssClass($options, empty($class) ? $this->pageCssClass : $class);

		if ($active) {
			Html::addCssClass($options, $this->activePageCssClass);
		}
		if ($disabled) {
			Html::addCssClass($options, $this->disabledPageCssClass);
//			$disabledItemOptions = $this->disabledListItemSubTagOptions;
//			$tag = ArrayHelper::remove($disabledItemOptions, 'tag', 'span');
//			return Html::tag($linkWrapTag, Html::tag($tag, $label, $disabledItemOptions), $options);

			if (!$linkOptions)
				$linkOptions = $this->linkOptions;
			else
				$linkOptions = ArrayHelper::merge($this->linkOptions, $linkOptions);
			$linkOptions['data-page'] = $page;

			return Html::tag($linkWrapTag, Html::a($label,$this->pagination->createUrl($page),$linkOptions), $options);
		}

		if (!$linkOptions)
			$linkOptions = $this->linkOptions;
		else
			$linkOptions = ArrayHelper::merge($this->linkOptions, $linkOptions);
		$linkOptions['data-page'] = $page;

		return Html::tag($linkWrapTag, Html::a($label, $this->pagination->createUrl($page), $linkOptions), $options);
	}

}